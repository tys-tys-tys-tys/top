<?php
namespace Common;

class Api_Ins
{

    public static function soapRequest($url, $data)
    {
        $data = array('request' => $data);
        $options = array(
            'trace' => true,
            'encoding' => 'UTF-8',
            'connection_timeout' => 120,
            'soap_version' => SOAP_1_1,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'keep_alive' => true
        );
        try {
            $soapclient = new \Soapclient($url, $options);
        } catch (Exception $e) {
            $this->logs['soap']['client'][] = $e;
            return false;
        }
        $funarr = $soapclient->__getFunctions();

        $funNewArr[0] = $funarr[2];

        $fun = substr($funNewArr[0], strpos($funNewArr[0], ' ') + 1, strpos($funNewArr[0], '(') - strpos($funNewArr[0], ' ') - 1);
        //dump($fun);exit();
        try {
            $back = $soapclient->$fun($data);
            dump($back);exit();
        } catch (Exception $e) {

            $this->logs['soap'][$fun][] = $e;

            $s = $soapclient->__getLastRequest();
            print_r($s);
            exit;
            return false;
        }
        return $back;
    }


    public static function getQueryInsuranceProduct($flightData)
    {
        $api = new Api_Ins();
        $url = "http://ws.51book.com:8000/ltips/services/insuranceProductService1.0?wsdl";
        $result = $api->soapRequest($url, $flightData);
        return $result;
    }

}

?>