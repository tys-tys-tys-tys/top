<?php

namespace Common;

/**
 * 短信发送
 *
 * @author J
 */
class Sms
{

    //10分钟内不能超过3条
    public static function Send($phone, $message, $cdkey, $pwd, $sign, $operatorId)
    {
        if (empty($cdkey)) {
            return false;
        }

        if (!C('SEND_SMS')) {
            return false;
        }

        if (strlen($phone) != 11) {
            return false;
        }
        //reg
        //http://sdk4report.eucp.b2m.cn:8080/sdkproxy/regist.action?cdkey=6SDK-EMY-6688-KIXPR&password=664430
        //emay短信

        $msg = urlencode($sign . $message);

        $url = 'http://sdk4report.eucp.b2m.cn:8080/sdkproxy/sendsms.action?cdkey=' . $cdkey . '&password=' . $pwd . '&phone=' . $phone . '&message=' . $msg . '&addserial=';

        $html = file_get_contents($url);

        $arr = explode('<error>0</error>', $html);

        $data['operator_id'] = $operatorId;
        $data['mobile'] = $phone;
        $data['message'] = $message;
        $data['create_time'] = date("Y-m-d H:i:s", time());

        if (count($arr) > 1) {
            $data['result'] = 1;
            $result = M('log_sms')->add($data);
            return true;
        } else {
            $data['result'] = 0;
            $result = M('log_sms')->add($data);
            return false;
        }
    }

    public static function GetBalance()
    {
        $url = 'http://sdk4report.eucp.b2m.cn:8080/sdkproxy/querybalance.action?cdkey=6SDK-EMY-6688-KIXPR&password=664438';

        /* <?xml version="1.0" encoding="UTF-8"?><response><error>0</error><message>9.4</message></response> */
        $html = file_get_contents($url);

        $r = preg_match('/<message>(?P<balance>.+?)<\/message>/', $html, $matches, PREG_OFFSET_CAPTURE);
        $balance = $matches['balance'][0];
        return $balance;
    }

}
