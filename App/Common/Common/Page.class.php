<?php

namespace Common\Common;

class Page
{

    private $total;
    private $listRows;
    private $limit;
    private $uri;
    private $pageNum;
    private $config = array('header' => "个记录", "prev" => "上一页", "next" => "下一页", "first" => "首 页", "last" => "末 页");
    private $listNum = 9;

    /*
     * $total 
     * $listRows
     */

    public function __construct($total, $listRows = 10, $pa = "")
    {
        $this->total = $total;
        $this->listRows = $listRows;
        $this->uri = $this->getUri($pa);
        $this->page = !empty($_GET["page"]) ? $_GET["page"] : 1;
        $this->pageNum = ceil($this->total / $this->listRows);
        $this->limit = $this->setLimit();

        if ($_GET['page'] > $this->pageNum) {
            $this->page = $page;
            $this->pageNum = ceil($this->total / $this->listRows);
            $page = intval($this->pageNum);
            $this->page = $page;
            $this->limit = "Limit " . abs(($this->pageNum - 1)) * $this->listRows . ", {$this->listRows}";
        }
    }

    private function setLimit()
    {

        return "Limit " . ($this->page - 1) * $this->listRows . ", {$this->listRows}";
    }

    private function getUri($pa)
    {
        $url = $_SERVER["REQUEST_URI"] . (strpos($_SERVER["REQUEST_URI"], '?') ? '' : "?") . $pa;
        $parse = parse_url($url);


        if (isset($parse["query"])) {
            parse_str($parse['query'], $params);
            unset($params["page"]);
            $url = $parse['path'] . '?' . http_build_query($params);
        }

        return $url;
    }

    function __get($args)
    {
        if ($args == "limit")
            return $this->limit;
        else
            return null;
    }

    private function start()
    {
        if ($this->total == 0)
            return 0;
        else
            return ($this->page - 1) * $this->listRows + 1;
    }

    private function end()
    {
        return min($this->page * $this->listRows, $this->total);
    }

    private function first()
    {
        $html = "";
        if ($this->page == 1)
            $html .= '';
        else
            $html .= "&nbsp;<a href='{$this->uri}&page=1' style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #a1a1a1;line-height: 1.42857;margin-left: -1px;'><{$this->config["first"]}</a>&nbsp;";

        return $html;
    }

    private function prev()
    {
        $html = "";
        if ($this->page == 1)
            $html .= '';
        else
            $html .= "&nbsp;<a href='{$this->uri}&page=" . ($this->page - 1) . "' style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #a1a1a1;line-height: 1.42857;margin-left: -1px;'>{$this->config["prev"]}</a>&nbsp;";

        return $html;
    }

    private function pageList()
    {
        $linkPage = "";

        $inum = floor($this->listNum / 2);

        for ($i = $inum; $i >= 1; $i--) {
            $page = $this->page - $i;

            if ($page < 1)
                continue;

            $linkPage .= "&nbsp;<a href='{$this->uri}&page={$page}'  style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #a1a1a1;line-height: 1.42857;margin-left: -1px;'>{$page}</a>&nbsp;";
        }

        $linkPage .= "&nbsp;<span  style='padding:6px 12px; background-color: #f08519;border: 1px solid #ddd;color: #fff;line-height: 1.42857;margin-left: -1px;'>{$this->page}</span>&nbsp;";


        for ($i = 1; $i <= $inum; $i++) {
            $page = $this->page + $i;
            if ($page <= $this->pageNum)
                $linkPage .= "&nbsp;<a href='{$this->uri}&page={$page}'  style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #a1a1a1;line-height: 1.42857;margin-left: -1px;'>{$page}</a>&nbsp;";
            else
                break;
        }

        return $linkPage;
    }

    private function next()
    {
        $html = "";
        if ($this->page == $this->pageNum)
            $html .= '';
        else
            $html .= "&nbsp;<a href='{$this->uri}&page=" . ($this->page + 1) . "' style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #5c5c5c;line-height: 1.42857;margin-left: -1px;'>{$this->config["next"]}</a>&nbsp;";

        return $html;
    }

    private function last()
    {
        $html = "";
        if ($this->page == $this->pageNum)
            $html .= '';
        else
            $html .= "&nbsp;<a href='{$this->uri}&page=" . ($this->pageNum) . "' style='padding:6px 12px; background-color: #fff;border: 1px solid #ddd;color: #5c5c5c;line-height: 1.42857;margin-left: -1px;'>{$this->config["last"]}</a>&nbsp;";

        return $html;
    }

    private function goPage()
    {
        return '&nbsp;<input type="text" onkeydown="javascript:if(event.keyCode==13){var page=(this.value>' . $this->pageNum . ')?' . $this->pageNum . ':this.value;location=\'' . $this->uri . '&page=\'+page+\'\'}" value="' . $this->page . '" style="width:40px;padding:5px 12px 6px 12px;border:1px solid #ccc;margin-right:5px;"><input type="button" class="btn" style="padding:5px 12px; background: #ecf2f3;border: 1px solid #ddd;color: #5c5c5c;line-height: 1.42857;margin-left: -1px;" value="GO" onclick="javascript:var page=(this.previousSibling.value>' . $this->pageNum . ')?' . $this->pageNum . ':this.previousSibling.value;location=\'' . $this->uri . '&page=\'+page+\'\'">&nbsp;';
    }

    function fpage($display = array(0, 1, 2, 3, 4, 5, 6, 7, 8))
    {
        //$html[0] = "&nbsp;共有<b>{$this->total}</b>{$this->config["header"]}&nbsp;";
        // $html[1] = "&nbsp;每页显示<b>" . ($this->end() - $this->start() + 1) . "</b>条，本页<b>{$this->start()}-{$this->end()}</b>条&nbsp;";
        //$html[2] = "&nbsp;<b>{$this->page}/{$this->pageNum}</b>页&nbsp;";

        $html[3] = $this->first();
        $html[4] = $this->prev();
        $html[5] = $this->pageList();
        $html[6] = $this->next();
        $html[7] = $this->last();
        $html[8] = $this->goPage();
        $fpage = '';
        foreach ($display as $index) {
            $fpage .= $html[$index];
        }

        return $fpage;
    }

}
