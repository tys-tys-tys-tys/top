<?php

namespace Common\Common;

use Common\Common\CommonController;
use Common\Top;

class AdminController extends CommonController
{

    public function __construct()
    {
        parent::__construct();

        if (!session('user_name') && !session('user_id') && !session('role_id')) {
            $this->redirect('Manager/showLogin');
        } else {
            $controllerName = strtolower(CONTROLLER_NAME);
            $actionName = ACTION_NAME;

            $authId = M('auth')->where("controller = '{$controllerName}' && action = '{$actionName}'")->field('id')->find();

            $now_ac = $authId['id'];

            $sql = "select * from top_user as a join top_role as b on a.role_id=b.id where a.id=" . session('user_id') . " limit 0,1";
            $info = D()->query($sql);
            $role_auth_ac = $info[0]['auth_ids'];

            $ac_array = array('Index-index', 'Index-left', 'Index-head', 'Index-right', 'Index-footer', 'Index-logout');

            if (!in_array($now_ac, $ac_array) && session('user_id') != 1 && session('entity_type') != Top::EntityTypeSystem && strpos($role_auth_ac, $now_ac) === false) {
                $this->warnStatus = '10404';
                $this->warnInfo = '对不起你没有权限访问';
            }
        }

    }

}
