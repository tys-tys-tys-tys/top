<?php

function GetBank($str)
{
    $str1 = substr($str, 0, 4);
    $str2 = substr($str, -4, 4);
//    return $str1 . '******' . $str2;
    return $str2;
}

function GetYuan($fen)
{
    return Common\Utils::getYuan($fen);
}

function GetYuanWhole($fen)
{
    $yuan = Common\Utils::getYuan($fen);
    return substr($yuan, 0, strpos($yuan, "."));
}

function GetOperator($id)
{
    $data = M('operator')->field('name')->find($id);
    return $data['name'];
}

function getAgencyState($id)
{
    $data = M('agency')->where("id = '{$id}'")->field('state')->find();
    return $data['state'];
}

function getAgencyAccountState($id)
{
    $data = M('agency')->where("id = '{$id}'")->field("account_state")->find();
    return $data['account_state'];
}

function getProviderState($id)
{
    $data = M('provider')->where("id = '{$id}'")->field('state')->find();
    return $data['state'];
}

function getProviderAccountState($id)
{
    $data = M('provider')->where("id = '{$id}'")->field("account_state")->find();
    return $data['account_state'];
}
function  log_result($file,$word)
{
    $fp = fopen($file,"a");
    flock($fp, LOCK_EX) ;
    fwrite($fp,"执行日期：".strftime("%Y-%m-%d-%H：%M：%S",time())."\n".$word."\n\n");
    flock($fp, LOCK_UN);
    fclose($fp);
}

function DayDaXie($number)
{
    $numbers = substr($number, 0, 2);
    $arr = array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");

    if (strlen($numbers) == 1) {
        $result = $arr[$numbers];
    } else {
        if ($numbers == 10) {
            $result = "十";
        } else {
            if ($numbers < 20) {
                $result = "十";
            } else {
                $result = $arr[substr($numbers, 0, 1)] . "十";
            }
            if (substr($numbers, 1, 1) != "0") {
                $result .= $arr[substr($numbers, 1, 1)];
            }
        }
    }
    return $result;
}

function GetCommissionRate($num)
{
    return Common\Utils::DispCommissionRate($num);
}

function GetRealImageUrl($num)
{
    return Common\Utils::GetRealImageUrl($num);
}
//id加密
function setEncrypt($id)
{
    return Common\Utils::setEncrypt($id);
}

function getPicPath($img)
{
    if (strpos($img, ',')) {
        $size = strpos($img, ",");
        $img = substr($img, 0, $size);
        return $img;
    } else {
        return $img;
    }
}


function getOperatorAccountNum($id)
{
    $operatorType = \Common\Top::EntityTypeOperator;
    $operatorNum = M('user')->where("entity_id = '{$id}' && entity_type = '{$operatorType}'")->count();
    return $operatorNum;
}


function getProviderAccountNum($operatorId)
{
    $providerType = \Common\Top::EntityTypeProvider;
    $state = \Common\Top::StateDeleted;
    $providerList = M('provider')->where("operator_id  = '{$operatorId}'")->field('id')->select();

    $providerNum = 0;
    foreach ($providerList as $k => $v) {
        $providerAccountNum = M('user')->where("entity_id = '{$v['id']}' && entity_type = '{$providerType}' && state != $state")->count();
        $providerNum += $providerAccountNum;
    }

    return $providerNum;
}


function getAgencyAccountNum($operatorId)
{
    $state = \Common\Top::StateDeleted;
    $agencyNum = M('agency')->where("operator_id = '{$operatorId}' && state != $state")->field('id')->count();
    return $agencyNum;
}

function getOperatorTotalAccountNum($id)
{
    return getOperatorAccountNum($id) + getProviderAccountNum($id) + getAgencyAccountNum($id);
}

function getUserName($id)
{
    $info = M('role')->where("id = '{$id}'")->field('id,name')->find();
    return $info['name'];
}

function getOperatorName($id)
{
    if ($id == 1) {
        $name = '北京';
    } else if ($id == 2) {
        $name = '内部';
    } else if ($id == 3) {
        $name = '天津';
    } else if ($id == 4) {
        $name = '河北';
    } else if ($id == 5) {
        $name = '唐山';
    } else if ($id == 0) {
        $name = '平台';
    } else if ($id == 6) {
        $name = '安徽';
    }

    return $name;
}
//查询供应商名称
function getSupplierName($id)
{
    $info = M('provider')->where("id = '{$id}'")->field('id,name')->find();
    return $info['name'];
}
//首页栏目二维数组
function getColumnSort(){
    $column_list = array(
        array(
            'name'=>'特价',
            'url'=>U('BargainPrice/index'),
            'icon'=>8,
        ),
        array(
            'name'=>'国内游',
            'url'=>U('Inbound/index'),
            'icon'=>1,
        ),
        array(
            'name'=>'出境游',
            'url'=>U('Outbound/index'),
            'icon'=>2,
        ),
        array(
            'name'=>'周边游',
            'url'=>U('Around/index'),
            'icon'=>3,
        ),
        array(
            'name'=>'自由行',
            'url'=>U('Freetravel/index'),
            'icon'=>4,
        ),
        array(
            'name'=>'机票',
            'url'=>U('WxJsAPI/index'),
            'icon'=>5,
        ),
        array(
            'name'=>'门票',
            'url'=>U('Ticket/index'),
            'icon'=>7,
        ),
        array(
            'name'=>'其他',
            'url'=>U('Product/index'),
            'icon'=>6,
        ),
    );
    return $column_list;
}

function getContractEndTime($contract_end_time)
{
    $time = date('Y-m-d 00:00:00', strtotime('-30 day', strtotime($contract_end_time)));
    return $time;
}

function getOneDeparture($dep)
{
    $departure = explode(" ", $dep);
    if ($departure['0'] == $departure['1']) {
        unset($departure['1']);
        $departure = implode(" ", $departure);
    } else {
        $departure = implode(" ", $departure);
    }

    return $departure;
}


function getDisCount($num)
{
    if ($num < 1) {
        $num = substr($num, 2);
    } else {
        $num = str_replace('.', '', $num);
        $num = substr($num, 0, 2);
    }

    return $num;
}


//三字码
function nameToAirport($code)
{
    $codearr = array(
        'AVA' => '黄果树机场',
        'AYN' => '北郊机场',
        'AKU' => '阿克苏机场',
        'AAT' => '阿勒泰机场',
        'AKA' => '安康机场',
        'AQG' => '安庆机场',
        'AOG' => '鞍山机场',
        'AEB' => '巴马机场',
        'BSD' => '云瑞机场',
        'BFU' => '蚌埠机场',
        'BDU' => '昌都邦达',
        'BAV' => '二里半机场',
        'BFJ' => '飞雄机场',
        'BHY' => '福城机场',
        'PEK' => '首都机场',
        'NAY' => '南苑机场',
        'BPL' => '博乐阿拉山口机场',
        'CGQ' => '龙嘉国际机场',
        'CHW' => '酒泉机场',
        'CYI' => '嘉义机场',
        'CGD' => '桃花源机场',
        'CMJ' => '七美机场',
        'BPX' => '邦达机场',
        'CSX' => '黄花机场',
        'CIH' => '王村机场',
        'CZX' => '奔牛机场',
        'CNI' => '长海机场',
        'CHG' => '朝阳机场',
        'CTU' => '双流机场',
        'CIF' => '玉龙机场',
        'CKG' => '江北机场',
        'NBS' => '长白山机场',
        'DLC' => '国际机场',
        'DCY' => '亚丁机场',
        'DLU' => '荒草坝机场',
        'DDG' => '浪头机场',
        'DAT' => '大同机场',
        'DAX' => '达州机场',
        'DIG' => '香格里拉机场',
        'DOY' => '胜利机场',
        'DNH' => '敦煌机场',
        'DQA' => '萨尔图机场',
        'LUM' => '芒市机场',
        'DSN' => '伊金霍洛机场',
        'ENH' => '恩施机场',
        'ENY' => '二十里铺机场',
        'ERL' => '赛乌苏国际机场',
        'FUO' => '沙堤机场',
        'FYN' => '富蕴机场',
        'FOC' => '长乐机场',
        'FUG' => '西关机场',
        'JIL' => '二台子机场',
        'KOW' => '黄金机场',
        'GOQ' => '格尔木机场',
        'GYS' => '盘龙机场',
        'GXH' => '夏河机场',
        'GHN' => '广汉机场',
        'CAN' => '白云机场',
        'KWL' => '两江机场',
        'KWE' => '龙洞堡机场',
        'KHH' => '高雄机场',
        'GYU' => '六盘山机场',
        'HAK' => '美兰机场',
        'HLD' => '东山机场',
        'HMI' => '哈密机场',
        'HDG' => '邯郸机场',
        'HSC' => '韶关机场',
        'HGH' => '萧山机场',
        'HZG' => '西关机场',
        'HRB' => '太平机场',
        'HFE' => '骆岗机场',
        'HEK' => '黑河机场',
        'HTN' => '和田机场',
        'HPG' => '红坪机场',
        'HET' => '白塔国际机场',
        'TXN' => '屯溪机场',
        'HYN' => '路桥机场',
        'HUZ' => '平潭机场',
        'HJJ' => '芷江机场',
        'HIA' => '涟水机场',
        'HNY' => '东江机场',
        'JMU' => '佳木斯机场',
        'JGN' => '嘉峪关机场',
        'TNA' => '遥墙机场',
        'JDZ' => '罗家机场',
        'JGS' => '井冈山机场',
        'JNG' => '济宁机场',
        'JUH' => '九华山机场',
        'JNZ' => '小岭子机场',
        'JIU' => '庐山机场',
        'JIC' => '金昌机场',
        'JZH' => '黄龙机场',
        'JXA' => '兴凯湖机场',
        'JJN' => '晋江机场',
        'KJI' => '喀纳斯机场',
        'KRY' => '克拉玛依机场',
        'KHG' => '喀什机场',
        'KRL' => '库尔勒机场',
        'KCA' => '库车机场',
        'KMG' => '巫家坝机场',
        'KNH' => '尚义机场',
        'KGT' => '康定机场',
        'KNC' => '吉安机场',
        'LHW' => '兰州机场',
        'LXA' => '贡嘎机场',
        'LCX' => '连城机场',
        'LYG' => '白塔埠机场',
        'LJG' => '三义机场',
        'LZJ' => '云南丽江机场',
        'LNJ' => '临沧机场',
        'LYI' => '沭埠岭机场',
        'LZY' => '米林机场',
        'HZH' => '黎平机场',
        'LZH' => '白莲机场',
        'LYA' => '洛阳机场',
        'LZO' => '蓝田机场',
        'LCX' => '福建龙岩冠豸山机场',
        'LLB' => '荔波机场',
        'LUM' => '芒市机场',
        'NZH' => '西郊机场',
        'MXZ' => '梅州机场',
        'MZG' => '马公机场',
        'MIG' => '南郊机场',
        'MDG' => '海浪机场',
        'OHE' => '古莲机场',
        'KHN' => '昌北机场',
        'NAO' => '高坪机场',
        'NGQ' => '昆莎机场',
        'NKG' => '禄口机场',
        'NNG' => '吴圩机场',
        'NTG' => '兴东机场',
        'NLH' => '泸沽湖机场',
        'NNY' => '姜营机场',
        'NGB' => '栎社机场',
        'NNN' => '嫩江机场',
        'NLT' => '那拉提机场',
        'PZI' => '保安营机场',
        'IQM' => '且末机场',
        'TAO' => '流亭机场',
        'IQN' => '庆阳机场',
        'NDG' => '三家子机场',
        'JJN' => '泉州晋江国际机场',
        'JUZ' => '衢州机场',
        'JIQ' => '武陵山机场',
        'RMQ' => '清泉岗机场',
        'RKZ' => '和平机场',
        'RLK' => '天吉泰机场',
        'SZV' => '苏州机场',
        'SYX' => '凤凰机场',
        'SHA' => '虹桥机场',
        'PVG' => '浦东机场',
        'SWA' => '揭阳潮汕机场',
        'SHE' => '桃仙机场',
        'SZX' => '宝安机场',
        'SJW' => '正定机场',
        'SYM' => '普洱机场',
        'SHS' => '沙市机场',
        'SHP' => '山海关机场',
        'SWA' => '外砂机场',
        'TNH' => '通化机场',
        'TCG' => '塔城机场',
        'TVS' => '三女河机场',
        'TTT' => '丰年机场',
        'TLQ' => '交河机场',
        'TYN' => '武宿机场',
        'TVS' => '三女河机场',
        'TSN' => '滨海机场',
        'TSA' => '台北松山机场',
        'TPE' => '桃园机场',
        'TGO' => '通辽机场',
        'TEN' => '凤凰机场',
        'TCZ' => '驼峰机场',
        'THQ' => '麦积山机场',
        'TNN' => '台南机场',
        'HYN' => '台州路桥机场',
        'HUN' => '花莲机场',
        'HLH' => '乌兰浩特机场',
        'URC' => '地窝堡机场',
        'WXN' => '五桥机场',
        'WEF' => '潍坊机场',
        'WEH' => '文登大水泊机场',
        'WOT' => '望安机场',
        'WNH' => '普者黑机场',
        'WNZ' => '永强机场',
        'WUA' => '乌海机场',
        'WUH' => '天河机场',
        'WJD' => '王家墩国际机场',
        'WUX' => '硕放机场',
        'WUS' => '武夷山机场',
        'WUZ' => '长洲岛机场',
        'XMN' => '高崎机场',
        'XFN' => '刘集机场',
        'XEN' => '兴城机场',
        'XIY' => '咸阳机场',
        'XIC' => '青山机场',
        'XIL' => '锡林浩特机场',
        'XNT' => '褡裢机场',
        'XNN' => '曹家堡机场',
        'NLT' => '那拉提机场',
        'JHG' => '嘎洒机场',
        'XUZ' => '观音机场',
        'ACX' => '兴义万峰林机场',
        'DIG' => '香格里拉机场',
        'ENY' => '二十里铺机场',
        'YNZ' => '南洋机场',
        'YNJ' => '朝阳川机场',
        'YIE' => '伊尔施机场',
        'YIC' => '明月山机场',
        'YNT' => '莱山机场',
        'YBP' => '宜宾机场',
        'YIH' => '三峡机场',
        'INC' => '河东机场',
        'YIN' => '伊宁机场',
        'YZY' => '张掖机场',
        'YIW' => '义乌机场',
        'LLF' => '零陵机场',
        'UYN' => '西沙机场',
        'YCU' => '关公机场',
        'LDS' => '林都机场',
        'YUS' => '巴塘机场',
        'YTY' => '泰州机场',
        'DYG' => '荷花机场',
        'ZHA' => '湛江机场',
        'ZAT' => '昭通机场',
        'CGO' => '新郑机场',
        'HSN' => '普陀山朱家尖机场',
        'ZUH' => '三灶机场',
        'ZHY' => '沙坡头机场',
        'ZYI' => '遵义机场',
        'ZQZ' => '宁远机场',
    );
    return $codearr[$code];
}


//三字码
function codeToAirport($code)
{
    $codearr = array(
        'AKU' => '阿克苏',
        'AAT' => '阿勒泰',
        'AKA' => '安康',
        'AVA' => '安顺',
        'AQG' => '安庆',
        'AYN' => '安阳',
        'AOG' => '鞍山',
        'AEB' => '百色',
        'BSD' => '保山',
        'BFJ' => '毕节',
        'BFU' => '蚌埠',
        'BAV' => '包头',
        'BHY' => '北海',
        'PEK' => '北京',
        'NAY' => '北京',
        'BPL' => '博乐',
        'CGQ' => '长春',
        'CGD' => '常德',
        'CNI' => '长海',
        'BPX' => '昌都',
        'CSX' => '长沙',
        'CIH' => '长治',
        'CZX' => '常州',
        'CHW' => '酒泉',
        'CHG' => '朝阳',
        'CTU' => '成都',
        'CIF' => '赤峰',
        'CKG' => '重庆',
        'NBS' => '长白山',
        'DLC' => '大连',
        'DAX' => '达州',
        'DLU' => '大理',
        'DDG' => '丹东',
        'DAT' => '大同',
        'DAX' => '达县',
        'DCY' => '稻城',
        'DIG' => '迪庆',
        'DOY' => '东营',
        'DNH' => '敦煌',
        'DQA' => '大庆',
        'LUM' => '德宏',
        'DSN' => '鄂尔多斯',
        'ENH' => '恩施',
        'ENY' => '延安',
        'ERL' => '二连浩特',
        'FUO' => '佛山',
        'FOC' => '福州',
        'FYN' => '富蕴',
        'FUG' => '阜阳',
        'KOW' => '赣州',
        'GOQ' => '格尔木',
        'GHN' => '广汉',
        'GXH' => '甘南',
        'GYS' => '广元',
        'CAN' => '广州',
        'KWL' => '桂林',
        'KWE' => '贵阳',
        'GYU' => '固原',
        'HAK' => '海口',
        'HLD' => '海拉尔',
        'HMI' => '哈密',
        'HDG' => '邯郸',
        'HPG' => '神农架',
        'HSC' => '韶关',
        'HUZ' => '惠州',
        'HGH' => '杭州',
        'HZG' => '汉中',
        'HRB' => '哈尔滨',
        'HFE' => '合肥',
        'HEK' => '黑河',
        'HNY' => '衡阳',
        'HUN' => '花莲',
        'HTN' => '和田',
        'HET' => '呼和浩特',
        'TXN' => '黄山',
        'HYN' => '黄岩',
        'HJJ' => '怀化',
        'HIA' => '淮安',
        'JIL' => '吉林',
        'JZH' => '黄龙',
        'JMU' => '佳木斯',
        'JGN' => '嘉峪关',
        'TNA' => '济南',
        'JDZ' => '景德镇',
        'JGS' => '井冈山',
        'JNG' => '济宁',
        'JNZ' => '锦州',
        'JIU' => '九江',
        'JIC' => '金昌',
        'JUH' => '池州',
        'JZH' => '九寨沟',
        'JXA' => '鸡西',
        'JJN' => '晋江',
        'JHG' => '景洪',
        'KRY' => '克拉玛依',
        'KHG' => '喀什',
        'KRL' => '库尔勒',
        'KCA' => '库车',
        'KJI' => '布尔津',
        'KMG' => '昆明',
        'KHH' => '高雄',
        'KGT' => '康定',
        'LHW' => '兰州',
        'LXA' => '拉萨',
        'LYG' => '连云港',
        'LJG' => '丽江',
        'LCX' => '连城',
        'LNJ' => '临沧',
        'LYI' => '临沂',
        'LZY' => '林芝',
        'HZH' => '黎平',
        'LZH' => '柳州',
        'LYA' => '洛阳',
        'LZO' => '泸州',
        'LLB' => '荔波',
        'LCX' => '龙岩',
        'LUM' => '芒市',
        'NZH' => '满洲里',
        'MZG' => '马公',
        'MXZ' => '梅州',
        'MIG' => '绵阳',
        'MDG' => '牡丹江',
        'OHE' => '漠河',
        'KHN' => '南昌',
        'KNH' => '金门',
        'NAO' => '南充',
        'NKG' => '南京',
        'NLH' => '宁蒗',
        'NGQ' => '阿里',
        'NNG' => '南宁',
        'NTG' => '南通',
        'NNY' => '南阳',
        'NGB' => '宁波',
        'NLT' => '那拉提',
        'PZI' => '攀枝花',
        'IQM' => '且末',
        'TAO' => '青岛',
        'IQN' => '庆阳',
        'SHP' => '秦皇岛',
        'NDG' => '齐齐哈尔',
        'JJN' => '泉州',
        'JUZ' => '衢州',
        'JIQ' => '黔江',
        'RMQ' => '台中',
        'RLK' => '巴彦淖尔',
        'RKZ' => '日喀则',
        'SYX' => '三亚',
        'SHA' => '上海',
        'PVG' => '上海',
        'SWA' => '汕头',
        'SHS' => '荆州',
        'SHE' => '沈阳',
        'SYM' => '普洱',
        'SZX' => '深圳',
        'SJW' => '石家庄',
        'SYM' => '思茅',
        'SHP' => '山海关',
        'TCG' => '塔城',
        'TNH' => '通化',
        'TVS' => '唐山',
        'TYN' => '太原',
        'TSN' => '天津',
        'TGO' => '通辽',
        'TEN' => '铜仁',
        'TLQ' => '吐鲁番',
        'TCZ' => '腾冲',
        'TTT' => '台东',
        'TNN' => '台南',
        'THQ' => '天水',
        'HYN' => '台州',
        'HLH' => '乌兰浩特',
        'URC' => '乌鲁木齐',
        'WXN' => '万州',
        'WEF' => '潍坊',
        'WEH' => '威海',
        'WNH' => '文山',
        'WNZ' => '温州',
        'WUA' => '乌海',
        'WUH' => '武汉',
        'WUX' => '无锡',
        'WUS' => '武夷山',
        'WUZ' => '梧州',
        'WOT' => '望安',
        'XMN' => '厦门',
        'XFN' => '襄阳',
        'NLT' => '新源',
        'XIY' => '西安',
        'XIC' => '西昌',
        'XNT' => '邢台',
        'XIL' => '锡林浩特',
        'XNN' => '西宁',
        'JHG' => '西双版纳',
        'XUZ' => '徐州',
        'ACX' => '兴义',
        'DIG' => '香格里拉',
        'ENY' => '延安',
        'YNZ' => '盐城',
        'YNJ' => '延吉',
        'YNT' => '烟台',
        'YBP' => '宜宾',
        'YIH' => '宜昌',
        'YIE' => '阿尔山',
        'INC' => '银川',
        'YIN' => '伊宁',
        'YIW' => '义乌',
        'LLF' => '永州',
        'UYN' => '榆林',
        'YCU' => '运城',
        'LDS' => '伊春',
        'YUS' => '玉树',
        'YTY' => '扬州',
        'YZY' => '张掖',
        'YIC' => '宜春',
        'DYG' => '张家界',
        'ZHA' => '湛江',
        'ZAT' => '昭通',
        'CGO' => '郑州',
        'HSN' => '舟山',
        'ZUH' => '珠海',
        'ZHY' => '中卫',
        'HJJ' => '芷江',
        'ZYI' => '遵义',
        'ZQZ' => '张家口',
    );
    return $codearr[$code];
}


//二字码
function nameToAirLine($code)
{
    $codearr = array(
        'CA' => '中国国际航空公司',
        'CZ' => '中国南方航空公司',
        'MU' => '中国东方航空公司',
        'MF' => '中国厦门航空公司',
        '3U' => '中国四川航空公司',
        'CN' => '大新华航空有限公司',
        'G5' => '华夏航空有限公司',
        'HO' => '上海吉祥航空有限公司',
        'HU' => '海南航空公司',
        'SC' => '中国山东航空公司',
        'ZH' => '中国深圳航空公司',
        'FM' => '上海航空公司',
        'BK' => '奥凯航空有限公司',
        'EU' => '鹰联航空有限公司',
        'GS' => '天津航空有限公司',
        'KN' => '中国联合航空有限公司',
        'JD' => '首都航空',
        'IV' => '中国福建航空公司',
        'CJ' => '中国北方航空公司',
        '8C' => '东星航空有限公司',
        '8L' => '云南祥鹏航空有限责任公司',
        'OQ' => '重庆航空有限责任公司',
        'VD' => '鲲鹏航空有限公司',
        'JR' => '中国幸福航空',
        'NS' => '东北航空有限公司',
        'PN' => '西部航空有限责任公司',
        '9C' => '春秋航空公司',
        'HR' => '中国联合航空公司',
        'IJ' => '长城航空有限公司',
        'F4' => '上海国际货运航空有限公司',
        'JI' => '翡翠国际货运航空有限责任公司',
        'J5' => '东海航空有限公司',
        'PO' => '中国邮政航空有限公司',
        'LD' => '香港华民航空',
        'PN' => '西部航空有限责任公司',
        'HC' => '中国海洋直升机公司',
        'HY' => '中国航空货运公司',
        'Y8' => '扬子江快运航空有限公司',
        'NX' => '澳门航空公司',
        'ZG' => '非凡航空',
        'CX' => '国泰航空有限公司',
        'HX' => '香港航空公司',
        'O8' => '甘泉香港航空公司',
        'UO' => '香港快运航空',
        'CI' => '中华航空股份有限公司',
        'BR' => '长荣航空股份有限公司',
        'GE' => '复兴航空运输股份有限公司',
        'AE' => '华信航空股份有限公司',
        'B7' => '立荣航空公司EU鹰联航空',
        'EF' => '远东航空股份有限公司',
    );
    return $codearr[$code];
}


//时间截取 例20151101
function subStrTime($time)
{
    return date("Ymd", strtotime($time));
}

//年月日时分秒
function subStrDate($time)
{
    return date("YmdHis", strtotime($time));
}

//
function returnTime($time)
{
    return substr($time, 0, 2) . ':' . substr($time, 2, 2);
}

//121.000000  -> 121.00
function getMoney($price)
{
    return substr($price, 0, -4);
}

/**
 * 字符串截取
 */
function msubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true)
{
    if (function_exists("mb_substr"))
        $slice = mb_substr($str, $start, $length, $charset);
    elseif (function_exists('iconv_substr')) {
        $slice = iconv_substr($str, $start, $length, $charset);
        if (false === $slice) {
            $slice = '';
        }
    } else {
        $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
        $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
        $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
        $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
        preg_match_all($re[$charset], $str, $match);
        $slice = join("", array_slice($match[0], $start, $length));
    }
    return $suffix ? $slice . '...' : $slice;
}
