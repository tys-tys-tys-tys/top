<?php

namespace Common\Common;

use Think\Controller;
use Common\Top;
use Common\Utils;

class CommonController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        $name = $_SERVER['SERVER_NAME'];
        $operatorInfo = M('operator')->where("domain = '{$name}'")->find();

        if (empty($operatorInfo)) {
            $this->show('域名未绑定');
            exit;
        }

        $this->logo = $operatorInfo['logo'];
        $this->copyRight = $operatorInfo['copyright'];
        $this->cdkey = $operatorInfo['sms_ym_cdkey'];
        $this->pwd = $operatorInfo['sms_ym_pwd'];
        $this->signature = "【" . $operatorInfo['sms_ym_signature'] . "】";
        $this->smsOperatorId = $operatorInfo['id'];

        $this->code = 400;

        $this->dateStart = '00:00:00';
        $this->dateEnd = '24:59:59';
    }

    public function _empty()
    {
        echo '404页面';
    }
//公共列表控制
    public function lists()
    {
        $roleId = session('role_id');
        $state = Top::ApplyStateWait;

        if($roleId == 1 || $roleId == 186){
            //取出所有除了测试账号以外的运营商ID
            $operatorList = M('operator')->where('id != 2')->field('id')->select();
            //用array_column函数返回数组中指定的一列
            $operatorIdsData = array_column($operatorList, 'id');
            $operatorIds = implode(',', $operatorIdsData);
            $where['operator_id'] = array('in', $operatorIds);
            $this->operatorTopUpTotal = M('operator_credit_topup')->where("state = 2")->field('sum(amount) as total')->select();
        }else{
            $operatorId = session('operator_id');
            $where['operator_id'] = $operatorId;
        }

        $entityType = Top::EntityTypeAgency;
        $providerType = Top::EntityTypeProvider;
        $productPassState = Top::ProductStateIsSale;
        $productSubmittedState = Top::ProductStateSubmitted;
        $applyStatePass = Top::ApplyStateFinish;
        $userId = session('user_id');
        //查询运营商下代理商数量
        $agencyNum = M('agency')->where("istest = 0 && state != 4")->where($where)->field('id')->count();
        //查询待审核的充值申请数量
        $topupApplyStateWaitNum = M('account_topup')->where("state = '{$state}' && entity_type = '{$entityType}' && istest = 0")->where($where)->field('id')->count();
        $withdrawApplyStateWaitNum = M('account_withdraw')->where("state = '{$state}' && entity_type = '{$entityType}' && istest = 0")->where($where)->field('id,operator_id,state,entity_type')->count();
        $invoiceApplyStateWaitNum = M('agency_invoice')->where("state = '{$state}'")->where($where)->field('id,operator_id,state,entity_type')->count();
        $contractApplyStateWaitNum = M('agency_contract')->where("state = '{$state}'")->where($where)->field('id,operator_id,state,entity_type')->count();
        $topupTotal = M('account_topup')->where("istest = 0 && state = 2")->where($where)->field('id,operator_id,state,entity_type,sum(amount) amount')->select();


        $topupTotal = Utils::getYuan($topupTotal[0]['amount']);
        //获取代理商提现总金额
        $withdrawTotalList = M('account_withdraw')->where("entity_type = '{$entityType}' && istest = 0 && state = '{$applyStatePass}'")->where($where)->field('id,sum(amount) amount')->select();
        $withdrawTotal = Utils::getYuan($withdrawTotalList[0]['amount']);
       //获取代理被扣服务费总额
        $serviceChargeTotalList = M('service_charge')->where("entity_type= '{$entityType}' && state = 1")->where($where)->field('id,sum(service_money) service_money')->select();
        $serviceChargeTotal = Utils::getYuan($serviceChargeTotalList[0]['service_money']);
        
        $providerNum = M('provider')->where("state != 4")->where($where)->field('id')->count();
        $productPassStateNum = M('tour')->where("state = '{$productPassState}'")->where($where)->field('id,operator_id,state')->count();
        $productNotSubmittedNum = M('tour')->where("state = '{$productSubmittedState}'")->where($where)->field('id,operator_id,state')->count();
        $providerWithdrawStateWaitNum = M('account_withdraw')->where("entity_type = '{$providerType}' && state = '{$state}' && istest = 0")->where($where)->field('id,operator_id,entity_type,state,istest')->count();
        $providerWithdrawTotal = M('account_withdraw')->where("entity_type = '{$providerType}' && state = '{$applyStatePass}' && istest = 0")->where($where)->field("id,operator_id,entity_type,state,istest,sum(amount) amount")->select();
        $providerWithdrawTotal = Utils::getYuan($providerWithdrawTotal[0]['amount']);
        $nowDate = date('Y-m-d 00:00:00', time());
        $nowDate = date('Y-m-d 00:00:00', strtotime('-2 day', strtotime($nowDate)));
        $productTourNum = M('tour')->where("state = '{$productPassState}' && latest_sku >= '{$nowDate}'")->where($where)->field('id,latest_sku')->count();

        $userInfo = M('user')->where("id = '{$userId}'")->find();


        $this->assign('withdrawTotal', $withdrawTotal);
        $this->assign('serviceChargeTotal', $serviceChargeTotal);
        $this->assign('topupTotal', $topupTotal);
        $this->assign('contractApplyStateWaitNum', $contractApplyStateWaitNum);
        $this->assign('invoiceApplyStateWaitNum', $invoiceApplyStateWaitNum);
        $this->assign('withdrawApplyStateWaitNum', $withdrawApplyStateWaitNum);
        $this->assign('topupApplyStateWaitNum', $topupApplyStateWaitNum);
        $this->assign('agencyNum', $agencyNum);

        $this->assign('productTourNum', $productTourNum);
        $this->assign('providerNum', $providerNum);
        $this->assign('productPassStateNum', $productPassStateNum);
        $this->assign('productNotSubmittedNum', $productNotSubmittedNum);
        $this->assign('providerWithdrawStateWaitNum', $providerWithdrawStateWaitNum);
        $this->assign('providerWithdrawTotal', $providerWithdrawTotal);

        $this->assign('userInfo', $userInfo);
        $this->display();
    }

    /**
     * WeChat send message
     * 微信发送消息
     */
    public function send_tpl($template)
    {
        vendor('WxPayPubHelper.WxPayPubHelper');
        $jsApi = new \JsApi_pub();
        $accessToken = $jsApi->getAccessToken();
        $json_template = json_encode($template);
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $accessToken;
        Utils::vpost($url, urldecode($json_template));
    }

    /**
     * Object To Array
     * 对象数组
     */
    function object_to_array($obj)
    {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? $this->object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }


}
