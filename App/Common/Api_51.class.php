<?php
namespace Common;

class Api_51
{

    public static function soapRequest($url, $data)
    {
        $data = array('request' => $data);
        $options = array(
            'trace' => true,
            'encoding' => 'UTF-8',
            'connection_timeout' => 120,
            'soap_version' => SOAP_1_1,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'keep_alive' => true
        );
        try {
            $soapclient = new \Soapclient($url, $options);
        } catch (Exception $e) {
            $this->logs['soap']['client'][] = $e;
            return false;
        }
        $funarr = $soapclient->__getFunctions();
        $fun = substr($funarr[0], strpos($funarr[0], ' ') + 1, strpos($funarr[0], '(') - strpos($funarr[0], ' ') - 1);
        try {
            $back = $soapclient->$fun($data);
        } catch (Exception $e) {

            $this->logs['soap'][$fun][] = $e;

            $s = $soapclient->__getLastRequest();
            print_r($s);
            exit;
            return false;
        }
        return $back;
    }

    /**
     * 描术多张机票的请求，及soap请求方式
     */
    public static function orderCreate($orderData)
    {
        $api = new Api_51();
        $url = "http://ws.tongyedns.com:8000/ltips/services/createOrderByPassengerService1.0?wsdl";
        $result = $api->soapRequest($url, $orderData);
        return $result;
    }


    /**
     *根据航班列表出发日期实时匹配政策
     */
    public static function getPassengerPolicyAndFlightService($flightData)
    {
        $api = new Api_51();
        $url = "http://ws.51book.com:8000/ltips/services/getChildPolicyAndFlightService1.0?wsdl";
        $result = $api->soapRequest($url, $flightData);
        return $result;
    }

}

?>