<?php

namespace Common;

class Top {
//通用状态
const StateEnabled                      = 1;//启用
const StateDisabled                     = 2;//禁用
const StateDeleted                      = 4;//删除

//实体类型
const EntityTypeSystem                  = 1;//系统
const EntityTypeOperator                = 2;//运营商
const EntityTypeProvider                = 3;//供应商
const EntityTypeAgency                  = 4;//代理商

//(发票、充值、合同)申请状态
const ApplyStateWait                    = 1;//待审核
const ApplyStatePass		            = 2;//审核通过，待财务处理
const ApplyStateReject                  = 3;//审核失败
const ApplyStateFinish                  = 4;//财务操作完成
const ApplyStateStayPay                = 5;//微信待支付
const ApplyStateHasPay                = 6;//微信已支付

//产品状态
const ProductStateNotSubmitted          = 1;//待提交
const ProductStateIsSale                = 2;//审核通过，已上架
const ProductStateSubmitted             = 3;//已提交，待审核
const ProductStateRejected              = 4;//审核失败
const ProductStateCanceled              = 5;//供应商取消，已下架

//通用产品状态
const GeneralProductStateIsSale         = 0;//已上架
const GeneralProductStateCanceled       = 1;//下架

//订单状态
const OrderStateNotSubmitted            = 1;//未提交
const OrderStateSubmitted               = 2;//已提交
const OrderStateWaitForPayment          = 3;//已确认待付款
const OrderStateCanceled                = 4;//已取消
const OrderStateClosed                  = 5;//已关闭
const OrderStatePrepayed                = 6;//已付预付款
const OrderStateAllPayed                = 7;//已付全款
const OrderStateRefunded                = 8;//已退款
const OrderStateCanceling               = 9;//取消中
const OrderStateWaitingForRefund        = 10;//取消中,待退款(供应商确认)
const OrderStateCancelRefund            = 11;//拒绝取消订单
const OrderStateAgencyAuditRefund       = 12;//取消中,待退款(代理商管理确认)
const OrderStateFinanceAuditRefund      = 13;//已取消,已退款(运营商财务管理确认)

//机票订单状态
const FlightOrderStateSubmitted         = 1;//已提交
const FlightOrderStateWaitingForPayment = 14;//已预定,待付款
const FlightOrderStatePaying            = 15;//已预定,付款中
const FlightOrderStatePayed             = 2;//已付款,待出票
const FlightOrderStateTicked            = 3;//已付款,已出票
const FlightOrderStateCanceled          = 4;//已取消
const FlightOrderStateRefunded          = 5;//已退票,退票成功
const FlightOrderStateTicketFailed      = 6;//出票失败
const FlightOrderStateScheduledFailed   = 7;//预定失败
const FlightOrderStateRefundFailed      = 8;//退票失败
const FlightOrderStateRefundWaited      = 9;//退票中,待退款
const FlightOrderStateHasCanceled       = 10;//有取消
const FlightOrderStateHasRefunded       = 11;//有退票
const FlightOrderStateDeleted           = 12;//代理商已删除

//门票订单状态
const TicketOrderStateSubmitted         = 1;//已提交
const TicketOrderStatePayed             = 2;//已付款,已出票
const TicketOrderStateCanceled          = 4;//已取消
const TicketOrderStateRefunded          = 5;//已退票,退票成功
const TicketOrderStateRefundWaited      = 9;//退票中,待退款


const paymentStateAlreadyPayed          = 1;//已付款
const paymentStateNotPayed              = 0;//未支付

//object_type
const ObjectTypeTopUp                   = 1;//充值
const ObjectTypeWithDraw                = 2;//提现
const ObjectTypeInvoice                 = 3;//发票
const ObjectTypeContact                 = 4;//合同
const ObjectTypeTour                    = 5;//线路
const ObjectTypeProduct                 = 6;//通用产品
const ObjectTypeFlight                  = 7;//机票
const ObjectTypeRefund                  = 8;//账户调整
const ObjectTypeTicket                  = 9;//门票
const ObjectTypeCredit                  = 10;//授信
const ObjectTypeService                 = 11;//扣除顾问服务费


//默认角色
const DefalutOperatorRoleId             =2;
const DefalutProviderRoleId             =3;

//代金券状态
const CouponStateNotUsed                = 0; //未使用
const CouponStateUsed                   = 1; //已使用

//政策状态
const PolicyStateEnabled                = 0;//正常
const PolicyStateDisabled               = 1;//禁用

const topRecommended                    = 1;//已推荐置顶
const cancelTopRecommended              = 0;//撤销推荐置顶

const IsPromotion                       = 1;//是特价产品
const NotPromotion                      = 0;//不是特价产品

const IsTested                          = 1;//是测试账号
const NotTested                         = 0;//不是测试账号


const IsOutOrder                        = 1;//是外部订单
const NotOutOrder                       = 0;//不是外部订单
}