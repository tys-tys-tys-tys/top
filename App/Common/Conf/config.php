<?php
return array(
//'配置项'=>'配置值'
    'DB_TYPE' => 'mysql',
    'DB_HOST' => '127.0.0.1',
    'DB_NAME' => 'top',
    'DB_USER' => 'root',
    'DB_PWD' => 'root',
    'DB_PORT' => '3306',
    'DB_PREFIX' => 'top_',
    //'DB_PREFIX' => '',
    'DB_CHARSET' => 'utf8',
    'SESSION_EXPIRE' => '60', // 默认Session有效期默认是1小时
    //'URL_MODEL' => 2, //url重写模式
    'URL_CASE_INSENSITIVE' => true, //URL不区分大小写
    'TMPL_TEMPLATE_SUFFIX'  =>  '.html',
    'DEFAULT_FILTER'        => 'strip_tags,htmlspecialchars',//I方法默认过滤
	'SHOW_OPERATOR_DATA' => '1,185,186',
    'WX_PAY_URI'=>'http://yangxiaomao.m.dalvu.com/',
    'PAGE_SET'=>'20',
	'REPORT_PAGE_SET' => '100',
	'PRODUCT_PAGE_SET' => '10',
	'TRANSACTION_PAGE_SET' => '50',
	'SERVICE_FEE' => '2',
	'FILE1_BASE_URL' => 'http://file1.lydlr.com',
    'MOFILE_BASE_URL' => 'http://file.m.lydlr.com/agency',
	'PRODUCT_FILE_BASE_URL' => 'http://file.m.lydlr.com/product',
	'SEND_SMS' => 'true',
    'PRODUCT_UPLOAD_PATH' => 'D:/Web/Jet.FileDownload/product',
    'AGENCY_UPLOAD_PATH' => 'D:/Web/Jet.FileDownload/agency',
	'VISITOR_WXPAY_NOTIFY_PATH' => 'D:/logs/wxpay-notify',
        /* 'URL_HTML_SUFFIX' => '', //设置伪静态后缀名
          'TMPL_PARSE_STRING' => array(
          '__UPLOAD__' => '/Uploads', //上传路径替换规则
          ), */
);
?>