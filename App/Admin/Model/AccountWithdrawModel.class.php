<?php

namespace Admin\Model;

use Think\Model;
use Common\Top;
use Common\Utils;

class AccountWithdrawModel extends model
{

    public function showList()
    {
        $entityType = Top::EntityTypeProvider;
        $operatorId = session('operator_id');

        if (session('entity_type') != Top::EntityTypeSystem && session('role_id') != 1 && session('user_id') != 1) {
            $where = "operator_id= " . session('operator_id') . ' and entity_type=' . Top::EntityTypeProvider;
        } else {
            $where = "entity_type = '{$entityType}' && operator_id = '{$operatorId}'";
        }
        $order = 'create_time desc ';
        $pageInfo = Utils::pages('account_withdraw', $where, C('PAGE_SET'), $order);
        return $withdraw = array(
            'info' => $pageInfo['info'],
            'page' => $pageInfo['page'],
        );
    }

    public function transaction($id)
    {
        $account_transaction['0'] = M('account_withdraw')->field('operator_id,account,amount,entity_type,entity_id,operator_id')->find($id);
        $account_transaction['1'] = M('provider')->field('account_balance')->where('state=' . Top::StateEnabled . ' and account_state=' . Top::StateEnabled . ' and id=' . $account_transaction['0']['entity_id'])->find();
        $providerId = $account_transaction['0']['entity_id'];
        $operatorId = $account_transaction['0']['operator_id'];
        $account_transaction['2'] = M('provider_account')->field('account_balance')->where("provider_id = '{$providerId}' && operator_id = '{$operatorId}'")->find();
        return $account_transaction;
    }

}
