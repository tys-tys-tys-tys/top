<?php

namespace Admin\Model;

use Think\Model;
use Common\Utils;

class OrderProductModel extends Model
{

    /**
     * 当前供应商对应的订单信息
     */
    public function orderTourInfo()
    {
        $providerId['id'] = session('entity_id');
        $where = "agency_operator_id = " . session('operator_id') . ' and state > 0 ';
        $order = "create_time desc ";
        $pageInfo = Utils::pages('order_product', $where, C('PAGE_SET'), $order);

        foreach ($pageInfo['info'] as $key => $vo) {
            $arr[$key] = array(
                'id' => $vo['id'],
                'tour_name' => $vo['product_name'],
                'contact_person' => $vo['contact_person'],
                'quantity' => $vo['quantity'],
                'price_hotel' => $vo['price_hotel'],
                'price_adjust' => $vo['price_adjust'],
                'price_total' => $vo['price_total'],
                'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                'prepay_amount' => $vo['prepay_amount'],
                'state' => $vo['state'],
                'create_time' => $vo['create_time'],
                'agency_name' => $vo['agency_name'],
                'memo' => $vo['memo'],
            );
        }
        return $info = array(
            'info' => $arr,
            'page' => $pageInfo['page'],
        );
    }

    /**
     * 请求返回成功或者失败信息
     */
    public function orderOne($id, $state)
    {
        return M('order_product')->where('id=' . $id)->setField('state', $state);
    }

    /**
     * 请求返回成功或者失败信息
     */
    public function orderEdit($id, $state, $adjust, $editadjust, $amount, $memo)
    {
        if ($adjust === '' || $adjust === 0) {
            $editamount = $adjust;
        } else {
            $editamount = $amount;
        }
        $data = array(
            'state' => $state,
            'price_adjust' => $editadjust, //调整价格
            'prepay_amount' => $editamount, //预付
            'memo' => $memo,
        );
        return M('order_product')->where('id=' . $id)->setField($data);
    }

    /**
     * 详情
     */
    public function orderDetails($id)
    {
        $info = $this->find($id);
        $info['tour'] = M('tour')->where('id=' . $info['tour_id'])->find();
        $info['price_total_money'] = $info['price_total'] + $info['price_adjust'];
        return $info;
    }

}