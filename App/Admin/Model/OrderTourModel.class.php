<?php

namespace Admin\Model;

use Think\Model;
use Common\Utils;

class OrderTourModel extends Model
{

    /**
     * 当前供应商对应的订单信息
     */
    public function orderTourInfo()
    {
        $providerId['id'] = session('entity_id');
        $providerCommissionRate = M('provider')->where($providerId)->field('id,commission_rate')->find();
        $dbRate = intval($providerCommissionRate['commission_rate']);
        $where = "provider_id=" . session('entity_id') . ' and state>1 ';
        $order = "create_time desc ";
        $pageInfo = Utils::pages('order_tour', $where, C('PAGE_SET'), $order);
        foreach ($pageInfo['info'] as $key => $vo) {
            $arr[$key] = array(
                'id' => $vo['id'],
                'tour_name' => $vo['tour_name'],
                'contact_person' => $vo['contact_person'],
                'contact_phone' => $vo['contact_phone'],
                'addr' => $vo['addr'],
                'start_time' => $vo['start_time'],
                'pickup_location' => $vo['pickup_location'],
                'client_adult_count' => $vo['client_adult_count'],
                'client_child_count' => $vo['client_child_count'],
                'hotel_count' => $vo['hotel_count'],
                'price_hotel' => $vo['price_hotel'],
                'price_adjust' => $vo['price_adjust'],
                'price_total' => $vo['price_total'],
                'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                'prepay_amount' => $vo['prepay_amount'],
                'prepay_amount_commission' => $vo['prepay_amount'] - Utils::CalcCommission($dbRate, $vo['prepay_amount']),
                'price_payable_commission' => $vo['price_total'] + $vo['price_adjust'] - Utils::CalcCommission($dbRate, $vo['price_total'] + $vo['price_adjust']),
                'state' => $vo['state'],
                'create_time' => $vo['create_time'],
                'agency_name' => $vo['agency_name'],
                'memo' => $vo['memo'],
            );
        }
        return $info = array(
            'info' => $arr,
            'page' => $pageInfo['page'],
        );
    }

    /**
     * 请求返回成功或者失败信息
     */
    public function orderOne($id, $state)
    {
        return M('order_tour')->where('id=' . $id)->setField('state', $state);
    }

    /**
     * 请求返回成功或者失败信息
     */
    public function orderEdit($id, $state, $adjust, $editadjust, $amount, $memo)
    {
        if ($adjust === '' || $adjust === 0) {
            $editamount = $adjust;
        } else {
            $editamount = $amount;
        }
        $data = array(
            'state' => $state,
            'price_adjust' => $editadjust, //调整价格
            'prepay_amount' => $editamount, //预付
            'memo' => $memo,
        );
        return M('order_tour')->where('id=' . $id)->setField($data);
    }

    public function orderDetails($id)
    {
        $info = $this->find($id);
        $info['tour'] = M('tour')->where('id=' . $info['tour_id'])->find();
        $info['price_total_money'] = $info['price_total'] + $info['price_adjust'];
        return $info;
    }

}