<?php

namespace Admin\Model;

use Think\Model;
use Common\Top;
use Common\Utils;

class TourModel extends Model
{

    /**
     * 供应商添加新的产品--基本信息
     */
    public function addTour()
    {

        $operator = M('provider')->field('operator_id')->find(session('provider_id'));
        $_POST['provider_name'] = I('p_name', '', 'trim');
        $_POST['operator_id'] = $operator['operator_id'];
        $_POST['provider_id'] = session('provider_id');
        $_POST['create_time'] = date('Y-m-d H:i:s', time());
        $_POST['departure'] = I('departure') . ' ' . I('departure1');
        $_POST['destinations'] = I('destinations');
        $_POST['state'] = Top::ProductStateNotSubmitted;
        $_POST['user_id'] = session('user_id');
        $_POST['user_name'] = session('name');
        $_POST['provider_type'] = Top::NotOutOrder;
        $phone = "/^((\+?86)|(\(\+86\)))?\d{3,4}-\d{7,8}(-\d{3,4})?$|^((\+?86)|(\(\+86\)))?1\d{10}$/";

        if (!I('post.name') || strlen(I('post.name')) > 120 || !I('post.departure') || !I('post.departure1') || !I('post.destinations') || !I('post.contact_person') || !I('post.contact_phone') || !preg_match($phone, I('post.contact_phone'))) {

            return false;
        } else {
            if (I('tid') != 0) {
                $_POST['update_time'] = date('Y-m-d H:i:s', time());
                return $this->where('id=' . I('tid'))->save($this->create());
            } else {
                return $this->add($this->create());
            }
        }
    }

    /**
     * 供应商编辑新的产品--基本信息
     */
    public function editTour($arr, $id)
    {
        $data = array(
            'name' => $arr['name'],
            'user_id' => session('user_id'),
            'user_name' => session('name'),
            'line_type' => $arr['line_type'],
            'departure' => $arr['departure'] . " " . $arr['departure1'],
            'destination' => $arr['destination'],
            'destinations' => $arr['destinations'],
            'start_time' => $arr['start_time'],
            'traffic_go' => $arr['traffic_go'],
            'traffic_back' => $arr['traffic_back'],
            'contact_person' => $arr['contact_person'],
            'contact_phone' => $arr['contact_phone'],
            'travel_type' => $arr['travel_type'],
            'cover_pic' => $arr['cover_pic'],
            'pics' => $arr['pics'],
            'update_time' => date('Y-m-d H:i:s', time()),
            'promotion' => $arr['promotion'],
        );

        if (session('entity_type') == '3') {
            $data['state'] = 1;
        } else {
            $data['state'] = 2;
        }

        if (!$data['name'] && !$data['departure'] && !$data['departure1'] && !$data['destination'] && !$data['destination1'] && !$data['contact_person'] && !$data['contact_phone']) {
            return false;
        } else {
            //图片上传
            return $this->where('id=' . $id)->save($data);
        }
    }

    /**
     * 供应商添加新的产品--行程
     */
    public function addPlan()
    {
        return $this->add(M('tour_plan')->create());
    }

    /**
     * 供应商添加新的产品--详细信息
     */
    public function addSku($tour_id, $arr)
    {

        $dateTime = explode(',', $arr['times']);
        foreach ($dateTime as $v) {
            $data = array(
                'tour_id' => $tour_id,
                'start_time' => $v,
                'price_adult_original' => Utils::getFen($arr['aoriginal']),
                'price_adult_agency' => Utils::getFen($arr['aagency']),
                'price_adult_list' => Utils::getFen($arr['alist']),
                'price_child_original' => Utils::getFen($arr['coriginal']),
                'price_child_agency' => Utils::getFen($arr['cagency']),
                'price_child_list' => Utils::getFen($arr['clist']),
                'price_hotel_agency' => Utils::getFen($arr['hagency']),
                'price_hotel_list' => Utils::getFen($arr['hlist']),
                'memo' => $arr['desc'],
                'sku' => $arr['num'],
                'fake_sold' => $arr['sold'],
                'pre_join_days' => $arr['days'],
                'create_time' => date('Y-m-d H:i:s', time()),
                'state' => 1,
            );
            $skuInfo = M('tour_sku')->add($data);
        }
        $min_price = M('tour_sku')->where('tour_id=' . $tour_id . ' and start_time>' . date('Y-m-d', time()))->min('price_adult_list');
        $this->where('id=' . $tour_id)->setField('min_price', $min_price);
        return $skuInfo;
    }

    /**
     * 供应商添加新的产品--位控/团期
     */
    public function addExtend()
    {
        return $this->save($this->create());
    }

    /**
     * 获取当前供应商的名称
     */
    public function userInfo()
    {

        $userInfo = M('provider')->where('id=' . session('entity_id'))->field('name,contact_person,mobile,destinations')->find();
        if (I('tid', 0, 'int') > 0) {
            $info = $this->where('id=' . I('tid', 0, 'int'))->find();
            $userInfo['addInfo'] = array(
                'id' => $info['id'],
                'name' => $info['name'],
                'operator_id' => $info['operator_id'],
                'provider_id' => $info['provider_id'],
                'line_type' => $info['line_type'],
                'departure' => explode(' ', $info['departure']),
                'destination' => $info['destination'],
                'destinations' => $info['destinations'],
                'start_time' => $info['start_time'],
                'traffic_go' => $info['traffic_go'],
                'traffic_back' => $info['traffic_back'],
                'min_price' => $info['min_price'],
                'cover_pic' => $info['cover_pic'],
                'pics' => $info['pics'],
                'contact_person' => $info['contact_person'],
                'contact_phone' => $info['contact_phone'],
                'travel_type' => $info['travel_type'],
                'notice' => $info['notice'],
                'description' => $info['description'],
                'detail' => $info['detail'],
                'fee_include' => $info['fee_include'],
                'fee_exclude' => $info['fee_exclude'],
                'memo' => $info['memo'],
                'audit_memo' => $info['audit_memo'],
                'order_num' => $info['order_num'],
                'create_time' => $info['create_time'],
                'update_time' => $info['update_time'],
                'promotion' => $info['promotion'],
                'state' => $info['state']
            );
        } else {
            $userInfo['addInfo']['contact_phone'] = $userInfo['mobile'];
            $userInfo['addInfo']['contact_person'] = $userInfo['contact_person'];
        }
        $infoPics = explode('@@@', $info['pics']);
        foreach ($infoPics as $k => $v) {
            if (!empty($v)) {
                $infoImgAll[] = explode('|', $v);
            }
        }
        $userInfo['imgs'] = $infoImgAll;
        return $userInfo;
    }

    public function skuEdit($arr)
    {
        $eidtsku = array(
            'id' => $arr['tid'],
            'start_time' => $arr['timestartOne'],
            'price_adult_original' => Utils::getFen($arr['aoriginal']),
            'price_adult_agency' => Utils::getFen($arr['aagency']),
            'price_adult_list' => Utils::getFen($arr['alist']),
            'price_child_original' => Utils::getFen($arr['coriginal']),
            'price_child_agency' => Utils::getFen($arr['cagency']),
            'price_child_list' => Utils::getFen($arr['clist']),
            'price_hotel_agency' => Utils::getFen($arr['hagency']),
            'price_hotel_list' => Utils::getFen($arr['hlist']),
            'sku' => $arr['num'],
            'fake_sold' => $arr['sold'],
            'pre_join_days' => $arr['days'],
            'update_time' => date('Y-m-d H:i:s', time()),
            'memo' => $arr['desc'],
        );

        $sku = M('tour_sku');
        $skuEdits = $sku->save($eidtsku);
        $tour = $sku->where('id=' . $arr['tid'])->field('tour_id')->find();
        $min_price = M('tour_sku')->where('tour_id=' . $tour['tour_id'] . ' and start_time>' . date('Y-m-d', time()))->min('price_adult_list');
        $this->where('id=' . $tour['tour_id'])->setField('min_price', $min_price);
        return $skuEdits;
    }

    public function CruiseSkuEdit($arr)
    {
        $eidtsku = array(
            'id' => $arr['tid'],
            'start_time' => $arr['timestartOne'],
            'cruise' => $arr['cruise'],
            'price_adult_list' => Utils::getFen($arr['min_price']),
            'price_adult_agency' => Utils::getFen($arr['min_price']),
            'sku' => $arr['num'],
            'update_time' => date('Y-m-d H:i:s', time()),
        );

        $sku = M('tour_sku');
        $skuEdits = $sku->save($eidtsku);
        $tour = $sku->where('id=' . $arr['tid'])->field('tour_id')->find();
        $min_price = M('tour_sku')->where('id=' . $arr['tid'] . ' and start_time>' . date('Y-m-d', time()))->min('price_adult_list');
        $this->where('id=' . $tour['tour_id'])->setField('min_price', $min_price);
        return $skuEdits;
    }

    public function skuEditOneInfo($id)
    {

        $sku_info = M('tour_sku')->where('id=' . $id)->find();
        $infosku = array(
            'start' => date("Y-m-d", strtotime($sku_info['start_time'])),
            'aoriginal' => intval(Utils::getYuan($sku_info['price_adult_original'])),
            'aagency' => intval(Utils::getYuan($sku_info['price_adult_agency'])),
            'alist' => intval(Utils::getYuan($sku_info['price_adult_list'])),
            'coriginal' => intval(Utils::getYuan($sku_info['price_child_original'])),
            'cagency' => intval(Utils::getYuan($sku_info['price_child_agency'])),
            'clist' => intval(Utils::getYuan($sku_info['price_child_list'])),
            'hagency' => intval(Utils::getYuan($sku_info['price_hotel_agency'])),
            'hlist' => intval(Utils::getYuan($sku_info['price_hotel_list'])),
            'num' => $sku_info['sku'],
            'sold' => $sku_info['fake_sold'],
            'days' => $sku_info['pre_join_days'],
            'desc' => $sku_info['memo'],
            'price_adult_list' => Utils::getYuan($sku_info['price_adult_list']),
            'cruise' => $sku_info['cruise'],
        );
        return $infosku;
    }

}
