<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class CreditController extends AdminController
{


    public function applyCredit()
    {
        if (IS_POST && IS_AJAX) {
            $state = I('state');

            if ($state != 1) {
                $data['status'] = 0;
                $data['msg'] = '账户被禁用';
                $this->ajaxReturn($data);
            }

            $postCode = I('code');
            $code = session('creditCode');

            if ($code == $postCode) {
                session('creditCode', null);
                $data['operator_id'] = session('operator_id');
                $data['entity_type'] = Top::EntityTypeOperator;
                $data['entity_name'] = session('entity_name');
                $data['account'] = I('account');
                $data['entity_id'] = session('entity_id');
                $data['amount'] = Utils::getFen(I('amount'));
                $data['topup_type'] = I('topup_type');
                $data['state'] = Top::ApplyStateWait;
                $data['create_time'] = date("Y-m-d H:i:s", time());
                $data['payer'] = I('payer');
                $result = M('operator_credit_topup')->add($data);

                $template = array(
                    'touser' => 'oK9HQsxpTtSSMURoe9NJp3lnpP_0',
                    'template_id' => Utils::topUpTemplateId(),

                    'data' => array(
                        'first' => array('value' => urlencode("您好, 有运营商提交充值申请, 请登录系统查看"), 'color' => "#000"),
                        'keyword1' => array('value' => urlencode(Utils::getOperatorName(session('operator_id'))), 'color' => "#000"),
                        'keyword2' => array('value' => urlencode(I('amount')), 'color' => '#000'),
                        'keyword3' => array('value' => urlencode(date("Y-m-d H:i:s", time())), 'color' => '#000'),
                    )
                );

                if ($result) {
                    $this->send_tpl($template);

                    $data['status'] = 1;
                    $data['msg'] = '申请成功';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '申请失败';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 3;
                $data['msg'] = '请勿重复提交';
                $this->ajaxReturn($data);
            }
        } else {
            $operatorId = session('operator_id');
            $roleId = session('role_id');

            if ($roleId == 1) {
                $operatorId = 0;
            }

            $where['id'] = $operatorId;
            $operatorInfo = M('operator')->where($where)->find();
            $sysInfo = M('sys')->where("id = 1")->find();
            $this->assign('sysInfo', $sysInfo);
            $this->assign('info', $operatorInfo);
            $_SESSION['creditCode'] = 400;
            $this->display();
        }
    }


    public function index()
    {
        if (IS_POST && IS_AJAX) {
            $state = I('state');
            $operatorId['id'] = I('operator_id');
            $operatorInfo = M('operator')->where($operatorId)->find();
            $operatorState = $operatorInfo['state'];

            if ($operatorState != 1) {
                $data['status'] = 0;
                $data['msg'] = '运营商账户被禁用';
                $this->ajaxReturn($data);
            }

            $id = I('id');
            $where['id'] = $id;
            $keyid = Utils::getDecrypt(I('keyid'));

            if ($id != $keyid) {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }

            if ($state == 3) {
                $data['state'] = Top::ApplyStateReject;
                $data['update_time'] = date("Y-m-d H:i:s", time());
                $result = M('operator_credit_topup')->where($where)->save($data);

                if ($result) {
                    $data['status'] = 1;
                    $data['msg'] = '操作成功';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '操作失败';
                    $this->ajaxReturn($data);
                }
            }

            $postCode = I('code');
            $code = session('checkApplyCode');

            if ($code == $postCode) {
                session('creditCode', null);
                M('operator_credit_transaction')->startTrans();
                try {
                    $where['id'] = $id;
                    $data['state'] = Top::ApplyStatePass;
                    $data['update_time'] = date("Y-m-d H:i:s", time());
                    $result = M('operator_credit_topup')->where($where)->save($data);

                    $info = M('operator_credit_topup')->where($where)->find();
                    $amount = $info['amount'];

                    $operatorId['id'] = I('operator_id');
                    $operatorResult = M('operator')->where($operatorId)->setInc('account_credit_balance', $amount);

                    $operatorInfo = M('operator')->where($operatorId)->find();
                    $data['operator_id'] = I('operator_id');
                    $data['sn'] = Utils::getSn();
                    $data['account'] = $operatorInfo['account'];
                    $data['entity_type'] = Top::EntityTypeOperator;
                    $data['entity_id'] = $operatorInfo['id'];
                    $data['action'] = "运营商账户充值申请";
                    $data['amount'] = $amount;
                    $data['balance'] = $operatorInfo['account_credit_balance'];
                    $data['remark'] = "top_operator_credit_topup.id:" . I('id');;
                    $data['object_id'] = I('id');
                    $data['object_type'] = Top::ObjectTypeCredit;
                    $data['object_name'] = "运营商账户充值申请";
                    $data['create_time'] = date('Y-m-d H:i:s', time());

                    $transactionResult = M('operator_credit_transaction')->add($data);

                    if ($result && $operatorResult && $transactionResult) {
                        M('operator_credit_transaction')->commit();
                        $data['status'] = 1;
                        $data['msg'] = '操作成功';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '操作失败';
                        $this->ajaxReturn($data);
                    }
                } catch (Exception $ex) {
                    M('operator_credit_transaction')->rollback();
                }
            } else {
                $data['status'] = 3;
                $data['msg'] = '请勿重复提交';
                $this->ajaxReturn($data);
            }
        } else {
            $_SESSION['checkApplyCode'] = 400;
            $role_id = session('role_id');
            $operator_id = session('operator_id');
            $where = "operator_id = '{$operator_id}'";

            $this->operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($this->operatorData, $role_id) !== false) {
                $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
                $operator_id = $this->oid;
                $where = "operator_id = '{$operator_id}'";

                if ($operator_id == 0) {
                    $where = "1 = 1";
                }
            }

            $pageInfo = Utils::pages('operator_credit_topup', $where, C('PAGE_SET'), $order = " id desc ");

            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
            $this->display();
        }
    }


    public function updateProof()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $data['proof_type'] = I('proof_type');
            $data['proof_code'] = I('proof_code');
            $result = M('operator_credit_topup')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }
    }


    public function details()
    {
        $role_id = session('role_id');
        $operator_id = session('operator_id');
        $where = "operator_id = '{$operator_id}'";

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $role_id) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $operator_id = $this->oid;
            $where = "operator_id = '{$operator_id}'";

            if ($this->oid == 0) {
                $where = "operator_id != 0";
            }
        }

        $pageInfo = Utils::pages('operator_credit_transaction', $where, C('PAGE_SET'), $order = " id desc ");

        foreach ($pageInfo['info'] as $key => $vo) {
            if ($vo['object_type'] == 1) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '充值';
            } else if ($vo['object_type'] == 2) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '提现';
            } else if ($vo['object_type'] == 3) {
                $pageInfo['info'][$key]['objectName'] = M('agency_invoice')->where("id = " . $vo['object_id'])->field('detail tour_name')->find();
            } else if ($vo['object_type'] == 4) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '合同';
            } else if ($vo['object_type'] == 5) {
                $pageInfo['info'][$key]['objectName'] = M('order_tour')->where("id = " . $vo['object_id'])->field('tour_name')->find();
            } else if ($vo['object_type'] == 6) {
                $pageInfo['info'][$key]['objectName'] = M('order_product')->where("id = " . $vo['object_id'])->field('product_name tour_name')->find();
            } else if ($vo['object_type'] == 7) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '机票';
            } else if ($vo['object_type'] == 8) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '账户调整';
            } else if ($vo['object_type'] == 9) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '门票';
            } else if ($vo['object_type'] == 10) {
                $pageInfo['info'][$key]['objectName']['tour_name'] = '授信充值';
            }

        }

        $this->info = $pageInfo['info'];
        $this->page = $pageInfo['page'];
        $this->assign('oid', $operator_id);
        $this->display();
    }

}
