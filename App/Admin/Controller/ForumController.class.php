<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class ForumController extends AdminController
{


    public function applyCredit()
    {
        if (IS_POST && IS_AJAX) {
            $state = I('state');

            if ($state != 1) {
                $data['status'] = 0;
                $data['msg'] = '账户被禁用';
                $this->ajaxReturn($data);
            }

            $postCode = I('code');
            $code = session('creditCode');

            if ($code == $postCode) {
                session('creditCode', null);
                $data['operator_id'] = 0;
                $data['entity_type'] = Top::EntityTypeSystem;
                $data['entity_name'] = '北京大旅科技有限公司';
                $data['account'] = I('account');
                $data['entity_id'] = session('entity_id');
                $data['amount'] = Utils::getFen(I('amount'));
                $data['topup_type'] = I('topup_type');
                $data['state'] = Top::ApplyStateWait;
                $data['create_time'] = date("Y-m-d H:i:s", time());
                $result = M('flight_topup')->add($data);

                if ($result) {
                    $data['status'] = 1;
                    $data['msg'] = '申请成功';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '申请失败';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 3;
                $data['msg'] = '请勿重复提交';
                $this->ajaxReturn($data);
            }
        } else {
            $where['id'] = 1;
            $operatorInfo = M('sys')->where($where)->find();
            $this->assign('info', $operatorInfo);
            $_SESSION['creditCode'] = 400;
            $this->display();
        }
    }



    public function index()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id');
            $keyid = Utils::getDecrypt(I('keyid'));
            $where['id'] = $id;
            $state = I('state');

            if ($id != $keyid) {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }

            if ($state == 3) {
                $data['state'] = Top::ApplyStateReject;
                $data['update_time'] = date("Y-m-d H:i:s", time());
                $result = M('flight_topup')->where($where)->save($data);

                if ($result) {
                    $data['status'] = 1;
                    $data['msg'] = '操作成功';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '操作失败';
                    $this->ajaxReturn($data);
                }
            }

            $postCode = I('code');
            $code = session('checkApplyCode');

            if ($code == $postCode) {
                session('creditCode', null);
                M('flight_transaction')->startTrans();
                try {
                    $where['id'] = $id;
                    $data['state'] = Top::ApplyStatePass;
                    $data['update_time'] = date("Y-m-d H:i:s", time());
                    $result = M('flight_topup')->where($where)->save($data);

                    $info = M('flight_topup')->where($where)->find();
                    $amount = $info['amount'];

                    $sysResult = M('sys')->where("id = 1")->setInc('account_credit_balance', $amount);

                    $sysInfo = M('sys')->where("id = 1")->find();
                    $data['operator_id'] = 0;
                    $data['sn'] = Utils::getSn();
                    $data['account'] = $sysInfo['account'];
                    $data['entity_type'] = Top::EntityTypeSystem;
                    $data['entity_id'] = $sysInfo['id'];
                    $data['action'] = "大旅平台账户充值申请";
                    $data['amount'] = $amount;
                    $data['balance'] = $sysInfo['account_credit_balance'];
                    $data['remark'] = "top_operator_credit_topup.id:" . I('id');;
                    $data['object_id'] = I('id');
                    $data['object_type'] = Top::ObjectTypeCredit;
                    $data['object_name'] = "大旅平台账户充值申请";
                    $data['create_time'] = date('Y-m-d H:i:s', time());

                    $transactionResult = M('flight_transaction')->add($data);

                    if ($result && $sysResult && $transactionResult) {
                        M('flight_transaction')->commit();
                        $data['status'] = 1;
                        $data['msg'] = '操作成功';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '操作失败';
                        $this->ajaxReturn($data);
                    }
                } catch (Exception $ex) {
                    M('flight_transaction')->rollback();
                }
            } else {
                $data['status'] = 3;
                $data['msg'] = '请勿重复提交';
                $this->ajaxReturn($data);
            }
        } else {
            $_SESSION['checkApplyCode'] = 400;
            $where = "operator_id = 0";

            $pageInfo = Utils::pages('flight_topup', $where, C('PAGE_SET'), $order = " id desc ");
            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
            $this->display();
        }
    }


    //平台账户变更记录
    public function details()
    {
         $startTime = !empty(I('start_time')) ? I('start_time') : date("2015-05-01", time());
        $endTime = !empty(I('end_time')) ? I('end_time') : date('Y-m-d', time());
        $where = "top_flight_transaction.operator_id = 0 && (top_flight_transaction.create_time >= '{$startTime} {$this->dateStart}' && top_flight_transaction.create_time <= '{$endTime} {$this->dateEnd}')";
        $filed = "top_flight_transaction.*,top_order_flight.agency_name";
        $dl_on = "top_flight_transaction.object_id = top_order_flight.id";
        $pageInfo = Utils::left_join_pages('flight_transaction','order_flight',$filed,$dl_on, $where, C('PAGE_SET'), $order = " top_flight_transaction.id desc ");

        $this->info = $pageInfo['info'];
        $this->page = $pageInfo['page'];
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->display();
    }


}
