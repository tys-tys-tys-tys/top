<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class ReportController extends AdminController
{

    /**
     *  供应商提现报表
     */
    public function providerWithdrawReport()
    {

        $roleId = session('role_id');
        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 1;
            $operatorId = $this->oid;
        }else{
            $operatorId = session('operator_id');
        }

        $entityType = Top::EntityTypeProvider;
        $state = Top::ApplyStateFinish;
        $startTime = !empty(I('start_time')) ? I('start_time') : date("2015-05-01", time());
        $endTime = !empty(I('end_time')) ? I('end_time') : date('Y-m-d', time());
        $providerName = trim(I('entity_name'));

        $where = "operator_id = '{$operatorId}' && entity_type = '{$entityType}' && state = '{$state}' && entity_name like '%%$providerName%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

        $order = 'create_time desc ';
        $pageInfo = Utils::pages('account_withdraw', $where, C('REPORT_PAGE_SET'), $order);

        $this->list = $pageInfo['info'];
        $list = $this->list;

        $withdrawTotal = 0;
        foreach ($list as $k => $v) {
            $withdrawTotal += $v['amount'];
        }

        $this->assign('providerName', $providerName);
        $this->assign('withdrawTotal', $withdrawTotal);
        $this->page = $pageInfo['page'];
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->display();
    }


    /**
     * 订单报表
     */
    public function orderReport()
    {
        $roleId = session('role_id');
        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 1;
            $operatorId = $this->oid;
        }else{
            $operatorId = session('operator_id');
        }

        $prePayedState = Top::OrderStatePrepayed;
        $allPayedState = Top::OrderStateAllPayed;
        $startTime = !empty(I('start_time')) ? I('start_time') : date("2015-05-01", time());
        $endTime = !empty(I('end_time')) ? I('end_time') : date('Y-m-d', time());
        $providerName = trim(I('entity_name'));
        $where = "(agency_operator_id = '{$operatorId}' || provider_operator_id = '{$operatorId}') && (state = '{$prePayedState}' || state = '{$allPayedState}') && (provider_name like '%%$providerName%%' || agency_name like '%%$providerName%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

        $order = 'create_time desc ';
        $pageInfo = Utils::pages('order_tour', $where, C('REPORT_PAGE_SET'), $order);

        foreach ($pageInfo['info'] as $key => $vo) {
            $arr[$key] = array(
                'id' => $vo['id'],
                'tour_name' => $vo['tour_name'],
                'provider_name' => $vo['provider_name'],
                'price_hotel' => $vo['price_hotel'],
                'price_adjust' => $vo['price_adjust'],
                'price_total' => $vo['price_total'],
                'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                'prepay_amount' => $vo['prepay_amount'],
                'prepay_amount_commission' => $vo['prepay_amount'] - Utils::CalcCommission($vo['commission_rate'], $vo['prepay_amount']),
                'price_payable_commission' => $vo['price_total'] + $vo['price_adjust'] - Utils::CalcCommission($vo['commission_rate'], $vo['price_total'] + $vo['price_adjust']),
                'state' => $vo['state'],
                'client_adult_count' => $vo['client_adult_count'],
                'client_child_count' => $vo['client_child_count'],
                'start_time' => $vo['start_time'],
                'create_time' => $vo['create_time'],
                'agency_name' => $vo['agency_name'],
            );
        }

        $this->info = $arr;
        $this->list = $pageInfo['info'];
        $list = $this->info;

        $realTotal = 0;
        foreach ($list as $k => $v) {
            if ($v['state'] == 6) {
                $realTotal += $v['prepay_amount_commission'];
            } else {
                $realTotal += $v['price_payable_commission'];
            }
        }

        $priceTotal = 0;
        foreach ($list as $k => $v) {
            $priceTotal += $v['price_total'] + $v['price_adjust'];
        }

        $this->assign('providerName', $providerName);
        $this->assign('realTotal', $realTotal);
        $this->assign('priceTotal', $priceTotal);
        $this->page = $pageInfo['page'];
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->display();
    }


    /**
     * 运营商交易统计
     */
    public function operatorTransactionReport()
    {
        $roleId = session('role_id');
        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 1;
            $operatorId = $this->oid;
        }else{
            $operatorId = session('operator_id');
        }

        $entityType = Top::EntityTypeOperator;
        $invoiceType = Top::ObjectTypeInvoice;
        $contractType = Top::ObjectTypeContact;
        $tourType = Top::ObjectTypeTour;
        $productType = Top::ObjectTypeProduct;

        $invoiceTotal = M('account_transaction')->where("operator_id = '{$operatorId}' && object_type = '{$invoiceType}' && entity_type = '{$entityType}'")->sum('amount');
        $contractTotal = M('account_transaction')->where("operator_id = '{$operatorId}' && object_type = '{$contractType}' && entity_type = '{$entityType}'")->sum('amount');
        $tourTotal = M('account_transaction')->where("operator_id = '{$operatorId}' && (object_type = '{$tourType}' || object_type = '{$productType}') && entity_type = '{$entityType}'")->sum('amount');

        $total = $invoiceTotal + $contractTotal + $tourTotal;

        $this->assign('invoiceTotal', $invoiceTotal);
        $this->assign('contractTotal', $contractTotal);
        $this->assign('tourTotal', $tourTotal);
        $this->assign('total', $total);
        $this->display();
    }


    /**
     * 决策分析
     */
    public function stat()
    {
        $name = session('baker_login_name');
        $time = date("YmdHis", time());
        $expire = date('YmdHis', strtotime('+60 second', strtotime($time)));
        $key = 'acfb381028b2bdcd';
        $sign = strtolower(md5("name=" . $name . "&expire=" . $expire . "&key=" . $key));
        $url = "http://stat.dalvu.com/home/sso?name=" . $name . "&expire=" . $expire . "&sign=" . $sign;
        $this->assign('url', $url);
        $this->display();
    }


}