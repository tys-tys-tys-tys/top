<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class OperatorController extends AdminController
{


    public function index()
    {
//       dump(session());exit;
        $roleId = session('role_id');
        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $where = "1 = 1";
        } else {
            $operatorId = session('operator_id');
            $where = "id = '{$operatorId}'";
        }

        $list = Utils::pages('operator', $where, C('PAGE_SET'), $order = " id asc ");
        $this->info = $list['info'];
        $this->page = $list['page'];
        $this->display();
    }


    public function add()
    {
        if (IS_POST && IS_AJAX) {
            $login_name = I('login_name');
            $entity_type = Top::EntityTypeOperator;
            $list = M('user')->where("login_name = '{$login_name}'")->find();
            $login_pwd = I('login_pwd');
            $login_pwd2 = I('login_pwd2');

            if ($login_pwd != $login_pwd2) {
                $data['status'] = 0;
                $data['msg'] = '两次密码不一致';
                $this->ajaxReturn($data);
            }

            if (!empty($list)) {
                $data['status'] = 0;
                $data['msg'] = '该运营商信息已存在，不可用';
                $this->ajaxReturn($data);
            } else {
                M('operator')->startTrans();
                try {
                    $data['name'] = I('name');
                    $data['account_balance'] = 0;
                    $data['account_credit_balance'] = 0;
                    $data['state'] = Top::StateEnabled;
                    $data['mobile'] = I('phone');
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $result = M('operator')->add($data);

                    $data['account'] = Utils::getNextAccount($entity_type, $result);
                    M('operator')->where("id = '{$result}'")->save($data);

                    $user_data['is_admin'] = 1;
                    $user_data['role_id'] = Top::DefalutOperatorRoleId;
                    $user_data['entity_type'] = Top::EntityTypeOperator;
                    $user_data['entity_id'] = $result;
                    $user_data['name'] = I('name');
                    $user_data['login_name'] = I('login_name');
                    $user_data['login_pwd'] = Utils::getHashPwd(I('login_pwd'));
                    $user_data['mobile'] = I('phone');
                    $user_data['state'] = Top::StateEnabled;
                    $user_data['create_time'] = date("Y-m-d H:i:s", time());

                    $userResult = M('user')->add($user_data);

                    if ($result && $userResult) {
                        M('operator')->commit();
                        $data['status'] = 1;
                        $data['msg'] = '操作成功';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '操作失败';
                        $this->ajaxReturn($data);
                    }
                } catch (Exception $ex) {
                    M('operator')->rollback();
                }
            }
        }
    }


    public function edit()
    {
        $this->operatorData = C('SHOW_OPERATOR_DATA');
        $operatorId = I('get.oid');
        $where['id'] = $operatorId;
        $operatorInfo = M('operator')->where($where)->find();

        $agencyApplyInvoiceList = M('operator_invoice_fee')->where("operator_id = '{$operatorId}'")->select();
        $entityId = $operatorInfo['id'];
        $operatorLoginName = M('user')->where("entity_type = 2 && entity_id = '{$entityId}'")->find();
        $operatorInfo['login_name'] = $operatorLoginName['login_name'];
        $this->assign('info', $operatorInfo);
        $this->assign('agencyApplyInvoiceList', $agencyApplyInvoiceList);
        $this->display();
    }


    public function saveOperatorInfo()
    {
        $where['id'] = I('id');
        $data['name'] = I('name');
        $data['share_operators'] = I('share_operators');
        $data['mobile'] = I('mobile');
        $data['tel'] = I('tel');
        $data['fax'] = I('fax');
        $data['address'] = I('address');
        $data['topup_info'] = $_POST['topup_info'];
        $data['alipay_info'] = $_POST['alipay_info'];
        $data['invoice_title'] = I('invoice_title');
        $data['domain'] = I('domain');
        $data['sms_ym_cdkey'] = I('cdkey');
        $data['sms_ym_pwd'] = I('pwd');
        $data['sms_ym_signature'] = I('signature');
        $data['copyright'] = I('copyright');
        $data['msg1'] = I('msg1');
        $data['contract_start_time'] = !empty(I('contract_start_time')) ? I('contract_start_time') : '0000-00-00 00:00:00';
        $data['contract_end_time'] = !empty(I('contract_end_time')) ? I('contract_end_time') : '0000-00-00 00:00:00';
        $data['update_time'] = date("Y-m-d H:i:s", time());
        $result = M('operator')->where($where)->save($data);

        $userWhere['entity_id'] = I('id');
        $userWhere['entity_type'] = Top::EntityTypeOperator;
        $userData['login_name'] = I('login_name');
        $userData['update_time'] = date("Y-m-d H:i:s", time());
        $userInfo = M('user')->where($userWhere)->find();

        if ($userData['login_name'] != $userInfo['login_name']) {
            M('user')->where($userWhere)->save($userData);
        }

        if ($result) {
            $data['status'] = 1;
            $data['msg'] = '修改成功';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 0;
            $data['msg'] = '修改失败';
            $this->ajaxReturn($data);
        }
    }


    public function saveInfo()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $data['mobile'] = I('mobile');
            $data['tel'] = I('tel');
            $data['fax'] = I('fax');
            $data['address'] = I('address');
            $data['topup_info'] = $_POST['topup_info'];
            $data['alipay_info'] = $_POST['alipay_info'];
            $data['invoice_title'] = I('invoice_title');
            $data['contract_fee'] = Utils::getFen(I('contractFee'));
            $data['express_fee'] = Utils::getFen(I('expressFee'));
            $data['copyright'] = I('copyright');
            $data['update_time'] = date("Y-m-d H:i:s", time());
            $result = M('operator')->where($where)->save($data);


            $operatorId = session('operator_id');
            $operatorInvoiceFeeData[0] = Utils::SetCommission(I('invoiceFee1'));
            $operatorInvoiceFeeData[1] = Utils::SetCommission(I('invoiceFee2'));
            $operatorInvoiceFeeData[2] = Utils::SetCommission(I('invoiceFee3'));
            $operatorInvoiceFeeData[3] = Utils::SetCommission(I('invoiceFee4'));
            $operatorInvoiceFeeData[4] = Utils::SetCommission(I('invoiceFee5'));
            $operatorInvoiceFeeData[5] = Utils::SetCommission(I('invoiceFee6'));
            $operatorInvoiceFeeData[6] = Utils::SetCommission(I('invoiceFee7'));


            $list = M('operator_invoice_fee')->where("operator_id = '{$operatorId}'")->select();
           

            foreach ($list as $k => $v) {
                $id = $v['id'];
                M('operator_invoice_fee')->execute("update top_operator_invoice_fee set fee_rate = '$operatorInvoiceFeeData[$k]' where id = '$id'");
            }

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '修改成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '修改失败';
                $this->ajaxReturn($data);
            }
        }
    }

}