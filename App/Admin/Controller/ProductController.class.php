<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Think\Controller;
use Think\Upload;
use Common\Top;
use Common\Utils;

class ProductController extends AdminController
{
    public function index()
    {
        if (IS_POST && IS_AJAX) {
            $auditState = I('post.state', 0, 'int');
            $id = I('post.id', 0, 'int');
            $state = M('product')->where('id=' . $id)->field('state,audit_memo')->find();

            $changeData['state'] = $auditState;

            if (!$auditState && $id) {
                $data['status'] = 1;
                $data['msg'] = $state['audit_memo'];
                $this->ajaxReturn($data);
            } else {
                if ($auditState == Top::ProductStateSubmitted && $state['state'] == Top::ProductStateSubmitted) {
                    $data['status'] = 2;
                    $data['msg'] = '该产品已经提交';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateCanceled && $state['state'] == Top::ProductStateSubmitted) {
                    $data['status'] = 3;
                    $data['msg'] = '该产品还在审核中';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateSubmitted && $state['state'] == Top::ProductStateCanceled) {
                    $data['status'] = 4;
                    $data['msg'] = '该产品已下架';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateCanceled && $state['state'] == Top::ProductStateRejected) {
                    $data['status'] = 5;
                    $data['msg'] = '该产品审核失败，请检查重新提交';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateSubmitted && $state['state'] == Top::ProductStateIsSale) {
                    $data['status'] = 6;
                    $data['msg'] = '该产品已经上架，无需再次提交';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateCanceled && $state['state'] == Top::ProductStateCanceled) {
                    $data['status'] = 7;
                    $data['msg'] = '该产品已经下架，请重新选择';
                    $this->ajaxReturn($data);
                } else if ($auditState == Top::ProductStateCanceled && $state['state'] == Top::ProductStateNotSubmitted) {
                    $data['status'] = 8;
                    $data['msg'] = '该产品还没有提交';
                    $this->ajaxReturn($data);
                } else {
                    $tour = M('product')->where('id=' . $id)->save($changeData);
                    if ($tour) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                }
            }
        } else {
            $entityId = session('entity_id');
            $name = trim(I('get.name'));

            $nameCode = substr($name, 0, 2);

            if ($nameCode == 20 && is_numeric($name)) {
                $code = substr($name, 2);
            }

            if (!empty($name)) {
                $where = "provider_id = '{$entityId}' && (name like '%%$name%%' || id = '{$code}')";
            } else {
                $where = "provider_id = '{$entityId}'";
            }

            $pageInfo = Utils::pages('product', $where, C('PAGE_SET'), $order = "id desc ");

            $this->assign('name', $name);
            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
            $this->display();
        }
    }


    public function add()
    {
        if (IS_POST && IS_AJAX) {
            $operatorId = session('operator_id');
            $providerId = session('provider_id');
            $user_id = session('user_id');
            $user_name = session('name');
            $providerInfo = M('provider')->where("id = '{$providerId}'")->field('id,commission_rate')->find();

            $data['provider_id'] = $providerId;
            $data['cover_pic'] = I('cover_pic');
            $data['pics'] = I('pics');
            $data['name'] = I('name');
            $data['description'] = $_POST['description'][0];
            $data['price_agency'] = Utils::getFen(I('price'));
            $data['price_cost'] = Utils::getFen(I('price_cost'));
            $data['para_type'] = I('para_type');
            $data['para_value'] = I('para_value');
            $data['stock'] = I('stock');
            $data['user_id'] = $user_id;
            $data['user_name'] = $user_name;
            $data['commission_rate'] = $providerInfo['commission_rate'];
            $data['operator_id'] = $operatorId;
            $data['state'] = Top::ProductStateNotSubmitted;
            $data['create_time'] = date("Y-m-d H:i:s", time());
            $result = M('product')->add($data);

            if (I('tid')) {
                $tid = I('tid');
            } else {
                $tid = $result;
            }

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '添加成功';
                $data['url'] = U('Product/add', array('id' => 111, 'tid' => $tid));
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '添加失败';
                $this->ajaxReturn($data);
            }
        } else {
            $product_id = I('get.tid');

            if ($product_id == '' && $product_id == null) {
                $product_id = 0;
            }

            if ($product_id) {
                $info = M('product')->where("id = '{$product_id}'")->find();
            }

            $infoPics = explode('@@@', $info['pics']);
            foreach ($infoPics as $k => $v) {
                if (!empty($v)) {
                    $infoImgAll[] = explode('|', $v);
                }
            }
            $info['imgs'] = $infoImgAll;
            $this->assign('info', $info);
            $this->display();
        }
    }


    public function edit()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id', 0, 'int');
            $keyid = Utils::getDecrypt(I('keyid'));

            if ($id != $keyid) {
                $data['status'] = 0;
                $data['msg'] = '修改失败';
                $this->ajaxReturn($data);
            }

            $user_id = session('user_id');
            $user_name = session('name');
            $entityType = session('entity_type');
            $providerId = session('provider_id');
            $providerInfo = M('provider')->where("id = '{$providerId}'")->field('id,commission_rate')->find();

            $data['cover_pic'] = I('cover_pic');
            $data['pics'] = I('pics');
            $data['name'] = I('name');
            $data['description'] = $_POST['description'][0];
            $data['price_agency'] = Utils::getFen(I('price'));
            $data['price_cost'] = Utils::getFen(I('price_cost'));
            $data['para_type'] = I('para_type');
            $data['para_value'] = I('para_value');
            $data['stock'] = I('stock');
            $data['user_id'] = $user_id;
            $data['user_name'] = $user_name;
            $data['commission_rate'] = $providerInfo['commission_rate'];
            $data['create_time'] = date("Y-m-d H:i:s", time());

            $result = M('product')->where("id = '{$id}'")->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '修改成功';

                if ($entityType == 3) {
                    $data['url'] = U('Product/edit', array('id' => 111, 'tid' => $id));
                } else {
                    $data['url'] = U('Product/edit', array('id' => 119, 'tid' => $id));
                }
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '修改失败';
                $this->ajaxReturn($data);
            }
        } else {
            $id = I('tid', 0, 'int');
            $list = M('product')->where("id='{$id}'")->find();
            $infoPics = explode('@@@', $list['pics']);
            foreach ($infoPics as $k => $v) {
                if (!empty($v)) {
                    $infoImgAll[] = explode('|', $v);
                }
            }

            $list['imgs'] = $infoImgAll;
            $this->assign('list', $list);
            $this->display();
        }
    }


    public function delete()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $ids = I('action_code');

            foreach ($ids as $id) {
                $id = Utils::getDecrypt($id);
                $result = M('product')->where("id = '{$id}'")->delete();
            }

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '删除成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '删除失败';
                $this->ajaxReturn($data);
            }
        }
    }


    public function putaway()
    {
        if (IS_AJAX && IS_POST) {
            $auditState = I('state', 0, 'int');
            $id = I('id', 0, 'int');
            $tour = M('product')->where("id = '{$id}'")->setField('state', $auditState);

            if ($tour) {
                $this->ajaxReturn(1);
            } else {
                $this->ajaxReturn(0);
            }
        }

        $entityId = session('entity_id');
        $state = Top::ProductStateIsSale;
        $name = trim(I('get.name'));

        $nameCode = substr($name, 0, 2);

        if ($nameCode == 20 && is_numeric($name)) {
            $code = substr($name, 2);
        }

        if (!empty($name)) {
            $where = "provider_id = '{$entityId}' && state = '{$state}' && (name like '%%$name%%' || id = '{$code}')";
        } else {
            $where = "provider_id = '{$entityId}' && state = '{$state}'";
        }

        $order = "id desc ";
        $pageInfo = Utils::pages('product', $where, C('PAGE_SET'), $order);
        $info = array(
            'info' => $pageInfo['info'],
            'page' => $pageInfo['page'],
        );

        $this->assign('name', $name);
        $this->info = $info['info'];
        $this->page = $info['page'];
        $this->display();
    }


    public function soldout()
    {
        if (IS_AJAX && IS_POST) {
            $auditState = I('state', 0, 'int');
            $id = I('id', 0, 'int');
            $tour = M('tour')->where("id = '{$id}'")->setField('state', $auditState);

            if ($tour) {
                $this->ajaxReturn(1);
            } else {
                $this->ajaxReturn(0);
            }
        }

        $entityId = session('entity_id');
        $state = Top::ProductStateCanceled;
        $name = trim(I('get.name'));

        $nameCode = substr($name, 0, 2);

        if ($nameCode == 20 && is_numeric($name)) {
            $code = substr($name, 2);
        }

        if (!empty($name)) {
            $where = "provider_id = '{$entityId}' && state = '{$state}' && (name like '%%$name%%' || id = '{$code}')";
        } else {
            $where = "provider_id = '{$entityId}' && state = '{$state}'";
        }

        $order = "id desc ";
        $pageInfo = Utils::pages('product', $where, C('PAGE_SET'), $order);
        $info = array(
            'info' => $pageInfo['info'],
            'page' => $pageInfo['page'],
        );

        $this->assign('name', $name);
        $this->info = $info['info'];
        $this->page = $info['page'];
        $this->display();
    }

}
