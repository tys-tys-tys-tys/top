<?php

namespace Admin\Controller;

use Common\Common\AdminController;

class ErrorController extends AdminController
{

    public function index()
    {
        $msg = '对不起,你没有访问权限';
        $this->assign('msg', $msg);
        $this->display();
    }


}
