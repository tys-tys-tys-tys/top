<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class AuthController extends AdminController
{

    public function index()
    {
        if (session('entity_type') != Top::EntityTypeSystem) {
            $where['id'] = session('role_id');
            $auth_ids = M('role')->where($where)->field('auth_ids')->find();
            $where = "id in " . $auth_ids['auth_ids'] . " entity_type !=" . Top::EntityTypeSystem;
        } else {
            $where = "1 = 1";
        }

        $order = ' path asc ';
        $pageInfo = Utils::pages('auth', $where, 25, $order);

        foreach ($pageInfo['info'] as $k => $v) {
            $pageInfo['info'][$k]['name'] = str_repeat('┣━', $v['level']) . $pageInfo['info'][$k]['name'];
        }

        $info = $this->info($level = 2);
        $this->authInfo = $info;
        $this->page = $pageInfo['page'];
        $this->info = $pageInfo['info'];
        $this->display();
    }


    public function add()
    {
        if (IS_POST && IS_AJAX) {
            $auth = new \Model\AuthModel();
            $auth_info = $auth->authAdd();

            if ($auth_info) {
                $msg['status'] = 1;
                $msg['msg'] = "权限添加成功";
                $this->ajaxReturn($msg);
            } else {
                $msg['status'] = 1;
                $msg['msg'] = "权限添加失败";
                $this->ajaxReturn($msg);
            }
        }
    }


    public function edit()
    {
        if (IS_POST && IS_AJAX) {
            if (I('post.skuState') == 'edit' && I('post.skuState')) {
                $auth = new \Model\AuthModel();
                $editAuthInfo = $auth->update();

                if ($editAuthInfo) {
                    $data['status'] = 1;
                    $data['msg'] = '修改成功！';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '修改失败！';
                    $this->ajaxReturn($data);
                }
            } else {
                $auth_one_info = M('auth')->find(I('id', 0, 'int'));
                $pauth_name = M('auth')->where('id=' . $auth_one_info['parent_id'])->field('name')->find();

                if (empty($pauth_name)) {
                    $pauth_name = "请选择";
                } else {
                    $pauth_name = $pauth_name['name'];
                }

                $authInfo = array(
                    'name' => $auth_one_info['name'],
                    'pname' => $pauth_name,
                    'parent_id' => $auth_one_info['parent_id'],
                    'controller' => $auth_one_info['controller'],
                    'method' => $auth_one_info['action'],
                    'entity_type' => $auth_one_info['entity_type'],
                    'remark' => $auth_one_info['remark'],
                );

                if ($auth_one_info) {
                    $data['status'] = 1;
                    $data['info'] = $authInfo;
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '系统繁忙！';
                    $this->ajaxReturn($data);
                }
            }
        }
    }


    public function delete()
    {
        if (IS_POST && IS_AJAX) {
            $auth = M('auth');
            $authids['id'] = array('in', I('action_code'));
            $where['parent_id'] = array('in', I('action_code'));
            $info = $auth->where($where)->count();

            if ($info > 0) {
                $data['status'] = 0;
                $data['msg'] = '请先删除其下的子栏目';
                $this->ajaxReturn($data);
            } else {
                if ($auth->where($authids)->delete()) {
                    $data['status'] = 1;
                    $data['msg'] = '删除成功！';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '删除失败！';
                    $this->ajaxReturn($data);
                }
            }
        }
    }

    /**
     * 权限层级显示
     */
    public function info($level = '')
    {
        if ($level == 2) {
            $info = D('Auth')->where('level<3')->order('path asc')->select();
        } else {
            $info = D('Auth')->order('path asc')->order('path asc')->select();
        }

        foreach ($info as $k => $v) {
            $info[$k]['name'] = str_repeat('&nbsp;┣━', $v['level']) . $info[$k]['name'];
        }

        return $info;
    }

}
