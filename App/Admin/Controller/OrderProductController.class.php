<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;
use Common\Sms;

class OrderProductController extends AdminController
{

    public function orderProduct()
    {
        if (IS_POST && IS_AJAX) {
            if (Utils::getFen(I('adjust')) != 0 && preg_match('/^((-)?\d+(\.\d+)?)?$/', I('adjust'))) {
                $edit_price = Utils::getFen(I('total')) + Utils::getFen(I('adjust'));
                $data['status'] = 1;
                $data['price'] = Utils::getYuan($edit_price);
                $this->ajaxReturn($data);
            }

            $orderInfo = D('OrderProduct')->where('id=' . I('id', 0, 'int'))->field('agency_id,price_adjust,price_total,prepay_amount,state,product_name,create_time,order_sn')->find();

            $state = I('state');

            if ($state == Top::OrderStateClosed && $state && $orderInfo['state'] != Top::OrderStatePrepayed && $orderInfo['state'] != Top::OrderStateAllPayed) {
                $id = I('id');
                $stateData['state'] = Top::OrderStateClosed;
                M('order_product')->where("id = '{$id}'")->save($stateData);

                $data['status'] = 6;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            }

            if ((I('editstate') && (I('editstate') == Top::OrderStateWaitForPayment || I('editstate') == Top::OrderStatePrepayed)) || $orderInfo['state'] = Top::OrderStateWaitForPayment) {
                if ($orderInfo['state'] == Top::OrderStateNotSubmitted) {
                    $data['status'] = 2;
                    $data['msg'] = '当前订单没有提交';
                    $this->ajaxReturn($data);
                } else if ($orderInfo['state'] == Top::OrderStateClosed) {
                    $data['status'] = 3;
                    $data['msg'] = '当前订单已关闭';
                    $this->ajaxReturn($data);
                } else if ($orderInfo['state'] == Top::OrderStateCanceled) {
                    $data['status'] = 4;
                    $data['msg'] = '当前订单已取消';
                    $this->ajaxReturn($data);
                } else if ($orderInfo['state'] == Top::OrderStateAllPayed) {
                    $data['status'] = 5;
                    $data['msg'] = '当前订单已付款';
                    $this->ajaxReturn($data);
                } else {

                    if (!I('editstate') && I('editamount') < 0 && $orderInfo['price_total'] >= I('editamount') && !preg_match('/^[0-9]+([.]{1}[0-9]+){0,1}$/', I('editamount'))) {
                        return false;
                    } else {
                        $order = D('OrderProduct')->orderEdit(I('id', 0, 'int'), I('editstate', 0, 'int'), Utils::getFen(I('edit_price')), Utils::getFen(I('editadjust')), Utils::getFen(I('editamount')), I('memo'));

                        $orderInfo = D('OrderProduct')->where('id=' . I('id', 0, 'int'))->find();

                        if ($orderInfo['prepay_amount'] == 0 || $orderInfo['prepay_amount'] == null) {
                            $content = "您的" . $orderInfo['product_name'] . "订单已被产品供应商确认，请尽快付全款，全款金额：" . (Utils::getYuan($orderInfo['price_total'] + $orderInfo['price_adjust']));
                        } else {
                            $orderState = (int)$orderInfo['state'];

                            if ($orderState == Top::OrderStateWaitForPayment) {
                                $content = "您的" . $orderInfo['product_name'] . "订单已被产品供应商确认，请尽快付预付款，预付金额：" . Utils::getYuan($orderInfo['prepay_amount']);
                            } else {
                                $payMent = intval($orderInfo['price_total'] + $orderInfo['price_adjust'] - $orderInfo['prepay_amount']);
                                $content = "您的" . $orderInfo['product_name'] . "订单已被产品供应商确认，请尽快付尾款，尾款金额：" . Utils::getYuan($payMent);
                            }
                        }

                        $agencyId = $orderInfo['agency_id'];
                        $agencyInfo = M('agency')->where("id = '{$agencyId}'")->find();
                        if ($order) {
                            $template = array(
                                'touser' => $agencyInfo['weixinmp_openid'],
                                'template_id' => Utils::orderStateChangeTemplateId(),

                                'data' => array(
                                    'first' => array('value' => urlencode($content), 'color' => "#000"),
                                    'keyword1' => array('value' => urlencode($orderInfo['order_sn']), 'color' => "#000"),
                                    'keyword2' => array('value' => urlencode(Utils::getYuan($orderInfo['price_total'] + $orderInfo['price_adjust'])), 'color' => '#000'),
                                    'keyword3' => array('value' => urlencode($orderInfo['create_time']), 'color' => '#000'),
                                    'keyword4' => array('value' => urlencode('供应商已确认,待付款'), 'color' => '#000'),
                                )
                            );
                            $this->send_tpl($template);

                            $data['status'] = 1;
                            $this->ajaxReturn($data);
                        } else {
                            $data['status'] = 0;
                            $data['msg'] = '确认订单失败';
                            $this->ajaxReturn($data);
                        }
                    }
                }
            } else if (I('state') && I('state') == Top::OrderStateClosed) {

                if (I('state') == Top::OrderStateClosed && $orderInfo['state'] == Top::OrderStateNotSubmitted) {
                    $data['status'] = 2;
                    $data['msg'] = '订单未提交，不能关闭';
                    $this->ajaxReturn($data);
                } else if (I('state') == Top::OrderStateClosed && $orderInfo['state'] == Top::OrderStateCanceled) {
                    $data['status'] = 3;
                    $data['msg'] = '订单已取消';
                    $this->ajaxReturn($data);
                } else if ($state['state'] == Top::OrderStatePrepayed || $orderInfo['state'] == Top::OrderStateAllPayed || $orderInfo['state'] == Top::OrderStatePrepayed) {
                    $data['status'] = 4;
                    $data['msg'] = '已付款不能关闭';
                    $this->ajaxReturn($data);
                } else if ($orderInfo['state'] == Top::OrderStateClosed) {
                    $data['status'] = 5;
                    $data['msg'] = '订单已关闭';
                    $this->ajaxReturn($data);
                } else {

                    $changeState = D('OrderProduct')->orderOne(I('id', 0, 'int'), I('state', 0, 'int'));

                    if ($changeState) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $data['status'] = 6;
                $data['msg'] = '系统请求出错';
                $this->ajaxReturn($data);
            }
        } else {
            $providerId['id'] = session('entity_id');
            $entityId = session('entity_id');
            $name = trim(I('get.name'));

            if (!empty($name)) {
                $where = "provider_id = '{$entityId}' && state > 0 && (agency_name like '%%$name%%' || product_name like '%%$name%%')";
            } else {
                $where = "provider_id = '{$entityId}' && state > 0";
            }

            $order = " id desc ";
            $pageInfo = Utils::pages('order_product', $where, C('PAGE_SET'), $order);

            foreach ($pageInfo['info'] as $key => $vo) {
                $arr[$key] = array(
                    'id' => $vo['id'],
                    'tour_name' => $vo['product_name'],
                    'contact_person' => $vo['contact_person'],
                    'quantity' => $vo['quantity'],
                    'price_hotel' => $vo['price_hotel'],
                    'price_adjust' => $vo['price_adjust'],
                    'price_total' => $vo['price_total'],
                    'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                    'prepay_amount' => $vo['prepay_amount'],
                    'state' => $vo['state'],
                    'create_time' => $vo['create_time'],
                    'agency_name' => $vo['agency_name'],
                    'memo' => $vo['memo'],
                );
            }
            $info = array(
                'info' => $arr,
                'page' => $pageInfo['page'],
            );

            $this->assign('name', $name);
            $this->info = $info['info'];
            $this->page = $info['page'];
            $this->display();
        }
    }


    public function details()
    {
        $this->info = D('OrderProduct')->orderDetails(I('sid'));
        $orderId = I('sid');
        $this->orderLog = M('log_order_tour')->where("order_id = '{$orderId}'")->select();
        $this->display();
    }

}
