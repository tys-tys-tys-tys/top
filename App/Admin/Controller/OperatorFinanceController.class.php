<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Utils;
use Common\Top;

class OperatorFinanceController extends AdminController
{


    public function details()
    {
        $role_id = session('role_id');
        $entity_type = session('entity_type');
        $operator_id = session('operator_id');
        $action = trim(I('entity_name'));
        $startTime = !empty(I('start_time')) ? I('start_time') : date("2015-05-01", time());
        $endTime = !empty(I('end_time')) ? I('end_time') : date('Y-m-d', time());

        $where = "operator_id = '{$operator_id}' && entity_type = '{$entity_type}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $role_id) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $this->type = I('get.type');
            $operatorId = $this->oid;

            if (empty($this->type)) {
                $this->type = 't1';
            }

            if ($this->type == 't1') {
                $where = "operator_id = '{$operatorId}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

                if ($this->oid == 0) {
                    $where = "operator_id != 0 && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                }
            }

            if ($this->type == 't2') {
                $where = "operator_id = '{$this->oid}' && (action = '收入线路产品订单佣金' || action = '收入通用产品订单佣金') && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

                if ($this->oid == 0) {
                    $where = "operator_id != 0 && (action = '收入线路产品订单佣金' || action = '收入通用产品订单佣金') && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                }
            }
        }

        $pageInfo = Utils::pages('account_transaction', $where, C('TRANSACTION_PAGE_SET'), $order = " id desc ");

        if ($pageInfo['info']) {
            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
            $this->assign('startTime', $startTime);
            $this->assign('endTime', $endTime);
            $this->assign('name', $action);
        } else {
            $this->msg = '当前没有记录';
        }

        $this->display();
    }


    public function operatorAccount()
    {
        $operatorId = session('operator_id');
        $roleId = session('role_id');

        if ($roleId == 1 || $roleId == 185 || $roleId == 186) {
            $operatorInfo = M('sys')->where("id = 1")->find();
            $list = M('operator')->where('id != 2')->field('id,account_credit_balance,display_name')->select();
        } else {
            $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
        }

        $this->assign('operatorInfo', $operatorInfo);
        $this->assign('list', $list);
        $this->display();
    }

}
