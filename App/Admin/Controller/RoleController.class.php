<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class RoleController extends AdminController
{


    public function index()
    {
        $entityId = session('entity_id');
        $entityType = session('entity_type');

        $where = "entity_id = '{$entityId}' && entity_type = '{$entityType}'";
        $order = "id asc ";
        $list = Utils::pages('role', $where, C('PAGE_SET'), $order);

        $this->assign('info', $list['info']);
        $this->assign('page', $list['page']);
        $this->display();
    }



    public function add()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $role = M('role');
            $_POST['is_admin'] = '0';
            $_POST['entity_type'] = session('entity_type');
            $_POST['entity_id'] = session('entity_id');
            $_POST['create_time'] = date('Y-m-d H:i:s', time());

            $role->create();

            if ($role->add()) {
                $data['status'] = 1;
                $data['msg'] = '角色添加成功！';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '角色添加成功！';
                $this->ajaxReturn($data);
            }
        }
    }

    /**
     * 分配权限
     */
    public function allAuth()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $ids = I('action_code');
            $auth_ids = implode(',', $ids);

            $data = array(
                'id' => I('get.aid'),
                'auth_ids' => $auth_ids,
                'update_time' => date('Y-m-d H:i:s', time()),
            );

            if (M('role')->setField($data)) {
                $data['status'] = 1;
                $data['msg'] = '分配权限成功！';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '分配权限失败！';
                $this->ajaxReturn($data);
            }
        } else {
            if (session('entity_type') != 1 && session('role_id') != 1) {
                $auth_show = M('role')->where('entity_id= 0' . ' and entity_type=' . session('entity_type'))->field('auth_ids')->find();
                $auth_ids = $auth_show['auth_ids']; //当前实体有那些权限
                $auth = M('role')->where('id=' . I('get.aid'))->field('auth_ids')->find();
                $data['auth_id'] = $auth['auth_ids']; //当前用户在当前实体中的权限
                $data['top'] = D('Auth')->where("level=0 and id in ($auth_ids)")->select();
                $data['stair'] = D('Auth')->where("level=1 and id in ($auth_ids)")->select();
                $data['second'] = D('Auth')->where("level=2 and id in ($auth_ids)")->select();
                $data['three_stage'] = D('Auth')->where("level=3 and id in ($auth_ids)")->select();
//                 dump($data);exit;
            } else {
                $auth_show = M('role')->field('auth_ids')->find(I('get.aid'));
                $data['auth_id'] = $auth_show['auth_ids'];//当前实体有那些权限
                $data['top'] = D('auth')->where('level=0')->select();
                $data['stair'] = D('auth')->where('level=1')->select();
                $data['second'] = D('auth')->where('level=2')->select();
                $data['three_stage'] = D('auth')->where('level=3')->select();
               
            }
            $data['info'] = M('role')->field('id,name')->find(I('get.aid'));
            $this->assign('roleinfo', $data);
            $this->display();
        }
    }


    public function delete()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $roleIds = I('action_code');

            foreach ($roleIds as $id) {
                $id = Utils::getDecrypt($id);
                $info = M('role')->where("id = '{$id}'")->field('is_admin')->find();

                if ($info['is_admin'] == 1) {
                    $data['status'] = 0;
                    $data['msg'] = '管理员不能删除！';
                    $this->ajaxReturn($data);
                } else {
                    $result = M('role')->where("id = '{$id}'")->delete();
                }
            }

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '删除成功！';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '删除失败！';
                $this->ajaxReturn($data);
            }
        }
    }


    public function edit()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            if (I('name')) {
                $arr = array(
                    'name' => I('name'),
                    'update_time' => date('Y-m-d H:i:s', time()),
                );

                $id = I('tid');
                $name = I('name');
                $roleInfo = M('role')->where("id != '{$id}' && name = '{$name}'")->find();

                if (!empty($roleInfo)) {
                    $data['status'] = 0;
                    $data['msg'] = '该角色已存在，请重新输入！';
                    $this->ajaxReturn($data);
                } else {
                    if (M('role')->where("id = '{$id}'")->setField($arr)) {
                        $data['status'] = 1;
                        $data['msg'] = '角色修改成功！';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '角色修改失败！';
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $role = M('role')->where('id=' . I('id'))->field('name')->find();

                if ($role) {
                    $data['name'] = $role['name'];
                    $data['status'] = 1;
                    $data['info'] = $role;
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '系统繁忙！';
                    $this->ajaxReturn($data);
                }
            }
        }
    }

}
