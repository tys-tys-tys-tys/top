<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class OrderController extends AdminController
{


    public function index()
    {
        $name = trim(I('names'));
        $operatorId = session('operator_id');
        $roleId = session('role_id');
        $startTime = I('start_time');
        $endTime = I('end_time');
        $dateType = I('date_type');

        if (empty($dateType)) {
            $dateType = 2;
        }

        $startTime = !empty($startTime) ? $startTime : date("2015-05-01", time());
        $endTime = !empty($endTime) ? $endTime : date('Y-m-d', time());

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $operatorId = $this->oid;

            if ($this->oid == 0) {
                $operatorId = implode(',', array_column($this->operatorList, 'id'));
            }
        }

        if ($dateType == 1) {
            $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) and (agency_name like '%%$name%%' or tour_name like '%%$name%%' or provider_name like '%%$name%%' or provider_full_name like '%%$name%%' or id like '%%$name%%') && (start_time >= '{$startTime} {$this->dateStart}' && start_time <= '{$endTime} {$this->dateEnd}')";
        } else {
            $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) and (agency_name like '%%$name%%' or tour_name like '%%$name%%' or provider_name like '%%$name%%' or provider_full_name like '%%$name%%' or id like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
        }

        $pageInfo = Utils::pages('order_tour', $where, C('REPORT_PAGE_SET'), $order = " create_time desc ");
        foreach ($pageInfo['info'] as $key => $vo) {
            $arr[$key] = array(
               
                'id' => $vo['id'],
                'tour_name' => $vo['tour_name'],
                'provider_name' => $vo['provider_name'],
                'price_hotel' => $vo['price_hotel'],
                'price_adjust' => $vo['price_adjust'],
                'price_total' => $vo['price_total'],
                'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                'prepay_amount' => $vo['prepay_amount'],
                'prepay_amount_commission' => $vo['prepay_amount'] - Utils::CalcCommission($vo['commission_rate'], $vo['prepay_amount']),
                'price_payable_commission' => $vo['price_total'] + $vo['price_adjust'] - Utils::CalcCommission($vo['commission_rate'], $vo['price_total'] + $vo['price_adjust']),
                'prepay_commission' => Utils::CalcCommission($vo['commission_rate'], $vo['prepay_amount']),
                'total_commission' => Utils::CalcCommission($vo['commission_rate'], $vo['price_total'] + $vo['price_adjust']),
                'state' => $vo['state'],
                'client_adult_count' => $vo['client_adult_count'],
                'client_child_count' => $vo['client_child_count'],
                'start_time' => $vo['start_time'],
                'create_time' => $vo['create_time'],
                'agency_name' => $vo['agency_name'],
                'operator_name' => getOperatorName($vo['agency_operator_id']),
                'prepay_time' => $vo['prepay_time'],
                'fullpay_time' => $vo['fullpay_time'],
            );
        }

        $this->info = $arr;
        $this->page = $pageInfo['page'];
        $this->assign('name', $name);
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->assign('dateType', $dateType);
        $this->display();
    }


    /**
     * 通用产品订单列表
     */
    public function productList()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 2;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $where['id'] = I('id');
            $data['proof_type'] = I('proof_type');
            $data['proof_code'] = I('proof_code');
            $result = M('order_product')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }

        $name = trim(I('names'));
        $operatorId = session('operator_id');
        $roleId = session('role_id');
        $startTime = I('start_time');
        $endTime = I('end_time');
        $dateType = I('date_type');

        if (empty($dateType)) {
            $dateType = 2;
        }

        $startTime = !empty($startTime) ? $startTime : date("2015-05-01", time());
        $endTime = !empty($endTime) ? $endTime : date('Y-m-d', time());

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $operatorId = $this->oid;

            if ($this->oid == 0) {
                $operatorId = implode(',', array_column($this->operatorList, 'id'));
            }
        }

        if (IS_GET) {
            if ($dateType == 1) {
                $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) and (agency_name like '%%$name%%' or product_name like '%%$name%%' or provider_name like '%%$name%%' or id like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) and (agency_name like '%%$name%%' or product_name like '%%$name%%' or provider_name like '%%$name%%' or id like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            }
        }

        $pageInfo = Utils::pages('order_product', $where, C('REPORT_PAGE_SET'), $order = " create_time desc ");

        foreach ($pageInfo['info'] as $key => $vo) {
            $arr[$key] = array(
                'id' => $vo['id'],
                'product_name' => $vo['product_name'],
                'provider_name' => $vo['provider_name'],
                'quantity' => $vo['quantity'],
                'price_adjust' => $vo['price_adjust'],
                'price_total' => $vo['price_total'],
                'price_payable' => $vo['price_total'] + $vo['price_adjust'],
                'prepay_amount' => $vo['prepay_amount'],
                'state' => $vo['state'],
                'agency_name' => $vo['agency_name'],
                'create_time' => $vo['create_time'],
                'proof_type' => $vo['proof_type'],
                'proof_code' => $vo['proof_code'],
                'price_agency' => $vo['price_agency'],
                'price_cost' => $vo['price_cost'],
                'para_type' => $vo['para_type'],
                'para_value' => $vo['para_value'],
                'prepay_amount_commission' => $vo['prepay_amount'] - Utils::CalcCommission($vo['commission_rate'], $vo['prepay_amount']),
                'price_payable_commission' => $vo['price_total'] + $vo['price_adjust'] - Utils::CalcCommission($vo['commission_rate'], $vo['price_total'] + $vo['price_adjust']),
                'prepay_commission' => Utils::CalcCommission($vo['commission_rate'], $vo['prepay_amount']),
                'total_commission' => Utils::CalcCommission($vo['commission_rate'], $vo['price_total'] + $vo['price_adjust']),
            );
        }

        $this->info = $arr;
        $this->page = $pageInfo['page'];
        $this->assign('name', $name);
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->assign('dateType', $dateType);
        $this->display();
    }



    /**
     * 显示参数信息
     */
    public function showParaInfo()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $productInfo = M('order_product')->where($where)->find();

            if ($productInfo['para_type'] == '1') {
                $productInfo['para_type'] = '保险';
            }

            $data['para_type'] = $productInfo['para_type'];
            $data['para_value'] = $productInfo['para_value'];
            $this->ajaxReturn($data);
        }
    }



    /**
     * 添加参数信息
     */
    public function addParaInfo()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $para_type = I('para_type');

            if ($para_type == '保险') {
                $para_type = 1;
            }

            $data['para_type'] = $para_type;
            $data['para_value'] = I('para_value');
            $result = M('order_product')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 机票产品订单
     */

    public function planeTicketList()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $data['proof_type'] = I('proof_type');
            $data['proof_code'] = I('proof_code');
            $result = M('order_flight')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }

        $name = trim(I('names'));
        $operatorId = session('operator_id');
        $startTime = !empty(I('start_time')) ? I('start_time') : date("2015-05-01", time());
        $endTime = !empty(I('end_time')) ? I('end_time') : date('Y-m-d', time());
        $roleId = session('role_id');

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $operatorId = $this->oid;

            if ($this->oid == 0) {
                $operatorId = implode(',', array_column($this->operatorList, 'id'));
            }
        }

        $where = "operator_id in($operatorId) && (agency_name like '%$name%' or passenger_names like '%$name%' or id like '%%$name%%' or sequence_no like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

        $list = Utils::pages('order_flight', $where, C('REPORT_PAGE_SET'), $order = " id desc ");

        $this->info = $list['info'];
        $this->page = $list['page'];
        $this->assign('name', $name);
        $this->assign('startTime', $startTime);
        $this->assign('endTime', $endTime);
        $this->display();
    }


    /**
     * 机票订单详情
     */
    public function planeTicketDetail()
    {
        $flightId = I('get.pid');
        $list = M('order_flight_detail')->where("order_flight_id = '{$flightId}'")->select();
        $this->assign('list', $list);
        $this->display();
    }

    /**
     * 机票已退票,退款完成
     */
    public function refundFlightTicket()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $data['refund_fee'] = Utils::getFen(I('refund_fee'));
            $data['refund_time'] = date("Y-m-d H:i:s", time());
            $data['state'] = Top::FlightOrderStateRefunded;

            $result = M('order_flight_detail')->where($where)->save($data);

            $flightId = I('fid');
            $flightState['state'] = Top::FlightOrderStateHasRefunded;
            $flightResult = M('order_flight')->where("id = '{$flightId}'")->save($flightState);

            if ($result && $flightResult) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 门票
     */
    public function scenicTicketList()
    {
        $name = trim(I('names'));
        $operatorId = session('operator_id');

        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $data['proof_type'] = I('proof_type');
            $data['proof_code'] = I('proof_code');
            $result = M('order_ticket')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }

        $roleId = session('role_id');

        $this->operatorData = C('SHOW_OPERATOR_DATA');

        if (strpos($this->operatorData, $roleId) !== false) {
            $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
            $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
            $operatorId = $this->oid;

            if ($this->oid == 0) {
                $operatorId = implode(',', array_column($this->operatorList, 'id'));
            }
        }

        if (!empty($name)) {
            $where = "(id like '%$name%' or agency_name like '%$name%' or scenic_name like '%$name%') && operator_id in($operatorId)";
        } else {
            $where = "operator_id in($operatorId)";
        }

        $list = Utils::pages('order_ticket', $where, C('REPORT_PAGE_SET'), $order = " id desc ");

        $this->info = $list['info'];
        $this->page = $list['page'];
        $this->display();
    }


    /**
     * 订单取消 - 初核
     */
    function orderCancel()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $where['id'] = I('id');
            $data['state'] = I('state');
            $orderInfo = M('order_tour')->where($where)->find();

            $entityId = $orderInfo['provider_id'];
            $entityType = Top::EntityTypeProvider;
            $operatorId = $orderInfo['provider_operator_id'];

            $providerBalance = Utils::getProviderInOperatorFrozenFen($entityType, $entityId, $operatorId);

            if ($orderInfo['fullpay_time'] != '') {
                $tourMoney = $orderInfo['price_total'] + $orderInfo['price_adjust'] - Utils::CalcCommission($orderInfo['commission_rate'], $orderInfo['price_total'] + $orderInfo['price_adjust']);
            } else {
                $tourMoney = $orderInfo['prepay_amount'] - Utils::CalcCommission($orderInfo['commission_rate'], $orderInfo['prepay_amount']);
            }

            if ($data['state'] == 12) {
                if ($providerBalance >= $tourMoney) {
                    $orderLogData['content'] = $orderLogData['content'] = '代理商管理' . session('entity_name') . '审核订单取消,审核通过';
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '供应商冻结金额不足';
                    $this->ajaxReturn($data);
                }
            }

            if ($data['state'] == 11) {
                $orderLogData['content'] = '代理商管理' . session('entity_name') . '审核订单取消,拒绝取消';
            }

            $result = M('order_tour')->where($where)->save($data);

            $orderLogData['order_id'] = $orderInfo['id'];
            $orderLogData['user_id'] = session('user_id');
            $orderLogData['user_name'] = session('entity_name');
            $orderLogData['amount'] = 0;
            $orderLogData['payable_total'] = $tourMoney;
            $orderLogData['create_time'] = date('Y-m-d H:i:s', time());

            $orderLogResult = M('log_order_tour')->add($orderLogData);

            if ($result && $orderLogResult) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 订单取消 - 复核
     */
    function orderCancelFinance()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $where['id'] = I('id');
            $data['state'] = I('state');

            $orderInfo = M('order_tour')->where($where)->find();
            $entityId = $orderInfo['provider_id'];
            $entityType = Top::EntityTypeProvider;
            $operatorId = $orderInfo['provider_operator_id'];

            $providerBalance = Utils::getProviderInOperatorFrozenFen($entityType, $entityId, $operatorId);


            if ($orderInfo['fullpay_time'] != '') {
                $totalMoneyFen = $orderInfo['price_total'] + $orderInfo['price_adjust'] - Utils::CalcCommission($orderInfo['commission_rate'], $orderInfo['price_total'] + $orderInfo['price_adjust']);
                $totalMoney = $orderInfo['price_total'] + $orderInfo['price_adjust'];
            } else {
                $totalMoneyFen = $orderInfo['prepay_amount'] - Utils::CalcCommission($orderInfo['commission_rate'], $orderInfo['prepay_amount']);
                $totalMoney = $orderInfo['prepay_amount'];
            }

            $orderLogData['order_id'] = $orderInfo['id'];
            $orderLogData['user_id'] = session('user_id');
            $orderLogData['user_name'] = session('entity_name');
            $orderLogData['amount'] = $totalMoneyFen;
            $orderLogData['payable_total'] = $totalMoneyFen;
            $orderLogData['create_time'] = date('Y-m-d H:i:s', time());

            if ($data['state'] == 11) {
                $orderLogData['content'] = '运营商财务管理' . session('entity_name') . '审核订单取消,拒绝取消';
                //向 log_order_tour 表插入记录
                $orderLogResult = M('log_order_tour')->add($orderLogData);
                $result = M('order_tour')->where($where)->save($data);

                if ($result && $orderLogResult) {
                    $data['status'] = 1;
                    $data['msg'] = '操作成功';
                    $this->ajaxReturn($data);
                }
            }


            M('operator')->startTrans();
            try {
                if ($providerBalance >= $totalMoneyFen) {
                    $orderLogData['content'] = '运营商财务管理' . session('entity_name') . '审核订单取消,审核通过';
                    //向 log_order_tour 表插入记录
                    $orderLogResult = M('log_order_tour')->add($orderLogData);

                    $commissionRate = $orderInfo['commission_rate'];
                    $operatorId = $orderInfo['provider_operator_id'];
                    $providerId = $orderInfo['provider_id'];
                    $agencyId = $orderInfo['agency_id'];

                    $commissionFen = Utils::CalcCommission($commissionRate, $totalMoney);
                    //运营商减去佣金
                    $operatorDecMoney = M('operator')->where("id = '{$operatorId}'")->setDec("account_balance", $commissionFen);

                    $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
                    $nowTime = Utils::getBatch();
                    $operatorData['operator_id'] = $operatorInfo['id'];
                    $operatorData['action'] = '订单取消:佣金返还';
                    $operatorData['entity_id'] = $operatorInfo['id'];
                    $operatorData['balance'] = $operatorInfo['account_balance'];
                    $operatorData['entity_type'] = Top::EntityTypeOperator;
                    $operatorData['amount'] = '-' . $commissionFen;
                    $operatorData['account'] = $operatorInfo['account'];
                    $operatorData['sn'] = Utils::getSn();
                    $operatorData['batch'] = $nowTime;
                    $operatorData['create_time'] = date('Y-m-d H:i:s', time());
                    $operatorData['balance'] = $operatorInfo['account_balance'];
                    $operatorData['remark'] = 'top_order_tour.id:' . $orderInfo['id'];
                    $operatorData['object_id'] = $orderInfo['id'];
                    $operatorData['object_type'] = Top::ObjectTypeTour;
                    $operatorData['object_name'] = '线路';
                    //向 account_transaction 表插入一条运营商减钱的记录
                    $operator_account_rate_delmoney = M('account_transaction')->add($operatorData);


                    //供应商减去去除佣金的收入
                    $providerDecMoney = M('provider')->where("id = '{$providerId}'")->setDec("account_balance", $totalMoneyFen);
                    $providerAccountDecMoney = M('provider_account')->where("provider_id = '{$providerId}' && operator_id = '{$operatorId}'")->setDec("account_balance", $totalMoneyFen);

                    $providerAccountInfo = M('provider_account')->where("provider_id = '{$providerId}' && operator_id = '{$operatorId}'")->find();
                    $providerAccountData['operator_id'] = $providerAccountInfo['operator_id'];
                    $providerAccountData['action'] = '订单取消:退款';
                    $providerAccountData['entity_id'] = $providerAccountInfo['provider_id'];
                    $providerAccountData['entity_type'] = Top::EntityTypeProvider;
                    $providerAccountData['account'] = $providerAccountInfo['account'];
                    $providerAccountData['sn'] = Utils::getSn();
                    $providerAccountData['batch'] = $nowTime;
                    $providerAccountData['amount'] = '-' . $totalMoneyFen;
                    $providerAccountData['create_time'] = date('Y-m-d H:i:s', time());
                    $providerAccountData['balance'] = $providerAccountInfo['account_balance'];
                    $providerAccountData['remark'] = 'top_order_tour.id:' . $orderInfo['id'];
                    $providerAccountData['object_id'] = $orderInfo['id'];
                    $providerAccountData['object_type'] = Top::ObjectTypeTour;
                    $providerAccountData['object_name'] = '线路';
                    //向 account_transaction 表插入一条供应商增钱的记录
                    $provider_account_delmoney = M('account_transaction')->add($providerAccountData);

                    $agencyIncMoney = M('agency')->where("id = '{$agencyId}'")->setInc("account_balance", $totalMoney);
                    $agencyInfo = M('agency')->where("id = '{$agencyId}'")->find();
                    $agencyData['operator_id'] = $agencyInfo['operator_id'];
                    $agencyData['action'] = '订单取消:退款';
                    $agencyData['entity_id'] = $agencyInfo['id'];
                    $agencyData['entity_type'] = Top::EntityTypeAgency;
                    $agencyData['account'] = $agencyInfo['account'];
                    $agencyData['sn'] = Utils::getSn();
                    $agencyData['batch'] = $nowTime;
                    $agencyData['amount'] = $totalMoney;
                    $agencyData['create_time'] = date('Y-m-d H:i:s', time());
                    $agencyData['balance'] = $agencyInfo['account_balance'];
                    $agencyData['remark'] = 'top_order_tour.id:' . $orderInfo['id'];
                    $agencyData['object_id'] = $orderInfo['id'];
                    $agencyData['object_type'] = Top::ObjectTypeTour;
                    $agencyData['object_name'] = '线路';
                    //向 account_transaction 表插入一条代理商增钱的记录
                    $agency_account_addmoney = M('account_transaction')->add($agencyData);

                    $operatorAccountCreditBalanceIncMoney = M('operator')->where("id = '{$operatorId}'")->setInc("account_credit_balance", $totalMoney);
                    $operatorAccountInfo = M('operator')->where("id = '{$operatorId}'")->find();
                    $operatorAccountData['operator_id'] = $operatorAccountInfo['id'];
                    $operatorAccountData['action'] = '订单取消:退款';
                    $operatorAccountData['entity_id'] = $operatorAccountInfo['id'];
                    $operatorAccountData['entity_type'] = Top::EntityTypeOperator;
                    $operatorAccountData['account'] = $operatorAccountInfo['account'];
                    $operatorAccountData['sn'] = Utils::getSn();
                    //$operatorAccountData['batch'] = $nowTime;
                    $operatorAccountData['amount'] = $totalMoney;
                    $operatorAccountData['create_time'] = date('Y-m-d H:i:s', time());
                    $operatorAccountData['balance'] = $operatorAccountInfo['account_credit_balance'];
                    $operatorAccountData['remark'] = 'top_order_tour.id:' . $orderInfo['id'];
                    $operatorAccountData['object_id'] = $orderInfo['id'];
                    $operatorAccountData['object_type'] = Top::ObjectTypeTour;
                    $operatorAccountData['object_name'] = '线路';
                    //向 account_transaction 表插入一条运营商增钱的记录
                    $operator_account_addmoney = M('operator_credit_transaction')->add($operatorAccountData);


                    $result = M('order_tour')->where($where)->save($data);

                    if ($result && $operatorDecMoney && $operator_account_rate_delmoney && $providerDecMoney && $providerAccountDecMoney && $provider_account_delmoney && $agencyIncMoney && $agency_account_addmoney && $operatorAccountCreditBalanceIncMoney && $operator_account_addmoney && $orderLogResult) {
                        M('operator')->commit();
                        $data['status'] = 1;
                        $data['msg'] = '操作成功';
                        $this->ajaxReturn($data);
                    }
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '供应商冻结金额不足';
                    $this->ajaxReturn($data);
                }
            } catch (Exception $ex) {
                M('operator')->rollback();
            }
        }
    }


}
