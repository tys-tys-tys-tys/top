<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class GetApplyExcelController extends AdminController
{

    /**
     * Excel 代理商 - 列表 - 导出
     */
    public function getAgencyListExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name)) {
                $where = "operator_id in($operatorId) && name like '%%$name%%' && state != 4";
            } else {
                $where = "operator_id in($operatorId) && state != 4";
            }

            $data = M('agency')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}代理商列表.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '运营商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '名称');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '电话');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '合约起止日期');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '账户总额');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '微信绑定');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '账户状态');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '帐号状态');

            $i = 2;
            foreach ($data as $r) {
                $r['contract_time'] = substr($r['contract_start_time'], 0, 10) . '至' . substr($r['contract_end_time'], 0, 10);


                if ($r['operator_id'] == '1') {
                    $r['operator_name'] = '北京';
                } else if ($r['operator_id'] == '3') {
                    $r['operator_name'] = '天津';
                } else if ($r['operator_id'] == '4') {
                    $r['operator_name'] = '石家庄';
                } else if ($r['operator_id'] == '5') {
                    $r['operator_name'] = '唐山';
                }

                if ($r['weixinmp_openid'] == '') {
                    $r['weixinmp_openid'] = '否';
                } else {
                    $r['weixinmp_openid'] = '是';
                }


                if ($r['account_state'] == '1') {
                    $r['account_state'] = '正常';
                } else {
                    $r['account_state'] = '冻结';
                }

                if ($r['state'] == '1') {
                    $r['state'] = '正常';
                } else if ($r['state'] == '2') {
                    $r['state'] = '禁用';
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['operator_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, ' ' . $r['mobile']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['contract_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['account_balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['weixinmp_openid']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['account_state']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 代理商 - 交易记录 - 导出
     */
    public function getAgencyTransactionExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $entity_id = I('get.aid');

            $agencyInfo = M('agency')->where("id = '$entity_id'")->find();
            $operatorId = $agencyInfo['operator_id'];
            $name = $agencyInfo['name'];
            $action = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');

            if (!empty($action) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id = '{$operatorId}' && entity_type = '{$entity_type}' && entity_id = '{$entity_id}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id = '{$operatorId}' && entity_type = '{$entity_type}' && entity_id = '{$entity_id}'";
            }

            $data = M('account_transaction')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}{$name}的交易记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '交易号');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '明细');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '用途');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '金额');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '余额');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '操作时间');

            $i = 2;
            foreach ($data as $r) {
                if ($r['object_type'] == 1) {
                    $r['objectName']['tour_name'] = '充值';
                } else if ($r['object_type'] == 2) {
                    $r['objectName']['tour_name'] = '提现';
                } else if ($r['object_type'] == 3) {
                    $r['objectName'] = M('agency_invoice')->where("id = " . $r['object_id'])->field('detail tour_name')->find();
                } else if ($r['object_type'] == 4) {
                    $r['objectName']['tour_name'] = '合同';
                } else if ($r['object_type'] == 5) {
                    $r['objectName'] = M('order_tour')->where("id = " . $r['object_id'])->field('tour_name')->find();
                } else if ($r['object_type'] == 6) {
                    $r['objectName'] = M('order_product')->where("id = " . $r['object_id'])->field('product_name tour_name')->find();
                } else if ($r['object_type'] == 7) {
                    $r['objectName']['tour_name'] = '机票';
                } else if ($r['object_type'] == 8) {
                    $r['objectName']['tour_name'] = '账户调整';
                } else if ($r['object_type'] == 9) {
                    $r['objectName']['tour_name'] = '门票';
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['object_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, ' ' . $r['sn']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['objectName']['tour_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['action']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['memo']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 充值申请 - 导出
     */
    public function getAgencyTopUpExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($action) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}' && entity_name like '%%$name%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}'";
            }

            $data = M('account_topup')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}代理商的充值申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '充值金额');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 提现申请 - 导出
     */
    public function getAgencyWithDrawExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $state = I('state');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                if (!empty($state)) {
                    $where = "operator_id in($operatorId) && entity_type = '{$entity_type}' && state = '{$state}' && (entity_name like '%%$name%%' or amount like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                } else {
                    $where = "operator_id in($operatorId) && entity_type = '{$entity_type}' && (entity_name like '%%$name%%' or amount like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                }
            }

            $data = M('account_withdraw')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}代理商的提现申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '提现金额');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '账户总额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '提现账户');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '状态');

            $i = 2;
            foreach ($data as $r) {
                $entityId = $r['entity_id'];
                $balanceInfo = M('agency')->where("id = '{$entityId}' && operator_id = '{$operatorId}'")->field('id,account_balance')->find();
                $r['balance'] = $balanceInfo['account_balance'];

                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已确认，待财务处理';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                } else if ($r['state'] == 4) {
                    $r['state'] = '处理完成';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];
                $r['bank'] = $r['bank_name'] . $r['bank_account'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['bank']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 发票申请 - 导出
     */
    public function getAgencyInvoiceExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}' && entity_name like '%%$name%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}'";
            }

            $data = M('agency_invoice')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}代理商的发票申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '发票抬头');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '发票项目');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '发票金额');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '服务费');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '索取方式');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                if ($r['request_method'] == 1) {
                    $r['request_method'] = '自取';
                } else if ($r['request_method'] == 2 && $r['express_fee'] == 0) {
                    $r['request_method'] = '快递 到付';
                } else {
                    $r['request_method'] = '快递 包邮';
                }

                $r['amount'] = Utils::getYuan($r['amount']);
                if ($r['detail'] == '会议服务费') {
                    $r['tax'] = Utils::setServiceFeeTax($r['amount']);
                } else {
                    $r['tax'] = Utils::setTax($r['amount']);
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['title']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['detail']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['amount']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['tax']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['request_method']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $r['remark']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 合同申请 - 导出
     */
    public function getAgencyContractExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $operatorId = session('operator_id');
            $roleId = session('role_id');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}' && entity_name like '%%$name%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id in($operatorId) && entity_type = '{$entity_type}'";
            }

            $data = M('agency_contract')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}代理商的合同申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '国内合同');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '境外合同');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '单项委托');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '数量');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '单价');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '邮费');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '合计');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '索取方式');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('N1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['express_fee'] == 0) {
                    $r['express_fee'] = 0;
                } else {
                    $r['express_fee'] = Utils::getYuan($r['express_fee']);
                }

                $r['contract_fee'] = Utils::getYuan($r['contract_fee']);

                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                if ($r['request_method'] == 1) {
                    $r['request_method'] = '自取';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'];
                } else if ($r['request_method'] == 2 && $r['express_fee'] == 0) {
                    $r['request_method'] = '快递 到付';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'];
                } else {
                    $r['request_method'] = '快递 包邮';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'] + $r['express_fee'];
                }

                $r['totalNum'] = $r['inland_count'] + $r['inland_count'] + $r['peritem_count'];
                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['inland_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['outbound_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['peritem_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['totalNum']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['contract_fee']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['express_fee']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['totalPrice']);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $r['request_method']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 供应商信息 - 导出
     */
    public function getProviderExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $name = trim(I('sname'));
            $roleId = session('role_id');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name)) {
                $where = "operator_id in($operatorId) && (name like '%%$name%%' or full_name like '%%$name%%')";
            } else {
                $where = "operator_id in($operatorId)";
            }


            $data = M('provider')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}供应商基本信息.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代码');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '公司简称');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '公司全称');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '联系人');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '手机');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '银行账户');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '经营目的地');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '佣金比例');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '合约起止日');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '账户总额');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '账户状态');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '状态');
            $objPHPExcel->getActiveSheet()->setCellValue('N1', '创建时间');

            $i = 2;
            foreach ($data as $r) {
                $r['contractDate'] = substr($r['contract_start_time'], 0, 10) . " 至 " . substr($r['contract_end_time'], 0, 10);

                if ($r['account_state'] == '1') {
                    $r['account_state'] = '正常';
                } else if ($r['account_state'] == '2') {
                    $r['account_state'] = '冻结';
                }

                if ($r['state'] == '1') {
                    $r['state'] = '正常';
                } else if ($r['state'] == '2') {
                    $r['state'] = '禁用';
                }

                $r['bank'] = $r['bank_name'] . $r['bank_account'];
                $r['commission_rate'] = Utils::GetCommissionRate($r['commission_rate']) . "‰";

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['code']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['full_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['contact_person']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, ' ' . $r['mobile']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['bank']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['destinations']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['commission_rate']);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $r['contractDate']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, Utils::getYuan($r['account_balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['account_state']);
                $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $r['state']);
                $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 意见反馈-顾问 - 导出
     */
    public function getAdviserFeedbackExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $name = trim(I('sname'));
            $roleId = session('role_id');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

               if (!empty($name)) {
                $where = "operator_id in($operatorId) && comment_type = 0 && (cover_commnet_name like '%%$name%%')";
            } else {
                $where = "operator_id in($operatorId) && comment_type = 0 ";
            }


            $data = M('feedback')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}意见反馈-顾问-信息.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '运营商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '客户');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '顾问');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '评星');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '意见反馈内容');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '创建时间');

            $i = 2;
            foreach ($data as $r) {
                $r['OperatorName'] = getOperatorName($r['operator_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['OperatorName']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['cover_commnet_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['star']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['comm_content']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }
    /**
     * Excel 意见反馈-供应商 - 导出
     */
    public function getSupplierFeedbackExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $name = trim(I('sname'));
            $roleId = session('role_id');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

               if (!empty($name)) {
                $where = "operator_id in($operatorId) && comment_type = 1 && (cover_commnet_name like '%%$name%%')";
            } else {
                $where = "operator_id in($operatorId) && comment_type = 1 ";
            }


            $data = M('feedback')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}意见反馈-供应商-信息.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '运营商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '顾问');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '供应商');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '评星');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '意见反馈内容');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '创建时间');

            $i = 2;
            foreach ($data as $r) {
                $r['OperatorName'] = getOperatorName($r['operator_id']);
                $r['cover_commnet_name'] = getSupplierName($r['cover_commnet_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['OperatorName']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['cover_commnet_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['star']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['comm_content']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }
    /**
     * Excel 意见反馈-供应商 - 导出(供应商)
     */
    public function getSupplierFeedback_gExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $name = trim(I('sname'));
            $provider_id = session('provider_id');
            if (!empty($name)) {
                $where = "cover_commnet_id = '{$provider_id}'  && comment_type = 1 && (cover_commnet_name like '%%$name%%' )";
            } else {
                $where = "cover_commnet_id = '{$provider_id}' && comment_type = 1 ";
            }


            $data = M('feedback')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}意见反馈-供应商-信息.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '运营商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '顾问');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '供应商');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '评星');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '意见反馈内容');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '创建时间');

            $i = 2;
            foreach ($data as $r) {
                $r['OperatorName'] = getOperatorName($r['operator_id']);
                $r['cover_commnet_name'] = getSupplierName($r['cover_commnet_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['OperatorName']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['cover_commnet_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['star']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['comm_content']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 供应商 - 交易记录 - 导出
     */
    public function getProviderTransactionExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeProvider;
            $entity_id = I('get.aid');
            $operatorId = I('get.oid');
            $providerInfo = M('provider')->where("id = '$entity_id'")->find();
            $name = $providerInfo['name'];
            $action = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');

            if (!empty($action) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id = '{$operatorId}' && entity_type = '{$entity_type}' && entity_id = '{$entity_id}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id = '{$operatorId}' && entity_type = '{$entity_type}' && entity_id = '{$entity_id}'";
            }

            $data = M('account_transaction')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}{$name}的交易记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '交易号');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '明细');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '用途');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '金额');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '余额');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '操作时间');

            $i = 2;
            foreach ($data as $r) {
                if ($r['object_type'] == 5) {
                    $r['objectName'] = M('order_tour')->where("id = " . $r['object_id'])->field('tour_name')->find();
                } else if ($r['object_type'] == 6) {
                    $r['objectName'] = M('order_product')->where("id = " . $r['object_id'])->field('product_name tour_name')->find();
                } else if ($r['object_type'] == 2) {
                    $r['objectName']['tour_name'] = '提现';
                } else if ($r['object_type'] == 8) {
                    $r['objectName']['tour_name'] = '账户调整';
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['object_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, ' ' . $r['sn']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['objectName']['tour_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['action']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['memo']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 供应商 - 提现申请 - 导出
     */
    public function getProviderWithDrawExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $state = I('state');
            $roleId = session('role_id');
            $entityType = Top::EntityTypeProvider;

            if ($roleId == 185 || $roleId == 1) {
                if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                    if ($state != 0) {
                        $where = "(entity_name like '%%$name%%' or full_name like '%%$name%%' or amount = '{$name}') && entity_type = '{$entityType}' && state = '{$state}' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    } else {
                        $where = "(entity_name like '%%$name%%' or full_name like '%%$name%%'  or amount = '{$name}') && entity_type = '{$entityType}' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    }
                }
            } else {
                if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                    if ($state != 0) {
                        $where = "operator_id = '{$operatorId}' && entity_type = '{$entityType}' && (entity_name like '%%$name%%' or full_name like '%%$name%%'  or amount = '{$name}') && state = '{$state}' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    } else {
                        $where = "operator_id = '{$operatorId}' && entity_type = '{$entityType}' && (entity_name like '%%$name%%' or full_name like '%%$name%%'  or amount = '{$name}') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    }
                }
            }

            $data = M('account_withdraw')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}供应商的提现申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '供应商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '提现金额');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '账户总额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '提现账户');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '状态');

            $i = 2;
            foreach ($data as $r) {
                $entityId = $r['entity_id'];
                $balanceInfo = M('provider_account')->where("provider_id = '{$entityId}' && operator_id = '{$operatorId}'")->field('id,account_balance')->find();
                $r['balance'] = $balanceInfo['account_balance'];

                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已确认，待财务处理';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                } else if ($r['state'] == 4) {
                    $r['state'] = '处理完成';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];
                $r['bank'] = $r['bank_name'] . $r['bank_account'];
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['bank']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel  线路产品 - 导出
     */
    public function getTourExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $dateType = I('date_type');


            if ($dateType == '2') {
                $dateType = 'create_time';
            } else {
                $dateType = 'start_time';
            }

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) && (agency_name like '%$name%' or tour_name like '%$name%' or provider_name like '%%$name%%' or id like '%%$name%%') && ($dateType >= '{$startTime} {$this->dateStart}' && $dateType <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "agency_operator_id in($operatorId) or provider_operator_id in($operatorId)";
            }

            $data = M('order_tour')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}线路产品记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '线路名称');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '供应商');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '全款金额');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '预付金额');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '实际预付');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '预付时间');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '付尾款时间');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '实际收入');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '佣金');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '订单状态');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '成人');
            $objPHPExcel->getActiveSheet()->setCellValue('N1', '儿童');
            $objPHPExcel->getActiveSheet()->setCellValue('O1', '团期');
            $objPHPExcel->getActiveSheet()->setCellValue('P1', '创建时间');

            $i = 2;
            foreach ($data as $r) {
                $r['totalPrice'] = $r['price_total'] + $r['price_adjust'];

                if ($r['state'] == 1) {
                    $r['state'] = '未提交';
                    $r['actualPrice'] = 0.00;
                } else if ($r['state'] == 2) {
                    $r['state'] = '已提交';
                    $r['actualPrice'] = 0.00;
                } else if ($r['state'] == 3) {
                    $r['state'] = '已确认待付款';
                    $r['actualPrice'] = 0.00;
                } else if ($r['state'] == 4) {
                    $r['state'] = '已取消';
                    $r['actualPrice'] = 0.00;
                } else if ($r['state'] == 5) {
                    $r['state'] = '已关闭';
                    $r['actualPrice'] = 0.00;
                } else if ($r['state'] == 6) {
                    $r['state'] = '已付预付款';
                    $r['actualPrice'] = $r['prepay_amount'] - Utils::CalcCommission($r['commission_rate'], $r['prepay_amount']);
                    $r['commissionPrice'] = Utils::CalcCommission($r['commission_rate'], $r['prepay_amount']);
                } else if ($r['state'] == 7) {
                    $r['state'] = '已付全款';
                    $r['actualPrice'] = $r['price_total'] + $r['price_adjust'] - Utils::CalcCommission($r['commission_rate'], $r['price_total'] + $r['price_adjust']);
                    $r['commissionPrice'] = Utils::CalcCommission($r['commission_rate'], $r['price_total'] + $r['price_adjust']);
                } else if ($r['state'] == 8) {
                    $r['state'] = '已退款';
                    $r['actualPrice'] = $r['price_total'] + $r['price_adjust'] - Utils::CalcCommission($r['commission_rate'], $r['price_total'] + $r['price_adjust']);
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['tour_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['provider_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, Utils::getYuan($r['totalPrice']));
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['prepay_amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, Utils::getYuan($r['prepay_amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, Utils::getYuan($r['prepay_time']));
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, Utils::getYuan($r['fullpay_time']));
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, Utils::getYuan($r['actualPrice']));
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, Utils::getYuan($r['commissionPrice']));
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['state']);
                $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $r['client_adult_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $r['client_child_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $r['start_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel  通用产品 - 导出
     */
    public function getProductExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $dateType = I('date_type');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                if ($dateType == '2') {
                    $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) && (agency_name like '%$name%' or provider_name like '%$name%' or product_name like '%%$name%%' or id like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                } else {
                    $where = "(agency_operator_id in($operatorId) or provider_operator_id in($operatorId)) && (agency_name like '%$name%' or provider_name like '%$name%' or product_name like '%%$name%%' or id like '%%$name%%') && (para_value >= '{$startTime}' && para_value <= '{$endTime}')";
                }
            } else {
                $where = "agency_operator_id in($operatorId) or provider_operator_id in($operatorId)";
            }

            $data = M('order_product')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}通用产品订单记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '产品名称');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '供应商');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '数量');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '同行价');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '成本价');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '实际收入');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '佣金');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '利润');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '状态');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '参数类型');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '参数值');
            $objPHPExcel->getActiveSheet()->setCellValue('N1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('O1', '创建时间');

            $i = 2;
            foreach ($data as $r) {

                if ($r['state'] == 1) {
                    $r['state'] = '未提交';
                    $r['actualPrice'] = 0.00;
                    $r['commission'] = 0;
                    $r['payTotal'] = 0;
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 2) {
                    $r['state'] = '已提交';
                    $r['actualPrice'] = 0.00;
                    $r['commission'] = 0;
                    $r['payTotal'] = 0;
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 3) {
                    $r['state'] = '已确认待付款';
                    $r['actualPrice'] = 0.00;
                    $r['commission'] = 0;
                    $r['payTotal'] = 0;
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 4) {
                    $r['state'] = '已取消';
                    $r['actualPrice'] = 0.00;
                    $r['commission'] = 0;
                    $r['payTotal'] = 0;
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 5) {
                    $r['state'] = '已关闭';
                    $r['actualPrice'] = 0.00;
                    $r['commission'] = 0;
                    $r['payTotal'] = 0;
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 6) {
                    $r['state'] = '已付预付款';
                    $r['actualPrice'] = $r['prepay_amount'];
                    $r['commission'] = Utils::CalcCommission($r['commission_rate'], $r['prepay_amount']);
                    $r['payTotal'] = $r['prepay_amount'] - Utils::CalcCommission($r['commission_rate'], $r['prepay_amount']);
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                } else if ($r['state'] == 7) {
                    $r['state'] = '已付全款';
                    $r['actualPrice'] = $r['price_agency'] * $r['quantity'];
                    $r['commission'] = Utils::CalcCommission($r['commission_rate'], $r['price_total'] + $r['price_adjust']);
                    $r['payTotal'] = $r['price_total'] + $r['price_adjust'] - Utils::CalcCommission($r['commission_rate'], $r['price_total'] + $r['price_adjust']);
                    $r['profit'] = $r['price_agency'] * $r['quantity'] - $r['price_cost'] * $r['quantity'];
                }

                if ($r['price_cost'] == 0) {
                    $r['profit'] = 0;
                }

                if ($r['para_type'] == 1) {
                    $r['para_type'] = '保险';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['product_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['provider_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['quantity']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['price_agency']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, Utils::getYuan($r['price_cost']));
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, Utils::getYuan($r['payTotal']));
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, Utils::getYuan($r['commission']));
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, Utils::getYuan($r['profit']));
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $r['state']);
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['para_type']);
                $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $r['para_value']);
                $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel  机票 - 导出
     */
    public function getFlightTicketExcel()
    {
        if (IS_POST) {
            if ($this->warnStatus == '10404') {
                $this->redirect("Error/index");
            }

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = session('operator_id');
            $roleId = session('role_id');
            $name = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');

            $operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($operatorData, $roleId) !== false) {
                $operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $operatorId = !empty(I('get.oid')) ? I('get.oid') : 0;

                if ($operatorId == 0) {
                    $operatorId = implode(',', array_column($operatorList, 'id'));
                }
            }

            if (!empty($name) || !empty($startTime) || !empty($endTime)) {
                $where = "operator_id in($operatorId) && (agency_name like '%$name%' or passenger_names like '%$name%' or id like '%%$name%%' or sequence_no like '%%$name%%') && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
            } else {
                $where = "operator_id in($operatorId)";
            }

            $data = M('order_flight')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}机票产品订单记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '乘机人');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '应付金额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '下单时间');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '凭证');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '订单状态');


            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 1) {
                    $r['state'] = '未提交';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已付款,待出票';
                } else if ($r['state'] == 3) {
                    $r['state'] = '已付款,已出票';
                } else if ($r['state'] == 4) {
                    $r['state'] = '已取消';
                } else if ($r['state'] == 5) {
                    $r['state'] = '已退票,退票成功';
                } else if ($r['state'] == 6) {
                    $r['state'] = '出票失败';
                } else if ($r['state'] == 7) {
                    $r['state'] = '预定失败';
                } else if ($r['state'] == 8) {
                    $r['state'] = '退票失败';
                } else if ($r['state'] == 9) {
                    $r['state'] = '退票中,待退款';
                } else if ($r['state'] == 10) {
                    $r['state'] = '有取消';
                } else if ($r['state'] == 11) {
                    $r['state'] = '有退票';
                } else if ($r['state'] == 12) {
                    $r['state'] = '顾问已删除';
                } else if ($r['state'] == 14) {
                    $r['state'] = '已预订,待付款';
                } else if ($r['state'] == 15) {
                    $r['state'] = '已预订,付款中';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['passenger_names']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, Utils::getYuan($r['price_total']));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['proof']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel  财务管理 - 交易明细 - 导出
     */
    public function getOperatorFinanceExcel()
    {
        if (IS_POST) {
            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operator_id = session('operator_id');
            $role_id = session('role_id');
            $entity_type = session('entity_type');
            $action = trim(I('sname'));
            $startTime = I('start_time');
            $endTime = I('end_time');
            $date = date("Y-m-d", time());

            $where = "operator_id = '{$operator_id}' && entity_type = '{$entity_type}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

            $this->operatorData = C('SHOW_OPERATOR_DATA');

            if (strpos($this->operatorData, $role_id) !== false) {
                $this->operatorList = M('operator')->where('id != 2')->field('id,display_name')->select();
                $this->oid = !empty(I('get.oid')) ? I('get.oid') : 0;
                $this->type = I('get.type');
                $operatorId = $this->oid;

                if (empty($this->oid)) {
                    $this->oid = 0;
                }

                if (empty($this->type)) {
                    $this->type = 't1';
                }

                if ($this->type == 't1') {
                    $where = "operator_id = '{$operatorId}' && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

                    if ($this->oid == 0) {
                        $where = "operator_id != 0 && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    }
                }

                if ($this->type == 't2') {
                    $where = "operator_id = '{$this->oid}' && (action = '收入线路产品订单佣金' || action = '收入通用产品订单佣金') && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";

                    if ($this->oid == 0) {
                        $where = "operator_id != 0 && (action = '收入线路产品订单佣金' || action = '收入通用产品订单佣金') && action like '%%$action%%' && (create_time >= '{$startTime} {$this->dateStart}' && create_time <= '{$endTime} {$this->dateEnd}')";
                    }
                }

            }

            $data = M('account_transaction')->where($where)->select();
            $fileName = "_{$date}交易记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '流水号');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '交易说明');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '入账时间');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '收入金额');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '账户余额');

            if ($this->type == 't2') {
                $objPHPExcel->getActiveSheet()->setCellValue('G1', '分润比例');
            }


            $i = 2;
            foreach ($data as $r) {
                $r['profit_rate'] = $r['profit_rate'] / 10000 . "%";
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, ' ' . $r['sn']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['action']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['balance']));

                if ($this->type == 't2') {
                    $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['profit_rate']);
                }
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }
    /**
     * Excel  财务管理 - 退还机票服务费 - 导出
     */
    public function getServiceChargeExcel()
    {
        if (IS_POST) {
            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");
            $startTime = I('start_time');
            $endTime = I('end_time');
            $pingjie ="&& f.operator_id = '0' && f.create_time >= '{$startTime} {$this->dateStart}' && f.create_time <= '{$endTime} {$this->dateEnd}'";
            $where = " f.action = '退还服务费' $pingjie  || f.action = '扣除服务费' $pingjie";
            $date = date("Y-m-d", time());
            $data = M('flight_transaction')->alias('f')->field('f.*,o.agency_name')->join('top_order_flight o ON f.object_id = o.id')->where($where)->select();
            $fileName = "_{$date}机票服务费.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '顾问');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '用途');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '金额');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '操作时间');


            $i = 1;
            foreach ($data as $r) {
                $r['profit_rate'] = $r['profit_rate'] / 10000 . "%";
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['agency_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, ' ' . $r['object_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['action']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['memo']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

}
