<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Top;
use Common\Utils;

class RefundController extends AdminController
{

    public function index()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id');
            $keyid = Utils::getDecrypt(I('keyid'));

            if ($id != $keyid) {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }

            $state = I('state');
            $refundInfo = M('account_adjust')->where("id = '{$id}'")->find();
            $entity_id = $refundInfo['entity_id'];
            $entity_type = $refundInfo['account_type'];
            $dataState['state'] = $state;

            if ($state == Top::ApplyStateReject) {
                $stateResult = M('account_adjust')->where("id = '{$id}'")->save($dataState);

                if ($stateResult) {
                    $data['status'] = 1;
                    $data['msg'] = '操作成功';
                    $this->ajaxReturn($data);
                }
            }

            if ($state == Top::ApplyStatePass && $refundInfo['state'] == Top::ApplyStateWait) {
                $postCode = I('code');
                $code = session('checkRefundCode');

                if ($code == $postCode) {
                    session('checkRefundCode', null);

                    if ($entity_type == '4') {
                        $table = 'agency';
                    } else if ($entity_type == '3') {
                        $table = 'provider';
                    } else if ($entity_type == '2' || $entity_type == '5') {
                        $table = 'operator';
                    } else if ($entity_type == '6') {
                        $table = 'sys';
                    }

                    M($table)->startTrans();
                    try {
                        $where['id'] = $refundInfo['entity_id'];
                        $amount = $refundInfo['amount'];

                        if ($entity_type == '3') {
                            $providerWhere['provider_id'] = $entity_id;
                            $providerWhere['operator_id'] = $refundInfo['operator_id'];
                            $providerAccountInfo = M('provider_account')->where($providerWhere)->find();

                            if ($amount < 0) {
                                $amount = abs($amount);
                                $balance = $providerAccountInfo['account_balance'];

                                if ($balance - $amount >= 0) {
                                    $result = M($table)->where("id = '{$entity_id}'")->setDec('account_balance', $amount);
                                    M('provider_account')->where($providerWhere)->setDec('account_balance', $amount);
                                } else {
                                    $data['status'] = 0;
                                    $data['msg'] = '金额不能大于账户余额';
                                    $this->ajaxReturn($data);
                                }
                            } else {
                                $result = M($table)->where("id = '{$entity_id}'")->setInc('account_balance', $amount);
                                M('provider_account')->where($providerWhere)->setInc('account_balance', $amount);
                            }
                        } else {
                            if ($amount < 0) {
                                $balanceInfo = M($table)->where($where)->find();
                                $amount = abs($amount);
                                $balance = $balanceInfo['account_balance'];

                                if ($entity_type == '5' || $entity_type == '6') {
                                    $result = M($table)->where($where)->setDec('account_credit_balance', $amount);
                                } else {
                                    if ($balance - $amount >= 0) {
                                        $result = M($table)->where($where)->setDec('account_balance', $amount);
                                    } else {
                                        $data['status'] = 0;
                                        $data['msg'] = '金额不能大于账户余额';
                                        $this->ajaxReturn($data);
                                    }
                                }
                            } else {
                                if ($entity_type == '5' || $entity_type == '6') {
                                    $result = M($table)->where($where)->setInc('account_credit_balance', $amount);
                                } else {
                                    $result = M($table)->where($where)->setInc('account_balance', $amount);
                                }

                            }
                        }

                        $stateResult = M('account_adjust')->where("id = '{$id}'")->save($dataState);

                        $info = M($table)->where($where)->find();
                        $data['operator_id'] = $refundInfo['operator_id'];
                        $data['sn'] = Utils::getSn();
                        $data['account'] = $refundInfo['account'];
                        $data['entity_type'] = $refundInfo['entity_type'];
                        $data['entity_id'] = $refundInfo['entity_id'];
                        $data['action'] = "手动调整:" . $refundInfo['memo'];
                        $data['amount'] = $refundInfo['amount'];

                        if ($entity_type == '5') {
                            $data['balance'] = $info['account_credit_balance'];
                            $table = 'operator_credit_transaction';
                        } else if ($entity_type == '6') {
                            $data['balance'] = $info['account_credit_balance'];
                            $data['operator_id'] = 0;
                            $table = 'flight_transaction';
                        } else {
                            $data['balance'] = $info['account_balance'];
                            $table = 'account_transaction';
                        }

                        $data['remark'] = "top_account_adjust.id:$id";
                        $data['object_id'] = $id;
                        $data['object_type'] = Top::ObjectTypeRefund;
                        $data['object_name'] = "账户调整";
                        $data['create_time'] = date('Y-m-d H:i:s', time());

                        $transactionResult = M($table)->add($data);

                        if ($result && $stateResult && $transactionResult) {
                            M($table)->commit();
                            $data['status'] = 1;
                            $data['msg'] = '操作成功';
                            $this->ajaxReturn($data);
                        } else {
                            $data['status'] = 0;
                            $data['msg'] = '操作失败';
                            $this->ajaxReturn($data);
                        }

                    } catch (Exception $ex) {
                        M($table)->rollback();
                    }
                } else {
                    $data['status'] = 3;
                    $data['msg'] = '请勿重复提交';
                    $this->ajaxReturn($data);
                }
            }
        } else {
            $_SESSION['checkRefundCode'] = 400;
            $name = trim(I('names'));
            $operator_id = session('operator_id');
            $roleId = session('role_id');

            if ($operator_id != 1 && $roleId != 1) {
                $this->redirect('Refund/_empty', array('id' => 129));
            }

            if ($roleId == 1) {
                if (!empty($name)) {
                    $where = "entity_name like '%$name%'";
                } else {
                    $where = "1 = 1";
                }
            } else {
                if (!empty($name)) {
                    $where = "operator_id = '{$operator_id}' and entity_name like '%$name%'";
                } else {
                    $where = "operator_id = '{$operator_id}'";
                }
            }

            $pageInfo = Utils::pages('account_adjust', $where, C('PAGE_SET'), $order = " id desc ");

            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
        }

        $this->display();
    }


    public function add()
    {
        $operator_id = session('operator_id');
        $roleId = session('role_id');

        if (IS_POST) {
            $account_type = I('account_type');

            if ($account_type == '4') {
                $table = 'agency';
            } else if ($account_type == '3') {
                $table = 'provider';
            } else if ($account_type == '2' || $account_type == '5') {
                $table = 'operator';
            } else if ($account_type == '6') {
                $table = 'sys';
            }

            $operator_id = session('operator_id');
            $role_id = session('role_id');
            $name = trim(I('name'));

            if ($role_id == 1) {
                if ($account_type == '2' || $account_type == '5' || $account_type == '6') {
                    $where = "name like '%$name%'";
                } else {
                    $where = "name like '%$name%'";
                }
            } else {
                if ($account_type == '2' || $account_type == '5' || $account_type == '6') {
                    $where = "name like '%$name%'";
                } else {
                    $where = "name like '%$name%' && operator_id = '{$operator_id}'";
                }
            }

            $pageInfo = Utils::pages($table, $where, C('PAGE_SET'), $order = " id desc ");

            $this->assign('account_type', $account_type);
            $this->info = $pageInfo['info'];
            $this->page = $pageInfo['page'];
        }

        if ($operator_id != 1 && $roleId != 1) {
            $this->redirect('Refund/_empty', array('id' => 128));
        }

        $_SESSION['refundCode'] = 400;
        $this->display();
    }


    public function saveRefund()
    {
        if (IS_POST && IS_AJAX) {
            $postCode = I('code');
            $code = session('refundCode');

            if ($code == $postCode) {
                session('refundCode', null);
                $account_type = I('account_type');

                if ($account_type == 4) {
                    $table = 'agency';
                    $entityType = Top::EntityTypeAgency;
                } else if ($account_type == 3) {
                    $table = 'provider';
                    $entityType = Top::EntityTypeProvider;
                } else if ($account_type == 2 || $account_type == 5) {
                    $table = 'operator';
                    $entityType = Top::EntityTypeOperator;
                } else if ($account_type == 6) {
                    $table = 'sys';
                    $entityType = Top::EntityTypeSystem;
                }

                $id = I('id');
                $info = M($table)->where("id = '{$id}'")->find();

                if ($account_type == 3) {
                    $where['operator_id'] = $info['operator_id'];
                    $where['provider_id'] = I('id');
                    $providerInfo = M('provider_account')->where($where)->find();
                    $data['account'] = $providerInfo['account'];
                } else {
                    $data['account'] = $info['account'];
                }

                if ($account_type == 6) {
                    $data['operator_id'] = 0;
                } else if ($account_type == 2 || $account_type == 5) {
                    $data['operator_id'] = $info['id'];
                } else {
                    $data['operator_id'] = $info['operator_id'];
                }

                $data['action'] = I('memo');
                $data['amount'] = Utils::getFen(I('amount'));
                $data['entity_type'] = $entityType;
                $data['account_type'] = I('account_type');
                $data['entity_name'] = $info['name'];
                $data['entity_id'] = I('id');
                $data['user_id'] = session('user_id');
                $data['user_name'] = session('name');
                $data['memo'] = I('memo');
                $data['proof'] = I('proof');
                $data['state'] = Top::ApplyStateWait;
                $data['create_time'] = date("Y-m-d H:i:s", time());

                $result = M('account_adjust')->add($data);

                if ($result) {
                    $data['status'] = 1;
                    $data['msg'] = '提交成功';
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '提交失败';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 3;
                $data['msg'] = '请勿重复提交';
                $this->ajaxReturn($data);
            }
        }
    }

    public function _empty()
    {
        $this->display();
    }

}
