<?php

namespace Admin\Controller;

use Common\Common\AdminController;
use Common\Utils;
use Common\Top;

class UserController extends AdminController
{
    public function ceshi(){
          $template = array(
                    'touser' => 'oK9HQs0mOPwomUc1Cftv1SgB4TQI',
                    'template_id' => Utils::topUpTemplateId(),

                    'data' => array(
                        'first' => array('value' => urlencode("亲,您的充值申请于 "  . " 未通过审核,充值失败"), 'color' => "#000"),
                        'keyword1' => array('value' => urlencode('sdfsdfsdf'), 'color' => "#000"),
                        'keyword2' => array('value' => urlencode(Utils::getYuan(25)), 'color' => '#000'),
                        'keyword3' => array('value' => urlencode('2015132156'), 'color' => '#000'),
                        'remark' => array('value' => urlencode("充值类型: 4"), 'color' => '#000'),
                    )
                );
                $this->send_tpl($template);
    }

    public function index()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id', 0, 'int');
            $state = I('state', 0, 'int');
            $info = M('user')->where("id = '{$id}'")->find();

            $logData['operator_id'] = session('operator_id');
            $logData['user_id'] = session('user_id');
            $logData['user_name'] = session('entity_name');

            $logData['memo'] = I('memo');
            $logData['create_time'] = date('Y-m-d H:i:s', time());

            if ($state == $info['state']) {
                $data['status'] = 1;
                $this->ajaxReturn($data);
            }

            if ($state == Top::StateEnabled) {
                $logData['action'] = $logData['user_name'] . "恢复了用户." . $info['name'] . "的用户状态";
            } else if ($state == Top::StateDisabled) {
                $logData['action'] = $logData['user_name'] . "禁用了用户." . $info['name'] . "的用户状态";
            }

            M('log_account_status')->add($logData);
            $result = M('user')->where("id = '{$id}'")->setField('state', $state);

            if ($result) {
                $data['status'] = 1;
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '不能重复提交';
                $this->ajaxReturn($data);
            }
        } else {
            $this->role_info = M('role')->where('entity_type=' . session('entity_type') . ' and entity_id=' . session('entity_id') . ' and is_admin=0')->select();

            $this->info = M()->table('top_user as u ,top_role as r')
                ->where('u.role_id=r.id and u.state != 4 and u.entity_type=r.entity_type and u.entity_type=' . session('entity_type') . ' and u.entity_id=' . session('entity_id'))
                ->field('u.id,u.is_admin,u.name,u.login_name,u.create_time,u.state,r.name as role_name')
                ->select();
            $this->display();
        }
    }


    public function add()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $data = array(
                'role_id' => I('modal_role'),
                'entity_type' => session('entity_type'),
                'entity_id' => session('entity_id'),
                'name' => I('name'),
                'is_admin' => '0',
                'login_name' => I('modal_login'),
                'create_time' => date('Y-m-d H:i:s', time()),
                'mobile' => I('mobile'),
                'state' => Top::StateEnabled,
            );

            $login_pwd = I('modal_pwd');
            $login_pwd2 = I('modal_pwd2');

            if ($login_pwd != '' || $login_pwd2 != '') {
                if ($login_pwd != $login_pwd2) {
                    $data['status'] = 0;
                    $data['msg'] = '两次密码不一致';
                    $this->ajaxReturn($data);
                }

                $where['login_name'] = I('modal_login');
                $userInfo = M('user')->where($where)->find();

                if (!empty($userInfo)) {
                    $data['status'] = 0;
                    $data['msg'] = '该用户已存在，请重新输入！';
                    $this->ajaxReturn($data);
                } else {
                    $data['login_pwd'] = Utils::getHashPwd($login_pwd);
                    $result = M('user')->add($data);

                    if ($result) {
                        $data['status'] = 1;
                        $data['msg'] = '添加成功！';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '系统繁忙，请重新添加！';
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $data['status'] = 0;
                $data['msg'] = '密码不能为空';
                $this->ajaxReturn($data);
            }
        }
    }


    public function edit()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $id = I('tid');
            if (I('name')) {
                $data = array(
                    'name' => I('name'),
                    'login_name' => I('modal_login'),
                    'mobile' => I('mobile'),
                    'role_id' => I('modal_role'),
                    'update_time' => date('Y-m-d H:i:s', time()),
                );

                $where['login_name'] = I('modal_login');
                $where['id'] = array('neq', $id);
                $userInfo = M('user')->where($where)->find();

                if (!empty($userInfo)) {
                    $data['status'] = 0;
                    $data['msg'] = '该用户已存在，请重新输入！';
                    $this->ajaxReturn($data);
                } else {
                    $result = M('user')->where("id = '{$id}'")->save($data);

                    if ($result) {
                        $data['status'] = 1;
                        $data['msg'] = '修改成功！';
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $data['msg'] = '修改失败！';
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $userInfo = M('user')->where('id=' . I('id'))->find();

                $roleName = M('role')->where('id=' . $userInfo['role_id'])->field('name')->find();
                $user = array(
                    'name' => $userInfo['name'],
                    'login_name' => $userInfo['login_name'],
                    'type' => $userInfo['role_id'],
                    'rolename' => $roleName['name'],
                    'mobile' => $userInfo['mobile'],
                );

                if ($userInfo) {
                    $data['status'] = 1;
                    $data['info'] = $user;
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '系统繁忙！';
                    $this->ajaxReturn($data);
                }
            }
        }
    }


    public function delete()
    {
        if (IS_POST && IS_AJAX) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $userIds = I('action_code');
            $data['state'] = Top::StateDeleted;

            foreach ($userIds as $id) {
                $id = Utils::getDecrypt($id);
                $info = M('user')->where("id = '{$id}'")->field('is_admin')->find();

                if ($info['is_admin'] == 1) {
                    $data['status'] = 0;
                    $data['msg'] = '系统用户不能删除！';
                    $this->ajaxReturn($data);
                } else {
                    $result = M('user')->where("id = '{$id}'")->save($data);
                }
            }

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '删除成功！';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '删除失败！';
                $this->ajaxReturn($data);
            }
        }
    }


    public function editPwd()
    {
        $where['name'] = session('name');
        $where['id'] = session('user_id');
        $where['entity_id'] = session('entity_id');
        $userInfo = M('user')->where($where)->find();
        $login_pwd = $userInfo['login_pwd'];

        if (IS_AJAX && IS_POST) {
            if ($this->warnStatus == '10404') {
                $data['status'] = 0;
                $data['msg'] = $this->warnInfo;
                $this->ajaxreturn($data);
            }

            $pwd = I('pwd');
            $pwd = Utils::getHashPwd($pwd);

            if ($pwd == $login_pwd) {
                $newpwd = I('newpwd');
                $confirm_password = I('password2');

                if ($newpwd != $confirm_password) {
                    $data['status'] = 3;
                    $data['msg'] = '两次密码不一致';
                    $this->ajaxReturn($data);
                } else {
                    $data['login_pwd'] = Utils::getHashPwd($newpwd);
                    $data['update_time'] = date("Y-m-d H:i:s", time());

                    M('user')->where($where)->save($data);
                    $data['status'] = 1;
                    $data['msg'] = '修改成功';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 0;
                $data['msg'] = '原密码错误';
                $this->ajaxReturn($data);
            }
        } else {
            $this->assign('userInfo', $userInfo);
        }

        $this->display();
    }


    /**
     * 供应商指定消息类型时读取姓名
     */
    public function getUserName()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $userInfo = M('user')->where($where)->find();
            $notifyMsgTypes = explode(",", $userInfo['notify_msg_types']);
            array_shift($notifyMsgTypes);
            array_pop($notifyMsgTypes);

            if ($userInfo) {
                $data['productMessage'] = $notifyMsgTypes[0];
                $data['transactionMessage'] = $notifyMsgTypes[1];
                $data['name'] = $userInfo['name'];
                $data['status'] = 1;
                $this->ajaxReturn($data);
            }
        }
    }


    public function saveNotifyType()
    {
        if (IS_POST && IS_AJAX) {
            $where['id'] = I('id');
            $productMessage = I('productMessage');
            $transactionMessage = I('transactionMessage');
            $data['notify_msg_types'] = "," . $productMessage . "," . $transactionMessage . ",";
            $result = M('user')->where($where)->save($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        }
    }

}
