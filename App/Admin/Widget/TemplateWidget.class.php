<?php

namespace Admin\Widget;

use Common\Common\AdminController;
use Common\Top;

class TemplateWidget extends AdminController
{


    public function top()
    {
        $role_info = D('role')->find(session('role_id'));
        $auth_ids = $role_info['auth_ids'];
        if (session('entity_type') == Top::EntityTypeSystem) {
            if (session('role_id') != 1 && session('user_id') != 1) {
                $where['level'] = 0;
                $where['id'] = array('in', $auth_ids);
            } else {
                $where['level'] = 0;
                $where['entity_type'] = Top::EntityTypeSystem;
            }
        } else {
            if (session('entity_type') == Top::EntityTypeOperator) {
                if (session('role_id') != Top::DefalutOperatorRoleId) {
                    $where['level'] = 0;
                    $where['id'] = array('in', $auth_ids);
                } else {
                    $where['level'] = 0;
                    $where['id'] = array('in', $auth_ids);
                    $where['entity_type'] = Top::EntityTypeSystem;
                }
            } elseif (session('entity_type') == Top::EntityTypeProvider) {
                $where['level'] = 0;
                $where['id'] = array('in', $auth_ids);
            }
        }
        $top_info = M('auth')->where($where)->order('order_num')->limit(8)->select();
      
        $edit_info = array();


        //role表下的auth_id
        $ids = $role_info['auth_ids'];

        if ($auth['level'] == 0) {
            $where1['level'] = 2;
            if (!empty($ids)) {
                $where1['id'] = array('in', $ids);
            }
        }
        //去重查询
        $cauth_info = M('auth')->where($where1)->group('parent_id')->select();
        //dump($cauth_info);
        foreach ($cauth_info as $key => $v) {
            $action[] = array(
                'controller' => $v['controller'],
                'action' => $v['action'],
            );
        }

        $entity_type = session('entity_type');
        if ($entity_type != 1) {
            foreach ($top_info as $k => $v) {
                $edit_info[] = array(
                    'id' => $v['id'],
                    'entity_type' => $v['entity_type'],
                    'name' => $v['name'],
                    'action' => 'lists',
                    'parent_id' => $v['parent_id'],
                    'controller' => $v['controller'],
                    'path' => $v['path'],
                    'level' => $v['level'],
                    'icon' => $v['icon'],
                );
            }
        } else {
            foreach ($top_info as $k => $v) {
                $edit_info[] = array(
                    'id' => $v['id'],
                    'entity_type' => $v['entity_type'],
                    'name' => $v['name'],
                    'action' => 'lists',
                    'parent_id' => $v['parent_id'],
                    'controller' => $v['controller'],
                    'path' => $v['path'],
                    'level' => $v['level'],
                    'icon' => $v['icon'],
                );
            }
        }
        $this->ip = get_client_ip();
        $this->user_name = session('user_name');
        $this->assign('pauth_info', $edit_info);
        $this->display('Template:top');
    }


    public function left()
    {
        $id = I('get.id'); //current
        $auth = M('auth')->field('parent_id,level,path')->find($id); //current
        $parent = M('auth')->where("level = 1 && parent_id = '{$id}'")->field('id')->select();

        $roleId['id'] = session('role_id');
        $roleInfo = M('role')->where($roleId)->field('auth_ids')->find();
        $ids = $roleInfo['auth_ids'];

        $arr = array_column($parent, 'id');
        $parent = implode(',', $arr);

        if ($auth['level'] == 0) {
            $where['level'] = 1;
            $where['parent_id'] = I('id');

            $where1['level'] = 2;
            $where1['parent_id'] = array("in", $parent);

            if (!empty($ids)) {
                $where1['id'] = array('in', $ids);
            }
        } else if ($auth['level'] == 2) {
            $path = $auth['path'];
            $arr_path = explode('-', $path);
            $where['level'] = 1;
            $level1 = $arr_path[0]; //7-77-85  :7
            $where['parent_id'] = $level1;
            //begin:
            //1. parent_id = level1
            $where1['level'] = 2;
            $where1['path'] = array('like', $level1 . '-%');
            if (!empty($ids)) {
                $where1['id'] = array('in', $ids);
            }
        }


        $cauth_info = M('auth')->where($where1)->order('order_num')->select();

        $path = array();
        foreach ($cauth_info as $key => $v) {
            $path[] = array(
                'path' => substr($v['path'], 0, strrpos($v['path'], '-')),
            );
        }

        $path = array_column($path, 'path');
        $path = implode(',', $path);

        $map['path'] = array('in', $path);
        $pauth_info = M('auth')->where($where)->where($map)->select();
       // dump($cauth_info);

        $this->assign('pauth_info', $pauth_info);
        $this->assign('cauth_info', $cauth_info);
        $this->display('Template:left');
    }


    public function bottom()
    {
        $this->display('Template:bottom');
    }



    public function capacity()
    {
        $this->display('Template:capacity');
    }


}

?>