<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>微信安全支付</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">订单支付</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo ($domain); ?>" class="home"></a>
</div>

<td class="wapper ucenter" style="overflow: hidden">
    <form method="post" action="<?php echo U('WxJsAPI/jsApiCall');?>">
        <input type='hidden' id='orderDetail' value="<?php echo U('WxJsAPI/jsApiCall');?>">
        <input type='hidden' id='flightId' name="flightId" value='<?php echo ($flightId); ?>'>
        <input type="hidden" id="orderNo" name="orderNo" value="<?php echo ($orderNo); ?>">
        <input type="hidden" id="flightOrderCode" name="flightOrderCode" value="<?php echo ($flightOrderCode); ?>">
        <input type="hidden" id="seatCode" name="seatCode" value="<?php echo ($seatCode); ?>">
        <input type="hidden" id="flightNo" name="flightNo" value="<?php echo ($flightNo); ?>">
        <input type="hidden" id="specialParPrice" name="specialParPrice" value="<?php echo ($specialParPrice); ?>">
        <input type="hidden" id="code" name="code" value="<?php echo (session('flightOrderCode')); ?>">
        <input type="hidden" id="openid" name="openid" value="<?php echo ($openid); ?>">
        <table class="table" style='width:100%'>
            <tr>
                <td class="text-center" colspan="5"><h4>订单详情</h4></td>
            </tr>
            <tr>
                <td class="text-left" colspan="5">航班信息: &nbsp; <?php echo (nameToAirLine(substr($flightNo,0,2))); ?> <?php echo ($flightNo); ?><span
                        style='color:#EC975B'>|</span><?php echo ($planeModel); ?>
                </td>
            </tr>
            <tr>
                <th class="text-left" width="20%">乘客</th>
                <th class="text-left" width='15%'>类型</th>
                <th class="text-left" width='29%'>结算价</th>
                <th class="text-left" width='20%'>燃油</th>
                <th class="text-left" width='20%'>基建费</th>
            </tr>
            <?php if(is_array($orderList)): foreach($orderList as $key=>$vo): ?><tr>
                    <td class="text-left" width='15%'><?php echo ($vo["passenger_name"]); ?></td>
                    <td class="text-left" width='20%'>
                        <?php if($vo['passenger_type'] == 0): ?>成人
                            <?php else: ?>
                            儿童<?php endif; ?>
                    </td>
                    <td class="text-left" width='20%'>
                        <span class='flight-text-c2'>&yen; <?php echo ($vo["par_price"]); ?></span>
                    </td>
                    <td class="text-left" width='20%'>
                        <span class='flight-text-c2'>&yen;<?php echo ($vo["fuel_tax"]); ?></span>
                    </td>
                    <td class="text-left" width='20%'>
                        <?php if($vo['passenger_type'] == 0): ?><span class='flight-text-c2'>&yen;<?php echo ($vo["airport_tax"]); ?></span>
                            <?php else: ?>
                            <span class='flight-text-c2'>&yen;0.00</span><?php endif; ?>
                    </td>

                </tr><?php endforeach; endif; ?>
            <tr>
                <td class="text-left" colspan="5">起飞日期: &nbsp;<?php echo ($depDate); ?></td>
            </tr>
            <tr>
                <td class="text-left" colspan="5">起降机场: &nbsp;
                    <?php echo (returnTime($depTime)); ?>
                    <?php if($depCode == 'NAY'): ?>南苑机场
                        <?php elseif($depCode == 'PVG'): ?>
                        浦东机场
                        <?php else: ?>
                        <?php echo (nameToAirport($depCode)); endif; ?>
                    <?php if(($orgJetquay == 'Array') or ($orgJetquay == '--')): else: ?>
                        <?php echo ($orgJetquay); endif; ?>
                    --

                    <?php echo (returnTime($arrTime)); ?>
                    <?php if($arrCode == 'NAY'): ?>南苑机场
                        <?php elseif($arrCode == 'PVG'): ?>
                        浦东机场
                        <?php else: ?>
                        <?php echo (nameToAirport($arrCode)); endif; ?>
                    <?php if(($dstJetquay == 'Array') or ($dstJetquay == '--')): else: ?>
                        <?php echo ($dstJetquay); endif; ?>
                </td>
            </tr>
            <tr>
                <td class="text-left" colspan="5"><span style='margin-left:10px;'>联系人:</span> &nbsp;<?php echo ($linkMan); ?></td>
            </tr>
            <tr>
                <td class="text-left" colspan="5">联系电话: &nbsp;<?php echo ($linkPhone); ?></td>
            </tr>
            <tr>
                <td class="text-left" colspan="5">
                    订单总额: &nbsp;
                    <span class="flight-text-c2">&yen;<span class='priceTotal'
                                                            style="font-size:18px;"><?php echo ($priceTotal); ?></span></span>
                </td>
            </tr>
            <tr>
                <?php if($orderFlightInfo['state'] == 14): ?><td colspan="5" class="text-right showButton">
                    <span class="payContent">
                        <span style="font-size: 20px">您确定要支付吗</span>
                        <button class='btn btn-danger btn-sm doSubmitVisitorFlightOrder' type="button">确定
                        </button>
                    </span>
                </td>
                    <?php elseif($orderFlightInfo['state'] == 2): ?>
                    <td colspan='5' class='text-right'>
                        <a href="<?php echo ($redirectUrl); ?>">
                            <input type='button' class='btn btn-info' value="我的机票"/>
                        </a>
                    </td><?php endif; ?>
                <td colspan="5" class="text-right showLoad" style="display:none">
                    <img src="http://cdn.lydlr.com/public/images/load.gif"> <span style="color: #c6c6c6">订单提交中</span>
                </td>

                <td colspan='5' class='text-right showJumpButton' style="display: none">
                    <a href="<?php echo ($redirectUrl); ?>">
                        <input type='button' class='btn btn-info' value="我的机票"/>
                    </a>
                </td>


            </tr>
        </table>
    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，您最多可以添加6位乘机人</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitWarning" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default doCloseWarning" data-dismiss="modal"
                            aria-label="Close">取消
                    </button>
                </div>
            </div>
        </div>
    </div>
    <p></p>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>

    <script>
        $(".doSubmitVisitorFlightOrder").one("click", function() {
            var url = $("#orderDetail").val();
            var fid = $("#flightId").val();
            var openid = $("#openid").val();
            var total = <?php echo $total ?>;
            var creditBalance = <?php echo $creditBalance ?>;

            if(creditBalance < total){
                $("#myModalmob").modal({backdrop: 'static', keyboard: false});
                $("#myModalmob").modal();
                $(".tipsBox").html('系统额度受限,请联系系统管理员');
            }

            $(".doSubmitVisitorFlightOrder").attr("disabled", true);
            $(".showButton").hide();
            $(".showLoad").show();

            $.post(url, {fid:fid,openid:openid},function($data){
                //alert($data['status']);
                if($data['status'] == 2){
                    $(".showLoad").hide();
                    $(".showButton").show();
                    $(".doSubmitVisitorFlightOrder").attr("disabled", false);
                    $("#myModalmob").modal({backdrop: 'static', keyboard: false});
                    $("#myModalmob").modal();
                    $(".tipsBox").html('请勿重复支付');
                }

                if($data['status'] = 1){
                    if (typeof WeixinJSBridge == "undefined"){
                        if( document.addEventListener ){
                            document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
                        }else if (document.attachEvent){
                            document.attachEvent('WeixinJSBridgeReady', jsApiCall);
                            document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
                        }
                    }else{
                        //jsApiCall();
                        WeixinJSBridge.invoke(
                                'getBrandWCPayRequest',
                                {"appId":$data['appId'],"timeStamp":$data['timeStamp'],"nonceStr":$data['nonceStr'],"package":$data['package'],"signType":$data['signType'],"paySign":$data['paySign']},
                                function(res){
                                    WeixinJSBridge.log(res.err_msg);
                                    //alert(res.err_code+res.err_desc+res.err_msg);
                                    if(res.err_msg == "get_brand_wcpay_request:ok"){
                                        $(".showLoad").hide();
                                        $(".showJumpButton").show();
                                    }
                                }
                        );
                    }
                }
            })

        });

    </script>
    </div>
</body>
</html>