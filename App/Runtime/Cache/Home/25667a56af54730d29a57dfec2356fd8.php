<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>提交订单</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">提交订单</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <form method="post" action="<?php echo U('Agency/orderInfo',array('id'=>$list['id']));?>">
        <input type="hidden" id="code" value="<?php echo (session('orderInfoCode')); ?>">
        <input type="hidden" name="id" id="id" value="<?php echo ($list["id"]); ?>">
        <input type="hidden" id="keyid" value="<?php echo (setEncrypt($list["id"])); ?>">
        <input id="changeOrderMoney" type="hidden" value="<?php echo U('Agency/orderMoney');?>"/>
        <input id="orderInfo" type="hidden" value="<?php echo U('Agency/orderInfo',array('id'=>$list['id']));?>"/>
        <input id="myorder" type="hidden" value="<?php echo U('Personal/myorder');?>"/>
        <input type="hidden" id="coupon" value="<?php echo ($coupon); ?>">
        <input type="hidden" id="account_state" value="<?php echo ($userList["account_state"]); ?>">

        <div class="module ckOrder">
            <div class="top">
                <h2 class="ht"><a href="#"><?php echo ($list["name"]); ?></a></h2>

                <div class="it clearfix">
                    <span class="lb pull-left">成人<em>（常规）</em></span>
                        <span class="price pull-right">同行价：
                            <dfn>&yen;</dfn>
                            <span class="org priceAdultList">
                                <?php echo ($list["price_adult_agency"]); ?>
                            </span>/人
                        </span>
                </div>
                <div class="it clearfix">
                    <span class="lb pull-left">儿童<em>（常规）</em></span>
                        <span class="price pull-right">同行价：
                            <dfn>&yen;</dfn>
                            <span class="org priceChildList">
                                <?php echo ($list["price_child_agency"]); ?>
                            </span>/人
                        </span>
                </div>
            </div>
            <div class="wbox bgwhite">
                <p class="t">确认出团日期</p>

                <div class="form-inline">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">出团日期</div>
                            <select class="form-control" name="start_time" id="startTime" style="background: none">
                                <?php if(is_array($tour_date)): foreach($tour_date as $key=>$vo): ?><option value="<?php echo ($vo["start_time"]); ?>"><?php echo (substr($vo["start_time"],0,10)); ?></option><?php endforeach; endif; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wbox bgpur">
                <p class="t">选择出行人数及类型</p>

                <div class="selnum">
                    <div class="item">
                        <label>成人<em>（常规）</em></label>

                        <div class="num adult clearfix">
                            <a class="reduce adultReduce" href="javascript:void(0);">－</a>
                            <input type="text" class="txt adultTotal" id="adult" value="1" name="client_adult_count"/>
                            <a class="add adultAdd" href="javascript:void(0);">+</a>
                        </div>
                    </div>
                    <div class="item">
                        <label>儿童<em>（常规）</em></label>

                        <div class="num child clearfix">
                            <a class="reduce childReduce" href="javascript:void(0);">－</a>
                            <input type="text" id="child" class="txt childTotal" value="0" name="client_child_count"/>
                            <a class="add childAdd" href="javascript:void(0);">+</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wbox bgwhite" style="margin-top:0px; border-top:none;">
                <p class="t">可选资源</p>

                <div class="selnum">
                    <div class="item" style="padding-left:70px;">
                        <label style="width:70px; top:12px;">单房差</label>

                        <div class="num hotel clearfix">
                            <a class="reduce hotelReduce" href="javascript:void(0);">－</a>
                            <input type="text" class="txt hotelTotal" id="hotel" value="0" name="hotel_count"/>
                            <a class="add hotelAdd" href="javascript:void(0);">+</a>
                            <span style="color:#fc8b2e; margin-left:10px; margin-top:2px;" class="pull-left">&yen;<span
                                    class="org priceHotelList"><?php echo ($list["price_hotel_agency"]); ?></span>/份 (每晚)</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wbox bgpur">
                <p class="t">预定信息</p>

                <div class="form-inline preorder">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">联系人</div>
                            <input type="text" id="contacts" class="form-control nb" name="contacts" placeholder="必填"
                                   value="<?php echo ($userList["name"]); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-inline preorder">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">手机</div>
                            <input type="text" id="phone" class="form-control nb" placeholder="输入11位手机号码"
                                   name="contact_phone" value="<?php echo ($userList["mobile"]); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-inline preorder last">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">订单备注</div>
                            <input type="text" id="remark" class="form-control nb" name="memo">
                        </div>
                    </div>
                </div>
            </div>
            <div class="wbox bgwhite">
                <?php if($_SESSION['isLogin']== yes): if(($codeName == '1') && ($policyInfo['state'] == 0) && ($nowDate >= $startDate) && ($nowDate <= $endDate)): ?><div class="wbox bgwhite"
                             style="margin-top:0px; border-top:none;border-bottom: 1px dashed #ddd">
                            <p class="t">优惠券</p>

                            <div class="selnum">
                                <div class="item" style="padding-left:70px;">
                                    <label style="width:70px;margin-top: -8px;">
                                        <input type="checkbox" name="coupon" class="form-control coupon" value="1">
                                    </label>

                                    <div class="num hotel clearfix">
                                        <span style="color:#fc8b2e; margin-left:10px; margin-top:2px;"
                                              class="pull-left">500元代金券 (您还有 <?php echo ($couponNum); ?> 张可用,出行人数 2 人以上可使用1张代金券,每次仅限使用 1 张)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>
                        <?php elseif(($codeName == '0') && ($policyInfo['state'] == 0) && ($nowDate >= $startDate) && ($nowDate <= $endDate)): ?>
                        <div class="wbox bgwhite"
                             style="margin-top:0px; border-top:none;border-bottom: 1px dashed #ddd">
                            <p class="t">优惠券</p>

                            <div class="selnum">
                                <div class="item" style="padding-left:0px;">
                                    <div class="num hotel clearfix">
                                        <span style="color:#fc8b2e; margin-left:10px; margin-top:2px;"
                                              class="pull-left">暂无代金券</span>
                                    </div>
                                </div>
                            </div>
                        </div><?php endif; endif; ?>

                <div class="ordertotal clearfix">
                    <span class="pull-left">订单总额：</span>
                    <span class="price pull-right">&yen;<span id="total"></span></span>
                </div>
                <div class="pad">
                    <input type="button" class="btn btn-primary btn-lg btn-block proofAgain" id="doSubmit"
                           value="提交订单"/>
                </div>
            </div>
        </div>
    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalDel" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>请将信息填写完整</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>提交成功</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doOrderInfoJump" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>