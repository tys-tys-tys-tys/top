<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo ($list['name']); ?></title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>
<body>
<div class="actNav">
    <div class="doRefresh">产品详情</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>

<div class="wapper detailPage">
    <form method="post" action="<?php echo U('Agency/details',array('id'=>$list['id']));?>">
        <input type="hidden" name="id" id="id" value="<?php echo ($list["id"]); ?>">
        <input id="doRecommend" type="hidden" value="<?php echo U('Agency/recommend');?>"/>

        <div class="module">
            <h1 class="title" style="padding:10px;overflow:visible;height:auto;line-height:25px;"><?php echo ($list["name"]); ?></h1>
            <div class="price clearfix">
                  <span class="w pull-left">
                        价　格：<dfn>&yen;<?php echo (getYuan($list["price_agency"])); ?></dfn>起
                  </span>
            </div>
        </div>

        <div class="module gallery">
            <ul class="act clearfix">
                <li class="icon i-2">
                    <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo ($qq); ?>&site=qq&menu=yes"><i></i><span
                            class="w">在线沟通</span></a>
                </li>
            </ul>
        </div>

        <div class="module blockItem">
            <div class="t" data-event-trigger><i class="icon i-2"></i>产品特色<span class="arrow"></span></div>
            <div class="pad" style="margin-left: 20px">
                <p><?php echo ($list["description"]); ?></p>
            </div>
        </div>

        <div class="detail-bottom clearfix">

            <?php if($_SESSION['isLogin']== yes): ?><a href="tel:<?php echo ($contactPhone); ?>"><i></i>电话咨询</a>
                <?php else: ?>
                <a href="tel:<?php echo ($mobile); ?>"><i></i>电话咨询</a><?php endif; ?>

            <?php if($_SESSION['isLogin']== yes): ?><a href="<?php echo U('Product/orderInfo',array('id'=>$list['id']));?>" class="yd">
                    立即预订
                </a>
                <?php else: ?>
                <a class="yd"></a><?php endif; ?>
        </div>
        <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">提示</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p class="tipsBox"><i class="suc"></i>推荐成功</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade popWin bs-example-modal-sm" id="myModalAuc" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">提示</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p class="tipsBox"><i class="suc"></i>您已推荐该产品</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        </footer>
    </form>
</div>

<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });

    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "<?php echo ($list["name"]); ?>",
            title: "<?php echo ($list["name"]); ?>",
            link: "<?php echo ($domain); ?>",
            imgUrl: '<?php echo ($bgImg); ?>',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
</body>
</html>