<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>查看详情</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">查看详情</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <div class="module">
        <div id="content">
            <ul class="tradeList">
                <li>
                    <i class='icon-flight'></i>

                    <p class='price'><?php echo ($flightNo); ?> | <?php echo ($list["0"]["plane_model"]); ?></p>

                    <p class='price'>交易号 : <span class='flight-text-a5'><?php echo ($list["0"]["sequence_no"]); ?></span></p>

                    <p class='price'>航程信息：<span class='flight-text-a5'>
                        <?php echo (codeToAirport($list["0"]["dep_code"])); echo (nameToAirport($list["0"]["dep_code"])); ?>
                        <?php if($flightInfo['org_jetquay'] != 'Array'): echo ($flightInfo["org_jetquay"]); endif; ?>
                        -
                        <?php echo (codeToAirport($list["0"]["arr_code"])); echo (nameToAirport($list["0"]["arr_code"])); ?>
                        <?php if($flightInfo['dst_jetquay'] != 'Array'): echo ($flightInfo["dst_jetquay"]); endif; ?>
                    </span>
                    </p>

                    <p class='price'>起降时间：<span class='flight-text-a5'><?php echo ($list["0"]["dep_date"]); ?> <?php echo (returnTime($list["0"]["dep_time"])); ?> -- <?php echo ($list["0"]["dep_date"]); ?> <?php echo (returnTime($list["0"]["arr_time"])); ?></span>
                    </p>

                    <p class='price'>订单时间：<span class='flight-text-a5'><?php echo ($list["0"]["create_time"]); ?></span></p>

                    <p class='price'>机票状态：<span style='color:#f08519'>
                        <?php if($list[0]['state'] == 1): ?>未提交
                            <?php elseif($list[0]['state'] == 2): ?>
                            已付款,待出票
                            <?php elseif($list[0]['state'] == 3): ?>
                            已出票
                            <?php elseif($list[0]['state'] == 4): ?>
                            已取消
                            <?php elseif($list[0]['state'] == 5): ?>
                            已退票,退票成功
                            <?php elseif($list[0]['state'] == 6): ?>
                            出票失败
                            <?php elseif($list[0]['state'] == 7): ?>
                            预定失败
                            <?php elseif($list[0]['state'] == 8): ?>
                            退票失败
                            <?php elseif($list[0]['state'] == 9): ?>
                            退票中,待退款
                            <?php elseif($list[0]['state'] == 10): ?>
                            有取消
                            <?php elseif($list[0]['state'] == 11): ?>
                            有退票
                            <?php elseif($list[0]['state'] == 12): ?>
                            已删除
                            <?php elseif($list[0]['state'] == 14): ?>
                            已预定,待付款
                            <?php elseif($list[0]['state'] == 15): ?>
                            已预定,付款中<?php endif; ?>
                    </span></p>
                    <table class="table table-bordered">
                        <tr>
                            <td>订单结算价</td>
                            <td>订单总价</td>
                            <td>订单返佣</td>
                        </tr>
                        <tr>
                            <td><?php echo (getYuan($flightInfo["price_total"])); ?></td>
                            <td><?php echo (getYuan($orderTotal)); ?></td>
                            <td><?php echo (getYuan($orderTotal - $flightInfo['price_total'])); ?></td>
                        </tr>
                    </table>

                    <table class="table table-bordered">
                        <tr>
                            <td>单人结算价</td>
                            <td>单人总价</td>
                            <td>单人返佣</td>
                        </tr>
                        <tr>
                            <td><?php echo (getYuan($list[0]['settle_price'] + $list[0]['airport_tax'])); ?></td>
                            <td><?php echo (getYuan($list[0]['par_price'])); ?> + <?php echo (getYuan($list[0]['airport_tax'])); ?> =
                                <?php echo (getYuan($list[0]['par_price'] + $list[0]['airport_tax'])); ?>
                            </td>
                            <td><?php echo (getYuan($list[0]['par_price'] - $list[0]['settle_price'])); ?></td>
                        </tr>
                    </table>
                </li>

                <?php echo ($html); ?>
            </ul>
        </div>

    </div>
    <div style="margin-top: 10px;margin-bottom: 10px;margin-right: 5px;float:right">
        <input type="hidden" id="orderCancel" value="<?php echo U('Flight/orderCancel');?>">
        <input type="hidden" id="id" value="<?php echo ($flightInfo["id"]); ?>">
        <?php if($list[0]['state'] == 14): ?><a href="javascript:void(0)" class="orderCancel" style="margin-right: 10px;">
                <input type='button' class='btn btn-danger' value='取消订单'>
            </a><?php endif; ?>

        <?php if(($depDate > $date) and ($list[0]['state'] == 14)): ?><a href="<?php echo U('Flight/orderDetail', array('id' => $id));?>">
                <input type='button' class='btn btn-success' value='支付订单'>
            </a><?php endif; ?>

        <?php if($list[0]['state'] == 1): ?><a href="<?php echo U('Flight/deleteOrder', array('id' => $id));?>" style="margin-left: 10px;">
                <input type='button' class='btn btn-danger' value='删除订单'>
            </a><?php endif; ?>
    </div>
</div>

<footer class="footer" style="margin-top:55px;">
    <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


</footer>

<div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><span class="suc"></span></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认
                </button>
            </div>
        </div>
    </div>
</div>

</body>
</html>