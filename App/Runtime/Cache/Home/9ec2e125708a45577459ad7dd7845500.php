<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>产品详情</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <link href="http://cdn.lydlr.com/public/css/ticket.css?v=3" rel="stylesheet">
    <style>
        .ticket-bottom a i {
            background: rgba(0, 0, 0, 0) url("http://cdn.lydlr.com/public/kangtai/images/detail-icon.png") no-repeat scroll 0 -392px / 40px auto;
            display: inline-block;
            height: 26px;
            margin-right: 5px;
            position: relative;
            top: 8px;
            width: 25px;
        }
    </style>
    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>
<body>
<div class="actNav">
    <div class="doRefresh">产品详情</div>
    <input type="hidden" id="ticketUrl" value="<?php echo U('Ticket/index');?>">
    <input type="hidden" class="regionCode" value="<?php echo ($ticketList["0"]["code"]); ?>">
    <a href="javascript:void(0);" class="back" onclick="self.location=document.referrer;"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>

<div class="wapper detailPage">
    <img class='lazy headImg' data-original="<?php echo (getPicPath($ticketList["0"]["imgs"])); ?>"/>

    <form method="post" action="">
        <div class="module">
            <h1 class="title" style="padding:10px;overflow:visible;height:auto;line-height:25px;">
                <b><?php echo ($ticketList["0"]["name"]); ?></b>
            </h1>

            <!--<div class="price clearfix" style="border-top:0px;border-bottom: 0px">
                  <span class="w pull-left">
                        开放时间：09:00 - 17:00
                  </span>
            </div>-->

            <div class="price clearfix" style="border-top:0px;border-bottom: 0px">
                <span class="w pull-left">
                    景点地址：<?php echo ($ticketList["0"]["address"]); ?>
                </span>
            </div>

            <div style="height:10px;background:#EFF0F0;margin-top:15px;"></div>
        </div>

        <!--<ul id="myTab" class="nav nav-tabs" style='background:white'>-->
        <!--<li class="active text-center" style='width:50%'><a href="#home" data-toggle="tab">-->
        <!--购票</a></li>-->
        <!--<li class='text-center' style='width:50%'><a href="#ios" data-toggle="tab">详情</a></li>-->
        <!--</ul>-->

        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade in active" id="home">
                <div class="module blockItem">
                    <div class="t" data-event-trigger style='border-bottom:1px solid #D8D8D8'><i class="icon i-3"></i>门票<span
                            class="arrow"></span></div>
                    <div class="">
                        <div class="box">
                            <div style='margin-left:20px;'>
                                <?php if(is_array($ticketList)): foreach($ticketList as $key=>$vo): ?><p style='height:30px;line-height:20px;font-size:14px;margin-top:5px;'><b><a
                                            href="<?php echo U('Ticket/showFeeInclude',array('id' => $vo['id']));?>"><?php echo ($vo["product_name"]); ?></a></b>
                                    </p>

                                    <p style='height:50px;line-height:50px;'><dfn
                                            style='color:#fc810c;font-size:20px;'>&yen;
                                        <?php if($_SESSION['isLogin']== 'yes'): echo (getYuan($vo["sell_price"])); ?> - <?php echo (getYuan($vo["settle_price"])); ?> = <?php echo (getYuan($vo['sell_price'] - $vo['settle_price'])); ?>
                                            <?php else: ?>
                                            <?php echo (getYuan($vo["sell_price"])); endif; ?>
                                    </dfn>
                                    </p>
                                    <p style="height:40px;border-bottom: 1px dashed #ddd;margin-top: -10px;">

                                        <?php if($_SESSION['isLogin']== 'yes'): ?><a href="<?php echo U('Ticket/showTicketInfo',array('id' => $vo['id']));?>">
                                                <input type='button' class='btn btn-warning'
                                                       style='float:right;margin-right: 5px' value='立即预订'>
                                            </a>
                                            <?php else: ?>
                                            <a href="tel:<?php echo ($userInfo["mobile"]); ?>"
                                               style='color:#2DC2C7;float:right;margin-right:10px;'>
                                                <i style="background: rgba(0, 0, 0, 0) url('http://cdn.lydlr.com/public/kangtai/images/detail-icon.png') no-repeat scroll 0 -392px / 40px auto;display: inline-block;height: 26px;margin-right: 5px;position: relative;top: 8px;width: 25px;"></i>电话咨询</a><?php endif; ?>
                                    </p><?php endforeach; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="module blockItem">
                    <div class="t" data-event-trigger><i class="icon i-4"></i>预定须知<span class="arrow"></span></div>
                    <div class="con pad">
                        <p>★ 下单后 <?php echo ($ticketList["0"]["auto_cancel_time"]); ?> 分钟不支付自动取消</p>
                        <p>★ 预订限制：
                            <?php if($ticketList['0']['book_advance_day'] == '0'): ?>当天
                                <?php else: ?>
                                <?php echo ($ticketList["0"]["book_advance_day"]); ?> 天<?php endif; ?>
                        </p>
                        <p>★ 最大购买量： <?php echo ($ticketList["0"]["buy_max_num"]); ?> 张</p>
                        <p>★ 最小购买量： <?php echo ($ticketList["0"]["buy_min_num"]); ?> 张</p>
                        <p>★ 是否支持退款：
                            <?php if($ticketList['0']['can_refund'] == 'true'): ?>支持
                                <?php else: ?>
                                不支持<?php endif; ?>
                        </p>
                        <p>★ 是否实名制：
                            <?php if($ticketList['0']['is_real_name'] == 'true'): ?>是
                                <?php else: ?>
                                否<?php endif; ?>
                        </p>
                        <p>★ 是否需要游客证：
                            <?php if($ticketList['0']['credentials_required'] == 'true'): ?>需要,
                                <?php if($ticketList[0]['certificate_type'] == 1): ?>身份证
                                <?php elseif($ticketList[0]['certificate_type'] == 2): ?>
                                    导游证
                                <?php elseif($ticketList[0]['certificate_type'] == 4): ?>
                                    学生证
                                <?php elseif($ticketList[0]['certificate_type'] == 8): ?>
                                    军官证
                                <?php elseif($ticketList[0]['certificate_type'] == 16): ?>
                                    老年证
                                <?php elseif($ticketList[0]['certificate_type'] == 32): ?>
                                    出生证
                                <?php elseif($ticketList[0]['certificate_type'] == 64): ?>
                                    驾驶证
                                <?php elseif($ticketList[0]['certificate_type'] == 128): ?>
                                    其他<?php endif; ?>
                                <?php else: ?>
                                不需要<?php endif; ?>
                        </p>
                        <p>★ 游客购买日起 <?php echo ($ticketList["0"]["days_after_use_date_valid"]); ?> 天内有效</p>
                        <p>★ 自出行之日起 <?php echo ($ticketList["0"]["period_validity_of_refund"]); ?> 天内可退票</p>
                        <p>★ 退票费: &yen;<dfn><?php echo ($ticketList["0"]["refund_fee"]); ?></dfn> 元</p>
                        <p>★ 退票说明: <?php echo ($ticketList["0"]["refund_info"]); ?></p>
                        <p>★ 使用说明: <?php echo ($ticketList["0"]["remind"]); ?></p>
                    </div>
                </div>
                <div class="module blockItem">
                    <div class="t" data-event-trigger><i class="icon i-2"></i>退票说明<span class="arrow"></span></div>
                    <div class="con pad">
                        <p>★ 是否支持退票：
                            <?php if($ticketList['0']['can_refund'] == 'true'): ?>支持
                                <?php else: ?>
                                不支持<?php endif; ?>
                        </p>
                        <p>★ 退票说明: <?php echo ($ticketList["0"]["refund_info"]); ?></p>
                    </div>
                </div>
                <?php if($_SESSION['isLogin']== 'yes'): ?><div class="module blockItem">
                        <div class="t" data-event-trigger><i class="icon i-21"></i>景点电话<span class="arrow"></span></div>
                        <div class="con pad">
                            <span style='color:#2DC2C7;margin-right:10px;'>电话咨询: <?php echo ($ticketList["0"]["phone"]); ?></span>
                        </div>
                    </div><?php endif; ?>
            </div>

        </div>

        <footer class="footer">
            <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        </footer>
    </form>
</div>
<script src="/Public/js/ticket.js?v=1"></script>

<script type="text/javascript">
    $(".headImg").addClass("carousel-inner img-responsive img-rounded");

    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "<?php echo ($ticketList["0"]["SightName"]); ?>",
            title: "<?php echo ($ticketList["0"]["SightName"]); ?>",
            link: "<?php echo ($domain); ?>",
            imgUrl: '<?php echo ($headPic); ?>',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
</body>
</html>