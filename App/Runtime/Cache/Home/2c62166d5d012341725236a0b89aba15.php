<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>提交订单</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<body>
<div class="actNav">
    <!-- 查看订单 -->
    <div class="doRefresh">提交订单</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <form method="post" action="<?php echo U('Ticket/ticketOrderCreate');?>">
        <input type='hidden' class='SettlementPrice' value="<?php echo (getYuan($info["settle_price"])); ?>">
        <input type="hidden" class="CertificateType" value="<?php echo ($info["certificate_type"]); ?>">
        <input type="hidden" class="CredentialsRequired" value="<?php echo ($info["credentials_required"]); ?>">
        <input type="hidden" name="id" value="<?php echo ($info["id"]); ?>">
        <input type="hidden" name="ticketOrderCode" value="<?php echo (session('ticketOrderCode')); ?>">

        <div class="module ckOrder">
            <div class="top">
                <h2 class="ht">
                    <a href="#">门票名称：<?php echo ($info["name"]); ?></a>
                    <span style='color:#FC810C;float:right'>&yen;<dfn><?php echo (getYuan($info["settle_price"])); ?></dfn>/张</span>
                </h2>
            </div>

            <div class="wbox bgwhite">
                <div class="form-inline">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">出游日期</div>
                            <input class="form-control timestart" name="start_time" readonly id="datetimepicker"
                                   size="16" type="text" style="width:100%;float:left;height:40px;"
                                   value="<?php echo date('Y-m-d', time());?>">
                        </div>
                    </div>
                </div>
            </div>

            <?php if($info['credentials_required'] == 'false'): ?><div class="wbox bgwhite" style="margin-top:0px; border-top:none;">
                    <div class="selnum">
                        <div class="item" style="padding-left:70px;">
                            <label style="width:70px; top:12px;">购买数量</label>

                            <div class="num ticket clearfix">
                                <!--真实姓名的判断-->
                                <a class="reduce ticketReduce" href="javascript:void(0);">－</a>
                                <input type="text" class="txt ticketNumTotal" id="ticketNumTotal" name="ticket_num"
                                       value="1"/>
                                <a class="add ticketAdd" href="javascript:void(0);">+</a>
                            </div>
                        </div>
                    </div>
                </div><?php endif; ?>


            <?php if($info['is_real_name'] == 'true'): ?><div class="form-inline preorder" style="padding-top:5px;padding-left:10px;">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">游客姓名</div>
                            <input type="text" class="form-control nb" name="real_name1"
                                   placeholder="请输入游客姓名"/>
                        </div>
                    </div>
                </div><?php endif; ?>

            <div class="wbox bgpur">
                <p class="t">取票人信息</p>

                <div class="form-inline preorder">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">姓名</div>
                            <input type="text" id="contacts" class="form-control nb" name="contact_person"
                                   placeholder="请输入取票人的姓名" value="<?php echo ($userList["name"]); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-inline preorder">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">手机</div>
                            <input type="text" id="phone" class="form-control nb" placeholder="输入11位手机号码"
                                   name="contact_phone" value="<?php echo ($userList["mobile"]); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="form-inline preorder last">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">备注</div>
                            <input type="text" id="remark" class="form-control nb" name="memo">
                        </div>
                    </div>
                </div>
            </div>

            <?php if($info['credentials_required'] == 'true'): ?><div class="wbox bgpur">
                    <p class="t">出游人信息</p>

                    <div class="filterNum">
                        <div class="div1">
                            <p class="t">游客1</p>

                            <div class="form-inline preorder">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon pt">游客姓名</div>
                                        <input type="text" id="contacts" class="form-control nb" name="real_name1"
                                               placeholder="请输入游客姓名"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-inline preorder">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon pt">证件类型</div>
                                        <?php if($info['certificate_type'] == '1'): ?><input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='身份证' disabled/>
                                            <?php elseif($info['certificate_type'] == '2'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='导游张'/>
                                            <?php elseif($info['certificate_type'] == '4'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='学生证'/>
                                            <?php elseif($info['certificate_type'] == '8'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='军官证'/>
                                            <?php elseif($info['certificate_type'] == '16'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='老年证'/>
                                            <?php elseif($info['certificate_type'] == '32'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='出生证'/>
                                            <?php elseif($info['certificate_type'] == '64'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='驾驶证'/>
                                            <?php elseif($info['certificate_type'] == '128'): ?>
                                            <input type="text" id="contacts" class="form-control nb"
                                                   name="identity_type1" value='其他'/><?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-inline preorder last">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon pt">证件号码</div>
                                        <input type="text" id="remark" class="form-control nb" name="identity_no1"
                                               placeholder="请输入游客的证件号码">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>&nbsp;</p>

                        <div class="flight-list-border-dashed-b2"></div>
                        <p>&nbsp;</p>
                    </div>


                    <div class="row">
                        <div class="col-xs-11 text-right">
                            <input type="button" class="btn btn-success realNameAdd" value="添加游客">
                        </div>
                    </div>

                </div><?php endif; ?>
            <!--
            <div class="wbox bgpur">
                <div class="form-inline preorder">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">取票方式</div>
                            <input type="text" id="contacts" class="form-control nb" name="contacts" placeholder="门票自取" value="<?php echo ($userList["name"]); ?>"/>
                        </div>
                    </div>
                </div>

                <div class="form-inline preorder last">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon pt">取票地址</div>
                            <input type="text" id="remark" class="form-control nb" name="memo" value="雪场销售处">
                        </div>
                    </div>
                </div>
            </div>
            -->

            <div class="wbox bgwhite">
                <div class="ordertotal clearfix">
                    <span class="pull-left">订单总额：</span>
                    <span class="price pull-right">&yen;<span id="TicketTotal"></span></span>
                </div>
                <div class="pad">
                    <input type="submit" class="btn btn-primary btn-lg btn-block submitTicketOrder" value="提交订单"/>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        <script src="/Public/js/ticket.js?v=9"></script>
    </footer>
</div>
<script>
    $(function () {
        $('#datetimepicker').datetimepicker({
            minView: "month", //选择日期后，不会再跳转去选择时分秒
            format: "yyyy-mm-dd", //选择日期后，文本框显示的日期格式
            language: 'zh-CN', //汉化
            autoclose: true //选择日期后自动关闭
        });


        $("form").submit(function (event) {
            var str = $(".timestart").val();
            var year = str.split('-')[0];
            var month = str.split('-')[1];
            var date = str.split('-')[2];
            var newDate = year + month + date;

            var myDate = new Date();
            var now = myDate.toLocaleDateString();
            var nowYear = now.split('/')[0];
            var nowMonth = now.split('/')[1];
            if (nowMonth < 10) {
                nowMonth = 0 + nowMonth;
            }
            var nowDate = now.split('/')[2];
            if (nowDate < 10) {
                nowDate = 0 + nowDate;
            }
            var nowDate = nowYear + nowMonth + nowDate;

            var diff = newDate - nowDate;


            if (diff < 0) {
                $("#myModalmob").modal();
                $(".tipsBox").html('出发日期不能小于当前时间');
                $(".showCircle").hide();
                $(".searchAgain").show();
                return false;
            }
        });
    });

</script>
</body>
</html>