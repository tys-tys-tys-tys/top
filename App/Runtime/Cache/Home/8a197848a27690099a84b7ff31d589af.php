<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo ($list['name']); ?></title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">价格修改</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper bgwhite travalList">
    <dl class="item clearfix">
        <dt>
            <a href="<?php echo U('Agency/details',array('id'=>$tour_list['id']));?>">
                <?php if($tour_list["cover_pic"] == ''): ?><img src='http://cdn.lydlr.com/public/images/temp/1.jpg'/>
                    <?php else: ?>
                    <img src="<?php echo ($pic); ?>"/><?php endif; ?>
            </a>
        </dt>
        <dd>
            <a href="<?php echo U('Agency/details',array('id'=>$tour_list['id']));?>">
                <p class="name"><?php echo ($tour_list["name"]); ?></p>

                <div class="mid clearfix">
                    <span class="tag pull-left"><?php echo ($lineTypeName); ?></span>
                    <span class="price pull-right"><dfn>&yen;<?php echo ($tour_list["min_price"]); ?></dfn>起</span>
                </div>
                <div class="bottom clearfix">
                    <span class="date pull-left"><?php echo ($tour_list["begin_time"]); ?></span>
                    <span class="ck pull-right">[详情]</span>
                </div>
            </a>
        </dd>
    </dl>
</div>
<div class="wapper bgwhite">
    <?php if($list['0']['cruise'] == ''): ?><form method="post" action="<?php echo U('Agency/changePrice');?>">
            <input id="changePrice" type="hidden" value="<?php echo U('Agency/changePrice');?>"/>
            <table class="modifyPrice" width="100%">
                <tr>
                    <th>团期</th>
                    <th>同行价/建议零售价</th>
                    <th>自定义零售价</th>
                </tr>
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><input type="hidden" name="id" value="<?php echo ($tour_list["id"]); ?>" class="id">
                    <tr class="sku" skuid="<?php echo ($vo["id"]); ?>">
                        <td rowspan="3" class="date" id="start_time"><?php echo ($vo["start_time"]); ?></td>
                        <td>
                            <p class="t">成人价</p>

                            <p class="org">&yen;<?php echo ($vo["price_adult_agency"]); ?>/&yen;<?php echo ($vo["price_adult_list"]); ?></p>
                        </td>

                        <?php if(empty($list["0"]["user_defined_adult_list"])): ?><td><input priceType="adult" oprice="<?php echo ($vo["price_adult_list"]); ?>" type="text"
                                       class="form-control price_blur newPrice" value="<?php echo ($vo["price_adult_list"]); ?>"/></td>
                            <?php else: ?>
                            <td><input priceType="adult" oprice="<?php echo ($vo["price_adult_list"]); ?>" type="text"
                                       class="form-control price_blur newPrice" value="<?php echo ($vo["user_defined_adult_list"]); ?>"/>
                            </td><?php endif; ?>

                    </tr>
                    <tr class="sku" skuid="<?php echo ($vo["id"]); ?>">
                        <td>
                            <p class="t">儿童价</p>

                            <p class="org">&yen;<?php echo ($vo["price_child_agency"]); ?>/&yen;<?php echo ($vo["price_child_list"]); ?></p>
                        </td>

                        <?php if(empty($list["0"]["user_defined_child_list"])): ?><td><input priceType="child" type="text" class="form-control price_blur child newPrice"
                                       value="<?php echo ($vo["price_child_list"]); ?>"/></td>
                            <?php else: ?>
                            <td><input priceType="child" type="text" class="form-control price_blur child newPrice"
                                       value="<?php echo ($vo["user_defined_child_list"]); ?>"/></td><?php endif; ?>

                    </tr>
                    <tr class="sku" skuid="<?php echo ($vo["id"]); ?>">
                        <td>
                            <p class="t">单房差 (每晚)</p>

                            <p class="org">&yen;<?php echo ($vo["price_hotel_agency"]); ?>/&yen;<?php echo ($vo["price_hotel_list"]); ?></p>
                        </td>

                        <?php if(empty($list["0"]["user_defined_hotel_list"])): ?><td><input priceType="hotel" type="text" class="form-control price_blur hotel newPrice"
                                       value="<?php echo ($vo["price_hotel_list"]); ?>"/></td>
                            <?php else: ?>
                            <td><input priceType="hotel" type="text" class="form-control price_blur hotel newPrice"
                                       value="<?php echo ($vo["user_defined_hotel_list"]); ?>"/></td><?php endif; ?>

                    </tr>
                    <tr>
                        <td colspan="3">
                            <input type="button" value="保存" class="btn btn-warning btn-block pull-right savePrice"
                                   style="width:80px;"/>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>

        <?php else: ?>
        <div class="well well-lg">游轮暂不支持改价！</div><?php endif; ?>

    <!-- 添加成功提示  -->
    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>保存成功</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning submit" data-dismiss="modal" aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default reset" data-dismiss="modal" aria-label="Close">取消
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalErr" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>修改的价钱不能小于零</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning submitError" data-dismiss="modal" aria-label="Close">
                        确认
                    </button>
                    <button type="button" class="btn btn-default resetError" data-dismiss="modal" aria-label="Close">
                        取消
                    </button>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>