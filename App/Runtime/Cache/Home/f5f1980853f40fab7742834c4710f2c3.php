<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>供应商查询</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <style>
        .ul-s1{
            border-top: 1px solid #c0c0c0;
            line-height:30px;
            font-size:15px;
        }
        .li-s1{
            border-bottom: 1px solid #c0c0c0;
            padding-top:10px;
        }
        .li-t1{
            padding-top: 10px;
        }
        .text-c1{
            color:#DCAC6C;
        }
        .text-c2{
            color:#33b3b8;
        }
    </style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">供应商查询</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="container">
    <br>
    <form method="post" action="<?php echo U('Personal/providerQuery');?>">
        <div class="row">
            <div class="col-xs-12"><b>输入目的地,或供应商名称:</b></div>
            <div class="col-xs-9" style="margin-top:10px;">
                <input type="text" class="form-control name" name="name" value="<?php echo ($name); ?>">
            </div>
            <div class="col-xs-3" style="margin-top:10px;">
                <input type="submit" class="btn btn-success" value="搜索">
            </div>
        </div>
    </form>
    <br>

    <?php if($_POST['name']== ''): if($list[0]['id'] != ''): ?><ul class="ul-s1">
        <h3 class="text-center">本月最新加盟</h3>
        <?php if(is_array($list)): $i = 0; $__LIST__ = array_slice($list,0,4,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="jumpToProviderList" id="<?php echo ($vo["id"]); ?>">
                <li>供销商名称: <?php echo ($vo["name"]); ?></li>
                <li>公司全称: <?php echo ($vo["full_name"]); ?></li>
                <li>业务联系人: <?php echo ($vo["contact_person"]); ?></li>
                <li>经销目的地: <?php echo ($vo["destinations"]); ?></li>
            </div>
            <li>联系人电话: <a href="tel:<?php echo ($vo["mobile"]); ?>" class="text-c2"><?php echo ($vo["mobile"]); ?></a></li>
            <li class="li-s1"></li><?php endforeach; endif; else: echo "" ;endif; ?>
        <span class="text-c1 clickToMoreForProviderInfo">点击查看更多</span>

        <div class="hiddenProviderInfo" hidden>
        <?php if(is_array($list)): $i = 0; $__LIST__ = array_slice($list,4,null,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="jumpToProviderList" id="<?php echo ($vo["id"]); ?>">
                <li>供销商名称: <?php echo ($vo["name"]); ?></li>
                <li>公司全称: <?php echo ($vo["full_name"]); ?></li>
                <li>业务联系人: <?php echo ($vo["contact_person"]); ?></li>
                <li>经销目的地: <?php echo ($vo["destinations"]); ?></li>
            </div>
            <li>联系人电话: <a href="tel:<?php echo ($vo["mobile"]); ?>" class="text-c2"><?php echo ($vo["mobile"]); ?></a></li>
            <li class="li-s1"></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </ul><?php endif; ?>
    <?php else: ?>
    <ul class="ul-s1" style="margin-top:20px;">
    <?php if($queryList[0]['id'] != ''): if(is_array($queryList)): foreach($queryList as $key=>$v): ?><div class="jumpToProviderList" id="<?php echo ($v["id"]); ?>">
            <li class="li-t1">供销商名称: <?php echo ($v["name"]); ?></li>
            <li>公司全称: <?php echo ($v["full_name"]); ?></li>
            <li>业务联系人: <?php echo ($v["contact_person"]); ?></li>
            <li>经销目的地: <?php echo ($v["destinations"]); ?></li>
        </div>
        <li>联系人电话: <a href="tel:<?php echo ($v["mobile"]); ?>" class="text-c2"><?php echo ($v["mobile"]); ?></a></li>
        <li class="li-s1"></li><?php endforeach; endif; ?>
    <?php else: ?>
        <h4 class="text-center">
            <p>抱歉，暂时没有<span class="text-c2"> <?php echo ($name); ?> </span>的信息</p>
            我们已做登记，并会尽快完善</h4><?php endif; ?>
    </ul><?php endif; ?>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


</footer>
<script>
    $("form").submit(function (event) {
        if (!$(".name").val()) {
            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html('请输入您要搜索的信息!');
            return false;
        }
    })
</script>
</body>
</html>