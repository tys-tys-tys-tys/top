<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>创建订单</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">订单填写</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper ucenter" style="overflow: hidden">
    <form method="post" action="">
        <input type="hidden" class="flightNo" value="<?php echo ($flightNo); ?>">
        <input type="hidden" class="depCode" value="<?php echo ($orgCity); ?>">
        <input type="hidden" class="arrCode" value="<?php echo ($dstCity); ?>">
        <input type="hidden" class="depDate" value="<?php echo ($date); ?>">
        <input type="hidden" class="depTime" value="<?php echo ($depTime); ?>">
        <input type="hidden" class="arrTime" value="<?php echo ($arriTime); ?>">
        <input type="hidden" class="planeModel" value="<?php echo ($planeModel); ?>">
        <input type='hidden' class='seatCode' value='<?php echo ($seatCode); ?>'>
        <input type="hidden" class="airportTax" value="<?php echo ($airportTax); ?>">
        <input type="hidden" class="orgJetquay" value="<?php echo ($orgJetquay); ?>">
        <input type="hidden" class="dstJetquay" value="<?php echo ($dstJetquay); ?>">
        <input type="hidden" class="arrDate" value="<?php echo ($arrDate); ?>">
        <input type="hidden" class="passengerIds" value="<?php echo ($passengerIds); ?>">

        <input type="hidden" class="parPrice" value="<?php echo ($parPrice); ?>">
        <input type="hidden" class="settlePrice" value="<?php echo ($settlePrice); ?>">
        <input type="hidden" class="airportTax" value="<?php echo ($airportTax); ?>">
        <input type="hidden" class="fuelTax" value="<?php echo ($fuelTax); ?>">
        <input type="hidden" class="policyId" value="<?php echo ($policyId); ?>">
        <input type="hidden" class="type" value="<?php echo ($type); ?>">

        <input type="hidden" class="flightOrderCreate" value="<?php echo (session('flightOrderCreate')); ?>">
        <input type="hidden" class="submitUrl" value="<?php echo U('Flight/orderCreate');?>">
        <input type="hidden" class="detailUrl" value="<?php echo U('Flight/orderDetail');?>">
        <input type="hidden" class="addPassengerUrl" value="<?php echo U('Flight/addPassengers');?>">
        <input type="hidden" class="delId" value="">

        <p></p>

        <div class="row flight-row-s1 flight-create-top">
            <table style="width: 100%">
                <tr>
                    <td width="25%" style="padding-left: 10px;" class="text-left"><?php echo ($date); ?></td>
                    <td width="70%" class="text-right" style="padding-right: 5px"><?php echo (nameToAirLine(substr($flightNo,0,2))); ?>
                        <?php echo ($flightNo); ?> | <?php echo ($planeModel); ?>
                    </td>
                </tr>
            </table>
        </div>

        <table style="width: 100%;line-height: 30px;">
            <tr>
                <td width="40%" class="text-left">
                    <span style="margin-left: 10px;"><?php echo (codeToAirport($orgCity)); ?> </span>
                </td>
                <td width="20%"><img src="http://cdn.lydlr.com/public/images/flight/ticket_10.png" width="70%"></td>
                <td width="40%" class="text-right">
                    <span style="margin-right: 10px;"><?php echo (codeToAirport($dstCity)); ?> </span>
                </td>
            </tr>
            <tr>
                <td width="40%">
                    <?php if($orgCity == 'NAY'): ?><span style="margin-left: 10px;">南苑机场 </span>
                        <?php elseif($orgCity == 'PVG'): ?>
                        <span style="margin-left: 10px;">浦东机场 </span>
                        <?php else: ?>
                        <span style="margin-left: 10px;"><?php echo (nameToAirport($orgCity)); ?> </span><?php endif; ?>
                    <?php if(($orgJetquay == 'Array') or ($orgJetquay == '--')): else: ?>
                        <?php echo ($orgJetquay); endif; ?>
                </td>
                <td width="5%"></td>
                <td width="40%" class="text-right">
                    <?php if($dstCity == 'NAY'): ?>南苑机场
                        <?php elseif($dstCity == 'PVG'): ?>
                        浦东机场
                        <?php else: ?>
                        <?php echo (nameToAirport($dstCity)); endif; ?>
                    <?php if(($dstJetquay == 'Array') or ($dstJetquay == '--')): else: ?>
                        <span style="margin-right: 10px;"><?php echo ($dstJetquay); ?></span><?php endif; ?>
                </td>
            </tr>
            <tr>
                <td width="40%" class="text-left">
                    <span style="margin-left: 10px;"><?php echo (returnTime($depTime)); ?></span>
                </td>
                <td width="20%"></td>
                <td width="40%" class="text-right">
                    <span style="margin-right: 10px;"><?php echo (returnTime($arriTime)); ?></span>
                </td>
            </tr>
        </table>
        <p></p>

        <div class="flight-list-border--dashed-bottom"></div>

        <table class="table">
            <?php if($needSwitchPNR == 1): ?><tr>
                    <td width="20%" style="color:red;border:0;">*</td>
                </tr><?php endif; ?>
            <tr>
                <td width="20%">类型</td>
                <td width="20%">单人总价</td>
                <td width="20%">结算价</td>
                <td width="20%">燃油税</td>
                <td width="20%">基建费</td>
            </tr>
            <tr>
                <td width="20%">成人</td>
                <td width="20%"><span class="flight-text-c2">&yen;<span class="adultTotal"><?php echo (getYuan($totalPrice)); ?></span></span></td>
                <td width="20%"><span class="flight-text-c2">&yen;<?php echo (getYuan($settlePrice)); ?></span></td>
                <td width="20%"><span class="flight-text-c2">&yen;<?php echo (getYuan($fuelTax)); ?></span></td>
                <td width="20%"><span class="flight-text-c2">&yen;<?php echo (getYuan($airportTax)); ?></span></td>
            </tr>
        </table>


        <div class="row flight-create-flighter">
             
            <div class="col-xs-11 text-left flight-text-c4">&nbsp;&nbsp;乘机人信息</div>
        </div>


        <p></p>

        <div class="flight-list-border-bottom" style="border:none"></div>

        <input type="hidden" class="totalPrice" value="<?php echo (getYuan($totalPrice)); ?>">
        <input type="hidden" class="seatCode" value="<?php echo ($seatCode); ?>">

        <div class="row">
            <div class="col-xs-11 text-right">
                <input type="button" class="btn btn-success addPassenger" value="添加乘机人">
            </div>
        </div>
        <br>

        <?php if(is_array($passengerList)): foreach($passengerList as $key=>$vo): ?><table class="table table-bordered table<?php echo ($vo["id"]); ?>">
                <tr>
                    <td width="20%">乘客姓名</td>
                    <td><?php echo ($vo["passenger_name"]); ?></td>
                </tr>
                <tr>
                    <td width="20%">乘客类型</td>
                    <td>
                        <?php if($vo['passenger_type'] == 0): ?>成人
                            <?php elseif($vo['passenger_type'] == 1): ?>
                            儿童<?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>证件号码</td>
                    <td><?php echo ($vo["identity_no"]); ?></td>
                </tr>
                <tr>
                    <td style="height:40px;line-height: 40px;">证件类型</td>
                    <td style="height:40px;line-height: 40px;">
                        <?php if($vo['identity_type'] == 1): ?>身份证
                            <?php elseif($vo['identity_type'] == 2): ?>
                            护照
                            <?php elseif($vo['identity_type'] == 3): ?>
                            军官证
                            <?php elseif($vo['identity_type'] == 4): ?>
                            士兵证
                            <?php elseif($vo['identity_type'] == 5): ?>
                            台胞证
                            <?php elseif($vo['identity_type'] == 6): ?>
                            港澳通行证<?php endif; ?>
                        <input type="button" class="btn btn-danger pull-right delPassenger" onclick="deletePassenger(<?php echo ($vo["id"]); ?>, <?php echo ($vo["passenger_type"]); ?>)" value="删除">
                    </td>
                </tr>
            </table><?php endforeach; endif; ?>


        <div class="row">
             
            <div class="col-xs-12 text-left flight-text-c4 flight-create-flighter">&nbsp;&nbsp;联系人信息</div>
        </div>

        <p></p>

        <div class="form-inline preorder">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon pt border-none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;姓&nbsp;名:</div>
                    <input type="text" style="width:80%" class="form-control nb linkMan"
                           placeholder="请填写您的真实姓名"/>
                </div>
            </div>
        </div>


        <p></p>

        <div class="flight-list-border-bottom"></div>


        <div class="form-inline preorder">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon pt border-none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;手&nbsp;机:</div>
                    <input type="number" style="width:80%" class="form-control nb linkPhone"
                           placeholder="请填写手机号"/>
                </div>
            </div>
        </div>


        <div class="form-inline preorder">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon pt border-none">(该手机号会收到出票通知)</div>
                </div>
            </div>
        </div>

        <p></p>

        <div class="flight-list-b3">
            &nbsp;
        </div>
        <p></p>

        <div class="row flight-create-button">
             
            <div class="col-xs-6 text-left">&nbsp;&nbsp;订单总额:&nbsp;<span class="flight-text-c2">&yen; <span class="ordertotal"><?php echo (getYuan($totalPrice * $passengerNum)); ?></span></span>
            </div>
            <div class="col-xs-5 text-right" style="padding:0;margin:0;">
                <input type="button" class="btn btn-warning showSub submitOrder" value="提交订单">

                <div id="showDiv"></div>
                <div class="showLoad text-left" style="display:none">
                    <img src="http://cdn.lydlr.com/public/images/load.gif"> <span style="color: #c6c6c6">亲,您的订单正在提交,请勿关闭订单</span>
                </div>
            </div>
        </div>

    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning dia-confirm" data-dismiss="modal" aria-label="Close">确认</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        <script src="/Public/js/flightOrderInfo.js?v=76"></script>
    </footer>
</div>
</body>
</html>