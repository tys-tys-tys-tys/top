<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>机票查询</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>

    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>

    <script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="http://cdn.lydlr.com/public/dist/js/bootstrap-select.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>


<body>
<div class="actNav">
    <div class="compulsoryRefresh">机票查询</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="container" style="max-width: 720px">

    <form method="post" action="<?php echo U('WxJsAPI/flightQueryList');?>" class="form-horizontal" role="form">
        <div class="form-group">
            <label for="basic" class="col-lg-2 control-label">出发地</label>

            <div class="col-lg-10">
                <select id="basic" name="departure" class="selectpicker show-tick form-control departure"
                        data-live-search="true" title='出发地'>
                    <optgroup label="热门城市" data-subtext="">
                        <option value="北京">(bj)北京</option>
                        <option value="三亚">(sy)三亚</option>
                        <option value="上海">(sh)上海</option>
                        <option value="南京">(nj)南京</option>
                        <option value="厦门">(sm)厦门</option>
                        <option value="大连">(dl)大连</option>
                        <option value="天津">(tj)天津</option>
                        <option value="广州">(gz)广州</option>
                        <option value="成都">(cd)成都</option>
                        <option value="昆明">(km)昆明</option>
                        <option value="杭州">(hz)杭州</option>
                        <option value="武汉">(wh)武汉</option>
                        <option value="济南">(jn)济南</option>
                        <option value="深圳">(sz)深圳</option>
                        <option value="福州">(fz)福州</option>
                        <option value="西安">(xa)西安</option>
                        <option value="重庆">(cq)重庆</option>
                        <option value="长沙">(cs)长沙</option>
                        <option value="青岛">(qd)青岛</option>
                    </optgroup>
                    <optgroup label="A" data-subtext="">
                        <option value="阿尔山">(aes)阿尔山</option>
                        <option value="安康">(ak)安康</option>
                        <option value="阿克苏">(aks)阿克苏</option>
                        <option value="阿里">(al)阿里</option>
                        <option value="阿勒泰">(alt)阿勒泰</option>
                        <option value="安庆">(aq)安庆</option>
                        <option value="鞍山">(as)鞍山</option>
                        <option value="安顺">(as)安顺</option>
                        <option value="安阳">(ay)安阳</option>
                    </optgroup>
                    <optgroup label="B" data-subtext="">
                        <option value="蚌埠">(bb)蚌埠</option>
                        <option value="布尔津">(bej)布尔津</option>
                        <option value="北海">(bh)北海</option>
                        <option value="毕节">(bj)毕节</option>
                        <option value="保山">(bs)保山</option>
                        <option value="百色">(bs)百色</option>
                        <option value="包头">(bt)包头</option>
                        <option value="巴彦淖尔">(byne)巴彦淖尔</option>
                    </optgroup>
                    <optgroup label="C" data-subtext="">
                        <option value="长春">(cc)长春</option>
                        <option value="成都">(cd)成都</option>
                        <option value="常德">(cd)常德</option>
                        <option value="昌都">(cd)昌都</option>
                        <option value="赤峰">(cf)赤峰</option>
                        <option value="长海">(ch)长海</option>
                        <option value="重庆">(cq)重庆</option>
                        <option value="长沙">(cs)长沙</option>
                        <option value="朝阳">(cy)朝阳</option>
                        <option value="常州">(cz)常州</option>
                        <option value="长白山">(cbs)长白山</option>
                        <option value="长治">(cz)长治</option>
                        <option value="池州">(cz)池州</option>
                    </optgroup>
                    <optgroup label="D" data-subtext="">
                        <option value="稻城">(dc)稻城</option>
                        <option value="丹东">(dd)丹东</option>
                        <option value="敦煌">(dh)敦煌</option>
                        <option value="大连">(dl)大连</option>
                        <option value="大理">(dl)大理</option>
                        <option value="迪庆">(dq)迪庆</option>
                        <option value="大庆">(dq)大庆</option>
                        <option value="大同">(dt)大同</option>
                        <option value="东营">(dy)东营</option>
                        <option value="达州">(dz)达州</option>
                    </optgroup>
                    <optgroup label="E" data-subtext="">
                        <option value="鄂尔多斯">(eeds)鄂尔多斯</option>
                        <option value="二连浩特">(dlht)二连浩特</option>
                        <option value="恩施">(es)恩施</option>
                    </optgroup>
                    <optgroup label="F" data-subtext="">
                        <option value="佛山">(fs)佛山</option>
                        <option value="阜阳">(fy)阜阳</option>
                        <option value="富蕴">(fy)富蕴</option>
                        <option value="福州">(fz)福州</option>
                    </optgroup>
                    <optgroup label="G" data-subtext="">
                        <option value="格尔木">(gem)格尔木</option>
                        <option value="广汉">(gh)广汉</option>
                        <option value="桂林">(gl)桂林</option>
                        <option value="甘南">(gn)甘南</option>
                        <option value="高雄">(gx)高雄</option>
                        <option value="贵阳">(gy)贵阳</option>
                        <option value="固原">(gy)固原</option>
                        <option value="广元">(gy)广元</option>
                        <option value="赣州">(gz)赣州</option>
                    </optgroup>
                    <optgroup label="H" data-subtext="">
                        <option value="淮安">(ha)淮安</option>
                        <option value="邯郸">(hd)邯郸</option>
                        <option value="哈尔滨">(heb)哈尔滨</option>
                        <option value="合肥">(hf)合肥</option>
                        <option value="怀化">(hh)怀化</option>
                        <option value="黑河">(hh)黑河</option>
                        <option value="呼和浩特">(hhht)呼和浩特</option>
                        <option value="海口">(hk)海口</option>
                        <option value="海拉尔">(hle)海拉尔</option>
                        <option value="花莲">(hl)花莲</option>
                        <option value="哈密">(hm)哈密</option>
                        <option value="黄山">(hs)黄山</option>
                        <option value="和田">(ht)和田</option>
                        <option value="衡阳">(hy)衡阳</option>
                        <option value="汉中">(hz)汉中</option>
                        <option value="惠州">(hz)惠州</option>
                    </optgroup>
                    <optgroup label="J" data-subtext="">
                        <option value="金昌">(jc)金昌</option>
                        <option value="景德镇">(jdz)景德镇</option>
                        <option value="井冈山">(jgs)井冈山</option>
                        <option value="九江">(jj)九江</option>
                        <option value="吉林">(jl)吉林</option>
                        <option value="佳木斯">(jms)佳木斯</option>
                        <option value="金门">(jm)金门</option>
                        <option value="济宁">(jn)济宁</option>
                        <option value="酒泉">(jq)酒泉</option>
                        <option value="鸡西">(jx)鸡西</option>
                        <option value="嘉峪关">(jyg)嘉峪关</option>
                        <option value="锦州">(jz)锦州</option>
                        <option value="荆州">(jz)荆州</option>
                        <option value="九寨沟">(jzg)九寨沟</option>
                    </optgroup>
                    <optgroup label="K" data-subtext="">
                        <option value="库车">(kc)库车</option>
                        <option value="康定">(kd)康定</option>
                        <option value="库尔勒">(kel)库尔勒</option>
                        <option value="克拉玛依">(klmy)克拉玛依</option>
                        <option value="喀什">(ks)喀什</option>
                    </optgroup>
                    <optgroup label="L" data-subtext="">
                        <option value="临沧">(lc)临沧</option>
                        <option value="连城">(lc)连城</option>
                        <option value="丽江">(lj)丽江</option>
                        <option value="黎平">(lp)黎平</option>
                        <option value="拉萨">(ls)拉萨</option>
                        <option value="洛阳">(ly)洛阳</option>
                        <option value="临沂">(ly)临沂</option>
                        <option value="连云港">(lyg)连云港</option>
                        <option value="泸州">(lz)泸州</option>
                        <option value="兰州">(lz)兰州</option>
                        <option value="柳州">(lz)柳州</option>
                        <option value="林芝">(lz)林芝</option>
                    </optgroup>
                    <optgroup label="M" data-subtext="">
                        <option value="牡丹江">(mdj)牡丹江</option>
                        <option value="马公">(mg)马公</option>
                        <option value="漠河">(mh)漠河</option>
                        <option value="芒市">(ms)芒市</option>
                        <option value="绵阳">(my)绵阳</option>
                        <option value="梅州">(mz)梅州</option>
                        <option value="满洲里">(mzl)满洲里</option>
                    </optgroup>
                    <optgroup label="N" data-subtext="">
                        <option value="宁波">(nb)宁波</option>
                        <option value="南昌">(nc)南昌</option>
                        <option value="南充">(nc)南充</option>
                        <option value="宁蒗">(nlyz)宁蒗彝族</option>
                        <option value="南宁">(nn)南宁</option>
                        <option value="南通">(nt)南通</option>
                        <option value="南阳">(ny)南阳</option>
                    </optgroup>
                    <optgroup label="P" data-subtext="">
                        <option value="普洱">(pe)普洱</option>
                        <option value="攀枝花">(pzh)攀枝花</option>
                    </optgroup>
                    <optgroup label="Q" data-subtext="">
                        <option value="秦皇岛">(qhd)秦皇岛</option>
                        <option value="黔江">(qj)黔江</option>
                        <option value="且末">(qm)且末</option>
                        <option value="齐齐哈尔">(qqhe)齐齐哈尔</option>
                        <option value="庆阳">(qy)庆阳</option>
                        <option value="泉州">(qz)泉州</option>
                        <option value="衢州">(qz)衢州</option>
                    </optgroup>
                    <optgroup label="R" data-subtext="">
                        <option value="日喀则">(rkz)日喀则</option>
                    </optgroup>
                    <optgroup label="S" data-subtext="">
                        <option value="韶关">(sg)韶关</option>
                        <option value="石家庄">(sjz)石家庄</option>
                        <option value="神农架">(snj)神农架</option>
                        <option value="汕头">(st)汕头</option>
                        <option value="沈阳">(sy)沈阳</option>
                    </optgroup>
                    <optgroup label="T" data-subtext="">
                        <option value="塔城">(tc)塔城</option>
                        <option value="腾冲">(tc)腾冲</option>
                        <option value="台东">(td)台东</option>
                        <option value="通化">(th)通化</option>
                        <option value="通辽">(tl)通辽</option>
                        <option value="吐鲁番">(tlf)吐鲁番</option>
                        <option value="台南">(tn)台南</option>
                        <option value="铜仁">(tr)铜仁</option>
                        <option value="天水">(ts)天水</option>
                        <option value="唐山">(ts)唐山</option>
                        <option value="太原">(ty)太原</option>
                        <option value="台州">(tz)台州</option>
                        <option value="台中">(tz)台中</option>
                    </optgroup>
                    <optgroup label="W" data-subtext="">
                        <option value="望安">(wa)望安</option>
                        <option value="潍坊">(wf)潍坊</option>
                        <option value="乌海">(wh)乌海</option>
                        <option value="威海">(wh)威海</option>
                        <option value="乌兰浩特">(wlht)乌兰浩特</option>
                        <option value="乌鲁木齐">(wlmq)乌鲁木齐</option>
                        <option value="文山">(ws)文山</option>
                        <option value="无锡">(wx)无锡</option>
                        <option value="武夷山">(wys)武夷山</option>
                        <option value="万州">(wz)万州</option>
                        <option value="梧州">(wz)梧州</option>
                        <option value="温州">(wz)温州</option>
                    </optgroup>
                    <optgroup label="X" data-subtext="">
                        <option value="西安">(xa)西安</option>
                        <option value="西昌">(xc)西昌</option>
                        <option value="锡林浩特">(xlht)锡林浩特</option>
                        <option value="西宁">(xn)西宁</option>
                        <option value="西双版纳">(xsbn)西双版纳</option>
                        <option value="邢台">(xt)邢台</option>
                        <option value="襄阳">(xy)襄阳</option>
                        <option value="新源">(xy)新源</option>
                        <option value="徐州">(xz)徐州</option>
                    </optgroup>
                    <optgroup label="Y" data-subtext="">
                        <option value="延安">(ya)延安</option>
                        <option value="宜宾">(yb)宜宾</option>
                        <option value="伊春">(yc)伊春</option>
                        <option value="盐城">(yc)盐城</option>
                        <option value="宜昌">(yc)宜昌</option>
                        <option value="运城">(yc)运城</option>
                        <option value="银川">(yc)银川</option>
                        <option value="宜春">(yc)宜春</option>
                        <option value="延吉">(yj)延吉</option>
                        <option value="榆林">(yl)榆林</option>
                        <option value="伊宁">(yn)伊宁</option>
                        <option value="玉树">(ys)玉树</option>
                        <option value="烟台">(yt)烟台</option>
                        <option value="义乌">(yw)义乌</option>
                        <option value="扬州">(yz)扬州</option>
                        <option value="永州">(yz)永州</option>
                    </optgroup>
                    <optgroup label="Z" data-subtext="">
                        <option value="珠海">(zh)珠海</option>
                        <option value="湛江">(zj)湛江</option>
                        <option value="张家界">(zjj)张家界</option>
                        <option value="张家口">(zjk)张家口</option>
                        <option value="舟山">(zs)舟山</option>
                        <option value="昭通">(zt)昭通</option>
                        <option value="中卫">(zw)中卫</option>
                        <option value="张掖">(zy)张掖</option>
                        <option value="遵义">(zy)遵义</option>
                        <option value="郑州">(zz)郑州</option>
                    </optgroup>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="basic" class="col-lg-2 control-label">目的地</label>

            <div class="col-lg-10">
                <select id="basic" name="destination" class="selectpicker show-tick form-control destination"
                        data-live-search="true" title='目的地'>
                    <optgroup label="热门城市" data-subtext="">
                        <option value="三亚">(sy)三亚</option>
                        <option value="上海">(sh)上海</option>
                        <option value="北京">(bj)北京</option>
                        <option value="南京">(nj)南京</option>
                        <option value="厦门">(sm)厦门</option>
                        <option value="大连">(dl)大连</option>
                        <option value="天津">(tj)天津</option>
                        <option value="广州">(gz)广州</option>
                        <option value="成都">(cd)成都</option>
                        <option value="昆明">(km)昆明</option>
                        <option value="杭州">(hz)杭州</option>
                        <option value="武汉">(wh)武汉</option>
                        <option value="济南">(jn)济南</option>
                        <option value="深圳">(sz)深圳</option>
                        <option value="福州">(fz)福州</option>
                        <option value="西安">(xa)西安</option>
                        <option value="重庆">(cq)重庆</option>
                        <option value="长沙">(cs)长沙</option>
                        <option value="青岛">(qd)青岛</option>
                    </optgroup>
                    <optgroup label="A" data-subtext="">
                        <option value="阿尔山">(aes)阿尔山</option>
                        <option value="安康">(ak)安康</option>
                        <option value="阿克苏">(aks)阿克苏</option>
                        <option value="阿里">(al)阿里</option>
                        <option value="阿勒泰">(alt)阿勒泰</option>
                        <option value="安庆">(aq)安庆</option>
                        <option value="鞍山">(as)鞍山</option>
                        <option value="安顺">(as)安顺</option>
                        <option value="安阳">(ay)安阳</option>
                    </optgroup>
                    <optgroup label="B" data-subtext="">
                        <option value="蚌埠">(bb)蚌埠</option>
                        <option value="布尔津">(bej)布尔津</option>
                        <option value="北海">(bh)北海</option>
                        <option value="毕节">(bj)毕节</option>
                        <option value="保山">(bs)保山</option>
                        <option value="百色">(bs)百色</option>
                        <option value="包头">(bt)包头</option>
                        <option value="巴彦淖尔">(byne)巴彦淖尔</option>
                    </optgroup>
                    <optgroup label="C" data-subtext="">
                        <option value="长春">(cc)长春</option>
                        <option value="成都">(cd)成都</option>
                        <option value="常德">(cd)常德</option>
                        <option value="昌都">(cd)昌都</option>
                        <option value="赤峰">(cf)赤峰</option>
                        <option value="长海">(ch)长海</option>
                        <option value="重庆">(cq)重庆</option>
                        <option value="长沙">(cs)长沙</option>
                        <option value="朝阳">(cy)朝阳</option>
                        <option value="常州">(cz)常州</option>
                        <option value="长白山">(cbs)长白山</option>
                        <option value="长治">(cz)长治</option>
                        <option value="池州">(cz)池州</option>
                    </optgroup>
                    <optgroup label="D" data-subtext="">
                        <option value="稻城">(dc)稻城</option>
                        <option value="丹东">(dd)丹东</option>
                        <option value="敦煌">(dh)敦煌</option>
                        <option value="大连">(dl)大连</option>
                        <option value="大理">(dl)大理</option>
                        <option value="迪庆">(dq)迪庆</option>
                        <option value="大庆">(dq)大庆</option>
                        <option value="大同">(dt)大同</option>
                        <option value="东营">(dy)东营</option>
                        <option value="达州">(dz)达州</option>
                    </optgroup>
                    <optgroup label="E" data-subtext="">
                        <option value="鄂尔多斯">(eeds)鄂尔多斯</option>
                        <option value="二连浩特">(dlht)二连浩特</option>
                        <option value="恩施">(es)恩施</option>
                    </optgroup>
                    <optgroup label="F" data-subtext="">
                        <option value="佛山">(fs)佛山</option>
                        <option value="阜阳">(fy)阜阳</option>
                        <option value="富蕴">(fy)富蕴</option>
                        <option value="福州">(fz)福州</option>
                    </optgroup>
                    <optgroup label="G" data-subtext="">
                        <option value="格尔木">(gem)格尔木</option>
                        <option value="广汉">(gh)广汉</option>
                        <option value="桂林">(gl)桂林</option>
                        <option value="甘南">(gn)甘南</option>
                        <option value="高雄">(gx)高雄</option>
                        <option value="贵阳">(gy)贵阳</option>
                        <option value="固原">(gy)固原</option>
                        <option value="广元">(gy)广元</option>
                        <option value="赣州">(gz)赣州</option>
                    </optgroup>
                    <optgroup label="H" data-subtext="">
                        <option value="淮安">(ha)淮安</option>
                        <option value="邯郸">(hd)邯郸</option>
                        <option value="哈尔滨">(heb)哈尔滨</option>
                        <option value="合肥">(hf)合肥</option>
                        <option value="怀化">(hh)怀化</option>
                        <option value="黑河">(hh)黑河</option>
                        <option value="呼和浩特">(hhht)呼和浩特</option>
                        <option value="海口">(hk)海口</option>
                        <option value="海拉尔">(hle)海拉尔</option>
                        <option value="花莲">(hl)花莲</option>
                        <option value="哈密">(hm)哈密</option>
                        <option value="黄山">(hs)黄山</option>
                        <option value="和田">(ht)和田</option>
                        <option value="衡阳">(hy)衡阳</option>
                        <option value="汉中">(hz)汉中</option>
                        <option value="惠州">(hz)惠州</option>
                    </optgroup>
                    <optgroup label="J" data-subtext="">
                        <option value="金昌">(jc)金昌</option>
                        <option value="景德镇">(jdz)景德镇</option>
                        <option value="井冈山">(jgs)井冈山</option>
                        <option value="九江">(jj)九江</option>
                        <option value="吉林">(jl)吉林</option>
                        <option value="佳木斯">(jms)佳木斯</option>
                        <option value="金门">(jm)金门</option>
                        <option value="济宁">(jn)济宁</option>
                        <option value="酒泉">(jq)酒泉</option>
                        <option value="鸡西">(jx)鸡西</option>
                        <option value="嘉峪关">(jyg)嘉峪关</option>
                        <option value="锦州">(jz)锦州</option>
                        <option value="荆州">(jz)荆州</option>
                        <option value="九寨沟">(jzg)九寨沟</option>
                    </optgroup>
                    <optgroup label="K" data-subtext="">
                        <option value="库车">(kc)库车</option>
                        <option value="康定">(kd)康定</option>
                        <option value="库尔勒">(kel)库尔勒</option>
                        <option value="克拉玛依">(klmy)克拉玛依</option>
                        <option value="喀什">(ks)喀什</option>
                    </optgroup>
                    <optgroup label="L" data-subtext="">
                        <option value="临沧">(lc)临沧</option>
                        <option value="连城">(lc)连城</option>
                        <option value="丽江">(lj)丽江</option>
                        <option value="黎平">(lp)黎平</option>
                        <option value="拉萨">(ls)拉萨</option>
                        <option value="洛阳">(ly)洛阳</option>
                        <option value="临沂">(ly)临沂</option>
                        <option value="连云港">(lyg)连云港</option>
                        <option value="泸州">(lz)泸州</option>
                        <option value="兰州">(lz)兰州</option>
                        <option value="柳州">(lz)柳州</option>
                        <option value="林芝">(lz)林芝</option>
                    </optgroup>
                    <optgroup label="M" data-subtext="">
                        <option value="牡丹江">(mdj)牡丹江</option>
                        <option value="马公">(mg)马公</option>
                        <option value="漠河">(mh)漠河</option>
                        <option value="芒市">(ms)芒市</option>
                        <option value="绵阳">(my)绵阳</option>
                        <option value="梅州">(mz)梅州</option>
                        <option value="满洲里">(mzl)满洲里</option>
                    </optgroup>
                    <optgroup label="N" data-subtext="">
                        <option value="宁波">(nb)宁波</option>
                        <option value="南昌">(nc)南昌</option>
                        <option value="南充">(nc)南充</option>
                        <option value="宁蒗">(nlyz)宁蒗彝族</option>
                        <option value="南宁">(nn)南宁</option>
                        <option value="南通">(nt)南通</option>
                        <option value="南阳">(ny)南阳</option>
                    </optgroup>
                    <optgroup label="P" data-subtext="">
                        <option value="普洱">(pe)普洱</option>
                        <option value="攀枝花">(pzh)攀枝花</option>
                    </optgroup>
                    <optgroup label="Q" data-subtext="">
                        <option value="秦皇岛">(qhd)秦皇岛</option>
                        <option value="黔江">(qj)黔江</option>
                        <option value="且末">(qm)且末</option>
                        <option value="齐齐哈尔">(qqhe)齐齐哈尔</option>
                        <option value="庆阳">(qy)庆阳</option>
                        <option value="泉州">(qz)泉州</option>
                        <option value="衢州">(qz)衢州</option>
                    </optgroup>
                    <optgroup label="R" data-subtext="">
                        <option value="日喀则">(rkz)日喀则</option>
                    </optgroup>
                    <optgroup label="S" data-subtext="">
                        <option value="韶关">(sg)韶关</option>
                        <option value="石家庄">(sjz)石家庄</option>
                        <option value="神农架">(snj)神农架</option>
                        <option value="汕头">(st)汕头</option>
                        <option value="沈阳">(sy)沈阳</option>
                    </optgroup>
                    <optgroup label="T" data-subtext="">
                        <option value="塔城">(tc)塔城</option>
                        <option value="腾冲">(tc)腾冲</option>
                        <option value="台东">(td)台东</option>
                        <option value="通化">(th)通化</option>
                        <option value="通辽">(tl)通辽</option>
                        <option value="吐鲁番">(tlf)吐鲁番</option>
                        <option value="台南">(tn)台南</option>
                        <option value="铜仁">(tr)铜仁</option>
                        <option value="天水">(ts)天水</option>
                        <option value="唐山">(ts)唐山</option>
                        <option value="太原">(ty)太原</option>
                        <option value="台州">(tz)台州</option>
                        <option value="台中">(tz)台中</option>
                    </optgroup>
                    <optgroup label="W" data-subtext="">
                        <option value="望安">(wa)望安</option>
                        <option value="潍坊">(wf)潍坊</option>
                        <option value="乌海">(wh)乌海</option>
                        <option value="威海">(wh)威海</option>
                        <option value="乌兰浩特">(wlht)乌兰浩特</option>
                        <option value="乌鲁木齐">(wlmq)乌鲁木齐</option>
                        <option value="文山">(ws)文山</option>
                        <option value="无锡">(wx)无锡</option>
                        <option value="武夷山">(wys)武夷山</option>
                        <option value="万州">(wz)万州</option>
                        <option value="梧州">(wz)梧州</option>
                        <option value="温州">(wz)温州</option>
                    </optgroup>
                    <optgroup label="X" data-subtext="">
                        <option value="西安">(xa)西安</option>
                        <option value="西昌">(xc)西昌</option>
                        <option value="锡林浩特">(xlht)锡林浩特</option>
                        <option value="西宁">(xn)西宁</option>
                        <option value="西双版纳">(xsbn)西双版纳</option>
                        <option value="邢台">(xt)邢台</option>
                        <option value="襄阳">(xy)襄阳</option>
                        <option value="新源">(xy)新源</option>
                        <option value="徐州">(xz)徐州</option>
                    </optgroup>
                    <optgroup label="Y" data-subtext="">
                        <option value="延安">(ya)延安</option>
                        <option value="宜宾">(yb)宜宾</option>
                        <option value="伊春">(yc)伊春</option>
                        <option value="盐城">(yc)盐城</option>
                        <option value="宜昌">(yc)宜昌</option>
                        <option value="运城">(yc)运城</option>
                        <option value="银川">(yc)银川</option>
                        <option value="宜春">(yc)宜春</option>
                        <option value="延吉">(yj)延吉</option>
                        <option value="榆林">(yl)榆林</option>
                        <option value="伊宁">(yn)伊宁</option>
                        <option value="玉树">(ys)玉树</option>
                        <option value="烟台">(yt)烟台</option>
                        <option value="义乌">(yw)义乌</option>
                        <option value="扬州">(yz)扬州</option>
                        <option value="永州">(yz)永州</option>
                    </optgroup>
                    <optgroup label="Z" data-subtext="">
                        <option value="珠海">(zh)珠海</option>
                        <option value="湛江">(zj)湛江</option>
                        <option value="张家界">(zjj)张家界</option>
                        <option value="张家口">(zjk)张家口</option>
                        <option value="舟山">(zs)舟山</option>
                        <option value="昭通">(zt)昭通</option>
                        <option value="中卫">(zw)中卫</option>
                        <option value="张掖">(zy)张掖</option>
                        <option value="遵义">(zy)遵义</option>
                        <option value="郑州">(zz)郑州</option>
                    </optgroup>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="basic" class="col-lg-2 control-label">出发日期</label>

            <div class="col-lg-10">
                <input class="form-control timestart" name="timestart" readonly id="datetimepicker" size="16"
                       type="text" style="width:100%;float:left;height:40px;"
                       value="<?php echo date('Y-m-d', time());?>">
            </div>
        </div>

        <!--
        <div class="form-group">
            <label for="basic" class="col-lg-1 control-label"></label>

            <div class="col-lg-11">
                <div class="flight-query-border-bottom"></div>
                <br/>
            </div>

        </div>
        -->
        <br/>

        <div class="form-group">
            <!--<label for="basic" class="col-lg-1 control-label"></label>-->

            <div style="max-width: 720px;">
                <div class="flight-query-button">
                    <input type="submit" class="btn btn-warning btn-lg flight-query-button-length searchAgain"
                           value="查询">

                    <div class="showCircle" style="display:none">
                        <div style="float:left">
                            <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g id="circle" class="g-circles g-circles--v1">
                                    <circle id="12"
                                            transform="translate(35, 16.698730) rotate(-30) translate(-35, -16.698730) "
                                            cx="35" cy="16.6987298" r="10"></circle>
                                    <circle id="11"
                                            transform="translate(16.698730, 35) rotate(-60) translate(-16.698730, -35) "
                                            cx="16.6987298" cy="35" r="10"></circle>
                                    <circle id="10" transform="translate(10, 60) rotate(-90) translate(-10, -60) "
                                            cx="10" cy="60" r="10"></circle>
                                    <circle id="9"
                                            transform="translate(16.698730, 85) rotate(-120) translate(-16.698730, -85) "
                                            cx="16.6987298" cy="85" r="10"></circle>
                                    <circle id="8"
                                            transform="translate(35, 103.301270) rotate(-150) translate(-35, -103.301270) "
                                            cx="35" cy="103.30127" r="10"></circle>
                                    <circle id="7" cx="60" cy="110" r="10"></circle>
                                    <circle id="6"
                                            transform="translate(85, 103.301270) rotate(-30) translate(-85, -103.301270) "
                                            cx="85" cy="103.30127" r="10"></circle>
                                    <circle id="5"
                                            transform="translate(103.301270, 85) rotate(-60) translate(-103.301270, -85) "
                                            cx="103.30127" cy="85" r="10"></circle>
                                    <circle id="4" transform="translate(110, 60) rotate(-90) translate(-110, -60) "
                                            cx="110" cy="60" r="10"></circle>
                                    <circle id="3"
                                            transform="translate(103.301270, 35) rotate(-120) translate(-103.301270, -35) "
                                            cx="103.30127" cy="35" r="10"></circle>
                                    <circle id="2"
                                            transform="translate(85, 16.698730) rotate(-150) translate(-85, -16.698730) "
                                            cx="85" cy="16.6987298" r="10"></circle>
                                    <circle id="1" cx="60" cy="10" r="10"></circle>
                                </g>
                                <use xlink:href="#circle" class="use"/>
                            </svg>
                        </div>
                        <span class="circle-size">拼命加载中...</span>
                    </div>
                </div>
            </div>

        </div>
    </form>

    <br/><br/><br/><br/>

    <div class="row text-center flight-row-p">
        <div class="col-xs-3">
            <img width="80%" src="http://cdn.lydlr.com/public/images/flight/ticket_03.png">
        </div>
        <div class="col-xs-3">
            <img width="80%" src="http://cdn.lydlr.com/public/images/flight/ticket_04.png">
        </div>
        <div class="col-xs-3">
            <img width="80%" src="http://cdn.lydlr.com/public/images/flight/ticket_05.png">
        </div>
        <div class="col-xs-3">
            <img width="80%" src="http://cdn.lydlr.com/public/images/flight/ticket_06.png">
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    $(document).ready(function () {
        $(".timestart").keypress(function (event) {
            return false;
        }).focus(function () {
            this.style.imeMode = 'disabled';
        });

        var mySelect = $('#first-disabled2');

        $('#special').on('click', function () {
            mySelect.find('option:selected').prop('disabled', true);
            mySelect.selectpicker('refresh');
        });

        $('#special2').on('click', function () {
            mySelect.find('option:disabled').prop('disabled', false);
            mySelect.selectpicker('refresh');
        });

        $('#basic2').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
    });

    $(".compulsoryRefresh").click(function () {
        location.reload(true)
    });
</script>
<script>
    $(function () {
        $('#datetimepicker').datetimepicker({
            minView: "month", //选择日期后，不会再跳转去选择时分秒
            format: "yyyy-mm-dd", //选择日期后，文本框显示的日期格式
            language: 'zh-CN', //汉化
            autoclose: true //选择日期后自动关闭
        });


        $("form").submit(function (event) {
            var str = $(".timestart").val();
            var year = str.split('-')[0];
            var month = str.split('-')[1];
            var date = str.split('-')[2];
            var newDate = year + month + date;

            var myDate = new Date();
            var now = myDate.toLocaleDateString();
            var nowYear = now.split('/')[0];
            var nowMonth = now.split('/')[1];
            if (nowMonth < 10) {
                nowMonth = 0 + nowMonth;
            }
            var nowDate = now.split('/')[2];
            if (nowDate < 10) {
                nowDate = 0 + nowDate;
            }
            var nowDate = nowYear + nowMonth + nowDate;

            var diff = newDate - nowDate;

            var departure = $(".departure").val();
            var destination = $(".destination").val();

            if (departure == '' || destination == '') {
                $("#myModalmob").modal();
                $(".tipsBox").html('出发地或目的地不能为空');
                $(".showCircle").hide();
                $(".searchAgain").show();
                return false;
            }

            if (diff < 0) {
                $("#myModalmob").modal();
                $(".tipsBox").html('出发日期不能小于当前时间');
                $(".showCircle").hide();
                $(".searchAgain").show();
                return false;
            }
        });
    });

    $(".searchAgain").click(function () {
        $(".searchAgain").hide();
        $(".showCircle").show();
        //$(".searchAgain").attr('disabled','true');
    });
</script>
<script>
    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "我的手机旅行社 - 机票查询",
            title: "我的手机旅行社 - 机票查询",
            link: "<?php echo ($domain); ?>",
            imgUrl: 'http://cdn.lydlr.com/public/images/flight/plane.jpg',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
<footer class="footer">
    <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?></pre>
    <img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
    <p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>
</footer>
</body>
</html>