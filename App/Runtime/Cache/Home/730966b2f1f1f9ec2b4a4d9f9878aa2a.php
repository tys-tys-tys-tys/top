<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>退票申请</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">退票申请</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper" style="margin: 10px;">
    <form method="post" action="<?php echo U('Flight/refundApplication');?>" class="form-horizontal" role="form">
        <input type="hidden" id="refundApplication" value="<?php echo U('Flight/refundApplication');?>">
        <input type='hidden' class='segment' value="<?php echo ($dep_code); ?>-<?php echo ($arr_code); ?>">

        <div class="form-group">
            <label class="col-sm-2 control-label">姓名：</label>

            <div class="col-sm-10">
                <input type="text" class="form-control passengerName" placeholder=""
                       value="<?php echo ($flightInfo["passenger_name"]); ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">票号：</label>

            <div class="col-sm-10">
                <input type="text" class="form-control ticketNo" placeholder=""
                       value="<?php echo ($flightInfo["ticket_no"]); ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">航段：</label>

            <div class="col-sm-10">
                <input type="text" class="form-control ticketNo" placeholder=""
                       value="<?php echo (nameToAirport($flightInfo["dep_code"])); ?> - <?php echo (nameToAirport($flightInfo["arr_code"])); ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">订单号：</label>

            <div class="col-sm-10">
                <input type="text" class="form-control orderNo" placeholder=""
                       value="<?php echo ($flightInfo["sequence_no"]); ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">退票类型：</label>

            <div class="col-sm-10">
                <select class="form-control refundType">
                    <option value="0">--请按实际情况选择退票类型--</option>
                    <option value="1">【当日作废】：当日作废，收取10元手续费</option>
                    <option value="2">【当日作废】： 其他废票情况</option>
                    <option value="3">【自愿退票】：客人自愿退票，按客规收取手续费</option>
                    <option value="5">【非自愿退票】：航班延误、取消，申请全退</option>
                    <option value="8">【非自愿退票】：客人因病无法乘机，申请全退，病退证明已邮寄</option>
                    <option value="9">【非自愿退票】： 特殊退票情况</option>
                    <option value="6">【升舱换开】：升舱换开，申请自愿退票</option>
                    <option value="7">【升舱换开】：名字错换开重出，申请全退（填写新票号）</option>
                    <option value="10">申请退回票款差价【申退差价】</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php if($flightInfo['state'] == 3): ?><button type="button" class="btn btn-warning pull-right doRefundFlightApplication">确定</button>
                    <?php else: ?>
                    <div class="text-right" style="color: #f08519">退票中,待退款</div><?php endif; ?>
            </div>
        </div>
    </form>


    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>

</body>
</html>