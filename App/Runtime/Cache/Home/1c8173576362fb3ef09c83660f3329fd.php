<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>修改密码</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">修改密码</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper ucenter">
    <header class="header clearfix">
        <?php if($list['bg_pic'] != ''): ?><div class="bg" style="background: url('<?php echo ($bgPic); ?>')"></div>
            <?php else: ?>
            <div class="bg" style="background: url('http://cdn.lydlr.com/public/images/header-bg.jpg')"></div><?php endif; ?>
        <dl class="adviser pull-left">
            <dt>
                <?php if($list['head_pic'] != ''): ?><img src="<?php echo ($headPic); ?>" alt="">
                    <?php else: ?>
                    <img src="http://cdn.lydlr.com/public/images/temp/3.jpg"><?php endif; ?>
            </dt>
        </dl>
        <?php if($list['weixin_qrcode'] != ''): ?><img src="<?php echo ($weixinQrcode); ?>" class="pull-right codeimg"/>
            <?php else: ?>
            <img src="http://cdn.lydlr.com/public/images/temp/2.jpg" class="pull-right codeimg"><?php endif; ?>
    </header>
    <div class="modifyInfo module">
        <h2 class="title">个人资料</h2>

        <form class="form-horizontal zcform" method="post"
              action="<?php echo U('Personal/postEdit',array('id'=>$userList['id']));?>" enctype="multipart/form-data">
            <input id="personalJump" type="hidden" value="<?php echo U('Personal/index');?>"/>

            <div class="form-group">
                <label class="col-sm-2 control-label">姓&nbsp;&nbsp;&nbsp;名：</label>

                <div class="col-sm-10">
                    <?php echo ($list["name"]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">登录名：</label>

                <div class="col-sm-10">
                    <?php echo ($userList["login_name"]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label pas">密&nbsp;&nbsp;&nbsp;码：</label>

                <div class="col-sm-10">
                    <input id="password" name="password" class="form-control" type="password"
                           class="{required:true,rangelength:[8,20],}" placeholder="请输入密码">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label pas">确&nbsp;&nbsp;&nbsp;认：</label>

                <div class="col-sm-10">
                    <input class="form-control" id="confirm_password" name="confirm_password" type="password"
                           class="{required:true,equalTo:'#password'}" placeholder="请再次输入密码">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">手&nbsp;&nbsp;&nbsp;机：</label>

                <div class="col-sm-10">
                    <?php echo ($list["mobile"]); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">邮&nbsp;&nbsp;&nbsp;箱：</label>

                <div class="col-sm-10">
                    <?php echo ($list["email"]); ?>
                </div>
            </div>
            <div class="form-group btbox">
                <input type="submit" id="doSaveEdit" value="保存" class="sbtn save pull-left"/>
                <input type="reset" value="重置" class="sbtn reset pull-left"/>
            </div>
        </form>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalDel" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>请将信息填写完整</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>保存成功</p>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>