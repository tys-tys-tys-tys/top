<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>乘机人</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div>乘机人</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>

<div class="wapper ucenter" style="background-color: white;padding:15px;overflow: hidden">
    <form class="form-horizontal">
        <input type="hidden" class="addUrl" value="<?php echo U('Flight/addFrequentPassenger');?>">
        <input type="hidden" class="jumpUrl" value="<?php echo U('Flight/addPassengers');?>">
        <input type="hidden" class="passengerIds" value="<?php echo ($passengerIds); ?>">
        <input type="hidden" class="flightNo" value="<?php echo ($flightNo); ?>">
        <input type="hidden" class="depCode" value="<?php echo ($depCode); ?>">
        <input type="hidden" class="arrCode" value="<?php echo ($arrCode); ?>">
        <input type="hidden" class="depDate" value="<?php echo ($depDate); ?>">
        <input type="hidden" class="depTime" value="<?php echo ($depTime); ?>">
        <input type="hidden" class="arrTime" value="<?php echo ($arrTime); ?>">
        <input type="hidden" class="planeModel" value="<?php echo ($planeModel); ?>">
        <input type="hidden" class="policyId" value="<?php echo ($policyId); ?>">
        <input type="hidden" class="seatCode" value="<?php echo ($seatCode); ?>">
        <input type="hidden" class="settlePrice" value="<?php echo ($settlePrice); ?>">
        <input type="hidden" class="parPrice" value="<?php echo ($parPrice); ?>">
        <input type="hidden" class="fuelTax" value="<?php echo ($fuelTax); ?>">
        <input type="hidden" class="airportTax" value="<?php echo ($airportTax); ?>">
        <input type="hidden" class="orgJetquay" value="<?php echo ($orgJetquay); ?>">
        <input type="hidden" class="dstJetquay" value="<?php echo ($dstJetquay); ?>">
        <input type="hidden" class="arrDate" value="<?php echo ($arrDate); ?>">
        <input type="hidden" class="type" value="<?php echo ($type); ?>">
        <input type="hidden" class="firstWord" value="">

        <div class="form-group">
            <label for="passenger_name" class="col-sm-2 control-label">乘客姓名</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="passenger_name" value="<?php echo ($info["passenger_name"]); ?>">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">乘客类型</label>

            <div class="col-sm-10 passenger_type">
                <?php if($info['passenger_type'] == '0'): ?><input type="radio" name="passenger_type" value="0" checked>成人
                    <input type="radio" name="passenger_type" value="1" style="margin-left: 20px;">儿童
                    <?php elseif($info['passenger_type'] == '1'): ?>
                    <input type="radio" name="passenger_type" value="0">成人
                    <input type="radio" name="passenger_type" value="1" style="margin-left: 20px;" checked>儿童
                    <?php else: ?>
                    <input type="radio" name="passenger_type" value="0" checked>成人
                    <input type="radio" name="passenger_type" value="1" style="margin-left: 20px;">儿童<?php endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">证件类型</label>

            <div class="col-sm-10">
                <select class="form-control identity_type">
                    <option value="1"
                    <?php if($info['identity_type'] == 1): ?>selected="selected"<?php endif; ?>
                    >身份证</option>
                    <option value="2"
                    <?php if($info['identity_type'] == 2): ?>selected="selected"<?php endif; ?>
                    >护照</option>
                    <option value="3"
                    <?php if($info['identity_type'] == 3): ?>selected="selected"<?php endif; ?>
                    >军官证</option>
                    <option value="4"
                    <?php if($info['identity_type'] == 4): ?>selected="selected"<?php endif; ?>
                    >士兵证</option>
                    <option value="5"
                    <?php if($info['identity_type'] == 5): ?>selected="selected"<?php endif; ?>
                    >台胞证</option>
                    <option value="6"
                    <?php if($info['identity_type'] == 6): ?>selected="selected"<?php endif; ?>
                    >港澳通行证</option>
                </select>
            </div>
        </div>

        <div class="form-group hide birth">
            <label for="identity_no" class="col-sm-2 control-label">出生日期</label>

            <div class="col-sm-10">
                <input class="form-control birthday" readonly id="datetimepicker" size="16"
                       type="text"
                       value="<?php echo ($info["birthday"]); ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="identity_no" class="col-sm-2 control-label">证件号码</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" id="identity_no" value="<?php echo ($info["identity_no"]); ?>">
            </div>
        </div>

        <input type="button" class="btn btn-success btn-block subFrequentPassenger" value="确认">
    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitWarning" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                </div>
            </div>
        </div>
    </div>
    <p></p>

</div>

<footer class="footer">
    <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    <script src="/Public/js/flightOrderInfo.js?v=5"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
    <script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="/Public/js/jQuery.Hz2Py-min.js"></script>
</footer>

<script>
    $(function () {
        $('#datetimepicker').datetimepicker({
            minView: "month", //选择日期后，不会再跳转去选择时分秒
            format: "yyyy-mm-dd", //选择日期后，文本框显示的日期格式
            language: 'zh-CN', //汉化
            autoclose: true //选择日期后自动关闭
        });
    });

    var passenger_type = $("input:radio:checked").val();

    if (passenger_type == 1) {
        $(".birth").removeClass("hide");
        $(".birth").addClass("show");
    }

    $("#passenger_name").focus();
</script>
</body>
</html>