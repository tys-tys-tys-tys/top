<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>个人中心</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">个人中心</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper ucenter">
    <header class="header clearfix">
        <?php if($agency['bg_pic'] != ''): ?><div class="bg" style="background: url('<?php echo ($bgPic); ?>')">
                <span style="float:right;margin-top:125px;margin-right:11px;color:#DCAC6C">长按识别二维码</span>
            </div>
            <?php else: ?>
            <div class="bg" style="background: url('http://cdn.lydlr.com/public/images/header-bg.jpg')">
                <span style="float:right;margin-top:125px;margin-right:11px;color:#DCAC6C">长按识别二维码</span>
            </div><?php endif; ?>

        <dl class="adviser pull-left">
            <dt>
                <?php if($agency['head_pic'] != ''): ?><img src="<?php echo ($headPic); ?>" alt="" class="headPic">
                    <?php else: ?>
                    <img src="http://cdn.lydlr.com/public/images/temp/3.jpg" class="headPic"><?php endif; ?>
            </dt>
            <dd>
                <p class="name">
                    <?php if($userList['nick_name'] == ''): echo ($userList['name']); ?>
                        <?php else: ?>
                        <?php echo ($userList['nick_name']); endif; ?>
                </p>
                <a style="color:white;" href="tel:<?php echo ($userList['mobile']); ?>">TEL:<?php echo ($userList['mobile']); ?></a>
            </dd>
        </dl>
        <?php if($agency["weixin_qrcode"] != ''): ?><img src="<?php echo ($weixinQrcode); ?>" class="pull-right codeimg"/>
            <?php else: ?>
            <img src="http://cdn.lydlr.com/public/images/temp/2.jpg" class="pull-right codeimg"><?php endif; ?>
    </header>
    <ul class="actlist">
        <li>
            <a href="<?php echo U('Personal/edit');?>">
                <i class="i-1"></i>
                修改密码
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/pageSetting');?>">
                <i class="i-2"></i>
                页面设置
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/column_sort');?>">
                <i class="i-2"></i>
                模块排序
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/myorder');?>">
                <i class="i-3"></i>
                线路订单
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/otherOrder');?>">
                <i class="i-3"></i>
                其他订单
            </a>
        </li>
        <li>
            <a href="<?php echo U('Finance/index');?>">
                <i class="i-4"></i>
                财务管理
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/myrecommend');?>">
                <i class="i-5"></i>
                我的推荐
            </a>
        </li>
        <li>
            <a href="<?php echo U('Flight/myPlaneTicket');?>">
                <i class="i-5" style="background-position: 0 -421px;"></i>
                我的机票
            </a>
        </li>
        <li>
            <a href="<?php echo U('Ticket/myScenicTicket');?>">
                <i class="i-11" style="background-position: 0 -450px;"></i>
                我的门票
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/myGuest');?>">
                <i class="i-11" style="background-position: 0 -479px;"></i>
                我的直客
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/providerQuery');?>">
                <i class="i-11" style="background-position: 0 -308px;"></i>
                供应商查询
            </a>
        </li>
        <li>
            <a href="<?php echo U('Personal/userFeedback');?>">
                <i class="i-11" style="background-position: 0 -308px;"></i>
                C客户意见反馈
            </a>
        </li>
        <li>
            <a href="<?php echo U('Login/logout');?>">
                <i class="i-6"></i>
                退出登录
            </a>
        </li>
    </ul>
    <div class="actBot">
        <ul class="list clearfix">
            <li>
                <?php if($_SESSION['isLogin']== yes): ?><a href="javascript:void(0)" class="shopKeeper">
                        <i class="i-1"></i>我要开店
                    </a>
                    <?php else: ?>
                    <a href="javascript:void(0)" class="stores">
                        <i class="i-1"></i>我要开店
                    </a><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo U('Login/index');?>">
                    <i class="i-2"></i>个人中心
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" class="friendRemind">
                    <i class="i-3"></i>意见反馈
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" class="contact">
                    <i class="i-4"></i>联系方式
                </a>
            </li>
        </ul>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，如有问题请联系：<?php echo ($agency["mobile"]); ?></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade popWin bs-example-modal-sm" id="myModalstore" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，您已是店老大了哦</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalstores" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，想开店联系店主，电话：<?php echo ($$agency["mobile"]); ?></i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalNotice" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">温馨提示：功能完善中</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>