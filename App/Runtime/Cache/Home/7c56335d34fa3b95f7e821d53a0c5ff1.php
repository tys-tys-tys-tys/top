<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>乘机人</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div>乘机人</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>

<div class="wapper ucenter" style="background-color: white;padding-left:15px;padding-right:15px;overflow: hidden">
    <input type="hidden" class="flightNo" value="<?php echo ($flightNo); ?>">
    <input type="hidden" class="depCode" value="<?php echo ($depCode); ?>">
    <input type="hidden" class="arrCode" value="<?php echo ($arrCode); ?>">
    <input type="hidden" class="depDate" value="<?php echo ($depDate); ?>">
    <input type="hidden" class="depTime" value="<?php echo ($depTime); ?>">
    <input type="hidden" class="arrTime" value="<?php echo ($arrTime); ?>">
    <input type="hidden" class="planeModel" value="<?php echo ($planeModel); ?>">
    <input type='hidden' class='seatCode' value='<?php echo ($seatCode); ?>'>
    <input type='hidden' class='agencyId' value='<?php echo ($agencyId); ?>'>
    <input type="hidden" class="orgJetquay" value="<?php echo ($orgJetquay); ?>">
    <input type="hidden" class="dstJetquay" value="<?php echo ($dstJetquay); ?>">
    <input type="hidden" class="arrDate" value="<?php echo ($arrDate); ?>">

    <input type="hidden" class="parPrice" value="<?php echo ($parPrice); ?>">
    <input type="hidden" class="settlePrice" value="<?php echo ($settlePrice); ?>">
    <input type="hidden" class="airportTax" value="<?php echo ($airportTax); ?>">
    <input type="hidden" class="fuelTax" value="<?php echo ($fuelTax); ?>">
    <input type="hidden" class="type" value="<?php echo ($type); ?>">
    <input type="hidden" class="policyId" value="<?php echo ($policyId); ?>">

    <input type="hidden" class="flightOrderCreate" value="<?php echo (session('flightOrderCreate')); ?>">
    <input type="hidden" class="createUrl" value="<?php echo U('Flight/orderCreate');?>">
    <input type="hidden" class="addUrl" value="<?php echo U('Flight/addFrequentPassenger');?>">

    <input type="button" class="btn btn-success btn-block addFrequentPassenger" value="新增乘机人"
           style="margin-top:10px;margin-bottom: 15px;">

    <?php if($list[0]['passenger_name'] != ''): ?><input type="button" class="btn btn-success btn-block submitPassengerInfo" value="确定"
               style="margin-top:10px;margin-bottom: 15px;"><?php endif; ?>


    <?php if(is_array($list)): foreach($list as $key=>$vo): ?><div class="row" style="padding-top: 10px;">
            <div class="col-xs-1 col-md-1">
                <?php if($vo['passenger_type'] == 0): ?><input type="checkbox" name="passengerId" value="<?php echo ($vo["id"]); ?>" class="adultCheckBox"
                    <?php if($vo['is_checked'] == 1): ?>checked<?php endif; ?>
                    >
                <?php else: ?>
                    <input type="checkbox" name="passengerId" class="childCheckBox" value="<?php echo ($vo["id"]); ?>"
                    <?php if($vo['is_checked'] == 1): ?>checked<?php endif; ?>
                    ><?php endif; ?>
            </div>
            <div class="col-xs-8 col-md-7">
                <b>
                    <?php if($vo['passenger_type'] == 1): ?><span style="color: #239259">*</span><?php endif; ?>
                    <?php echo ($vo["passenger_name"]); ?><!--<?php echo ($seatCode); ?>--></b>
            </div>
            <input type="hidden" class="editUrl" value="<?php echo U('Flight/addFrequentPassenger');?>">

            <div class="col-xs-3 col-md-4 glyphicon glyphicon-pencil editPassenger"
                 style="color: #239259;"><input type="hidden" class="id" value="<?php echo ($vo["id"]); ?>">
            </div>
        </div>

        <div class="row" style="border-bottom: 1px solid #d0d0d0;padding-bottom: 10px;">
            <div class="col-xs-1"></div>
            <div class="col-xs-3 col-md-3">
                <?php if($vo['identity_type'] == 1): ?>身份证
                    <?php elseif($vo['identity_type'] == 2): ?>
                    护照
                    <?php elseif($vo['identity_type'] == 3): ?>
                    军官证
                    <?php elseif($vo['identity_type'] == 4): ?>
                    士兵证
                    <?php elseif($vo['identity_type'] == 5): ?>
                    台胞证
                    <?php elseif($vo['identity_type'] == 6): ?>
                    港澳通行证<?php endif; ?>
            </div>
            <div class="col-xs-6">
                <?php echo ($vo["identity_no"]); ?>
            </div>
        </div><?php endforeach; endif; ?>


    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，您最多可以添加6位乘机人</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitWarning" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                </div>
            </div>
        </div>
    </div>
    <p></p>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        <script src="/Public/js/flightOrderInfo.js?v=2"></script>
    </footer>
</div>
</body>
</html>