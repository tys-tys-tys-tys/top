<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>我的手机旅行社</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>
<body>
<div class="wapper">
    <header class="header clearfix">
        <?php if($agency['bg_pic'] != ''): ?><div class="bg" style="background: url('<?php echo ($bgPic); ?>')">
                <span style="float:right;margin-top:125px;margin-right:11px;color:#DCAC6C">长按识别二维码</span>
            </div>
            <?php else: ?>
            <div class="bg" style="background: url('http://demo.dalvu.com/public/images/header-bg.jpg')">
                <span style="float:right;margin-top:125px;margin-right:11px;color:#DCAC6C">长按识别二维码</span>
            </div><?php endif; ?>

        <dl class="adviser pull-left">
            <dt>
                <a href="<?php echo U('WxJsAPI/personal');?>">
                    <img class="lazy headPic" data-original="<?php echo ($headPic); ?>" alt="">
                </a>
            </dt>
            <dd>
                <p class="name">
                    <?php if($userList['nick_name'] == ''): echo ($userList['name']); ?>
                        <?php else: ?>
                        <?php echo ($userList['nick_name']); endif; ?>
            <?php echo $signPackage["appId"];?>
                </p>
                <a style="color:white;"
                   href="tel:<?php echo ($userList['mobile']); ?>">TEL:<?php echo ($userList['mobile']); ?></a>
            </dd>
        </dl>
        <?php if($agency['weixin_qrcode'] != ''): ?><img src="<?php echo ($weixinQrcode); ?>" class="pull-right codeimg"/>
            <?php else: ?>
            <img src="http://cdn.lydlr.com/public/images/temp/2.jpg" class="pull-right codeimg"><?php endif; ?>
    </header>
<!--    <div id="scroll" class="text-left"
         style="height: 30px;line-height: 30px;width: 100%;padding-left:10px;background-color: #6C6C6C">
        <ul>
            <?php if($marqueeNum == 1): ?><a href="<?php echo U('Agency/details',array('id' => $promotionList[0]['id']));?>">
                    <img src="/Public/images/t-1.png">
                    <span style="color: white"><?php echo (msubstr($promotionList["0"]["name"],0,15, 'utf-8', false)); ?></span>
                    <span class="pull-right" style="color:orangered;padding-right:10px;">详情</span>
                </a><?php endif; ?>
            <?php if($marqueeNum > 1): if(is_array($promotionList)): $i = 0; $__LIST__ = $promotionList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                        <a href="<?php echo U('Agency/details',array('id' => $vo['id']));?>">
                            <img src="/Public/images/t-1.png">
                            <span style="color: white"><?php echo (msubstr($vo["name"],0,20, 'utf-8', false)); ?></span>
                            <span class="pull-right" style="color:orangered;padding-right:10px;">详情</span>
                        </a>
                    </li><?php endforeach; endif; else: echo "" ;endif; endif; ?>
        </ul>

    </div>-->
    <div class="module">
        <ul class="navlist clearfix">
            <?php if(is_array($Column_list)): $i = 0; $__LIST__ = $Column_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                <a href="<?php echo ($vo["url"]); ?>">
                    <span class="icon i-<?php echo ($vo["icon"]); ?>"></span>

                    <p><?php echo ($vo["name"]); ?></p>
                </a>
              </li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
        <div class="searchbox">
            <form method="post" action="">
                <input type="text" class="txt" name="names" placeholder="支持模糊搜索：线路、景点、代码"/>
                <input type="submit" value="搜索" class="sbtn"/>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
<?php if($search == 0): ?><div class="module recLine">
        <h2 class="h2t"><i></i>限时特价</h2>

        <div class="con">
            <?php if(is_array($topTours)): $i = 0; $__LIST__ = $topTours;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dl class="item clearfix">
                    <dt>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <?php if($vo['cover_pic'] == ''): ?><img class='lazy headPic'
                                     data-original="http://cdn.lydlr.com/public/images/temp/1.jpg"/>
                                <?php else: ?>
                                <img class='lazy headPic' data-original="<?php echo ($Pic_topTours[$key]); ?>"/><?php endif; ?>
                        </a>
                    </dt>
                    <dd>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <p class="name" style="overflow: visible;height: auto"><?php echo (msubstr($vo["name"],0,10,'utf-8',false)); ?></p>
                            <span style="background-color:#1e86d4;color:white;padding: 2px 5px; border-radius: 3px;"><?php echo (getOneDeparture($vo["departure"])); ?> 出发</span>
                            <span class="price"><dfn>&yen;<?php echo (GetYuan($vo["min_price"])); ?></dfn>起</span>
                        </a>
                    </dd>

                </dl><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
    <?php if(!empty($agencyMyTourList)): ?><div class="module recLine">
        <h2 class="h2t"><i></i>推荐线路</h2>

        <div class="con">
            <?php if(is_array($agencyMyTourList)): $i = 0; $__LIST__ = $agencyMyTourList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dl class="item clearfix">
                    <dt>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <?php if($vo['cover_pic'] == ''): ?><img class='lazy headPic'
                                     data-original="http://cdn.lydlr.com/public/images/temp/1.jpg"/>
                                <?php else: ?>
                                <img class='lazy headPic' data-original="<?php echo ($Pic_agencyMyTourList[$key]); ?>"/><?php endif; ?>
                        </a>
                    </dt>
                    <dd>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <p class="name" style="overflow: visible;height: auto"><?php echo (msubstr($vo["name"],0,10,'utf-8',false)); ?></p>
                            <span style="background-color:#1e86d4;color:white;padding: 2px 5px; border-radius: 3px;"><?php echo (getOneDeparture($vo["departure"])); ?> 出发</span>
                            <span class="price"><dfn>&yen;<?php echo (GetYuan($vo["min_price"])); ?></dfn>起</span>
                        </a>
                    </dd>

                </dl><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div><?php endif; ?>
    <div class="module recLine">
        <h2 class="h2t"><i></i>优选线路</h2>

        <div class="con">
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dl class="item clearfix">
                    <dt>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">

                            <?php if($vo['cover_pic'] == ''): ?><img class='lazy headPic'
                                     data-original="http://cdn.lydlr.com/public/images/temp/1.jpg"/>
                                <?php else: ?>
                                <img class='lazy headPic' data-original="<?php echo ($Pic[$key]); ?>"/><?php endif; ?>
                        </a>
                    </dt>
                    <dd>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <p class="name" style="overflow: visible;height: auto"><?php echo (msubstr($vo["name"],0,10,'utf-8',false)); ?></p>
                            <span style="background-color:#1e86d4;color:white;padding: 2px 5px; border-radius: 3px;"><?php echo (getOneDeparture($vo["departure"])); ?> 出发</span>
                            <span class="price"><dfn>&yen;<?php echo (GetYuan($vo["min_price"])); ?></dfn>起</span>
                        </a>
                    </dd>

                </dl><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
    </div>
    <?php else: ?>
    <div class="module recLine">
        <h2 class="h2t"><i></i>搜素线路</h2>

        <div class="con">
            <?php if(empty($list)): ?><div class="alert alert-danger">您搜索的内容不存在, 请重新输入</div>
             <?php else: ?>
              <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><dl class="item clearfix">
                    <dt>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">

                            <?php if($vo['cover_pic'] == ''): ?><img class='lazy headPic'
                                     data-original="http://cdn.lydlr.com/public/images/temp/1.jpg"/>
                                <?php else: ?>
                                <img class='lazy headPic' data-original="<?php echo ($Pic[$key]); ?>"/><?php endif; ?>
                        </a>
                    </dt>
                    <dd>
                        <a href="<?php echo U('Agency/details',array('id'=>$vo['id']));?>">
                            <p class="name" style="overflow: visible;height: auto"><?php echo (msubstr($vo["name"],0,10,'utf-8',false)); ?></p>
                            <span style="background-color:#1e86d4;color:white;padding: 2px 5px; border-radius: 3px;"><?php echo (getOneDeparture($vo["departure"])); ?> 出发</span>
                            <span class="price"><dfn>&yen;<?php echo (GetYuan($vo["min_price"])); ?></dfn>起</span>
                        </a>
                    </dd>

                </dl><?php endforeach; endif; else: echo "" ;endif; endif; ?>   
        </div>
    </div><?php endif; ?>
<!--    <div class="module allDes">
        <h2 class="h2t"><i></i>全部目的地</h2>

        <div class="con">
            <ul class="list">
                <li>
                    <h3 class="h3t"><a href="#">国内</a></h3>

                    <div class="item clearfix">
                        <?php if(is_array($inboundDestinationList)): foreach($inboundDestinationList as $key=>$vo): ?><div class="col-4"><a
                                    href="<?php echo U('Inbound/lists',array('id'=>$vo['id']));?>"><?php echo ($vo["destination"]); ?></a></div><?php endforeach; endif; ?>
                        <div class="col-4 more"><a href="<?php echo U('Inbound/index');?>">更多<em>+</em></a></div>
                    </div>
                </li>
                <li class="last">
                    <h3 class="h3t"><a href="#">出境</a></h3>

                    <div class="item clearfix">
                        <?php if(is_array($outboundDestinationList)): foreach($outboundDestinationList as $key=>$vo): ?><div class="col-4"><a
                                    href="<?php echo U('Outbound/lists',array('id'=>$vo['id']));?>"><?php echo ($vo["destination"]); ?></a></div><?php endforeach; endif; ?>
                        <div class="col-4 more"><a href="<?php echo U('Outbound/index');?>">更多<em>+</em></a></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>-->

    <footer style="display: none">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
    <p style="background-color: #F4F4F4;height:75px;border:0;padding:0;line-height: 20px;margin-top: 0px;font-family: '微软雅黑';font-size:13px;"
       class="text-center">大旅提供技术支持</p>


    <div class="actBot navbar-fixed-bottom"
         style="padding: 0;background-color: white;max-width: 720px;margin: 0 auto;font-size: 12px;">
        <ul class="list clearfix">
            <li>
                <?php if($_SESSION['isLogin']== yes): ?><a href="javascript:void(0)" class="shopKeeper">
                        <i class="i-1"></i>我要开店
                    </a>
                    <?php else: ?>
                    <a href="javascript:void(0)" class="stores">
                        <i class="i-1"></i>我要开店
                    </a><?php endif; ?>
            </li>
            <li>
                <a href="<?php echo U('WxJsAPI/personal');?>">
                    <i class="i-2"></i>个人中心
                </a>
            </li>
            <li>
            <?php if($_SESSION['isLogin']== yes): ?><a href="javascript:void(0)" class="friendRemind_login">
                    <i class="i-3"></i>意见反馈
                </a>
                <?php else: ?>
                <a href="javascript:void(0)" class="friendRemind">
                    <i class="i-3"></i>意见反馈
                </a><?php endif; ?>
            </li>
            <li>
                <a href="tel:<?php echo ($userList['mobile']); ?>" class="contactff">
                    <i class="i-4"></i>联系方式
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="suc">亲，如有问题请联系：<?php echo ($userList["mobile"]); ?></i></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade popWin bs-example-modal-sm" id="myModalstore" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="suc">亲，您已是店老大了哦</i></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalstores" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="suc">亲，想开店联系店主，电话：<?php echo ($userList["mobile"]); ?></i></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalNotice" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="suc">评论</i></p>
                <input type="number" id="dl_star" >
                <input type="text" id="comm_content" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close" id="saveAdviserFeedback">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade popWin bs-example-modal-sm" id="myModalSupplier" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="suc">评论</i></p>
                <select name="supplier" id='dl_supplier_id'> 
                <option value="0"  selected = "selected">请选择供应商</option> 
                <?php if(is_array($supplier)): $i = 0; $__LIST__ = $supplier;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select><br/>
                <input type="number" id="dl_star_login" ><br/>
                <input type="text" id="comm_content_login" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close" id="saveAdviserFeedback_login">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>
    <input type="hidden" id="feddback_url" value="<?php echo U('personal/agencyFeedback');?>">
<script src="/Public/js/scroll.js?v=1"></script>
<script src="/Public/js/friendRemind.js?v=1"></script>
<script>
    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "我的手机旅行社",
            title: "<?php echo ($title); ?>",
            link: "<?php echo ($domain); ?>",
            imgUrl: '<?php echo ($headPic); ?>',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
</body>
</html>