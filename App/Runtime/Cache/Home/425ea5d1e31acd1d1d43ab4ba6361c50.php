<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>发票申请</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">发票申请</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <form method="post" action="" id="signupForm" class="zcform">
        <input id="applyInvoice" type="hidden" value="<?php echo U('Finance/applyInvoice');?>"/>
        <input id="invoiceList" type="hidden" value="<?php echo U('Finance/index');?>"/>
        <input type="hidden" id="code" value="<?php echo (session('applyInvoice')); ?>">

        <div class="fpApply">
            <label class="t">发票抬头</label>

            <div class="input-group">
                <span class="input-group-addon">公司名称</span>
                <input type="text" class="form-control" name="title" id="title"/>
            </div>
            <label class="t">发票项目</label>

            <div class="input-group">
                <span class="input-group-addon">请选择发票项目</span>
                <select class="form-control" name="detail" id="detail">
                    <?php if(is_array($invoiceTypeList)): foreach($invoiceTypeList as $key=>$vo): ?><option value="<?php echo ($vo["detail"]); ?>"><?php echo ($vo["detail"]); ?></option><?php endforeach; endif; ?>

                </select>
            </div>
            <label class="t">剩余发票额度: <span style="color: #f08519;"><?php echo (getYuan($lastTotal)); ?></span></label>

            <div class="input-group">

            </div>
            <label class="t">发票金额</label>

            <div class="input-group">
                <span class="input-group-addon">请输入发票金额</span>
                <input type="number" class="form-control" name="amount" id="amount"/>
            </div>
            <label class="t">索取方式</label>

            <div class="input-group clearfix">
                <div class="radio">
                    <label>
                        <input type="radio" name="request_method" value="2" checked data-num="1" autocomplete="off">
                        快递
                    </label>
                </div>
                <div class="radio r-2">
                    <label>
                        <input type="radio" name="request_method" value="1" data-num="2" autocomplete="off">
                        自取
                    </label>
                </div>
            </div>
            <div id="kd">
                <label class="t">快递选择</label>

                <div class="input-group clearfix">
                    <div class="radio">
                        <label>
                            <input type="radio" name="express_fee" value="3" checked data-num="3" autocomplete="off">
                            到付 (货到付款)
                        </label>
                    </div>
                    <div class="radio r-2">
                        <label>
                            <input type="radio" name="express_fee" value="4" data-num="4" autocomplete="off">
                            邮寄 (支付<?php echo (GetYuanWhole($invoiceFeeInfo["express_fee"])); ?>元快递费用)
                        </label>
                    </div>
                </div>
                <div class="item-group clearfix">
                    <label class="t">邮寄地址</label>
                    <textarea class="form-control" id="addr" name="addr" rows="3" placeholder="请输入邮寄地址"></textarea>
                </div>
                <div class="item-group clearfix">
                    <label class="t">联&nbsp;系&nbsp;人</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="请输入联系人">
                </div>
                <div class="item-group clearfix">
                    <label class="t">联系电话</label>
                    <input type="number" class="form-control" id="phone" name="phone" placeholder="请输入手机号">
                </div>
                <div class="item-group clearfix">
                    <label class="t">备注</label>
                    <input type="text" class="form-control" id="remark" name="remark" placeholder="请输入备注信息">
                </div>
            </div>
            <div id="zq" style="display:none;">
                <div class="item-group clearfix">
                    <label class="t">公司地址</label>
                    <input type="text" class="form-control" value="<?php echo ($address); ?>" disabled>
                </div>
                <div class="item-group clearfix">
                    <label class="t">联系电话</label>
                    <input type="text" class="form-control" value="<?php echo ($tel); ?>" disabled>
                </div>
            </div>
            <div class="pad"><input type="button" id="doSubmitApplyInvoice"
                                    class="btn btn-primary btn-lg btn-block proofAgain" value="提交申请"/></div>
        </div>
    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalDel" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>请将信息填写完整</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>申请成功</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitApplyInvoice doSubmit" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>

</body>
</html>