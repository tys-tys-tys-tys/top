<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>机票列表</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">
        <?php if($flightinfo[0]['orgCity'] == 'NAY'): ?>北京
            <?php elseif($flightinfo[0]['orgCity'] == 'PVG'): ?>
            上海
            <?php else: ?>
            <?php echo ($flightinfo["0"]["depname"]); endif; ?>
        -
        <?php if($flightinfo[0]['dstCity'] == 'NAY'): ?>北京
            <?php elseif($flightinfo[0]['dstCity'] == 'PVG'): ?>
            上海
            <?php else: ?>
            <?php echo ($flightinfo["0"]["arrname"]); endif; ?>
    </div>
    <a href="javascript:" onclick="self.location=document.referrer;" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper ucenter" style="overflow: hidden;">
    <div class="row flight-row-p1 flight-list-t">
        <div class="col-xs-3 text-center flight-list-text-l flight-row-s1">
            <form method="post" action="<?php echo U('Flight/flightQueryList');?>">
                <input type="hidden" name="dayBefore" value="<?php echo ($flightinfo["0"]["day"]); ?>">
                <input type="hidden" name="departure" value="<?php echo ($flightinfo["0"]["orgCity"]); ?>">
                <input type="hidden" name="destination" value="<?php echo ($flightinfo["0"]["dstCity"]); ?>">
                <input type="submit" value="< 前一天" style="border:none;background: #E5E5E5">
            </form>
        </div>

        <div class="col-xs-6 text-center flight-list-text-l flight-row-s1">
            <input id="d4322" class="Wdate ui-datepicker" type="text" onclick="WdatePicker({el: $dp.$('d12')})"
                   placeholder="<?php echo ($date); ?>" name="start_time" disabled='disabled'>
        </div>

        <div class="col-xs-3 text-center flight-list-text-l flight-row-s1">
            <form method="post" action="<?php echo U('Flight/flightQueryList');?>">
                <input type="hidden" name="dayAfter" value="<?php echo ($flightinfo["0"]["day"]); ?>">
                <input type="hidden" name="departure" value="<?php echo ($flightinfo["0"]["orgCity"]); ?>">
                <input type="hidden" name="destination" value="<?php echo ($flightinfo["0"]["dstCity"]); ?>">
                <input type="submit" value="后一天 >" style="border:none;background: #E5E5E5">
            </form>
        </div>
    </div>

    <br/>
    <?php if(is_array($flightinfo)): foreach($flightinfo as $key=>$vo): if($vo['days'] >= $now): ?><div class="flight-list">
                <div class="blockItem">
                    <div data-event-trigger>
                        <table style="width: 100%">
                            <tr>
                                <td width="40%" class="text-left flight-list-t-l1"><b><?php echo (returnTime($vo["depTime"])); ?></b></td>
                                <td width="20%" class="text-center flight-list-t-l1"><img
                                        src="http://cdn.lydlr.com/public/images/flight/ticket_10.png" width="70%"
                                        style="margin-bottom: 2px;"></td>
                                <td width="40%" class="text-right flight-list-t-r1"><b><?php echo (returnTime($vo["arriTime"])); ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td width="40%" class="text-left flight-list-t-l1">
                                    <?php if($vo['orgCity'] == 'PVG'): ?>浦东机场
                                        <?php elseif($vo['orgCity'] == 'NAY'): ?>
                                        南苑机场
                                        <?php else: ?>
                                        <?php echo (nameToAirport($vo["orgCity"])); endif; ?>
                                    <?php if(($vo['orgJetquay'] == 'Array') or ($vo['orgJetquay'] == '--')): else: ?>
                                        <?php echo ($vo["orgJetquay"]); endif; ?>
                                </td>
                                <td width="20%">&nbsp;</td>
                                <td width="40%" class="text-right flight-list-t-r1">
                                    <?php if($vo['dstCity'] == 'PVG'): ?>浦东机场
                                        <?php elseif($vo['dstCity'] == 'NAY'): ?>
                                        南苑机场
                                        <?php else: ?>
                                        <?php echo (nameToAirport($vo["dstCity"])); endif; ?>

                                    <?php if(($vo['dstJetquay'] == 'Array') or ($vo['dstJetquay'] == '--')): else: ?>
                                        <?php echo ($vo["dstJetquay"]); endif; ?>
                                </td>

                            </tr>
                            <tr>
                                <td width="100%" colspan="3" class="flight-list-t-l1">
                                    <?php echo (nameToAirLine(substr($vo["flightNo"],0,2))); ?> <?php echo ($vo["flightNo"]); ?> | <?php echo ($vo["planeType"]); ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="3" class="text-right flight-list-t-r1">
                                    <span style="color:#F0AD4E ">查看全部舱位</span>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <p></p>


                    <?php if(is_array($vo["price"])): foreach($vo["price"] as $key=>$v): ?><div class="con">
                            <table style="width: 100%">
                                <tr style="border:0px;">
                                    <td width="37%">
                                        <span class='flight-list-t-l' style="margin-left: 10px;">
                                        <?php if(($v['seatMsg'] == '高端经济舱') or ($v['seatMsg'] == '超级经济舱') or ($v['seatMsg'] == '特价舱位')): ?>经济舱
                                            <?php else: ?>
                                            <?php echo ($v["seatMsg"]); endif; ?><!--<?php echo ($v["seatCode"]); echo ($v["commisionPoint"]); ?>--><?php echo ($v["seatCode"]); ?>&nbsp;&nbsp;&nbsp;<?php echo (getDisCount($v["agio"])); ?> 折
                                        <?php if(($_SESSION['isLogin']== yes) and ($v['needSwitchPNR'] == 1)): ?><span style="color:red">*</span><?php endif; ?>
                                        </span>
                                    </td>
                                    <td class="flight-list-t-l text-right" width="33%">
                                        <?php if($v['ticketnum'] == 'A'): elseif($v['ticketnum'] == 'L'): ?>
                                            <?php elseif($v['ticketnum'] == 'Q'): ?>
                                            <?php elseif($v['ticketnum'] == 'S'): ?>
                                            <?php elseif($v['ticketnum'] == 'C'): ?>
                                            <?php elseif($v['ticketnum'] == 'X'): ?>
                                            <?php elseif($v['ticketnum'] == 'Z'): ?>
                                            <?php else: ?>
                                            (余 <span class='flight-list-t-r'><?php echo ($v["ticketnum"]); ?> </span>张)<?php endif; ?>


                                        <?php if($_SESSION['isLogin']== yes): ?><span class='flight-list-t-r'>&yen;<?php echo ($v['settlePrice'] + $vo['fuelTax'] + $vo['airportTax']); ?></span>
                                            <?php else: ?>
                                    <span class='flight-list-t-r'>
                                        &yen;<?php echo ($v["parPrice"]); ?>
                                    </span><?php endif; ?>
                                    </td>
                                    <td width="30%" class="text-center">
                                        <?php if($_SESSION['isLogin']== yes): ?><form method="post" action="<?php echo U('Flight/showOrderCreateInfo');?>">
                                                <input type="hidden" name="date" value="<?php echo ($date); ?>">
                                                <input type="hidden" name="type" value="0">
                                                <input type="hidden" name="flightNo" value="<?php echo ($vo["flightNo"]); ?>">
                                                <input type="hidden" name="orgCity" value="<?php echo ($vo["orgCity"]); ?>">
                                                <input type="hidden" name="dstCity" value="<?php echo ($vo["dstCity"]); ?>">
                                                <input type="hidden" name="depTime" value="<?php echo ($vo["depTime"]); ?>">
                                                <input type="hidden" name="arriTime" value="<?php echo ($vo["arriTime"]); ?>">
                                                <input type="hidden" name="settlePrice" value="<?php echo ($v["settlePrice"]); ?>">
                                                <input type="hidden" name="planeType" value="<?php echo ($vo["planeType"]); ?>">
                                                <input type="hidden" name="policyId" value="<?php echo ($v["policyId"]); ?>">
                                                <input type="hidden" name="seatCode" value="<?php echo ($v["seatCode"]); ?>">
                                                <input type="hidden" name="fuelTax" value="<?php echo ($vo["fuelTax"]); ?>">
                                                <input type="hidden" name="airportTax" value="<?php echo ($vo["airportTax"]); ?>">
                                                <input type="hidden" name="parPrice" value="<?php echo ($v["parPrice"]); ?>">
                                                <input type="hidden" name="orgJetquay" value="<?php echo ($vo["orgJetquay"]); ?>">
                                                <input type="hidden" name="dstJetquay" value="<?php echo ($vo["dstJetquay"]); ?>">
                                                <input type="hidden" name="arrDate" value="<?php echo ($vo["arrDate"]); ?>">
                                                <input type="hidden" name="commisionMoney" value="<?php echo ($v["commisionMoney"]); ?>">
                                                <input type="hidden" name="needSwitchPNR" value="<?php echo ($v["needSwitchPNR"]); ?>">
                                        <span class='flight-list-t-l'>
                                            <input type="submit" class="btn btn-warning btn-sm text-center"
                                                   style="height: 30px;line-height: 10px;" value="预订">
                                        </span>
                                            </form>
                                            <?php else: ?>
                                            <div class="detail-bottom clearfix">
                                                <a href="tel:<?php echo ($mobile); ?>"
                                                   style="background:#EFF0F0;height: 50px;line-height: 50px;width: 100%;font-size:14px;"><i></i>咨询</a>
                                            </div><?php endif; ?>
                                    </td>

                                </tr>
                                <?php if($_SESSION['isLogin']== yes): ?><tr>
                                        <td width="25%">
                                            &nbsp;
                                        </td>
                                        <td width="45%" class="text-right"><span
                                                style="border-top: 0px solid #ddd; padding: -8px;color:#33b3b8">
                                                <?php echo ($v['parPrice'] + $vo['fuelTax'] + $vo['airportTax']); ?>-<?php echo ($v['settlePrice'] + $vo['fuelTax'] + $vo['airportTax']); ?>=<?php echo ($v["earnPrice"]); ?></span>
                                        </td>
                                    </tr>
                                    <?php else: ?>
                                    <tr>
                                        <td colspan="2" width="70%">
                                            &nbsp;燃油税<span style="color:#F0AD4E"><?php echo ($vo["fuelTax"]); ?></span> 基建费<span
                                                style="color:#F0AD4E"><?php echo ($vo["airportTax"]); ?></span>
                                        </td>
                                        <td width="30%">&nbsp;</td>
                                    </tr><?php endif; ?>
                            </table>
                        </div><?php endforeach; endif; ?>
                </div>
            </div>
            <div class="p-line">&nbsp;</div><?php endif; endforeach; endif; ?>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">日期选择错误!</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>


<script type="text/javascript" src="http://cdn.lydlr.com/public/js/showDate/WdatePicker.js"></script>
</body>
</html>