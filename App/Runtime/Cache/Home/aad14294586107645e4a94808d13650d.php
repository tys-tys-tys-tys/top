<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>订单支付</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">订单支付</div>
    <a href="javascript:void(0);" class="back" onclick="window.location.href = document.referrer"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <form method="post" action="">
        <input id="preForum" type="hidden" value="<?php echo U('Personal/preForum');?>"/>
        <input type="hidden" id="code" value="<?php echo (session('preForumCode')); ?>">
        <input id="id" type="hidden" value="<?php echo ($orderTourList["id"]); ?>"/>
        <input id="keyid" type="hidden" value="<?php echo (setEncrypt($orderTourList["id"])); ?>"/>
        <input id="myorder" type="hidden" value="<?php echo U('Personal/myorder');?>"/>
        <input type="hidden" id="account_state" value="<?php echo ($userInfo["account_state"]); ?>">

        <div class="module orderList">
            <ul class="list">
                <li>
                    <h3 class="h3t"><?php echo ($orderTourList["name"]); ?></h3>
                    <dl class="clearfix">
                        <dt>
                            <?php if($orderTourList["cover_pic"] != ''): ?><img src="<?php echo ($pic); ?>"/>
                                <?php else: ?>
                                <img src="http://cdn.lydlr.com/public/images/temp/1.jpg"><?php endif; ?>
                        </dt>
                        <dd>
                            <p>
                                <span class="mr">
                                    成人 <dfn><?php echo ($orderTourList["client_adult_count"]); ?></dfn>
                                </span>
                                <span class="mr">
                                    儿童 <dfn><?php echo ($orderTourList["client_child_count"]); ?></dfn>
                                </span>
                                <span class="mr">团期<?php echo (substr($orderTourList["start_time"],0,10)); ?></span>
                            </p>

                            <p class="price">
                                订单金额： <em><dfn>&yen;<?php echo (getYuan($orderTourList["price_total"])); ?></dfn></em>
                            </p>

                            <p class="price">
                                调整金额： <em><dfn>&yen;<?php echo (getYuan($orderTourList["price_adjust"])); ?></dfn></em>
                            </p>

                            <p class="price">
                                应付金额： <em><dfn>&yen;<?php echo ($totalMoney); ?></dfn></em>
                            </p>

                            <p class="price">
                                尾款金额： <em><dfn>&yen;<?php echo ($preForum); ?>.00</dfn></em>
                            </p>

                            <p class="price">
                                特别说明： <em><dfn><?php echo ($orderTourList["memo"]); ?></dfn></em>
                            </p>
                            <?php if($orderTourList['state'] == 6): ?><p class="text-center" style="margin-top:10px;"><span class="text-large text-danger   ">您确认要支付尾款吗</span>
                                    &nbsp;&nbsp;<input type="button" value="确定"
                                                       class="btn btn-danger btn-sm proofAgain preForum"></p>
                                <?php else: ?>
                                <dfn>付款成功！您已付尾款</dfn><?php endif; ?>
                        </dd>
                    </dl>
                </li>
            </ul>
        </div>
    </form>
    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>付款成功</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitPrePayed" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default doReset doSubmitPrePayed" data-dismiss="modal"
                            aria-label="Close">取消
                    </button>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>