<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>订单支付</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div>订单支付</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<input type='hidden' id='orderPay' value="<?php echo U('Flight/orderPay');?>">
<input type='hidden' id='flightId' value='<?php echo ($flightId); ?>'>
<input type='hidden' id='keyid' value='<?php echo (setEncrypt($flightId)); ?>'>
<input type="hidden" id="orderNo" value="<?php echo ($orderNo); ?>">
<input type="hidden" id="flightOrderCode" value="<?php echo ($flightOrderCode); ?>">
<input type="hidden" id="seatCode" value="<?php echo ($seatCode); ?>">
<input type="hidden" id="flightNo" value="<?php echo ($flightNo); ?>">
<input type="hidden" id="code" value="<?php echo (session('flightOrderCode')); ?>">

<div class="wapper ucenter" style="overflow: hidden">
    <table class="table" style='width:100%'>
        <tr>
            <td class="text-center" colspan="5"><h4>订单详情</h4></td>
        </tr>
        <tr>
            <td class="text-left" colspan="5">航班信息: &nbsp; <?php echo (nameToAirLine(substr($flightNo,0,2))); ?> <?php echo ($flightNo); ?><span
                    style='color:#EC975B'>|</span><?php echo ($planeModel); ?>
            </td>
        </tr>
        <tr>
            <th class="text-left" width="20%">乘客</th>
            <th class="text-left" width='15%'>类型</th>
            <th class="text-left" width='29%'>结算价</th>
            <th class="text-left" width='20%'>燃油</th>
            <th class="text-left" width='20%'>基建费</th>
        </tr>
        <?php if(is_array($orderList)): foreach($orderList as $key=>$vo): ?><tr>
                <td class="text-left" width='15%'><?php echo ($vo["passenger_name"]); ?></td>
                <td class="text-left" width='20%'>
                    <?php if($vo['passenger_type'] == 0): ?>成人
                        <?php else: ?>
                        儿童<?php endif; ?>
                </td>
                <td class="text-left" width='20%'>
                    <span class='flight-text-c2'>&yen; <?php echo ($vo["par_price"]); ?></span>
                </td>
                <td class="text-left" width='20%'>
                    <span class='flight-text-c2'>&yen;<?php echo ($vo["fuel_tax"]); ?></span>
                </td>
                <td class="text-left" width='20%'>
                    <?php if($vo['passenger_type'] == 0): ?><span class='flight-text-c2'>&yen;<?php echo ($vo["airport_tax"]); ?></span>
                        <?php else: ?>
                        <span class='flight-text-c2'>&yen;0.00</span><?php endif; ?>
                </td>

            </tr><?php endforeach; endif; ?>
        <tr>
            <td class="text-left" colspan="5">起飞日期: &nbsp;<?php echo ($depDate); ?></td>
        </tr>
        <tr>
            <td class="text-left" colspan="5">起降机场: &nbsp;
                <?php echo (returnTime($depTime)); ?>
                <?php if($depCode == 'NAY'): ?>南苑机场
                    <?php elseif($depCode == 'PVG'): ?>
                    浦东机场
                    <?php else: ?>
                    <?php echo (nameToAirport($depCode)); endif; ?>
                <?php if(($orgJetquay == 'Array') or ($orgJetquay == '--')): else: ?>
                    <?php echo ($orgJetquay); endif; ?>
                --

                <?php echo (returnTime($arrTime)); ?>
                <?php if($arrCode == 'NAY'): ?>南苑机场
                    <?php elseif($arrCode == 'PVG'): ?>
                    浦东机场
                    <?php else: ?>
                    <?php echo (nameToAirport($arrCode)); endif; ?>
                <?php if(($dstJetquay == 'Array') or ($dstJetquay == '--')): else: ?>
                    <?php echo ($dstJetquay); endif; ?>
            </td>
        </tr>
        <tr>
            <td class="text-left" colspan="5"><span style='margin-left:10px;'>联系人:</span> &nbsp;<?php echo ($linkMan); ?></td>
        </tr>
        <tr>
            <td class="text-left" colspan="5">联系电话: &nbsp;<?php echo ($linkPhone); ?></td>
        </tr>
        <tr>
            <td class="text-left" colspan="5">
                订单总额: &nbsp;
                <span class="flight-text-c2">&yen;<span class='priceTotal' style="font-size:18px;"><?php echo ($priceTotal); ?></span></span>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="text-right showButton">
                    <span class="payContent">
                        <span style="font-size: 20px">您确定要付款吗</span> <input type='button'
                                                                            class='btn btn-danger btn-sm doSubmitFlightOrder proofAgain'
                                                                            style="width:80px;height: 40px;" value='确定'>
                    </span>
            </td>
            <td colspan="5" class="text-right showCircle" style="display:none">
                <div style="float:right">
                    <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="circle" class="g-circles g-circles--v1">
                            <circle id="12" transform="translate(35, 16.698730) rotate(-30) translate(-35, -16.698730) "
                                    cx="35" cy="16.6987298" r="10"></circle>
                            <circle id="11" transform="translate(16.698730, 35) rotate(-60) translate(-16.698730, -35) "
                                    cx="16.6987298" cy="35" r="10"></circle>
                            <circle id="10" transform="translate(10, 60) rotate(-90) translate(-10, -60) " cx="10"
                                    cy="60" r="10"></circle>
                            <circle id="9" transform="translate(16.698730, 85) rotate(-120) translate(-16.698730, -85) "
                                    cx="16.6987298" cy="85" r="10"></circle>
                            <circle id="8"
                                    transform="translate(35, 103.301270) rotate(-150) translate(-35, -103.301270) "
                                    cx="35" cy="103.30127" r="10"></circle>
                            <circle id="7" cx="60" cy="110" r="10"></circle>
                            <circle id="6"
                                    transform="translate(85, 103.301270) rotate(-30) translate(-85, -103.301270) "
                                    cx="85" cy="103.30127" r="10"></circle>
                            <circle id="5"
                                    transform="translate(103.301270, 85) rotate(-60) translate(-103.301270, -85) "
                                    cx="103.30127" cy="85" r="10"></circle>
                            <circle id="4" transform="translate(110, 60) rotate(-90) translate(-110, -60) " cx="110"
                                    cy="60" r="10"></circle>
                            <circle id="3"
                                    transform="translate(103.301270, 35) rotate(-120) translate(-103.301270, -35) "
                                    cx="103.30127" cy="35" r="10"></circle>
                            <circle id="2" transform="translate(85, 16.698730) rotate(-150) translate(-85, -16.698730) "
                                    cx="85" cy="16.6987298" r="10"></circle>
                            <circle id="1" cx="60" cy="10" r="10"></circle>
                        </g>
                        <use xlink:href="#circle" class="use"/>
                    </svg>
                </div>
                <span class="circle-size">订单支付中, 请勿关闭订单</span>
            </td>
            <td colspan='5' class='text-right showJumpButton' style='display: none'>
                <a href="<?php echo U('Flight/myPlaneTicket');?>">
                    <input type='button' class='btn btn-info' value="我的机票"/>
                </a>
            </td>
            <td colspan='5' class='text-right showTopupButton' style='display: none'>
                <a href="<?php echo U('Finance/applyTopup');?>">
                    <input type='button' class='btn btn-info' value="立即充值"/>
                </a>
            </td>
        </tr>
    </table>
    <div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">温馨提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc">亲，您最多可以添加6位乘机人</i></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmitWarning" data-dismiss="modal"
                            aria-label="Close">确认
                    </button>
                </div>
            </div>
        </div>
    </div>
    <p></p>
    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
<script src="/Public/js/flightOrder.js?v=20"></script>
</html>