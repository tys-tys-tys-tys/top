<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>代理商加盟表</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body style="background: #fff">

<div class="container">
    <table class="table table-hover table-bordered">
        <thead><h3 class="text-center">代理商加盟表</h3></thead>
        <tr>
            <th width="10%" class="text-center">姓名</th>
            <th width="10%" class="text-center">手机</th>
            <th width="10%" class="text-center">城市</th>
            <th width="10%" class="text-center">备注</th>
            <th width="10%" class="text-center">申请时间</th>
        </tr>

        <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
                <td class="text-center"><?php echo ($vo["name"]); ?></td>
                <td class="text-center"><?php echo ($vo["phone"]); ?></td>
                <td class="text-center"><?php echo ($vo["province"]); ?></td>
                <td class="text-center"><?php echo ($vo["remark"]); ?></td>
                <td class="text-center"><?php echo ($vo["create_time"]); ?></td>
            </tr><?php endforeach; endif; ?>
        <?php if(empty($info)): ?><td colspan="5" class="text-center">暂无数据</td><?php endif; ?>
    </table>

    <div class="pagebox text-center">
        <?php echo ($page); ?>
    </div>
</div>
</body>
</html>