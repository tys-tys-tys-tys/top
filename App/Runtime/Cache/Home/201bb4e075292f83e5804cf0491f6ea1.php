<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php if(empty($agencyTourTitle["title"])): echo ($list['name']); else: echo ($agencyTourTitle["title"]); endif; ?></title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <style>
        .detail-bottom a i {
            background: rgba(0, 0, 0, 0) url("http://cdn.lydlr.com/public/kangtai/images/detail-icon.png") no-repeat scroll 0 -496px / 40px auto;
            display: inline-block;
            height: 26px;
            margin-right: 5px;
            position: relative;
            top: 8px;
            width: 25px;
        }
    </style>
    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>
<body>
<div class="actNav">
    <div class="doRefresh">线路详情</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>

<div class="wapper detailPage">
    <form method="post" action="<?php echo U('Agency/details',array('id'=>$list['id']));?>">
        <input type="hidden" name="id" id="id" value="<?php echo ($list["id"]); ?>">
        <input id="doRecommend" type="hidden" value="<?php echo U('Agency/recommend');?>"/>

        <div class="module">
            <h1 class="title" style="padding:10px;overflow:visible;height:auto;line-height:25px;"><?php echo ($list["name"]); ?>  <?php if($_SESSION['isLogin']== yes): ?><a href="<?php echo U('Agency/changeTitle',array('id'=>$list['id']));?>" class="btn btn-warning pull-right">
                        改分享标题
                    </a><?php endif; ?></h1>

            <div class="price clearfix" style="border-bottom: 0px">
                <span class="w pull-left">
                    代码：<b><span style='color:#0564ac;font-size: 20px;'>10<?php echo ($list["id"]); ?></span>
                    (在首页搜索该代码可直达本页)</b>
                </span>
            </div>
            <?php if($_SESSION['isLogin']== yes): ?><div class="price clearfix" style="border-bottom: 0px">
                    <span class="w pull-left">
                          供应商：<b><?php echo ($list["provider_name"]); ?></b>
                    </span>
                </div>
                <div class="price clearfix" style="border-bottom: 0px">
                    <span class="w pull-left">
                          联系人：<b><?php echo ($list["contact_person"]); ?></b>
                    </span>
                </div><?php endif; ?>
            <div class="price clearfix">
                <span class="w pull-left">
                    价　格：<dfn>&yen;<?php echo (getYuan($list["min_price"])); ?></dfn>起
                </span>
                <?php if($_SESSION['isLogin']== yes): ?><a href="<?php echo U('Agency/changePrice',array('id'=>$list['id']));?>" class="btn btn-warning pull-right">
                        改价
                    </a><?php endif; ?>
            </div>


            <div class="price clearfix" style='border-top:0px'>
                <?php if(($_SESSION['isLogin']!= yes) and ($cruiseArr['0'] != '')): ?><div>
                        <select class="form-control start_time" name="start_time">
                            <?php if(empty($tourSkuDate)): ?><option value="">暂无班期</option>
                                <?php else: ?>
                                <?php if(is_array($tourSkuDate)): $i = 0; $__LIST__ = $tourSkuDate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["start_time"]); ?>">
                                        <?php echo ($vo["start_time"]); ?>&nbsp;&nbsp;&nbsp;
                                    </option><?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </select>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>房型</th>
                            <th>市场价</th>

                        </tr>
                        </thead>
                        <?php if(is_array($cruiseArr)): $k = 0; $__LIST__ = $cruiseArr;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tbody class="cruiseId">
                            <?php if(is_array($vo)): $i = 0; $__LIST__ = $vo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                                    <td><?php echo ($v["0"]); ?>&nbsp;<?php echo ($v["1"]); ?></td>
                                    <td><?php echo ($v["2"]); ?></td>
                                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                            </tbody><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>

                    <?php elseif(($_SESSION['isLogin']== yes) and ($cruiseArr['0'] != '')): ?>
                    <div>
                        <select class="form-control start_time" name="start_time">
                            <?php if(empty($tourSkuDate)): ?><option value="">暂无班期</option>
                                <?php else: ?>
                                <?php if(is_array($tourSkuDate)): $i = 0; $__LIST__ = $tourSkuDate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["start_time"]); ?>">
                                        <?php echo ($vo["start_time"]); ?>&nbsp;&nbsp;&nbsp;
                                    </option><?php endforeach; endif; else: echo "" ;endif; endif; ?>
                        </select>
                    </div>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>房型</th>
                            <th>市场价</th>
                            <th>同行价</th>
                        </tr>
                        </thead>
                        <?php if(is_array($cruiseArr)): $k = 0; $__LIST__ = $cruiseArr;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><tbody class="cruiseId">
                            <?php if(is_array($vo)): $i = 0; $__LIST__ = $vo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                                    <td><?php echo ($v["0"]); ?>&nbsp;<?php echo ($v["1"]); ?></td>
                                    <td><?php echo ($v["2"]); ?></td>
                                    <td><?php echo ($v["3"]); ?></td>
                                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                            </tbody><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>

                    <?php elseif(($_SESSION['isLogin']== yes) and ($cruiseArr['0'] == '')): ?>
                    <div class="module blockItem">
                        <div class="t" data-event-trigger><i class="icon i-5"></i>出团班期<span class="arrow"></span></div>
                        <div class="con pad">

                            <select class="form-control" name="start_time">
                                <?php if(empty($tourSkuDate)): ?><option value="">暂无班期</option>
                                    <?php else: ?>
                                    <?php if(is_array($tourSkuDate)): $i = 0; $__LIST__ = $tourSkuDate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["start_time"]); ?>">
                                            <?php echo ($vo["start_time"]); ?>&nbsp;&nbsp;&nbsp;零售价&yen;<?php echo (getYuan($vo["price_adult_list"])); ?> 同行价&yen;<?php echo (GetYuan($vo["price_adult_agency"])); ?>
                                        </option><?php endforeach; endif; else: echo "" ;endif; endif; ?>
                            </select>
                        </div>
                    </div>

                    <?php elseif(($_SESSION['isLogin']!= yes) and ($cruiseArr['0'] == '')): ?>
                    <div class="module blockItem">
                        <div class="t" data-event-trigger><i class="icon i-5"></i>出团班期<span class="arrow"></span></div>
                        <div class="con pad">

                            <select class="form-control" name="start_time">
                                <?php if(empty($tourSkuDate)): ?><option value="">暂无班期</option>
                                    <?php else: ?>
                                    <?php if(is_array($tourSkuDate)): $i = 0; $__LIST__ = $tourSkuDate;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["start_time"]); ?>">
                                            <?php echo ($vo["start_time"]); ?>&nbsp;&nbsp;&nbsp;&yen;<?php echo (GetYuan($vo["price_adult_list"])); ?>
                                        </option><?php endforeach; endif; else: echo "" ;endif; endif; ?>
                            </select>
                        </div>
                    </div><?php endif; ?>
            </div>


            <ul class="list">
                <li><label><i class="icon i-1"></i>出发城市：</label>&nbsp;<?php echo ($departure); ?></li>
                <li><label><i class="icon" style="background-position: 0 -615px;"></i> 目 的 地 ：</label>&nbsp;<?php echo ($list["destinations"]); ?>
                </li>
                <li><label><i class="icon i-2"></i>行程天数：</label>&nbsp;
                    <?php if($totalDay != 0): echo ($totalDay); ?>
                        <?php else: ?>
                        &nbsp;<?php endif; ?>
                </li>
                <li><label><i class="icon i-3"></i>出发交通：</label>&nbsp;<?php echo ($list["traffic_go"]); ?></li>
                <li><label><i class="icon i-4"></i>返程交通：</label>&nbsp;<?php echo ($list["traffic_back"]); ?></li>
            </ul>
        </div>



        <div class="module blockItem">
            <div class="t" data-event-trigger><i class="icon i-1"></i>行程安排<span class="arrow"></span></div>
            <div class="con">
                <?php if($tour_description != ''): if(is_array($tour_description)): $i = 0; $__LIST__ = $tour_description;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="box">

                            <label class="lab">第<?php echo ($i); ?>天</label>

                            <p>
                            <pre style="background-color: white;white-space:pre-wrap;border:0"><?php echo ($vo["description"]); ?></pre>
                            </p>
                            <div class="pie clearfix">
                                  <span class="it pull-left"><label><i class="food"></i>用餐：</label>
                                        <?php if($vo['dining'] == '2,3'): ?>中,晚
                                            <?php elseif($vo['dining'] == '1,3'): ?>
                                            早,晚
                                            <?php elseif($vo['dining'] == '1,2'): ?>
                                            早,中
                                            <?php elseif($vo['dining'] == '1,2,3'): ?>
                                            早,中,晚
                                            <?php elseif($vo['dining'] == '1'): ?>
                                            早
                                            <?php elseif($vo['dining'] == '2'): ?>
                                            中
                                            <?php elseif($vo['dining'] == '3'): ?>
                                            晚
                                            <?php elseif($vo['dining'] == ''): ?>
                                            自理<?php endif; ?>
                                  </span>

                                  <span class="it pull-right"><label><i
                                          class="hotel"></i>酒店：</label>
                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                           data-placement="top" title="<?php echo ($vo["hotel"]); ?>"><?php echo ($vo["hotel"]); ?></a>
                                  </span>
                            </div>

                        </div><?php endforeach; endif; else: echo "" ;endif; ?>
                    <?php else: endif; ?>
            </div>
        </div>
        <div class="module blockItem">
            <div class="t" data-event-trigger><i class="icon i-2"></i>产品亮点<span class="arrow"></span></div>
            <div class="con pad">
                <p><?php echo ($list["description"]); ?></p>
            </div>
        </div>
        <div class="module blockItem">
            <div class="t" data-event-trigger><i class="icon i-3"></i>费用说明<span class="arrow"></span></div>
            <div class="con pad">
                <p><label class="lab">费用包含</label><?php echo ($list["fee_include"]); ?></p>

                <p><label class="lab">费用不包含</label><?php echo ($list["fee_exclude"]); ?></p>
            </div>
        </div>
        <div class="module blockItem">
            <div class="t" data-event-trigger><i class="icon i-4"></i>注意事项<span class="arrow"></span></div>
            <div class="con pad">
                <p><?php echo ($list["notice"]); ?></p>
            </div>
        </div>
        <div class="module gallery">
            <ul class="act clearfix">
                <?php if($_SESSION['isLogin']== yes): ?><li class="icon i-3" id="msg">
                        <a id="recommend" href="javascript:void(0);">
                            <i></i><span class="w">推到首页</span>
                        </a>
                    </li><?php endif; ?>
            </ul>
            <?php if($list['pics'] != ''): ?><ul class="imglist clearfix">
                    <?php if(is_array($picArr)): $i = 0; $__LIST__ = array_slice($picArr,0,3,true);if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                            <a href="<?php echo U('Agency/morePics',array('id'=>$list['id']));?>"><img src="<?php echo ($vo); ?>"/></a>
                        </li><?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <a href="<?php echo U('Agency/morePics',array('id'=>$list['id']));?>" class="more">更多图片</a>
                <?php else: endif; ?>
        </div>
        <div class="detail-bottom clearfix">

            <?php if($_SESSION['isLogin']== yes): ?><a href="tel:<?php echo ($tourList["contact_phone"]); ?>"><i></i>电话咨询</a>
                <?php else: ?>
                <a href="tel:<?php echo ($userInfo["mobile"]); ?>" style="width: 100%" class="yd"><i></i>电话咨询</a><?php endif; ?>

            <?php if($_SESSION['isLogin']== yes): if($tourSku[0]['cruise'] == ''): ?><a href="<?php echo U('Agency/orderInfo',array('id'=>$list['id']));?>" class="yd">
                        立即预订
                    </a>
                    <?php else: ?>
                    <a href="<?php echo U('Agency/orderCruiseInfo',array('id'=>$list['id']));?>" class="yd">
                        立即预订
                    </a><?php endif; ?>
                <?php else: endif; ?>
        </div>
        <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">提示</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p class="tipsBox"><i class="suc"></i>推荐成功</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade popWin bs-example-modal-sm" id="myModalAuc" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">提示</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p class="tipsBox"><i class="suc"></i></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


        </footer>
    </form>
</div>

<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });

    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "<?php echo ($list["name"]); ?>",
            title: "<?php echo ($list["name"]); ?>",
            link: "<?php echo ($domain); ?>",
            imgUrl: '<?php echo ($bgImg); ?>',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
</body>
</html>