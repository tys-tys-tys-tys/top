<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>我的门票</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
    <?php
require_once "./App/Home/Common/jssdk.php"; $jssdk = new JSSDK("wx50158a5e179525ab", "d4065289c8314cbb82bb64d58eda674b"); $signPackage = $jssdk->GetSignPackage(); ?>
    <style>
        .actNav {
            background-color: #0564ac;
            box-sizing: content-box;
            color: white;
            font-size: 20px;
            height: 27px;
            overflow: visible;
            line-height: 27px;
            padding: 10px 40px;
            position: relative;
            text-align: center;
        }
    </style>
    <script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="http://cdn.lydlr.com/public/dist/js/bootstrap-select.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<body>

<div class="actNav">
    <select id="first-disabled" class="selectpicker regionCode" data-hide-disabled="true" data-live-search="true">
        <optgroup label="热门城市" data-subtext="">
            <option value="北京">(bj)北京</option>
            <option value="三亚">(sy)三亚</option>
            <option value="上海">(sh)上海</option>
            <option value="南京">(nj)南京</option>
            <option value="厦门">(sm)厦门</option>
            <option value="大连">(dl)大连</option>
            <option value="天津">(tj)天津</option>
            <option value="广州">(gz)广州</option>
            <option value="成都">(cd)成都</option>
            <option value="昆明">(km)昆明</option>
            <option value="杭州">(hz)杭州</option>
            <option value="武汉">(wh)武汉</option>
            <option value="济南">(jn)济南</option>
            <option value="深圳">(sz)深圳</option>
            <option value="福州">(fz)福州</option>
            <option value="西安">(xa)西安</option>
            <option value="重庆">(cq)重庆</option>
            <option value="长沙">(cs)长沙</option>
            <option value="青岛">(qd)青岛</option>
        </optgroup>
        <optgroup label="A" data-subtext="">
            <option value="安康">(ak)安康</option>
            <option value="阿克苏">(aks)阿克苏</option>
            <option value="阿勒泰">(alt)阿勒泰</option>
            <option value="阿拉善盟">(alsm)阿拉善盟</option>
            <option value="澳门">(am)澳门</option>
            <option value="安庆">(aq)安庆</option>
            <option value="安顺">(as)安顺</option>
            <option value="安阳">(ay)安阳</option>
        </optgroup>
        <optgroup label="B" data-subtext="">
            <option value="蚌埠">(bb)蚌埠</option>
            <option value="白城">(bc)白城</option>
            <option value="保定">(bd)保定</option>
            <option value="博尔塔拉">(betl)博尔塔拉</option>
            <option value="北海">(bh)北海</option>
            <option value="毕节">(bj)毕节</option>
            <option value="宝鸡">(bj)宝鸡</option>
            <option value="保山">(bs)保山</option>
            <option value="百色">(bs)百色</option>
            <option value="白山">(bs)白山</option>
            <option value="包头">(bt)包头</option>
            <option value="本溪">(bx)本溪</option>
            <option value="白银">(by)白银</option>
            <option value="巴音郭楞">(bygl)巴音郭楞</option>
            <option value="巴彦淖尔">(byne)巴彦淖尔</option>
            <option value="滨州">(bz)滨州</option>
            <option value="巴中">(bz)巴中</option>
        </optgroup>
        <optgroup label="C" data-subtext="">
            <option value="长春">(cc)长春</option>
            <option value="昌都">(cd)昌都</option>
            <option value="常德">(cd)常德</option>
            <option value="赤峰">(cf)赤峰</option>
            <option value="巢湖">(ch)巢湖</option>
            <option value="昌吉">(cj)昌吉</option>
            <option value="楚雄">(cx)楚雄</option>
            <option value="朝阳">(cy)朝阳</option>
            <option value="常州">(cz)常州</option>
            <option value="长治">(cz)长治</option>
            <option value="池州">(cz)池州</option>
            <option value="沧州">(cz)沧州</option>
            <option value="滁州">(cz)滁州</option>
            <option value="潮州">(cz)潮州</option>
            <option value="崇左">(cz)崇左</option>
        </optgroup>
        <optgroup label="D" data-subtext="">
            <option value="丹东">(dd)丹东</option>
            <option value="东莞">(dg)东莞</option>
            <option value="德宏">(dh)德宏</option>
            <option value="大理">(dl)大理</option>
            <option value="迪庆">(dq)迪庆</option>
            <option value="大庆">(dq)大庆</option>
            <option value="大同">(dt)大同</option>
            <option value="大兴安岭">(dxal)大兴安岭</option>
            <option value="东营">(dy)东营</option>
            <option value="德阳">(dy)德阳</option>
            <option value="达州">(dz)达州</option>
            <option value="德州">(dz)德州</option>
        </optgroup>
        <optgroup label="E" data-subtext="">
            <option value="鄂尔多斯">(eeds)鄂尔多斯</option>
            <option value="恩施">(es)恩施</option>
        </optgroup>
        <optgroup label="F" data-subtext="">
            <option value="防城港">(fcg)防城港</option>
            <option value="佛山">(fs)佛山</option>
            <option value="抚顺">(fs)抚顺</option>
            <option value="阜新">(fx)阜新</option>
            <option value="阜阳">(fy)阜阳</option>
            <option value="抚州">(fz)抚州</option>
        </optgroup>
        <optgroup label="G" data-subtext="">
            <option value="广安">(ga)广安</option>
            <option value="桂林">(gl)桂林</option>
            <option value="果洛">(gl)果洛</option>
            <option value="甘南">(gn)甘南</option>
            <option value="贵阳">(gy)贵阳</option>
            <option value="广元">(gy)广元</option>
            <option value="固原">(gy)固原</option>
            <option value="赣州">(gz)赣州</option>
            <option value="贵港">(gz)贵港</option>
            <option value="甘孜">(gz)甘孜</option>
        </optgroup>
        <optgroup label="H" data-subtext="">
            <option value="淮安">(ha)淮安</option>
            <option value="海北">(hb)海北</option>
            <option value="淮北">(hb)淮北</option>
            <option value="鹤壁">(hb)鹤壁</option>
            <option value="河池">(hc)河池</option>
            <option value="邯郸">(hd)邯郸</option>
            <option value="海东">(hd)海东</option>
            <option value="哈尔滨">(heb)哈尔滨</option>
            <option value="合肥">(hf)合肥</option>
            <option value="鹤岗">(hg)鹤岗</option>
            <option value="怀化">(hh)怀化</option>
            <option value="黑河">(hh)黑河</option>
            <option value="红河">(hh)红河</option>
            <option value="呼和浩特">(hhht)呼和浩特</option>
            <option value="呼伦贝尔">(hlbe)呼伦贝尔</option>
            <option value="葫芦岛">(hld)葫芦岛</option>
            <option value="海口">(hk)海口</option>
            <option value="哈密">(hm)哈密</option>
            <option value="海南">(hn)海南</option>
            <option value="淮南">(hn)淮南</option>
            <option value="黄南">(hn)黄南</option>
            <option value="黄山">(hs)黄山</option>
            <option value="黄石">(hs)黄石</option>
            <option value="衡水">(hs)衡水</option>
            <option value="和田">(ht)和田</option>
            <option value="海西">(hx)海西</option>
            <option value="衡阳">(hy)衡阳</option>
            <option value="河源">(hy)河源</option>
            <option value="汉中">(hz)汉中</option>
            <option value="惠州">(hz)惠州</option>
            <option value="湖州">(hz)湖州</option>
            <option value="菏泽">(hz)菏泽</option>
            <option value="贺州">(hz)贺州</option>
            <option value="毫州">(hz)毫州</option>
        </optgroup>
        <optgroup label="J" data-subtext="">
            <option value="吉安">(ja)吉安</option>
            <option value="金昌">(jc)金昌</option>
            <option value="晋城">(jc)晋城</option>
            <option value="景德镇">(jdz)景德镇</option>
            <option value="金华">(jh)金华</option>
            <option value="九江">(jj)九江</option>
            <option value="吉林">(jl)吉林</option>
            <option value="九龙">(jl)九龙</option>
            <option value="佳木斯">(jms)佳木斯</option>
            <option value="江门">(jm)江门</option>
            <option value="济宁">(jn)济宁</option>
            <option value="酒泉">(jq)酒泉</option>
            <option value="鸡西">(jx)鸡西</option>
            <option value="嘉兴">(jx)嘉兴</option>
            <option value="嘉峪关">(jyg)嘉峪关</option>
            <option value="揭阳">(jy)揭阳</option>
            <option value="锦州">(jz)锦州</option>
            <option value="荆州">(jz)荆州</option>
            <option value="晋中">(jz)晋中</option>
            <option value="焦作">(jz)焦作</option>
        </optgroup>
        <optgroup label="K" data-subtext="">
            <option value="开封">(kf)开封</option>
            <option value="克拉玛依">(klmy)克拉玛依</option>
            <option value="喀什">(ks)喀什</option>
            <option value="克孜勒苏">(kzls)克孜勒苏</option>
        </optgroup>
        <optgroup label="L" data-subtext="">
            <option value="六安">(la)六安</option>
            <option value="来宾">(lb)来宾</option>
            <option value="临沧">(lc)临沧</option>
            <option value="聊城">(lc)聊城</option>
            <option value="娄底">(ld)娄底</option>
            <option value="离岛">(ld)离岛</option>
            <option value="廊坊">(lf)廊坊</option>
            <option value="临汾">(lf)临汾</option>
            <option value="丽江">(lj)丽江</option>
            <option value="辽宁">(ln)辽宁</option>
            <option value="黎平">(lp)黎平</option>
            <option value="六盘水">(lps)六盘水</option>
            <option value="拉萨">(ls)拉萨</option>
            <option value="乐山">(ls)乐山</option>
            <option value="凉山">(ls)凉山</option>
            <option value="丽水">(ls)丽水</option>
            <option value="临夏">(lx`)临夏</option>
            <option value="洛阳">(ly)洛阳</option>
            <option value="临沂">(ly)临沂</option>
            <option value="辽源">(ly)辽源</option>
            <option value="吕梁">(ly)吕梁</option>
            <option value="龙岩">(ly)龙岩</option>
            <option value="连云港">(lyg)连云港</option>
            <option value="泸州">(lz)泸州</option>
            <option value="兰州">(lz)兰州</option>
            <option value="柳州">(lz)柳州</option>
            <option value="林芝">(lz)林芝</option>
        </optgroup>
        <optgroup label="M" data-subtext="">
            <option value="马鞍山">(mas)马鞍山</option>
            <option value="牡丹江">(mdj)牡丹江</option>
            <option value="茂名">(mm)茂名</option>
            <option value="眉山">(ms)眉山</option>
            <option value="绵阳">(my)绵阳</option>
            <option value="梅州">(mz)梅州</option>
        </optgroup>
        <optgroup label="N" data-subtext="">
            <option value="宁波">(nb)宁波</option>
            <option value="南昌">(nc)南昌</option>
            <option value="南充">(nc)南充</option>
            <option value="宁德">(nd)宁德</option>
            <option value="内江">(nj)内江</option>
            <option value="怒江">(nj)怒江</option>
            <option value="南宁">(nn)南宁</option>
            <option value="南平">(np)南平</option>
            <option value="那曲">(nq)那曲</option>
            <option value="南通">(nt)南通</option>
            <option value="南阳">(ny)南阳</option>
        </optgroup>
        <optgroup label="P" data-subtext="">
            <option value="平顶山">(pds)平顶山</option>
            <option value="盘锦">(pj)盘锦</option>
            <option value="平凉">(pl)平凉</option>
            <option value="莆田">(pt)莆田</option>
            <option value="萍乡">(px)萍乡</option>
            <option value="濮阳">(py)濮阳</option>
            <option value="攀枝花">(pzh)攀枝花</option>
        </optgroup>
        <optgroup label="Q" data-subtext="">
            <option value="黔东南">(qdn)黔东南</option>
            <option value="秦皇岛">(qhd)秦皇岛</option>
            <option value="曲靖">(qj)曲靖</option>
            <option value="黔南">(qn)黔南</option>
            <option value="齐齐哈尔">(qqhe)齐齐哈尔</option>
            <option value="七台河">(qth)七台河</option>
            <option value="黔西南">(qxn)黔西南</option>
            <option value="庆阳">(qy)庆阳</option>
            <option value="清远">(qy)清远</option>
            <option value="泉州">(qz)泉州</option>
            <option value="钦州">(qz)钦州</option>
            <option value="衢州">(qz)衢州</option>
        </optgroup>
        <optgroup label="R" data-subtext="">
            <option value="日喀则">(rkz)日喀则</option>
            <option value="日照">(rz)日照</option>
        </optgroup>
        <optgroup label="S" data-subtext="">
            <option value="韶关">(sg)韶关</option>
            <option value="绥化">(sh)绥化</option>
            <option value="石家庄">(sjz)石家庄</option>
            <option value="商洛">(sl)商洛</option>
            <option value="三明">(sm)三明</option>
            <option value="思茅">(sm)思茅</option>
            <option value="三门峡">(smx)三门峡</option>
            <option value="遂宁">(sn)遂宁</option>
            <option value="山南">(sn)山南</option>
            <option value="四平">(sp)四平</option>
            <option value="宿迁">(sq)宿迁</option>
            <option value="商丘">(sq)商丘</option>
            <option value="上饶">(sr)上饶</option>
            <option value="汕头">(st)汕头</option>
            <option value="汕尾">(sw)汕尾</option>
            <option value="绍兴">(sx)绍兴</option>
            <option value="沈阳">(sy)沈阳</option>
            <option value="十堰">(sy)十堰</option>
            <option value="松原">(sy)松原</option>
            <option value="双鸭山">(sys)双鸭山</option>
            <option value="朔州">(sz)朔州</option>
            <option value="苏州">(sz)苏州</option>
            <option value="宿州">(sz)宿州</option>
            <option value="随州">(sz)随州</option>
            <option value="石嘴山">(szs)石嘴山</option>
        </optgroup>
        <optgroup label="T" data-subtext="">
            <option value="泰安">(ta)泰安</option>
            <option value="塔城">(tc)塔城</option>
            <option value="铜川">(tc)铜川</option>
            <option value="通化">(th)通化</option>
            <option value="漯河">(th)漯河</option>
            <option value="通辽">(tl)通辽</option>
            <option value="铁岭">(tl)铁岭</option>
            <option value="铜陵">(tl)铜陵</option>
            <option value="吐鲁番">(tlf)吐鲁番</option>
            <option value="台南">(tn)台南</option>
            <option value="铜仁">(tr)铜仁</option>
            <option value="天水">(ts)天水</option>
            <option value="唐山">(ts)唐山</option>
            <option value="台湾">(tw)台湾</option>
            <option value="太原">(ty)太原</option>
            <option value="台州">(tz)台州</option>
            <option value="泰州">(tz)泰州</option>
        </optgroup>
        <optgroup label="W" data-subtext="">
            <option value="潍坊">(wf)潍坊</option>
            <option value="乌海">(wh)乌海</option>
            <option value="威海">(wh)威海</option>
            <option value="芜湖">(wh)芜湖</option>
            <option value="乌鲁木齐">(wlmq)乌鲁木齐</option>
            <option value="乌兰察布">(wlcb)乌兰察布</option>
            <option value="渭南">(wn)渭南</option>
            <option value="文山">(ws)文山</option>
            <option value="无锡">(wx)无锡</option>
            <option value="武夷">(wy)武夷</option>
            <option value="梧州">(wz)梧州</option>
            <option value="温州">(wz)温州</option>
            <option value="吴忠">(wz)吴忠</option>
        </optgroup>
        <optgroup label="X" data-subtext="">
            <option value="兴安盟">(xam)兴安盟</option>
            <option value="宣城">(xc)宣城</option>
            <option value="许昌">(xc)许昌</option>
            <option value="襄樊">(xf)襄樊</option>
            <option value="孝感">(xg)孝感</option>
            <option value="香港">(xg)香港</option>
            <option value="锡林郭勒盟">(xlglm)锡林郭勒盟</option>
            <option value="新界">(xj)新界</option>
            <option value="西宁">(xn)西宁</option>
            <option value="咸宁">(xn)咸宁</option>
            <option value="西双版纳">(xsbn)西双版纳</option>
            <option value="邢台">(xt)邢台</option>
            <option value="湘潭">(xt)湘潭</option>
            <option value="新乡">(xx)新乡</option>
            <option value="湘西">(xx)湘西</option>
            <option value="新余">(xy)新余</option>
            <option value="信阳">(xy)信阳</option>
            <option value="徐州">(xz)徐州</option>
            <option value="忻州">(xz)忻州</option>
        </optgroup>
        <optgroup label="Y" data-subtext="">
            <option value="延安">(ya)延安</option>
            <option value="雅安">(ya)雅安</option>
            <option value="宜宾">(yb)宜宾</option>
            <option value="延边">(yb)延边</option>
            <option value="伊春">(yc)伊春</option>
            <option value="盐城">(yc)盐城</option>
            <option value="宜昌">(yc)宜昌</option>
            <option value="运城">(yc)运城</option>
            <option value="银川">(yc)银川</option>
            <option value="宜春">(yc)宜春</option>
            <option value="云浮">(yf)云浮</option>
            <option value="阳江">(yj)阳江</option>
            <option value="营口">(yk)营口</option>
            <option value="榆林">(yl)榆林</option>
            <option value="伊犁哈萨克">(ylhsk)伊犁哈萨克</option>
            <option value="玉林">(yl)玉林</option>
            <option value="阳泉">(yq)阳泉</option>
            <option value="玉树">(ys)玉树</option>
            <option value="烟台">(yt)烟台</option>
            <option value="鹰潭">(yt)鹰潭</option>
            <option value="玉溪">(yx)玉溪</option>
            <option value="岳阳">(yy)岳阳</option>
            <option value="益阳">(yy)益阳</option>
            <option value="扬州">(yz)扬州</option>
            <option value="永州">(yz)永州</option>
        </optgroup>
        <optgroup label="Z" data-subtext="">
            <option value="淄博">(zb)淄博</option>
            <option value="自贡">(zg)自贡</option>
            <option value="珠海">(zh)珠海</option>
            <option value="湛江">(zj)湛江</option>
            <option value="镇江">(zj)镇江</option>
            <option value="张家界">(zjj)张家界</option>
            <option value="张家口">(zjk)张家口</option>
            <option value="周口">(zk)周口</option>
            <option value="驻马店">(zmd)驻马店</option>
            <option value="肇庆">(zq)肇庆</option>
            <option value="舟山">(zs)舟山</option>
            <option value="昭通">(zt)昭通</option>
            <option value="中卫">(zw)中卫</option>
            <option value="张掖">(zy)张掖</option>
            <option value="遵义">(zy)遵义</option>
            <option value="资阳">(zy)资阳</option>
            <option value="郑州">(zz)郑州</option>
            <option value="枣庄">(zz)枣庄</option>
            <option value="漳州">(zz)漳州</option>
            <option value="株洲">(zz)株洲</option>
        </optgroup>
    </select>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper" style="background-color: white">
    <br>
    <div class="searchbox">
        <input type="text" class="txt scenicName" placeholder="京东大峡谷风景"/>
        <input type="submit" value="搜索" class="sbtn scenicNameSearch"/>
    </div>

    <div class="module recLine" style="border-top:1px solid #e6e6e6">
        <h2 class="h2t"><i></i>推荐景点</h2>

        <div class="con">
            <div id="content">
                <?php echo ($html); ?>
            </div>

            <div class="showCircle" style="display:none;padding-bottom: 30px;">
                <div style="float:right">
                    <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="circle" class="g-circles g-circles--v1">
                            <circle id="12" transform="translate(35, 16.698730) rotate(-30) translate(-35, -16.698730) "
                                    cx="35" cy="16.6987298" r="10"></circle>
                            <circle id="11" transform="translate(16.698730, 35) rotate(-60) translate(-16.698730, -35) "
                                    cx="16.6987298" cy="35" r="10"></circle>
                            <circle id="10" transform="translate(10, 60) rotate(-90) translate(-10, -60) " cx="10"
                                    cy="60" r="10"></circle>
                            <circle id="9" transform="translate(16.698730, 85) rotate(-120) translate(-16.698730, -85) "
                                    cx="16.6987298" cy="85" r="10"></circle>
                            <circle id="8"
                                    transform="translate(35, 103.301270) rotate(-150) translate(-35, -103.301270) "
                                    cx="35" cy="103.30127" r="10"></circle>
                            <circle id="7" cx="60" cy="110" r="10"></circle>
                            <circle id="6"
                                    transform="translate(85, 103.301270) rotate(-30) translate(-85, -103.301270) "
                                    cx="85" cy="103.30127" r="10"></circle>
                            <circle id="5"
                                    transform="translate(103.301270, 85) rotate(-60) translate(-103.301270, -85) "
                                    cx="103.30127" cy="85" r="10"></circle>
                            <circle id="4" transform="translate(110, 60) rotate(-90) translate(-110, -60) " cx="110"
                                    cy="60" r="10"></circle>
                            <circle id="3"
                                    transform="translate(103.301270, 35) rotate(-120) translate(-103.301270, -35) "
                                    cx="103.30127" cy="35" r="10"></circle>
                            <circle id="2" transform="translate(85, 16.698730) rotate(-150) translate(-85, -16.698730) "
                                    cx="85" cy="16.6987298" r="10"></circle>
                            <circle id="1" cx="60" cy="10" r="10"></circle>
                        </g>
                        <use xlink:href="#circle" class="use"/>
                    </svg>
                </div>
                <span class="circle-size text-right" style="float:right">加载中,请勿关闭</span>
            </div>

            <input type="hidden" name='page_nav' id='page_nav' value='2'>
            <input type="hidden" id="ticketUrl" value="<?php echo U('Ticket/index');?>">
            <input type="hidden" id="searchUrl" value="<?php echo U('Ticket/indexSearch');?>">
            <a href="javascript:void(0)" class="more moreTicketContent" style="margin-top: 10px;">点击查看更多</a>

        </div>
    </div>
</div>

<footer class="footer">
    <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?></pre>
    <img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
    <p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>
</footer>

<script type="text/javascript" src="http://cdn.lydlr.com/public/js/Home.js?v=49"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>
<script src="/Public/js/ticket.js?v=14"></script>
<script>
    $(document).ready(function () {

        var mySelect = $('#first-disabled2');

        $('#special').on('click', function () {
            mySelect.find('option:selected').prop('disabled', true);
            mySelect.selectpicker('refresh');
        });

        $('#special2').on('click', function () {
            mySelect.find('option:disabled').prop('disabled', false);
            mySelect.selectpicker('refresh');
        });

        $('#basic2').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
    });

    $(".compulsoryRefresh").click(function () {
        location.reload(true)
    });
</script>
<script>
    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: '<?php echo $signPackage["timestamp"]; ?>',
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
        ]
    });

    wx.ready(function () {
        var shareData = {
            desc: "我的手机旅行社",
            title: "我的手机旅行社",
            link: "<?php echo ($domain); ?>",
            imgUrl: '<?php echo ($headPic); ?>',
        };
        wx.onMenuShareTimeline(shareData);
        wx.onMenuShareAppMessage(shareData);
        wx.onMenuShareQQ(shareData);
    });

</script>
</body>
</html>