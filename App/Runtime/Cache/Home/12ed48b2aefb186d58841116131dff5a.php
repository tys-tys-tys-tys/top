<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>提现申请</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
<link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" href="http://cdn.lydlr.com/public/dist/css/bootstrap-select.css">
<link href="http://cdn.lydlr.com/public/css/bootstrap-datetimepicker.css?v=1" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/home.css?v=3" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/circle.css?v=3" rel="stylesheet">
<link href="http://demo.dalvu.com/public/kangtai/index.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/list.css?v=2" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/detail.css?v=4" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/kangtai/ucenter.css?v=8" rel="stylesheet">
<link href="http://cdn.lydlr.com/public/css/flight.css?v=10" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css">
<style>
    .userStyle {
    / / 您可以按照您的页面需要，自主定义外套样式，给您更大的灵活性 width : 100 %;
        height: 30px;
        background: #ccc;
        display: block;
    }
</style>
</head>
<body>
<div class="actNav">
    <div class="doRefresh">提现申请</div>
    <a href="javascript:history.go(-1);" class="back"></a>
    <a href="<?php echo U('Index/index');?>" class="home"></a>
</div>
<div class="wapper">
    <form method="post" action="<?php echo U('Finance/withdrawList');?>">
        <input id="applyWithdraw" type="hidden" value="<?php echo U('Finance/applyWithdraw');?>"/>
        <input id="doSubmitWithdraw" type="hidden" value="<?php echo U('Finance/index');?>"/>
        <input type="hidden" id="account_state" value="<?php echo ($list["account_state"]); ?>">
        <input type="hidden" id="code" value="<?php echo (session('applyWithDraw')); ?>">

        <div class="fpApply">
            <label class="t">提现帐号</label>

            <div class="wb">
                <span class="bankMessage"><?php echo ($list["bank_name"]); ?></span><span
                    class="pur">（尾号<?php echo (substr($list["bank_account"],-4)); ?>）</span>
            </div>
            <label class="t">帐户总额</label>

            <div class="wb">
                <?php echo ($list["account_balance"]); ?>
            </div>
            <label class="t">当前余额</label>

            <div class="wb" id="account_balance">
                <?php echo (getYuan($availableBalance)); ?>
            </div>
            <label class="t">提现金额</label>

            <div class="input-group">
                <input type="number" class="form-control" id="amount" name="amount" placeholder="请输入转出金额"/>
            </div>
            <div class="pad"><input type="button" class="btn btn-primary btn-lg btn-block proofAgain"
                                    id="doSubmitApplyWithdraw" value="确认提现"/></div>
        </div>
    </form>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalDel" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>请填写1元以上的提现金额</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalDanger" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>您的现金余额不足</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalThan" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="warn"></i>提现金额不能超过可用余额</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">提示</h4>
                </div>
                <div class="modal-body text-center">
                    <p class="tipsBox"><i class="suc"></i>申请成功</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认
                    </button>
                    <button type="button" class="btn btn-default doCancel" data-dismiss="modal" aria-label="Close">取消</button>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <pre style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;font-family: '微软雅黑'"><?php echo ($copyRight); ?>
</pre>
<img src="http://cdn.lydlr.com/public/images/support.png" class="img-responsive img-rounded center-block" style="margin-top: -5px;">
<p style="background-color: #35383B;border:0;color:#a8a8a8;padding:0;line-height: 20px;margin-top: 7px;font-family: '微软雅黑';font-size:13px;" class="text-center">大旅提供技术支持</p>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "//hm.baidu.com/hm.js?7cf42eadc6c0835f4a6048378bddbe36";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

<script type="text/javascript" charset="utf-8" src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.lydlr.com/public/js/friendRemind.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.js"></script>
<script src="http://cdn.lydlr.com/public/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script src="http://libs.baidu.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Public/js/Home.js?v=72"></script>
<script src="http://cdn.lydlr.com/public/artDialog-6.0.4/dialog-min.js"></script>
<script src="http://cdn.lydlr.com/public/js/common.js"></script>
<script src="/Public/js/flight.js?v=3"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="http://cdn.lydlr.com/public/js/jquery.lazyload.js?v=1"></script>


    </footer>
</div>
</body>
</html>