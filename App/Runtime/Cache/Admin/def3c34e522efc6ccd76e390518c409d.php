<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt;意见反馈-供应商</div>
        <div class="cTitle2">供应商列表</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Provider/supplier_feedback_g', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Provider/supplier_feedback_g', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Provider/supplier_feedback_g',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入供应商简称/全称" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>

<!--            <form class="pull-right" style="padding-bottom: 5px;">
                <input id="chkr" type="hidden" value="<?php echo U('Provider/delete');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
            </form>
            <p></p>-->

<!--            <form method="post" action="<?php echo U('GetApplyExcel/getSupplierFeedback_gExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>-->
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="3%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold"></label></th>
                <th width="5%">编号</th>
                <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><th width="5%">运营商</th><?php endif; ?>
                <th width="5%">顾问</th>
                <th width="5%">供应商</th>
                <th width="5%">评星</th>
                <th width="15%">意见反馈内容</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/>
                    </td>
                    <td><?php echo ($v["id"]); ?></td>
                    <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><td><?php echo (getOperatorName($v["operator_id"])); ?></td><?php endif; ?>
                    <td><?php echo ($v["agency_name"]); ?></td>
                    <td><?php echo (getSupplierName($v["cover_commnet_id"])); ?></td>
                    <td><?php echo ($v["star"]); ?></td>
                    <td><?php echo ($v["comm_content"]); ?></td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">供应商添加</h4>
        </div>
        <div class="modal-body" style="border-bottom: 1px solid #e5e5e5;">
            <div class="form-horizontal">
                <div class="form-group" id="op_name">
                    <div class="col-sm-4">
                        <input id="operid" type="hidden" name="operid" value="<?php echo ($oper["id"]); ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">公司简称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="name"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*请输入公司简称</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">公司全称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="fullName"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*请输入公司全称</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"
                           style="width:17.667%;padding-right:0px;">管理员登录名：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="login_name"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*必须填写登录名称（英文、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"
                           style="width:17.667%;padding-right:0px;">管理员登录密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" value="" id="login_pwd"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*必须输入登录名密码（字母、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">确认密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" value="" id="login_pwd2"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*请确认登录名密码（字母、数字）</span>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"
                           style="width:17.667%;padding-right:0px;">许可经营目的地：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="destinations"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*许可经营的目的地（多个用逗号分隔）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">佣金比例:(千分比)</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="10" id="commission_rate"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%"><em style='color:#333'>（‰）</em>&nbsp;佣金比例必须填写</span>
                </div>
            </div>
        </div>
        <div class="modal-footer pull-right" style="border:none">
            <button type="button" class="btn btn-warning" id="buttonSaveProviderInfo">保存</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>


<div class="modal fade popWin bs-example-modal-lg" id="myModalNotice" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
            </div>
            <div class="modal-body text-center">
                <textarea class="form-control reason" placeholder="请输入理由"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    修改供应商密码
                </h4>
            </div>
            <div class="modal-body">
                供应商：
                <input type="text" id="agencyName" style="border:0px">

                <p>
                    新密码：
                    <input type="password" class="form-control" id="newpassword">
                    确认密码：
                    <input type="password" class="form-control" id="password2">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveAgencyPwd">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalProviderInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    高级编辑
                </h4>
            </div>
            <div class="modal-body" style="font-size: 15px">
                公司简称
                <input type="text" class="form-control name" disabled>
                公司全称
                <input type="text" class="form-control full_name">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-info submitEdit"
                            data-dismiss="modal">确认
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    更新供应商代码
                </h4>
            </div>
            <div class="modal-body">
                代码：
                <input type="text" class="form-control" id="providerCode">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveProviderCode">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel1">
                    查看供应商账户信息
                </h4>
            </div>
            <div class="modal-body" style="font-size: 15px">
                <p>&nbsp;&nbsp;供应商：<span class="text-c5 provider_name"></span></p>

                <p>账户总额: <span class="text-c5">&yen; <dfn class="total"></dfn></span></p>

                <p>冻结金额: <span class="text-c5">&yen; <dfn class="frozenMoney"></dfn></span></p>

                <p>可用余额: <span class="text-c5">&yen; <dfn class="availableBalance"></dfn></span></p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="/Public/js/providerInfo.js?v=7"></script>
</body>
</html>