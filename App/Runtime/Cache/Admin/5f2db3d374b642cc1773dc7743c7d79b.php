<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <input id="status" type='hidden' value='<?php echo ($state); ?>'/>
        <input id="chkState" type="hidden" value="<?php echo U('Withdraw/providerAuditWithDraw');?>"/>
        <input id="chkStatePass" type="hidden" value="<?php echo U('Withdraw/financeAuditWithDraw');?>"/>
        <input id="showAccountBalance" type="hidden" value="<?php echo U('Withdraw/showAccountBalance');?>"/>
        <input type="hidden" id="showFullName" value="<?php echo U('Withdraw/showFullName');?>">
        <input type="hidden" id="code" value="<?php echo (session('providerWithdrawCode')); ?>">
        <input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
        <input type="hidden" id="invoiceList" value="<?php echo U('Withdraw/updateWithDrawProof');?>">

        <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt;提现申请</div>
        <div class="cTitle2">提现申请</div>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Withdraw/index',array('id'=>I('get.id')));?>" class="form-inline">
                    <input type="hidden" name="state" value="<?php echo ($state); ?>">

                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="供应商简称/全称/提现金额" value="<?php echo ($name); ?>"
                               style="width:200px;"/>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                    <span style="font-size: 16px;" class='text-c1'>(查询金额时，请以“分”为单位查询)</span>
                </form>
            </div>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getProviderWithDrawExcel',array('id' => I('get.id')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="hidden" name="state" value="<?php echo ($state); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
            <div class="status">
                <?php if($_SESSION['role_id']!= 185): ?><label>状态：</label>
                    <a class="nav-audit-state nav-audit-state0"
                       href="<?php echo U('Withdraw/index',array('state'=>0,'id'=>I('get.id')));?>">全部
                        <?php if($state == 0): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>
                    <a class="nav-audit-state nav-audit-state1"
                       href="<?php echo U('Withdraw/index',array('state'=>1,'id'=>I('get.id')));?>">待审核
                        <?php if($state == 1): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>
                    <a class="nav-audit-state nav-audit-state2"
                       href="<?php echo U('Withdraw/index',array('state'=>2,'id'=>I('get.id')));?>">待财务处理
                        <?php if($state == 2): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>
                    <a class="nav-audit-state nav-audit-state4"
                       href="<?php echo U('Withdraw/index',array('state'=>4,'id'=>I('get.id')));?>">处理完成
                        <?php if($state == 4): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>
                    <a class="nav-audit-state nav-audit-state3"
                       href="<?php echo U('Withdraw/index',array('state'=>3,'id'=>I('get.id')));?>">拒绝
                        <?php if($state == 3): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>

                    <?php elseif($_SESSION['role_id']== 185): ?>
                    <label>状态：</label>
                    <a class="nav-audit-state nav-audit-state2"
                       href="<?php echo U('Withdraw/index',array('state'=>2,'id'=>I('get.id')));?>">待财务处理
                        <?php if($state == 2): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a>
                    <a class="nav-audit-state nav-audit-state4"
                       href="<?php echo U('Withdraw/index',array('state'=>4,'id'=>I('get.id')));?>">处理完成
                        <?php if($state == 4): ?>(<?php echo ($total); ?>)<?php endif; ?>
                    </a><?php endif; ?>
            </div>
        </div>

        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%">编号</th>
                <th width="10%">供应商</th>
                <th width="10%">所属运营商</th>
                <th width="10%">提现金额</th>
                <th width="8%">提现周期</th>
                <th width="10%">账户总额</th>
                <th width="8%">提现帐户</th>
                <th width="9%">申请时间</th>
                <th width="9%">审核时间</th>
                <th width="8%">凭证</th>
                <th width="10%">状态</th>
                <th width="8%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i; if(($_SESSION['role_id']!= 185) && ($_SESSION['role_id']!= 1)): ?><tr>
                        <td><?php echo ($v["id"]); ?></td>
                        <td class="tl">
                            <a href="javascript:void(0)" onclick="showFullName(<?php echo ($v["entity_id"]); ?>, <?php echo ($v["id"]); ?>)"
                               class="text-c5 showFullName<?php echo ($v["id"]); ?>">
                                <?php echo ($v["entity_name"]); ?>
                            </a>
                            <span style="display: none" class="fullName<?php echo ($v["id"]); ?>"></span>
                        </td>
                        <td><?php echo (getOperatorName($v["operator_id"])); ?></td>
                        <td class="text-c1"> &yen;<?php echo (GetYuan($v["amount"])); ?></td>
                        <td class="text-c1">
                            <?php if($v['settlement_interval'] == 0): else: ?>
                                T+<?php echo ($v["settlement_interval"]); endif; ?>
                        </td>
                        <td>
                            <a href="javascript:void(0)"
                               onclick="showAccountBalance(<?php echo ($v["entity_id"]); ?>, <?php echo ($v["id"]); ?>, <?php echo ($v["operator_id"]); ?>)"
                               class="text-c5 showAccountBalance<?php echo ($v["id"]); ?>">查看</a>
                            <span style="display: none" class="showBalance<?php echo ($v["id"]); ?>"></span>
                        </td>
                        <td><?php echo ($v["bank_name"]); ?><br/>账号 <?php echo ($v["bank_account"]); ?></td>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <td><?php echo ($v["update_time"]); ?></td>
                        <td class="text-c1"><?php echo ($v["proof_type"]); ?> <?php echo ($v["proof_code"]); ?></td>
                        <td class="text-c3">
                            <?php if($v["state"] == 1): ?><span style="color:saddlebrown">待审核</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span class="text-c6">已收发票，待财务处理</span>
                                <?php elseif($v["state"] == 4): ?>
                                <span class="text-c7">处理完成</span>
                                <?php else: ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                   title="<?php echo ($v["memo"]); ?>" style="text-decoration: none"><span class="text-c8">拒绝</span></a><?php endif; ?>
                        </td>

                        <td>
                            <?php if($v['state'] == 1): ?><div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a onclick="changeWithDrawState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo Common\Top::ApplyStatePass;?>, <?php echo (getProviderState($v["entity_id"])); ?>, <?php echo (getProviderAccountState($v["entity_id"])); ?>)"
                                               style="cursor: pointer">已收发票，待财务处理</a></li>
                                        <li>
                                            <a onclick="providerWithDrawReject(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo Common\Top::ApplyStateReject;?>)"
                                               style="cursor: pointer">拒绝</a></li>
                                    </ul>
                                </div>
                                <?php elseif($v['state'] == 2): ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a onclick="providerWithDrawReject(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo Common\Top::ApplyStateReject;?>)"
                                               style="cursor: pointer">拒绝</a></li>
                                    </ul>
                                </div>
                                <?php else: ?>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                </div><?php endif; ?>
                        </td>
                    </tr><?php endif; ?>
                <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): if($v['state'] == 2 or $v['state'] == 4): ?><tr>
                            <td><?php echo ($v["id"]); ?></td>
                            <td class="tl">
                                <a href="javascript:void(0)" onclick="showFullName(<?php echo ($v["entity_id"]); ?>, <?php echo ($v["id"]); ?>)"
                                   class="text-c5 showFullName<?php echo ($v["id"]); ?>">
                                    <?php echo ($v["entity_name"]); ?>
                                </a>
                                <span style="display: none" class="fullName<?php echo ($v["id"]); ?>"></span>
                            </td>
                            <td><?php echo (getOperatorName($v["operator_id"])); ?></td>
                            <td class="text-c1"> &yen;<?php echo (GetYuan($v["amount"])); ?></td>
                            <td class="text-c1">
                                <?php if($v['settlement_interval'] == 0): else: ?>
                                    T+<?php echo ($v["settlement_interval"]); endif; ?>
                            </td>
                            <td>
                                <a href="javascript:void(0)"
                                   onclick="showAccountBalance(<?php echo ($v["entity_id"]); ?>, <?php echo ($v["id"]); ?>, <?php echo ($v["operator_id"]); ?>)"
                                   class="text-c5 showAccountBalance<?php echo ($v["id"]); ?>">查看</a>
                                <span style="display: none" class="showBalance<?php echo ($v["id"]); ?>"></span>
                            </td>
                            <td><?php echo ($v["bank_name"]); ?><br/>账号 <?php echo ($v["bank_account"]); ?></td>
                            <td><?php echo ($v["create_time"]); ?></td>
                            <td><?php echo ($v["update_time"]); ?></td>
                            <td class="text-c1"><?php echo ($v["proof_type"]); ?> <?php echo ($v["proof_code"]); ?></td>
                            <td class="text-c3">
                                <?php if($v["state"] == 1): ?><span style="color:saddlebrown">待审核</span>
                                    <?php elseif($v["state"] == 2): ?>
                                    <span class="text-c6">已收发票，待财务处理</span>
                                    <?php elseif($v["state"] == 4): ?>
                                    <span class="text-c7">处理完成</span>
                                    <?php else: ?>
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                       title="<?php echo ($v["memo"]); ?>" style="text-decoration: none"><span
                                            class="text-c8">拒绝</span></a><?php endif; ?>
                            </td>

                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <?php if($v['state'] == 2): ?><li>
                                                <a href="javascript:void(0)"
                                                   onclick="changeProviderWithdrawState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo Common\Top::ApplyStateFinish;?>, <?php echo (getProviderState($v["entity_id"])); ?>, <?php echo (getProviderAccountState($v["entity_id"])); ?>)">处理完成</a>
                                            </li>
                                            <?php elseif($v['state'] == 4): ?>
                                            <li>
                                                <a href="javascript:void(0)"
                                                   onclick="updateProof(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo ($v["state"]); ?>, 2)">更新凭证</a>
                                            </li><?php endif; ?>
                                    </ul>
                                </div>
                            </td>
                        </tr><?php endif; endif; endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">拒绝理由</h4>
        </div>
        <div class="modal-body" id="modal_reject">
            <textarea class="form-control" name="reason" style="resize: none;" placeholder="请填写拒绝理由"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    填写凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>