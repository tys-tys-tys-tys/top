<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">供应商管理</a> ><a
                href="<?php echo U('Provider/auditTourProduct',array('id'=>I('get.id')));?>">产品详情</a>&gt;<?php echo ($provider_info["name"]); ?>
        </div>
        <div class="cTitle2"><?php echo ($provider_info["name"]); ?></div>
        <div role="tabpanel">
            <div class="nav ckAct" role="tablist" id="myTab">
                <span role="presentation" class="active"><a style='cursor: pointer' aria-controls="base" role="tab"
                                                            data-toggle="tab" class="a-1"><i
                        class="i-1"></i>基本信息</a></span>
                <span role="presentation"><a style='cursor: pointer' aria-controls="detail" role="tab" data-toggle="tab"
                                             class="a-2"><i class="i-2"></i>详细行程</a></span>
                <span role="presentation"><a style='cursor: pointer' aria-controls="kzinfo" role="tab" data-toggle="tab"
                                             class="a-3"><i class="i-3"></i>扩展信息</a></span>
                <span role="presentation"><a style='cursor: pointer' aria-controls="time" role="tab" data-toggle="tab"
                                             class="a-4"><i class="i-4"></i>团期/位控</a></span>
            </div>
            <div class="tab-content">
                <div class="tableshow" role="tabpanel" id="base">
                    <table class="baseInfo-Tb table-hover">
                        <tr>
                            <th>供应商：</th>
                            <td><?php echo ($provider_info["provider_name"]); ?></td>
                        </tr>
                        <tr>
                            <th>线路名称：</th>
                            <td><?php echo ($provider_info["name"]); ?></td>
                        </tr>
                        <tr>
                            <th>线路类型：</th>
                            <td>
                                <?php if($provider_info['line_type'] == 1): ?>周边游
                                    <?php elseif($provider_info['line_type'] == 2): ?>
                                    国内游
                                    <?php else: ?>
                                    出境游<?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>出发地：</th>
                            <td><?php echo ($provider_info["departure"]); ?></td>
                        </tr>
                        <tr>
                            <th>目的地：</th>
                            <td><?php echo ($provider_info["destination"]); ?></td>
                        </tr>
                        <tr>
                            <th>途径地：</th>
                            <td><?php echo ($provider_info["destinations"]); ?></td>
                        </tr>
                        <tr>
                            <th>去程交通：</th>
                            <td>
                                <?php if($provider_info['traffic_go'] == '火车'): ?>火车
                                    <?php elseif($provider_info['traffic_go'] == '飞机'): ?>
                                    飞机
                                    <?php elseif($provider_info['traffic_go'] == '轮船'): ?>
                                    轮船
                                    <?php elseif($provider_info['traffic_go'] == '巴士'): ?>
                                    巴士<?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>返程交通：</th>
                            <td>
                                <?php if($provider_info['traffic_back'] == '火车'): ?>火车
                                    <?php elseif($provider_info['traffic_back'] == '飞机'): ?>
                                    飞机
                                    <?php elseif($provider_info['traffic_back'] == '轮船'): ?>
                                    轮船
                                    <?php elseif($provider_info['traffic_back'] == '巴士'): ?>
                                    巴士<?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>线路图片：</th>
                            <td>
                                <?php if(is_array($provider_info['imgs'])): $i = 0; $__LIST__ = $provider_info['imgs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                    <img src="<?php echo (GetRealImageUrl($vo["0"])); ?>" alt="" style='margin:5px 5px;'
                                         class="img-thumb"/><?php endforeach; endif; else: echo "" ;endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>联系人：</th>
                            <td><?php echo ($provider_info["contact_person"]); ?></td>
                        </tr>
                        <tr>
                            <th>联系电话：</th>
                            <td><?php echo ($provider_info["contact_phone"]); ?></td>
                        </tr>
                        <tr>
                            <th>参团方式：</th>
                            <td>
                                <?php if($provider_info['travel_type'] == 1): ?>散拼团
                                    <?php elseif($provider_info['travel_type'] == 2): ?>
                                    自由行
                                    <?php elseif($provider_info['travel_type'] == 3): ?>
                                    当地游<?php endif; ?>
                            </td>
                        </tr>

                    </table>
                </div>
                <div class="tableshow" role="tabpanel" id="detail">
                    <table class="baseInfo-Tb table-hover">
                        <?php if(is_array($provider_info['tour_plan'])): $i = 0; $__LIST__ = $provider_info['tour_plan'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                                <th colspan="2" class="num"><strong>第<?php echo (DayDaXie($i)); ?>天</strong></th>
                            </tr>
                            <tr>
                                <th>活动地区：</th>
                                <td><?php echo ($v["activity_addr"]); ?></td>
                            </tr>
                            <tr>
                                <th>交通信息：</th>
                                <td>
                                    <?php if($v['traffic'] == 1): ?>火车
                                        <?php elseif($v['traffic'] == 2): ?>
                                        飞机
                                        <?php elseif($v['traffic'] == 3): ?>
                                        轮船
                                        <?php elseif($v['traffic'] == 4): ?>
                                        大巴<?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>景点及活动：</th>
                                <td><?php echo ($v["scenery"]); ?></td>
                            </tr>
                            <tr>
                                <th>行程描术：</th>
                                <td><?php echo ($v["description"]); ?></td>
                            </tr>
                            <tr>
                                <th>酒店信息：</th>
                                <td><?php echo ($v["hotel"]); ?></td>
                            </tr>
                            <tr>
                                <th>用餐信息：</th>
                                <td>
                                    <?php if(empty($v['dining'])): ?>自理
                                        <?php else: ?>
                                        <?php if(in_array(($din1), is_array($v['dining'])?$v['dining']:explode(',',$v['dining']))): ?>早餐&nbsp;<?php endif; ?>
                                        <?php if(in_array(($din2), is_array($v['dining'])?$v['dining']:explode(',',$v['dining']))): ?>午餐&nbsp;<?php endif; ?>
                                        <?php if(in_array(($din3), is_array($v['dining'])?$v['dining']:explode(',',$v['dining']))): ?>晚餐&nbsp;<?php endif; endif; ?>
                                </td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>
                </div>
                <div class="tableshow" role="tabpanel" id="kzinfo">
                    <table class="baseInfo-Tb table-hover">
                        <tr>
                            <th>产品特色：</th>
                            <td><?php echo ($provider_info["description"]); ?></td>
                        </tr>
                        <tr>
                            <th>注意事项：</th>
                            <td><?php echo ($provider_info["notice"]); ?></td>
                        </tr>
                        <tr>
                            <th>费用包含：</th>
                            <td><?php echo ($provider_info["fee_include"]); ?></td>
                        </tr>
                        <tr>
                            <th>费用不包含：</th>
                            <td><?php echo ($provider_info["fee_exclude"]); ?></td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tableshow" id="time">
                    <table class="cTable table-hover" width="100%">
                        <tr>
                            <th>编号</th>
                            <th>出发时间</th>
                            <th>成人零售价</th>
                            <th>成人同行价</th>
                            <th>儿童零售价</th>
                            <th>儿童同行价</th>
                            <th>可售人数</th>
                            <th>报名截止</th>
                            <th>状态</th>
                        </tr>
                        <?php if(is_array($provider_info['tour_sku'])): $i = 0; $__LIST__ = $provider_info['tour_sku'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                                <td><?php echo ($i); ?></td>
                                <td><?php echo (substr($vo["start_time"],0,10)); ?></td>
                                <?php if($vo['cruise'] == ''): ?><td class="text-c1"><?php echo (GetYuan($vo["price_adult_list"])); ?></td>
                                    <td class="text-c5"><?php echo (GetYuan($vo["price_adult_agency"])); ?></td>
                                    <?php else: ?>
                                    <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                           title="<?php echo ($vo["cruise"]); ?>" class="text-c1"><?php echo (GetYuan($vo["price_adult_list"])); ?></a></td>
                                    <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                           title="<?php echo ($vo["cruise"]); ?>" class="text-c5"><?php echo (GetYuan($vo["price_adult_agency"])); ?></a>
                                    </td><?php endif; ?>
                                <td><?php echo (GetYuan($vo["price_child_list"])); ?></td>
                                <td><?php echo (GetYuan($vo["price_child_agency"])); ?></td>
                                <td><?php echo ($vo["sku"]); ?></td>
                                <td><?php echo ($vo["pre_join_days"]); ?>天</td>
                                <td>
                                    <?php if($vo["state"] == 1): ?><span style="color:blue">正常</span>
                                        <?php elseif($vo["state"] == 4): ?>
                                        <span style="color:red">异常</span><?php endif; ?>
                                </td>
                            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        //审核产品切换
        $('.tableshow').not(".tableshow:first").hide();
        $('.ckAct span:first').addClass('active');
        $('.ckAct span').click(function () {
            var id = $(this).index();
            $('.ckAct span').removeClass('active');
            $(this).addClass('active');
            $('.tableshow').hide();
            $('.tableshow').eq(id).show();
        });
    })


    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>