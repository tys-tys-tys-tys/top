<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">代理商管理</a>&gt;代理商列表</div>
        <div class="cTitle2">代理商列表 <span class='text-c1'>(蓝色姓名表示测试人员)</span></div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="agencyTotalNum" value="<?php echo ($agencyTotalNum); ?>">
            <input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-agency-state nav-operator-agency-state0 agency-total-num"
                   href="<?php echo U('Agency/index', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-agency-state nav-operator-agency-state<?php echo ($vo["id"]); ?> agency-total-num"
                       href="<?php echo U('Agency/index', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search" style="margin-bottom: 28px">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Agency/index',array('id' => I('get.id'), 'oid' => I('get.oid')));?>"
                      class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入代理商姓名" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>

            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getAgencyListExcel');?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>

            <form class="form-inline pull-right">
                <a href="javascript:void(0)" id="addAgency" class="btn btn-info">新增</a>
                <input id="chkr" type="hidden" value="<?php echo U('Agency/delete');?>"/>
                <input id="chka" type="hidden" value="<?php echo U('Agency/add');?>"/>
                <input id="chkState" type="hidden" value="<?php echo U('Agency/setAccountStatus');?>"/>
                <input id="chke" type="hidden" value="<?php echo U('Agency/edit');?>"/>
                <input id="checkName" type="hidden" value="<?php echo U('Agency/add');?>"/>
                <input id="checkDomain" type="hidden" value="<?php echo U('Agency/checkDomain');?>"/>
                <input id="editAgencyPwd" type="hidden" value="<?php echo U('Agency/editAgencyPwd');?>"/>
                <input id="reduceServiceCharge" type="hidden" value="<?php echo U('Agency/reduceServiceCharge');?>"/>
                <input id="saveSeniorEdit" type="hidden" value="<?php echo U('Agency/seniorEdit');?>"/>
                <input id="resetAgencyOpenId" type="hidden" value="<?php echo U('Agency/resetAgencyOpenId');?>"/>
                <input id="setAccount" type="hidden" value="<?php echo U('Agency/setAccount');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
                <input id="showProviderAccount" type="hidden" value="<?php echo U('Agency/showAgencyAccountInfo');?>">
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%" style="padding-left:5px;"><input type="checkbox" id="male"
                                                                onclick="selectAll(this);"/>&nbsp;<label for="male"
                                                                                                         style="font-weight: bold">全选 </label>
                </th>
                <th width="8%">运营商</th>
                <th width="10%">名称</th>
                <th width="10%">电话</th>
                <th width="10%">合约起止</th>
                <th width="10%">账户总额</th>
                <th width="5%">账户</th>
                <th width="8%">微信绑定</th>
                <th width="8%">账户状态</th>
                <th width="8%">状态</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem"
                                                                          value="<?php echo (setEncrypt($v["id"])); ?>"/>
                    </td>
                   
                    <td><?php echo (getOperatorName($v["operator_id"])); ?></td>
                    <td>
                        <?php if($v['istest'] == 1): ?><span class='text-c1'><?php echo ($v["name"]); ?></span>
                            <?php else: ?>
                            <?php echo ($v["name"]); endif; ?>
                    </td>
                    <td><?php echo ($v["mobile"]); ?></td>
                    <td>
                        <?php if(($v['contract_start_time'] != '0000-00-00 00:00:00') and $v['contract_end_time'] != '0000-00-00 00:00:00'): if(getContractEndTime($v['contract_end_time']) < $todayDate): ?><span class="text-c8"><?php echo (substr($v["contract_start_time"],0,10)); ?><br/><?php echo (substr($v["contract_end_time"],0,10)); ?></span>
                                <?php else: ?>
                                <?php echo (substr($v["contract_start_time"],0,10)); ?><br/><?php echo (substr($v["contract_end_time"],0,10)); endif; endif; ?>
                    </td>
                    <td id="money-right" class="text-c5">&yen; <dfn><?php echo (GetYuan($v["account_balance"])); ?></dfn></td>
                    <td>
                        <a onclick="showProviderAccountInfo(<?php echo ($v["id"]); ?>)" class="text-c1" style="cursor: pointer">查看</a>
                    </td>
                    <td>
                        <?php if($v['weixinmp_openid'] == ''): ?><span class="text-c5">否</span>
                            <?php else: ?>
                            <span class="text-c1">是</span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($v["account_state"] == 1): ?><span class="text-c7">正常</span>
                            <?php elseif($v["account_state"] == 2): ?>
                            <span class="text-c8">冻结</span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($v["state"] == 1): ?><span class="text-c7">正常</span>
                            <?php elseif($v["state"] == 2): ?>
                            <span class="text-c8">禁用</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                            
                                <li><a href="<?php echo U('Agency/edit',array('aid'=>$v['id'],'id'=>I('get.id')));?>">编辑</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="changeState(<?php echo ($v["id"]); ?>, '<?php echo Common\Top::StateEnabled;?>')">正常</a></li>
                                <li>
                                    <a href="javascript:void(0)"
                                       onclick="changeAccountState(<?php echo ($v["id"]); ?>, '<?php echo Common\Top::StateDisabled;?>')">禁用</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="changeAccountState(<?php echo ($v["id"]); ?>, 'freeze<?php echo Common\Top::StateDisabled;?>')">冻结</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="changeState(<?php echo ($v["id"]); ?>, 'nofreeze<?php echo Common\Top::StateEnabled;?>')">取消冻结</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="seniorEditAgencyInfo(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo ($v["name"]); ?>', '<?php echo ($v["idcard"]); ?>')">高级编辑</a></li>
                                     <li><a href="javascript:void(0)"
                                       onclick="reduceServiceCharge(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo ($v["name"]); ?>')">扣除服务费</a>
                                </li>
                                <li><a href="javascript:void(0)"
                                       onclick="editAgencyPwd(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo ($v["name"]); ?>')">修改管理密码</a>
                                </li>
                                 
                                <li><a href="javascript:void(0)"
                                       onclick="resetAgencyOpenId(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo ($v["name"]); ?>')">重置微信绑定</a>
                                </li>
                                <li><a href="<?php echo U('Agency/showTransaction',array('id' => 6,'aid' => $v['id']));?>"
                                       target='_blank'>查看交易记录</a></li>

                                <?php if($v['istest'] == 0): ?><li><a href="javascript:void(0)"
                                           onclick="setAccount(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo Common\Top::IsTested;?>')">设为测试账号</a>
                                    </li>
                                    <?php else: ?>
                                    <li><a href="javascript:void(0)"
                                           onclick="setAccount(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo Common\Top::NotTested;?>')">取消测试账号</a>
                                    </li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<!--渲染输出公共底部-->
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="http://cdn.lydlr.com/public/js/showDate/WdatePicker.js"></script>
<form method="post" action="<?php echo U('Agency/add');?>" class="zcform">
    <div class="theme-popover">
        <div class="theme-poptit">
            <a title="关闭" class="closes">&times;</a>

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">真实姓名：</label>

                        <div class="col-sm-4">
                            <input type="text" id="userName" class="form-control" placeholder="请输入代理商姓名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">手机：</label>

                        <div class="col-sm-4">
                            <input type="number" class="form-control" id="telphone"
                                   placeholder="请输入手机号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">密码：</label>

                        <div class="col-sm-4">
                            <input type="password" class="form-control" id="password"
                                   placeholder="请输入密码"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">确认密码：</label>

                        <div class="col-sm-4">
                            <input type="password" class="form-control" id="confirm_password"
                                   placeholder="确认密码"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">绑定域名：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="domain" placeholder="请输入要绑定的域名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">所属行业：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="vocation" placeholder="如：领队、导游、职员等"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="doSubmit">确定</button>
                <button type="button" class="btn btn-default " id="reset">取消</button>
            </div>
        </div>
    </div>
    <div class="theme-popover-mask"></div>
</form>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    修改代理商密码
                </h4>
            </div>
            <div class="modal-body">
                代理商：
                <input type="text" id="agencyName" style="border:0px">

                <p>
                    新密码：
                    <input type="password" class="form-control" id="newpassword">
                    确认密码：
                    <input type="password" class="form-control" id="password2">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveAgencyPwd">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalServer" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                   扣除服务费
                </h4>
            </div>
            <div class="modal-body">
                代理商：
                <input type="text" id="agencyName" style="border:0px">

                <p>
                    服务名称：
                    <input type="text" class="form-control" id="dl_server_name">
                    服务价格：
                    <input type="number" class="form-control" id="dl_server_money">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveAgencyServer">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel1">
                    查看代理商账户信息
                </h4>
            </div>
            <div class="modal-body" style="font-size: 15px">
                <p>&nbsp;&nbsp;代理商：<span class="text-c5 provider_name"></span></p>

                <p>账户总额: <span class="text-c5">&yen; <dfn class="total"></dfn></span></p>

                <p>冻结金额: <span class="text-c5">&yen; <dfn class="frozenMoney"></dfn></span></p>

                <p>可用余额: <span class="text-c5">&yen; <dfn class="availableBalance"></dfn></span></p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-lg" id="myModalNotice" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
            </div>
            <div class="modal-body text-center">
                <textarea class="form-control reason" placeholder="请输入理由"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalAgencyInfo" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    高级编辑
                </h4>
            </div>
            <div class="modal-body" style="font-size: 15px">
                真实姓名
                <input type="text" class="form-control real_name">
                证件类型
                <select class="form-control identity_type">
                    <option value="1">身份证</option>
                </select>
                证件号码
                <input type="text" class="form-control identity_num">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-info submitEdit"
                            data-dismiss="modal">确认
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="/Public/js/jQuery.Hz2Py-min.js"></script>
<script type="text/javascript">
    $("#userName").blur(function () {
        var url = $("#checkDomain").val();
        $("#domain").val($(this).toPinyin().replace(/[ ]/g, "").toLowerCase() + ".m.dalvu.com");
        var domain = $("#domain").val();
        $.post(url, {domain: domain}, function ($data) {
            console.info($data);
            if ($data['status'] == 1) {
                $("#domain").val($data['domain']);
            }
        })
    });

    $("#doSubmit").click(function () {
        var userName = $("#userName").val();
        var url = $("#checkName").val();
        var telphone = $("#telphone").val();
        var login_pwd = $("#password").val();
        var login_pwd2 = $("#confirm_password").val();
        var domain = $("#domain").val();
        var vocation = $("#vocation").val();
        var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

        if (userName == '') {
            jalert('真实姓名不能为空', function () {
            });
            return false;
        }

        if (telphone == '') {
            jalert('手机号不能为空', function () {
            });
            return false;
        }

        if (!reg.test(telphone)) {
            jalert('请填写11位手机号', function () {
            });
            return false;
        }

        if (login_pwd == '') {
            jalert('密码长度不能为空', function () {
            });
            return false;
        }

        if (login_pwd.length < 6) {
            jalert('密码长度不得少于6位', function () {
            });
            return false;
        }

        if (login_pwd != login_pwd2) {
            jalert('两次密码输入不一致', function () {
            });
            return false;
        }

        if (domain == '') {
            jalert('域名不能为空', function () {
            });
            return false;
        }

        if (vocation == '') {
            jalert('请输入所属行业', function () {
            });
            return false;
        }

        $.post(url, {
            telphone: telphone,
            login_name: telphone,
            domain: domain,
            name: userName,
            login_pwd: login_pwd,
            vocation: vocation
        }, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        });

    });

</script>
</body>
</html>