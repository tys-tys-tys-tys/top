<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>

    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">通用产品管理</a></div>
        <div class="padbox slides">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="bg"></div>
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/1.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/2.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/9.jpg" alt="...">
                    </div>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="cTitle" style="display: none">统计信息</div>
        <table class="countTable" width="100%" style="display: none">
            <tr>
                <th>代理商数</th>
                <td>33</td>
            </tr>
            <tr>
                <th>供应商数</th>
                <td>2</td>
            </tr>
            <tr>
                <th>上架产品数</th>
                <td>5</td>
            </tr>
            <tr>
                <th>下架产品数</th>
                <td>4</td>
            </tr>
            <tr>
                <th>提现已审核</th>
                <td>25</td>
            </tr>
            <tr>
                <th>提现待审核</th>
                <td>1</td>
            </tr>
            <tr>
                <th>提现拒绝</th>
                <td>10</td>
            </tr>
        </table>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width: 350px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <p>微信扫描二维码</p>
                </h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox">
                    <img src="<?php echo U('Tour/temporary');?>">
                <p class="friend-info">微信号成功绑定后, 您可以实时微信接收系统消息</p>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info center-block" data-dismiss="modal" aria-label="Close" style="width: 150px;">关闭</button>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<input type="hidden" class="user_id" value="<?php echo (session('user_id')); ?>">
<input type="hidden" class="userOpenId" value="<?php echo ($userInfo["weixinmp_openid"]); ?>">
<input type="hidden" class="userInfo" value="<?php echo U('Tour/userInfo');?>">
<script>
    var user_id = $(".user_id").val();
    var userOpenId = $(".userOpenId").val();

    if(userOpenId == ''){
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        var s1 = false;
        var t1 = window.setInterval(queryInfo, 3000);

        if(s1 == true){
            window.clearInterval(queryInfo());
        }
    }

    function queryInfo(){
        var url = $(".userInfo").val();
        var info = {};
        info.id = user_id;
        $.post(url, info, function($data){
            if($data['status'] == 1){
                s1 = true;
                $(".tipsBox").empty();
                $(".friend-info").empty();
                $(".tipsBox").html("<span style='color:green;'><img src='http://cdn.lydlr.com/public/system/images/icon-suc.png'>&nbsp;恭喜您!已成功绑定账号！</span>");
            }
        })
    }
</script>
</body>
</html>