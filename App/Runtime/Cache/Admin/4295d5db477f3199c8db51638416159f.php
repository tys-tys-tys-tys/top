<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">用户/角色</a>&gt;用户列表</div>
        <div class="cTitle2">用户列表</div>
        <div class="main-search">
            <form class="pull-right" style="padding-bottom: 5px;">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-info"
                   onclick="addUser()">新增</a>
                <input id="chkr" type="hidden" value="<?php echo U('User/delete');?>"/>
                <input id="chka" type="hidden" value="<?php echo U('User/add');?>"/>
                <input id="chke" type="hidden" value="<?php echo U('User/edit');?>"/>
                <input id="chkState" type="hidden" value="<?php echo U('User/index');?>"/>
                <input id="getUserName" type="hidden" value="<?php echo U('User/getUserName');?>"/>
                <input id="saveNotifyType" type="hidden" value="<?php echo U('User/saveNotifyType');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold">全选</label></th>
                <th>姓名</th>
                <th>登录名</th>
                <th>角色</th>
                <th>创建时间</th>
                <th>状态</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>
                    <td><?php echo ($v["name"]); ?></td>
                    <td><?php echo ($v["login_name"]); ?></td>
                    <td><?php echo ($v["role_name"]); ?></td>
                    <td><?php echo ($v["create_time"]); ?></td>
                    <td>
                        <?php if($v["state"] == 1): ?><span class="text-c7">正常</span>
                            <?php else: ?>
                            <span class="text-c8">禁用</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="editUser(<?php echo ($v["id"]); ?>, '用户编辑')" style="cursor: pointer">编辑</a></li>
                                <li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::StateEnabled;?>)"
                                       style="cursor: pointer">正常</a></li>
                                <li><a onclick="changeAccountState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::StateDisabled;?>)"
                                       style="cursor: pointer">禁用</a></li>
                                <?php if($_SESSION['entity_type']== 3): ?><li><a onclick="providerAppointType(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer">消息管理</a></li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">用户添加</h4>
        </div>
        <div class="modal-body" style="border-bottom: 1px solid #e5e5e5;">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">真实姓名：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="" id="modal_name"/>
                    </div>
                    <span class="col-sm-6 tp">*必须是真实姓名</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">登录名：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="" id="modal_login"/>
                    </div>
                    <span class="col-sm-6 tp">*必须填写登录名称（英文、数字）</span>
                </div>
                <div class="form-group user-pass">
                    <label class="col-sm-2 control-label">密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" placeholder="" id="modal_pwd"/>
                    </div>
                    <span class="col-sm-6 tp">*必须输入登录名密码（字母、数字）</span>
                </div>
                <div class="form-group user-confirm-pass">
                    <label class="col-sm-2 control-label">确认密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" placeholder="" id="modal_pwd2"/>
                    </div>
                    <span class="col-sm-6 tp">*必须输入登录名密码（字母、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">手机：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="" id="mobile"/>
                    </div>
                    <span class="col-sm-6 tp">*必须输入11位手机号</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">所属角色：</label>

                    <div class="col-sm-4">
                        <select class="form-control" id="modal_role">
                            <option value="0" id="sel">请选择</option>
                            <?php if(is_array($role_info)): $i = 0; $__LIST__ = $role_info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                    <span class="col-sm-6 tp">*角色必须选择</span>
                </div>
            </div>
            <div class="modal-footer pull-right" style="border:0;margin-top:15px;">
                <button type="button" class="btn btn-warning" id="buttonSaveUserInfo">保存</button>
                <button type="button" class="btn btn-default " id="reset">取消</button>
            </div>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalmob" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width: 380px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">请为 <span class="appointName" style="color: #33b3b8"></span> 指定接收消息的类型</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="checkbox" class="productMessage"> 产品消息
                    <p></p>
                    <input type="checkbox" class="transactionMessage"> 交易消息
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning saveNotifyType" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade popWin bs-example-modal-lg" id="myModalNotice" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 700px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">温馨提示</h4>
            </div>
            <div class="modal-body text-center">
                <textarea class="form-control reason" placeholder="请输入理由"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script src="/Public/js/user.js?v=6"></script>
</body>
</html>