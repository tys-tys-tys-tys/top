<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：
            <?php if($_SESSION['entity_type']== 3): ?><a href="<?php echo U('Tour/index',array('id'=>I('get.id')));?>">产品列表</a>
            <?php else: ?>
                <a href="<?php echo U('Provider/auditTourProduct',array('id'=>I('get.id')));?>">产品列表</a><?php endif; ?>
            &gt;编辑路线&gt;基本信息</div>
        <div class="cTitle2">基本信息</div>
        <div role="tabpanel">
            <div class="nav ckAct">
                <span class="active"><a href="<?php echo U('Tour/edit',array('id'=>I('get.id'),'tid'=>$tourInfo['id']));?>"
                                        class="a-1"><i class="i-1"></i>基本信息</a></span>
                <span><a href="<?php echo U('Tour/editPlan',array('id'=>I('get.id'),'tid'=>$tourInfo['id']));?>" class="a-2"><i
                        class="i-2"></i>详细行程</a></span>
                <span><a href="<?php echo U('Tour/extendEdit',array('id'=>I('get.id'),'tid'=>$tourInfo['id']));?>" class="a-3"><i
                        class="i-3"></i>扩展信息</a></span>
                <span><a href="<?php echo U('Tour/editSku',array('id'=>I('get.id'),'tid'=>$tourInfo['id']));?>" class="a-4"><i
                        class="i-4"></i>团期/位控</a></span>
            </div>
            <div class="tab-content">
                <div class="panelCon tab-pane active">
                    <table class="baseInfo-Tb table-hover">
                        <tr>
                            <th>供应商：</th>
                            <td><?php echo ($tourInfo["provider"]["name"]); ?></td>
                        </tr>
                        <tr>
                            <th>线路名称：</th>
                            <td>
                                <input type="text" name="name" class="form-control txt" id="linename" placeholder="<?php echo ($tourInfo["name"]); ?>" value="<?php echo ($tourInfo["name"]); ?>"/>
                                <span class="text-c4" id="test1Tip"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>线路类型：</th>
                            <td id="line_type">
                                <label class="checkbox-inline">
                                    <input type="radio" value="1" name="line_type"
                                    <?php if($tourInfo['line_type'] == 1): ?>checked<?php endif; ?>
                                    />周边游
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="2" name="line_type"
                                    <?php if($tourInfo['line_type'] == 2): ?>checked<?php endif; ?>
                                    />国内游
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="3" name="line_type"
                                    <?php if($tourInfo['line_type'] == 3): ?>checked<?php endif; ?>
                                    />出境游
                                </label>
                                <span style="margin-left:40px;line-height: 25px;"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>出发地：</th>
                            <td>
                                <select name="departure" id="departure" class="form-control sel" onchange="departure()">
                                    <option value="0">请选择</option>
                                    <?php if(is_array($province)): $i = 0; $__LIST__ = $province;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["name"]); ?>"
                                        <?php if(($tourInfo['departure']['0'] == $v['name'])): ?>selected<?php endif; ?>
                                        ><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                                <select name="departure1" id="departure1" class="form-control sel"
                                        onchange="departure1()">
                                    <?php if(is_array($city)): $i = 0; $__LIST__ = $city;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vv["name"]); ?>"
                                        <?php if(($tourInfo['departure']['1'] == $vv['name'])): ?>selected<?php endif; ?>
                                        ><?php echo ($vv["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                                <span class="text-c4" id="csnyTip"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>授权区域：</th>
                            <td>
                                <select name="destination" id="destination" class="form-control sel"
                                        onchange="destination('destination')">
                                    <option value="0">请选择</option>
                                    <?php if(is_array($tourInfo['destination1'])): $i = 0; $__LIST__ = $tourInfo['destination1'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v); ?>"
                                        <?php if(($tourInfo['destination'] == $v)): ?>selected<?php endif; ?>
                                        ><?php echo ($v); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                                <span class="text-c4" id="csnyTips"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>目的地：</th>
                            <td>
                                <input type="text" name="destinations" id="destinations" onblur="destinations()"
                                       class="form-control txt" placeholder="<?php echo ($tourInfo["destinations"]); ?>"
                                       value="<?php echo ($tourInfo["destinations"]); ?>"/>
                                <span class="text-c4" id="csnyTipss"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>去程交通：</th>
                            <td id="traffic_go">
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_go" value="火车"
                                    <?php if(($tourInfo['traffic_go'] == '火车')): ?>checked<?php endif; ?>
                                    />火车
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_go" value="飞机"
                                    <?php if(($tourInfo['traffic_go'] == '飞机')): ?>checked<?php endif; ?>
                                    />飞机
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_go" value="轮船"
                                    <?php if(($tourInfo['traffic_go'] == '轮船')): ?>checked<?php endif; ?>
                                    />轮船
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_go" value="巴士"
                                    <?php if(($tourInfo['traffic_go'] == '巴士')): ?>checked<?php endif; ?>
                                    />巴士
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_go" value="其他"
                                    <?php if(($tourInfo['traffic_go'] == '其他')): ?>checked<?php endif; ?>
                                    />其他
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>返程交通：</th>
                            <td id="traffic_back">
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_back" value="火车"
                                    <?php if(($tourInfo['traffic_back'] == '火车')): ?>checked<?php endif; ?>
                                    />火车
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_back" value="飞机"
                                    <?php if(($tourInfo['traffic_back'] == '飞机')): ?>checked<?php endif; ?>
                                    />飞机
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_back" value="轮船"
                                    <?php if(($tourInfo['traffic_back'] == '轮船')): ?>checked<?php endif; ?>
                                    />轮船
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_back" value="巴士"
                                    <?php if(($tourInfo['traffic_back'] == '巴士')): ?>checked<?php endif; ?>
                                    />巴士
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="traffic_back" value="其他"
                                    <?php if(($tourInfo['traffic_back'] == '其他')): ?>checked<?php endif; ?>
                                    />其他
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>业务联系人：</th>
                            <td>
                                <input type="text" class="form-control txt" id="contact_person"
                                       onblur="contact_person()" placeholder="<?php echo ($tourInfo["contact_person"]); ?>"
                                       value="<?php echo ($tourInfo["contact_person"]); ?>"/>
                                <span class="text-c4" id="contact_personTip"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>业务联系电话：</th>
                            <td>
                                <input type="text" name="contact_phone" id="contact_phone" onblur="contact_phone()"
                                       class="form-control txt" placeholder="<?php echo ($tourInfo["contact_phone"]); ?>"
                                       value="<?php echo ($tourInfo["contact_phone"]); ?>"/>
                                <span class="text-c4" id="contact_phoneTip"></span>
                            </td>
                        </tr>
                        <tr>
                            <th>参团方式：</th>
                            <td id="travel_type">
                                <label class="checkbox-inline">
                                    <input type="radio" value="1" name="travel_type"
                                    <?php if(($tourInfo['travel_type'] == 1)): ?>checked<?php endif; ?>
                                    />散拼团
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="2" name="travel_type"
                                    <?php if(($tourInfo['travel_type'] == 2)): ?>checked<?php endif; ?>
                                    />自由行
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" value="3" name="travel_type"
                                    <?php if(($tourInfo['travel_type'] == 3)): ?>checked<?php endif; ?>
                                    />当地游
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <th>产品图片：</th>
                            <td id="pics">
                                <div class="row" style="margin:10px;">
                                    <div class="col-sm-7">
                                        <h4 id="uploadMessage">当前共有 <?php echo ($pic_num); ?> 张图片</h4>
                                    </div>
                                    <div class="col-sm-5">
                                        <div id="filePicker"
                                             class="btn btn-sm btn-primary webuploader-container webuploader-picker">
                                            选择图片
                                        </div>
                                        <input id="btnUploadCtrl" type="button" class="btn btn-sm btn-primary"
                                               value="开始上传"/>
                                    </div>
                                </div>
                                <hr style="padding-bottom: 1px;"/>
                                <input type="hidden" id="cover-pic"/>

                                <div class="container" style="max-width: 660px;" id="fileList">
                                    <?php if(is_array($tourInfo['imgs'])): $i = 0; $__LIST__ = $tourInfo['imgs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo['0'])): ?><div id="" class="row upload-item" style="margin-bottom: 10px;"
                                                 path="<?php echo ($vo["0"]); ?>" isCover="<?php echo ($vo["1"]); ?>">
                                                <div class="col-sm-2"><img
                                                    <?php if($vo['0'] == $tourInfo['cover_pic']): ?>class="img-preview img-cover"
                                                        <?php else: ?>
                                                        class="img-preview"<?php endif; ?>
                                                    alt="" src="<?php echo (GetRealImageUrl($vo["0"])); ?>">
                                                </div>
                                                <div class="col-sm-8">
                                                    <textarea rows="3" class="form-control" placeholder="请输入图片描述"><?php echo ($vo["2"]); ?></textarea>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="dropdown">
                                                        <button data-toggle="dropdown" id="dropdownMenu1"
                                                                class="btn dropdown-toggle" type="button">
                                                            操作
                                                            <span class="caret"></span>
                                                        </button>
                                                        <ul aria-labelledby="dropdownMenu1" role="menu"
                                                            class="dropdown-menu">
                                                            <li role="presentation">
                                                                <a class="move-up" href="#" tabindex="-1"
                                                                   role="menuitem">上移</a>
                                                            </li>
                                                            <li role="presentation">
                                                                <a class="move-down" href="#" tabindex="-1"
                                                                   role="menuitem">下移</a>
                                                            </li>
                                                            <li role="presentation">
                                                                <a class="setCover" href="javascript:;" tabindex="-1"
                                                                   role="menuitem">设为封面</a>
                                                            </li>
                                                            <li role="presentation">
                                                                <a class="removeFile uploaded" href="javascript:;"
                                                                   tabindex="-1" role="menuitem">
                                                                    移除图片
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="upload-msg"></div>
                                                </div>
                                            </div><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td>
                                <button type="button" class="btn btn-warning pull-right"
                                        onclick="edittourinfo(<?php echo ($tourInfo["id"]); ?>, '<?php echo (setEncrypt($tourInfo["id"])); ?>')">保存
                                </button>
                                <input type="hidden" value="<?php echo U('Tour/edit',array('id'=>I('get.id')));?>"
                                       id="edittourinfo"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="/Public/js/addtour.js?v=2"></script>
<script type="text/javascript" src="http://cdn.lydlr.com/public/webuploader-0.1.5/webuploader.js"></script>
<script type="text/javascript" src="http://cdn.lydlr.com/public/js/upload.js?v=2"></script>
</body>
</html>