<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">订单管理</a>&gt;线路产品订单</div>
        <div class="cTitle2">
            线路产品订单
        </div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Order/index', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Order/index', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Order/index',array('id' => I('get.id'), 'oid' => I('get.oid')));?>"
                      class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="编号/代理/线路/供应商" value="<?php echo ($name); ?>"/>
                    </div>
                    <span style="margin-left: 50px"> </span>

                    <div class="form-group" style="margin-right: -40px;">
                        <select name="date_type" style="height:35px;line-height: 35px;border: 1px solid #c0c0c0">
                            <?php if($dateType == 2): ?><option value="2">创建时间</option>
                                <option value="1">团期</option>
                                <?php else: ?>
                                <option value="1">团期</option>
                                <option value="2">创建时间</option><?php endif; ?>
                        </select>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                               placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                               placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>

                    <input type="hidden" id="chkState" value="<?php echo U('OrderTour/index');?>"/>
                </form>
            </div>
            <p></p>

            <form method="post"
                  action="<?php echo U('GetApplyExcel/getTourExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="hidden" name="date_type" value="<?php echo ($dateType); ?>">
                <input type="hidden" id="orderCancel" value="<?php echo U('Order/orderCancel');?>"/>
                <input type="hidden" id="orderCancelFinance" value="<?php echo U('Order/orderCancelFinance');?>"/>
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="5%">代理商</th>
                <th width="5%">所属运营商</th>
                <th width="10%">线路名称</th>
                <th width="9%">供应商名称</th>
                <th width="8%">全款金额</th>
                <th width="8%">预付金额</th>
                <th width="8%">实际预付</th>
                <th width="9%">预付时间</th>
                <th width="9%">付尾款时间</th>
                <th width="8%">实际收入</th>
                <th width="5%">佣金</th>
                <th width="6%">团期</th>
                <th width="10%">创建时间</th>
                <th width="10%">状态</th>
                <th width="10%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td><?php echo ($vo["agency_name"]); ?></td>
                    <td><?php echo ($vo["operator_name"]); ?></td>
                    <td>
                        <a href="<?php echo U('OrderTour/details',array('id'=>43,'sid'=>$vo['id']));?>" target='_blank'
                           class='text-c1'>
                            <?php echo ($vo["tour_name"]); ?>
                        </a>
                    </td>
                    <td><?php echo ($vo["provider_name"]); ?></td>
                    <td id="money-right"
                        class="text-c5"><?php echo (($vo['price_adjust'] + $vo['price_total'])/100).".00"; ?></td>
                    <td id="money-right" class="text-c1"><?php echo (GetYuan($vo["prepay_amount"])); ?></td>
                    <td id="money-right" class="text-c1"><?php echo (GetYuan($vo["prepay_amount"])); ?></td>
                    <td><?php echo ($vo["prepay_time"]); ?></td>
                    <td><?php echo ($vo["fullpay_time"]); ?></td>
                    <?php if($vo['state'] == 2): ?><td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 3): ?>
                        <td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 5): ?>
                        <td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 6): ?>
                        <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td>
                        <?php elseif($vo['state'] == 7): ?>
                        <td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                        <?php elseif($vo['state'] == 8): ?>
                        <td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                        <?php elseif($vo['state'] == 9): ?>
                        <?php if($vo['fullpay_time'] != ''): ?><td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                            <?php else: ?>
                            <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td><?php endif; ?>
                        <?php elseif($vo['state'] == 10): ?>
                        <?php if($vo['fullpay_time'] != ''): ?><td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                            <?php else: ?>
                            <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td><?php endif; ?>
                        <?php elseif($vo['state'] == 11): ?>
                        <?php if($vo['fullpay_time'] != ''): ?><td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                            <?php else: ?>
                            <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td><?php endif; ?>
                        <?php elseif($vo['state'] == 12): ?>
                        <?php if($vo['fullpay_time'] != ''): ?><td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                            <?php else: ?>
                            <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td><?php endif; ?>
                        <?php elseif($vo['state'] == 13): ?>
                        <?php if($vo['fullpay_time'] != ''): ?><td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td>
                            <?php else: ?>
                            <td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td><?php endif; endif; ?>
                    <td id="money-right" class="text-c5">
                        <?php if($vo['state'] == 6): echo (GetYuan($vo["prepay_commission"])); ?>
                            <?php elseif($vo['state'] == 7): ?>
                            <?php echo (GetYuan($vo["total_commission"])); ?>
                            <?php else: ?>
                            0.00<?php endif; ?>
                    </td>
                    <td class="text-c1"><?php echo (substr($vo["start_time"],0,10)); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td>
                        <?php if($vo["state"] == 1): ?><span style="color:red">未提交</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:blue">已提交</span></a>
                            <?php elseif($vo["state"] == 3): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:darkgreen">已确认待付款</span></a>
                            <?php elseif($vo["state"] == 4): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:red">已取消</span></a>
                            <?php elseif($vo["state"] == 5): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:red">已关闭</span></a>
                            <?php elseif($vo["state"] == 6): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:darkgreen">已付预付款</span></a>
                            <?php elseif($vo["state"] == 7): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:blue">已付全款</span></a>
                            <?php elseif($vo["state"] == 8): ?>
                            <span style="color:green">已退款</span>
                            <?php elseif($vo["state"] == 9): ?>
                            <span style="color:orangered">申请取消,待确认</span>
                            <?php elseif($vo["state"] == 10): ?>
                            <span style="color:orangered">确认取消,待初核</span>
                            <?php elseif($vo["state"] == 11): ?>
                            <span style="color:orangered">拒绝取消</span>
                            <?php elseif($vo["state"] == 12): ?>
                            <span style="color:orangered">确认取消,待复核</span>
                            <?php elseif($vo["state"] == 13): ?>
                            <span style="color:orangered">已取消</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group pull-right">
                            <button type="button" style="padding:3px 7px" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="margin: 0px;background: #eee">
                                <?php if($vo['state'] == 10): ?><li>
                                        <a onclick="orderCancel(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateAgencyAuditRefund;?>, '您确定要同意该订单的取消申请吗')"
                                           style="cursor: pointer;padding:0px 3px;">同意取消订单</a></li>
                                    <li>
                                        <a onclick="orderCancel(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateCancelRefund;?>, '您确定要拒绝该订单的取消申请吗')"
                                           style="cursor: pointer;padding:0px 3px;">拒绝取消订单</a></li><?php endif; ?>

                                <?php if($vo['state'] == 12): ?><li>
                                        <a onclick="orderCancelFinance(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateFinanceAuditRefund;?>, '您确定要同意该订单的取消申请吗')"
                                           style="cursor: pointer;padding:0px 3px;">同意取消订单</a></li>
                                    <li>
                                        <a onclick="orderCancelFinance(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateCancelRefund;?>, '您确定要拒绝该订单的取消申请吗')"
                                           style="cursor: pointer;padding:0px 3px;">拒绝取消订单</a></li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>