<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>

    <form class="form-horizontal zcform" method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="<?php echo ($info["id"]); ?>">
        <input id="editName" type="hidden" value="<?php echo U('Operator/saveOperatorInfo');?>"/>
        <input id="editOperatorName" type="hidden" value="<?php echo U('Operator/saveInfo');?>"/>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><div class="rightbox pull-right">
                <div class="ur-here">
                    您当前的位置：
                    <a href="#">修改运营商11</a>
                </div>
                <div class="cTitle2">修改运营商</div>
                <div class="action">
                    <a href="javascript:void(0)" class="current" data-click-tab="1">基本信息</a>
                    <a href="javascript:void(0)" data-click-tab="2">扩展信息</a>
                </div>

                <div class="panelCon" id="tab-1">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">运营商名称：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control name"
                                   value="<?php echo ($info["name"]); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">登录名：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control login_name"
                                   value="<?php echo ($info["login_name"]); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">线路共享：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control share_operators" value="<?php echo ($info["share_operators"]); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">手机：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control mobile" value="<?php echo ($info["mobile"]); ?>"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">地址：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control address" value="<?php echo ($info["address"]); ?>"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">代理商充值信息：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control topup_info" value="<?php echo ($info["topup_info"]); ?>"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">支付宝信息：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control alipay_info"
                                   value="<?php echo ($info["alipay_info"]); ?>"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">发票抬头：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control invoice_title"
                                   value="<?php echo ($info["invoice_title"]); ?>"></div>
                    </div>


                    <div class="text-center clearfix pwb pull-right">
                        <input type="button" value="修改" class="btn btn-warning saveOperatorEdit"/>
                    </div>
                </div>

                <div class="panelCon" style="display:none" id="tab-2">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">绑定域名：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control domain"
                                       value="<?php echo ($info["domain"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">短信cdkey：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control cdkey"
                                       value="<?php echo ($info["sms_ym_cdkey"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">短信密码：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control pwd"
                                       value="<?php echo ($info["sms_ym_pwd"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">短信签名：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control signature"
                                       value="<?php echo ($info["sms_ym_signature"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">版权信息：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control copyright"
                                       value="<?php echo ($info["copyright"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">信息1：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control msg1" placeholder=""
                                       value="<?php echo ($info["msg1"]); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">座机：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control tel"
                                       placeholder="" value="<?php echo ($info["tel"]); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">传真：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control fax"
                                       placeholder="" value="<?php echo ($info["fax"]); ?>"></div>
                        </div>
                        <div class="form-group" id="modal_times">
                            <label class="col-sm-2 control-label">合约起止时间：</label>

                            <div class="col-sm-4" style="width:37.333%;">
                                <input type="text" id="timestart"
                                       style="width:42%;border:1px solid #ccc;height:35px;"
                                <?php if($info['contract_start_time'] != '00-00-00 00:00:00'): ?>value="<?php echo (substr($info["contract_start_time"],0,10)); ?>"<?php endif; ?>
                                class="Wdate" id="d4322" onclick="WdatePicker({el:$dp.$('d12')})"/>
                                到
                                <input type="text" id="timeend"
                                       style="width:42%;border:1px solid #ccc;height:35px;"
                                <?php if($info['contract_end_time'] != '00-00-00 00:00:00'): ?>value="<?php echo (substr($info["contract_end_time"],0,10)); ?>"<?php endif; ?>
                                class="Wdate" id="d4322"
                                onFocus="WdatePicker({minDate:'#F<?php echo ($dp["$D('d4321',{d:3"]); ?>);}'})"/>
                            </div>
                            <span class="col-sm-6 tp" style="width:45%"></span>
                        </div>

                        <div class="text-center clearfix pwb pull-right">
                            <input type="button" value="修改" class="btn btn-warning saveOperatorEdit"/>
                        </div>
                    </div>
                </div>

            </div><?php endif; ?>


        <?php if(!in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><div class="rightbox pull-right">
                <div class="ur-here">
                    您当前的位置：
                    <a href="#">修改运营商</a>
                </div>
                <div class="cTitle2">修改运营商</div>
                <div class="action">
                    <a href="javascript:void(0)" class="current" data-click-tab="1">基本信息</a>
                    <a href="javascript:void(0)" data-click-tab="2">扩展信息</a>
                </div>

                <div class="panelCon" id="tab-1">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">运营商名称：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control name"
                                   value="<?php echo ($info["name"]); ?>" disabled>
                        </div>
                    </div>

                    <?php if(is_array($agencyApplyInvoiceList)): foreach($agencyApplyInvoiceList as $key=>$vo): ?><div class="form-group">
                            <label class="col-sm-2 control-label"><?php echo ($vo["detail"]); ?>：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control <?php echo ($vo["detail"]); ?>"
                                       value="<?php echo ($vo['fee_rate'] / 1000); ?>"></div><span style="padding-top:-25px;">（‰）</span>
                        </div><?php endforeach; endif; ?>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">合同费：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control contract_fee"
                                   value="<?php echo (getYuan($info["contract_fee"])); ?>"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">快递费：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control express_fee"
                                   value="<?php echo (getYuan($info["express_fee"])); ?>"></div>
                    </div>


                    <div class="text-center clearfix pwb pull-right">
                        <input type="button" value="修改" class="btn btn-warning saveOperatorEditInfo"/>
                    </div>
                </div>

                <div class="panelCon" style="display:none" id="tab-2">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">手机：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control mobile" value="<?php echo ($info["mobile"]); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">代理商充值信息：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control topup_info" value="<?php echo ($info["topup_info"]); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">支付宝信息：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control alipay_info"
                                       value="<?php echo ($info["alipay_info"]); ?>" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">发票抬头：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control invoice_title"
                                       value="<?php echo ($info["invoice_title"]); ?>" disabled></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">地址：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control address" value="<?php echo ($info["address"]); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">版权信息：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control copyright"
                                       value="<?php echo ($info["copyright"]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">座机：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control tel" value="<?php echo ($info["tel"]); ?>"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">传真：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control fax" value="<?php echo ($info["fax"]); ?>"></div>
                        </div>

                        <div class="text-center clearfix pwb pull-right">
                            <input type="button" value="修改" class="btn btn-warning saveOperatorEditInfo"/>
                        </div>
                    </div>
                </div>

            </div><?php endif; ?>
    </form>
</div>
<?php echo W('Template/bottom');?>
<script type="text/javascript" charset="utf-8" src="/Public/js/operator.js?v=2"></script>
</body>
</html>