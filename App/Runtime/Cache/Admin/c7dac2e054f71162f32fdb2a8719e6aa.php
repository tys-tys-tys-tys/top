<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href=''>订单管理</a>&gt;通用产品订单</div>
        <div class="cTitle2">通用产品订单</div>
        <div class="main-search">
            <div class="top clearfix">
                <form class="form-inline" method="get" value="<?php echo U('OrderProduct/orderProduct', array('id' => 115));?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="产品名称/代理商" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                    <input type="hidden" id="chkState" value="<?php echo U('OrderProduct/orderProduct');?>"/>
                </form>
            </div>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="20%">产品名称</th>
                <th width="5%">数量</th>
                <th width="10%">实际收入</th>
                <th width="10%">代理商</th>
                <th width="10%">应付全款</th>
                <th width="10%">应付预付</th>
                <th width="8%">下单时间</th>
                <th width="10%">备注</th>
                <th width="15%">状态</th>
                <th width="4%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($i); ?></td>
                    <td class="text-c1"><?php echo ($vo["tour_name"]); ?></td>

                    <td><?php echo ($vo["quantity"]); ?></td>
                    <?php if($vo['state'] == 2): ?><td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 3): ?>
                        <td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 5): ?>
                        <td id="money-right">0.00</td>
                        <?php elseif($vo['state'] == 6): ?>
                        <td id="money-right"><?php echo (getYuan($vo["prepay_amount"])); ?></td>
                        <?php elseif($vo['state'] == 7): ?>
                        <td id="money-right"><?php echo (getYuan($vo["price_payable"])); ?></td><?php endif; ?>
                    <td><?php echo ($vo["agency_name"]); ?></td>
                    <td id="money-right" class="text-c5"><?php echo (GetYuan($vo["price_payable"])); ?></td>
                    <td id="money-right" class="text-c1"><?php echo (GetYuan($vo["prepay_amount"])); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                           class="text-c1">
                        <?php if($vo['memo'] == ''): ?>无
                            <?php else: ?>
                            <?php echo (substr($vo["memo"],0,12)); endif; ?>
                    </a></td>
                    <td>
                        <?php if($vo["state"] == 1): ?><span style="color:red">未提交</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <span style="color:blue">已提交</span>
                            <?php elseif($vo["state"] == 3): ?>
                            <span style="color:darkgreen">已确认待付款</span>
                            <?php elseif($vo["state"] == 4): ?>
                            <span style="color:red">已取消</span>
                            <?php elseif($vo["state"] == 5): ?>
                            <span style="color:red">已关闭</span>
                            <?php elseif($vo["state"] == 6): ?>
                            <span style="color:darkgreen">已付预付款</span>
                            <?php elseif($vo["state"] == 7): ?>
                            <span style="color:blue">已付全款</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group pull-right">
                            <button type="button" style="padding:3px 7px" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="margin: 0px;background: #eee">
                                <?php if(($vo['state'] == 2) or $vo['state'] == 3): ?><li><a onclick="orderChange(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateWaitForPayment;?>)"
                                           style="cursor: pointer;padding:0px 3px;">确认订单</a></li>
                                    <li><a onclick="changeState(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStateClosed;?>)"
                                           style="cursor: pointer;padding:0px 3px;">关闭订单</a></li><?php endif; ?>

                                <?php if($vo['state'] == 6): ?><li><a onclick="orderChange(<?php echo ($vo["id"]); ?>, <?php echo Common\Top::OrderStatePrepayed;?>)"
                                           style="cursor: pointer;padding:0px 3px;">确认订单</a></li><?php endif; ?>


                            </ul>
                        </div>
                    </td>
                    <td id="showMSG<?php echo ($vo["id"]); ?>" style="display:none">
                        <div class="theme-popover">
                            <div class="theme-poptit">
                                <a title="关闭" class="closes">&times;</a>

                                <div class="modal-header">
                                    <h4 class="modal-title" style="text-align: left" id="myModalLabel"></h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <div class="form-group" id="name">
                                            <label class="col-sm-2 control-label">订单金额：</label>

                                            <div class="col-sm-4" style="text-align: left">
                                                <a style="color:red"></a><a id="total<?php echo ($vo["id"]); ?>" style="color:red"><?php echo (GetYuan($vo["price_total"])); ?></a>
                                            </div>
                                        </div>
                                        <div class="form-group" id="editp<?php echo ($vo["id"]); ?>">
                                            <label class="col-sm-2 control-label">价格调整：</label>

                                            <div class="col-sm-4">
                                                <?php if($vo['price_adjust'] == 0): ?><input type="text" onblur="editprice(<?php echo ($vo["id"]); ?>)" class="form-control"
                                                           id="adjust<?php echo ($vo["id"]); ?>" name="adjust" placeholder="0"
                                                           value="0"/>
                                                    <?php else: ?>
                                                <input type="text" onblur="editprice(<?php echo ($vo["id"]); ?>)" class="form-control"
                                                       id="adjust<?php echo ($vo["id"]); ?>" name="adjust" placeholder="0"
                                                       value="<?php echo (getYuan($vo["price_adjust"])); ?>"/><?php endif; ?>
                                            </div>
                                            <span style="text-align: left" class="col-sm-6 tp" id='edirPrice'>（例如：减：-10，加：10）</span>
                                        </div>
                                        <div class="form-group" id="edit<?php echo ($vo["id"]); ?>">
                                            <label class="col-sm-2 control-label">调后价格：</label>

                                            <div class="col-sm-4" style="text-align: left">
                                                <a id="edit_price<?php echo ($vo["id"]); ?>"
                                                   style="color:red"><?php echo (GetYuan($vo["price_payable"])); ?></a>
                                            </div>
                                            <span style="text-align: left" class="col-sm-6 tp"></span>
                                        </div>

                                        <div class="form-group" id="prepay<?php echo ($vo["id"]); ?>">
                                            <label class="col-sm-2 control-label">预付金额：</label>

                                            <div class="col-sm-4">
                                                <?php if($vo['state'] != 6): ?><input type="text" id="amount<?php echo ($vo["id"]); ?>" name="amount"
                                                           class="form-control" placeholder="0"
                                                           value="0"/>
                                                    <?php else: ?>
                                                    <input type="text" id="amount<?php echo ($vo["id"]); ?>" name="amount"
                                                           class="form-control" placeholder="0"
                                                           value="<?php echo (getYuan($vo["prepay_amount"])); ?>" disabled/><?php endif; ?>
                                            </div>
                                            <span style="text-align: left" class="col-sm-6 tp"></span>
                                        </div>

                                        <div class="form-group" id="memo<?php echo ($vo["id"]); ?>">
                                            <label class="col-sm-2 control-label">备注：</label>

                                            <div class="col-sm-4" style="text-align: left;">
                                                <textarea name="memos"
                                                          style="width:300px;height:50px;resize:none;padding:6px 12px"><?php echo ($vo["memo"]); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer pull-right">
                                    <button type="button" class="btn btn-warning" id="button<?php echo ($vo["id"]); ?>">确定</button>
                                    <button type="button" class="btn btn-default " id="reset">取消</button>
                                </div>
                            </div>
                        </div>
                        <div class="theme-popover-mask"></div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>