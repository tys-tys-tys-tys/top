<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href=''>财务管理</a>&gt;提现记录</div>
        <div class="cTitle2">提现记录</div>
        <?php if(empty($info)): ?><table class="cTable" width="100%">
                <tr>
                    <th><?php echo ($msg); ?></th>
                </tr>
            </table><?php endif; ?>
        <?php if(!empty($info)): ?><div class="main-search">
                <div class="top">
                    <form method="get" action="<?php echo U('ProviderFinance/withdrawInfo',array('id'=>I('get.id')));?>" class="form-inline">
                        <div class="form-group">
                            <label>时间：</label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                                   value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                                   style="width:80%">
                        </div>
                        <span class="end">到</span>

                        <div class="form-group">
                            <label class="sr-only"></label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                                   value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                        </div>
                        <button type="submit" class="btn btn-warning">查询</button>
                    </form>
                </div>
            </div>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="6%">编号</th>
                    <th>申请时间</th>
                    <th>提现金额</th>
                    <th>提现帐户</th>
                    <th>审核时间</th>
                    <th>状态</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                        <td id="money-right" class="text-c1"><?php echo (GetYuan($vo["amount"])); ?></td>
                        <td><?php echo ($vo["bank_name"]); ?><br/>
                            账号: <?php echo ($vo["bank_account"]); ?>
                        </td>
                        <td><?php echo ($vo["update_time"]); ?></td>
                        <td class="text-c5">
                            <?php if($vo["state"] == 1): ?><span style="color:saddlebrown">待审核</span>
                                <?php elseif($vo["state"] == 2): ?>
                                <span style="color:blue">已确认，待财务处理</span>
                                <?php elseif($vo["state"] == 4): ?>
                                <span style="color:green">处理完成</span>
                                <?php else: ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>" style="text-decoration: none;color: red">拒绝</a><?php endif; ?>
                        </td>

                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table><?php endif; ?>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel" style="text-align: left"></h4>
        </div>
        <div class="modal-body" id="modalB">
            <textarea class="form-control" name="reason" style="resize: none;"></textarea>
        </div>
        <div class="modal-footer pull-right">
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>