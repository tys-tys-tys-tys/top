<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">代理商管理</a>&gt;意见反馈-顾问</div>
        <div class="cTitle2">供应商列表</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Agency/adviser_feedback', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Agency/adviser_feedback', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Agency/adviser_feedback',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入顾问简称/全称" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>

            <form class="pull-right" style="padding-bottom: 5px;">
                <input id="chkr" type="hidden" value="<?php echo U('Agency/deleteAdviserFeedback');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
            </form>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getAdviserFeedbackExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="3%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold"></label></th>
                <th width="5%">编号</th>
                <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><th width="5%">运营商</th><?php endif; ?>
                <th width="5%">客户</th>
                <th width="5%">顾问</th>
                <th width="10%">评星</th>
                <th width="15%">意见反馈内容</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/>
                    </td>
                    <td><?php echo ($v["id"]); ?></td>
                    <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><td><?php echo (getOperatorName($v["operator_id"])); ?></td><?php endif; ?>
                    <td><?php echo ($v["agency_name"]); ?></td>
                    <td><?php echo ($v["cover_commnet_name"]); ?></td>
                    <td><?php echo ($v["star"]); ?></td>
                    <td><?php echo ($v["comm_content"]); ?></td>
                 
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="/Public/js/providerInfo.js?v=7"></script>
</body>
</html>