<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">运营商账户余额管理</a>&gt;运营商账户充值申请</div>
        <div class="cTitle2">运营商账户充值申请</div>
        <div class="main-search">
        </div>

        <form method="post" action="">
            <input type='hidden' id='code' value='<?php echo (session('creditCode')); ?>'>
            <input id="account_type" type="hidden" value="10"/>
            <input id="applyList" type="hidden" value="<?php echo U('Credit/index',array('id' => 165));?>"/>
            <input id="saveApplyCredit" type="hidden" value="<?php echo U('Credit/applyCredit');?>"/>
            <input id="account" type="hidden" value="<?php echo ($info["account"]); ?>">
            <input id="state" type="hidden" value="<?php echo ($info["state"]); ?>">
            <?php if(!empty($info)): ?><table class="cTable table-hover" width="100%">
                <tr>
                    <th width="25%">名称</th>
                    <th width="25%">账户可用余额</th>
                    <th width="25%">状态</th>
                    <th width="25%">操作</th>
                </tr>
                <td>
                    <?php echo ($info["name"]); ?>
                </td>
                <td class="text-center text-c5">
                    <dfn>&yen;<?php echo (GetYuan($info["account_credit_balance"])); ?></dfn>
                </td>
                <td>
                    <?php if($info["state"] == 1): ?><span class="text-c7">正常</span>
                        <?php elseif($info["state"] == 2): ?>
                        <span class="text-c8">禁用</span><?php endif; ?>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-expanded="false">
                            操作<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="javascript:void(0)" onclick="applyCredit()">充值</a></li>
                        </ul>
                    </div>
                </td>
                </tr>
            </table><?php endif; ?>
        </form>
    </div>
</div>

<div class="modal fade" id="myModalLabel" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    账户充值申请
                </h4>
            </div>
            <div class="modal-body">
                金额：
                <input type="text" class="form-control" id="amount">
                类型：
                <select class="form-control topup_type" id="topUpType">
                    <option value="">--选择充值类型--</option>
                    <option value="1">现金充值</option>
                    <option value="2">刷卡充值</option>
                    <option value="3">支票充值</option>
                    <option value="4">银行转账</option>
                </select>

                <div class="remitterName" style="display:none">
                    汇款方
                    <input type="text" class="form-control payer" placeholder="汇款方名称"/>
                </div>

                <div class="bankInfo" style="display:none;">
                    <p> </p>
                    <pre class="form-control" style="height:100%"><?php echo ($sysInfo["topup_info"]); ?></pre>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveApply">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script src="/Public/js/credit.js?v=3"></script>
</body>
</html>