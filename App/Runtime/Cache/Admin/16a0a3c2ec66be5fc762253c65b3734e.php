<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">统计报表</a>&gt;供应商提现报表</div>
        <div class="cTitle2">供应商提现报表</div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-report-state nav-operator-report-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Report/providerWithdrawReport', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search">
            <div class="top">
                <form method="get" action="<?php echo U('Report/providerWithdrawReport',array('id' => I('get.id'), 'oid' => I('get.oid')));?>"
                      class="form-inline">
                    <div class="form-group">
                        <input type="text" name="entity_name" class="form-control" placeholder="供应商简称" value="<?php echo ($providerName); ?>">
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                               value="<?php echo ($startTime); ?>" id="d4322"
                               onclick="WdatePicker({el: $dp.$('d12')})" style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                               value="<?php echo ($endTime); ?>" id="d4322"
                               onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="10%">编号</th>
                <th width="15%">供应商</th>
                <th width="13%">提现金额</th>
                <!--<th width="12%">账户总额</th>-->
                <th width="20%">提现帐户</th>
                <th width="10%">申请时间</th>
                <th width="10%">审核时间</th>
                <th width="10%">状态</th>

            </tr>
            <?php if($list[0]['id'] != ''): if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($i); ?></td>
                        <td class="tl"><?php echo ($v["entity_name"]); ?></td>
                        <td class="text-c1"> &yen;<?php echo (GetYuan($v["amount"])); ?></td>
                        <!--<td class="text-c5"><?php echo ($vo["balance"]); ?></td>-->
                        <td><?php echo ($v["bank_name"]); ?><br/>账号 <?php echo ($v["bank_account"]); ?></td>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <td><?php echo ($v["update_time"]); ?></td>
                        <td class="text-c3">
                            <?php if($v["state"] == 2): ?><span class="text-c6">已确认，待财务处理</span>
                                <?php elseif($v["state"] == 4): ?>
                                <span class="text-c7">处理完成</span><?php endif; ?>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                <tr>
                    <td colspan="7" class="text-c1" id="money-right">
                        <h5>提现总额: <span class="text-c5">&yen;<?php echo (GetYuan($withdrawTotal)); ?></span></h5>
                    </td>
                </tr>
                <?php else: ?>
                <tr>
                    <td colspan='7' class='text-c5'>没有数据</td>
                </tr><?php endif; ?>
        </table>
        <?php if(!empty($list)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
</body>
</html>