<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">代理商管理</a>&gt;充值申请</div>
        <div class="cTitle2">充值申请</div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Agency/topupList', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Agency/topupList', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Agency/topupList',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入代理商姓名" value="<?php echo ($name); ?>"/>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                               placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                               placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getAgencyTopUpExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
                <input id="invoiceList" type="hidden" value="<?php echo U('Agency/updateTopUpProof');?>"/>
            </form>
        </div>

        <input id="chkState" type="hidden" value="<?php echo U('Agency/auditTopUp');?>"/>
        <input id="showReject" type="hidden" value="<?php echo U('Agency/showTopupReject');?>"/>
        <input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
        <input type="hidden" id="code" value="<?php echo (session('topUpCode')); ?>">
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%">编号</th>
                <th width="8%">运营商</th>
                <th width="10%">代理商</th>
                <th width="10%">扣除服务费名</th>
                <th width="12%">扣除服务费</th>
                <th width="10%">扣除时间</th>
                
                <th width="10%">状态</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td><?php echo (getOperatorName($vo["operator_id"])); ?></td>
                    <td><?php echo ($vo["entity_name"]); ?></td>
                     <td class="text-c1">
                        <?php echo ($vo["service_name"]); ?>
                    </td>
                    <td id="money-right" class="text-c5"><?php echo (GetYuan($vo["service_money"])); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td class="text-c5">
                        <?php if($vo["state"] == 0): ?><span style="color:saddlebrown">待确认</span>
                            <?php elseif($vo["state"] == 1): ?>
                            <span class="text-c7">已确认</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group" id="state">
                            <?php if($vo['state'] == 1): ?><button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="javascript:void(0)"
                                           onclick="changeTopupPassState(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo (subStrTime($vo["create_time"])); ?>, <?php echo Common\Top::ApplyStatePass;?>, <?php echo (getAgencyState($vo["entity_id"])); ?>, <?php echo (getAgencyAccountState($vo["entity_id"])); ?>)">已审核</a>
                                    </li>
                                    <li><a href="javascript:void(0)"
                                           onclick="rejectState(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo Common\Top::ApplyStateReject;?>)">拒绝</a>
                                    </li>
                                </ul>
                                <?php elseif($vo['state'] == 3): ?>
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="javascript:void(0)"
                                           onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 1)">更新凭证</a>
                                    </li>
                                </ul>
                                <?php else: ?>
                                <div class="btn-group" id="state">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="javascript:void(0)"
                                               onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 1)">更新凭证</a>
                                        </li>
                                    </ul>
                                </div><?php endif; ?>
                        </div>
                    </td>
                </tr><?php endforeach; endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">拒绝理由</h4>
        </div>
        <div class="modal-body" id="modal_reject">
            <textarea class="form-control" name="reason" style="resize: none;" placeholder='请填写拒绝理由'></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    填写凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>