<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">运营商账户余额管理</a>&gt;运营商账户变更记录</div>
        <div class="cTitle2">
            运营商账户变更记录
        </div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Credit/details', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Credit/details', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <table class="cTable table-hover" width="100%">
            <tr>
                <th width='8%'>编号</th>
                <th width='10%'>运营商</th>
                <th width='10%'>明细</th>
                <th width='20%'>用途</th>
                <th width='15%'>金额</th>
                <th width='15%'>余额</th>
                <th width='10%'>备注</th>
                <th width='15%'>操作时间</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($v["id"]); ?></td>
                    <td><?php echo (getOperatorName($v["operator_id"])); ?></td>
                    <td><?php echo ($v["objectName"]["tour_name"]); ?></td>
                    <td><?php echo ($v["action"]); ?></td>
                    <td id="money-right" class='text-c5'><dfn>&yen;<?php echo (GetYuan($v["amount"])); ?></dfn></td>
                    <td class="text-c1">
                        <dfn>&yen;<?php echo (GetYuan($v["balance"])); ?></dfn>
                    </td>
                    <td class="text-c5">
                        <?php if($vo['memo'] != ''): echo ($vo["memo"]); ?>
                            <?php else: ?>
                            无<?php endif; ?>
                    </td>
                    <td><?php echo ($v["create_time"]); ?></td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script src="/Public/js/credit.js"></script>
</body>
</html>