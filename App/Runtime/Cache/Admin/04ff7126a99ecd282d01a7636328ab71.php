<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">统计报表</a>&gt;订单报表</div>
        <div class="cTitle2">订单报表</div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-report-state nav-operator-report-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Report/orderReport', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search">
            <div class="top">
                <form method="get" action="<?php echo U('Report/orderReport',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" name="entity_name" class="form-control" placeholder="代理商/供应商简称" value="<?php echo ($providerName); ?>">
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                              value="<?php echo ($startTime); ?>" id="d4322"
                               onclick="WdatePicker({el: $dp.$('d12')})" style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                               value="<?php echo ($endTime); ?>" id="d4322"
                               onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="10%">代理商</th>
                <th width="15%">线路名称</th>
                <th width="10%">供应商名称</th>
                <th width="8%">应付全款</th>
                <th width="8%">应付预付</th>
                <th width="8%">实际收入</th>
                <th width="5%">成人</th>
                <th width="5%">儿童</th>
                <th width="8%">团期</th>
                <th width="8%">创建时间</th>
                <th>状态</th>
            </tr>
            <?php if($info[0]['id'] != ''): if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td><?php echo ($vo["agency_name"]); ?></td>
                        <td><?php echo ($vo["tour_name"]); ?></td>
                        <td><?php echo ($vo["provider_name"]); ?></td>
                        <td id="money-right"
                            class="text-c5"><?php echo (($vo['price_adjust'] + $vo['price_total'])/100).".00"; ?></td>
                        <td id="money-right" class="text-c1"><?php echo (GetYuan($vo["prepay_amount"])); ?></td>
                        <?php if($vo['state'] == 6): ?><td id="money-right"><?php echo (GetYuan($vo["prepay_amount_commission"])); ?></td>
                            <?php elseif($vo['state'] == 7): ?>
                            <td id="money-right"><?php echo (GetYuan($vo["price_payable_commission"])); ?></td><?php endif; ?>
                        <td class="text-c1"><?php echo ($vo["client_adult_count"]); ?>人</td>
                        <td class="text-c1"><?php echo ($vo["client_child_count"]); ?>人</td>
                        <td class="text-c1"><?php echo (substr($vo["start_time"],0,10)); ?></td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                        <td>
                            <?php if($vo["state"] == 6): ?><span class="text-c6">已付预付款</span>
                                <?php elseif($vo["state"] == 7): ?>
                                <span class="text-c7">已付全款</span><?php endif; ?>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                <tr>
                    <td colspan="12" class="text-c1" id="money-right">
                        <h5>实际收入总额: <span class="text-c5">&yen;<?php echo (GetYuan($realTotal)); ?></span></h5>
                        <h5>应付全款总额: <span class="text-c5">&yen;<?php echo (GetYuan($priceTotal)); ?></span></h5>
                    </td>
                </tr>
                <?php else: ?>
                <tr>
                    <td colspan='12' class='text-c5'>没有数据</td>
                </tr><?php endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
</body>
</html>