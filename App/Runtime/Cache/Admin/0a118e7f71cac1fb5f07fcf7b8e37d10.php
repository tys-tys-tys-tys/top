<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">订单管理</a>&gt;机票产品订单</div>
        <div class="cTitle2">机票产品订单</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Order/planeTicketList', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Order/planeTicketList', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Order/planeTicketList',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="编号/订单编号/代理/乘机人" value="<?php echo ($name); ?>" style="width:200px"/>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getFlightTicketExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="5%">订单编号</th>
                <th width="10%">代理商</th>
                <th width="25%">乘机人</th>
                <th width="10%">应付金额</th>
                <th width="10%">下单时间</th>
                <th width="15%">凭证</th>
                <th width="10%">订单状态</th>
                <th width="10%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <?php if($vo['sequence_no'] != ''): ?><td><?php echo ($vo["sequence_no"]); ?></td>
                        <?php else: ?>
                        <td>无</td><?php endif; ?>
                    <td><?php echo ($vo["agency_name"]); ?></td>
                    <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                           title="<?php echo ($vo["passenger_names"]); ?>" class="text-c1"
                           style="word-break:break-all;word-wrap:break-word;"><?php echo (substr($vo["passenger_names"],0,50)); ?></a></td>
                    <td class="text-c5"><?php echo (getYuan($vo["price_total"])); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td class="text-c1"><?php echo ($vo["proof_type"]); ?> <?php echo ($vo["proof_code"]); ?></td>
                    <td>
                        <?php if($vo["state"] == 1): ?><span style="color:#ddd">未提交</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <span style="color:blue">已付款</span>
                            <?php elseif($vo["state"] == 3): ?>
                            <span style="color:darkgreen">已出票</span>
                            <?php elseif($vo["state"] == 4): ?>
                            <span style="color:red">有取消</span>
                            <?php elseif($vo["state"] == 5): ?>
                            <span style="color:red">有退票</span>
                            <?php elseif($vo["state"] == 6): ?>
                            <span style="color:red">出票失败</span>
                            <?php elseif($vo["state"] == 7): ?>
                            <span style="color:blue">预定失败</span>
                            <?php elseif($vo["state"] == 8): ?>
                            <span style="color:blue">退票失败</span>
                            <?php elseif($vo["state"] == 9): ?>
                            <span style="color:red">有退票</span>
                            <?php elseif($vo["state"] == 10): ?>
                            <span style="color:red">有取消</span>
                            <?php elseif($vo["state"] == 11): ?>
                            <span style="color:red">有退票</span>
                            <?php elseif($vo["state"] == 12): ?>
                            <span style="color:red">已删除</span>
                            <?php elseif(($vo["state"] == 14) or ($vo["state"] == 15)): ?>
                            <span style="color:red">已预订,待付款</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a style="cursor: pointer"
                                       href="<?php echo U('Order/planeTicketDetail',array('id' => 123,'sid' => 124,'pid' => $vo['id']));?>">查看</a>
                                </li>
                                <li><a style="cursor: pointer"
                                       onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 7)">凭证</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="chkState" value="<?php echo U('Order/planeTicketList');?>"/>
<input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
<input type="hidden" id="type" value="flight">
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>