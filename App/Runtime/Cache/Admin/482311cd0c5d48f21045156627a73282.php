<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <form class="form-inline" method="get"
          action="<?php echo U('Provider/auditGeneralProduct',array('state'=>0,'id'=>I('get.id')));?>">
        <div class="rightbox pull-right">
            <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt;审核通用产品</div>
            <div class="cTitle2">审核通用产品</div>
            <div class="main-search">
                <div class="top" style="margin-bottom:0px;">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="请输入产品名称" name='name' value="<?php echo ($name); ?>">
                    </div>
                    <input type="submit" class="btn btn-warning" value='查询'>
                </div>
            </div>
            <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

                <div class="status">
                    <label>运营商：</label>
                    <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-report-state nav-operator-report-state<?php echo ($vo["id"]); ?>"
                           href="<?php echo U('Provider/auditGeneralProduct', array('id' => I('get.id'), 'oid' => $vo['id'], 'state' => I('get.state')));?>"><?php echo ($vo["display_name"]); ?>
                        </a><?php endforeach; endif; ?>
                </div><?php endif; ?>
            <div class="status">
                <input type="hidden" id="changeAudit" value="<?php echo U('Provider/auditGeneralProduct');?>">
                <label>状态：</label>
                <a class="navi-audit-state navi-audit-state0"
                   href="<?php echo U('Provider/auditGeneralProduct',array('state'=>0,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">全部
                    <?php if($state == 0): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state3"
                   href="<?php echo U('Provider/auditGeneralProduct',array('state'=>3,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">待审核
                    <?php if($state == 3): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state2"
                   href="<?php echo U('Provider/auditGeneralProduct',array('state'=>2,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">审核通过
                    <?php if($state == 2): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state4"
                   href="<?php echo U('Provider/auditGeneralProduct',array('state'=>4,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">拒绝
                    <?php if($state == 4): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state5"
                   href="<?php echo U('Provider/auditGeneralProduct',array('state'=>5,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">已下架
                    <?php if($state == 5): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
            </div>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="6%">代码</th>
                    <th width='15%'>产品名程</th>
                    <th width='10%'>供应商</th>
                    <th width='10%'>佣金比例</th>
                    <th width='10%'>同行价</th>
                    <th width='10%'>参数</th>
                    <th width='12%'>状态</th>
                    <th width='15%'>创建时间</th>
                    <th width="14%">操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td>20<?php echo ($v["id"]); ?></td>
                        <td width="22%" class="text-c1" style="padding:2px 0 0 2px"><?php echo ($v["name"]); ?></td>
                        <td><?php echo ($v["user_name"]); ?></td>
                        <td><?php echo ($v['commission_rate'] / 1000); ?>‰</td>
                        <td class='text-c5'><dfn>&yen;<?php echo (getYuan($v["price_agency"])); ?></dfn></td>
                        <td class='text-c1'>
                            <?php if($v['para_type'] == 1): ?>保险 <?php echo ($v["para_value"]); endif; ?>
                        </td>
                        
                        <td class="show_msg">
                            <?php if($v["state"] == 1): ?><span style="color:#000666">未提交</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:blue">已审核，已上架</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:darkgreen">已提交，待审核</span>
                                <?php elseif($v["state"] == 4): ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                   title="<?php echo ($v["audit_memo"]); ?>" style="text-decoration: none"><span
                                        style="color:red">拒绝</a></span>
                                <?php elseif($v["state"] == 5): ?>
                                <span style="color:red">已下架</span><?php endif; ?>
                        </td>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <td>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateIsSale;?>)"
                                           style="cursor: pointer">已审核</a></li>
                                    <li><a onclick="reject(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateRejected;?>)"
                                           style="cursor: pointer">拒绝</a></li>
                                    <li><a onclick="setCommission(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">佣金设置</a></li>
                                    <li><a href="<?php echo U('Product/edit',array('id'=>I('get.id'), 'tid'=>$v['id']));?>">编辑</a></li>
                                </ul>
                            </div>

                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
            <?php if(!empty($info)): ?><div class="pagebox">
                    <?php echo ($page); ?>
                </div><?php endif; ?>
        </div>
    </form>
</div>

<div class="theme-popover">
    <div class="theme-poptit" style="border-bottom: 1px solid #e5e5e5;">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">拒绝理由</h4>
        </div>
        <div class="modal-body" id="modal_reject">
            <textarea class="form-control" name="reason" style="resize: none;" placeholder="请填写拒绝理由"></textarea>
        </div>
        <div class="modal-footer pull-right">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">
                    佣金设置
                </h4>
            </div>
            <div class="modal-body">
                佣金比例：
                <div class="input-group">
                    <input type="text" class="commission_rate form-control" style="border:1px solid #e5e5e5">
                    <span class="input-group-addon">‰</span>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="<?php echo U('Provider/auditGeneralProduct');?>" id="chkState"/>
<input type="hidden" value="<?php echo U('Provider/saveGeneralProductCommission');?>" id="saveCommission"/>
<input type="hidden" value="<?php echo U('Provider/showGeneralProductCommission');?>" id="showCommission"/>

<div class="theme-popover-mask"></div>
<input id='state' type='hidden' value='<?php echo ($state); ?>'/>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>