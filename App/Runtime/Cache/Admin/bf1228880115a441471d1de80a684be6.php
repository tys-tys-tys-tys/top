<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>

    <form class="form-horizontal zcform" id="signupForm" method="post" action="<?php echo U('Agency/edit',array('id'=> $list['id']));?>" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="<?php echo ($list["id"]); ?>">
        <input type="hidden" id="keyid" value="<?php echo (setEncrypt($list["id"])); ?>">
        <input id="editName" type="hidden" value="<?php echo U('Agency/edit');?>"/>

        <div class="rightbox pull-right">
            <div class="ur-here">
                您当前的位置：
                <a href="#">修改代理商</a>
            </div>
            <div class="cTitle2">修改代理商</div>
            <div class="action">
                <a href="javascript:void(0)" class="current" data-click-tab="1">基本信息</a>
                <a href="javascript:void(0)" data-click-tab="2">扩展信息</a>
            </div>

            <div class="panelCon" id="tab-1">

                <div class="form-group">
                    <label class="col-sm-2 control-label">真实姓名：</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="请输入代理商姓名" id="name"
                               value="<?php echo ($list["name"]); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">手机：</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="telphone"
                               placeholder="请输入手机号" value="<?php echo ($list["mobile"]); ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">证件类型：</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="idcard_type" id="idcard_type" disabled>
                            <option value="1">身份证</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">证件号码：</label>
                    <div class="col-sm-4">
                        <input id="cardnumber" class="form-control" type="text" placeholder="请输入证件号" value="<?php echo ($list["idcard"]); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">开户行：</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="bank_name"
                               placeholder="请输入开户行名称" value="<?php echo ($list["bank_name"]); ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">开户名：</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="name2" placeholder="请输入开户人名称" value="<?php echo ($user_list["name"]); ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">卡号：</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="请输入银行卡号"
                               id="bank_account" value="<?php echo ($list["bank_account"]); ?>"></div>
                </div>
                <div class="text-center clearfix pwb pull-right">
                    <input type="button" value="修改" class="btn btn-warning saveEditAgencyInfo"/>
                </div>
            </div>

            <div class="panelCon" style="display:none" id="tab-2">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">绑定域名：</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="请输入要绑定的域名" id="domain"
                                   value="<?php echo ($list["domain"]); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">邮 箱：</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="请输入邮箱"
                                   value="<?php echo ($list["email"]); ?>" id="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Q　Q：</label>
                        <div class="col-sm-4">
                            <input type="text" value="<?php echo ($list["qq"]); ?>" class="form-control" placeholder="请输入QQ号"
                                   id="qq"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">签约人：</label>
                        <div class="col-sm-4">
                            <input type="text" value="<?php echo ($list["client_manager"]); ?>" class="form-control"
                                   placeholder="请输入签约人" id="client_manager"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">职业：</label>
                        <div class="col-sm-4">
                            <input type="text" value="<?php echo ($list["vocation"]); ?>" class="form-control"
                                   placeholder="如：领队、导游、职员等" id="vocation"></div>
                    </div>
                    <div class="form-group" id="modal_times">
                        <label class="col-sm-2 control-label">合约起止时间：</label>
                        <div class="col-sm-4" style="width:37.333%;">
                            <input type="text" id="timestart" type="text"
                                   style="width:42%;border:1px solid #ccc;height:35px;"
                                   <?php if($list['contract_start_time'] != '0000-00-00 00:00:00'): ?>value="<?php echo (substr($list["contract_start_time"],0,10)); ?>"<?php endif; ?>
                                    class="Wdate" id="d4322"
                                   onclick="WdatePicker({el:$dp.$('d12')})"/>
                            到
                            <input type="text" id="timeend" type="text"
                                   style="width:42%;border:1px solid #ccc;height:35px;"
                                   <?php if($list['contract_end_time'] != '0000-00-00 00:00:00'): ?>value="<?php echo (substr($list["contract_end_time"],0,10)); ?>"<?php endif; ?>
                                    class="Wdate" id="d4322"
                                   onFocus="WdatePicker({minDate:'#F<?php echo ($dp["$D('d4321',{d:3"]); ?>);}'})"/>
                        </div>
                        <span class="col-sm-6 tp" style="width:45%"></span>
                    </div>
                    <div class="text-center clearfix pwb pull-right">
                        <input type="button" value="修改" class="btn btn-warning saveEditAgencyInfo"/>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>