<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">线路产品</a>&gt;已上架线路</div>
        <div class="cTitle2">已上架线路</div>
        <div class="main-search">
            <div class="top clearfix">
                <form class="form-inline" method="get" action="<?php echo U('Tour/putaway', array('id' => 50));?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="代码/线路名称/目的地" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <form class="pull-right" style="padding-bottom: 5px;">
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
                <input type="hidden" id="chkr" value="<?php echo U('Tour/delete');?>"/>
                <input id="chkState" type="hidden" value="<?php echo U('Tour/index');?>"/>
            </form>
        </div>
        <table id='subBoxOne' class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" onclick="selectAll(this);"/>&nbsp;<label style="font-weight: bold">全选</label></th>
                <th width="8%">代码</th>
                <th width="10%">预览线路</th>
                <th width="15%">线路名称</th>
                <th width='10%'>供应商</th>
                <th width='10%'>出发地</th>
                <th width='8%'>目的地</th>
                <th width='12%'>状态</th>
                <th width='10%'>最新团期</th>
                <th width='10%'>创建时间</th>
                <th width="8%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>
                    <td>10<?php echo ($v["id"]); ?></td>
                    <td>
                        <a class="showqrc" value="<?php echo ($v["id"]); ?>" style="cursor: pointer">手机预览</a>
                    </td>
                    <td style="text-align: left">
                        <?php if($v['promotion'] == 1): ?><span class="badge" style="background: red">特</span>&nbsp;<?php endif; ?>
                        <a style="color:blue;text-decoration: none" href="<?php echo U('Tour/details',array('id'=>$v['id']));?>"
                           target="_balank"><?php echo ($v["name"]); ?></a></td>
                    <td><?php echo ($v["provider_name"]); ?></td>
                    <td><?php echo ($v["departure"]); ?></td>
                    <td><?php echo ($v["destination"]); ?></td>
                    <td class="show_msg">
                        <?php if($v["state"] == 1): ?><a style="color:#000666">未提交</a>
                            <?php elseif($v["state"] == 2): ?>
                            <a style="color:blue">已审核，已上架</a>
                            <?php elseif($v["state"] == 3): ?>
                            <a style="color:darkgreen">已提交，待审核</a>
                            <?php elseif($v["state"] == 4): ?>
                            <a style="color:red">审核失败</a> &nbsp;|&nbsp;<a id="infos"
                                                                          style="color:blue;cursor: pointer;">查看原因</a>
                            <?php elseif($v["state"] == 5): ?>
                            <a style="color:red">已下架</a><?php endif; ?>
                    </td>
                    <td>
                        <?php if($v['latest_sku'] <= $nowDate): ?><span style='color:red'><?php echo (substr($v["latest_sku"],0,10)); ?></span>
                            <?php else: ?>
                            <span class="text-c1"><?php echo (substr($v["latest_sku"],0,10)); ?></span><?php endif; ?>
                    </td>
                    <td><?php echo ($v["create_time"]); ?></td>
                    <td>
                        <div class="btn-group pull-right">
                            <button type="button" style="padding:3px 7px" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="margin: 0px;background: #eee">
                                <?php if(($_SESSION['user_id']== $v['user_id']) or ($_SESSION['is_admin']== 1)): ?><li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateCanceled;?>)"
                                           style="cursor: pointer;padding:0px 14px;">下架</a></li>
                                    <?php else: ?>
                                    <li><a onclick="friendRemind(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer;padding:0px 14px;">下架</a></li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width:300px;height:330px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">微信扫一扫,在手机上预览</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox" style="width: 300px;margin-left: -120px;margin-top: -30px;"><i class="suc"></i></p>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
</body>
</html>