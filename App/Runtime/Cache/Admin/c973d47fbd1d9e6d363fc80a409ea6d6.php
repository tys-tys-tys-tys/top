<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<style>
    .div2-1,.div3-1,.div4,.div4-1{
        display:block;
    }
</style>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">用户/角色</a>&gt;<a href="<?php echo U('Role/index',array('id' => 10));?>">角色列表</a>&gt;权限分配
        </div>
        <div class="cTitle2">分配 <span class="text-c5"><?php echo ($roleinfo["info"]["name"]); ?></span> 权限</div>
        <div style="width:100%;">
            <?php if(is_array($roleinfo['top'])): $i = 0; $__LIST__ = $roleinfo['top'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><dl class="div1">
                    <dt class="mainName authHover">
                        <div style="border: 1px solid #e3e3e3;line-height: 45px;">
                            <div style="width:35%;border-right:1px solid #e3e3e3">
                                <input class="checkMain" id="top<?php echo ($v["id"]); ?>" style="margin-left:5px;" name="subBox" type="checkbox" value="<?php echo ($v["id"]); ?>"
                                <?php if(in_array(($v["id"]), is_array($roleinfo['auth_id'])?$roleinfo['auth_id']:explode(',',$roleinfo['auth_id']))): ?>checked<?php endif; ?>
                                />
                                <span class="name"><?php echo ($v["name"]); ?></span>
                            </div>
                        <div style="width:65%;float:right;padding-left:10px;margin-top:-45px;"><?php echo ($v["remark"]); ?></div>
                        </div>
                    </dt>

                    <dd>
                        <?php if(is_array($roleinfo['stair'])): foreach($roleinfo['stair'] as $key=>$vv): if(($vv["parent_id"]) == $v["id"]): ?><div class="div2">
                                    <div style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;border-left:1px solid #e3e3e3;line-height: 45px;" class="authHover">
                                        <div style="width:35%;border-right:1px solid #e3e3e3">
                                            <input class="sub parentRole" type="checkbox" style="margin-left: 60px;" name="subBox" value="<?php echo ($vv["id"]); ?>"
                                            <?php if(in_array(($vv["id"]), is_array($roleinfo['auth_id'])?$roleinfo['auth_id']:explode(',',$roleinfo['auth_id']))): ?>checked<?php endif; ?>
                                            />
                                            <span class="name2"><?php echo ($vv["name"]); ?></span>
                                        </div>
                                        <div style="width:65%;float:right;padding-left:10px;margin-top:-45px;"><?php echo ($vv["remark"]); ?></div>
                                    </div>

                                    <?php if(is_array($roleinfo['second'])): foreach($roleinfo['second'] as $key=>$vvv): if(($vvv["parent_id"]) == $vv["id"]): ?><div class="div3">
                                                <div style="width:100%;border-bottom: 1px solid #e3e3e3;line-height: 45px;margin-bottom: -18px;" class="authHover">
                                                    <div style="width:35%;border-right:1px solid #e3e3e3">
                                                        <input class="sub check3" type="checkbox" style="margin-left:120px;" name="subBox" value="<?php echo ($vvv["id"]); ?>"
                                                        <?php if(in_array(($vvv["id"]), is_array($roleinfo['auth_id'])?$roleinfo['auth_id']:explode(',',$roleinfo['auth_id']))): ?>checked<?php endif; ?>
                                                        />
                                                        <span class="name3"><?php echo ($vvv["name"]); ?></span>
                                                    </div>
                                                    <div style="width:65%;float:right;padding-left:10px;margin-top:-45px;"><?php echo ($vvv["remark"]); ?></div>
                                                </div>
                                                <br>
                                                <?php if(is_array($roleinfo['three_stage'])): foreach($roleinfo['three_stage'] as $key=>$vvvv): if(($vvvv["parent_id"]) == $vvv["id"]): ?><div style="border-bottom:1px solid #e3e3e3;line-height:45px;" class="div4 authHover">
                                                            <div style="width:35%;border-right:1px solid #e3e3e3">
                                                                <input class="sub checkRole" type="checkbox" style="margin-left:180px;" name="subBox"
                                                                       value="<?php echo ($vvvv["id"]); ?>"
                                                                <?php if(in_array(($vvvv["id"]), is_array($roleinfo['auth_id'])?$roleinfo['auth_id']:explode(',',$roleinfo['auth_id']))): ?>checked<?php endif; ?>
                                                                />
                                                                <?php echo ($vvvv["name"]); ?>
                                                            </div>
                                                            <div style="width:65%;float:right;padding-left:10px;margin-top:-45px;"><?php echo ($vvvv["remark"]); ?></div>
                                                        </div><?php endif; endforeach; endif; ?>
                                            </div><?php endif; endforeach; endif; ?>
                                </div><?php endif; endforeach; endif; ?>
                    </dd>
                </dl>
                <div style="height:40px;background-color: #e1e6ea"></div><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>

        <div class="text-center clearfix pwb pull-right">
            <input type="submit" value="保存" class="btn btn-warning" onclick="saveAuth()"/>
            <a href="<?php echo U('Role/index',array('id'=>I('get.id')));?>" class="btn btn-info">返回</a>
            <input type="hidden" value="<?php echo U('Role/allAuth',array('id'=>I('get.id'),'aid'=>I('get.aid')));?>" id="chkr"/>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>

</body>
</html>