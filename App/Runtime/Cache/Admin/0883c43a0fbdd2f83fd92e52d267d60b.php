<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>

    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">统计报表</a></div>
        <div class="padbox slides">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="bg"></div>
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/1.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/2.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/9.jpg" alt="...">
                    </div>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="cTitle" style="display: none">统计信息</div>
        <table class="countTable" width="100%" style="display: none">
            <tr>
                <th>代理商数</th>
                <td>33</td>
            </tr>
            <tr>
                <th>供应商数</th>
                <td>2</td>
            </tr>
            <tr>
                <th>上架产品数</th>
                <td>5</td>
            </tr>
            <tr>
                <th>下架产品数</th>
                <td>4</td>
            </tr>
            <tr>
                <th>提现已审核</th>
                <td>25</td>
            </tr>
            <tr>
                <th>提现待审核</th>
                <td>1</td>
            </tr>
            <tr>
                <th>提现拒绝</th>
                <td>10</td>
            </tr>
        </table>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>