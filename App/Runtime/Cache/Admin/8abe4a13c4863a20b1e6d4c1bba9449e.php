<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">财务管理</a>&gt;
            <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185) or ($_SESSION['role_id']== 186)): ?>账户信息
                <?php else: ?>
                运营商账户<?php endif; ?>
        </div>
        <div class="cTitle2">
            <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185) or ($_SESSION['role_id']== 186)): ?>平台商账户
                <?php else: ?>
                运营商账户<?php endif; ?>
        </div>

        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="50%">账户余额</th>
                <td class="text-c5">&yen;<dfn><?php echo (getYuan($operatorInfo["account_balance"])); ?></dfn></td>
            </tr>
            <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185) or ($_SESSION['role_id']== 186)): ?><tr>
                    <th>门票可用余额</th>
                    <td class="text-c5">&yen;<dfn><?php echo (getYuan($operatorInfo["account_ticket_balance"])); ?></dfn></td>
                </tr>
                <tr>
                    <th>机票可用余额</th>
                    <td class="text-c5">&yen;<dfn><?php echo (getYuan($operatorInfo["account_credit_balance"])); ?></dfn></td>
                </tr><?php endif; ?>
        </table>


        <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185) or ($_SESSION['role_id']== 186)): ?><table class="cTable table-hover" width="100%">
                <div class="cTitle2" style="margin-top:50px;">运营商账户余额</div>
                <?php if(is_array($list)): foreach($list as $key=>$vo): ?><tr>
                        <th width="50%"><?php echo ($vo["display_name"]); ?></th>
                        <td class="text-c5">&yen;<dfn><?php echo (getYuan($vo["account_credit_balance"])); ?></dfn></td>
                    </tr><?php endforeach; endif; ?>
            </table><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>