<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="">财务管理</a>&gt;提现申请</div>
        <div class="cTitle2">提现申请</div>
        <div class="form-horizontal txAply">
            <div class="form-group">
                <input type="hidden" id="code" value="<?php echo (session('applyWithDrawCode')); ?>">

                <?php if(is_array($list)): foreach($list as $key=>$vo): ?><table class="table table-bordered" style="width:80%;margin-left: 100px">
                        <tr>
                            <td class="text-center">发票抬头</td>
                            <td class="text-c1"><?php echo ($vo["invoice_title"]); ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">提现账号</td>
                            <td>
                                <span class="bankNameInfo"><?php echo ($info["bank_name"]); ?></span>&nbsp;&nbsp;
                                <span class="bankAccountInfo"><?php echo ($info["bank_account"]); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">账户总额</td>
                            <td class="text-c5 balance"><?php echo (getYuan($vo['account_balance'])); ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">冻结金额</td>
                            <td class="text-c5 balance"><?php echo (getYuan($vo['frozen_amount'] + $vo['order_tour_frozen_amount'])); ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">可用余额</td>
                            <td class="text-c5 balance"><?php echo (GetYuan($vo['account_balance'] - $vo['frozen_amount'] - $vo['order_tour_frozen_amount'])); ?></td>
                        </tr>
                        <tr>
                            <td class="text-center">提现金额</td>
                            <td><input type="number" class="form-control withdraw<?php echo ($vo["id"]); ?>" name="withdraw" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="line-height: 40px;">
                                <span style="color:red">*温馨提示：</span>您需要给该运营商开具发票，且发票金额应大于等于申请提现的金额。
                                <button type="button" style="margin-left:37px;line-height: 26px;height:40px;" class="btn btn-warning pull-right"
                                        onclick="withdraw(<?php echo ($vo["id"]); ?>, <?php echo ($vo["account_balance"]); ?>, <?php echo ($vo["operator_id"]); ?>)">申请提现
                                </button>
                            </td>
                        </tr>
                    </table>
                    <p>&nbsp;</p><?php endforeach; endif; ?>

                <div style='border-top:1px dashed #ccc;margin-top:40px;margin-left: 100px;margin-bottom: 40px;width: 80%'></div>

                <table class='table table-bordered' style='width: 80%;margin-left: 100px'>
                    <tr>
                        <td class='text-center'>账户总额:</td>
                        <td class='text-c5'>&yen;<?php echo (GetYuan($info["account_balance"])); ?></td>
                    </tr>
                    <tr>
                        <td class='text-center'>冻结金额:</td>
                        <td class='text-c5'>&yen;<?php echo (GetYuan($amount)); ?></td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>