<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>

    <form class="form-horizontal zcform" id="signupForm" method="post" action="<?php echo U('Provider/edit',array('id'=> $list['id']));?>" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="<?php echo ($list["id"]); ?>">
        <input type="hidden" id="keyid" value="<?php echo (setEncrypt($list["id"])); ?>">
        <input id="editName" type="hidden" value="<?php echo U('Provider/edit');?>"/>

        <div class="rightbox pull-right">
            <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt;<a
                    href="<?php echo U('Provider/index',array('id' => 4));?>">供应商列表</a>&gt;修改供应商
            </div>
            <div class="cTitle2">修改供应商</div>
            <div class="action">
                <a href="javascript:void(0)" class="current" data-click-tab="1">基本信息</a>
                <a href="javascript:void(0)" data-click-tab="2">扩展信息</a>
            </div>

            <div class="panelCon" id="tab-1">

                <div class="form-group">
                    <label class="col-sm-2 control-label">公司简称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="请输入公司简称" name="name" id="name"
                               value="<?php echo ($list["name"]); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">公司全称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="请输入公司全称" name="full_name" id="fullName"
                               value="<?php echo ($list["full_name"]); ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">登录名：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="login_name" placeholder="请输入登录名" id="login_name"
                               value="<?php echo ($user_list["login_name"]); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">固定电话：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="phone" placeholder="请输入供应商固定电话" id="phone"
                               value="<?php echo ($list["phone"]); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">业务联系人：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="login_name" placeholder="请输入业务联系人"
                               id="contact_person" value="<?php echo ($list["contact_person"]); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">业务联系人手机：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="mobile" name="telphone" class="required"
                               placeholder="请输入手机号" value="<?php echo ($list["mobile"]); ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">消息类型：</label>

                    <div class="col-sm-4">
                        <label class="checkbox-inline">
                            <input type="checkbox" style="margin-top: 1px;" class="productMessage">产品消息
                        </label>
                        <label class="checkbox-inline" style="margin-left: 20px;">
                            <input type="checkbox" style="margin-top: 1px;" class="transactionMessage">交易消息
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">开户行：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="bank_name" name="bank_name" class="required"
                               placeholder="请输入开户行名称" value="<?php echo ($list["bank_name"]); ?>"></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">账号：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" placeholder="请输入银行卡号" name="bank_account"
                               id="bank_account" value="<?php echo ($list["bank_account"]); ?>"></div>
                </div>
                <div class="text-center clearfix pwb pull-right">
                    <input type="button" value="修改" class="btn btn-warning saveEditProviderInfo"/>
                </div>
            </div>

            <div class="panelCon" style="display:none" id="tab-2">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">许可经营目的地：</label>

                        <div class="col-sm-4">
                            <input type="text" name="destinations" class="form-control" placeholder="请输入许可经营目的地"
                                   id="destinations" value="<?php echo ($list["destinations"]); ?>">
                        </div>
                        <div class="col-sm-4" style="color: red">
                            *许可经营的目的地（多个用逗号分隔）
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">佣金比例：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="0" value="<?php echo ($list["commission_rate"]); ?>"
                                   id="commission_rate"/>
                        </div>
                        <span class="col-sm-6 tp" style="width:49%;margin-left:-10px;"><em style='color:#333'>（‰）</em>&nbsp;</span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">提现周期：</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" data-toggle="tooltip" data-placement="top" title="提加几结算"
                                   id="settlement_interval" value="<?php echo ($list["settlement_interval"]); ?>">
                        </div>
                    </div>
                    <div class="form-group" id="modal_times">
                        <label class="col-sm-2 control-label"
                               style="width:17.667%;padding-right:0px;">合约起止时间：</label>

                        <div class="col-sm-4" style="width:37.333%;">
                            <input type="text" id="timestart" name="contract_start_time" type="text" style="width:45%;border:1px solid #ccc;height:35px;"
                                   <?php if($list['contract_start_time'] != '0000-00-00 00:00:00'): ?>value="<?php echo (substr($list["contract_start_time"],0,10)); ?>"<?php endif; ?>
                                   class="Wdate" id="d4322"
                                   onclick="WdatePicker({el: $dp.$('d12')})"/>
                            到
                            <input type="text" id="timeend" name="contract_end_time" type="text" style="width:45%;border:1px solid #ccc;height:35px;"
                                   <?php if($list['contract_end_time'] != '0000-00-00 00:00:00'): ?>value="<?php echo (substr($list["contract_end_time"],0,10)); ?>"<?php endif; ?>
                                   class="Wdate" id="d4322"
                                   onFocus="WdatePicker({minDate: '#F<?php echo ($dp["$D('d4321',{d:3"]); ?>);}'})"/>
                        </div>
                        <span class="col-sm-6 tp" style="width:45%"></span>
                    </div>
                    <div class="text-center clearfix pwb pull-right">
                        <input type="button" value="修改" class="btn btn-warning saveEditProviderInfo"/>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
<input type="hidden" class="productMessageVal" value="<?php echo ($productMessage); ?>">
<input type="hidden" class="transactionMessageVal" value="<?php echo ($transactionMessage); ?>">
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="/Public/js/providerInfo.js?v=9"></script>
<script>
    $("#settlement_interval").hover(function(){
        $('#settlement_interval').tooltip('show')
    });

    var productMessageVal = $(".productMessageVal").val();
    var transactionMessageVal = $(".transactionMessageVal").val();
    if(productMessageVal == 1){
        $(".productMessage").prop("checked", true);
    }

    if(transactionMessageVal == 1){
        $(".transactionMessage").prop("checked", true);
    }
</script>
</body>
</html>