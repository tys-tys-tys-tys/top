<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">线路产品</a>&gt;线路产品列表</div>
        <div class="cTitle2">线路产品列表</div>
        <div class="main-search">
            <div class="top clearfix">
                <form class="form-inline" method="get" value="<?php echo U('Tour/index', array('id' => 35));?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="代码/线路名称/目的地" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <form class="pull-right" style="padding-bottom: 5px;">
                <a href="<?php echo U('Tour/add',array('id'=>I('get.id')));?>" class="btn btn-info">新增</a>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
                <input type="hidden" id="chkr" value="<?php echo U('Tour/delete');?>"/>
                <input id="chkState" type="hidden" value="<?php echo U('Tour/index');?>"/>
            </form>
        </div>
        <table id='subBoxOne' class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold">全选</label></th>
                <th width="8%">代码</th>
                <th width="10%">预览线路</th>
                <th width="15%">线路名称</th>
                <th width='10%'>录入</th>
                <th width='10%'>出发地</th>
                <th width='8%'>目的地</th>
                <th width='12%'>状态</th>
                <th width='10%'>最新团期</th>
                <th width='10%'>创建时间</th>
                <th width="8%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>
                    <td>10<?php echo ($v["id"]); ?></td>
                    <td>
                        <a class="showqrc" value="<?php echo ($v["id"]); ?>" style="cursor: pointer">手机预览</a>
                    </td>
                    <td style="text-align: left">
                        <?php if($v['promotion'] == 1): ?><span class="badge" style="background: red">特</span>&nbsp;<?php endif; ?>
                        <a style="color:blue;text-decoration: none" href="<?php echo U('Tour/details',array('id'=>$v['id']));?>"
                           target="_balank"><?php echo ($v["name"]); ?></a></td>
                    <td><?php echo ($v["contact_person"]); ?></td>
                    <td><?php echo ($v["departure"]); ?></td>
                    <td><?php echo ($v["destination"]); ?></td>
                    <td class="show_msg">
                        <?php if($v["state"] == 1): ?><span style="color:#000666">未提交</span>
                            <?php elseif($v["state"] == 2): ?>
                            <span style="color:blue">已审核，已上架</span>
                            <?php elseif($v["state"] == 3): ?>
                            <span style="color:darkgreen">已提交，待审核</span>
                            <?php elseif($v["state"] == 4): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                               title="<?php echo ($v["audit_memo"]); ?>"><span style="color:red">审核失败</span></a>
                            <?php elseif($v["state"] == 5): ?>
                            <span style="color:red">已下架</span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($v['latest_sku'] <= $nowDate): ?><span style='color:red'><?php echo (substr($v["latest_sku"],0,10)); ?></span>
                            <?php else: ?>
                            <span class="text-c1"><?php echo (substr($v["latest_sku"],0,10)); ?></span><?php endif; ?>
                    </td>
                    <td><?php echo ($v["create_time"]); ?></td>
                    <td>
                        <div class="btn-group pull-right">
                            <button type="button" style="padding:3px 7px" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="margin: 0px;background: #eee">
                                <?php if(($_SESSION['user_id']== $v['user_id']) or ($_SESSION['is_admin']== 1) or ($canModifyTours == 1)): ?><li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateSubmitted;?>)"
                                           style="cursor: pointer;padding:0px 14px;">申请上架</a></li>
                                    <li><a href="<?php echo U('Tour/edit',array('id'=>I('get.id'),'tid'=>$v['id']));?>"
                                           style="cursor: pointer;padding:0px 14px;">修改</a></li>
                                    <li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateCanceled;?>)"
                                           style="cursor: pointer;padding:0px 14px;">下架</a></li>
                                    <?php else: ?>
                                    <li><a onclick="friendRemind(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer;padding:0px 14px;">申请上架</a></li>
                                    <li><a onclick="friendRemind(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer;padding:0px 14px;">修改</a></li>
                                    <li><a onclick="friendRemind(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer;padding:0px 14px;">下架</a></li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <p>

        <div class="well">
            温馨提示：
            <p style='margin-left: 50px;'>鼠标移到<span style="color: red"> 审核失败 </span>红字上，您可查看被拒绝原因</p>

            <p style='margin-left: 50px;'>最新团期为<span style="color: red"> 红色 </span>时，请即时添加团期</p></div>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel" style="text-align: left"></h4>
        </div>
        <div class="modal-body" id="modalB">
            <textarea class="form-control" name="reason" style="resize: none;"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalDel" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">提示</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox"><i class="warn"></i>请将信息填写完整</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">确认</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">取消</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width:300px;height:330px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">微信扫一扫,在手机上预览</h4>
            </div>
            <div class="modal-body text-center">
                <p class="tipsBox" style="width: 300px;margin-left: -120px;margin-top: -30px;"><i class="suc"></i></p>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>

</body>
</html>