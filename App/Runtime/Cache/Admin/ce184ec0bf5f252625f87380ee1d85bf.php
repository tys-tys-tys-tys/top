<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>

    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">用户/角色</a></div>
        <div class="padbox slides">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="bg"></div>
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/1.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/2.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="http://cdn.lydlr.com/public/system/images/temp/9.jpg" alt="...">
                    </div>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="cTitle">统计信息</div>
        <?php if($_SESSION['entity_type']== 2): ?><table class="countTable" width="100%">
                <tr>
                    <th>运营商账号数(含子账号)</th>
                    <td><?php echo (getOperatorAccountNum(session('operator_id'))); ?></td>
                </tr>
                <tr>
                    <th>供应商账号数(含子账号)</th>
                    <td><?php echo (getProviderAccountNum(session('operator_id'))); ?></td>
                </tr>
                <tr>
                    <th>代理商账号数(含测试账号)</th>
                    <td><?php echo (getAgencyAccountNum(session('operator_id'))); ?></td>
                </tr>
                <tr>
                    <th>当前用户所属角色</th>
                    <td><?php echo (getUserName(session('role_id'))); ?></td>
                </tr>
            </table><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>