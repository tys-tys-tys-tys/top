<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" />
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title>大旅通 - 旅游交易系统</title>
    <link rel="shortcut icon" href="http://cdn.lydlr.com/public/images/favicon.ico"/>
    <link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.css?v=1" rel="stylesheet"/>
    <link href="http://cdn.lydlr.com/public/css/capacity.css?v=2" rel="stylesheet"/>
    <link href="/Public/system/index.css?v=9" rel="stylesheet"/>
    <link rel="stylesheet" href="http://cdn.lydlr.com/public/artDialog-6.0.4/ui-dialog.css"/>
    <link href="http://cdn.lydlr.com/public/css/top.css?v=3" rel="stylesheet"/>
    <script src="http://cdn.lydlr.com/public/js/html5shiv.js"></script>
    <style>
        .dropdown-menu li a:hover {
            background: #ccc;
        }
    </style>
</head>
<body>
<header class="header">
    <div class="navtop">
        <div class="wrapper text-right" style="position:relative;background: #255E89">
            <?php if($_SESSION['entity_type']== 1): ?><span class="glyphicon glyphicon-user" style="margin-right:-15px;color:#96e7fa"></span>
                <span class="username"><span style="color:#fff;">北京平台中心</span></span>
            <?php elseif($_SESSION['entity_type']== 2): ?>
                <span class="glyphicon glyphicon-user" style="margin-right:-15px;color:#ffffff"></span>
                <span class="username"><span style="color:#fff;"><?php echo (session('entity_name')); ?></span></span>
            <?php elseif($_SESSION['entity_type']== 3): ?>
                <span class="glyphicon glyphicon-user" style="margin-right:-15px;color:#fd3149"></span>
                <span class="username"><span style="color:#fff;"><?php echo (session('entity_name')); ?></span></span>
            <?php else: ?>
                <span class="glyphicon glyphicon-user" style="margin-right:-15px;color:#ffb20e"></span>
                <span class="username"><span style="color:#fff;"><?php echo (session('entity_name')); ?></span></span><?php endif; ?>
                <?php echo (session('name')); ?> (<?php echo (session('baker_login_name')); ?>)
            <a onclick="loginOut()" style="cursor: pointer"> 退出登录</a>
            <input type="hidden" id="loginOut" value="<?php echo U('Manager/logout');?>"/>
        </div>
    </div>
    <div class="navbot">
        <div class="wrapper clearfix" style="background: #F7FAFA">
            <div class="logo pull-left"><a href="#"><img src="http://cdn.lydlr.com/public/system/images/logo.png"/></a>
            </div>
            <ul class="pull-left item">
                <?php if(is_array($pauth_info)): $i = 0; $__LIST__ = $pauth_info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><li>
                        <a href="/index.php/Admin/<?php echo ($v["controller"]); ?>/<?php echo ($v['action']); ?>/id/<?php echo ($v["id"]); echo C('TMPL_TEMPLATE_SUFFIX');?>"
                           target="_self">
                            <i class="i-<?php echo ($v["icon"]); ?>"></i>
                            <span
                            <?php if($v['id'] == I('id')): ?>class="w hovers"
                                <?php else: ?>
                                class="w"<?php endif; ?>
                            ><?php echo ($v["name"]); ?></span>
                        </a>
                    </li><?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
    </div>
</header>