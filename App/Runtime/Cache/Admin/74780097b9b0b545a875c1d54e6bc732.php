<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：订单管理&gt;
            <?php if($_SESSION['entity_type']== 3): ?><a href="<?php echo U('OrderTour/index',array('id'=>I('get.id')));?>">
                    线路产品订单
                </a>
                <?php else: ?>
                线路产品订单<?php endif; ?>
            &gt;订单详情
        </div>
        <div class="cTitle2">订单详情</div>
        <div class="orderDetail">
            <div class="top">
                <a href="#">游客信息</a><a href="#">订单跟踪</a>
            </div>

            <table width="100%" class="order-Tb table-hover">
                <tr>
                    <th>成人数</th>
                    <th>儿童数</th>
                    <th>单房差数</th>
                    <th>参团方式</th>
                    <th>去程交通</th>
                    <th>回程交通</th>
                    <th>联系人</th>
                    <th>联系电话</th>
                    <th>出团时间</th>
                </tr>
                <tr>
                    <td><?php echo ($info["client_adult_count"]); ?>人</td>
                    <td><?php echo ($info["client_child_count"]); ?>人</td>
                    <td><?php echo ($info["hotel_count"]); ?>人</td>
                    <td>
                        <?php if($info['tour']['travel_type'] == 1): ?>散拼团
                            <?php elseif($info['tour']['travel_type'] == 2): ?>
                            自由行
                            <?php elseif($info['tour']['travel_type'] == 3): ?>
                            当地游<?php endif; ?>
                    </td>
                    <td><?php echo ($info["tour"]["traffic_go"]); ?></td>
                    <td><?php echo ($info["tour"]["traffic_back"]); ?></td>
                    <td><?php echo ($info["tour"]["contact_person"]); ?></td>
                    <td><?php echo ($info["tour"]["contact_phone"]); ?></td>
                    <td><?php echo (substr($info["start_time"],0,10)); ?></td>
                </tr>
            </table>
            <table width="100%" class="order-Tb">
                <tr>
                    <th>订单跟踪</th>
                    <th>发生金额</th>
                    <th>订单总额</th>
                    <th>备注</th>
                    <th>处理时间</th>
                    <th>操作人</th>
                </tr>
                <?php if(is_array($orderLog)): foreach($orderLog as $key=>$vo): ?><tr>
                        <td style="text-align:left"><?php echo ($vo["content"]); ?></td>
                        <td class="text-c5"><?php echo (getYuan($vo["amount"])); ?></td>
                        <td class="text-c1"><?php echo (getYuan($vo["payable_total"])); ?></td>
                        <td>
                            <?php if($vo['remark'] != ''): echo ($vo["remark"]); ?>
                                <?php else: ?>
                                --<?php endif; ?>
                        </td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                        <td><?php echo ($vo["user_name"]); ?></td>
                    </tr><?php endforeach; endif; ?>
            </table>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        //审核产品切换
        $('.order-Tb').not(".order-Tb:first").hide();
        $('.top a:first').addClass('current');
        $('.top a').click(function () {
            var id = $(this).index();
            $('.top a').removeClass('current');
            $(this).addClass('current');
            $('.order-Tb').hide();
            $('.order-Tb').eq(id).show();
        });
    })
</script>
</body>
</html>