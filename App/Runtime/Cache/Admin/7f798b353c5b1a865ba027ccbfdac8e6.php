<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<style>
    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 0px solid #ddd;
        line-height: 1.42857;
        padding: 8px;
        vertical-align: top;
    }
</style>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">通用产品</a>&gt;<a
                href="<?php echo U('Product/index',array('id' => 111));?>">通用产品列表</a>&gt;新增通用产品
        </div>
        <div class="cTitle2">新增通用产品</div>
        <form method="post" action="<?php echo U('Product/add');?>" enctype="multipart/form-data">
            <input type="hidden" id="editProduct" value="<?php echo U('Product/add');?>">
            <table class='table' style='width: 77%;margin-left: 50px;'>
                <tr>
                    <th class='text-right' style="line-height: 29px;">产品名称：</th>
                    <td>
                        <input type="text" id="name" name="name" class="form-control" placeholder="请输入产品名称"
                               style='width: 650px' value="<?php echo ($info["name"]); ?>"/>
                    </td>
                </tr>
                <tr>
                    <th class='text-right' style="line-height: 29px;">产品描述：</th>
                    <td>
                        <script id="editor1" type="text/plain" name="description"><?php echo ($info["description"]); ?></script>
                    </td>
                </tr>
                <tr class="priceCost" hidden>
                    <th class='text-right' style="line-height: 29px;">成本价格：</th>
                    <td>
                        <input type="text" id="price_cost" name="price_cost" class="form-control"
                               placeholder="请输入成本价格" style='width: 650px' value="<?php echo (getYuan($info["price_cost"])); ?>"/>
                    </td>
                </tr>
                <tr>
                    <th class='text-right' style="line-height: 29px;">同行价<span class="showPriceCost">：</span></th>
                    <td>
                        <input type="text" id="price" name="price" class="form-control" placeholder="请输入同行价"
                               style='width: 650px' value="<?php echo (getYuan($info["price_agency"])); ?>"/>
                    </td>
                </tr>
                <!--<tr>-->
                    <!--<th class='text-right' style="line-height: 29px;">参数类型：</th>-->
                    <!--<td>-->
                        <!--<select name="para_type" id="para_type"-->
                                <!--style="width: 100px;height: 35px;line-height: 35px;border: 1px solid #d0d0d0">-->
                            <!--<?php if($info['para_type'] == 1): ?>-->
                                <!--<option value="<?php echo ($para_type); ?>" checked>保险</option>-->
                                <!--<?php else: ?>-->
                                <!--<option value="">未选择</option>-->
                                <!--<option value="1">保险</option>-->
                            <!--<?php endif; ?>-->
                        <!--</select>-->
                    <!--</td>-->
                <!--</tr>-->
                <!--<tr>-->
                    <!--<th class='text-right' style="line-height: 29px;">参数值：</th>-->
                    <!--<td>-->
                        <!--<input type="number" id="para_value" name="para_value" class="form-control" placeholder="请输入参数值"-->
                               <!--style='width: 650px' value="<?php echo ($info["para_value"]); ?>"/>-->
                    <!--</td>-->
                <!--</tr>-->
                <tr>
                    <th>产品图片：</th>
                    <td id="pics">
                        <div class="row" style="margin:10px;">
                            <div class="col-sm-7">&nbsp;
                            </div>
                            <div class="col-sm-5">
                                <div id="filePicker"
                                     class="btn btn-sm btn-primary webuploader-container webuploader-picker">
                                    选择图片
                                </div>
                                <input id="btnUploadCtrl" type="button" class="btn btn-sm btn-primary"
                                       value="开始上传"/>
                            </div>
                        </div>
                        <hr style="padding-bottom: 1px;"/>
                        <input type="hidden" id="cover-pic" value="<?php echo ($info["cover_pic"]); ?>"/>

                        <div class="container" style="max-width: 660px;" id="fileList">
                            <?php if(is_array($info['imgs'])): $i = 0; $__LIST__ = $info['imgs'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(!empty($vo['0'])): ?><div id="" class="row upload-item" style="margin-bottom: 10px;"
                                         path="<?php echo ($vo["0"]); ?>" isCover="<?php echo ($vo["1"]); ?>">
                                        <div class="col-sm-2"><img
                                            <?php if($vo['0'] == $info['cover_pic']): ?>class="img-preview img-cover"
                                                <?php else: ?>
                                                class="img-preview"<?php endif; ?>
                                            alt="" src="<?php echo (GetRealImageUrl($vo["0"])); ?>">
                                        </div>
                                        <div class="col-sm-8">
                                            <textarea rows="3" class="form-control"
                                                      placeholder="请输入图片描述"><?php echo ($vo["2"]); ?></textarea>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="dropdown">
                                                <button data-toggle="dropdown" id="dropdownMenu1"
                                                        class="btn dropdown-toggle" type="button">
                                                    操作
                                                    <span class="caret"></span>
                                                </button>
                                                <ul aria-labelledby="dropdownMenu1" role="menu"
                                                    class="dropdown-menu">
                                                    <li role="presentation">
                                                        <a class="move-up" href="#" tabindex="-1"
                                                           role="menuitem">上移</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a class="move-down" href="#" tabindex="-1"
                                                           role="menuitem">下移</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a class="setCover" href="javascript:;" tabindex="-1"
                                                           role="menuitem">设为封面</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a class="removeFile uploaded" href="javascript:;"
                                                           tabindex="-1" role="menuitem">
                                                            移除图片
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="upload-msg"></div>
                                        </div>
                                    </div><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class='text-right' style="line-height: 29px;">产品库存：</th>
                    <td>
                        <a href="#" data-toggle="tooltip" data-placement="top"
                           title="暂无意义">
                            <input type="number" class="form-control" id="stock" name="stock" placeholder="请输入您的库存"
                                   style='width: 650px' value="0"/>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th class='text-right' style="line-height: 29px;">&nbsp;</th>
                    <td class='text-right'>
                        <input type='button' class='btn btn-warning' value='提交' id="saveProductEdit">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script type="text/javascript" charset="utf-8" src="http://cdn.lydlr.com/public/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.lydlr.com/public/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://cdn.lydlr.com/public/ueditor/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript" src="http://cdn.lydlr.com/public/webuploader-0.1.5/webuploader.js"></script>
<script type="text/javascript" src="http://cdn.lydlr.com/public/js/upload.js?v=1"></script>
<script type="text/javascript">
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });

    var ue = UE.getEditor('editor1');

    $(".showPriceCost").click(function () {
        $(".priceCost").show();
    });
</script>
</body>
</html>