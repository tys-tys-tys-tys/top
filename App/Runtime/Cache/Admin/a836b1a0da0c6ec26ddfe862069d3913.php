<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">账户调整管理</a>&gt;账户调整添加</div>
        <div class="cTitle2">账户调整添加</div>
        <div class="main-search">
            <div class="top clearfix">
                <form method="post" action="<?php echo U('Refund/add',array('id'=>I('get.id')));?>" class="form-inline">
                    <div class="btn-group">
                        <select name='account_type' class='account_type'
                                style='height:33px;width:105px;border:1px solid #ddd;'>
                            <option value='1'>&nbsp;&nbsp;-----类型-----</option>
                            <option value='4'>&nbsp;&nbsp;代理商</option>
                            <option value='3'>&nbsp;&nbsp;供应商</option>
                            <?php if($_SESSION['role_id']== 1): ?><option value='2'>&nbsp;&nbsp;运营商账户余额</option>
                                <option value='5'>&nbsp;&nbsp;运营商授信余额</option>
                                <option value='6'>&nbsp;&nbsp;平台商机票</option><?php endif; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control name" name='name' placeholder="请输入姓名"
                               style='margin-left: 10px;'/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
        </div>

        <form method="post" action="">
            <input type='hidden' id='code' value='<?php echo (session('refundCode')); ?>'>
            <input id="account_type" type="hidden" value="<?php echo ($account_type); ?>"/>
            <input id="refundList" type="hidden" value="<?php echo U('OperatorFinance/lists',array('id' => 99));?>"/>
            <input id="saveRefundAdd" type="hidden" value="<?php echo U('Refund/saveRefund');?>"/>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th>名称</th>
                    <th>合约起止</th>
                    <th>账户总额</th>
                    <th>账户</th>
                    <th>状态</th>
                    <th width="12%">操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><td>
                        <?php if($v['istest'] == 1): ?><span class='text-c1'><?php echo ($v["name"]); ?></span>
                            <?php else: ?>
                            <?php echo ($v["name"]); endif; ?>
                    </td>

                    <td><?php echo (substr($v["contract_start_time"],0,10)); ?><br/><?php echo (substr($v["contract_end_time"],0,10)); ?></td>
                    <td id="money-right" class="text-c5">
                        <?php if(($account_type == '5') or ($account_type == '6')): echo (GetYuan($v["account_credit_balance"])); ?>
                            <?php else: ?>
                            <?php echo (GetYuan($v["account_balance"])); endif; ?>
                    </td>
                    <td>
                        <?php if($v["account_state"] == 1): ?><span class="text-c7">正常</span>
                            <?php elseif($v["account_state"] == 2): ?>
                            <span class="text-c8">冻结</span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($v["state"] == 1): ?><span class="text-c7">正常</span>
                            <?php elseif($v["state"] == 2): ?>
                            <span class="text-c8">禁用</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="javascript:void(0)" onclick="addRefund(<?php echo ($v["id"]); ?>)">变动</a></li>
                            </ul>
                        </div>
                    </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="modal fade" id="myModalLabel" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    账户手动调整添加
                </h4>
            </div>
            <div class="modal-body">
                金额：
                <input type="text" class="form-control" id="amount">
                凭证：
                <input type="text" class="form-control" id="proof">
                备注：
                <input type="text" class="form-control" id="memo">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary" id="saveRefund">
                        提交
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>

<script>
    $("form").submit(function (event) {
        if ($(".account_type").val() == 1) {
            jalert('未选择类型', function () {
            });
            return false;
        }

        if (!$(".name").val()) {
            jalert('请输入姓名', function () {
            });
            return false;
        }
    });
</script>
</body>
</html>