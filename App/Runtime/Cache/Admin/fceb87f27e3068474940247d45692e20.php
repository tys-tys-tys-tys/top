<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：运营商列表</div>
        <div class="cTitle2">运营商列表</div>
        <?php if($_SESSION['role_id']== 1): ?><div class="main-search">
                <form class="pull-right" style="padding-bottom: 5px;">
                    <span style="cursor: pointer" class="btn btn-info" onclick="addOpoerator()">新增</span>
                    <button type="button" class="btn btn-danger" onclick="optertorDelete()">删除</button>
                    <input id="chka" type="hidden" value="<?php echo U('Operator/add');?>"/>
                </form>
            </div><?php endif; ?>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="6%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male">全选</label></th>
                <th width="5%">编号</th>
                <th width="20%">名称</th>
                <th width="15%">账户余额</th>
                <th width="15%">授信余额</th>
                <th width="15%">域名</th>
                <th width="10%">账号</th>
                <th width="10%">状态</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo ($v["id"]); ?>"/></td>
                    <td><?php echo ($v["id"]); ?></td>
                    <td><?php echo ($v["name"]); ?></td>
                    <td class="text-c1" id="money-right"><dfn>&yen;<?php echo (GetYuan($v["account_balance"])); ?></dfn></td>
                    <td class="text-c5" id="money-right"><dfn>&yen;<?php echo (GetYuan($v["account_credit_balance"])); ?></dfn></td>
                    <td><?php echo ($v["domain"]); ?></td>
                    <td><?php echo (getOperatorTotalAccountNum($v["id"])); ?></td>
                    <td>
                        <?php if($v["state"] == 1): ?><span style="color:green">正常</span>
                            <?php else: ?>
                            <span style="color:red">禁用</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="<?php echo U('Operator/edit', array('id' => 2,'oid' => $v['id']));?>">编辑</a></li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">运营商添加</h4>
        </div>
        <div class="modal-body" style="border-bottom: 1px solid #e5e5e5;">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">公司名称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="modal_name"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*请输入公司名称</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">登录名称：</label>

                    <div class="col-sm-4">
                        <input type="text" class="form-control" value="" id="modal_login"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*必须填写登录名称（英文、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" value="" id="modal_pwd"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*必须输入登录名密码（字母、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">确认密码：</label>

                    <div class="col-sm-4">
                        <input type="password" class="form-control" value="" id="modal_pwd2"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*必须输入登录名密码（字母、数字）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:17.667%;padding-right:0px;">手机：</label>

                    <div class="col-sm-4">
                        <input type="number" class="form-control" value="" id="modal_phone"/>
                    </div>
                    <span class="col-sm-6 tp" style="width:45%">*手机号必须填写</span>
                </div>

            </div>
        </div>
        <div class="modal-footer pull-right" style="border:0">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script type="text/javascript" src="/Public/js/operator.js?v=4"></script>
</body>
</html>