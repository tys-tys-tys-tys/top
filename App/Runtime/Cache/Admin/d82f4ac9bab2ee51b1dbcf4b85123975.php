<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">账户调整管理</a>&gt;账户调整列表</div>
        <div class="cTitle2">账户调整列表</div>
        <div class="main-search" style="margin-bottom: 28px">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Refund/index',array('id'=>I('get.id')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入姓名"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>

            <form class="form-inline pull-right">
                <input type="hidden" id="code" value="<?php echo (session('checkRefundCode')); ?>">
                <input id="chkState" type="hidden" value="<?php echo U('Refund/index');?>"/>
            </form>
        </div>
        <form method="post" action="">
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width='8%'>编号</th>
                    <th width='10%'>姓名</th>
                    <th width='10%'>实体类型</th>
                    <th width='12%'>账户类型</th>
                    <th width='15%'>金额</th>
                    <th width='15%'>凭证</th>
                    <th width='15%'>状态</th>
                    <th width='15%'>备注</th>
                    <th width="12%">操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($v["id"]); ?></td>
                        <td><?php echo ($v["entity_name"]); ?></td>
                        <td>
                            <?php if($v['entity_type'] == 4): ?><span>代理商</span>
                                <?php elseif($v['entity_type'] == 3): ?>
                                <span>供应商</span>
                                <?php elseif($v['entity_type'] == 2): ?>
                                <span>运营商</span>
                                <?php elseif($v['entity_type'] == 5): ?>
                                <span>运营商</span>
                                <?php elseif($v['entity_type'] == 1): ?>
                                <span>平台商</span><?php endif; ?>
                        </td>
                        <td>
                            <?php if($v['account_type'] == 4): ?><span class='text-c1'>代理商</span>
                                <?php elseif($v['account_type'] == 3): ?>
                                <span class='text-c5'>供应商</span>
                                <?php elseif($v['account_type'] == 2): ?>
                                <span class='text-c3'>运营商账户余额</span>
                                <?php elseif($v['account_type'] == 5): ?>
                                <span class='text-c3'>运营商账户充值</span>
                                <?php elseif($v['account_type'] == 6): ?>
                                <span class='text-c3'>平台商机票授信</span><?php endif; ?>
                        </td>
                        <td id="money-right" class='text-c5'><?php echo (GetYuan($v["amount"])); ?></td>
                        <td class="text-c1"><?php echo ($v["proof"]); ?></td>

                        <td>
                            <?php if($v["state"] == 1): ?><span style="color:blue">已提交,待审核</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:darkgreen">已审核</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:red">已拒绝</span><?php endif; ?>
                        </td>
                        <td class="text-c1"><?php echo ($v["memo"]); ?></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <?php if($v['state'] == 1): ?><ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:void(0)"
                                               onclick="changeRefundState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo Common\Top::ApplyStatePass;?>')">已审核</a>
                                        </li>
                                        <li><a href="javascript:void(0)"
                                               onclick="changeRefundState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', '<?php echo Common\Top::ApplyStateReject;?>')">拒绝</a>
                                        </li>
                                    </ul>
                                    <?php else: endif; ?>

                            </div>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>

</body>
</html>