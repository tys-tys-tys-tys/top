<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">统计报表</a>&gt;运营商交易统计</div>
        <div class="cTitle2">运营商交易统计</div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-report-state nav-operator-report-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Report/operatorTransactionReport', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <table class="table table-hover table-bordered" style="width:80%;margin-left: 60px;">
            <tr>
                <td class="text-center">财务服务费总额</td>
                <td class="text-c5 text-right">&yen;<dfn> <?php echo (getYuan($invoiceTotal)); ?></dfn></td>
            </tr>

            <tr>
                <td class="text-center">合同销售收入总额</td>
                <td class="text-c5 text-right">&yen;<dfn> <?php echo (getYuan($contractTotal)); ?></dfn></td>
            </tr>

            <tr>
                <td class="text-center">佣金收入总额</td>
                <td class="text-c5 text-right">&yen;<dfn> <?php echo (getYuan($tourTotal)); ?></dfn></td>
            </tr>

            <tr>
                <td class="text-center">总计金额</td>
                <td class="text-c5 text-right">&yen;<dfn> <?php echo (getYuan($total)); ?></dfn></td>
            </tr>
        </table>

    </div>
</div>

<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
</body>
</html>