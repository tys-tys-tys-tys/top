<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style='border: 1px solid #ddd'>
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="<?php echo U('Tour/index',array('id'=>I('get.id')));?>">产品列表</a>&gt;新增路线&gt;详细行程</div>
        <div class="cTitle2">详细行程</div>
        <div class="nav ckAct">
            <span><a
                    href="<?php if($aid == 0): echo U('Tour/add',array('id'=>I('get.id'))); else: echo U('Tour/add',array('id'=>I('get.id'),'tid'=>$aid)); endif; ?>"
                    class="a-1"><i class="i-1"></i>基本信息</a></span>
            <span class="active"><a
                    href="<?php if($aid == 0): echo U('Tour/addPlan',array('id'=>I('get.id'))); else: echo U('Tour/addPlan',array('id'=>I('get.id'),'tid'=>$aid)); endif; ?>"
                    class="a-2"><i class="i-2"></i>详细行程</a></span>
            <span><a
                    href=" <?php if($aid == 0): echo U('Tour/addExtend',array('id'=>I('get.id'))); else: echo U('Tour/addExtend',array('id'=>I('get.id'),'tid'=>$aid)); endif; ?>"
                    class="a-3"><i class="i-3"></i>扩展信息</a></span>
            <span><a
                    href="<?php if($aid == 0): echo U('Tour/addSku',array('id'=>I('get.id'))); else: echo U('Tour/addSku',array('id'=>I('get.id'),'tid'=>$aid)); endif; ?>"
                    class="a-4"><i class="i-4"></i>团期/位控</a></span>
        </div>
        <div class="panelCon"
        <?php if(empty($info)): ?>style='display: block'<?php endif; ?>
        >
        <button type="button" class="btn btn-info pull-right" onclick="addPlan('', '<?php echo (setEncrypt($aid)); ?>')">新增</button>
        <input type="hidden" value="<?php echo U('Tour/planDelete');?>" id='chkr'/>
        <input type="hidden" value="<?php echo U('Tour/planEdit');?>" id='chke'/>
        <input type="hidden" name="id" value="<?php echo ($aid); ?>" id="tourId">
        <input type="hidden" value="<?php echo U('Tour/addPlan',array('id'=>I('get.id'),aid=>$aid));?>" id='chka'/>
        <input type="hidden" value="<?php echo ($aid); ?>" id='kid'/>
        
        <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><table class="baseInfo-Tb table-hover">
                <tr>
                    <th colspan="2" class="num" id="nums<?php echo ($vo["id"]); ?>">
                        <strong>第<?php echo (DayDaXie($i)); ?>天</strong>

                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="editPlan(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>')" style="cursor: pointer">修改</a></li>
                                <li><a onclick="jqchk('<?php echo (setEncrypt($vo["id"])); ?>')" style="cursor: pointer">删除</a></li>
                            </ul>
                        </div>
                    </th>
                </tr>
                <tr>
                    <th>活动地区：</th>
                    <td><?php echo ($vo["activity_addr"]); ?></td>
                </tr>
                <tr>
                    <th>交通信息：</th>
                    <td>
                        <?php if($vo['traffic'] == 1): ?>火车
                            <?php elseif($vo['traffic'] == 2): ?>
                            飞机
                            <?php elseif($vo['traffic'] == 3): ?>
                            轮船
                            <?php elseif($vo['traffic'] == 4): ?>
                            大巴
                            <?php elseif($vo['traffic'] == 5): ?>
                            其他<?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th>景点及活动：</th>
                    <td><?php echo ($vo["scenery"]); ?></td>
                </tr>
                <tr>
                    <th>行程描述：</th>
                    <td><?php echo ($vo["description"]); ?></td>
                </tr>
                <tr>
                    <th>用餐信息：</th>
                    <td>
                        <?php if($vo['dining'] == '1'): ?>早餐
                            <?php elseif($vo['dining'] == '2'): ?>
                            午餐
                            <?php elseif($vo['dining'] == '3'): ?>
                            晚餐
                            <?php elseif($vo['dining'] == '1,2'): ?>
                            早餐、午餐
                            <?php elseif($vo['dining'] == '1,3'): ?>
                            早餐、晚餐
                            <?php elseif($vo['dining'] == '2,3'): ?>
                            午餐、晚餐
                            <?php elseif($vo['dining'] == '1,2,3'): ?>
                            早餐、午餐、晚餐
                            <?php else: ?>
                            自理<?php endif; ?>
                    </td>
                </tr>
            </table><?php endforeach; endif; else: echo "" ;endif; ?>

    </div>
</div>
</div>
<div class="theme-popover" style="width:750px">
    <div class="theme-poptit">
        <div class="itemBlock">
            <div class="top" id="top" data-event-click>第<?php echo (DayDaXie($count+1)); ?>天</div>
            <div class="slide">
                <table class="dLine-tb" width="100%">
                    <tr>
                        <th>活动地区：</th>
                        <td>
                            <input type="text" id="activity_addr" class="form-control txt" name="activity_addr"
                                   placeholder=""/>
                            <!--                            <input type="button" value="添加活动地区" class="btn btn-info" />-->
                            <span class="text-c4" id="msgs">*活动地区必填</span>
                        </td>
                    </tr>
                    <tr>
                        <th>交通信息：</th>
                        <td>
                            <div id="traffics">
                                <label class="checkbox-inline">
                                    <input class="traffic" type="radio" id="traffic1" value="1"/>火车
                                </label>
                                <label class="checkbox-inline">
                                    <input class="traffic" type="radio" id="traffic2" value="2"/>飞机
                                </label>
                                <label class="checkbox-inline">
                                    <input class="traffic" type="radio" id="traffic3" value="3"/>轮船
                                </label>
                                <label class="checkbox-inline">
                                    <input class="traffic" type="radio" id="traffic4" value="4"/>大巴
                                </label>
                                <label class="checkbox-inline">
                                    <input class="traffic" type="radio" id="traffic5" value="5"/>其他
                                </label>
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <th>景点及活动：</th>
                        <td>
                            <input type="text" id="scenery" name="scenery" class="form-control txt" value=""/>
                            <span class="text-c4" id="scenerymsg">输入景点及活动（必填）</span>
                        </td>
                    </tr>
                    <tr>
                        <th>行程描述：</th>
                        <td>
                            <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>住宿信息：</th>
                        <td>
                            <input name="hotel" id="hotel" type="text" class="form-control txt" placeholder=""/>
                            
                            <span class="text-c4 hotelmsg">可输入当日住宿酒店信息等（必填）</span>
                        </td>
                    </tr>
                    <tr>
                        <th>用餐信息：</th>
                        <td>
                            <div id="dinings">
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="dining" value="1" id="dining1"> 早餐
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="dining" value="2" id="dining2"> 午餐
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="dining" value="3" id="dining3"> 晚餐
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="pd">
                            <div class="pull-right">
                                <input type="hidden" id="tid"/>
                                <input id="savePlanUrl" type="hidden" value="<?php echo U('Tour/planEdit');?>"/>
                                <button id="buttonSavePlan" class="btn btn-warning " type="button">保存</button>
                                <button id="reset" class="btn btn-default " type="button">取消</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script src="/Public/js/plan.js?v=3"></script>
</body>
</html>