<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">财务管理</a>&gt;交易明细</div>
        <div class="cTitle2">
            交易明细
        </div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" id="status" value="<?php echo ($type); ?>">
            <input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status" style="margin-bottom: -10px;">
                <label>&nbsp;&nbsp;&nbsp;&nbsp;类型：</label>
                <a class="nav-audit-state nav-audit-state0 nav-audit-statet1"
                   href="<?php echo U('OperatorFinance/details', array('id' => I('get.id'), 'oid' => I('get.oid'), 'type' => 't1'));?>"
                   style="margin-left:5px;">收支明细
                </a>

                <a class="nav-audit-state nav-audit-statet2"
                   href="<?php echo U('OperatorFinance/details', array('id' => I('get.id'), 'oid' => I('get.oid'), 'type' => 't2'));?>">分润明细
                </a>
            </div>

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('OperatorFinance/details', array('id' => I('get.id'), 'oid' => 0, 'type' => I('get.type')));?>"
                   style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('OperatorFinance/details', array('id' => I('get.id'), 'oid' => $vo['id'], 'type' => I('get.type')));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <?php if(empty($info)): ?><table class="cTable" width="100%">
                <tr>
                    <th><?php echo ($msg); ?></th>
                </tr>
            </table><?php endif; ?>

        <?php if(!empty($info)): ?><div class="main-search">
                <div class="top">
                    <form method="get"
                          action="<?php echo U('OperatorFinance/details',array('id' => I('get.id'), 'oid' => I('get.oid'), 'type' => I('get.type')));?>"
                          class="form-inline">
                        <div class="form-group">
                            <input type="text" name="entity_name" class="form-control" placeholder="请输入交易说明">
                        </div>
                        <div class="form-group ml">
                            <label>时间：</label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                                   value="<?php echo ($startTime); ?>" id="d4322"
                                   onclick="WdatePicker({el: $dp.$('d12')})" style="width:80%">
                        </div>
                        <span class="end">到</span>

                        <div class="form-group">
                            <label class="sr-only"></label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                                   value="<?php echo ($endTime); ?>" id="d4322"
                                   onclick="WdatePicker({el: $dp.$('d12')})">
                        </div>
                        <button type="submit" class="btn btn-warning">查询</button>
                    </form>
                </div>

                <form method="post"
                      action="<?php echo U('GetApplyExcel/getOperatorFinanceExcel',array('id' => I('get.id'), 'oid' => I('get.oid'), 'type' => I('get.type')));?>">
                    <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                    <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                    <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                    <input type="submit" class="btn btn-success" value="导出生成Excel">
                </form>
            </div>
            <table class="cTable table-hover" width="100%">

                <tr>
                    <th width="5%">编号</th>
                    <th width="8%">运营商</th>
                    <th width="8%">订单号</th>
                    <th width="10%">流水号</th>
                    <th width="20%">交易说明</th>
                    <th width="10%">收入金额</th>
                    <?php if(I('get.type') == 't2'): ?><th width="10%">分润比例</th><?php endif; ?>
                    <th width="10%">账户余额</th>
                    <th width="10%">入账时间</th>
                </tr>

                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td><?php echo (getOperatorName($vo["operator_id"])); ?></td>
                        <td><?php echo ($vo["object_id"]); ?></td>
                        <td><?php echo ($vo["sn"]); ?></td>
                        <td><?php echo ($vo["action"]); ?></td>
                        <td id="money-right">
                            ￥<?php echo (GetYuan($vo["amount"])); ?>
                        </td>
                        <?php if(I('get.type') == 't2'): ?><td><?php echo ($vo['profit_rate']/10000); ?>%</td><?php endif; ?>
                        <td>￥<?php echo (GetYuan($vo["balance"])); ?></td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table><?php endif; ?>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>