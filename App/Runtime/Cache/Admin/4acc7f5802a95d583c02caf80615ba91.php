<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">平台账户余额管理</a>&gt;平台账户充值记录</div>
        <div class="cTitle2">平台账户充值记录</div>
        <div class="main-search">
        </div>
        <form method="post" action="">
            <input type="hidden" id="code" value="<?php echo (session('checkApplyCode')); ?>">
            <input id="chkState" type="hidden" value="<?php echo U('Forum/index');?>"/>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width='8%'>编号</th>
                    <th width='20%'>名称</th>
                    <th width='15%'>金额</th>
                    <th width='15%'>充值类型</th>
                    <th width='15%'>状态</th>
                    <th width='15%'>充值时间</th>
                    <th width='15%'>操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($v["id"]); ?></td>
                        <td><?php echo ($v["entity_name"]); ?></td>
                        <td id="money-right" class='text-c5'><dfn>&yen;<?php echo (GetYuan($v["amount"])); ?></dfn></td>
                        <td class="text-c1">
                            <?php if($v['topup_type'] == 1): ?>现金充值
                            <?php elseif($v['topup_type'] == 2): ?>
                                刷卡充值
                            <?php elseif($v['topup_type'] == 3): ?>
                                支票充值
                            <?php elseif($v['topup_type'] == 4): ?>
                                银行转账
                            <?php elseif($v['topup_type'] == 5): ?>
                                支付宝转账<?php endif; ?>
                        </td>
                        <td>
                            <?php if($v["state"] == 1): ?><span style="color:blue">已提交,待审核</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:darkgreen">已审核</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:red">已拒绝</span><?php endif; ?>
                        </td>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <?php if($v['state'] == 1): ?><ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:void(0)"
                                               onclick="changeApplyState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo ($v["operator_id"]); ?>, '<?php echo Common\Top::ApplyStatePass;?>')">已审核</a>
                                        </li>
                                        <li><a href="javascript:void(0)"
                                               onclick="changeRejectState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo ($v["operator_id"]); ?>, '<?php echo Common\Top::ApplyStateReject;?>')">拒绝</a>
                                        </li>
                                    </ul>
                                    <?php else: endif; ?>
                            </div>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script src="/Public/js/credit.js"></script>
</body>
</html>