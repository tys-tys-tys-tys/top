<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">订单管理</a>&gt;门票产品订单</div>
        <div class="cTitle2">门票产品订单</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Order/scenicTicketList', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Order/scenicTicketList', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Order/scenicTicketList',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="编号/代理商/景点名称"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="10%">代理商</th>
                <th width="15%">景点名称</th>
                <th width="5%">门票数量</th>
                <th width="10%">应付金额</th>
                <th width="15%">下单时间</th>
                <th width="10%">凭证</th>
                <th width="15%">订单状态</th>
                <th width="10%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td><?php echo ($vo["agency_name"]); ?></td>
                    <td class="text-c1"><?php echo ($vo["scenic_name"]); ?></td>
                    <td class="text-c5"><?php echo ($vo["client_total"]); ?></td>
                    <td id="money-right" class="text-c5"><?php echo (GetYuan($vo["price_total"])); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td class="text-c1"><?php echo ($vo["proof_type"]); ?> <?php echo ($vo["proof_code"]); ?></td>
                    <td>
                        <?php if($vo["state"] == 1): ?><span style="color:blue">已提交</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <span style="color:darkgreen">已付款</span>
                            <?php elseif($vo["state"] == 4): ?>
                            <span style="color:red">已取消</span>
                            <?php elseif($vo["state"] == 5): ?>
                            <span style="color:red">已退票,待退款</span>
                            <?php elseif($vo["state"] == 9): ?>
                            <span style="color:orangered">已退票,已退款</span><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a style="cursor: pointer" onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 8)">凭证</a></li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="chkState" value="<?php echo U('Order/scenicTicketList');?>"/>
<input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
<input type="hidden" id="type" value="ticket">
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>