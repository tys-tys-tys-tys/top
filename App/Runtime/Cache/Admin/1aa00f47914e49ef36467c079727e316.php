<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">通用产品</a>&gt;通用产品列表</div>
        <div class="cTitle2">通用产品列表</div>
        <div class="main-search" style="margin-bottom: 28px">
            <form class="form-inline">
                <form method="get" value="<?php echo U('Product/index', array('id' => 111));?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="请输入代码/产品名称" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
                <div class="pull-right">
                    <a href="<?php echo U('Product/add',array('id' => 111));?>" class="btn btn-info">新增</a>
                    <input id="chkr" type="hidden" value="<?php echo U('Product/delete');?>"/>
                    <input id="chka" type="hidden" value="<?php echo U('Product/add');?>"/>
                    <input id="chkState" type="hidden" value="<?php echo U('Product/index');?>"/>
                    <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
                </div>
            </form>
        </div>
        <form method="post" action="">
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male"
                                                                    onclick="selectAll(this);"/>&nbsp;<label for="male"
                                                                                                             style="font-weight: bold">全选 </label>
                    </th>
                    <th width='10%'>代码</th>
                    <th width='15%'>产品名称</th>
                    <th width='10%'>同行价</th>
                    <th width='15%'>参数</th>
                    <th width='15%'>产品录入</th>
                    <th width='15%'>产品状态</th>
                    <th width='10%'>创建时间</th>
                    <th width="12%">操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                              class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>
                        <td>20<?php echo ($v["id"]); ?></td>
                        <td class="text-c1"><?php echo ($v["name"]); ?></td>
                        <td class="text-c3"><?php echo (getYuan($v["price_agency"])); ?></td>
                        <td class="text-c1">
                            <?php if($v['para_type'] == 1): ?>保险 <?php echo ($v["para_value"]); endif; ?>
                        </td>
                        <td class="text-c5"><?php echo ($v["user_name"]); ?></td>
                        <td class="text-c1">
                            <?php if($v["state"] == 1): ?><span style="color:#000666">未提交</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:blue">已审核，已上架</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:darkgreen">已提交，待审核</span>
                                <?php elseif($v["state"] == 4): ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                   title="<?php echo ($v["audit_memo"]); ?>"><span style="color:red">审核失败</span></a>
                                <?php elseif($v["state"] == 5): ?>
                                <span style="color:red">已下架</span><?php endif; ?>
                        </td>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateSubmitted;?>)"
                                           style="cursor: pointer;">提交</a></li>
                                    <li><a href="<?php echo U('Product/edit',array('id'=>I('get.id'), 'tid'=>$v['id']));?>">编辑</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>

</body>
</html>