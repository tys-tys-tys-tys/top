<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt; <a
                href="<?php echo U('Provider/index',array('id' => 4));?>">供应商列表</a> &gt;交易记录
        </div>
        <div class="cTitle2"><?php echo ($name); ?> 的交易记录</div>
        <div class="main-search">
            <div class="top">
                <form method="get"
                      action="<?php echo U('Provider/showTransaction',array('id' => I('get.id'),'aid' => I('get.aid'), 'oid' => I('get.oid')));?>"
                      class="form-inline">
                    <div class="form-group">
                        <input type="text" name="entity_name" class="form-control" placeholder="请输入用途" value="<?php echo ($sname); ?>">
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                    <span style="font-size: 16px;margin-left: 20px;"><span
                            style="color:#33b3b8"><?php echo ($total["0"]["totalnum"]); ?></span> 条
                    共<span style="color:#f08519"> <?php echo (getYuan($total["0"]["totalprice"])); ?></span> 元(佣金 <span
                                style="color: #f08519"><?php echo (getYuan($commissionTotal["0"]["commissiontotal"])); ?></span> 元)</span>
                </form>
            </div>
            <form method="post"
                  action="<?php echo U('GetApplyExcel/getProviderTransactionExcel',array('id' => I('get.id'),'aid' => I('get.aid'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($sname); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>

        </div>

        <form method="post" action="">
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="5%">编号</th>
                    <th width="5%">订单号</th>
                    <th width="10%">交易号</th>
                    <th width="10%">明细</th>
                    <th width="10%">用途</th>
                    <th width="5%">金额</th>
                    <th width="5%">余额</th>
                    <th width="10%">备注</th>
                    <th width="10%">操作时间</th>
                </tr>
                <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td><?php echo ($vo["object_id"]); ?></td>
                        <td><?php echo ($vo["sn"]); ?></td>
                        <td><?php echo ($vo["objectName"]["tour_name"]); ?></td>
                        <td class="text-c5"><?php echo ($vo["action"]); ?></td>
                        <td class="text-c1"><?php echo (getYuan($vo["amount"])); ?></td>
                        <td class="text-c5"><?php echo (getYuan($vo["balance"])); ?></td>
                        <td class="text-c5">
                            <?php if($vo['memo'] != ''): echo ($vo["memo"]); ?>
                                <?php else: ?>
                                无<?php endif; ?>
                        </td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                    </tr><?php endforeach; endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>
</body>
</html>