<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">用户/角色</a>&gt;角色列表</div>
        <div class="cTitle2">角色列表</div>
        <div class="main-search">
            <form class="pull-right" style="padding-bottom: 5px;">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-info"
                   onclick="addRole('角色添加')">新增</a>
                <input id="chkr" type="hidden" value="<?php echo U('Role/delete');?>"/>
                <input id="chka" type="hidden" value="<?php echo U('Role/add');?>"/>
                <input id="chke" type="hidden" value="<?php echo U('Role/edit');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold">全选</label></th>
                <th>角色名称</th>
                <th>创建时间</th>
                <th width="13%">操作</th>
            </tr>
            <?php if(is_array($info)): foreach($info as $key=>$v): ?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>
                    <td><?php echo ($v["name"]); ?></td>
                    <td><?php echo ($v["create_time"]); ?></td>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="editRole(<?php echo ($v["id"]); ?>)" style="cursor: pointer">编辑</a></li>
                                <li><a href="<?php echo U('Role/allAuth',array('id'=>I('get.id'),'aid'=>$v['id']));?>">分配权限</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">角色编辑</h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal editRole" style="display: block">
                <div class="form-group" style="color: darkgreen">
                    <label class="col-sm-3 control-label">当前角色名称：</label>
                    <div class="col-sm-3 roleName" style="margin-top: 7px;">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">修改角色名称：</label>

                    <div class="col-sm-4" style="margin-top: 5px;">
                        <input type='text' class='form-control auth_name' placeholder='' name='name' value=''/>
                    </div>
                    <span class="col-sm-5 tp">*必须是填写</span>
                </div>
            </div>

            <div class="form-horizontal addRole" style="display: none">
                <div class="form-group">
                    <label class="col-sm-3 control-label">当前角色名称：</label>

                    <div class="col-sm-4" style="margin-top: 5px;">
                        <input type='text' class='form-control auth_name_add' placeholder='' name='name'/>
                    </div>
                    <span class="col-sm-5 tp">*必须是填写</span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer pull-right" style="border:none">
        <button type="button" class="btn btn-warning" id="buttonSaveRoleInfo">保存</button>
        <button type="button" class="btn btn-default " id="reset">取消</button>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script src="/Public/js/user.js?v=9"></script>
</body>
</html>