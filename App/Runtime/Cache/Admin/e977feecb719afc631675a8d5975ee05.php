<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">订单管理</a>&gt;通用产品订单</div>
        <div class="cTitle2">通用产品订单</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Order/productList', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Order/productList', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Order/productList',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="编号/代理/产品/供应商" value="<?php echo ($name); ?>"/>
                    </div>
                    <span style="margin-left: 50px"> </span>
                    <div class="form-group" style="margin-right: -40px;">
                        <select name="date_type" style="height:35px;line-height: 35px;border: 1px solid #c0c0c0">
                            <?php if($dateType == 2): ?><option value="2">创建时间</option>
                                <?php else: ?>
                                <option value="2">创建时间</option><?php endif; ?>
                        </select>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>

                    <input type="hidden" id="chkState" value="<?php echo U('Order/productList');?>"/>
                    <input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
                    <input type="hidden" id="type" value="product">
                    <input type="hidden" id="showPara" value="<?php echo U('Order/showParaInfo');?>">
                    <input type="hidden" id="addPara" value="<?php echo U('Order/addParaInfo');?>">
                </form>
            </div>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getProductExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="hidden" name="date_type" value="<?php echo ($dateType); ?>">
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="5%">编号</th>
                <th width="10%">代理商</th>
                <th width="10%">产品名称</th>
                <th width="10%">供应商</th>
                <th width="5%">数量</th>
                <th width="8%">同行价</th>
                <th width="8%">成本价</th>
                <th width="10%"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="供应商实际收入">实际收入</a></th>
                <th width="10%">佣金</th>
                <th width="10%">利润</th>
                <th width="10%">下单时间</th>
                <th width="10%">参数</th>
                <th width="10%">凭证</th>
                <th width="10%">状态</th>
                <th width="10%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td><?php echo ($vo["agency_name"]); ?></td>
                    <td class="text-c1"><?php echo ($vo["product_name"]); ?></td>
                    <td><?php echo ($vo["provider_name"]); ?></td>
                    <td><?php echo ($vo["quantity"]); ?></td>
                    <td><?php echo (getYuan($vo['price_agency'])); ?></td>
                    <td><?php echo (getYuan($vo["price_cost"])); ?></td>
                    <td id="money-right" class="text-c5">
                        <?php if($vo['state'] == 6): echo (GetYuan($vo["prepay_amount_commission"])); ?>
                            <?php elseif($vo['state'] == 7): ?>
                            <?php echo (GetYuan($vo["price_payable_commission"])); ?>
                            <?php else: ?>
                            0.00<?php endif; ?>
                    </td>
                    <td class="text-c5">
                        <?php if($vo['state'] == 6): echo (GetYuan($vo["prepay_commission"])); ?>
                            <?php elseif($vo['state'] == 7): ?>
                            <?php echo (GetYuan($vo["total_commission"])); ?>
                            <?php else: ?>
                            0.00<?php endif; ?>
                    </td>
                    <td class="text-c5">
                        <?php if($vo['price_cost'] != 0): echo (getYuan($vo['price_agency'] * $vo['quantity'] - $vo['price_cost'] * $vo['quantity'])); ?>
                        <?php else: ?>
                        0.00<?php endif; ?>
                    </td>
                    <td class="text-c1"><?php echo ($vo["create_time"]); ?></td>
                    <td class="text-c1">
                        <?php if($vo['para_type'] == 1): ?>保险 <?php echo ($vo["para_value"]); ?>
                        <?php else: ?>
                            <?php echo ($vo["para_type"]); ?> <?php echo ($vo["para_value"]); endif; ?>
                    </td>
                    <td class="text-c1"><?php echo ($vo["proof_type"]); ?> <?php echo ($vo["proof_code"]); ?></td>
                    <td>
                        <?php if($vo["state"] == 1): ?><span style="color:red">未提交</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:blue">已提交</span></a>
                            <?php elseif($vo["state"] == 3): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:darkgreen">已确认待付款</span></a>
                            <?php elseif($vo["state"] == 4): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:red">已取消</span></a>
                            <?php elseif($vo["state"] == 5): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:red">已关闭</span></a>
                            <?php elseif($vo["state"] == 6): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:darkgreen">已付预付款</span></a>
                            <?php elseif($vo["state"] == 7): ?>
                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($vo["memo"]); ?>"
                               style="text-decoration: none"><span style="color:blue">已付全款</span></a><?php endif; ?>
                    </td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a style="cursor: pointer" onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 6)">凭证</a></li>
                                <li><a style="cursor: pointer" onclick="showPara(<?php echo ($vo["id"]); ?>)">参数</a></li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalPara" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    参数信息
                </h4>
            </div>
            <div class="modal-body">
                参数类型：
                <input type="text" class="para_type form-control" style="border:1px solid #e5e5e5">
                <p>
                    参数值：
                    <input type="text" class="para_value form-control" style='border:1px solid #e5e5e5'>
                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>