<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">账户余额管理</a>&gt;账户变更记录</div>
        <div class="cTitle2">账户变更记录</div>
        <div class="main-search">
              <div class="top clearfix">
                <form method="get" action="<?php echo U('Forum/details',array('id' => I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <p></p>
              <form method="post" action="<?php echo U('GetApplyExcel/getServiceChargeExcel');?>">
                  <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input type="submit" class="btn btn-success" value="导出机票服务费Excel">
            </form>
        </div>
        <form method="post" action="">
            <input type="hidden" id="code" value="<?php echo (session('checkApplyCode')); ?>">
            <input id="chkState" type="hidden" value="<?php echo U('Credit/index');?>"/>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width='8%'>编号</th>
                    <th width='8%'>顾问</th>
                    <th width='8%'>订单号</th>
                    <th width='15%'>用途</th>
                    <th width='10%'>金额</th>
                    <th width='10%'>余额</th>
                    <th width='10%'>备注</th>
                    <th width='10%'>操作时间</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($v["id"]); ?></td>
                        <td><?php echo ($v["agency_name"]); ?></td>
                        <td><?php echo ($v["object_id"]); ?></td>
                        <td><?php echo ($v["action"]); ?></td>
                        <td id="money-right" class='text-c5'><dfn>&yen;<?php echo (GetYuan($v["amount"])); ?></dfn></td>
                        <td id="money-right" class="text-c1">
                            <dfn>&yen;<?php echo (GetYuan($v["balance"])); ?></dfn>
                        </td>
                        <td class="text-c5">
                            <?php if($vo['memo'] != ''): echo ($vo["memo"]); ?>
                                <?php else: ?>
                                无<?php endif; ?>
                        </td>
                        <td><?php echo ($v["create_time"]); ?></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script src="/Public/js/credit.js"></script>
</body>
</html>