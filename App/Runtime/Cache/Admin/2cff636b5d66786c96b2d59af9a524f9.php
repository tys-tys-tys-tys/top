<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <form class="form-inline" method="get">
        <div class="rightbox pull-right">
            <div class="ur-here">您当前的位置：<a href="#">供应商管理</a>&gt;审核线路产品</div>
            <div class="cTitle2">审核线路产品</div>
            <div class="main-search">
                <form class="form-inline" method="get"
                      action="<?php echo U('Provider/auditTourProduct',array('state'=>I('get.state'),'id'=>I('get.id')));?>">
                    <div class="top" style="margin-bottom:0px;">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="代码/线路/供应商/目的地" name='name' value="<?php echo ($name); ?>" style="width:200px;">
                        </div>
                        <input type="submit" class="btn btn-warning" value='查询'>
                    </div>
                </form>
            </div>
            <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

                <div class="status">
                    <label>运营商：</label>
                    <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-report-state nav-operator-report-state<?php echo ($vo["id"]); ?>"
                           href="<?php echo U('Provider/auditTourProduct', array('id' => I('get.id'), 'oid' => $vo['id'], 'state' => I('get.state')));?>"><?php echo ($vo["display_name"]); ?>
                        </a><?php endforeach; endif; ?>
                </div><?php endif; ?>
            <div class="status">
                <input type="hidden" id="changeAudit" value="<?php echo U('Provider/auditTourProduct');?>">
                <label>状态：</label>
                <a class="navi-audit-state navi-audit-state0"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>0,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">全部
                    <?php if($state == 0): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state3"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>3,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">待审核
                    <?php if($state == 3): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state2"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>2,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">审核通过
                    <?php if($state == 2): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state4"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>4,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">拒绝
                    <?php if($state == 4): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state5"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>5,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">已下架
                    <?php if($state == 5): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="navi-audit-state navi-audit-state6"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>6,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">置顶
                    <?php if($state == 6): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
<!--                <a class="navi-audit-state navi-audit-state7"
                   href="<?php echo U('Provider/auditTourProduct',array('state'=>7,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">跑马灯
                    <?php if($state == 7): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>-->
            </div>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="6%">代码</th>
                    <th width="15%">线路名程</th>
                    <th width="10%">供应商</th>
                    <th width="10%">出发地</th>
                    <th width="8%">目的地</th>
                    <th width="12%">状态</th>
                    <th width="10%">最新团期</th>
                    <th width="10%">成人零售最低价</th>
                    <th width="10%">创建/更新时间</th>
                    <th width="12%">操作</th>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td>10<?php echo ($v["id"]); ?></td>
                        <td width="22%" class="tl" style="padding:2px 0 0 2px"><a
                                href="<?php echo U('Provider/details',array('id'=>I('get.id'),'aid'=>$v['id']));?>"
                                target="_blank" style='color:blue'>
                            <?php if($v['is_top'] == 1): if($v['promotion'] == 1): ?><span class="badge" style="background: red">特</span>&nbsp;<?php endif; ?>
                                <?php if($v['provider_type'] == 1): ?><span class="badge" style="background: red">外</span>&nbsp;<?php endif; ?>
                                <?php echo ($v["name"]); ?>
                                <span class="badge" style="background: red">置顶</span>&nbsp;
<!--                                <?php if($v['is_marquee'] == 1): ?><span class="badge" style="background: red">跑马灯</span><?php endif; ?>-->

                                <?php else: ?>
                                <?php if($v['promotion'] == 1): ?><span class="badge" style="background: red">特</span>&nbsp;<?php endif; ?>
                                <?php if($v['provider_type'] == 1): ?><span class="badge" style="background: red">外</span>&nbsp;<?php endif; ?>
                                <?php echo ($v["name"]); ?>
                                <!--<?php if($v['is_marquee'] == 1): ?><span class="badge" style="background: red">跑马灯</span><?php endif; ?>--><?php endif; ?>
                        </a></td>
                        <td><?php echo ($v["provider_name"]); ?></td>
                        <td><?php echo ($v["departure"]); ?></td>
                        <td><?php echo ($v["destination"]); ?></td>
                        
                        <td class="show_msg">
                            <?php if($v["state"] == 1): ?><span style="color:#000666">未提交</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:blue">已审核，已上架</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:darkgreen">已提交，待审核</span>
                                <?php elseif($v["state"] == 4): ?>
                                <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                   title="<?php echo ($v["audit_memo"]); ?>" style="text-decoration: none"><span
                                        style="color:red">拒绝</span></a>
                                <?php elseif($v["state"] == 5): ?>
                                <span style="color:red">已下架</span><?php endif; ?>
                        </td>
                        <td>
                            <?php if($v["latest_sku1"] < $nowDate): ?><span style='color:red'><?php echo (substr($v["latest_sku"],0,10)); ?></span>
                                <?php else: ?>
                                <span class="text-c1"><?php echo (substr($v["latest_sku"],0,10)); ?></span><?php endif; ?>
                        </td>
                        <td class="text-c5"><dfn>&yen;<?php echo (getYuan($v["min_price"])); ?></dfn></td>
                        <td><?php echo ($v["create_time"]); ?><p><?php echo ($v["update_time"]); ?></p></td>
                        <td>
                            
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                        aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">

                                    <li><a onclick="changeAuditState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateIsSale;?>)"
                                           style="cursor: pointer">已审核</a></li>
                                    <li><a onclick="reject(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateRejected;?>)"
                                           style="cursor: pointer">拒绝</a></li>
                                    <li><a href="<?php echo U('Tour/edit',array('id'=>I('get.id'),'tid'=>$v['id']));?>"
                                           style="cursor: pointer;">编辑</a></li>
                                    <?php if($v['is_top'] == 0): ?><li><a onclick="changeTop(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">推荐置顶</a></li>
                                        <?php else: ?>
                                        <li><a onclick="changeTop(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">取消置顶</a></li><?php endif; ?>
                                    <!--<li><a onclick="marquee(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">跑马灯设置</a></li>-->
                                    <?php if($v['promotion'] == 0): ?><li><a onclick="changePromotion(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">设为特价</a></li>
                                        <?php else: ?>
                                        <li><a onclick="changePromotion(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">取消特价</a></li><?php endif; ?>
                                    <?php if($v['provider_type'] == 0): ?><li><a onclick="changeProviderType(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">转为外部订单</a></li>
                                        <?php else: ?>
                                        <li><a onclick="changeProviderType(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>')" style="cursor: pointer">取消外部订单</a></li><?php endif; ?>
                                    <li>
                                        <a href="http://zhangda.m.lydlr.com/index.php/Home/Agency/details/id/<?php echo ($v["id"]); ?>.html"
                                           target="_blank" style="cursor: pointer">手机版</a></li>
                                </ul>
                            </div>

                        </td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
            <?php if(!empty($info)): ?><div class="pagebox">
                    <?php echo ($page); ?>
                </div><?php endif; ?>
        </div>
    </form>
</div>

<div class="theme-popover">
    <div class="theme-poptit" style="border-bottom: 1px solid #e5e5e5;">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">拒绝理由</h4>
        </div>
        <div class="modal-body" id="modal_reject">
            <textarea class="form-control" name="reason" style="resize: none;" placeholder="请填写拒绝理由"></textarea>
        </div>
        <div class="modal-footer pull-right">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
            <input type="hidden" value="<?php echo U('Provider/auditTourProduct');?>" id="chkState"/>
            <input type="hidden" value="<?php echo U('Provider/topRecommended');?>" id="topRecommended"/>
            <input type="hidden" value="<?php echo U('Provider/marquee');?>" id="marquee"/>
            <!--<input type="hidden" value="<?php echo U('Provider/showMarqueeInfo');?>" id="marqueeInfo"/>-->
            <input type="hidden" value="<?php echo U('Provider/setPromotion');?>" id="promotion"/>
            <input type="hidden" value="<?php echo U('Provider/setOutOrder');?>" id="outOrder"/>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>

<!--<div class="modal fade popWin bs-example-modal-sm" id="myModalSuc" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">跑马灯设置</h4>
            </div>
            <div class="modal-body">
                <p>
                    生效时间:
                    <input type="text" class="ui-datepicker Wdate form-control marquee_start_time" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})" style='border:1px solid #e5e5e5'>
                <p>
                <p>
                    失效时间:
                    <input type="text" class="ui-datepicker Wdate form-control marquee_end_time" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})" style='border:1px solid #e5e5e5'>
                <p>
                <p>
                    是否生效:
                    <input type="checkbox" class="is_marquee" style='border:0px solid #e5e5e5'>
                <p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning doSubmit" data-dismiss="modal" aria-label="Close">确认
                </button>
                <button type="button" class="btn btn-default doReset" data-dismiss="modal" aria-label="Close">取消
                </button>
            </div>
        </div>
    </div>
</div>-->

<input id='state' type='hidden' value='<?php echo ($state); ?>'/>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>