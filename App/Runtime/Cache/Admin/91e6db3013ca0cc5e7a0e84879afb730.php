<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">通用产品</a>&gt;已下架产品</div>
        <div class="cTitle2">已下架产品</div>
        <div class="main-search">
            <div class="top clearfix">
                <form class="form-inline" method="get" value="<?php echo U('Product/soldout', array('id' => 121));?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="请输入代码/产品名称" value="<?php echo ($name); ?>"/>
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                </form>
            </div>
            <form class="pull-right" style="padding-bottom: 5px;">
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
                <input type="hidden" id="chkr" value="<?php echo U('Product/delete');?>"/>
                <input id="chkState" type="hidden" value="<?php echo U('Product/index');?>"/>
            </form>
        </div>
        <table id='subBoxOne' class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold">全选 </label></th>
                <th width='10%'>代码</th>
                <th width='15%'>产品名称</th>
                <th width='15%'>产品描述</th>
                <th width='10%'>同行价</th>
                <th width='15%'>产品录入</th>
                <th width='15%'>产品状态</th>
                <th width='10%'>创建时间</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo (setEncrypt($v["id"])); ?>"/></td>

                    <td>20<?php echo ($v["id"]); ?></td>
                    <td class='text-c1'><?php echo ($v["name"]); ?></td>
                    <td class='text-c2'><?php echo ($v["description"]); ?></td>
                    <td class='text-c3'><?php echo (getYuan($v["price_agency"])); ?></td>
                    <td class='text-c5'><?php echo ($v["user_name"]); ?></td>
                    <td class="show_msg text-c1">
                        <?php if($v["state"] == 1): ?><a style="color:#000666">未提交</a>
                            <?php elseif($v["state"] == 2): ?>
                            <a style="color:blue">已审核，已上架</a>
                            <?php elseif($v["state"] == 3): ?>
                            <a style="color:darkgreen">已提交，待审核</a>
                            <?php elseif($v["state"] == 4): ?>
                            <a style="color:red">审核失败</a> &nbsp;|&nbsp;<a id="infos"
                                                                          style="color:blue;cursor: pointer;">查看原因</a>
                            <?php elseif($v["state"] == 5): ?>
                            <a style="color:red">已下架</a><?php endif; ?>
                    </td>
                    <td><?php echo ($v["create_time"]); ?></td>
                    <td>
                        <div class="btn-group pull-right">
                            <button type="button" style="padding:3px 7px" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown" aria-expanded="false">
                                操作 <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="margin: 0px;background: #eee">
                                <?php if(($_SESSION['user_id']== $v['user_id']) or ($_SESSION['is_admin']== 1)): ?><li><a onclick="changeState(<?php echo ($v["id"]); ?>, <?php echo Common\Top::ProductStateIsSale;?>)"
                                           style="cursor: pointer;padding:0px 14px;">上架</a></li>
                                    <?php else: ?>
                                    <li><a onclick="friendRemind(<?php echo ($v["id"]); ?>)"
                                           style="cursor: pointer;padding:0px 14px;">上架</a></li><?php endif; ?>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>