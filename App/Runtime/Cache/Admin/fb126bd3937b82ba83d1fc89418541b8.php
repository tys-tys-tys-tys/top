<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：订单管理&gt;<a href="<?php echo U('Order/planeTicketList',array('id'=>I('get.id')));?>">机票产品订单</a>&gt;订单详情
        </div>
        <div class="cTitle2">订单详情</div>
        <div class="orderDetail">
            <h5><b>&nbsp;航程信息</b></h5>
            <table width="100%" class="table table-bordered table-hover">
                <input type="hidden" id="refundFlightTicket" value="<?php echo U('Order/refundFlightTicket');?>">
                <tr>
                    <th>编号</th>
                    <th>航班日期</th>
                    <th>起降时间</th>
                    <th>航程信息</th>
                    <th>航班信息</th>
                    <th>票面价</th>
                    <th>结算价</th>
                    <th>基建费</th>
                    <th>燃油税</th>
                </tr>
                <tr>
                    <td class='text-c5'><?php echo ($list["0"]["order_flight_id"]); ?></td>
                    <td class='text-c5'><?php echo ($list["0"]["dep_date"]); ?></td>
                    <td class='text-c1'><?php echo (substr($list["0"]["dep_time"],0,2)); ?>:<?php echo (substr($list["0"]["dep_time"],2,2)); ?> -
                        <?php echo (substr($list["0"]["arr_time"],0,2)); ?>:<?php echo (substr($list["0"]["arr_time"],2,2)); ?>
                    </td>
                    <td class='text-c5'><?php echo (codeToAirport($list["0"]["dep_code"])); ?> <?php echo ($list["0"]["dep_code"]); ?> -
                        <?php echo (codeToAirport($list["0"]["arr_code"])); ?> <?php echo ($list["0"]["arr_code"]); ?>
                    </td>
                    <td class='text-c1'><?php echo (nameToAirLine(substr($list["0"]["flight_no"],0,2))); ?> <?php echo ($list["0"]["flight_no"]); ?></td>
                    <td class='text-c5'><?php echo (getYuan($list["0"]["par_price"])); ?></td>
                    <td class='text-c5'><?php echo (getYuan($list["0"]["settle_price"])); ?></td>
                    <td class='text-c5'><?php echo (getYuan($list["0"]["airport_tax"])); ?></td>
                    <td class='text-c5'><?php echo (getYuan($list["0"]["fuel_tax"])); ?></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <h5><b>&nbsp;乘客信息</b></h5>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width="5%">编号</th>
                    <th width="8%">乘客姓名</th>
                    <th width="8%">证件类型</th>
                    <th width="10%">证件号码</th>
                    <th width="8%">乘客类型</th>
                    <th width="10%">票号</th>
                    <th width="8%">票面价</th>
                    <th width="8%">结算价</th>
                    <!--<th width="8%">基建费</th>-->
                    <!--<th width="8%">燃油税</th>-->
                    <th width="8%">退款时间</th>
                    <th width="8%">退款金额</th>
                    <th width="12%">状态</th>
                    <th width="10%">操作</th>
                </tr>
                <?php if(is_array($list)): foreach($list as $key=>$vo): ?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td class="text-c1"><?php echo ($vo["passenger_name"]); ?></td>
                        <td class="text-c5">
                            <?php if($vo['identity_type'] == 1): ?>身份证
                                <?php elseif($vo['identity_type'] == 2): ?>
                                护照
                                <?php elseif($vo['identity_type'] == 3): ?>
                                军官证
                                <?php elseif($vo['identity_type'] == 4): ?>
                                士兵证
                                <?php elseif($vo['identity_type'] == 5): ?>
                                台胞证
                                <?php else: ?>
                                其他<?php endif; ?>
                        </td>
                        <td class="text-c1"><?php echo ($vo["identity_no"]); ?></td>
                        <td class='text-c5'>
                            <?php if($vo['passenger_type'] == 0): ?>成人
                                <?php else: ?>
                                儿童<?php endif; ?>
                        </td>
                        <td><?php echo ($vo["ticket_no"]); ?></td>
                        <td><?php echo (getYuan($vo["par_price"])); ?> + <?php echo (getYuan($vo["airport_tax"])); ?> + <?php echo (getYuan($vo["fuel_tax"])); ?></td>
                        <td><?php echo (getYuan($vo["settle_price"])); ?> + <?php echo (getYuan($vo["airport_tax"])); ?> + <?php echo (getYuan($vo["fuel_tax"])); ?></td>
                        <!--<td><?php echo (getYuan($vo["airport_tax"])); ?></td>-->
                        <!--<td><?php echo (getYuan($vo["fuel_tax"])); ?></td>-->
                        <td><?php echo ($vo["refund_time"]); ?></td>
                        <td><?php echo (getYuan($vo["refund_fee"])); ?></td>
                        <td>
                            <?php if($vo["state"] == 1): ?><span style="color:red">未提交</span>
                                <?php elseif($vo["state"] == 2): ?>
                                <span style="color:blue">已付款</span>
                                <?php elseif($vo["state"] == 3): ?>
                                <span style="color:darkgreen">已出票</span>
                                <?php elseif($vo["state"] == 4): ?>
                                <span style="color:red">已取消</span>
                                <?php elseif($vo["state"] == 5): ?>
                                <span style="color:darkgreen">已退票,退款成功</span>
                                <?php elseif($vo["state"] == 6): ?>
                                <span style="color:red">出票失败</span>
                                <?php elseif($vo["state"] == 7): ?>
                                <span style="color:blue">预定失败</span>
                                <?php elseif($vo["state"] == 8): ?>
                                <span style="color:blue">退票失败</span>
                                <?php elseif($vo["state"] == 9): ?>
                                <span style="color:darkgreen">退票中,待退款</span>
                                <?php elseif($vo["state"] == 10): ?>
                                <span style="color:darkgreen">有取消</span>
                                <?php elseif($vo["state"] == 11): ?>
                                <span style="color:red">有退票</span>
                                <?php elseif($vo["state"] == 12): ?>
                                <span style="color:red">已删除</span>
                                <?php elseif($vo["state"] == 14): ?>
                                <span style="color:#f08519">已预订,待付款</span>
                                <?php elseif($vo["state"] == 15): ?>
                                <span style="color:#f08519">已预订,付款中</span><?php endif; ?>
                        </td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <?php if(($_SESSION['role_id']== 1)): ?><li>
                                        <a href="javascript:void(0)"
                                           onclick="refundFlightTicket(<?php echo ($vo["id"]); ?>, <?php echo ($vo["order_flight_id"]); ?>)">已退票,退票成功</a>
                                    </li><?php endif; ?>
                                </ul>
                            </div>
                        </td>
                    </tr><?php endforeach; endif; ?>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    填写退款金额
                </h4>
            </div>
            <div class="modal-body">
                退款金额：
                <input type="text" class="refund_fee form-control" style="border:1px solid #e5e5e5">
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitRefundFlightTicket">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script>
    $(function () {
        //审核产品切换
        $('.order-Tb').not(".order-Tb:first").hide();
        $('.top a:first').addClass('current');
        $('.top a').click(function () {
            var id = $(this).index();
            $('.top a').removeClass('current');
            $(this).addClass('current');
            $('.order-Tb').hide();
            $('.order-Tb').eq(id).show();
        });
    })
</script>
</body>
</html>