<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href=''>财务管理</a>&gt;<?php echo ($operatorInfo["name"]); ?>下的交易明细</div>
        <div class="cTitle2"><?php echo ($operatorInfo["name"]); ?>下的交易明细</div>
        <?php if(empty($info)): ?><table class="cTable" width="100%">
                <tr>
                    <th><?php echo ($msg); ?></th>
                </tr>
            </table><?php endif; ?>
        <?php if(!empty($info)): ?><div class="main-search">
                <div class="top">
                    <form method="get" action="<?php echo U('ProviderFinance/details',array('id'=>I('get.id'), 'oid' => I('get.oid')));?>" class="form-inline">
                        <div class="form-group">
                            <input type="text" class="form-control" name="names" placeholder="请输入资金用途" value="<?php echo ($name); ?>"/>
                        </div>
                        <div class="form-group ml">
                            <label>时间：</label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="start_time" placeholder="开始日期"
                                   value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                                   style="width:80%">
                        </div>
                        <span class="end">到</span>

                        <div class="form-group">
                            <label class="sr-only"></label>
                            <input type="text" class="form-control ui-datepicker Wdate" name="end_time" placeholder="结束日期"
                                   value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                        </div>
                        <button type="submit" class="btn btn-warning">查询</button>
                    </form>
                </div>
            </div>
            <table class="cTable table-hover" width="100%">

                <tr>
                    <th>编号</th>
                    <th>流水号</th>
                    <th>资金用途</th>
                    <th>运营商</th>
                    <th>提交时间</th>
                    <th>收入</th>
                    <th>支出</th>
                    <th>余额</th>
                </tr>

                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($vo["id"]); ?></td>
                        <td><?php echo ($vo["sn"]); ?></td>
                        <td><?php echo ($vo["action"]); ?></td>
                        <td><?php echo (GetOperator($vo["operator_id"])); ?></td>
                        <td><?php echo ($vo["create_time"]); ?></td>
                        <td id="money-right">
                            <?php if($vo["amount"] > 0): ?>￥<?php echo (GetYuan($vo["amount"])); endif; ?>
                        </td>
                        <td id="money-right">
                            <?php if($vo["amount"] < 0): ?>￥<?php echo (GetYuan($vo["amount"])); endif; ?>
                        </td>
                        <td id="money-right">￥<?php echo (GetYuan($vo["balance"])); ?></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table><?php endif; ?>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<?php echo W('Template/bottom');?>
</body>
</html>