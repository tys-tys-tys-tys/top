<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">用户/权限管理</a>&gt;权限列表</div>
        <div class="cTitle2">权限列表</div>
        <div class="main-search">
            <form class="pull-right" style="padding-bottom: 5px;">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn btn-info"
                   onclick="addAuth()">新增</a>
                <input id="chkr" type="hidden" value="<?php echo U('Auth/delete');?>"/>
                <input id="chka" type="hidden" value="<?php echo U('Auth/add');?>"/>
                <input id="chke" type="hidden" value="<?php echo U('Auth/edit');?>"/>
                <button type="button" class="btn btn-danger" onclick="jqchk()">删除</button>
            </form>
        </div>
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%" style="padding-left:5px;"><input type="checkbox" id="male" onclick="selectAll(this);"/>&nbsp;<label
                        for="male" style="font-weight: bold">全选</label></th>
                <th width="20%">权限名称</th>
                <th width="15%">控制器</th>
                <th width="15%">方法</th>
                <th width="15%">说明</th>
                <th width="10%">路径</th>
                <th width="5%">层级</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                    <td style="padding-left:10px;text-align: left"><input name="subBox" type="checkbox"
                                                                          class="checkitem" value="<?php echo ($v["id"]); ?>"/></td>
                    <td style="text-align:left;"><?php echo ($v['name']); ?></td>
                    <td><?php echo ($v["controller"]); ?></td>
                    <td><?php echo ($v["action"]); ?></td>
                    <td><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="<?php echo ($v["remark"]); ?>"><?php echo (substr($v["remark"],0,24)); ?></a></td>
                    <td><?php echo ($v["path"]); ?></td>
                    <td><?php echo ($v["level"]); ?></td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                操作<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a onclick="editAuth(<?php echo ($v["id"]); ?>, '权限编辑')" style="cursor: pointer">编辑</a></li>
                            </ul>
                        </div>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>
<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">权限名称：</label>

                    <div class="col-sm-7">
                        <input type="text" id="auth_name" class="form-control" placeholder="" name="name"/>
                    </div>
                    <span class="col-sm-3 tp">*必须是填写（中文）</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">上级分类：</label>

                    <div class="col-sm-7">
                        <select class="form-control" name="parent_id" id="parent">
                            <option value="0">请选择</option>
                            <?php if(is_array($authInfo)): $i = 0; $__LIST__ = $authInfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">控制器：</label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="controller" placeholder="" id="controller"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">方法：</label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="method" placeholder="" id="method"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">说明：</label>

                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="remark" placeholder="" id="remark"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">所属角色：</label>

                    <div class="col-sm-7">
                        <select class="form-control" id="entity_type">
                            <option value="0">请选择</option>
                            <option value="<?php echo Common\Top::EntityTypeSystem;?>">系统/运营商</option>
                            <option value="<?php echo Common\Top::EntityTypeProvider;?>">供应商</option>
                        </select>
                    </div>
                    <span class="col-sm-3 tp">*所属角色必须选择</span>
                </div>
            </div>
        </div>
        <div class="modal-footer pull-right">
            <button type="button" class="btn btn-warning" id="buttonSaveAuthInfo">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>
<?php echo W('Template/bottom');?>
<script src="/Public/js/auth.js?v=2"></script>
<script>
    $(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>
</body>
</html>