<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">运营商账户余额管理</a>&gt;运营商账户充值记录</div>
        <div class="cTitle2">运营商账户充值记录</div>
        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Credit/index', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Credit/index', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>
        <form method="post" action="">
            <input type="hidden" id="code" value="<?php echo (session('checkApplyCode')); ?>">
            <input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
            <input id="invoiceList" type="hidden" value="<?php echo U('Credit/updateProof');?>"/>
            <input id="chkState" type="hidden" value="<?php echo U('Credit/index');?>"/>
            <table class="cTable table-hover" width="100%">
                <tr>
                    <th width='8%'>编号</th>
                    <th width='18%'>名称</th>
                    <th width='10%'>金额</th>
                    <th width='10%'>充值类型</th>
                    <th width='15%'>状态</th>
                    <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><th width='12%'>凭证</th><?php endif; ?>
                    <th width='15%'>充值时间</th>
                    <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><th width='12%'>操作</th><?php endif; ?>
                </tr>
                <?php if(is_array($info)): $i = 0; $__LIST__ = $info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td><?php echo ($v["id"]); ?></td>
                        <td><?php echo ($v["entity_name"]); ?></td>
                        <td id="money-right" class='text-c5'><dfn>&yen;<?php echo (GetYuan($v["amount"])); ?></dfn></td>
                        <td class="text-c1">
                            <?php if($v['topup_type'] == 1): ?>现金充值
                                <?php elseif($v['topup_type'] == 2): ?>
                                刷卡充值
                                <?php elseif($v['topup_type'] == 3): ?>
                                支票充值
                                <?php elseif($v['topup_type'] == 4): ?>
                                银行转账
                                <?php elseif($v['topup_type'] == 5): ?>
                                支付宝转账<?php endif; ?>
                        </td>
                        <td>
                            <?php if($v["state"] == 1): ?><span style="color:blue">已提交,待审核</span>
                                <?php elseif($v["state"] == 2): ?>
                                <span style="color:darkgreen">已审核</span>
                                <?php elseif($v["state"] == 3): ?>
                                <span style="color:red">已拒绝</span><?php endif; ?>
                        </td>
                        <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><td class="text-c1"><?php echo ($v["proof_type"]); ?> <?php echo ($v["proof_code"]); ?></td><?php endif; ?>
                        <td><?php echo ($v["create_time"]); ?></td>
                        <?php if(($_SESSION['role_id']== 1) or ($_SESSION['role_id']== 185)): ?><td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false">
                                        操作<span class="caret"></span>
                                    </button>
                                    <?php if($v['state'] == 1): ?><ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:void(0)"
                                                   onclick="changeApplyState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>',<?php echo ($v["operator_id"]); ?>, '<?php echo Common\Top::ApplyStatePass;?>')">已审核</a>
                                            </li>
                                            <li><a href="javascript:void(0)"
                                                   onclick="changeRejectState(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>',  <?php echo ($v["operator_id"]); ?>, '<?php echo Common\Top::ApplyStateReject;?>')">拒绝</a>
                                            </li>
                                        </ul>
                                        <?php else: ?>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:void(0)"
                                                   onclick="updateProof(<?php echo ($v["id"]); ?>, '<?php echo (setEncrypt($v["id"])); ?>', <?php echo ($v["state"]); ?>, 5)">更新凭证</a>
                                            </li>
                                        </ul><?php endif; ?>
                                </div>
                            </td><?php endif; ?>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </table>
        </form>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    填写凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo W('Template/bottom');?>
<script src="/Public/js/credit.js?v=1"></script>
</body>
</html>