<?php if (!defined('THINK_PATH')) exit(); echo W('Template/top');?>
<div class="wrapper clearfix content" style="position: relative;border: 1px solid #ddd">
    <?php echo W('Template/left');?>
    <div class="rightbox pull-right">
        <div class="ur-here">您当前的位置：<a href="#">代理商管理</a>&gt;提现申请</div>
        <div class="cTitle2">提现申请</div>

        <?php if(in_array(($_SESSION['role_id']), is_array($operatorData)?$operatorData:explode(',',$operatorData))): ?><input type="hidden" class="operatorId" value="<?php echo ($oid); ?>">

            <div class="status">
                <label>运营商：</label>
                <a class="nav-operator-state nav-operator-state0"
                   href="<?php echo U('Agency/withdrawList', array('id' => I('get.id'), 'oid' => 0));?>" style="margin-left:5px;">全部
                </a>
                <?php if(is_array($operatorList)): foreach($operatorList as $key=>$vo): ?><a class="nav-operator-state nav-operator-state<?php echo ($vo["id"]); ?>"
                       href="<?php echo U('Agency/withdrawList', array('id' => I('get.id'), 'oid' => $vo['id']));?>"><?php echo ($vo["display_name"]); ?>
                    </a><?php endforeach; endif; ?>
            </div><?php endif; ?>

        <div class="main-search">
            <div class="top clearfix">
                <form method="get" action="<?php echo U('Agency/withdrawList',array('id'=>I('get.id')));?>"
                      class="form-inline">
                    <input name="state" type='hidden' value='<?php echo ($state); ?>'/>

                    <div class="form-group">
                        <input type="text" class="form-control" name="names" placeholder="请输入代理商姓名/金额" value="<?php echo ($name); ?>"/>
                    </div>
                    <div class="form-group ml">
                        <label>时间：</label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="start_time"
                               placeholder="开始日期"
                               value="<?php echo ($startTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})"
                               style="width:80%">
                    </div>
                    <span class="end">到</span>

                    <div class="form-group">
                        <label class="sr-only"></label>
                        <input type="text" class="form-control ui-datepicker Wdate" name="end_time"
                               placeholder="结束日期"
                               value="<?php echo ($endTime); ?>" id="d4322" onclick="WdatePicker({el: $dp.$('d12')})">
                    </div>
                    <button type="submit" class="btn btn-warning">查询</button>
                    <span style="font-size: 16px;" class='text-c1'>(查询金额时，请以“分”为单位查询)</span>
                </form>
            </div>
            <p></p>

            <form method="post" action="<?php echo U('GetApplyExcel/getAgencyWithDrawExcel',array('id' => I('get.id'), 'oid' => I('get.oid')));?>">
                <input type="hidden" name="sname" value="<?php echo ($name); ?>">
                <input type="hidden" name="start_time" value="<?php echo ($startTime); ?>">
                <input type="hidden" name="end_time" value="<?php echo ($endTime); ?>">
                <input id="status" name="state" type='hidden' value='<?php echo ($state); ?>'/>
                <input type="submit" class="btn btn-success" value="导出生成Excel">
            </form>
            <div class="status">
                <label>状态：</label>
                <a class="nav-audit-state nav-audit-state0"
                   href="<?php echo U('Agency/withdrawList',array('state'=>0,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">全部
                    <?php if($state == 0): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="nav-audit-state nav-audit-state1"
                   href="<?php echo U('Agency/withdrawList',array('state'=>1,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">待审核
                    <?php if($state == 1): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="nav-audit-state nav-audit-state2"
                   href="<?php echo U('Agency/withdrawList',array('state'=>2,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">待财务处理
                    <?php if($state == 2): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="nav-audit-state nav-audit-state4"
                   href="<?php echo U('Agency/withdrawList',array('state'=>4,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">处理完成
                    <?php if($state == 4): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
                <a class="nav-audit-state nav-audit-state3"
                   href="<?php echo U('Agency/withdrawList',array('state'=>3,'id'=>I('get.id'), 'oid' => I('get.oid')));?>">拒绝
                    <?php if($state == 3): ?>(<?php echo ($total); ?>)<?php endif; ?>
                </a>
            </div>
        </div>

        <input id="chkState" type="hidden" value="<?php echo U('Agency/agencyAuditWithDraw');?>"/>
        <input id="financeAuditState" type="hidden" value="<?php echo U('Agency/financeAuditWithDraw');?>"/>
        <input id="chkStatePass" type="hidden" value="<?php echo U('Agency/financeAuditWithDraw');?>"/>
        <input id="invoiceList" type="hidden" value="<?php echo U('Agency/updateWithDrawProof');?>">
        <input id="showReject" type="hidden" value="<?php echo U('Agency/showWithdrawReject');?>"/>
        <input id="showAccountBalance" type="hidden" value="<?php echo U('Agency/showAccountBalance');?>"/>
        <input type="hidden" id="showProof" value="<?php echo U('Agency/showProofInfo');?>">
        <input type="hidden" id="code" value="<?php echo (session('withDrawCode')); ?>">
        <table class="cTable table-hover" width="100%">
            <tr>
                <th width="8%">编号</th>
                <th width="8%">运营商</th>
                <th width="10%">代理商</th>
                <th width="10%">提现金额</th>
                <th width="10%">帐户总额</th>
                <th width="10%">提现帐户</th>
                <th width="10%">申请时间</th>
                <th width="10%">审核时间</th>
                <th width="10%">凭证</th>
                <th width="10%">状态</th>
                <th width="12%">操作</th>
            </tr>
            <?php if(is_array($info)): foreach($info as $key=>$vo): ?><tr>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td><?php echo (getOperatorName($vo["operator_id"])); ?></td>
                    <td><?php echo ($vo["entity_name"]); ?></td>
                    <td id="money-right" class="text-c1 amount"><?php echo (GetYuan($vo["amount"])); ?></td>
                    <td>
                        <a href="javascript:void(0)" onclick="showAccountBalance(<?php echo ($vo["entity_id"]); ?>, <?php echo ($vo["id"]); ?>)"
                           class="text-c5 showAccountBalance<?php echo ($vo["id"]); ?>">查看</a>
                        <span style="display: none" class="showBalance<?php echo ($vo["id"]); ?>"></span>
                    </td>
                    <td><?php echo ($vo["bank_name"]); ?> <p><?php echo ($vo["bank_account"]); ?></td>
                    <td><?php echo ($vo["create_time"]); ?></td>
                    <td><?php echo ($vo["update_time"]); ?></td>
                    <td class="text-c1"><?php echo ($vo["proof_type"]); ?> <?php echo ($vo["proof_code"]); ?></td>
                    <td class="text-c5">
                        <?php if($vo["state"] == 1): ?><span style="color:saddlebrown">待审核</span>
                            <?php elseif($vo["state"] == 2): ?>
                            <span class="text-c6">已确认，待财务处理</span>
                            <?php elseif($vo["state"] == 4): ?>
                            <span class="text-c7">处理完成</span>
                            <?php else: ?>
                                <span class="text-c8">
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                       title="<?php echo ($vo["memo"]); ?>" style="color:red">拒绝</a>
                                </span><?php endif; ?>
                    </td>
                    <td>
                        <?php if($vo['state'] == 1): ?><div class="btn-group" id="state">
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="javascript:void(0)"
                                           onclick="changeWithdrawPassState(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo Common\Top::ApplyStatePass;?>, <?php echo (getAgencyState($vo["entity_id"])); ?>, <?php echo (getAgencyAccountState($vo["entity_id"])); ?>)">已确认，待财务处理</a>
                                    </li>
                                    <li><a href="javascript:void(0)"
                                           onclick="rejectWithdrawState(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["amount"]); ?>)">拒绝</a>
                                    </li>
                                </ul>
                            </div>

                            <?php elseif($vo['state'] == 3): ?>
                            <div class="btn-group" id="state">
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                            </div>

                            <?php elseif($vo['state'] == 4): ?>
                            <div class="btn-group" id="state">
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="javascript:void(0)"
                                           onclick="updateProof(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["state"]); ?>, 2)">更新凭证</a>
                                    </li>
                                </ul>
                            </div>

                            <?php elseif($vo['state'] == 2): ?>
                            <div class="btn-group" id="state">
                                <button type="button" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    操作<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="javascript:void(0)"
                                           onclick="changeWithdrawPass(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo Common\Top::ApplyStateFinish;?>, <?php echo (getAgencyState($vo["entity_id"])); ?>, <?php echo (getAgencyAccountState($vo["entity_id"])); ?>)">处理完成</a>
                                    </li>
                                    <li><a href="javascript:void(0)"
                                           onclick="financeRejectWithdrawState(<?php echo ($vo["id"]); ?>, '<?php echo (setEncrypt($vo["id"])); ?>', <?php echo ($vo["amount"]); ?>)">拒绝</a>
                                    </li>
                                </ul>
                            </div><?php endif; ?>
                    </td>
                </tr><?php endforeach; endif; ?>
        </table>
        <?php if(!empty($info)): ?><div class="pagebox">
                <?php echo ($page); ?>
            </div><?php endif; ?>
    </div>
</div>

<div class="theme-popover">
    <div class="theme-poptit">
        <a title="关闭" class="closes">&times;</a>

        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">拒绝理由</h4>
        </div>
        <div class="modal-body" id="modal_reject">
            <textarea class="form-control" name="reason" style="resize: none;" placeholder="请填写拒绝理由"></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-warning" id="button">确定</button>
            <button type="button" class="btn btn-default " id="reset">取消</button>
        </div>
    </div>
</div>
<div class="theme-popover-mask"></div>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    填写凭证信息
                </h4>
            </div>
            <div class="modal-body">
                凭证类型：
                <input type="text" class="proof_type form-control" style="border:1px solid #e5e5e5">

                <p>
                    凭证号码：
                    <input type="text" class="proof_code form-control" style='border:1px solid #e5e5e5'>

                <p>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary doSubmitTopup">
                        确定
                    </button>
                    <button type="button" class="btn btn-default cancel"
                            data-dismiss="modal">关闭
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo W('Template/bottom');?>
<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>