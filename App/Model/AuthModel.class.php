<?php

namespace Model;

use Think\Model;

class AuthModel extends Model {

    public function AuthAdd() {

        $addInfo = array(
            'name' => I('name'),
            'parent_id' => I('parent'),
            'controller' => I('controller'),
            'action' => I('action'),
            'entity_type' => I('entity_type'),
            'remark' => I('remark'),
            'create_time' => date("Y-m-d H:i:s", time()),
        );
        $add_id = $this->add($addInfo);

        if (I('post.parent') == 0) {
            $path = $add_id;
        } else {
            $info = $this->find(I('post.parent'));
            $path = $info['path'] . '-' . $add_id;
        }
        $level = count(explode('-', $path)) - 1;

        $data = array(
            'id' => $add_id,
            'path' => $path,
            'level' => $level,
        );
        return $this->save($data);
    }

    public function update() {
            if (I('post.parent') == 0) {
                $path = I('post.tid');
            } else {
                $auth_infos = M('auth')->find(I('post.parent'));
                $path = $auth_infos['path'] . '-' . I('post.tid');
            }
        $level = count(explode('-', $path)) - 1;
        $data = array(
            'name' => I('name'),
            'parent_id' => I('parent'),
            'controller' => I('controller'),
            'action' => I('action'),
            'entity_type' => I('entity_type'),
            'path' => $path,
            'level' => $level,
            'remark' => I('remark'),
        );
        return $this->where('id=' . I('post.tid'))->setField($data);
    }

}

?>
