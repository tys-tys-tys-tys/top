<?php

namespace Model;

use \Think\Model;
use Common\Utils;
use Common\Top;

class ProviderModel extends Model {
    /**
     * 查看产品
     */
    public function checkAudit($id) {

        $provider_info = M('tour')->find($id);
        $infoPics = explode('@@@', $provider_info['pics']);
        
        foreach ($infoPics as $k => $v) {
            if (!empty($v)) {
                $infoImgAll[] = explode('|', $v);
            }
        }

        $provider_info['tour_plan'] = M('tour_plan')->where('tour_id=' . $id)->select();
        $provider_info['tour_sku'] = M('tour_sku')->where('tour_id=' . $id)->order("start_time desc")->select();
        $provider_info['imgs'] = $infoImgAll;
        return $provider_info;
    }

}

?>