<?php

namespace Home\Controller;

use Common\Utils;
use Think\Controller;

class PromptPageController extends Controller
{

    public function index()
    {
        $this->display();
    }


    public function agencyList()
    {
        $type = I('type');

        if ($type === 'beijing') {
            $type = '北京市';
        } else if ($type === 'tianjin') {
            $type = '天津市';
        } else if ($type === 'shijiazhuang') {
            $type = '石家庄市';
        } else if ($type === 'tangshan') {
            $type = '唐山市';
        }

        $where = "province like '$type%%'";

        if (empty($type)) {
            $where = "1 = 1";
        }

        $list = Utils::pages('agency_join', $where, C('PAGE_SET'), $order = " id desc ");
        $this->info = $list['info'];
        $this->page = $list['page'];
        $this->display();
    }

}
