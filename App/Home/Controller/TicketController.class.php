<?php

namespace Home\Controller;

use Common\Soap;
use Common\Top;
use Common\Utils;
use Common\STD3Des;

class TicketController extends CommonController
{
    public function index()
    {
        $page = I('page');
        $regioncode = $this->nameToCity(I('regionCode'));

        if (empty($regioncode)) {
            $regioncode = '110100';
        }

        if ($page == '') {
            $page = 1;
        }

        $html = $this->scenicListGetPageHtml($page, $regioncode);

        if (IS_POST && IS_AJAX) {
            $this->ajaxReturn($html);
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $domain = "http://" . $domain . "/index.php/Home/Ticket/index.html";
            $this->assign('domain', $domain);

            $headPic = "http://zhangda.m.lydlr.com/Public/images/ticket.jpg";
            $this->assign("headPic", $headPic);

            $this->assign('html', $html);
        }

        $this->display();
    }


    /**
     * 首页 - 加载更多
     */
    protected function scenicListGetPageHtml($page, $regioncode)
    {
        $pageSize = C('PRODUCT_PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = M('ticket')
            ->where("state = 4 && code = '{$regioncode}'")
            ->field("id,name,supplier_key_id,code,sell_price,settle_price,imgs,state")
            ->order('id desc')
            ->group('supplier_key_id')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $k => $v) {

            if (strpos($v['imgs'], ',')) {
                $imgs = explode(",", $v['imgs']);
                $v['imgs'] = $imgs[0];
            }

            if (!empty(session('isLogin'))) {
                $v['settle_price'] = Utils::getYuan($v['settle_price']) . " 起";
            } else {
                $v['settle_price'] = Utils::getYuan($v['sell_price']) . " 起";
            }

            if ($v['state'] == '4') {
                if ($v['imgs'] == '') {
                    $coverPic = "<img class='lazy' data-original='http://www.piao58.cn/Upload/supplierDefaultPic.png'/>";
                } else {
                    $coverPic = "<img class='lazy img-responsive' data-original='" . $v['imgs'] . "' style='min-height:76px;min-width:120px;'/>";
                }

                $evaldata .= "<dl class='item clearfix' style='border-bottom: 1px dashed #d8d9d9;'>";
                $evaldata .= "<dt><a href=" . U('Ticket/details', array('id' => $v['supplier_key_id'])) . ">" . $coverPic . "</a></dt>";
                $evaldata .= "<dd>";
                $evaldata .= "<a href=" . U('Ticket/details', array('id' => $v['supplier_key_id'])) . ">";
                $evaldata .= "<p class='name'>" . $v['name'] . "</p>";
                $evaldata .= "<p class='bt'><span class='price'><dfn>&yen;" . $v['settle_price'] . "</dfn></span></p>";
                $evaldata .= "</a>";
                $evaldata .= "</dd>";
                $evaldata .= "</dl>";
            }
        }

        return $evaldata;
    }


    /**
     * 景区搜索
     */
    public function indexSearch()
    {
        if (IS_POST && IS_AJAX) {
            $name = I('name');
            $code = $this->nameToCity(I('code'));

            $list = M('ticket')->where("name like '%%$name%%' && state = 4 && code = '{$code}'")->field("id,name,supplier_key_id,code,sell_price,settle_price,imgs,state")->group("supplier_key_id")->select();

            if (empty($list)) {
                $evaldata = "暂无可售门票";
            }

            foreach ($list as $k => $v) {

                if (strpos($v['imgs'], ',')) {
                    $imgs = explode(",", $v['imgs']);
                    $v['imgs'] = $imgs[0];
                }

                if (!empty(session('isLogin'))) {
                    $v['settle_price'] = Utils::getYuan($v['settle_price']) . " 起";
                } else {
                    $v['settle_price'] = Utils::getYuan($v['sell_price']) . " 起";
                }

                if ($v['state'] == '4') {
                    if ($v['imgs'] == '') {
                        $coverPic = "<img class='lazy' data-original='http://www.piao58.cn/Upload/supplierDefaultPic.png'/>";
                    } else {
                        $coverPic = "<img class='lazy img-responsive' data-original='" . $v['imgs'] . "' style='min-height:76px;min-width:120px;'/>";
                    }

                    $evaldata .= "<dl class='item clearfix' style='border-bottom: 1px dashed #d8d9d9;'>";
                    $evaldata .= "<dt><a href=" . U('Ticket/details', array('id' => $v['supplier_key_id'])) . ">" . $coverPic . "</a></dt>";
                    $evaldata .= "<dd>";
                    $evaldata .= "<a href=" . U('Ticket/details', array('id' => $v['supplier_key_id'])) . ">";
                    $evaldata .= "<p class='name'>" . $v['name'] . "</p>";
                    $evaldata .= "<p class='bt'><span class='price'><dfn>&yen;" . $v['settle_price'] . "</dfn></span></p>";
                    $evaldata .= "</a>";
                    $evaldata .= "</dd>";
                    $evaldata .= "</dl>";
                }
            }
            $this->ajaxReturn($evaldata);
        }
    }


    /**
     * 详情页
     */
    public function details()
    {
        $id = I('get.id');

        $productInfo = M('ticket')->where("supplier_key_id = '{$id}'")->select();

        if (isset($productInfo[0])) {
            $ticketList = $this->my_sort($productInfo, sell_price);
        } else {
            $ticketList[0] = $productInfo;
        }

        $domain = $_SERVER['SERVER_NAME'];
        $domain = "http://" . $domain . "/index.php/Home/Ticket/details/id/{$id}.html";
        $this->assign('domain', $domain);

        $headPic = $ticketList[0]['imgs'];
        $this->assign('headPic', $headPic);

        $this->assign('ticketList', $ticketList);
        $this->display();
    }


    /**
     * 服务内容
     */
    public function showFeeInclude()
    {
        $where['id'] = I('get.id');
        $info = M('ticket')->field('id,fee_include')->where($where)->find();
        $this->assign('info', $info);
        $this->display();
    }


    /**
     * 订单页
     */
    public function showTicketInfo()
    {
        $_SESSION['ticketOrderCode'] = 400;
        $where['id'] = I('get.id');
        $info = M('ticket')->where($where)->find();
        $this->assign('info', $info);
        $this->display();
    }


    /**
     * 创建订单
     */
    public function ticketOrderCreate()
    {
        if (IS_POST) {
            $postCode = I('ticketOrderCode');
            $code = session('ticketOrderCode');
            session('ticketOrderCode', null);

            if ($postCode != $code) {
                return;
            }

            $agencyInfo = $this->userInfo;
            $count = I('ticket_num');

            if (empty($count)) {
                $count = 1;
            }

            $where['id'] = I('id');
            $info = M('ticket')->where($where)->field('id,imgs,key_id,product_name,settle_price,sell_price,market_price,can_refund')->find();

            $settlePrice = $info['settle_price'];
            $orderData['agency_id'] = $agencyInfo['id'];
            $orderData['agency_name'] = $agencyInfo['name'];
            $orderData['operator_id'] = $agencyInfo['operator_id'];
            $orderData['product_id'] = $info['key_id'];
            $orderData['scenic_name'] = $info['product_name'];
            $orderData['client_total'] = $count;
            $orderData['payment_state'] = 0;
            $orderData['trip_date'] = I('start_time');
            $orderData['can_refund'] = $info['can_refund'];
            $orderData['mobile'] = I('contact_phone');
            $orderData['cover_pic'] = $info['imgs'];
            $orderData['price_total'] = $settlePrice * $count;
            $orderData['settle_price'] = $settlePrice;
            $orderData['state'] = Top::TicketOrderStateSubmitted;
            $orderData['create_time'] = date("Y-m-d H:i:s", time());

            M('order_ticket')->startTrans();
            try {
                $result = M('order_ticket')->add($orderData);

                $dataList = I('post.');

                for ($i = 1; $i <= $count; $i++) {
                    $real_name = real_name . $i;
                    $identity_no = identity_no . $i;
                    $identity_type = identity_type . $i;
                    $data['agency_id'] = $agencyInfo['id'];
                    $data['agency_name'] = $agencyInfo['name'];
                    $data['operator_id'] = $agencyInfo['operator_id'];
                    $data['scenic_name'] = $info['product_name'];
                    $data['product_id'] = $info['key_id'];
                    $data['settle_price'] = $info['settle_price'];
                    $data['sell_price'] = $info['sell_price'];
                    $data['market_price'] = $info['market_price'];
                    $data['quantity'] = I('ticket_num');
                    $data['trip_date'] = I('start_time');
                    $data['contact_person'] = I('contact_person');
                    $data['contact_phone'] = I('contact_phone');
                    $data['identity_type'] = $dataList[$identity_type];
                    $data['identity_no'] = $dataList[$identity_no];

                    if (!empty($data['identity_no']) && (strlen($data['identity_no']) < 15 || (strlen($data['identity_no']) > 15 && strlen($data['identity_no']) != 18))) {
                        echo '您输入的身份证位数不对, 请重新输入';
                        exit();
                    }

                    $data['real_name'] = $dataList[$real_name];
                    $data['memo'] = I('memo');
                    $data['order_ticket_id'] = $result;
                    $data['state'] = Top::TicketOrderStateSubmitted;
                    $data['create_time'] = date("Y-m-d H:i:s", time());
                    $ticketDetailResult = M('order_ticket_detail')->add($data);
                }

                $ticketDetails = M('order_ticket_detail')->where("order_ticket_id = '{$result}'")->field('id,real_name,identity_no')->select();

                if (!$ticketDetails[0]) {
                    $ticketDetails[0] = $ticketDetails;
                }

                if (!empty($ticketDetails[0]['real_name'])) {
                    $realName['ticket_names'] = implode(";", array_column($ticketDetails, 'real_name'));
                    $realName['identity_nos'] = implode(";", array_column($ticketDetails, 'identity_no'));
                    M('order_ticket')->where("id = '{$result}'")->save($realName);
                }

                if ($result && $ticketDetailResult) {
                    $orderResult = $this->addOrder($result);

                    if (!empty($orderResult['Head']['StatusCode'])) {
                        echo $orderResult['Head']['Message'];
                        exit();
                    }
                }

                M('order_ticket')->commit();
                $orderId['order_key_id'] = $orderResult;
                M('order_ticket')->where("id = '{$result}'")->save($orderId);
                $this->redirect("Ticket/orderDetail", array("id" => $result));
            } catch (Exception $ex) {
                M('order_ticket')->rollback();
            }
        }
    }


    public function orderDetail()
    {
        $_SESSION['ticketCode'] = 400;
        $where['id'] = I('get.id');
        $info = M('order_ticket')->where($where)->find();
        $this->assign('ticketInfo', $info);
        $this->display();
    }


    /**
     * 向票工厂创建订单
     */
    public function addOrder($id)
    {
        $url = $this->ticketUrl() . 'Agent_AddOrder';
        $_pid = $this->ticketPid();
        $_ts = time();

        $ticketInfo = M('order_ticket')->where("id = '{$id}'")->find();

        $productversionkeyid = $ticketInfo['product_id'];
        $tripdate = substr($ticketInfo['trip_date'], 0, 10);
        $mobilenumbertogeteticket = $ticketInfo['mobile'];
        $totalticketquantity = $ticketInfo['client_total'];
        $orderkeyid = $ticketInfo['id'];
        $productversionprice = (float)Utils::getYuan($ticketInfo['settle_price']);
        $sfzh = $ticketInfo['identity_nos'];
        $name = $ticketInfo['ticket_names'];

        if (!empty($ticketInfo['ticket_names']) && !empty($ticketInfo['identity_nos'])) {
            $_sign = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&name=$name&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&sfzh=$sfzh&totalticketquantity=$totalticketquantity&tripdate=$tripdate" . $this->secretKey();
            $_sign = md5($_sign);
            $data1 = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&name=$name&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&sfzh=$sfzh&totalticketquantity=$totalticketquantity&tripdate=$tripdate&_sign=$_sign";
        } else if (!empty($ticketInfo['ticket_names'])) {
            $_sign = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&name=$name&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&totalticketquantity=$totalticketquantity&tripdate=$tripdate" . $this->secretKey();
            $_sign = md5($_sign);
            $data1 = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&name=$name&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&totalticketquantity=$totalticketquantity&tripdate=$tripdate&_sign=$_sign";
        } else {
            $_sign = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&totalticketquantity=$totalticketquantity&tripdate=$tripdate" . $this->secretKey();
            $_sign = md5($_sign);
            $data1 = "_pid=$_pid&_ts=$_ts&format=xml&mobilenumbertogeteticket=$mobilenumbertogeteticket&orderkeyid=$orderkeyid&productversionkeyid=$productversionkeyid&productversionprice=$productversionprice&totalticketquantity=$totalticketquantity&tripdate=$tripdate&_sign=$_sign";
        }


        $result = Utils::vpost($url, $data1);

        $list = $this->xml_to_array($result);

        if ($list['Head']['StatusCode'] != '100') {
            return $list;
        } else {
            $bodyContent = implode('', $list['Body']);

            $list = $this->des($bodyContent);
            $list = $list['OrderId'][0];
            return $list;
        }
    }


    /**
     * 支付
     */
    public function orderPay()
    {
        if (IS_POST && IS_AJAX) {
            $agencyInfo = $this->userInfo;
            $postCode = I('code');
            $code = session('ticketCode');

            if ($agencyInfo['account_state'] != '1') {
                $data['status'] = 6;
                $this->ajaxReturn($data);
            }

            if ($code == $postCode) {
                $id = I('id');
                $keyid = Utils::getDecrypt(I('keyid'));

                if ($keyid != $id) {
                    $data['status'] = 0;
                    $data['msg'] = '支付失败';
                    $this->ajaxReturn($data);
                }

                $ticketInfo = M('order_ticket')->where("id = '{$id}'")->find();

                $agencyId = $agencyInfo['id'];
                $availableBalance = Utils::getAgencyAvailableFen($agencyId);
                $ticketPriceTotal = $ticketInfo['price_total'];

                $operatorId = $agencyInfo['operator_id'];
                $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
                $creditBalance = $operatorInfo['account_credit_balance'];
                $operatorState = $operatorInfo['state'];

                if ($operatorState != 1) {
                    $data['status'] = 0;
                    $data['msg'] = '运营商被禁用';
                    $this->ajaxReturn($data);
                }

                $sysInfo = M('sys')->where("id = 1")->find();
                $sysTicketBalcne = $sysInfo['account_ticket_balance'];

                if ($sysTicketBalcne < $ticketPriceTotal) {
                    $data['status'] = 0;
                    $data['msg'] = '平台余额不足';
                    $this->ajaxReturn($data);
                }

                if ($creditBalance >= $ticketPriceTotal) {

                    if ($availableBalance >= $ticketPriceTotal && $ticketInfo['state'] == Top::TicketOrderStateSubmitted) {
                        session('ticketCode', null);

                        $payResult = $this->forumPay($ticketInfo['order_key_id']);

                        $statusCode = $payResult['Head']['StatusCode'];

                        if ($statusCode !== '100') {
                            $msg = $payResult['Head']['Message'];
                            $data['status'] = 0;
                            $data['msg'] = $msg;
                            $this->ajaxReturn($data);
                        } else {
                            M('operator')->startTrans();
                            try {
                                $agencyResult = M('agency')->where("id = '{$agencyId}'")->setDec('account_balance', $ticketPriceTotal);

                                $agencyNewInfo = M('agency')->where("id = '{$agencyId}'")->find();
                                $agencyData['operator_id'] = $agencyInfo['operator_id'];
                                $agencyData['sn'] = Utils::getSn();
                                $agencyData['account'] = $agencyInfo['account'];
                                $agencyData['entity_type'] = Top::EntityTypeAgency;
                                $agencyData['entity_id'] = $agencyInfo['id'];
                                $agencyData['action'] = '支出门票产品金额';
                                $agencyData['amount'] = $ticketPriceTotal;
                                $agencyData['balance'] = $agencyNewInfo['account_balance'];
                                $agencyData['remark'] = "top_order_ticket.id:" . $ticketInfo['id'];
                                $agencyData['object_id'] = $ticketInfo['id'];
                                $agencyData['object_type'] = Top::ObjectTypeTicket;
                                $agencyData['object_name'] = '门票';
                                $agencyData['create_time'] = date("Y-m-d H:i:s", time());
                                $agencyTransactionResult = M('account_transaction')->add($agencyData);

                                $operatorResult = M('operator')->where("id = '{$operatorId}'")->setDec('account_credit_balance', $ticketPriceTotal);

                                $operatorList = M('operator')->where("id = '{$operatorId}'")->find();
                                $operatorData['operator_id'] = $operatorId;
                                $operatorData['action'] = '支出门票订单金额';
                                $operatorData['entity_type'] = Top::EntityTypeOperator;
                                $operatorData['entity_id'] = $operatorId;
                                $operatorData['account'] = $operatorList['account'];
                                $operatorData['sn'] = Utils::getSn();
                                $operatorData['amount'] = $ticketPriceTotal;
                                $operatorData['balance'] = $operatorList['account_credit_balance'];
                                $operatorData['create_time'] = date('Y-m-d H:i:s', time());
                                $operatorData['remark'] = 'top_order_ticket.id:' . $ticketInfo['id'];
                                $operatorData['object_id'] = $ticketInfo['id'];
                                $operatorData['object_type'] = Top::ObjectTypeTicket;
                                $operatorData['object_name'] = '门票';
                                $operatorDelmoney = M('operator_credit_transaction')->add($operatorData);

                                $sysResult = M('sys')->where("id = 1")->setDec('account_ticket_balance', $ticketPriceTotal);

                                $sysList = M('sys')->where("id = 1")->find();
                                $sysData['operator_id'] = 0;
                                $sysData['action'] = '支出门票订单金额';
                                $sysData['entity_type'] = Top::EntityTypeSystem;
                                $sysData['entity_id'] = 1;
                                $sysData['account'] = $sysList['account'];
                                $sysData['sn'] = Utils::getSn();
                                $sysData['amount'] = $ticketPriceTotal;
                                $sysData['balance'] = $sysList['account_ticket_balance'];
                                $sysData['create_time'] = date('Y-m-d H:i:s', time());
                                $sysData['remark'] = 'top_order_ticket.id:' . $ticketInfo['id'];
                                $sysData['object_id'] = $ticketInfo['id'];
                                $sysData['object_type'] = Top::ObjectTypeTicket;
                                $sysData['object_name'] = '门票';
                                $sysDelmoney = M('ticket_transaction')->add($sysData);

                                //订单状态改变
                                $ticketId = $ticketInfo['id'];
                                $ticketData['payment_state'] = Top::paymentStateAlreadyPayed;
                                $ticketData['state'] = Top::TicketOrderStatePayed;
                                $state['state'] = Top::TicketOrderStatePayed;
                                $ticketResult = M('order_ticket')->where("id = '{$ticketId}'")->save($ticketData);
                                $ticketDetailResult = M('order_ticket_detail')->where("order_ticket_id = '{$ticketId}'")->save($state);

                                if ($agencyResult && $agencyTransactionResult && $operatorResult && $operatorDelmoney && $ticketResult && $ticketDetailResult && $sysResult && $sysDelmoney) {
                                    M('operator')->commit();
                                    $data['status'] = 1;
                                    $data['msg'] = '支付成功';
                                    $this->ajaxReturn($data);
                                }
                            } catch (Exception $ex) {
                                M('operator')->rollback();
                            }
                        }
                    } else {
                        $data['status'] = 2;
                        $data['msg'] = '账户可用余额不足';
                        $this->ajaxReturn($data);
                    }
                } else {
                    $data['status'] = 0;
                    $data['msg'] = $operatorInfo['msg1'];
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 10;
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 平台支付
     */
    private function forumPay($id)
    {
        $url = $this->ticketUrl() . 'Agent_OrderPay';
        $_pid = $this->ticketPid();
        $_ts = time();
        $orderkeyid = $id;
        $paytype = '64';
        $_sign = "_pid=$_pid&_ts=$_ts&format=xml&orderkeyid=$orderkeyid&paytype=$paytype" . $this->secretKey();
        $_sign = md5($_sign);
        $data = "_pid=$_pid&_ts=$_ts&format=xml&orderkeyid=$orderkeyid&paytype=$paytype&_sign=$_sign";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        return $list;
    }


    /**
     * 电子票验票推送
     */
    public function ticketCheckPush()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');
            $content = urldecode(substr($fileContent, 4));
            $listAllData = $this->xml_to_array($content);
            $listData = $listAllData['Body'][0];
            $list = $this->des($listData);

            $data['service_id'] = $list['EticketServiceId'][0];
            $data['checked_id'] = $list['CheckedId'][0];
            $data['checked_date'] = $list['CheckedDate'][0];
            $data['checked_num'] = $list['CheckedNumber'][0];
            $data['ticket_num'] = $list['EticketNum'][0];
            $data['ticket_used_num'] = $list['EticketUsedNum'][0];
            $data['ticket_withdraw_num'] = $list['EticketWithDrawNum'][0];
            $data['partner_order_id'] = $list['OrderKeyId'][0];
            $data['product_version_key_id'] = $list['ProductVersionKeyId'][0];
            $where['id'] = $list['PartnerOrderId'][0];
            M('order_ticket')->where($where)->save($data);

            $d = date("Ymd", time());
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "/ticketCheckPush_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $fileContent);
            flock($fp, LOCK_UN);
            fclose($fp);
        }

        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "eticketchecked";
        $secretKey = "fdd37a18324da335cd6b47ed";

        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=$sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);

        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketCheckPush', 'utf-8', 'text/xml');
    }


    /**
     * 电子票过期推送
     */
    public function ticketExpirePush()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');
            $d = date("Ymd", time());
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "ticketExpirePush_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $fileContent);
            flock($fp, LOCK_UN);
            fclose($fp);
        }
        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "eticketexpired";
        $secretKey = "fdd37a18324da335cd6b47ed";
        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=.
        $sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);
        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketExpirePush', 'utf-8', 'text/xml');
    }

    /**
     * 订单电子码推送
     */
    public function ticketOrderPush()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');
            $content = urldecode(substr($fileContent, 4));
            $listAllData = $this->xml_to_array($content);
            $listData = $listAllData['Body'][0];
            $list = $this->des($listData);

            $data['partner_order_id'] = $list['PartnerOrderId'][0];
            $data['ticket_code'] = $list['ListETicketCode']['string'][0];

            $where['id'] = $list['PartnerOrderId'][0];
            M('order_ticket')->where($where)->save($data);

            $d = date("Ymd", time());
            $content = "推送时间:" . date("Y-m-d H:i:s") . "\n";
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "/ticketOrderPush_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $content . $fileContent . "\n");
            flock($fp, LOCK_UN);
            fclose($fp);
        }

        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "eticketcreated";
        $secretKey = "fdd37a18324da335cd6b47ed";
        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=$sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);
        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketOrderPush', 'utf-8', 'text/xml');
    }


    /**
     * 景区修改推送
     */
    public function ticketSecnicStateUpdate()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');
            $d = date("Ymd", time());
            $content = "推送时间:" . date("Y-m-d H:i:s") . "\n";
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "SecnicStateUpdate_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $content . $fileContent . "\n");
            flock($fp, LOCK_UN);
            fclose($fp);
        }
        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "secnicstateupdate";
        $secretKey = "fdd37a18324da335cd6b47ed";
        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=$sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);
        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketSecnicStateUpdate', 'utf-8', 'text/xml');
    }

    /**
     * 产品修改推送
     */
    public function ticketProductPush()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');
            $d = date("Ymd", time());
            $content = "推送时间:" . date("Y-m-d H:i:s") . "\n";
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "ticketProductPush_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $content . $fileContent . "\n");
            flock($fp, LOCK_UN);
            fclose($fp);
        }
        //编号
        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "productstatechange";
        $secretKey = "fdd37a18324da335cd6b47ed";
        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=.$sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);
        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketProductPush', 'utf-8', 'text/xml');
    }


    /**
     * 退票结果推送
     */
    public function ticketRefundPush()
    {
        if (IS_POST) {
            $fileContent = file_get_contents('php://input');

            $d = date("Ymd", time());
            $content = "推送时间:" . date("Y-m-d H:i:s") . "\n";
            //$file = "./Public/notify_$d.log";
            $file = C('VISITOR_WXPAY_NOTIFY_PATH') . "/ticketRefundPush_$d.log";
            $fp = fopen($file, "a+");
            flock($fp, LOCK_EX);
            fwrite($fp, $content . $fileContent . "\n");
            flock($fp, LOCK_UN);
            fclose($fp);

            $content = urldecode(substr($fileContent, 4));
            $listAllData = $this->xml_to_array($content);
            $listData = $listAllData['Body'][0];
            $list = $this->des($listData);

            $refund_passed = $list['IsPassed'][0];
            $product_version_key_id = $list['ProductVersionKeyId'][0];
            $refund_audit_time = $list['AuditDate'][0];
            $refund_num = $list['RefundNum'][0];

            $order_key_id = $list['OrderKeyId'][0];
            $result = M('order_ticket')->execute("update top_order_ticket set refund_passed = '{$refund_passed}',product_version_key_id = '{$product_version_key_id}',refund_num = '{$refund_num}',refund_audit_time = '{$refund_audit_time}' where order_key_id = '{$order_key_id}'");

            if ($list['IsPassed'][0] == 'true') {
                M('sys')->startTrans();
                try {
                    $orderInfo = M('order_ticket')->where("order_key_id = '{$order_key_id}'")->find();

                    $dataState['state'] = Top::TicketOrderStateRefunded;
                    $orderResult = M('order_ticket')->where("order_key_id = '{$order_key_id}'")->save($dataState);

                    $settlePrice = $orderInfo['settle_price'];
                    $refundTotalPrice = $settlePrice * (int)$list['RefundNum'][0];
                    $agencyId = $orderInfo['agency_id'];

                    $agencyResult = M('agency')->where("id = '{$agencyId}'")->setInc('account_balance', $refundTotalPrice);
                    $agencyInfo = M('agency')->where("id = '{$agencyId}'")->find();
                    $agencyData['operator_id'] = $agencyInfo['operator_id'];
                    $agencyData['sn'] = Utils::getSn();
                    $agencyData['account'] = $agencyInfo['account'];
                    $agencyData['entity_type'] = Top::EntityTypeAgency;
                    $agencyData['entity_id'] = $agencyId;
                    $agencyData['action'] = '门票退款';
                    $agencyData['amount'] = $refundTotalPrice;
                    $agencyData['balance'] = $agencyInfo['account_balance'];
                    $agencyData['remark'] = 'top_order_ticket.id:' . $orderInfo['id'];
                    $agencyData['object_id'] = $orderInfo['id'];
                    $agencyData['object_type'] = Top::ObjectTypeTicket;
                    $agencyData['object_name'] = '门票';
                    $agencyData['create_time'] = date("Y-m-d H:i:s", time());
                    $agencyIncResult = M('account_transaction')->add($agencyData);

                    $operatorId = $orderInfo['operator_id'];
                    $operatorResult = M('operator')->where("id = '{$operatorId}'")->setInc('account_credit_balance', $refundTotalPrice);
                    $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
                    $operatorData['operator_id'] = $operatorId;
                    $operatorData['sn'] = Utils::getSn();
                    $operatorData['account'] = $operatorInfo['account'];
                    $operatorData['entity_type'] = Top::EntityTypeOperator;
                    $operatorData['entity_id'] = $operatorId;
                    $operatorData['action'] = '门票退款';
                    $operatorData['amount'] = $refundTotalPrice;
                    $operatorData['balance'] = $operatorInfo['account_credit_balance'];
                    $operatorData['remark'] = 'top_order_ticket.id:' . $orderInfo['id'];
                    $operatorData['object_id'] = $orderInfo['id'];
                    $operatorData['object_type'] = Top::ObjectTypeTicket;
                    $operatorData['object_name'] = '门票';
                    $operatorData['create_time'] = date("Y-m-d H:i:s", time());
                    $operatorIncResult = M('operator_credit_transaction')->add($operatorData);

                    $sysResult = M('sys')->where("id = 1")->setInc('account_ticket_balance', $refundTotalPrice);
                    $sysInfo = M('sys')->where("id = 1")->find();
                    $sysData['operator_id'] = 0;
                    $sysData['sn'] = Utils::getSn();
                    $sysData['account'] = $sysInfo['account'];
                    $sysData['entity_type'] = Top::EntityTypeSystem;
                    $sysData['entity_id'] = $sysInfo['id'];
                    $sysData['action'] = '门票退款';
                    $sysData['amount'] = $refundTotalPrice;
                    $sysData['balance'] = $sysInfo['account_ticket_balance'];
                    $sysData['remark'] = 'top_order_ticket.id:' . $orderInfo['id'];
                    $sysData['object_id'] = $orderInfo['id'];
                    $sysData['object_type'] = Top::ObjectTypeTicket;
                    $sysData['object_name'] = '门票';
                    $sysData['create_time'] = date("Y-m-d H:i:s", time());
                    $sysIncResult = M('ticket_transaction')->add($sysData);

                    if ($result && $orderResult && $agencyResult && $agencyIncResult && $operatorResult && $operatorIncResult && $sysResult && $sysIncResult) {
                        M('sys')->commit();
                    }
                } catch (Exception $ex) {
                    M('sys')->rollback();
                }
            }


        }
        $_pid = "207539";
        $version = "2.0";
        $sequenceId = time();
        $message = "refundorder";
        $secretKey = "fdd37a18324da335cd6b47ed";
        $_sign = strtolower("Message=$message&RequestCode=$_pid&SequenceId=$sequenceId&Version=$version" . $secretKey);
        $sign = md5($_sign);
        $returnXmlInfo = "<?xml version='1.0'?>
                        <Response>
                            <Head>
                                <StatusCode>100</StatusCode>
                                <RequestCode>" . $_pid . "</RequestCode>
                                <SequenceId>" . $sequenceId . "</SequenceId>
                                <Signed>" . $sign . "</Signed>
                                <Message>" . $message . "</Message>
                                <Version>2.0</Version>
                            </Head>
                        </Response>";
        $this->assign('returnXmlInfo', $returnXmlInfo);
        $this->display('ticketRefundPush', 'utf-8', 'text/xml');
    }

    /**
     * 我的门票
     */
    public function myScenicTicket()
    {
        $isLogin = session('isLogin');

        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->scenicTicketListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    /**
     * [scenicTicketListGetPageHtml 我的门票 加载更多]
     */
    protected function scenicTicketListGetPageHtml($page)
    {
        $M = M();
        $agencyInfo = $this->userInfo;
        $agencyId = $agencyInfo['id'];

        $pageSize = C('PRODUCT_PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_order_ticket_detail as d,top_order_ticket as f')
            ->where("f.product_id = d.product_id && f.id = d.order_ticket_id && d.agency_id = '{$agencyId}'")
            ->field(array(
                'f.id' => 'id',
                'd.scenic_name' => 'scenic_name',
                'd.trip_date' => 'trip_date',
                'f.ticket_names' => 'ticket_names',
                'f.create_time' => 'create_time',
                'f.price_total' => 'price_total',
                'f.client_total' => 'client_total',
                'f.state' => 'state',
                'f.ticket_code' => 'ticket_code',
                'f.can_refund' => 'can_refund',
            ))
            ->order('f.id desc')
            ->group('d.order_ticket_id')
            ->limit($index, $pageSize)
            ->select();


        foreach ($list as $key => $vo) {
            $vo['ticket_names'] = str_replace(",", " ", $vo['ticket_names']);
            $vo['trip_date'] = substr($vo['trip_date'], 0, 10);
            $vo['price_total'] = Utils::getYuan($vo['price_total']);

            if ($vo['state'] == 1) {
                $vo['state'] = '已提交,待付款';
                $refund = "<a href=" . U('Ticket/orderCancel', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-danger' style='float:right;margin-top:-6px;' value='订单取消'>
                    </a>


                    <a href=" . U('Ticket/orderDetail', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right;margin-top:-6px;margin-right:10px;' value='立即付款'>
                    </a>";
            } else if ($vo['state'] == 2 && $vo['can_refund'] == 'true') {
                $vo['state'] = '支付成功';
                $refund = "<a href=" . U('Ticket/refundApplication', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right;margin-top:-6px;' value='申请退票'>
                    </a>";
            } else if ($vo['state'] == 9) {
                $vo['state'] = '退票成功,待退款';
                $refund = '';
            } else if ($vo['state'] == 5) {
                $vo['state'] = '退票成功,已退款';
                $refund = '';
            } else if ($vo['state'] == 4) {
                $vo['state'] = '已取消';
                $refund = '';
            }

            $evaldata .= "<ul class='tradeList' style='height:100%'>";

            if (!empty($refund)) {
                $evaldata .= "<li style='padding-bottom:40px;'>";
            } else {
                $evaldata .= "<li>";
            }
            $evaldata .= "<p class='price'><b>" . $vo['scenic_name'] . "</b></p>";
            $evaldata .= "<p class='price'>出游时间：<span class='flight-text-a5'>" . $vo['trip_date'] . "</span></p>";
            $evaldata .= "<p class='price'>出游人数：<span class='flight-text-a5'>" . $vo['client_total'] . "人</span></p>";
            $evaldata .= "<p class='price'>订单时间：<span class='flight-text-a5'>" . $vo['create_time'] . "</span><i class='icon-ticket'></i></p>";
            $evaldata .= "<p class='price'>订单金额：<dfn>&yen; " . $vo['price_total'] . "</dfn></p>";
            $evaldata .= "<p class='price'>电子编码：<span class='flight-text-a5'>" . $vo['ticket_code'] . "</span></p>";
            $evaldata .= "<p class='price'>订单状态：<dfn>" . $vo['state'] . "</dfn></p>";

            if (!empty($refund)) {
                $evaldata .= "<p class='price' style='margin-top:10px;'> " . $refund . "</p>";
            }

            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }
        return $evaldata;
    }


    /**
     * 订单取消
     */
    public function orderCancel()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id');
            $keyid = Utils::getDecrypt(I('keyid'));

            if ($id != $keyid) {
                $data['status'] = 0;
                $data['msg'] = '取消失败';
                $this->ajaxReturn($data);
            }

            $info = M('order_ticket')->where("id = '{$id}'")->find();

            $url = $this->ticketUrl() . 'Agent_OrderCancel';
            $_pid = $this->ticketPid();
            $_ts = time();
            $orderkeyid = $info['order_key_id'];

            $_sign = "_pid=$_pid&_ts=$_ts&format=xml&orderkeyid=$orderkeyid" . $this->secretKey();
            $_sign = md5($_sign);
            $data = "_pid=$_pid&_ts=$_ts&format=xml&orderkeyid=$orderkeyid&_sign=$_sign";
            $result = Utils::vpost($url, $data);

            if ($result == '500001') {
                $data1['status'] = 0;
                $data1['msg'] = '取消失败';
                $this->ajaxReturn($data1);
            }

            $list = $this->xml_to_array($result);

            $status = $list['Head']['Result'];
            $msg = $list['Head']['Message'];

            if ($status == 'false') {
                $data1['status'] = 0;
                $data1['msg'] = $msg;
                $this->ajaxReturn($data1);
            }

            $dataState['state'] = Top::TicketOrderStateCanceled;
            $result = M('order_ticket')->where("id = '{$id}'")->save($dataState);

            if ($result) {
                $data1['status'] = 1;
                $data1['msg'] = '取消成功';
                $this->ajaxReturn($data1);
            }

        } else {
            $where['id'] = I('id');
            $info = M('order_ticket')->where($where)->field('id,price_total,trip_date,client_total,cover_pic,scenic_name')->find();
            $this->assign('ticketInfo', $info);
            $this->display();
        }
    }


    /**
     * 退票申请
     */
    public function refundApplication()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id');
            $ticketInfo = M('order_ticket')->where("id = '{$id}'")->find();

            $url = $this->ticketUrl() . 'Agent_OrderRefund';
            $_pid = $this->ticketPid();
            $_ts = time();

            $orderkeyid = $ticketInfo['order_key_id'];
            $parnterid = $ticketInfo['id'];
            $productversionkeyid = $ticketInfo['product_id'];
            $num = I('num');

            $_sign = "_pid=$_pid&_ts=$_ts&format=xml&num=$num&orderkeyid=$orderkeyid&parnterid=$parnterid&productversionkeyid=$productversionkeyid" . $this->secretKey();
            $_sign = md5($_sign);
            $data = "_pid=$_pid&_ts=$_ts&format=xml&num=$num&orderkeyid=$orderkeyid&parnterid=$parnterid&productversionkeyid=$productversionkeyid&_sign=$_sign";
            $result = Utils::vpost($url, $data);

            $list = $this->xml_to_array($result);

            $status = $list['Head']['Result'];
            $msg = $list['Head']['Message'];

            if ($status == 'false') {
                $data1['status'] = 0;
                $data1['msg'] = $msg;
                $this->ajaxReturn($data1);
            }

            $ticketStatus['state'] = Top::TicketOrderStateRefundWaited;
            M('order_ticket')->where("id = '{$id}'")->save($ticketStatus);
            M('order_ticket_detail')->where("order_ticket_id = '{$id}'")->save($ticketStatus);

            $data1['status'] = 1;
            $data1['msg'] = $msg;
            $this->ajaxReturn($data1);
        } else {
            $id = I('get.id');
            $ticketInfo = M('order_ticket')->where("id = '{$id}'")->find();
            $this->assign('ticketInfo', $ticketInfo);
        }
        $this->display();
    }


    /**
     * 转换数据
     */
    function des($bodyContent)
    {
        $rep = new STD3Des ();
        $list = $rep->decrypt($bodyContent);
        $list = $this->xml_to_array($list);
        return $list;
    }

    /**
     *regioncode 转换
     */
    function nameToCity($code)
    {
        $codearr = array(
            '北京' => '110100',
            '天津' => '120100',
            //河北省 130000
            '石家庄' => '130100',
            '唐山' => '130200',
            '秦皇岛' => '130300',
            '邯郸' => '130400',
            '邢台' => '130500',
            '保定' => '130600',
            '张家口' => '130700',
            '承德' => '130800',
            '沧州' => '130900',
            '廊坊' => '131000',
            '衡水' => '131100',
            //陕西省 140000
            '太原' => '140100',
            '大同' => '140200',
            '阳泉' => '140300',
            '长治' => '140400',
            '晋城' => '140500',
            '朔州' => '140600',
            '晋中' => '140700',
            '运城' => '140800',
            '忻州' => '140900',
            '临汾' => '141000',
            '吕梁' => '141100',
            //内蒙古自治区 150000
            '呼和浩特' => '150100',
            '包头' => '150200',
            '乌海' => '150300',
            '赤峰' => '150400',
            '通辽' => '150500',
            '鄂尔多斯' => '150600',
            '呼伦贝尔' => '150700',
            '巴彦淖尔' => '150800',
            '乌兰察布' => '150900',
            '兴安盟' => '152200',
            '锡林郭勒盟' => '152500',
            '阿拉善盟' => '152900',
            //辽宁省 210000
            '沈阳' => '210100',
            '大连' => '210200',
            '鞍山' => '210300',
            '抚顺' => '210400',
            '本溪' => '210500',
            '丹东' => '210600',
            '锦州' => '210700',
            '营口' => '210800',
            '阜新' => '210900',
            '辽阳' => '211000',
            '盘锦' => '211100',
            '铁岭' => '211200',
            '朝阳' => '211300',
            '葫芦岛' => '211400',
            //吉林省 220000
            '长春' => '220100',
            '吉林' => '220200',
            '四平' => '220300',
            '辽源' => '220400',
            '通化' => '220500',
            '白山' => '220600',
            '松原' => '220700',
            '白城' => '220800',
            '延边' => '222400',
            //黑龙江省 230000
            '哈尔滨' => '230100',
            '齐齐哈尔' => '230200',
            '鸡西' => '230300',
            '鹤岗' => '230400',
            '双鸭山' => '230500',
            '大庆' => '230600',
            '伊春' => '230700',
            '佳木斯' => '230800',
            '七台河' => '230900',
            '牡丹江' => '231000',
            '黑河' => '231100',
            '绥化' => '231200',
            '大兴安岭' => '232700',

            '上海' => '310100',
            //江苏省 320000
            '南京' => '320100',
            '无锡' => '320200',
            '徐州' => '320300',
            '常州' => '320400',
            '苏州' => '320500',
            '南通' => '320600',
            '连云港' => '320700',
            '淮安' => '320800',
            '盐城' => '320900',
            '扬州' => '321000',
            '镇江' => '321100',
            '泰州' => '321200',
            '宿迁' => '321300',
            //浙江省 330000
            '杭州' => '330100',
            '宁波' => '330200',
            '温州' => '330300',
            '嘉兴' => '330400',
            '湖州' => '330500',
            '绍兴' => '330600',
            '金华' => '330700',
            '衢州' => '330800',
            '舟山' => '330900',
            '台州' => '331000',
            '丽水' => '331100',
            //安徽省 340000
            '合肥' => '340100',
            '芜湖' => '340200',
            '蚌埠' => '340300',
            '淮南' => '340400',
            '马鞍山' => '340500',
            '淮北' => '340600',
            '铜陵' => '340700',
            '安庆' => '340800',
            '黄山' => '341000',
            '滁市' => '341100',
            '阜阳' => '341200',
            '宿州' => '341300',
            '巢湖' => '341400',
            '六安' => '341500',
            '亳州' => '341600',
            '池州' => '341700',
            '宣城' => '341800',
            //福建省 350000
            '福州' => '350100',
            '厦门' => '350200',
            '莆市' => '350300',
            '三明' => '350400',
            '泉州' => '350500',
            '漳州' => '350600',
            '南平' => '350700',
            '龙岩' => '350800',
            '宁德' => '350900',
            //江西省 360000
            '南昌' => '360100',
            '景德镇' => '360200',
            '萍乡' => '360300',
            '九江' => '360400',
            '新余' => '360500',
            '鹰潭' => '360600',
            '赣州' => '360700',
            '吉安' => '360800',
            '宜春' => '360900',
            '抚州' => '361000',
            '上饶' => '361100',
            //山东省 370000
            '济南' => '370100',
            '青岛' => '370200',
            '淄博' => '370300',
            '枣庄' => '370400',
            '东营' => '370500',
            '烟台' => '370600',
            '潍坊' => '370700',
            '济宁' => '370800',
            '泰安' => '370900',
            '威海' => '371000',
            '日照' => '371100',
            '莱芜' => '371200',
            '临沂' => '371300',
            '德州' => '371400',
            '聊城' => '371500',
            '滨州' => '371600',
            '菏泽' => '371700',
            //河南省 410000
            '郑州' => '410100',
            '开封' => '410200',
            '洛阳' => '410300',
            '平顶山' => '410400',
            '安阳' => '410500',
            '鹤壁' => '410600',
            '新乡' => '410700',
            '焦作' => '410800',
            '濮阳' => '410900',
            '许昌' => '411000',
            '漯河' => '411100',
            '三门峡' => '411200',
            '南阳' => '411300',
            '商丘' => '411400',
            '信阳' => '411500',
            '周口' => '411600',
            '驻马店' => '411700',
            //湖北省 420000
            '武汉' => '420100',
            '黄石' => '420200',
            '十堰' => '420300',
            '宜昌' => '420500',
            '襄樊' => '420600',
            '鄂州' => '420700',
            '荆门' => '420800',
            '孝感' => '420900',
            '荆州' => '421000',
            '黄冈' => '421100',
            '咸宁' => '421200',
            '随州' => '421300',
            '恩施' => '422800',
            //湖南省 430000
            '长沙' => '430100',
            '株洲' => '430200',
            '湘潭' => '430300',
            '衡阳' => '430400',
            '邵阳' => '430500',
            '岳阳' => '430600',
            '常德' => '430700',
            '张家界' => '430800',
            '益阳' => '430900',
            '郴州' => '431000',
            '永州' => '431100',
            '怀化' => '431200',
            '娄底' => '431300',
            '湘西' => '433100',
            //广东省 440000
            '广州' => '440100',
            '韶关' => '440200',
            '深圳' => '440300',
            '珠海' => '440400',
            '汕头' => '440500',
            '佛山' => '440600',
            '江门' => '440700',
            '湛江' => '440800',
            '茂名' => '440900',
            '肇庆' => '441200',
            '惠州' => '441300',
            '梅州' => '441400',
            '汕尾' => '441500',
            '河源' => '441600',
            '阳江' => '441700',
            '清远' => '441800',
            '东莞' => '441900',
            '中山' => '442000',
            '潮州' => '445100',
            '揭阳' => '445200',
            '云浮' => '445300',
            //广西壮族自治区 450000
            '南宁' => '450100',
            '柳州' => '450200',
            '桂林' => '450300',
            '梧州' => '450400',
            '北海' => '450500',
            '防城港' => '450600',
            '钦州' => '450700',
            '贵港' => '450800',
            '玉林' => '450900',
            '百色' => '451000',
            '贺州' => '451100',
            '河池' => '451200',
            '来宾' => '451300',
            '崇左' => '451400',
            //海南省 460000
            '海口' => '460100',
            '三亚' => '460200',
            '重庆' => '500100',
            //四川省 510000
            '成都' => '510100',
            '自贡' => '510300',
            '攀枝花' => '510400',
            '泸州' => '510500',
            '德阳' => '510600',
            '绵阳' => '510700',
            '广元' => '510800',
            '遂宁' => '510900',
            '内江' => '511000',
            '乐山' => '511100',
            '南充' => '511300',
            '眉山' => '511400',
            '宜宾' => '511500',
            '广安' => '511600',
            '达州' => '511700',
            '雅安' => '511800',
            '巴中' => '511900',
            '资阳' => '512000',
            '阿坝' => '513200',
            '甘孜' => '513300',
            '凉山' => '513400',
            //贵州省 520000
            '贵阳' => '520100',
            '六盘水' => '520200',
            '遵义' => '520300',
            '安顺' => '520400',
            '铜仁' => '522200',
            '黔西南' => '522300',
            '毕节' => '522400',
            '黔东南' => '522600',
            '黔南' => '522700',
            //云南省 530000
            '昆明' => '530100',
            '曲靖' => '530300',
            '玉溪' => '530400',
            '保山' => '530500',
            '昭通' => '530600',
            '丽江' => '530700',
            '思茅' => '530800',
            '临沧' => '530900',
            '楚雄' => '532300',
            '红河' => '532500',
            '文山' => '532600',
            '西双版纳' => '532800',
            '大理' => '532900',
            '德宏' => '533100',
            '怒江' => '533300',
            '迪庆' => '533400',
            //西藏自治区 540000
            '拉萨' => '540100',
            '昌都' => '542100',
            '山南' => '542200',
            '日喀则' => '542300',
            '那曲' => '542400',
            '阿里' => '542500',
            '林芝' => '542600',
            //陕西省 610000
            '西安' => '610100',
            '铜川' => '610200',
            '宝鸡' => '610300',
            '咸阳' => '610400',
            '渭南' => '610500',
            '延安' => '610600',
            '汉中' => '610700',
            '榆林' => '610800',
            '安康' => '610900',
            '商洛' => '611000',
            //甘肃省 620000
            '兰州' => '620100',
            '嘉峪关' => '620200',
            '金昌' => '620300',
            '白银' => '620400',
            '天市' => '620500',
            '武威' => '620600',
            '张掖' => '620700',
            '平凉' => '620800',
            '酒泉' => '620900',
            '庆阳' => '621000',
            '定西' => '621100',
            '陇南' => '621200',
            '临夏' => '622900',
            '甘南' => '623000',
            //青海省 630000
            '西宁' => '630100',
            '海东' => '632100',
            '海北' => '632200',
            '黄南' => '632300',
            '海南' => '632500',
            '果洛' => '632600',
            '玉树' => '632700',
            '海西' => '632800',
            //宁夏回族自治区 640000
            '银川' => '640100',
            '石嘴山' => '640200',
            '吴忠' => '640300',
            '固原' => '640400',
            '中卫' => '640500',
            //新疆维吾尔自治区 650000
            '乌鲁木齐' => '650100',
            '克拉玛依' => '650200',
            '吐鲁番' => '652100',
            '哈密' => '652200',
            '昌吉' => '652300',
            '博尔塔拉' => '652700',
            '巴音郭楞' => '652800',
            '阿克苏' => '652900',
            '克孜勒苏' => '653000',
            '喀什' => '653100',
            '和田' => '653200',
            '伊犁哈萨克' => '654000',
            '塔城' => '654200',
            '阿勒泰' => '654300',
            '台湾' => '710000',
            //香港特别行政区 810000
            '香港' => '810100',
            '九龙' => '810200',
            '新界' => '810300',
            //澳门特别行政区 820000
            '澳门' => '820100',
            '离岛' => '820200',
        );
        return $codearr[$code];
    }

}
