<?php

namespace Home\Controller;

use Common\Utils;
use Think\Controller;

class HotelController extends CommonController
{

    /**
     * test
     */
    public function test()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20160504115329</timestamp>
                    </header>

                    <query type='countries' product='hotel'/>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * City Search
     */
    public function citySearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='cities' product='hotel'>
                        <country code='I'/>
                        <timestamp>20030423170100</timestamp>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Zones Search
     */
    public function zonesSearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='zones' product='hotel'>
                        <city code='VCEI'/>
                        <timestamp>20030423170100</timestamp>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Meeting Points Search
     */
    public function mettingPointsSearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='meeting_points' product='transfer'>
                        <city code='VCEI'/>
                        <zone code='aptve'/>
                        <timestamp>20030423170100</timestamp>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        dump($result);
        exit();
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Meeting Points Typologies Search
     */
    public function mettingPointsTypologiesSearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='meeting_point_types' product='transfer'>
                        <timestamp>20030423170100</timestamp>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        dump($result);
        exit();
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Hotels Search
     */
    public function HotelsSearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='hotels' product='hotel'>
                        <city code='VCEI'/>
                        <timestamp>20030423170100</timestamp>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Hotel Details
     */
    public function HotelDetails()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='details' product='hotel'>
                        <hotel id='123456'/>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Hotel Catalog
     */
    public function HotelCatalog()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='hotel' product='catalog'>
                        <hotel code='30213'/>
                        <period start='20030423' end='20030426'/>
                        <contract code='31568'/>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Remarks
     */
    public function remarks()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='remarks' product='hotel' />
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Hotels Generic Details
     */
    public function hotelsGenericDetails()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='hoteldetails' product='hotel' />
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();
    }

    /**
     * Availability search
     */
    public function AvailabilitySearch()
    {
        $url = $this->hotelRequestUrl();
        $data = "<envelope>
                    <header>
                        <actor>" . $this->hotelActor() . "</actor>
                        <user>" . $this->hotelUser() . "</user>
                        <password>" . $this->hotelPassword() . "</password>
                        <version>" . $this->hotelVersion() . "</version>
                        <timestamp>20030423170100</timestamp>
                    </header>

                    <query type='availability' product='hotel'>
                        <nationality>IT</nationality>
                        <filters>
                            <filter>AVAILONLY</filter>
                            <filter>BESTARRANGMENT</filter>
                        </filters>
                        <checkin date='2003-09-05'/>
                        <checkout date='2003-09-13'/>
                        <city code='vce'/>
                        <category code='st'/>
                        <hotel id='1234'>CARLTON GRAN CANAL</hotel>
                        <details>
                            <room type='sgl' required='3'/>
                            <room type='dbl' required='1' extrabed='true' age='5'/>
                            <room type='trp' required='1' cot='true'/>
                        </details>
                    <stars>
                            <star>1</star>
                            <star>2</star>
                            <star>3</star>
                            <star>4</star>
                            <star>5</star>
                    </stars>
                    </query>
                </envelope>";
        $result = Utils::vpost($url, $data);
        $list = $this->xml_to_array($result);
        dump($list);
        exit();

        /*For what concerns rooms mapping, you can use the attribute "occupancy" in the instead of the "type" attribute, an example would be the following:
        <query type="availability" product="hotel">
         <filters>
          <filter>AVAILONLY</filter>
         </filters>
         <checkin date="2014-12-12" />
         <checkout date="2014-12-13" />
         <city code="ROM"/>
         <details>
          <room occupancy="2" required="1" extrabed="false"/>
         </details>
        </query>
        /**/
    }

}