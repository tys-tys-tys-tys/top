<?php

namespace Home\Controller;

use Think\Controller;
use Common\Utils;

class CommonController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $name = $_SERVER['SERVER_NAME'];

        if ($name != 'oauth.lydlr.com' && $name != 'wxpay.m.lydlr.com') {
            $this->userInfo = M('agency')->where("domain = '{$name}' && state = 1")->find();
            if (empty($this->userInfo)) {
                $this->show('域名未绑定');
                exit;
            }
        }

        /**
         * About Operator
         */
        $operatorId = $this->userInfo['operator_id'];
        $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
        $this->logo = $operatorInfo['logo'];
        $this->copyRight = $operatorInfo['copyright'];
        $this->shareOperators = $operatorInfo['share_operators'];
        $this->address = $operatorInfo['address'];
        $this->tel = $operatorInfo['tel'];
        $this->cdkey = $operatorInfo['sms_ym_cdkey'];
        $this->pwd = $operatorInfo['sms_ym_pwd'];
        $this->signature = "【" . $operatorInfo['sms_ym_signature'] . "】";
        $this->smsOperatorId = $this->userInfo['operator_id'];

        $this->code = 400;
    }

    /**
     * Ticket Url
     */
    function ticketUrl()
    {
        $url = "http://bridge.piao58.cn/PlatForm/API?method=";
        return $url;
    }

    /**
     * Ticket Pid
     */
    function ticketPid()
    {
        $pid = "13911881801";
        return $pid;
    }

    /**
     * Ticket Secret
     */
    function secretKey()
    {
        $secretKey = "fdd37a18324da335cd6b47ed";
        return $secretKey;
    }


    /**
     * Flight Code
     */
    function agencyCode()
    {
        $code = 'BJSKT';
        return $code;
    }

    /**
     * Flight Sign
     */
    function sign()
    {
        $sign = 'Y!cpR7&e';
        return $sign;
    }

    /**
     * FlightQuery Url
     */
    function flightQueryUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getAvailableFlightWithPriceAndCommisionServiceRestful1.0/getAvailableFlightWithPriceAndCommision';
        return $url;
    }
    /**
     * demo FlightQuery Url
     */
    function demo_flightQueryUrl()
    {
        $url = 'http://ws.tongyedns.com:55779/ltips/services/getAvailableFlightWithPriceAndCommisionServiceRestful1.0/getAvailableFlightWithPriceAndCommision';
        return $url;
    }

    /**
     * Create Order Url (创建订单)
     */
    function createOrderUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/createOrderByPassengerServiceRestful1.0/createOrderByPassenger';
        return $url;
    }

    /**
     * Order Pay Url (订单支付)
     */
    function orderPayUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/payServiceRestful1.0/pay';
        return $url;
    }

    /**
     * Order Query Url (订单查询)
     */
    function orderQueryUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getOrderStatusServiceRestful1.0/getOrderStatus';
        return $url;
    }

    /**
     * Order Cancel Url (订单取消)
     */
    function orderCancelUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/cancelOrderServiceRestful1.0/cancelOrder';
        return $url;
    }

    /**
     * Order Detail Url (订单详情)
     */
    function orderDetailsUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getOrderByOrderNoServiceRestful1.0/getOrderByOrderNo';
        return $url;
    }

    /**
     * Order Refund Url (订单退废票)
     */
    function orderRefundUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/applyPolicyOrderRefundServiceRestful1.0/applyPolicyOrderRefund';
        return $url;
    }

    /**
     * Order Refund Detail Url (订单退废票详情)
     */
    function orderRefundDetailsUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getRefundDetailServiceRestful1.0/getRefundDetail';
        return $url;
    }

    /**
     * Order Refund Stipulate Url (根据航空公司、舱位获取退改签规定)
     */
    function orderRefundStipulateUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getModifyAndRefundStipulateServiceRestful1.0/getModifyAndRefundStipulate';
        return $url;
    }

    /**
     * Order All Refund Stipulate Url (获取所有退改签规定)
     */
    function orderAllRefundStipulateUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getModifyAndRefundStipulatesServiceRestful1.0/getModifyAndRefundStipulates';
        return $url;
    }

    /**
     * Order Daily Lowest Price Url (实时获取每日最低价)
     */
    function orderDailyLowestPriceUrl()
    {
        $url = 'http://ws.tongyedns.com:8000/ltips/services/getDailyLowestPriceServiceRestful1.0/getDailyLowestPrice';
        return $url;
    }

    /**
     * Hotel Request Url
     */
    function hotelRequestUrl()
    {
        $url = "http://towers.netstorming.net/kalima/call.php";
        return $url;
    }

    /**
     * Hotel Actor
     */
    function hotelActor()
    {
        $actor = "KTLXSBJ";
        return $actor;
    }

    /**
     * Hotel User
     */
    function hotelUser()
    {
        $user = "xmluser";
        return $user;
    }

    /**
     * Hotel Password
     */
    function hotelPassword()
    {
        $pass = "ktlxsbjxml";
        return $pass;
    }

    /**
     * Hotel Version
     */
    function hotelVersion()
    {
        $version = "1.6.1";
        return $version;
    }

    /**
     * XML to Array
     */
    function xml_to_array($xml)
    {
        $array = (array)(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA));
      
        foreach ($array as $key => $item) {
            $array[$key] = $this->struct_to_array((array)$item);
        }
        return $array;
    }

    /**
     * 转换
     */
    function struct_to_array($item)
    {
        if (!is_string($item)) {
            $item = (array)$item;
            foreach ($item as $key => $val) {
                $item[$key] = self::struct_to_array($val);
            }
        }
        return $item;
    }

    /**
     * Array Sort
     */
    function my_sort($arrays, $sort_key, $sort_order = SORT_ASC, $sort_type = SORT_NUMERIC)
    {
        if (is_array($arrays)) {
            foreach ($arrays as $array) {
                if (is_array($array)) {
                    $key_arrays[] = $array[$sort_key];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        array_multisort($key_arrays, $sort_order, $sort_type, $arrays);
        return $arrays;
    }

    /**
     * Object To Array
     */
    function object_to_array($obj)
    {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach ($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? $this->object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }

    /**
     * WeChat Send Message
     */
    public function send_tpl($template)
    {
        vendor('WxPayPubHelper.WxPayPubHelper');
        $jsApi = new \JsApi_pub();
        $accessToken = $jsApi->getAccessToken();
        $json_template = json_encode($template);
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $accessToken;
        Utils::vpost($url, urldecode($json_template));
    }


}
