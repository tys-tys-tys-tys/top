<?php

namespace Home\Controller;

use Common\Top;
use Common\Utils;
use Think\Controller;

class IndexController extends CommonController
{


    public function index()
    {
        $M = M();
        //获取顾问身份
        $agencyInfo = $this->userInfo;
        //获取顾问ID
        $agencyId = $agencyInfo['id'];
        //获取顾问所对应的运营商的线路共享的权限id
        $operatorId = $this->shareOperators;
        $operatorId = !empty($operatorId) ? $operatorId : 0;
        //获取当前时间
        $nowDate = date('Y-m-d 00:00:00');
        //获取顾问所对应的运营商ID
        $operator_id = $agencyInfo['operator_id'];
        if($agencyInfo['column_sort'] == 0){
            $info = '1,2,3,4,5,6,7,8';
        }else{
            $info = $agencyInfo['column_sort'];
        }
        $info_arr = explode(",",$info);
       
        //获取首页栏目二维数组
        $Column_list = getColumnSort();
        foreach ($info_arr as $k=>$vo){
            $Column_list[$k]['sort'] = $vo;
        }
        $sort = array(
            'direction' => 'SORT_ASC', //排序顺序标志 SORT_DESC 降序；SORT_ASC 升序  
            'field' => 'sort', //排序字段  
        );
        $arrSort = array();
        foreach ($Column_list AS $uniqid => $row) {
            foreach ($row AS $key => $value) {
                $arrSort[$key][$uniqid] = $value;
            }
        }
        if ($sort['direction']) {
            array_multisort($arrSort[$sort['field']], constant($sort['direction']), $Column_list);
        }
        //dump($Column);exit;
        if (IS_POST) {
            $name = I('names');
            $nameId = substr($name, 2);
            $nameCode = substr($name, 0, 2);
            $state = Top::ProductStateIsSale;

            if ($nameCode == '10') {
                if (is_numeric($name)) {
                    $tourInfo = M('tour')->where("id = '{$nameId}' && state = '{$state}' && operator_id in($operatorId)")->find();
                    $rediRectUrl = "Agency/details";
                }
            } else if ($nameCode == '20') {
                if (is_numeric($name)) {
                    $tourInfo = M('product')->where("id = '{$nameId}' && state = '{$state}'")->find();
                    $rediRectUrl = "Product/details";
                }
            }

            if (!empty($tourInfo)) {
                $this->redirect($rediRectUrl, array("id" => $nameId));
            }

            $where['name|destinations|provider_name'] = array('like', '%' . $name . '%');
            $where['state'] = Top::ProductStateIsSale;
            //$where['operator_id'] = array('in', $operatorId);
            $list = M('tour')->where($where)->where("operator_id = '{$operator_id}'")
                ->union("select * from top_tour where operator_id in($operatorId) and operator_id != '{$operatorId}' and state = 2 and ( name LIKE '%$name%' OR destinations LIKE '%$name%' OR provider_name LIKE '%$name%')", true)->select();
          
                $this->assign('name', $name);
                $this->assign('search', 1);
        } else {
            //获取定义好的内部订单状态码
            $providerType = Top::NotOutOrder;
            // 查询我的推荐 agency_tour 表
            $agencyMyTourList = $M->table('top_tour as t,top_agency_tour as a ')
                ->where("a.agency_id = '{$agencyId}' && a.tour_id = t.id && t.state = 2 && t.operator_id in($operatorId) && t.latest_sku >= '{$nowDate}' && t.provider_type = '{$providerType}'")
                ->field(array(
                    't.id' => 'id',
                    't.name' => 'name',
                    't.cover_pic' => 'cover_pic',
                    't.min_price' => 'min_price',
                    't.departure' => 'departure',
                ))
                ->order('a.create_time desc')
                ->limit(2)
                ->select();
//                dump($agencyMyTourList);exit;
            //统计我的推荐表的id         
            $ids = array();
            foreach ($agencyMyTourList as $vo) {
                $ids[] = $vo['id'];
            }

            $ids = implode(',', $ids);
            $tourid['id'] = array('not in', $ids);
            //运营商置顶并满足条件的线路
            $topTours = M('tour')->where("state = 2 && operator_id in($operator_id) && latest_sku >= '{$nowDate}' && provider_type = '{$providerType}' && is_top = 1")->order("update_time desc")->limit(2)->select();
            if($ids){
                $list_where = "state = 2 && operator_id in($operator_id) && latest_sku >= '{$nowDate}' && provider_type = '{$providerType}' && is_top = 0 && id not in($ids)";
            }else{
                $list_where = "state = 2 && operator_id in($operator_id) && latest_sku >= '{$nowDate}' && provider_type = '{$providerType}' && is_top = 0";
            }
//            dump($topTours);exit;
            //满足条件的运营商未置顶的线路
            $list = M('tour')
                ->where($list_where)
                ->order('update_time desc')
                ->limit(16)
                ->select();

           
            foreach($list as $k=>$v){
                $indexIds[$k] = $v['id'];
            }

            $indexIds = implode(',', $indexIds);
            session('indexIds', $indexIds);

            $domain = $_SERVER['SERVER_NAME'];
            $domain = "http://" . $domain;
            $this->assign('domain', $domain);

            $title = $agencyInfo['share_home_title'];
            if (empty($title)) {
                $title = '我的手机旅行社';
            }

            $this->assign('title', $title);
            $this->assign('search', 0);
        }
        $now = date("Y-m-d", time());
        //$promotionList = M('tour')->where("state = 2 && operator_id in($operator_id) && is_marquee = 1 && marquee_start_time <= '$now' && marquee_end_time >= '$now'")->order("update_time desc")->select(); 
        //$this->marqueeNum = count($promotionList);
        //查看是否改价
        $now = date("Y-m-d 00:00:00");
        $agencyTourList = M('agency_tour_price')->where("agency_id = '{$agencyId}' && start_time >= '{$now}'")->field('agency_id,tour_id,min(price_adult_list) price_adult_list')->group('tour_id')->select();
        $arr3 = array();
        foreach ($list as $key => $vo) {
            foreach ($agencyTourList as $k => $v) {
                if ($vo['id'] == $v['tour_id']) {
                    $list[$key]['id'] = $vo['id'];
                    $list[$key]['min_price'] = min((int)$vo['min_price'], (int)$v['price_adult_list']);
                } else {
                    $list[$key]['id'] = $vo['id'];
                    $list[$key]['min_price'] = (int)$vo['min_price'];
                }
            }
        }


        $Pic = array();
        foreach ($list as $key => $vo) {
            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic[] = Utils::GetImageUrl($url, 240, 160, 1);
        }
        //处理顾问推荐线路图片
        $Pic_agencyMyTourList = array();
        foreach ($agencyMyTourList as $key => $vo) {
            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic_agencyMyTourList[] = Utils::GetImageUrl($url, 240, 160, 1);
        }
        //处理运营商置顶线路图片
        $Pic_topTours = array();
        foreach ($topTours as $key => $vo) {
            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic_topTours[] = Utils::GetImageUrl($url, 240, 160, 1);
        }
        

        $baseUrl = C('MOFILE_BASE_URL');
        if (empty($agencyInfo['head_pic'])) {
            $url = 'http://file1.lydlr.com/tour/2016/0826/00945877530.jpg';
            $headPic = Utils::GetImageUrl($url, 300, 300, 1);
        } else {
            $headPic = $baseUrl . $agencyInfo['head_pic'];
            $headPic = Utils::GetImageUrl($headPic, 300, 300, 1);
        }

        $bgPic = $baseUrl . $agencyInfo['bg_pic'];
        $bgPic = Utils::GetImageUrl($bgPic, 720, 720, 0);

        $weixinQrcode = $baseUrl . $agencyInfo['weixin_qrcode'];
        $weixinQrcode = Utils::GetImageUrl($weixinQrcode, 300, 300, 1);
//   dump($Column_list);exit;
        $this->assign('Column_list', $Column_list);
        $this->assign('minPirce', $arr3);
        $this->assign('headPic', $headPic);
        $this->assign('bgPic', $bgPic);
        $this->assign('weixinQrcode', $weixinQrcode);

        $this->assign('Pic', $Pic);
        $this->assign('Pic_topTours', $Pic_topTours);
        $this->assign('Pic_agencyMyTourList', $Pic_agencyMyTourList);
        $this->assign('userList', $agencyInfo);
        $this->assign('agency', $agencyInfo);
        $this->assign('agencyMyTourList', $agencyMyTourList);
        $this->assign('topTours', $topTours);
        $this->assign('list', $list);
        //$this->assign('promotionList', $promotionList);
        //获取当前运营商下的所有供应商的id，name
        $supplier = M('provider')->field('id,name')->where("operator_id in($operatorId) && state != 4 ")->order('id desc')->select();
        $this->assign('supplier', $supplier);
        $state = Top::ProductStateIsSale;

        $inboundDestinationList = M('tour')->where("operator_id in($operatorId) && line_type = 2 && state = '{$state}' && provider_type = 0")->field('id,destination')->group('destination')->order('id desc')->select();
        $this->assign('inboundDestinationList', $inboundDestinationList);

        $outboundDestinationList = M('tour')->where("operator_id in($operatorId) && line_type = 3 && state = '{$state}' && provider_type = 0")->field('id,destination')->group('destination')->order('id desc')->select();
        $this->assign('outboundDestinationList', $outboundDestinationList);
//        dump($list);exit;
        $this->display();
    }
    


    public function contact()
    {
        $userList = $this->userInfo;
        $contact = $userList['mobile'];
        $this->ajaxReturn($contact);
    }

}
