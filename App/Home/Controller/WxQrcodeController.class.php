<?php

namespace Home\Controller;

use Common\Utils;
use Think\Controller;

class WxQrcodeController extends CommonController
{


    public function index()
    {
        if (IS_POST) {
            vendor('WxPayPubHelper.WxPayPubHelper');
            $jsApi = new \JsApi_pub();
            $accessToken = $jsApi->getAccessToken();
            $data['action_name'] = 'QR_LIMIT_STR_SCENE';
            $data['action_info'] = array(
                'scene' => array(
                    'scene_str' => I('scene_str'),
                ),
            );
            $data = json_encode($data);
            $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=$accessToken";
            $result = Utils::vpost($url, $data);
            $resultObject = json_decode($result);//object
            $resultArray = $this->object_to_array($resultObject);//array
            $url = $resultArray['url'];
            $this->assign('url', $url);
        }

        $this->display();

    }


    /**
     * 带 LOGO 二维码
     */
    public function qrcode()
    {
        $value = ''; //二维码内容
        $errorCorrectionLevel = 'L';//容错级别
        $matrixPointSize = 6;//生成图片大小
        //生成二维码图片
        Vendor('phpqrcode.phpqrcode');
        $object = new \QRcode();
        $object->png($value, 'qrcode.png', $errorCorrectionLevel, $matrixPointSize, 2);
        $logo = 'http://cdn.lydlr.com/public/img/logo.png';//准备好的logo图片
        $QR = 'http://cdn.lydlr.com/public/images/qrcode.png';//已经生成的原始二维码图

        if ($logo !== FALSE) {
            $QR = imagecreatefromstring(file_get_contents($QR));
            $logo = imagecreatefromstring(file_get_contents($logo));
            $QR_width = imagesx($QR);//二维码图片宽度
            $QR_height = imagesy($QR);//二维码图片高度
            $logo_width = imagesx($logo);//logo图片宽度
            $logo_height = imagesy($logo);//logo图片高度
            $logo_qr_width = $QR_width / 5;
            $scale = $logo_width / $logo_qr_width;
            $logo_qr_height = $logo_height / $scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            //重新组合图片并调整大小
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,
                $logo_qr_height, $logo_width, $logo_height);
        }
        //输出图片
        imagepng($QR, 'helloweixin.png');
    }
}