<?php

namespace Home\Controller;

use Common\Top;
use Common\Utils;
use Common\Sms;
use Think\Controller;

class WeixinController extends Controller
{

    public function bind()
    {
        if (IS_AJAX && IS_POST) {
            $mobile = I('mobile');
            $pwd = I('pwd');
            $openId = I('openId');
            $where['login_name'] = $mobile;
            $where['login_pwd'] = Utils::getHashPwd($pwd);

            $agencyInfo = M('agency')->where("mobile = '{$mobile}'")->find();
            $userInfo = M('user')->where($where)->find();

            if (!$userInfo) {
                $data['status'] = 0;
                $data['msg'] = '账号或密码错误';
                $this->ajaxReturn($data);
            }

            if ($agencyInfo && !empty($agencyInfo['domain'])) {
                //判断手机号是否存在
                if ($mobile == $agencyInfo['mobile']) {
                    //判断 openid 字段是否为空
                    if ($agencyInfo['weixinmp_openid'] != '') {
                        $openIdInfo = M('agency')->where("weixinmp_openid = '{$openId}'")->field('weixinmp_openid,domain')->find();
                        if (!empty($openIdInfo)) {
                            $domain = $openIdInfo['domain'];
                            $url = "http://" . $domain . "/index.php";
                            header("Location:" . $url);
                        }
                    } else {
                        $today = date("Y-m-d H:i:s", time());
                        $agencyUpdate = M('agency')->execute("update top_agency set weixinmp_openid = '{$openId}',weixinmp_bind_time = '{$today}' where mobile = '{$mobile}'");
                        $agencyInfo = M('agency')->where("weixinmp_openid = '{$openId}'")->field('weixinmp_openid,domain')->find();
                        $url = $agencyInfo['domain'];

                        if (strpos($url, '.demo.') !== false) {
                            $url = "http://" . $url . ":8080/index.php";
                        } else {
                            $url = "http://" . $url . "/index.php";
                        }

                        if ($agencyUpdate) {
                            $data['url'] = $url;
                            $data['status'] = 1;
                            $data['msg'] = '恭喜您！绑定成功！';
                            $this->ajaxReturn($data);
                        }
                    }
                } else {
                    $data['status'] = 0;
                    $data['msg'] = '手机号码不正确';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 0;
                $data['msg'] = '信息不存在或域名配置错误';
                $this->ajaxReturn($data);
            }
        } else {
            $openId = I('openId');
            $openIdInfo = M('agency')->where("weixinmp_openid = '{$openId}'")->field('weixinmp_openid,domain')->find();

            if (!empty($openIdInfo)) {
                $domain = $openIdInfo['domain'];

                if (strpos($domain, '.demo.') !== false) {
                    $url = "http://" . $domain . ":8080/index.php";
                    header("Location:" . $url);
                } else {
                    $url = "http://" . $domain . "/index.php";
                    header("Location:" . $url);
                }

            }
        }
        $this->display();
    }

}
