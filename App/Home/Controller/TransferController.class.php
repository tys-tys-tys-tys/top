<?php

namespace Home\Controller;

use Think\Controller;

class TransferController extends Controller
{

    public function jump()
    {
        $target = I('get.target');

        vendor('WxPayPubHelper.WxPayPubHelper');
        //使用jsapi接口
        $jsApi = new \JsApi_pub();
        //通过code获得openid
        if (!isset($_GET['code'])) {
            //触发微信返回code码
            $url = $jsApi->createOauthUrlForCode('http://oauth.lydlr.com/index.php/Home/Transfer/getOpenId?target=' . $target);
            Header("Location: $url");
        }

    }

    /**
     * 获得openid
     */
    public function getOpenId()
    {
        $code = I('get.code');

        if (!empty($code)) {
            //从tencent redirect 来
            vendor('WxPayPubHelper.WxPayPubHelper');
            //使用jsapi接口
            $jsApi = new \JsApi_pub();

            $jsApi->setCode($code);
            $openid = $jsApi->getOpenId();
        }

        $target = I('get.target');

        if($target == 'hebei' || $target == 'tianjin'){
            $domain = 'http://www.dalvu.com/register/agencyRegister.html';
        }else{
            $agencyInfo = M('agency')->where("weixinmp_openid = '{$openid}'")->find();
            $domain = $agencyInfo['domain'];
        }

        $this->jumpToProduct($domain, $target, $openid);
    }


    /**
     * 大旅游产品
     */
    public function jumpToProduct($domain, $target, $openid)
    {
        if (!$domain) {
            $agencyInfo = M('agency')->where("state = 1 && istest = 0")->order('rand()')->limit(1)->find();
            $visitorDomain = $agencyInfo['domain'];
        }
        //门票
        if ($target == 'ticket') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php/Home/Ticket/index";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://" . $visitorDomain . "/index.php/Home/Ticket/index";
                Header("Location: $url");
                exit();
            }
        }

        //出境
        if ($target == 'outBound') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php/Home/Outbound/index";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://" . $visitorDomain . "/index.php/Home/Outbound/index";
                Header("Location: $url");
                exit();
            }
        }

        //特价
        if ($target == 'bargainPrice') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php/Home/BargainPrice/index";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://" . $visitorDomain . "/index.php/Home/BargainPrice/index";
                Header("Location: $url");
                exit();
            }
        }


        //机票
        if ($target == 'flight') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php/Home/Flight/flightQuery";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://" . $visitorDomain . "/index.php/Home/Flight/flightQuery";
                Header("Location: $url");
                exit();
            }
        }

        //更多精彩
        if ($target == 'index') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://" . $visitorDomain . "/index.php";
                Header("Location: $url");
                exit();
            }
        }

        //我的旅行社
        if ($target == 'myIndex') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://m.lydlr.com/index.php/Home/Weixin/bind?openId=$openid";
                Header("Location: $url");
                exit();
            }
        }

        //个人中心
        if ($target == 'personalCenter') {
            if ($domain) {
                $url = "http://" . $domain . "/index.php/Home/Login/wxLoginIndex";
                Header("Location: $url");
                exit();
            } else {
                $url = "http://m.lydlr.com/index.php/Home/Weixin/bind?openId=$openid";
                Header("Location: $url");
                exit();
            }
        }


        //顾问加盟
        if($target == 'hebei' || $target == 'tianjin'){
            $url = $domain . "?from={$target}&openid={$openid}";
            Header("Location: $url");
            exit();
        }
    }

}
