<?php

namespace Home\Controller;

use Common\Soap;
use Common\Top;
use Common\Utils;
use Common\Api_51;
use Common\Sms;

class FlightController extends CommonController
{

    /**
     * 航班查询-页面
     */
    public function flightQuery()
    {
        $domain = $_SERVER['SERVER_NAME'];
        $domain = 'http://' . $domain . "/index.php/Home/WxJsAPI/flightQuery";
        $this->assign('shareUrl', $domain);
        $this->display();
    }


    /**
     * 机票列表
     */
    public function flightQueryList()
    {
        if (IS_POST) {
            $url = $this->flightQueryUrl();
            $data['agencyCode'] = $this->agencyCode();
            $dayBefore = I('dayBefore');
            $dayAfter = I('dayAfter');

            $orgAirportCode = I('departure');
            if ($orgAirportCode == 'NAY') {
                $orgAirportCode = 'PEK';
            } else if ($orgAirportCode == 'PVG') {
                $orgAirportCode = 'SHA';
            } else {
                $orgAirportCode = I('departure');
            }

            $dstAirportCode = I('destination');
            if ($dstAirportCode == 'NAY') {
                $dstAirportCode = 'PEK';
            } else if ($dstAirportCode == 'PVG') {
                $dstAirportCode = 'SHA';
            } else {
                $dstAirportCode = I('destination');
            }

            if (!empty($dayBefore)) {
                $data['orgAirportCode'] = $orgAirportCode;
                $data['dstAirportCode'] = $dstAirportCode;

                $data['date'] = date('Y-m-d', strtotime("$dayBefore - 1 day"));
            } else if (!empty($dayAfter)) {
                $data['orgAirportCode'] = $orgAirportCode;
                $data['dstAirportCode'] = $dstAirportCode;

                $data['date'] = date('Y-m-d', strtotime("$dayAfter + 1 day"));
            } else {
                //出发地
                $departure = I('departure');
                $data['orgAirportCode'] = $this->airportname($departure);
                //目的地
                $destination = I('destination');
                $data['dstAirportCode'] = $this->airportname($destination);
                $data['date'] = I('timestart');
            }

            $data['onlyAvailableSeat'] = 0;
            $data['onlyNormalCommision'] = 0;
            $data['onlyOnWorkingCommision'] = 0;
            $data['onlySelfPNR'] = 0;

            $sign = $this->agencyCode() . $data['dstAirportCode'] . $data['onlyAvailableSeat'] . $data['onlyNormalCommision'] . $data['onlyOnWorkingCommision'] . $data['onlySelfPNR'] . $data['orgAirportCode'] . $this->sign();
        
            $data['sign'] = strtolower(MD5($sign));

            //返回 xml 格式
            $result = Utils::vpost($url, $data);

            //转换成数组
            $listAllData = $this->xml_to_array($result);
  
            //返回值
            $returnCode = $listAllData['returnCode'][0];
            if ($returnCode !== 'S') {
                $departure = I('departure');
                $destination = I('destination');
                $msg = '很抱歉，暂未查询到符合您要求的航班。您可以调整出行时间或目的地重新查询。';
                $this->assign('msg', $msg);
                $this->assign('departure', $departure);
                $this->assign('destination', $destination);
                $this->display("Flight/flightQueryError");
                exit();
            }

            //时间
            $date = $listAllData['flightItems']['date'];
            //两块钱服务费
            $serviceFee = C('SERVICE_FEE');

            //数据重组		
            $flightinfo = array();

            $jjc = 0;
            $swc = 0;
            $tdc = 0;
            if ($listAllData['flightItems']['flights'][0]) {
                foreach ($listAllData['flightItems']['flights'] as $key => $v) {

                    if (isset($v['seatItems'][0])) {
                        $seatItems = $v['seatItems'];
                    } else {
                        $seatItems[0] = $v['seatItems'];
                    }

                    foreach ($seatItems as $ki => $svi) {
                        if ($svi['seatMsg'] == '经济舱' || $svi['seatMsg'] == '高端经济舱' || $svi['seatMsg'] == '超级经济舱' || $svi['seatMsg'] == '特价舱位') {
                            if (($svi['parPrice'] > $jjc) && ($svi['seatMsg'] = '经济舱' || $svi['seatMsg'] == '高端经济舱' || $svi['seatMsg'] == '超级经济舱' || $svi['seatMsg'] == '特价舱位')) {
                                $jjc = $svi['parPrice'];
                            }
                        }

                        if ($svi['seatMsg'] == '商务舱') {
                            if (($svi['parPrice'] > $swc) && ($svi['seatMsg'] = '商务舱')) {
                                $swc = $svi['parPrice'];
                            }
                        }

                        if ($svi['seatMsg'] == '头等舱') {
                            if (($svi['parPrice'] > $tdc) && ($svi['seatMsg'] = '头等舱')) {
                                $tdc = $svi['parPrice'];
                            }
                        }
                    }
                }
            } else {
                $v = $listAllData['flightItems']['flights'];

                if (isset($v['seatItems'][0])) {
                    $seatItems = $v['seatItems'];
                } else {
                    $seatItems[0] = $v['seatItems'];
                }

                foreach ($seatItems as $ki => $svi) {
                    if ($svi['seatMsg'] == '经济舱' || $svi['seatMsg'] == '高端经济舱' || $svi['seatMsg'] == '超级经济舱' || $svi['seatMsg'] == '特价舱位') {
                        if (($svi['parPrice'] > $jjc) && ($svi['seatMsg'] = '经济舱' || $svi['seatMsg'] == '高端经济舱' || $svi['seatMsg'] == '超级经济舱' || $svi['seatMsg'] == '特价舱位')) {
                            $jjc = $svi['parPrice'];
                        }
                    }

                    if ($svi['seatMsg'] == '商务舱') {
                        if (($svi['parPrice'] > $swc) && ($svi['seatMsg'] = '商务舱')) {
                            $swc = $svi['parPrice'];
                        }
                    }

                    if ($svi['seatMsg'] == '头等舱') {
                        if (($svi['parPrice'] > $tdc) && ($svi['seatMsg'] = '头等舱')) {
                            $tdc = $svi['parPrice'];
                        }
                    }
                }
            }

            if ($listAllData['flightItems']['flights'][0]) {
                foreach ($listAllData['flightItems']['flights'] as $key => $v) {
                    //航班信息重组
                    $flightinfo[$key]['orgCity'] = $v['orgCity'];
                    $flightinfo[$key]['depname'] = $this->nameToAirport($v['orgCity']);
                    $flightinfo[$key]['depTime'] = $v['depTime'];
                    $flightinfo[$key]['arrDate'] = $v['param1'];
                    $flightinfo[$key]['dstCity'] = $v['dstCity'];
                    $flightinfo[$key]['arrname'] = $this->nameToAirport($v['dstCity']);
                    $flightinfo[$key]['arriTime'] = $v['arriTime'];
                    $flightinfo[$key]['flightNo'] = $v['flightNo'];
                    $flightinfo[$key]['flightNoLetter'] = substr($v['flightNo'], 0, 2);
                    $flightinfo[$key]['planeType'] = $v['planeType'];
                    $flightinfo[$key]['dstJetquay'] = (string)$v['dstJetquay'];
                    $flightinfo[$key]['orgJetquay'] = (string)$v['orgJetquay'];
                    $flightinfo[$key]['fuelTax'] = $listAllData['flightItems']['audletFuelTax'];
                    $flightinfo[$key]['airportTax'] = $listAllData['flightItems']['audletAirportTax'];
                    $flightinfo[$key]['share'] = $v['codeShare'];
                    $flightinfo[$key]['stop'] = $v['stopnum'];
                    $flightinfo[$key]['day'] = $v['param1'];
                    $days = $v['param1'] . " " . $v['depTime'];
                    $flightinfo[$key]['days'] = date("YmdHis", strtotime($days));

                    if (isset($v['seatItems'][0])) {
                        $seatItems = $v['seatItems'];
                    } else {
                        $seatItems[0] = $v['seatItems'];
                    }

                    //仓位重组
                    foreach ($seatItems as $k => $sv) {
                        $flightinfo[$key]['price'][$k]['id'] = $k;
                        $flightinfo[$key]['price'][$k]['seatMsg'] = $sv['seatMsg'];
                        $flightinfo[$key]['price'][$k]['ticketnum'] = $sv['seatStatus'];
                        $flightinfo[$key]['price'][$k]['agio'] = $sv['discount'];
                        $flightinfo[$key]['price'][$k]['seatCode'] = $sv['seatCode'];
                        $flightinfo[$key]['price'][$k]['policyId'] = $sv['policyData']['policyId'];
                        $flightinfo[$key]['price'][$k]['commisionMoney'] = $sv['policyData']['commisionMoney'];
                        $flightinfo[$key]['price'][$k]['needSwitchPNR'] = $sv['policyData']['needSwitchPNR'];
                        //价格重组
                        $flightinfo[$key]['price'][$k]['parPrice'] = $sv['parPrice'];
                        $flightinfo[$key]['price'][$k]['settlePrice'] = $sv['settlePrice'] + $serviceFee;
                        //顾问所赚利润
                        $flightinfo[$key]['price'][$k]['earnPrice'] = $sv['parPrice'] - ($sv['settlePrice'] + $serviceFee);
                    }
                }
            } else {
                $v = $listAllData['flightItems']['flights'];
                //航班信息重组
                $flightinfo[0]['orgCity'] = $v['orgCity'];
                $flightinfo[0]['depname'] = $this->nameToAirport($v['orgCity']);
                $flightinfo[0]['depTime'] = $v['depTime'];
                $flightinfo[0]['arrDate'] = $v['param1'];
                $flightinfo[0]['dstCity'] = $v['dstCity'];
                $flightinfo[0]['arrname'] = $this->nameToAirport($v['dstCity']);
                $flightinfo[0]['arriTime'] = $v['arriTime'];
                $flightinfo[0]['flightNo'] = $v['flightNo'];
                $flightinfo[0]['flightNoLetter'] = substr($v['flightNo'], 0, 2);
                $flightinfo[0]['planeType'] = $v['planeType'];
                $flightinfo[0]['dstJetquay'] = (string)$v['dstJetquay'];
                $flightinfo[0]['orgJetquay'] = (string)$v['orgJetquay'];
                $flightinfo[0]['fuelTax'] = $listAllData['flightItems']['audletFuelTax'];
                $flightinfo[0]['airportTax'] = $listAllData['flightItems']['audletAirportTax'];
                $flightinfo[0]['share'] = $v['codeShare'];
                $flightinfo[0]['stop'] = $v['stopnum'];
                $flightinfo[0]['day'] = $v['param1'];
                $days = $v['param1'] . " " . $v['depTime'];
                $flightinfo[0]['days'] = date("YmdHis", strtotime($days));

                if (isset($v['seatItems'][0])) {
                    $seatItems = $v['seatItems'];
                } else {
                    $seatItems[0] = $v['seatItems'];
                }

                //仓位重组
                foreach ($seatItems as $k => $sv) {
                    $flightinfo[0]['price'][$k]['id'] = $k;
                    $flightinfo[0]['price'][$k]['seatMsg'] = $sv['seatMsg'];
                    $flightinfo[0]['price'][$k]['ticketnum'] = $sv['seatStatus'];
                    $flightinfo[0]['price'][$k]['agio'] = $sv['discount'];
                    $flightinfo[0]['price'][$k]['seatCode'] = $sv['seatCode'];
                    $flightinfo[0]['price'][$k]['policyId'] = $sv['policyData']['policyId'];
                    $flightinfo[0]['price'][$k]['commisionMoney'] = $sv['policyData']['commisionMoney'];
                    $flightinfo[0]['price'][$k]['needSwitchPNR'] = $sv['policyData']['needSwitchPNR'];
                    //价格重组
                    $flightinfo[0]['price'][$k]['parPrice'] = $sv['parPrice'];
                    $flightinfo[0]['price'][$k]['settlePrice'] = $sv['settlePrice'] + $serviceFee;
                    $flightinfo[0]['price'][$k]['earnPrice'] = $sv['parPrice'] - ($sv['settlePrice'] + $serviceFee);
                }
            }

            $nowDay = date("Ymd", time());
            $nowDate = date('Ymd', strtotime("$date"));

            if ($nowDate < $nowDay) {
                $departure = $this->nameToAirport(I('departure'));
                $destination = $this->nameToAirport(I('destination'));
                $msg = '很抱歉，暂未查询到符合您要求的航班。您可以调整出行时间或目的地重新查询。';
                $this->assign('msg', $msg);
                $this->assign('departure', $departure);
                $this->assign('destination', $destination);
                $this->display("Flight/flightQueryError");
                exit();
            }


            $domain = $_SERVER['SERVER_NAME'];
            $domain = "http://" . $domain;
            $this->assign('domain', $domain);

            $agencyInfo = $this->userInfo;
            $mobile = $agencyInfo['mobile'];

            $this->assign('mobile', $mobile);
            $now = date("YmdHis", strtotime('+2 hour'));
            $this->assign('now', $now);
            $this->assign('nowDay', $nowDay);
            $this->assign('nowDate', $nowDate);
            $this->assign('date', $date);
            $this->assign('flightinfo', $flightinfo);
            $this->display();
        } else {
            $this->display();
        }
    }


    /**
     * 创建订单页-显示
     */
    public function showOrderCreateInfo()
    {
        if (IS_POST) {
            $type = I('type');
            $policyId = I('policyId');
            //航班日期
            $date = I('date');
            $flightNo = I('flightNo');
            $planeModel = I('planeType');
            $seatCode = I('seatCode');
            $arrDate = I('arrDate');
            $depTime = I('depTime');
            $arriTime = I('arriTime');
            $orgCity = I('orgCity');
            $dstCity = I('dstCity');
            $settlePrice = I('settlePrice');
            $settlePrice = Utils::getFen($settlePrice);
            $parPrice = Utils::getFen(I('parPrice'));
            $commisionMoney = I('commisionMoney');
            $needSwitchPNR = I('needSwitchPNR');

            $orgJetquay = I('orgJetquay');
            $dstJetquay = I('dstJetquay');
            $fuelTax = I('fuelTax');
            $fuelTax = Utils::getFen($fuelTax);

            $airportTax = I('airportTax');
            $airportTax = Utils::getFen($airportTax);

            $totalPrice = $settlePrice + $fuelTax + $airportTax;

            $_SESSION['flightOrderCreate'] = 400;

            $this->assign('arrDate', $arrDate);
            $this->assign('orgJetquay', $orgJetquay);
            $this->assign('dstJetquay', $dstJetquay);
            $this->assign('date', $date);
            $this->assign('flightNo', $flightNo);
            $this->assign('planeModel', $planeModel);
            $this->assign('seatCode', $seatCode);
            $this->assign('depTime', $depTime);
            $this->assign('arriTime', $arriTime);
            $this->assign('orgCity', $orgCity);
            $this->assign('dstCity', $dstCity);
            $this->assign('settlePrice', $settlePrice);
            $this->assign('parPrice', $parPrice);
            $this->assign('fuelTax', $fuelTax);
            $this->assign('airportTax', $airportTax);
            $this->assign('totalPrice', $totalPrice);
            $this->assign('commisionMoney', $commisionMoney);
            $this->assign('needSwitchPNR', $needSwitchPNR);
            $this->assign('type', $type);
            $this->assign('policyId', $policyId);
            $this->display("Flight/orderCreate");
        }
    }

    /**
     * post提交 创建订单
     */
    public function orderCreate()
    {
        if (IS_POST && IS_AJAX) {
            $agencyInfo = $this->userInfo;

            if ($agencyInfo['account_state'] != 1) {
                $data['status'] = 0;
                $data['msg'] = '账户被冻结, 请联系管理员';
                $this->ajaxReturn($data);
            }

            $passengerIds = array_diff(explode(',', I('passengerIds')), explode(',', I('delId')));
            $names = '';
            $numSum = 0;
            $outOrderNo = "S" . rand(1000, 9999) . time();

            foreach ($passengerIds as $id) {
                $info = M('frequent_passenger')->where("id = '{$id}'")->find();
                $where['identity_no'] = $info['identity_no'];
                $where['flight_no'] = I('flightNo');
                $where['dep_date'] = I('depDate');
                $where['agency_id'] = $agencyInfo['id'];
                $detailNum = M('order_flight_detail')->where("state in(14,15,2,3)")->where($where)->find();

                if ($detailNum) {
                    $names .= $detailNum['passenger_name'] . ",";
                    $numSum += 1;
                    $data['status'] = 0;
                    $data['msg'] = "您有未完成的订单: 乘客 " . $names . " 重复下单";
                    $this->ajaxReturn($data);
                }

                $seatCode = I('seatCode');
                $depCode = I('depCode');
                $arrCode = I('arrCode');
                $depDate = I('depDate');
                $flightNo = I('flightNo');

                $parPrice = I('parPrice');
                $settlePrice = I('settlePrice');
                $fuelTax = I('fuelTax');
                $airportTax = I('airportTax');

                $agencyId = $agencyInfo['id'];

                $orderFlightData['passenger_name'] = Utils::trimall($info['passenger_name']);
                $orderFlightData['passenger_type'] = $info['passenger_type'];
                $orderFlightData['identity_type'] = $info['identity_type'];
                $orderFlightData['identity_no'] = $info['identity_no'];
                $orderFlightData['birthday'] = $info['birthday'];
                $orderFlightData['agency_id'] = $agencyId;
                $orderFlightData['flight_no'] = $flightNo;
                $orderFlightData['dep_code'] = $depCode;
                $orderFlightData['arr_code'] = $arrCode;
                $orderFlightData['seat_class'] = $seatCode;
                $orderFlightData['dep_date'] = $depDate;
                $orderFlightData['dep_time'] = I('depTime');
                $orderFlightData['arr_time'] = I('arrTime');
                $orderFlightData['plane_model'] = I('planeModel');
                $orderFlightData['settle_price'] = $settlePrice;
                $orderFlightData['par_price'] = $parPrice;
                $orderFlightData['fuel_tax'] = $fuelTax;
                $orderFlightData['airport_tax'] = $airportTax;
                $orderFlightData['policy_id'] = I('policyId');
                $orderFlightData['link_man'] = I('linkMan');
                $orderFlightData['link_phone'] = I('linkPhone');
                $orderFlightData['out_order_no'] = $outOrderNo;
                $orderFlightData['service_fee'] = Utils::getFen(C('SERVICE_FEE'));
                $orderFlightData['state'] = 1;
                $orderFlightData['create_time'] = date("Y-m-d H:i:s", time());
                $result = M('order_flight_detail')->add($orderFlightData);
            }

            $postCode = I('flightOrderCreate');
            $code = session('flightOrderCreate');

            if ($code == $postCode && $numSum == 0) {
                session('flightOrderCreate', null);
                $type = I('type');
                $orderData['agency_name'] = $agencyInfo['name'];
                $orderData['passenger_total'] = count($passengerIds);

                $passengerTotal = count(M('order_flight_detail')->where("out_order_no = '{$outOrderNo}' && passenger_type = '{$type}'")->select());

                if ($type == 0) {
                    $orderData['passenger_adult'] = $passengerTotal;
                } else {
                    $orderData['passenger_child'] = $passengerTotal;
                }

                $orderData['price_total'] = $passengerTotal * $settlePrice + $passengerTotal * $airportTax + $passengerTotal * $fuelTax;

                $passengerNamesList = M('order_flight_detail')->where("out_order_no = '{$outOrderNo}'")->field('passenger_name')->select();
                $passengerNamesData = array_column($passengerNamesList, 'passenger_name');
                $orderData['passenger_names'] = implode(',', $passengerNamesData);

                $orderData['agency_id'] = $agencyId;
                $orderData['operator_id'] = $agencyInfo['operator_id'];
                $orderData['flight_no'] = I('flightNo');
                $orderData['dep_code'] = I('depCode');
                $orderData['arr_code'] = I('arrCode');
                $orderData['dep_date'] = I('depDate') . " " . I('depTime');
                $orderData['arr_date'] = I('arrDate') . " " . I('arrTime');
                $orderData['org_jetquay'] = I('orgJetquay');
                $orderData['dst_jetquay'] = I('dstJetquay');
                $orderData['state'] = 1;
                $orderData['create_time'] = date("Y-m-d H:i:s", time());

                $orderFlightResult = M('order_flight')->add($orderData);

                $orderFlightDetailData['order_flight_id'] = $orderFlightResult;
                $orderFlightDataResult = M('order_flight_detail')->where("out_order_no = '{$outOrderNo}'")->save($orderFlightDetailData);

                //向平台提交订单
                $orderFlightList = M('order_flight_detail')->where("agency_id = '{$agencyId}' && order_flight_id = '{$orderFlightResult}' && passenger_type = '{$type}'")->select();

                foreach ($orderFlightList as $val) {
                    $passengers[] = array(
                        'name' => str_replace(' ', '/', $val['passenger_name']),
                        'type' => (int)$val['passenger_type'],
                        'identityType' => $val['identity_type'],
                        'identityNo' => $val['identity_no'],
                        'birthday' => $val['birthday'],
                    );
                }

                foreach ($orderFlightList as $val) {
                    //航段信息
                    $segments = array(
                        'flightNo' => $val['flight_no'],
                        'depCode' => $val['dep_code'],
                        'arrCode' => $val['arr_code'],
                        'seatClass' => $val['seat_class'],
                        'depDate' => $val['dep_date'],
                        'depTime' => $val['dep_time'],
                        'arrTime' => $val['arr_time'],
                        'planeModel' => $val['plane_model'],
                    );
                }

                $passengers[0]['param1'] = I('linkPhone');

                //提交
                $pnrInfo = array(
                    'segments' => $segments,
                    'passengers' => $passengers,
                    'parPrice' => Utils::getYuan(I('parPrice')),
                    'fuelTax' => Utils::getYuan(I('fuelTax')),
                    'airportTax' => Utils::getYuan(I('airportTax')),
                );

                $outOrderNo = "S" . rand(1000, 9999) . time();
                $policyId = I('policyId');
                $orderData = array(
                    'agencyCode' => $this->agencyCode(),
                    'policyId' => $policyId,
                    'sign' => md5($this->agencyCode() . $policyId . $this->sign()),
                    'linkMan' => I('linkMan'),
                    'linkPhone' => I('linkPhone'),
                    'notifiedUrl' => 'http://zhangda.m.lydlr.com/index.php/Home/Flight/orderSuccessNotify',
                    'paymentReturnUrl' => 'http://zhangda.m.lydlr.com/index.php/Home/Flight/paymentReturn',
                    'outOrderNo' => $outOrderNo,
                    'pnrInfo' => $pnrInfo,
                    'allowSwitchPolicy' => 0,
                    'needSpeRulePolicy' => 0,
                );


                if ($orderFlightList) {
                    $orderResult = Api_51::orderCreate($orderData);
                    $orderArr = $this->object_to_array($orderResult);

                    //返回值
                    $returnCode = $orderArr['return']['returnCode'];

                    if ($returnCode !== 'S') {
                        $data['status'] = 0;
                        $data['msg'] = $orderArr['return']['returnMessage'];
                        $this->ajaxReturn($data);
                    }
                }

                $orderData['state'] = Top::FlightOrderStateWaitingForPayment;

                $adultLiantuoOrderNo = $orderArr['return']['order']['liantuoOrderNo'];
                $orderData['sequence_no'] = (string)$adultLiantuoOrderNo;
                $adultOrderData['sequence_no'] = (string)$adultLiantuoOrderNo;
                $adultOrderData['state'] = Top::FlightOrderStateWaitingForPayment;
                $flightDetailSequenceNo = M('order_flight_detail')->where("order_flight_id = '{$orderFlightResult}' && agency_id = '{$agencyId}'")->save($adultOrderData);

                $orderFlightUpdateResult = M('order_flight')->where("id = '{$orderFlightResult}'")->save($orderData);
                //提交结束

                $data['status'] = 1;
                $data['id'] = $orderFlightResult;
                $data['msg'] = '订单创建成功, 点击确认支付';
                $this->ajaxReturn($data);
            }
        } else {
            $passengerIds = I('passengerId');
            $where['id'] = array('in', $passengerIds);
            $passengerType = I('type');
            $passengerList = M('frequent_passenger')->where($where)->select();
            $passengerNum = M('frequent_passenger')->where($where)->where("passenger_type = '{$passengerType}'")->count();

            //航班日期
            $date = I('depDate');
            $flightNo = I('flightNo');
            $planeModel = I('planeModel');
            $seatCode = I('seatCode');
            $arrDate = I('arrDate');
            $depTime = I('depTime');
            $arriTime = I('arrTime');
            $orgCity = I('depCode');
            $dstCity = I('arrCode');

            $parPrice = I('parPrice');
            $settlePrice = I('settlePrice');
            $commisionMoney = I('commisionMoney');
            $needSwitchPNR = I('needSwitchPNR');
            $orgJetquay = I('orgJetquay');
            $dstJetquay = I('dstJetquay');
            $fuelTax = I('fuelTax');
            $airportTax = I('airportTax');
            $policyId = I('policyId');
            $type = I('type');

            $totalPrice = $settlePrice + $fuelTax + $airportTax;

            $_SESSION['flightOrderCreate'] = 400;

            $this->assign('arrDate', $arrDate);
            $this->assign('orgJetquay', $orgJetquay);
            $this->assign('dstJetquay', $dstJetquay);
            $this->assign('date', $date);
            $this->assign('flightNo', $flightNo);
            $this->assign('planeModel', $planeModel);
            $this->assign('seatCode', $seatCode);
            $this->assign('depTime', $depTime);
            $this->assign('arriTime', $arriTime);
            $this->assign('orgCity', $orgCity);
            $this->assign('dstCity', $dstCity);
            $this->assign('parPrice', $parPrice);
            $this->assign('settlePrice', $settlePrice);
            $this->assign('fuelTax', $fuelTax);
            $this->assign('airportTax', $airportTax);
            $this->assign('totalPrice', $totalPrice);
            $this->assign('commisionMoney', $commisionMoney);
            $this->assign('needSwitchPNR', $needSwitchPNR);
            $this->assign('passengerList', $passengerList);
            $this->assign('passengerNum', $passengerNum);
            $this->assign('passengerIds', $passengerIds);
            $this->assign('policyId', $policyId);
            $this->assign('type', $type);
            $this->display();
        }
    }


    /**
     * 获取政策
     */
    public function getPassengerPolicyAndFlight($depCode = 'PEK', $arrCode = 'SHA', $flightNo = '0', $depDate = '2016-10-12', $seatClass = 'Y/C/F', $passengerType = 'CHILD')
    {
        $data['agencyCode'] = $this->agencyCode();
        $data['depCode'] = $depCode;
        $data['arrCode'] = $arrCode;
        $data['flightNo'] = $flightNo;
        $data['depDate'] = $depDate;
        $data['passengerType'] = $passengerType;
        $data['seatClass'] = $seatClass;
        $sign = $this->agencyCode() . $data['arrCode'] . $data['depCode'] . $this->sign();
        $data['sign'] = strtolower(md5($sign));

        $result = $this->object_to_array(Api_51::getPassengerPolicyAndFlightService($data));

        $returnCode = $result['return']['returnCode'];
        if ($returnCode == 'F') {
            $passengerPolicyInfo['returnCode'] = $returnCode;
            $passengerPolicyInfo['policyId'] = $result['return']['returnMessage'];
        } else {
            $passengerPolicyInfo['returnCode'] = $returnCode;
            $passengerPolicyInfo['policyId'] = $result['return']['flightDataList']['seatAndPolicyList']['policyList'][0]['policyId'];
            $passengerPolicyInfo['settlePrice'] = $result['return']['flightDataList']['seatAndPolicyList']['policyList'][0]['setChildPrice'];
            $passengerPolicyInfo['parPrice'] = $result['return']['flightDataList']['seatAndPolicyList']['ticketPrice'];
            $passengerPolicyInfo['airportTax'] = $result['return']['flightDataList']['childAirportTax'];
            $passengerPolicyInfo['fuelTax'] = $result['return']['flightDataList']['childFuelTax'];
        }

        return $passengerPolicyInfo;
    }


    /**
     * 订单取消
     */
    public function orderCancel()
    {
        if (IS_POST && IS_AJAX) {
            $id = I('id');
            $where['id'] = $id;
            $flightInfo = M('order_flight')->where($where)->find();
            $sequenceNos = explode(';', $flightInfo['sequence_no']);

            foreach ($sequenceNos as $v) {
                $url = $this->orderCancelUrl();
                $data['agencyCode'] = $this->agencyCode();
                $data['orderNo'] = $v;    //指平台订单号 (liantuoOrderNo)
                $data['canclePNR'] = 1;   //1同时取消PNR;0不取消PNR;
                $sign = $this->agencyCode() . $data['canclePNR'] . $data['orderNo'] . $this->sign();
                $data['sign'] = strtolower(md5($sign));

                //返回 xml 格式
                $result = Utils::vpost($url, $data);
                //转换成数组
                $listAllData = $this->xml_to_array($result);
                $returnCode = $listAllData['returnCode'][0];
            }

            if ($returnCode == 'S') {
                $state['state'] = Top::FlightOrderStateCanceled;
                $flightResult = M('order_flight')->where($where)->save($state);
                $detailRestult = M('order_flight_detail')->where("order_flight_id = '{$id}'")->save($state);

                if ($flightResult && $detailRestult) {
                    $data['status'] = 1;
                    $data['msg'] = '取消成功';
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 0;
                $data['msg'] = $listAllData['returnMessage'][0];
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 订单详情
     */
    public function orderDetail()
    {
        $_SESSION['flightOrderCode'] = 400;

        $id = I('get.id');
        $orderFlightInfo = M('order_flight')->where("id = '{$id}'")->find();

        $orgJetquay = $orderFlightInfo['org_jetquay'];
        $dstJetquay = $orderFlightInfo['dst_jetquay'];

        $flightOrderList = M("order_flight_detail")->where("order_flight_id = '{$id}'")->select();
        $depCode = $flightOrderList[0]['dep_code'];
        $arrCode = $flightOrderList[0]['arr_code'];
        $flightNo = $flightOrderList[0]['flight_no'];
        $planeModel = $flightOrderList[0]['plane_model'];
        $depDate = $flightOrderList[0]['dep_date'];
        $depTime = $flightOrderList[0]['dep_time'];
        $arrTime = $flightOrderList[0]['arr_time'];
        $linkMan = $flightOrderList[0]['link_man'];
        $linkPhone = $flightOrderList[0]['link_phone'];
        $priceTotal = Utils::getYuan($orderFlightInfo['price_total']);

        foreach ($flightOrderList as $k => $v) {
            $orderList[$k]['passenger_name'] = $v['passenger_name'];
            $orderList[$k]['passenger_type'] = $v['passenger_type'];
            $orderList[$k]['price_total'] = Utils::getYuan($v['settle_price'] + $v['fuel_tax'] + $v['airport_tax']);
            $orderList[$k]['child_price_total'] = Utils::getYuan($v['settle_price'] + $v['fuel_tax']);
            $orderList[$k]['par_price'] = Utils::getYuan($v['settle_price']);
            $orderList[$k]['fuel_tax'] = Utils::getYuan($v['fuel_tax']);
            $orderList[$k]['airport_tax'] = Utils::getYuan($v['airport_tax']);
        }

        $specialParPrice = Utils::getYuan($flightOrderList[0]['par_price']);
        $seatCode = $flightOrderList[0]['seat_class'];

        $this->assign('orgJetquay', $orgJetquay);
        $this->assign('dstJetquay', $dstJetquay);
        $this->assign('priceTotal', $priceTotal);
        $this->assign('depCode', $depCode);
        $this->assign('arrCode', $arrCode);
        $this->assign('flightNo', $flightNo);
        $this->assign('planeModel', $planeModel);
        $this->assign('depDate', $depDate);
        $this->assign('depTime', $depTime);
        $this->assign('arrTime', $arrTime);
        $this->assign('linkMan', $linkMan);
        $this->assign('linkPhone', $linkPhone);
        $this->assign('orderList', $orderList);
        $this->assign('flightId', $id);
        $this->assign('orderFlightInfo', $orderFlightInfo);
        $this->assign('seatCode', $seatCode);
        $this->assign('specialParPrice', $specialParPrice);
        $this->display();
    }


    /**
     * 订单支付
     */
    public function orderPay()
    {
        if (IS_POST && IS_AJAX) {
            $flightId = I('flightId');
            $keyid = Utils::getDecrypt(I('keyid'));

            if ($flightId != $keyid) {
                $data['status'] = 8;
                $data['msg'] = '支付失败';
                $this->ajaxReturn($data);
            }

            $flightOrderInfo = M('order_flight')->where("id = '{$flightId}'")->find();
            $priceTotal = $flightOrderInfo['price_total'];
            $flightOrderState = $flightOrderInfo['state'];

            $operatorId = $flightOrderInfo['operator_id'];
            $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
            $flightAvailableFen = $operatorInfo['account_credit_balance'];
            $operatorState = $operatorInfo['state'];

            if ($operatorState != 1) {
                $data['status'] = 8;
                $data['msg'] = '运营商被禁用';
                $this->ajaxReturn($data);
            }

            $sysInfo = M('sys')->where("id = 1")->find();
            $sysCreditBalance = $sysInfo['account_credit_balance'];

            if ($sysCreditBalance < $priceTotal) {
                $data['status'] = 8;
                $data['msg'] = '系统授信额度不足';
                $this->ajaxReturn($data);
            }

            $entityId = $flightOrderInfo['agency_id'];
            //当前顾问可使用额度
            $agencyAvailableFen = Utils::getAgencyAvailableFen($entityId);

            $agencyInfo = M('agency')->where("id = '{$entityId}'")->find();

            if ($agencyInfo['account_state'] == '1') {
                if ($flightOrderState == '14') {
                    //如果当前顾问可用额度大于订单金额
                    if ($agencyAvailableFen >= $priceTotal) {
                        if ($flightAvailableFen >= $priceTotal) {
                            $postCode = I('code');
                            $code = session('flightOrderCode');

                            if ($code == $postCode) {
                                session('flightOrderCode', null);

                                M('operator')->startTrans();
                                try {
                                    $paying['state'] = Top::FlightOrderStatePaying;
                                    M('order_flight')->where("agency_id = '{$entityId}' && id = '{$flightId}'")->save($paying);
                                    M('order_flight_detail')->where("agency_id = '{$entityId}' && order_flight_id = '{$flightId}'")->save($paying);
                                    //详情
                                    $orderFlightList = M('order_flight_detail')->where("agency_id = '{$entityId}' && order_flight_id = '{$flightId}'")->select();

                                    $mobile = $orderFlightList[0]['link_phone'];

                                    $agencyResult = M('agency')->where("id = '{$entityId}'")->setDec('account_balance', $priceTotal);

                                    $agencyList = M('agency')->where("id = '{$entityId}'")->find();
                                    $nowTime = Utils::getBatch();
                                    $agencyData['operator_id'] = $agencyList['operator_id'];
                                    $agencyData['action'] = '支出机票订单金额';
                                    $agencyData['entity_type'] = Top::EntityTypeAgency;
                                    $agencyData['entity_id'] = $entityId;
                                    $agencyData['account'] = $agencyList['account'];
                                    $agencyData['sn'] = Utils::getSn();
                                    $agencyData['amount'] = '-' . $priceTotal;
                                    $agencyData['balance'] = $agencyList['account_balance'];
                                    $agencyData['create_time'] = date('Y-m-d H:i:s', time());
                                    $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
                                    $agencyData['object_id'] = $flightId;
                                    $agencyData['object_type'] = Top::ObjectTypeFlight;
                                    $agencyData['object_name'] = '机票';
                                    $agencyDelmoney = M('account_transaction')->add($agencyData);

                                    $operatorId = $agencyInfo['operator_id'];
                                    $operatorResult = M('operator')->where("id = '{$operatorId}'")->setDec('account_credit_balance', $priceTotal);

                                    $operatorList = M('operator')->where("id = '{$operatorId}'")->find();
                                    $agencyData['operator_id'] = $operatorId;
                                    $agencyData['action'] = '支出机票订单金额';
                                    $agencyData['entity_type'] = Top::EntityTypeOperator;
                                    $agencyData['entity_id'] = $operatorId;
                                    $agencyData['account'] = $operatorList['account'];
                                    $agencyData['sn'] = Utils::getSn();
                                    $agencyData['batch'] = $nowTime;
                                    $agencyData['amount'] = '-' . $priceTotal;
                                    $agencyData['balance'] = $operatorList['account_credit_balance'];
                                    $agencyData['create_time'] = date('Y-m-d H:i:s', time());
                                    $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
                                    $agencyData['object_id'] = $flightId;
                                    $agencyData['object_type'] = Top::ObjectTypeFlight;
                                    $agencyData['object_name'] = '机票';
                                    $operatorDeloney = M('operator_credit_transaction')->add($agencyData);

                                    $sysResult = M('sys')->where("id = 1")->setDec('account_credit_balance', $priceTotal);
                                    $sysList = M('sys')->where("id = 1")->find();
                                    $sysData['operator_id'] = 0;
                                    $sysData['action'] = '支出机票订单金额';
                                    $sysData['entity_type'] = Top::EntityTypeSystem;
                                    $sysData['entity_id'] = 1;
                                    $sysData['account'] = $sysList['account'];
                                    $sysData['sn'] = Utils::getSn();
                                    $sysData['batch'] = $nowTime;
                                    $sysData['amount'] = '-' . $priceTotal;
                                    $sysData['balance'] = $sysList['account_credit_balance'];
                                    $sysData['create_time'] = date('Y-m-d H:i:s', time());
                                    $sysData['remark'] = 'top_order_flight.id:' . $flightId;
                                    $sysData['object_id'] = $flightId;
                                    $sysData['object_type'] = Top::ObjectTypeFlight;
                                    $sysData['object_name'] = '机票';
                                    $sysDeloney = M('flight_transaction')->add($sysData);

                                    $serviceFee = Utils::getFen(C('SERVICE_FEE')) * $flightOrderInfo['passenger_total'];
                                    $sysResult = M('sys')->where("id = 1")->setInc('account_credit_balance', $serviceFee);
                                    $sysList = M('sys')->where("id = 1")->find();
                                    $serviceData['operator_id'] = 0;
                                    $serviceData['action'] = '退还服务费';
                                    $serviceData['entity_type'] = Top::EntityTypeSystem;
                                    $serviceData['entity_id'] = 1;
                                    $serviceData['account'] = $sysList['account'];
                                    $serviceData['sn'] = Utils::getSn();
                                    $serviceData['batch'] = $nowTime;
                                    $serviceData['amount'] = $serviceFee;
                                    $serviceData['balance'] = $sysList['account_credit_balance'];
                                    $serviceData['create_time'] = date('Y-m-d H:i:s', time());
                                    $serviceData['remark'] = 'top_order_flight.id:' . $flightId;
                                    $serviceData['object_id'] = $flightId;
                                    $serviceData['object_type'] = Top::ObjectTypeFlight;
                                    $serviceData['object_name'] = '机票';
                                    $serviceIncMoney = M('flight_transaction')->add($serviceData);

                                    $url = $this->orderPayUrl();
                                    $data['agencyCode'] = $this->agencyCode();
                                    $data['orderNo'] = $orderFlightList[0]['sequence_no'];                                         //提交订单后,得到的订单号
                                    $data['payType'] = '1';
                                    $data['payerLoginName'] = 'bjskt';                                    //默认平台登录号
                                    $sign = $this->agencyCode() . $data['orderNo'] . $data['payType'] . $data['payerLoginName'] . $this->sign();
                                    $data['sign'] = strtolower(MD5($sign));
                                    //返回 xml 格式
                                    $result = Utils::vpost($url, $data);
                                    $listAllData = $this->xml_to_array($result);
                                    $returnCode = $listAllData['returnCode'][0];

                                    if ($returnCode == 'F') {
                                        $data['msg'] = $listAllData['returnMessage'][0];
                                        $data['status'] = 5;
                                        $this->ajaxReturn($data);
                                    }


                                    $payed['state'] = Top::FlightOrderStatePayed;
                                    M('order_flight')->where("agency_id = '{$entityId}' && id = '{$flightId}'")->save($payed);
                                    M('order_flight_detail')->where("agency_id = '{$entityId}' && order_flight_id = '{$flightId}'")->save($payed);

                                    $orderNo = $orderFlightList[0]['sequence_no'];
                                    Sms::Send($mobile, "您已成功预定机票,订单号:" . $orderNo . ",出票结果将在几分钟后短信通知您", $this->cdkey, $this->pwd, $this->signature, $this->smsOperatorId);

                                    $orgJetquay = $flightOrderInfo['org_jetquay'];
                                    $dstJetquay = $flightOrderInfo['dst_jetquay'];


                                    if ($orgJetquay && $orgJetquay != 'Array' && $orgJetquay != '--') {
                                        $orgJetquay = $orgJetquay;
                                    } else {
                                        $orgJetquay = '';
                                    }

                                    if ($dstJetquay && $dstJetquay != 'Array' && $dstJetquay != '--') {
                                        $dstJetquay = $dstJetquay;
                                    } else {
                                        $dstJetquay = '';
                                    }

                                    $depCity = $flightOrderInfo['dep_code'];
                                    if ($depCity == 'NAY') {
                                        $depCity = '北京';
                                    } else {
                                        $depCity = $this->nameToAirport($flightOrderInfo['dep_code']);
                                    }

                                    $arrCity = $flightOrderInfo['dep_code'];
                                    if ($arrCity == 'PVG') {
                                        $arrCity = '上海';
                                    } else {
                                        $arrCity = $this->nameToAirport($flightOrderInfo['arr_code']);
                                    }

                                    $depDate = $flightOrderInfo['dep_date'] . "起飞";
                                    $arrDate = $flightOrderInfo['arr_date'] . "到达";

                                    $flightNo = $this->nameToAirLine(substr($flightOrderInfo['flight_no'], 0, 2)) . substr($flightOrderInfo['flight_no'], 0);
                                    $depCode = $depCity . $this->nameToFullAirport($flightOrderInfo['dep_code']);
                                    $arrCode = $arrCity . $this->nameToFullAirport($flightOrderInfo['arr_code']);
                                    $message = $flightNo . " , " . $depCode . $orgJetquay . " - " . $arrCode . $dstJetquay . " , " . $depDate . " , " . $arrDate;

                                    $template = array(
                                        'touser' => $agencyList['weixinmp_openid'],
                                        'template_id' => Utils::flightPaySuccessTemplateId(),

                                        'data' => array(
                                            'first' => array('value' => urlencode("亲，您已成功预定机票，出票结果将在几分钟后通知您。"), 'color' => "#000"),
                                            'OrderID' => array('value' => urlencode($orderNo), 'color' => "#000"),
                                            'PersonName' => array('value' => urlencode($flightOrderInfo['passenger_names']), 'color' => '#000'),
                                            'FlightInfor' => array('value' => urlencode($message), 'color' => '#000'),
                                        )
                                    );
                                    $this->send_tpl($template);

                                    if ($agencyResult && $agencyDelmoney && $operatorResult && $operatorDeloney && $sysResult && $sysDeloney && $serviceIncMoney) {
                                        M('operator')->commit();
                                        $data['status'] = 1;
                                        $this->ajaxReturn($data);
                                    }
                                } catch (Exception $ex) {
                                    M('operator')->rollback();
                                }
                            } else {
                                $data['status'] = 7;
                                $data['msg'] = '请勿重复提交';
                                $this->ajaxreturn($data);
                            }
                        } else {
                            $data['status'] = 8;
                            $data['msg'] = $operatorInfo['msg1'];
                            $this->ajaxReturn($data);
                        }
                    } else {
                        $data['status'] = 2;
                        $this->ajaxReturn($data);
                    }
                } else {
                    $data['status'] = 3;
                    $this->ajaxReturn($data);
                }
            } else {
                $data['status'] = 6;
                $this->ajaxReturn($data);
            }
        }
    }


    /**
     * 订票 URL 通知
     */
    public function orderSuccessNotify()
    {
        $data['type'] = I('get.type');
        $data['sequence_no'] = I('get.sequenceNo');
        $data['passenger_names'] = I('get.passengerNames');
        $data['ticket_nos'] = I('get.ticketNos');
        $data['ticket_price'] = I('get.ticketPrice');
        $data['fuel_tax'] = I('get.fuelTax');
        $data['airport_tax'] = I('get.airportTax');
        $data['settle_price'] = I('get.settlePrice');
        $data['pnr_no'] = I('get.pnrNo');
        $data['old_pnr_no'] = I('get.oldPnrNo');
        $data['reason'] = I('get.reason');
        $data['refund_no'] = I('get.refundNo');
        $data['order_no'] = I('get.orderNo');
        $data['refund_time'] = I('get.refundTime');
        $data['refund_price'] = I('get.refundPrice');
        $data['flight_no'] = I('flightNo');
        $data['content'] = I('get.content');
        $data['create_time'] = date("Y-m-d H:i:s", time());

        $sequenceNo = $data['sequence_no'];
        $orderFlightDetails = M('order_flight_detail')->where("sequence_no = '{$sequenceNo}'")->limit(1)->find();
        $orderFlightInfo = M('order_flight')->where("sequence_no = '{$sequenceNo}'")->limit(1)->find();

        $entityId = $orderFlightInfo['agency_id'];
        $agencyInfo = M('agency')->where("id = '{$entityId}'")->find();

        $mobile = $orderFlightDetails['link_phone'];

        if ($data['type'] == '1') {
            $where['sequence_no'] = $data['sequence_no'];
            $orderFlightData['state'] = Top::FlightOrderStateTicked;
            $orderFlightData['ticket_no'] = $data['ticket_nos'];
            $orderFlightResult = M('order_flight')->where($where)->save($orderFlightData);

            $orderFlightDetailList = M('order_flight_detail')->where($where)->field('id')->select();

            $ticketNos = explode(",", $data['ticket_nos']);
            array_pop($ticketNos);
            $passengerNums = count($ticketNos);

            //判断多人订购还是单人
            if ($passengerNums == 1) {
                $where['id'] = $orderFlightDetailList[0]['id'];
                $orderFlightDetailData['pay_time'] = date('Y-m-d H:i:s', time());
                $orderFlightDetailData['ticket_no'] = $ticketNos[0];
                $orderFlightDetailData['state'] = Top::FlightOrderStateTicked;
                $orderFlightDetailResult = M('order_flight_detail')->where($where)->save($orderFlightDetailData);
            } else {
                for ($i = 0; $i < $passengerNums; $i++) {
                    $where['id'] = $orderFlightDetailList[$i]['id'];
                    $orderFlightDetailData['pay_time'] = date('Y-m-d H:i:s', time());
                    $orderFlightDetailData['state'] = Top::FlightOrderStateTicked;
                    $orderFlightDetailData['ticket_no'] = $ticketNos[$i];
                    $orderFlightDetailResult = M('order_flight_detail')->where($where)->limit(1)->save($orderFlightDetailData);
                }
            }

            $orgJetquay = $orderFlightInfo['org_jetquay'];
            $dstJetquay = $orderFlightInfo['dst_jetquay'];

            if ($orgJetquay && $orgJetquay == 'Array' && $orgJetquay == '--') {
                $orgJetquay = '';
            }

            if ($dstJetquay && $dstJetquay == 'Array' && $dstJetquay == '--') {
                $dstJetquay = '';
            }

            $depCity = $orderFlightInfo['dep_code'];
            if ($depCity == 'NAY') {
                $depCity = '北京';
            } else {
                $depCity = $this->nameToAirport($orderFlightInfo['dep_code']);
            }

            $arrCity = $orderFlightInfo['dep_code'];
            if ($arrCity == 'PVG') {
                $arrCity = '上海';
            } else {
                $arrCity = $this->nameToAirport($orderFlightInfo['arr_code']);
            }

            $depDate = $orderFlightInfo['dep_date'] . "起飞";
            $arrDate = $orderFlightInfo['arr_date'] . "到达";

            $flightNo = $this->nameToAirLine(substr($orderFlightInfo['flight_no'], 0, 2)) . substr($orderFlightInfo['flight_no'], 0);
            $depCode = $depCity . $this->nameToFullAirport($orderFlightInfo['dep_code']);
            $arrCode = $arrCity . $this->nameToFullAirport($orderFlightInfo['arr_code']);
            $message = $flightNo . " , " . $depCode . $orgJetquay . " - " . $arrCode . $dstJetquay . " , " . $depDate . " , " . $arrDate;

            $messageInfo = "亲，您预定的机票已成功出票。您可安心出行，祝您旅途愉快。旅客:" . $orderFlightInfo['passenger_names'] . "; 日期: " . $orderFlightDetails['dep_date'] . "; 航班: " . $orderFlightInfo['flight_no'] . "; 舱位 " . $orderFlightDetails['seat_class'] . "; 起飞: " . substr($orderFlightDetails['dep_time'], 0, 2) . ':' . substr($orderFlightDetails['dep_time'], 2, 2) . $depCode . $orgJetquay . "; 到达: " . substr($orderFlightDetails['arr_time'], 0, 2) . ':' . substr($orderFlightDetails['arr_time'], 2, 2) . $arrCode . $dstJetquay;
            Sms::Send($mobile, $messageInfo, $this->cdkey, $this->pwd, $this->signature, $this->smsOperatorId);

            $template = array(
                'touser' => $agencyInfo['weixinmp_openid'],
                'template_id' => Utils::flightDrawSuccessTemplateId(),

                'data' => array(
                    'first' => array('value' => urlencode("亲，您预定的机票已成功出票。您可安心出行，祝您旅途愉快。"), 'color' => "#000"),
                    'OrderID' => array('value' => urlencode($orderFlightInfo['sequence_no']), 'color' => "#000"),
                    'PersonName' => array('value' => urlencode($orderFlightInfo['passenger_names']), 'color' => '#000'),
                    'Amount' => array('value' => urlencode(Utils::getYuan($orderFlightInfo['price_total'])), 'color' => '#000'),
                    'FlightInfor' => array('value' => urlencode($message), 'color' => '#000'),
                    'remark' => array('value' => urlencode("客服电话 0551-65238888"), 'color' => '#000'),
                )
            );
            $this->send_tpl($template);
        } else if ($data['type'] == '2') {

            $where['sequence_no'] = $data['order_no'];
            $flightOrderInfo = M('order_flight')->where($where)->find();

            $orderFlightData['state'] = Top::FlightOrderStateTicketFailed;
            $orderFlightData['ticket_no'] = $data['ticket_nos'];

            M('order_flight')->where($where)->save($orderFlightData);

            M('order_flight_detail')->where($where)->save($orderFlightData);

            $priceTotal = $flightOrderInfo['price_total'];
            $flightId = $flightOrderInfo['id'];

            if ($flightOrderInfo['state'] == Top::FlightOrderStateTicketFailed || $flightOrderInfo['state'] == Top::FlightOrderStateScheduledFailed) {
                echo 'S';
                exit();
            }

            $agencyResult = M('agency')->where("id = '{$entityId}'")->setInc('account_balance', $priceTotal);

            $agencyList = M('agency')->where("id = '{$entityId}'")->find();
            $nowTime = Utils::getBatch();
            $agencyData['operator_id'] = $agencyList['operator_id'];
            $agencyData['action'] = '冲正:机票订单款';
            $agencyData['entity_type'] = Top::EntityTypeAgency;
            $agencyData['entity_id'] = $entityId;
            $agencyData['account'] = $agencyList['account'];
            $agencyData['sn'] = Utils::getSn();
            $agencyData['batch'] = $nowTime;
            $agencyData['amount'] = $priceTotal;
            $agencyData['balance'] = $agencyList['account_balance'];
            $agencyData['create_time'] = date('Y-m-d H:i:s', time());
            $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
            $agencyData['object_id'] = $flightId;
            $agencyData['object_type'] = Top::ObjectTypeFlight;
            $agencyData['object_name'] = '机票';
            $agencyDelmoney = M('account_transaction')->add($agencyData);

            $operatorId = $agencyList['operator_id'];
            $operatorResult = M('operator')->where("id = '{$operatorId}'")->setInc('account_credit_balance', $priceTotal);

            $operatorList = M('operator')->where("id = '{$operatorId}'")->find();
            $agencyData['operator_id'] = $operatorId;
            $agencyData['action'] = '冲正:机票订单款';
            $agencyData['entity_type'] = Top::EntityTypeOperator;
            $agencyData['entity_id'] = $operatorId;
            $agencyData['account'] = $operatorList['account'];
            $agencyData['sn'] = Utils::getSn();
            $agencyData['batch'] = $nowTime;
            $agencyData['amount'] = $priceTotal;
            $agencyData['balance'] = $operatorList['account_credit_balance'];
            $agencyData['create_time'] = date('Y-m-d H:i:s', time());
            $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
            $agencyData['object_id'] = $flightId;
            $agencyData['object_type'] = Top::ObjectTypeFlight;
            $agencyData['object_name'] = '机票';
            $agencyDelmoney = M('operator_credit_transaction')->add($agencyData);

            $sysResult = M('sys')->where("id = 1")->setInc('account_credit_balance', $priceTotal);

            $sysList = M('sys')->where("id = 1")->find();
            $sysData['operator_id'] = 0;
            $sysData['action'] = '冲正:机票订单款';
            $sysData['entity_type'] = Top::EntityTypeSystem;
            $sysData['entity_id'] = 1;
            $sysData['account'] = $sysList['account'];
            $sysData['sn'] = Utils::getSn();
            $sysData['batch'] = $nowTime;
            $sysData['amount'] = $priceTotal;
            $sysData['balance'] = $sysList['account_credit_balance'];
            $sysData['create_time'] = date('Y-m-d H:i:s', time());
            $sysData['remark'] = 'top_order_flight.id:' . $flightId;
            $sysData['object_id'] = $flightId;
            $sysData['object_type'] = Top::ObjectTypeFlight;
            $sysData['object_name'] = '机票';
            $sysDelmoney = M('flight_transaction')->add($sysData);

            $serviceFee = Utils::getFen(C('SERVICE_FEE')) * $flightOrderInfo['passenger_total'];
            $sysResult = M('sys')->where("id = 1")->setDec('account_credit_balance', $serviceFee);

            $sysList = M('sys')->where("id = 1")->find();
            $serviceData['operator_id'] = 0;
            $serviceData['action'] = '扣除服务费';
            $serviceData['entity_type'] = Top::EntityTypeSystem;
            $serviceData['entity_id'] = 1;
            $serviceData['account'] = $sysList['account'];
            $serviceData['sn'] = Utils::getSn();
            $serviceData['batch'] = $nowTime;
            $serviceData['amount'] = '-' . $serviceFee;
            $serviceData['balance'] = $sysList['account_credit_balance'];
            $serviceData['create_time'] = date('Y-m-d H:i:s', time());
            $serviceData['remark'] = 'top_order_flight.id:' . $flightId;
            $serviceData['object_id'] = $flightId;
            $serviceData['object_type'] = Top::ObjectTypeFlight;
            $serviceData['object_name'] = '机票';
            $sysDelmoney = M('flight_transaction')->add($serviceData);

            $flightNo = $orderFlightDetails['flight_no'];
            $message = "您预定的 " . $flightNo . " 航班出票失败, 请联系相关工作人员,客服电话 0551-65238888";

            Sms::Send($mobile, $message, $this->cdkey, $this->pwd, $this->signature, $this->smsOperatorId);

            $template = array(
                'touser' => $agencyInfo['weixinmp_openid'],
                'template_id' => Utils::flightOrderRefundTemplateId(),

                'data' => array(
                    'first' => array('value' => urlencode("亲，您预定的机票出票失败。"), 'color' => "#000"),
                    'keyword1' => array('value' => urlencode($flightOrderInfo['sequence_no']), 'color' => "#000"),
                    'keyword2' => array('value' => urlencode(Utils::getYuan($flightOrderInfo['price_total'])), 'color' => '#000'),
                    'keyword3' => array('value' => urlencode(Utils::getYuan($flightOrderInfo['price_total'])), 'color' => '#000'),
                    'remark' => array('value' => urlencode("请您尝试更换舱位，重新下单"), 'color' => '#000'),
                )
            );
            $this->send_tpl($template);

        } else if ($data['type'] == '0' || $data['type'] == '3') {

            $where['sequence_no'] = $data['sequence_no'];

            $flightOrderInfo = M('order_flight')->where($where)->find();

            $orderFlightData['state'] = Top::FlightOrderStateScheduledFailed;
            $orderFlightData['ticket_no'] = $data['ticket_nos'];
            M('order_flight')->where($where)->save($orderFlightData);

            M('order_flight_detail')->where($where)->save($orderFlightData);

            $priceTotal = $flightOrderInfo['price_total'];
            $flightId = $flightOrderInfo['id'];

            if ($flightOrderInfo['state'] == Top::FlightOrderStateTicketFailed || $flightOrderInfo['state'] == Top::FlightOrderStateScheduledFailed) {
                echo 'S';
                exit();
            }

            $agencyResult = M('agency')->where("id = '{$entityId}'")->setInc('account_balance', $priceTotal);

            $agencyList = M('agency')->where("id = '{$entityId}'")->find();
            $nowTime = Utils::getBatch();
            $agencyData['operator_id'] = $agencyList['operator_id'];
            $agencyData['action'] = '冲正:机票订单款';
            $agencyData['entity_type'] = Top::EntityTypeAgency;
            $agencyData['entity_id'] = $entityId;
            $agencyData['account'] = $agencyList['account'];
            $agencyData['sn'] = Utils::getSn();
            $agencyData['batch'] = $nowTime;
            $agencyData['amount'] = $priceTotal;
            $agencyData['balance'] = $agencyList['account_balance'];
            $agencyData['create_time'] = date('Y-m-d H:i:s', time());
            $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
            $agencyData['object_id'] = $flightId;
            $agencyData['object_type'] = Top::ObjectTypeFlight;
            $agencyData['object_name'] = '机票';
            $agencyDelmoney = M('account_transaction')->add($agencyData);

            $operatorId = $agencyInfo['operator_id'];
            $operatorResult = M('operator')->where("id = '{$operatorId}'")->setInc('account_credit_balance', $priceTotal);

            $operatorList = M('operator')->where("id = '{$operatorId}'")->find();
            $agencyData['operator_id'] = $operatorId;
            $agencyData['action'] = '冲正:机票订单款';
            $agencyData['entity_type'] = Top::EntityTypeOperator;
            $agencyData['entity_id'] = $operatorId;
            $agencyData['account'] = $operatorList['account'];
            $agencyData['sn'] = Utils::getSn();
            $agencyData['batch'] = $nowTime;
            $agencyData['amount'] = $priceTotal;
            $agencyData['balance'] = $operatorList['account_credit_balance'];
            $agencyData['create_time'] = date('Y-m-d H:i:s', time());
            $agencyData['remark'] = 'top_order_flight.id:' . $flightId;
            $agencyData['object_id'] = $flightId;
            $agencyData['object_type'] = Top::ObjectTypeFlight;
            $agencyData['object_name'] = '机票';
            $agencyDelmoney = M('operator_credit_transaction')->add($agencyData);

            $sysResult = M('sys')->where("id = 1")->setInc('account_credit_balance', $priceTotal);

            $sysList = M('sys')->where("id = 1")->find();
            $sysData['operator_id'] = 0;
            $sysData['action'] = '冲正:机票订单款';
            $sysData['entity_type'] = Top::EntityTypeSystem;
            $sysData['entity_id'] = 1;
            $sysData['account'] = $sysList['account'];
            $sysData['sn'] = Utils::getSn();
            $sysData['batch'] = $nowTime;
            $sysData['amount'] = $priceTotal;
            $sysData['balance'] = $sysList['account_credit_balance'];
            $sysData['create_time'] = date('Y-m-d H:i:s', time());
            $sysData['remark'] = 'top_order_flight.id:' . $flightId;
            $sysData['object_id'] = $flightId;
            $sysData['object_type'] = Top::ObjectTypeFlight;
            $sysData['object_name'] = '机票';
            $sysDelmoney = M('flight_transaction')->add($sysData);


            $serviceFee = Utils::getFen(C('SERVICE_FEE')) * $flightOrderInfo['passenger_total'];
            $sysResult = M('sys')->where("id = 1")->setDec('account_credit_balance', $serviceFee);

            $sysList = M('sys')->where("id = 1")->find();
            $serviceData['operator_id'] = 0;
            $serviceData['action'] = '扣除服务费';
            $serviceData['entity_type'] = Top::EntityTypeSystem;
            $serviceData['entity_id'] = 1;
            $serviceData['account'] = $sysList['account'];
            $serviceData['sn'] = Utils::getSn();
            $serviceData['batch'] = $nowTime;
            $serviceData['amount'] = '-' . $serviceFee;
            $serviceData['balance'] = $sysList['account_credit_balance'];
            $serviceData['create_time'] = date('Y-m-d H:i:s', time());
            $serviceData['remark'] = 'top_order_flight.id:' . $flightId;
            $serviceData['object_id'] = $flightId;
            $serviceData['object_type'] = Top::ObjectTypeFlight;
            $serviceData['object_name'] = '机票';
            $sysDelmoney = M('flight_transaction')->add($serviceData);

            $flightNo = $orderFlightDetails['flight_no'];
            $message = "您预定的 " . $flightNo . " 航班预定失败, 请联系相关工作人员,客服电话 0551-65238888";

            Sms::Send($mobile, $message, $this->cdkey, $this->pwd, $this->signature, $this->smsOperatorId);

            $template = array(
                'touser' => $agencyInfo['weixinmp_openid'],
                'template_id' => Utils::flightOrderRefundTemplateId(),

                'data' => array(
                    'first' => array('value' => urlencode("亲，您预定的机票预定失败。"), 'color' => "#000"),
                    'keyword1' => array('value' => urlencode($flightOrderInfo['sequence_no']), 'color' => "#000"),
                    'keyword2' => array('value' => urlencode(Utils::getYuan($flightOrderInfo['price_total'])), 'color' => '#000'),
                    'keyword3' => array('value' => urlencode(Utils::getYuan($flightOrderInfo['price_total'])), 'color' => '#000'),
                    'remark' => array('value' => urlencode("请您尝试更换舱位，重新下单"), 'color' => '#000'),
                )
            );
            $this->send_tpl($template);
        }

        $state = I('get.type');
        if ($state == '1' || $state == '2' || $state == '0' || $state == '3') {
            M('log_order_flight')->add($data);
            echo 'S';
        }
    }


    /**
     * 支付 URL 通知
     */
    public function paymentReturn()
    {
        $data['sequence_no'] = I('sequenceNo');
        $data['buyer_payment_account'] = I('get.buyerPaymentAccount');
        $data['pre_charge'] = I('get.preCharge');
        $data['order_status'] = I('get.orderStatus');
        $data['create_time'] = date("Y-m-d H:i:s", time());
        M('log_order_payment')->add($data);
    }


    /**
     * 退票 URL 通知
     */
    public function refundNotify()
    {
        $data['sequence_no'] = I('get.sequenceNo');
        $data['order_no'] = I('get.orderNo');
        $data['vender_refund_time'] = I('get.venderRefundTime');
        $data['vender_pay_price'] = I('get.venderPayPrice');
        $data['refund_fee'] = I('get.refundFee');
        $data['out_refund_no'] = I('get.outRefundNo');
        $data['vender_remark'] = I('get.venderRemark');
        $data['type'] = I('get.type');
        $data['create_time'] = date("Y-m-d H:i:s", time());

        if ($data['type'] == '1') {
            M('sys')->startTrans();
            try {
                $amount = Utils::getFen(I('get.venderPayPrice'));
                $refundFee = Utils::getFen(I('get.refundFee'));

                $total = $amount + $refundFee;

                $ticketNo = I('get.ticketNos');
                $orderNo = $data['order_no'];
                $where['sequence_no'] = $orderNo;
                $flightInfo = M('order_flight')->where($where)->find();
                $flightDetailInfo = M('order_flight_detail')->where("sequence_no = '{$orderNo}' && ticket_no like '$ticketNo%%'")->find();
                $flightPrice = $flightDetailInfo['settle_price'] + $flightDetailInfo['airport_tax'] - $flightDetailInfo['service_fee'];

                if ($total != $flightPrice || $flightDetailInfo['state'] == Top::FlightOrderStateRefunded) {
                    echo 'S';
                    exit();
                }

                $agencyId = $flightInfo['agency_id'];
                $operatorId = $flightInfo['operator_id'];
                $orderFlightData['state'] = Top::FlightOrderStateRefunded;

                $orderFlightDetailData['refund_fee'] = $amount;
                $orderFlightDetailData['refund_time'] = date("Y-m-d H:i:s", time());
                $orderFlightDetailData['state'] = Top::FlightOrderStateRefunded;

                $flightResult = M('order_flight')->where($where)->save($orderFlightData);
                $flightDetailResult = M('order_flight_detail')->where("sequence_no = '{$orderNo}' && ticket_no like '$ticketNo%%'")->save($orderFlightDetailData);
                $agencyResult = M('agency')->where("id = '{$agencyId}'")->setInc('account_balance', $amount);
                $operatorResult = M('operator')->where("id = '{$operatorId}'")->setInc('account_credit_balance', $amount);
                $sysResult = M('sys')->where("id = 1")->setInc('account_credit_balance', $amount);

                $agencyInfo = M('agency')->where("id = '{$agencyId}'")->find();
                $agencyData['operator_id'] = $agencyInfo['operator_id'];
                $agencyData['sn'] = Utils::getSn();
                $agencyData['account'] = $agencyInfo['account'];
                $agencyData['entity_type'] = Top::EntityTypeAgency;
                $agencyData['entity_id'] = $agencyId;
                $agencyData['action'] = '机票退款';
                $agencyData['amount'] = $amount;
                $agencyData['balance'] = $agencyInfo['account_balance'];
                $agencyData['remark'] = 'top_order_flight.id:' . $flightInfo['id'];
                $agencyData['object_id'] = $flightInfo['id'];
                $agencyData['object_type'] = Top::ObjectTypeFlight;
                $agencyData['object_name'] = '机票';
                $agencyData['create_time'] = date("Y-m-d H:i:s", time());
                $agencyIncResult = M('account_transaction')->add($agencyData);

                $operatorInfo = M('operator')->where("id = '{$operatorId}'")->find();
                $operatorData['operator_id'] = $operatorId;
                $operatorData['sn'] = Utils::getSn();
                $operatorData['account'] = $operatorInfo['account'];
                $operatorData['entity_type'] = Top::EntityTypeOperator;
                $operatorData['entity_id'] = $operatorId;
                $operatorData['action'] = '机票退款';
                $operatorData['amount'] = $amount;
                $operatorData['balance'] = $operatorInfo['account_credit_balance'];
                $operatorData['remark'] = 'top_order_flight.id:' . $flightInfo['id'];
                $operatorData['object_id'] = $flightInfo['id'];
                $operatorData['object_type'] = Top::ObjectTypeFlight;
                $operatorData['object_name'] = '机票';
                $operatorData['create_time'] = date("Y-m-d H:i:s", time());
                $operatorIncResult = M('operator_credit_transaction')->add($operatorData);

                $sysInfo = M('sys')->where("id = 1")->find();
                $sysData['operator_id'] = 0;
                $sysData['sn'] = Utils::getSn();
                $sysData['account'] = $sysInfo['account'];
                $sysData['entity_type'] = Top::EntityTypeSystem;
                $sysData['entity_id'] = $sysInfo['id'];
                $sysData['action'] = '机票退款';
                $sysData['amount'] = $amount;
                $sysData['balance'] = $sysInfo['account_credit_balance'];
                $sysData['remark'] = 'top_order_flight.id:' . $flightInfo['id'];
                $sysData['object_id'] = $flightInfo['id'];
                $sysData['object_type'] = Top::ObjectTypeFlight;
                $sysData['object_name'] = '机票';
                $sysData['create_time'] = date("Y-m-d H:i:s", time());
                $sysIncResult = M('flight_transaction')->add($sysData);

                if ($flightDetailResult && $agencyResult && $operatorResult && $sysResult && $agencyIncResult && $operatorIncResult && $sysIncResult) {
                    M('sys')->commit();
                }
            } catch (Exception $ex) {
                M('sys')->rollback();
            }
        } else if ($data['type'] == '0') {
            $orderNo = $data['order_no'];
            $where['sequence_no'] = $orderNo;
            $ticketNo = I('get.ticketNos');
            $orderFlightData['state'] = Top::FlightOrderStateRefundFailed;
            M('order_flight')->where($where)->save($orderFlightData);
            M('order_flight_detail')->where("sequence_no = '{$orderNo}' && ticket_no like '$ticketNo%%'")->save($orderFlightData);
        }

        $state = I('get.type');
        if ($state == '1' || $state == '0') {
            M('log_refund_ticket_flight')->add($data);
            echo 'S';
        }
    }


    /**
     * 我的机票
     */
    public function myPlaneTicket()
    {
        $isLogin = session('isLogin');

        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->planeTicketListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    /**
     * 退票
     */
    public function refundApplication()
    {
        if (IS_POST && IS_AJAX) {
            $url = $this->orderRefundUrl();
            $data['agencyCode'] = $this->agencyCode();
            //单号
            $data['liantuoOrderNo'] = I('orderNo');
            $data['actionType'] = I('refundType');
            //$data['refundType'] = 'APPLY_FOR_REFUND_FREEWILL';
            //退票时,输入乘客的姓名
            $data['refundTicketList.passengerName'] = I('passengerName');
            //必须13位,支付成功后,通知上附带票号信息
            $data['refundTicketList.ticketNo'] = I('ticketNo');
            //出发 - 到达目的地
            $data['refundTicketList.segment'] = I('segment');
            $data['cancelPnrStatus'] = '1';
            $data['refundNotifiedUrl'] = 'http://zhangda.m.lydlr.com/index.php/Home/Flight/refundNotify';
            $sign = $data['actionType'] . $this->agencyCode() . $this->sign();
            $data['sign'] = strtolower(md5($sign));

            $ticketNo = explode(",", I('ticketNo'));
            $ticketNo = $ticketNo[0];
            //返回 xml 格式
            $result = Utils::vpost($url, $data);
            //转换成数组
            $listAllData = $this->xml_to_array($result);
            $returnCode = $listAllData['returnCode'][0];

            if ($returnCode == 'S') {
                $state = Top::FlightOrderStateRefundWaited;
                $sequence_no = I('orderNo');
                $result = M('order_flight')->execute("update top_order_flight set state = '{$state}' where sequence_no = '{$sequence_no}'");

                $orderFlightDetailResult = M('order_flight_detail')->execute("update top_order_flight_detail set state = '{$state}' where sequence_no = '{$sequence_no}' && ticket_no = '{$ticketNo}'");

                $data['status'] = 1;
                $data['msg'] = '退票中,待退款';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 2;
                $data['msg'] = $listAllData['returnMessage'][0];
                $this->ajaxReturn($data);
            }
        } else {
            $id = I('id');
            $flightInfo = M('order_flight_detail')->where("id = '{$id}'")->find();
            $this->assign('flightInfo', $flightInfo);
            $this->display();
        }
    }


    /**
     * [我的机票 加载更多]
     */
    protected function planeTicketListGetPageHtml($page)
    {
        $M = M();
        $agencyInfo = $this->userInfo;
        $agencyId = $agencyInfo['id'];
        $state = Top::FlightOrderStateDeleted;

        $pageSize = C('PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_order_flight_detail as d,top_order_flight as f')
            ->where("f.id = d.order_flight_id && d.agency_id = '{$agencyId}' && d.state != '{$state}' && f.state != '{$state}'")
            ->field(array(
                'f.id' => 'id',
                'd.flight_no' => 'flight_no',
                'd.sequence_no' => 'sequence_no',
                'd.seat_class' => 'seat_class',
                'd.dep_code' => 'dep_code',
                'd.arr_code' => 'arr_code',
                'd.dep_date' => 'dep_date',
                'd.dep_time' => 'dep_time',
                'd.arr_time' => 'arr_time',
                'd.plane_model' => 'plane_model',
                'f.passenger_names' => 'passenger_name',
                'f.org_jetquay' => 'org_jetquay',
                'f.dst_jetquay' => 'dst_jetquay',
                'f.create_time' => 'create_time',
                'f.price_total' => 'price_total',
                'f.state' => 'state',
            ))
            ->order('f.id desc')
            ->group('d.order_flight_id')
            ->limit($index, $pageSize)
            ->select();


        foreach ($list as $key => $vo) {
            $vo['passenger_name'] = str_replace(",", " ", $vo['passenger_name']);

            if ($vo['org_jetquay'] == '--' || $vo['org_jetquay'] == 'Array') {
                $vo['org_jetquay'] = '';
            }

            if ($vo['dst_jetquay'] == '--' || $vo['dst_jetquay'] == 'Array') {
                $vo['dst_jetquay'] = '';
            }

            if ($vo['state'] == 1) {
                $orderState = "未提交";
            } else if ($vo['state'] == 2) {
                $orderState = "已付款,待出票";
            } else if ($vo['state'] == 3) {
                $orderState = "已付款,已出票";
            } else if ($vo['state'] == 4) {
                $orderState = "已取消";
            } else if ($vo['state'] == 5) {
                $orderState = "已退票,退票成功";
            } else if ($vo['state'] == 6) {
                $orderState = "出票失败";
            } else if ($vo['state'] == 7) {
                $orderState = "预定失败";
            } else if ($vo['state'] == 8) {
                $orderState = "退票失败";
            } else if ($vo['state'] == 9) {
                $orderState = "退票中,待退款";
            } else if ($vo['state'] == 10) {
                $orderState = "有取消";
            } else if ($vo['state'] == 11) {
                $orderState = "有退票";
            } else if ($vo['state'] == 12) {
                $orderState = "已删除";
            } else if ($vo['state'] == 14) {
                $orderState = "已预定,待付款";
            } else if ($vo['state'] == 15) {
                $orderState = "已预定,付款中";
            }

            $vo['flight_no'] = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];

            $vo['start_code'] = $this->nameToAirport($vo['dep_code']);
            $vo['end_code'] = $this->nameToAirport($vo['arr_code']);

            $vo['dep_code'] = $this->nameToFullAirport($vo['dep_code']);
            $vo['arr_code'] = $this->nameToFullAirport($vo['arr_code']);

            $vo['price_total'] = Utils::getYuan($vo['price_total']);


            $vo['dep_time'] = substr($vo['dep_time'], 0, 2) . ':' . substr($vo['dep_time'], 2, 2);
            $vo['arr_time'] = substr($vo['arr_time'], 0, 2) . ':' . substr($vo['arr_time'], 2, 2);

            $refund = "<a href=" . U('Flight/planeTicketDetail', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right;margin-top:-6px;' value='查看详情'>
                    </a>";

            $evaldata .= "<ul class='tradeList' style='height:100%'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='price'>" . $vo['flight_no'] . " | " . $vo['plane_model'] . "</p>";
            $evaldata .= "<p class='price'>交易号：<span class='flight-text-a5'>" . $vo['sequence_no'] . "</span></p>";
            $evaldata .= "<p class='price'>舱位码：<span class='flight-text-a5'>" . $vo['seat_class'] . " 舱</span></p>";
            $evaldata .= "<p class='price'>起降机场：<span class='flight-text-a5'>" . $vo['start_code'] . "" . $vo['dep_code'] . " " . $vo['org_jetquay'] . " - " . $vo['end_code'] . $vo['arr_code'] . " " . $vo['dst_jetquay'] . "</span></p>";
            $evaldata .= "<p class='price'>起降时间：<span class='flight-text-a5'>" . $vo['dep_date'] . " " . $vo['dep_time'] . " -- " . $vo['dep_date'] . " " . $vo['arr_time'] . "</span></p>";
            $evaldata .= "<p class='price'>乘机人：<span class='flight-text-a5'>" . $vo['passenger_name'] . "</span></p>";
            $evaldata .= "<p class='price'>订单时间：<span class='flight-text-a5'>" . $vo['create_time'] . "</span><i class='icon-flight'></i></p>";
            $evaldata .= "<p class='price'>订单金额：<dfn>&yen; " . $vo['price_total'] . "$refund</dfn></p>";
            $evaldata .= "<p class='price'>订单状态：<span style='color:#f08519'>$orderState</span></p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }
        return $evaldata;
    }


    /**
     * [我的机票 查看详情]
     */
    public function planeTicketDetail()
    {
        $M = M();
        $id = I('get.id');

        $list = $M->table('top_order_flight_detail as d')
            ->where("d.order_flight_id = '{$id}'")
            ->field(array(
                'd.id' => 'id',
                'd.order_flight_id' => 'order_flight_id',
                'd.pay_time' => 'pay_time',
                'd.flight_no' => 'flight_no',
                'd.sequence_no' => 'sequence_no',
                'd.dep_code' => 'dep_code',
                'd.arr_code' => 'arr_code',
                'd.dep_date' => 'dep_date',
                'd.dep_time' => 'dep_time',
                'd.arr_time' => 'arr_time',
                'd.plane_model' => 'plane_model',
                'd.passenger_name' => 'passenger_name',
                'd.passenger_type' => 'passenger_type',
                'd.settle_price' => 'settle_price',
                'd.par_price' => 'par_price',
                'd.fuel_tax' => 'fuel_tax',
                'd.airport_tax' => 'airport_tax',
                'd.state' => 'state',
                'd.identity_no' => 'identity_no',
                'd.ticket_no' => 'ticket_no',
                'd.create_time' => 'create_time',
            ))
            ->order('d.passenger_type desc')
            ->select();

        $this->assign('list', $list);

        $this->flightInfo = M('order_flight')->where("id = '{$id}'")->find();

        foreach ($list as $key => $vo) {

            $this->orderTotal += ($vo['par_price'] + $vo['airport_tax']);

            if ($vo['passenger_type'] == 1) {
                $vo['passenger_type'] = '儿童';
            } else {
                $vo['passenger_type'] = '成人';
            }

            $vo['price_total'] = $vo['settle_price'] + $vo['fuel_tax'] + $vo['airport_tax'];
            $price_total = $vo['settle_price'] + $vo['fuel_tax'] + $vo['airport_tax'];
            $this->assign('price_total', $price_total);

            if ($vo['org_jetquay'] == '--' || $vo['org_jetquay'] == 'Array') {
                $vo['org_jetquay'] = '';
            }

            if ($vo['dst_jetquay'] == '--' || $vo['dst_jetquay'] == 'Array') {
                $vo['dst_jetquay'] = '';
            }

            $vo['flight_no'] = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];
            $flightNo = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];
            $this->assign('flightNo', $flightNo);

            $vo['start_code'] = $this->nameToAirport($vo['dep_code']);
            $vo['end_code'] = $this->nameToAirport($vo['arr_code']);

            $vo['org'] = $vo['dep_code'];
            $vo['dst'] = $vo['arr_code'];

            $vo['dep_code'] = $this->nameToFullAirport($vo['dep_code']);
            $vo['arr_code'] = $this->nameToFullAirport($vo['arr_code']);
            $vo['price_total'] = Utils::getYuan($vo['price_total']);

            $vo['ticket_no'] = rtrim($vo['ticket_no'], ",");
            $vo['ticket_no'] = str_replace(",", " ", $vo['ticket_no']);

            $vo['dep_time'] = substr($vo['dep_time'], 0, 2) . ':' . substr($vo['dep_time'], 2, 2);
            $vo['arr_time'] = substr($vo['arr_time'], 0, 2) . ':' . substr($vo['arr_time'], 2, 2);

            $vo['detail'] = "(结算价" . Utils::getYuan($vo['settle_price']) . "+燃油税" . Utils::getYuan($vo['fuel_tax']) . "+基建费" . Utils::getYuan($vo['airport_tax']) . ")";
            $detail = "(结算价" . Utils::getYuan($vo['settle_price']) . "+燃油税" . Utils::getYuan($vo['fuel_tax']) . "+基建费" . Utils::getYuan($vo['airport_tax']) . ")";
            $this->assign('detail', $detail);

            $ticketState = Top::FlightOrderStateTicked;

            $vo['flightDate'] = $vo['dep_date'] . " " . $vo['dep_time'] . ":00";

            if ($vo['state'] == $ticketState) {
                $refund = "<a href=" . U('Flight/refundApplication', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right' value='申请退票'>
                    </a>";
            } else {
                $refund = '';
            }

            $evaldata .= "<ul class='tradeList' style='height:100%'>";
            $evaldata .= "<li style='padding-bottom:40px;'>";
            $evaldata .= "<p class='price'>乘机人：<span class='flight-text-a5'>" . $vo['passenger_name'] . "</span></p>";
            $evaldata .= "<p class='price'>票号：<span class='flight-text-a5'>" . $vo['ticket_no'] . "</span></p>";
            $evaldata .= "<p class='price'>乘客类型：<span class='flight-text-a5'>" . $vo['passenger_type'] . "</span></p>";
            $evaldata .= "<p class='price'>证件号码：<span class='flight-text-a5'>" . $vo['identity_no'] . "</span>";
            $evaldata .= $refund;
            $evaldata .= "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        $depDate = $list[0]['dep_date'] . " " . substr($list[0]['dep_time'], 0, 2) . ':' . substr($list[0]['dep_time'], 2, 2) . ":00";
        $date = date("Y-m-d H:i:s", time());
        $this->assign('date', $date);
        $this->assign('depDate', $depDate);
        $this->assign('id', $id);
        $this->assign('html', $evaldata);
        $this->display();
    }


    /**
     * 删除订单
     */
    public function deleteOrder()
    {
        $id = I('get.id');
        $data['state'] = Top::FlightOrderStateDeleted;

        M('order_flight_detail')->where("order_flight_id = '{$id}'")->save($data);
        M('order_flight')->where("id = '{$id}'")->save($data);
        $this->redirect("Flight/myPlaneTicket");
    }


    /**
     * 我的佣金
     */
    public function flightProfit()
    {
        $isLogin = session('isLogin');

        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $agencyInfo = $this->userInfo;
            $agencyId = $agencyInfo['id'];
            $profitList = M('agency_profit')->where("agency_id = '{$agencyId}'")->select();

            $profitTotal['profit_total'] = 0;
            foreach ($profitList as $k => $v) {
                $profitTotal['profit_total'] += $v['par_price'] - $v['settle_price'];
            }
            $total = Utils::getYuan($profitTotal['profit_total']);

            $html = $this->planeProfitListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('total', $total);
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    /**
     * [我的佣金 加载更多]
     */
    protected function planeProfitListGetPageHtml($page)
    {
        $M = M();
        $agencyInfo = $this->userInfo;
        $agencyId = $agencyInfo['id'];

        $pageSize = C('PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_agency_profit as p')
            ->where("p.agency_id = '{$agencyId}'")
            ->field(array(
                'p.par_price' => 'par_price',
                'p.settle_price' => 'settle_price',
                'p.create_time' => 'create_time',
                'p.nickname' => 'nickname',
                'p.order_time' => 'order_time',
                'p.profit_id' => 'pid',
            ))
            ->limit($index, $pageSize)
            ->select();


        foreach ($list as $key => $vo) {
            $vo['profit_price'] = Utils::getYuan($vo['par_price'] - $vo['settle_price']);
            $vo['create_time'] = substr($vo['create_time'], 0, 10);
            $vo['order_time'] = substr($vo['order_time'], 0, 10);

            $refund = "<a href=" . U('Flight/planeProfitDetail', array('id' => $vo['pid'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right;margin-top:-6px;' value='查看详情'>
                    </a>";

            $evaldata .= "<ul class='tradeList' style='height:100%'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='price'>机票购买：<span class='flight-text-a5'>" . $vo['nickname'] . "</span><i class='icon-flight'></i></p>";
            $evaldata .= "<p class='price'>下单时间：<span class='flight-text-a5'>" . $vo['order_time'] . "</span><i class='icon-flight'></i></p>";
            $evaldata .= "<p class='price'>发放时间：<span class='flight-text-a5'>" . $vo['create_time'] . "</span><i class='icon-flight'></i></p>";
            $evaldata .= "<p class='price'>佣金金额：<dfn>&yen; " . $vo['profit_price'] . "$refund</dfn></p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }
        return $evaldata;
    }


    /**
     * [机票佣金 查看详情]
     */
    public function planeProfitDetail()
    {
        $M = M();
        $state = Top::FlightOrderStateSubmitted;

        $id = I('get.id');

        $list = $M->table('top_visitor_order_flight_detail as d')
            ->where("d.order_flight_id = '{$id}'")
            ->field(array(
                'd.id' => 'id',
                'd.pay_time' => 'pay_time',
                'd.flight_no' => 'flight_no',
                'd.sequence_no' => 'sequence_no',
                'd.dep_code' => 'dep_code',
                'd.arr_code' => 'arr_code',
                'd.dep_date' => 'dep_date',
                'd.dep_time' => 'dep_time',
                'd.arr_time' => 'arr_time',
                'd.plane_model' => 'plane_model',
                'd.passenger_name' => 'passenger_name',
                'd.passenger_type' => 'passenger_type',
                'd.settle_price' => 'settle_price',
                'd.par_price' => 'par_price',
                'd.fuel_tax' => 'fuel_tax',
                'd.airport_tax' => 'airport_tax',
                'd.state' => 'state',
                'd.ticket_no' => 'ticket_no',
                'd.create_time' => 'create_time',
                'd.isprofit' => 'isprofit',
                'd.profit_time' => 'profit_time',
            ))
            ->order('d.id desc')
            ->select();

        $this->assign('list', $list);

        foreach ($list as $key => $vo) {

            if ($vo['isprofit'] == 0) {
                $vo['isprofit'] = '未发放';
            } else {
                $vo['isprofit'] = '已发放';
            }

            if ($vo['state'] == 2) {
                $state = '已付款,待出票';
            } else if ($vo['state'] == 3) {
                $successTime = $vo['pay_time'];
                $state = '已出票&nbsp;&nbsp;&nbsp;' . $successTime;
            } else if ($vo['state'] == 6) {
                $state = '出票失败';
            } else if ($vo['state'] == 4) {
                $state = '已取消';
            } else if ($vo['state'] == 7) {
                $state = '预定失败';
            } else if ($vo['state'] == 5) {
                $state = '已退票,退票成功';
            } else if ($vo['state'] == 8) {
                $state = '退票失败';
            } else if ($vo['state'] == 9) {
                $state = '退票中,待退款';
            }

            if ($vo['passenger_type'] == 1) {
                $vo['passenger_type'] = '儿童';
            } else {
                $vo['passenger_type'] = '成人';
            }

            $vo['price_total'] = $vo['par_price'] + $vo['fuel_tax'] + $vo['airport_tax'];
            $price_total = $vo['par_price'] + $vo['fuel_tax'] + $vo['airport_tax'];
            $settle_total = $vo['settle_price'] + $vo['fuel_tax'] + $vo['airport_tax'];
            $this->assign('price_total', $price_total);
            $this->assign('settle_total', $settle_total);

            if ($vo['org_jetquay'] == '--' || $vo['org_jetquay'] == 'Array') {
                $vo['org_jetquay'] = '';
            }

            if ($vo['dst_jetquay'] == '--' || $vo['dst_jetquay'] == 'Array') {
                $vo['dst_jetquay'] = '';
            }

            $vo['flight_no'] = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];
            $flightNo = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];
            $this->assign('flightNo', $flightNo);

            $vo['start_code'] = $this->nameToAirport($vo['dep_code']);
            $vo['end_code'] = $this->nameToAirport($vo['arr_code']);

            $vo['org'] = $vo['dep_code'];
            $vo['dst'] = $vo['arr_code'];

            $vo['dep_code'] = $this->nameToFullAirport($vo['dep_code']);
            $vo['arr_code'] = $this->nameToFullAirport($vo['arr_code']);
            $vo['price_total'] = Utils::getYuan($vo['price_total']);

            $vo['ticket_no'] = rtrim($vo['ticket_no'], ",");
            $vo['ticket_no'] = str_replace(",", " ", $vo['ticket_no']);

            $vo['dep_time'] = substr($vo['dep_time'], 0, 2) . ':' . substr($vo['dep_time'], 2, 2);
            $vo['arr_time'] = substr($vo['arr_time'], 0, 2) . ':' . substr($vo['arr_time'], 2, 2);

            $vo['detail'] = "(结算价" . Utils::getYuan($vo['par_price']) . "+燃油税" . Utils::getYuan($vo['fuel_tax']) . "+基建费" . Utils::getYuan($vo['airport_tax']) . ")";
            $detail = "(结算价" . Utils::getYuan($vo['par_price']) . "+燃油税" . Utils::getYuan($vo['fuel_tax']) . "+基建费" . Utils::getYuan($vo['airport_tax']) . ")";
            $this->assign('detail', $detail);

            $evaldata .= "<ul class='tradeList' style='height:100%'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='price'>乘机人：<span class='flight-text-a5'>" . $vo['passenger_name'] . "</span></p>";
            $evaldata .= "<p class='price'>票号：<span class='flight-text-a5'>" . $vo['ticket_no'] . "</span></p>";
            $evaldata .= "<p class='price'>乘客类型：<span class='flight-text-a5'>" . $vo['passenger_type'] . "</span></p>";
            $evaldata .= "<p class='price'>机票状态：<dfn>" . $state . "</dfn><i class='icon-head'></i>";
            $evaldata .= "<p class='price'>佣金发放：<dfn>" . $vo['isprofit'] . "</dfn><i class='icon-head'></i>";
            $evaldata .= "<p class='price'>发放时间：<dfn>" . $vo['profit_time'] . "</dfn><i class='icon-head'></i>";
            $evaldata .= "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        $this->assign('html', $evaldata);
        $this->display();
    }


    /**
     * 直客机票订单
     */
    public function visitorPlaneTicket()
    {
        $isLogin = session('isLogin');

        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->visitorPlaneTicketListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    /**
     * [直客机票 加载更多]
     */
    protected function visitorPlaneTicketListGetPageHtml($page)
    {
        $M = M();
        $agencyInfo = $this->userInfo;
        $agencyId = $agencyInfo['id'];
        $state = Top::FlightOrderStateSubmitted;

        $pageSize = C('PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_visitor_order_flight_detail as d,top_visitor_order_flight as f')
            ->where("f.sequence_no = d.sequence_no && f.id = d.order_flight_id && d.agency_id = '{$agencyId}' && d.state != '{$state}' && f.state != '{$state}'")
            ->field(array(
                'f.id' => 'id',
                'd.flight_no' => 'flight_no',
                'd.sequence_no' => 'sequence_no',
                'd.dep_code' => 'dep_code',
                'd.arr_code' => 'arr_code',
                'd.dep_date' => 'dep_date',
                'd.dep_time' => 'dep_time',
                'd.arr_time' => 'arr_time',
                'd.plane_model' => 'plane_model',
                'f.passenger_names' => 'passenger_name',
                'f.org_jetquay' => 'org_jetquay',
                'f.dst_jetquay' => 'dst_jetquay',
                'f.create_time' => 'create_time',
                'f.price_total' => 'price_total',
            ))
            ->order('f.id desc')
            ->group('d.sequence_no')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $vo['passenger_name'] = str_replace(",", " ", $vo['passenger_name']);

            if ($vo['org_jetquay'] == '--' || $vo['org_jetquay'] == 'Array') {
                $vo['org_jetquay'] = '';
            }

            if ($vo['dst_jetquay'] == '--' || $vo['dst_jetquay'] == 'Array') {
                $vo['dst_jetquay'] = '';
            }

            $vo['flight_no'] = $this->nameToAirLine(substr($vo['flight_no'], 0, 2)) . " " . $vo['flight_no'];

            $vo['start_code'] = $this->nameToAirport($vo['dep_code']);
            $vo['end_code'] = $this->nameToAirport($vo['arr_code']);

            $vo['dep_code'] = $this->nameToFullAirport($vo['dep_code']);
            $vo['arr_code'] = $this->nameToFullAirport($vo['arr_code']);

            $vo['price_total'] = Utils::getYuan($vo['price_total']);


            $vo['dep_time'] = substr($vo['dep_time'], 0, 2) . ':' . substr($vo['dep_time'], 2, 2);
            $vo['arr_time'] = substr($vo['arr_time'], 0, 2) . ':' . substr($vo['arr_time'], 2, 2);

            $refund = "<a href=" . U('Flight/planeProfitDetail', array('id' => $vo['id'])) . ">
                    <input type='button' class='btn btn-warning' style='float:right;margin-top:-6px;' value='查看详情'>
                    </a>";

            $evaldata .= "<ul class='tradeList' style='height:100%'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='price'>" . $vo['flight_no'] . " | " . $vo['plane_model'] . "</p>";
            $evaldata .= "<p class='price'>交易号：<span class='flight-text-a5'>" . $vo['sequence_no'] . "</span></p>";
            $evaldata .= "<p class='price'>起降机场：<span class='flight-text-a5'>" . $vo['start_code'] . "" . $vo['dep_code'] . " " . $vo['org_jetquay'] . " - " . $vo['end_code'] . $vo['arr_code'] . " " . $vo['dst_jetquay'] . "</span></p>";
            $evaldata .= "<p class='price'>起降时间：<span class='flight-text-a5'>" . $vo['dep_date'] . " " . $vo['dep_time'] . " -- " . $vo['dep_date'] . " " . $vo['arr_time'] . "</span></p>";
            $evaldata .= "<p class='price'>乘机人：<span class='flight-text-a5'>" . $vo['passenger_name'] . "</span></p>";
            $evaldata .= "<p class='price'>订单时间：<span class='flight-text-a5'>" . $vo['create_time'] . "</span><i class='icon-flight'></i></p>";
            $evaldata .= "<p class='price'>订单金额：<dfn>&yen; " . $vo['price_total'] . "$refund</dfn></p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }
        return $evaldata;
    }


    /**
     * 新增常旅客
     */
    public function addPassengers()
    {
        $ids = urldecode(I('passengerIds'));
        $passengerIds = explode(',', $ids);
        $delIds = explode(',', I('delId'));
        $checkPassengerIds = array_diff($passengerIds, $delIds);
        $this->flightNo = I('flightNo');
        $this->depCode = I('depCode');
        $this->arrCode = I('arrCode');
        $this->depDate = I('depDate');
        $this->depTime = I('depTime');
        $this->arrTime = I('arrTime');
        $this->planeModel = I('planeModel');
        $this->seatCode = I('seatCode');
        $this->agencyId = $this->userInfo['id'];
        $this->orgJetquay = I('orgJetquay');
        $this->dstJetquay = I('dstJetquay');
        $this->arrDate = I('arrDate');
        $this->parPrice = I('parPrice');
        $this->settlePrice = I('settlePrice');
        $this->airportTax = I('airportTax');
        $this->fuelTax = I('fuelTax');
        $this->type = I('type');
        $this->policyId = I('policyId');
        $_SESSION['flightOrderCreate'] = 400;

        $agencyId = $this->userInfo['id'];
        $list = M('frequent_passenger')->where("agency_id = '{$agencyId}'")->group('passenger_name')->order("pinyin asc")->select();

        $passengerNames = '';

        foreach ($list as $k => $v) {
            $passengerNames .= $v['passenger_name'] . ',';
            foreach ($checkPassengerIds as $key => $value) {
                if ($v['id'] == $value) {
                    $list[$k]['is_checked'] = 1;
                }
            }
        }

        $passengerNames = rtrim($passengerNames, ',');

        $this->assign('list', $list);
        $this->assign('passengerIds', $checkPassengerIds);
        $this->assign('passengerNames', $passengerNames);
        $this->display();
    }


    /**
     * 添加常旅客
     */
    public function addFrequentPassenger()
    {
        if (IS_POST && IS_AJAX) {
            $passenger_name = Utils::trimall(I('passenger_name'));
            $data['operator_id'] = $this->userInfo['operator_id'];
            $data['agency_id'] = $this->userInfo['id'];
            $data['agency_name'] = $this->userInfo['name'];
            $data['passenger_name'] = $passenger_name;
            $data['passenger_type'] = I('passenger_type');
            $data['birthday'] = I('birthday');
            $data['pinyin'] = I('pinyin');
            $data['identity_type'] = I('identity_type');
            $data['identity_no'] = trim(I('identity_no'));
            $data['create_time'] = date("Y-m-d H:i:s", time());

            $info = M('frequent_passenger')->where("passenger_name = '{$passenger_name}'")->find();

            if ($info) {
                $data['update_time'] = date("Y-m-d H:i:s", time());
                M('frequent_passenger')->where("passenger_name = '{$passenger_name}'")->save($data);
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            }

            $result = M('frequent_passenger')->add($data);

            if ($result) {
                $data['status'] = 1;
                $data['msg'] = '操作成功';
                $this->ajaxReturn($data);
            } else {
                $data['status'] = 0;
                $data['msg'] = '操作失败';
                $this->ajaxReturn($data);
            }
        } else {
            $this->passengerIds = urldecode(I('passengerIds'));
            $this->flightNo = I('flightNo');
            $this->depCode = I('depCode');
            $this->arrCode = I('arrCode');
            $this->depDate = I('depDate');
            $this->depTime = I('depTime');
            $this->arrTime = I('arrTime');
            $this->planeModel = I('planeModel');
            $this->seatCode = I('seatCode');
            $this->orgJetquay = I('orgJetquay');
            $this->dstJetquay = I('dstJetquay');
            $this->arrDate = I('arrDate');
            $this->parPrice = I('parPrice');
            $this->settlePrice = I('settlePrice');
            $this->fuelTax = I('fuelTax');
            $this->airportTax = I('airportTax');
            $this->type = I('type');
            $this->policyId = I('policyId');

            $where['id'] = I('get.id');
            $info = M('frequent_passenger')->where($where)->find();
            $this->assign('info', $info);
            $this->display();
        }
    }


    /**
     * 三字码
     */
    public function airportname($code)
    {
        $codeArr = array(
            "阿克苏" => "AKU",
            '阿尔山' => 'YIE',
            '阿里' => 'NGQ',
            '安顺' => 'AVA',
            "阿勒泰" => "AAT",
            "安康" => "AKA",
            "安庆" => "AQG",
            "安阳" => "AYN",
            "鞍山" => "AOG",
            "百色" => "AEB",
            "保山" => "BSD",
            '毕节' => 'BFJ',
            "包头" => "BAV",
            "蚌埠" => "BFU",
            "北海" => "BHY",
            "北京" => "PEK",
            //"北京南苑" => "NAY",
            "博乐" => "BPL",
            "长春" => "CGQ",
            "常德" => "CGD",
            "昌都" => "BPX",
            "长沙" => "CSX",
            '长海' => 'CNI',
            "长治" => "CIH",
            "常州" => "CZX",
            "朝阳" => "CHG",
            "成都" => "CTU",
            "赤峰" => "CIF",
            '酒泉' => 'CHW',
            "重庆" => "CKG",
            "长白山" => "NBS",
            "大连" => "DLC",
            "大理" => "DLU",
            "丹东" => "DDG",
            "大同" => "DAT",
            '迪庆' => 'DIG',
            '稻城' => 'DCY',
            "达县" => "DAX",
            "香格里拉" => "DIG",
            "东营" => "DOY",
            '达州' => 'DAX',
            "敦煌" => "DNH",
            "大庆" => "DQA",
            "芒市" => "LUM",
            "鄂尔多斯" => "DSN",
            "恩施" => "ENH",
            '延安' => 'ENY',
            "二连浩特" => "ERL",
            "佛山" => "FUO",
            '富蕴' => 'FYN',
            "福州" => "FOC",
            "阜阳" => "FUG",
            "赣州" => "KOW",
            "格尔木" => "GOQ",
            "广元" => "GYS",
            "广州" => "CAN",
            "桂林" => "KWL",
            "贵阳" => "KWE",
            '广汉' => 'GHN',
            "固原" => "GYU",
            '甘南' => 'GXH',
            "海口" => "HAK",
            "海拉尔" => "HLD",
            "哈密" => "HMI",
            "邯郸" => "HDG",
            '韶关' => 'HSC',
            "杭州" => "HGH",
            '怀化' => 'HJJ',
            "汉中" => "HZG",
            "哈尔滨" => "HRB",
            "合肥" => "HFE",
            "黑河" => "HEK",
            '花莲' => 'HUN',
            '衡阳' => 'HNY',
            '惠州' => 'HUZ',
            "和田" => "HTN",
            "呼和浩特" => "HET",
            "黄山" => "TXN",
            "台州" => "HYN",
            "芷江" => "HJJ",
            '神农架' => 'HPG',
            "淮安" => "HIA",
            "九寨沟" => "JZH",
            "佳木斯" => "JMU",
            "嘉峪关" => "JGN",
            "济南" => "TNA",
            "景德镇" => "JDZ",
            "井冈山" => "JGS",
            "济宁" => "JNG",
            "锦州" => "JNZ",
            "九江" => "JIU",
            "鸡西" => "JXA",
            "泉州" => "JJN",
            '吉林' => 'JIL',
            "西双版纳" => "JHG",
            "布尔津" => "KJI",
            "克拉玛依" => "KRY",
            '高雄' => 'KHH',
            "喀什" => "KHG",
            "库尔勒" => "KRL",
            "库车" => "KCA",
            "昆明" => "KMG",
            '金门' => 'KNH',
            "康定" => "KGT",
            "兰州" => "LHW",
            "拉萨" => "LXA",
            "连云港" => "LYG",
            "丽江" => "LJG",
            "临沧" => "LNJ",
            '连城' => 'LCX',
            "临沂" => "LYI",
            "林芝" => "LZY",
            "黎平" => "HZH",
            "柳州" => "LZH",
            "洛阳" => "LYA",
            "泸州" => "LZO",
            "龙岩" => "LCX",
            "荔波" => "LLB",
            "满洲里" => "NZH",
            "梅州" => "MXZ",
            "绵阳" => "MIG",
            "牡丹江" => "MDG",
            '马公' => 'MZG',
            "漠河" => "OHE",
            "南昌" => "KHN",
            "南充" => "NAO",
            "南京" => "NKG",
            '宁蒗' => 'NLH',
            "南宁" => "NNG",
            "南通" => "NTG",
            "南阳" => "NNY",
            "宁波" => "NGB",
            "那拉提" => "NLT",
            "攀枝花" => "PZI",
            "且末" => "IQM",
            "青岛" => "TAO",
            "庆阳" => "IQN",
            "山海关" => "SHP",
            "齐齐哈尔" => "NDG",
            "衢州" => "JUZ",
            "黔江" => "JIQ",
            '池州' => 'JUH',
            '金昌' => 'JIC',
            '台中' => 'RMQ',
            '巴彦淖尔' => 'RLK',
            '日喀则' => 'RKZ',
            "三亚" => "SYX",
            "上海" => "SHA",
            //"上海浦东" => "PVG",
            "汕头" => "SWA",
            "沈阳" => "SHE",
            '秦皇岛' => 'SHP',
            '普洱' => 'SYM',
            "深圳" => "SZX",
            '荆州' => 'SHS',
            "石家庄" => "SJW",
            "思茅" => "SYM",
            "塔城" => "TCG",
            "唐山" => "TVS",
            "太原" => "TYN",
            "天津" => "TSN",
            '台东' => 'TTT',
            '吐鲁番' => 'TLQ',
            '台南' => 'TNN',
            '通化' => 'TNH',
            "通辽" => "TGO",
            "铜仁" => "TEN",
            "腾冲" => "TCZ",
            "天水" => "THQ",
            "乌兰浩特" => "HLH",
            "乌鲁木齐" => "URC",
            "万州" => "WXN",
            "潍坊" => "WEF",
            "威海" => "WEH",
            "文山" => "WNH",
            "温州" => "WNZ",
            "乌海" => "WUA",
            "武汉" => "WUH",
            "无锡" => "WUX",
            '望安' => 'WOT',
            "武夷山" => "WUS",
            "梧州" => "WUZ",
            "厦门" => "XMN",
            "襄阳" => "XFN",
            "西安" => "XIY",
            '邢台' => 'XNT',
            '新源' => 'NLT',
            "西昌" => "XIC",
            "锡林浩特" => "XIL",
            "西宁" => "XNN",
            "徐州" => "XUZ",
            "兴义" => "ACX",
            "延安" => "ENY",
            "盐城" => "YNZ",
            "延吉" => "YNJ",
            "烟台" => "YNT",
            "宜宾" => "YBP",
            "宜昌" => "YIH",
            "银川" => "INC",
            "伊宁" => "YIN",
            "义乌" => "YIW",
            "永州" => "LLF",
            "榆林" => "UYN",
            "运城" => "YCU",
            "伊春" => "LDS",
            "玉树" => "YUS",
            '张掖' => 'YZY',
            "扬州" => "YTY",
            '宜春' => 'YIC',
            "张家界" => "DYG",
            "湛江" => "ZHA",
            "昭通" => "ZAT",
            "郑州" => "CGO",
            "舟山" => "HSN",
            "珠海" => "ZUH",
            "中卫" => "ZHY",
            "遵义" => "ZYI",
            '张家口' => 'ZQZ',
        );
        return $codeArr[$code];
    }


    /**
     * 三字码
     */
    function nameToAirport($code)
    {
        $codearr = array(
            'AKU' => '阿克苏',
            'AAT' => '阿勒泰',
            'AKA' => '安康',
            'AVA' => '安顺',
            'AQG' => '安庆',
            'AYN' => '安阳',
            'AOG' => '鞍山',
            'AEB' => '百色',
            'BSD' => '保山',
            'BFJ' => '毕节',
            'BFU' => '蚌埠',
            'BAV' => '包头',
            'BHY' => '北海',
            'PEK' => '北京',
            //'NAY' => '北京南苑',
            'BPL' => '博乐',
            'CGQ' => '长春',
            'CGD' => '常德',
            'CNI' => '长海',
            'BPX' => '昌都',
            'CSX' => '长沙',
            'CIH' => '长治',
            'CZX' => '常州',
            'CHW' => '酒泉',
            'CHG' => '朝阳',
            'CTU' => '成都',
            'CIF' => '赤峰',
            'CKG' => '重庆',
            'NBS' => '长白山',
            'DLC' => '大连',
            'DAX' => '达州',
            'DLU' => '大理',
            'DDG' => '丹东',
            'DAT' => '大同',
            'DAX' => '达县',
            'DCY' => '稻城',
            'DIG' => '迪庆',
            'DOY' => '东营',
            'DNH' => '敦煌',
            'DQA' => '大庆',
            'LUM' => '德宏',
            'DSN' => '鄂尔多斯',
            'ENH' => '恩施',
            'ENY' => '延安',
            'ERL' => '二连浩特',
            'FUO' => '佛山',
            'FOC' => '福州',
            'FYN' => '富蕴',
            'FUG' => '阜阳',
            'KOW' => '赣州',
            'GOQ' => '格尔木',
            'GHN' => '广汉',
            'GXH' => '甘南',
            'GYS' => '广元',
            'CAN' => '广州',
            'KWL' => '桂林',
            'KWE' => '贵阳',
            'GYU' => '固原',
            'HAK' => '海口',
            'HLD' => '海拉尔',
            'HMI' => '哈密',
            'HDG' => '邯郸',
            'HPG' => '神农架',
            'HSC' => '韶关',
            'HUZ' => '惠州',
            'HGH' => '杭州',
            'HZG' => '汉中',
            'HRB' => '哈尔滨',
            'HFE' => '合肥',
            'HEK' => '黑河',
            'HNY' => '衡阳',
            'HUN' => '花莲',
            'HTN' => '和田',
            'HET' => '呼和浩特',
            'TXN' => '黄山',
            'HYN' => '黄岩',
            'HJJ' => '怀化',
            'HIA' => '淮安',
            'JIL' => '吉林',
            'JZH' => '黄龙',
            'JMU' => '佳木斯',
            'JGN' => '嘉峪关',
            'TNA' => '济南',
            'JDZ' => '景德镇',
            'JGS' => '井冈山',
            'JNG' => '济宁',
            'JNZ' => '锦州',
            'JIU' => '九江',
            'JIC' => '金昌',
            'JUH' => '池州',
            'JZH' => '九寨沟',
            'JXA' => '鸡西',
            'JJN' => '晋江',
            'JHG' => '景洪',
            'KRY' => '克拉玛依',
            'KHG' => '喀什',
            'KRL' => '库尔勒',
            'KCA' => '库车',
            'KJI' => '布尔津',
            'KMG' => '昆明',
            'KHH' => '高雄',
            'KGT' => '康定',
            'LHW' => '兰州',
            'LXA' => '拉萨',
            'LYG' => '连云港',
            'LJG' => '丽江',
            'LCX' => '连城',
            'LNJ' => '临沧',
            'LYI' => '临沂',
            'LZY' => '林芝',
            'HZH' => '黎平',
            'LZH' => '柳州',
            'LYA' => '洛阳',
            'LZO' => '泸州',
            'LLB' => '荔波',
            'LCX' => '龙岩',
            'LUM' => '芒市',
            'NZH' => '满洲里',
            'MZG' => '马公',
            'MXZ' => '梅州',
            'MIG' => '绵阳',
            'MDG' => '牡丹江',
            'OHE' => '漠河',
            'KHN' => '南昌',
            'KNH' => '金门',
            'NAO' => '南充',
            'NKG' => '南京',
            'NLH' => '宁蒗',
            'NGQ' => '阿里',
            'NNG' => '南宁',
            'NTG' => '南通',
            'NNY' => '南阳',
            'NGB' => '宁波',
            'NLT' => '那拉提',
            'PZI' => '攀枝花',
            'IQM' => '且末',
            'TAO' => '青岛',
            'IQN' => '庆阳',
            'SHP' => '秦皇岛',
            'NDG' => '齐齐哈尔',
            'JJN' => '泉州',
            'JUZ' => '衢州',
            'JIQ' => '黔江',
            'RMQ' => '台中',
            'RLK' => '巴彦淖尔',
            'RKZ' => '日喀则',
            'SYX' => '三亚',
            'SHA' => '上海',
            //'PVG' => '上海浦东',
            'SWA' => '汕头',
            'SHS' => '荆州',
            'SHE' => '沈阳',
            'SYM' => '普洱',
            'SZX' => '深圳',
            'SJW' => '石家庄',
            'SYM' => '思茅',
            'SHP' => '山海关',
            'TCG' => '塔城',
            'TNH' => '通化',
            'TVS' => '唐山',
            'TYN' => '太原',
            'TSN' => '天津',
            'TGO' => '通辽',
            'TEN' => '铜仁',
            'TLQ' => '吐鲁番',
            'TCZ' => '腾冲',
            'TTT' => '台东',
            'TNN' => '台南',
            'THQ' => '天水',
            'HYN' => '台州',
            'HLH' => '乌兰浩特',
            'URC' => '乌鲁木齐',
            'WXN' => '万州',
            'WEF' => '潍坊',
            'WEH' => '威海',
            'WNH' => '文山',
            'WNZ' => '温州',
            'WUA' => '乌海',
            'WUH' => '武汉',
            'WUX' => '无锡',
            'WUS' => '武夷山',
            'WUZ' => '梧州',
            'WOT' => '望安',
            'XMN' => '厦门',
            'XFN' => '襄阳',
            'NLT' => '新源',
            'XIY' => '西安',
            'XIC' => '西昌',
            'XNT' => '邢台',
            'XIL' => '锡林浩特',
            'XNN' => '西宁',
            'JHG' => '西双版纳',
            'XUZ' => '徐州',
            'ACX' => '兴义',
            'DIG' => '香格里拉',
            'ENY' => '延安',
            'YNZ' => '盐城',
            'YNJ' => '延吉',
            'YNT' => '烟台',
            'YBP' => '宜宾',
            'YIH' => '宜昌',
            'YIE' => '阿尔山',
            'INC' => '银川',
            'YIN' => '伊宁',
            'YIW' => '义乌',
            'LLF' => '永州',
            'UYN' => '榆林',
            'YCU' => '运城',
            'LDS' => '伊春',
            'YUS' => '玉树',
            'YTY' => '扬州',
            'YZY' => '张掖',
            'YIC' => '宜春',
            'DYG' => '张家界',
            'ZHA' => '湛江',
            'ZAT' => '昭通',
            'CGO' => '郑州',
            'HSN' => '舟山',
            'ZUH' => '珠海',
            'ZHY' => '中卫',
            'HJJ' => '芷江',
            'ZYI' => '遵义',
            'ZQZ' => '张家口',
        );
        return $codearr[$code];
    }


    /**
     * 二字码
     */
    function nameToAirLine($code)
    {
        $codearr = array(
            'CA' => '中国国际航空公司',
            'CZ' => '中国南方航空公司',
            'MU' => '中国东方航空公司',
            'MF' => '中国厦门航空公司',
            '3U' => '中国四川航空公司',
            'CN' => '大新华航空有限公司',
            'G5' => '华夏航空有限公司',
            'HO' => '上海吉祥航空有限公司',
            'HU' => '海南航空公司',
            'SC' => '中国山东航空公司',
            'ZH' => '中国深圳航空公司',
            'FM' => '上海航空公司',
            'BK' => '奥凯航空有限公司',
            'EU' => '鹰联航空有限公司',
            'GS' => '天津航空有限公司',
            'KN' => '中国联合航空有限公司',
            'JD' => '首都航空',
            'IV' => '中国福建航空公司',
            'CJ' => '中国北方航空公司',
            '8C' => '东星航空有限公司',
            '8L' => '云南祥鹏航空有限责任公司',
            'OQ' => '重庆航空有限责任公司',
            'VD' => '鲲鹏航空有限公司',
            'JR' => '中国幸福航空',
            'NS' => '东北航空有限公司',
            'PN' => '西部航空有限责任公司',
            '9C' => '春秋航空公司',
            'HR' => '中国联合航空公司',
            'IJ' => '长城航空有限公司',
            'F4' => '上海国际货运航空有限公司',
            'JI' => '翡翠国际货运航空有限责任公司',
            'J5' => '东海航空有限公司',
            'PO' => '中国邮政航空有限公司',
            'LD' => '香港华民航空',
            'PN' => '西部航空有限责任公司',
            'HC' => '中国海洋直升机公司',
            'HY' => '中国航空货运公司',
            'Y8' => '扬子江快运航空有限公司',
            'NX' => '澳门航空公司',
            'ZG' => '非凡航空',
            'CX' => '国泰航空有限公司',
            'HX' => '香港航空公司',
            'O8' => '甘泉香港航空公司',
            'UO' => '香港快运航空',
            'CI' => '中华航空股份有限公司',
            'BR' => '长荣航空股份有限公司',
            'GE' => '复兴航空运输股份有限公司',
            'AE' => '华信航空股份有限公司',
            'B7' => '立荣航空公司EU鹰联航空',
            'EF' => '远东航空股份有限公司',
        );
        return $codearr[$code];
    }


    /**
     * 三字码
     */
    function nameToFullAirport($code)
    {
        $codearr = array(
            'AVA' => '黄果树机场',
            'AYN' => '北郊机场',
            'AKU' => '阿克苏机场',
            'AAT' => '阿勒泰机场',
            'AKA' => '安康机场',
            'AQG' => '安庆机场',
            'AOG' => '鞍山机场',
            'AEB' => '巴马机场',
            'BSD' => '云瑞机场',
            'BFU' => '蚌埠机场',
            'BDU' => '昌都邦达',
            'BAV' => '二里半机场',
            'BFJ' => '飞雄机场',
            'BHY' => '福城机场',
            'PEK' => '首都机场',
            'NAY' => '南苑机场',
            'BPL' => '博乐阿拉山口机场',
            'CGQ' => '龙嘉国际机场',
            'CHW' => '酒泉机场',
            'CYI' => '嘉义机场',
            'CGD' => '桃花源机场',
            'CMJ' => '七美机场',
            'BPX' => '邦达机场',
            'CSX' => '黄花机场',
            'CIH' => '王村机场',
            'CZX' => '奔牛机场',
            'CNI' => '长海机场',
            'CHG' => '朝阳机场',
            'CTU' => '双流机场',
            'CIF' => '玉龙机场',
            'CKG' => '江北机场',
            'NBS' => '长白山机场',
            'DLC' => '国际机场',
            'DCY' => '亚丁机场',
            'DLU' => '荒草坝机场',
            'DDG' => '浪头机场',
            'DAT' => '大同机场',
            'DAX' => '达州机场',
            'DIG' => '香格里拉机场',
            'DOY' => '胜利机场',
            'DNH' => '敦煌机场',
            'DQA' => '萨尔图机场',
            'LUM' => '芒市机场',
            'DSN' => '伊金霍洛机场',
            'ENH' => '恩施机场',
            'ENY' => '二十里铺机场',
            'ERL' => '赛乌苏国际机场',
            'FUO' => '沙堤机场',
            'FYN' => '富蕴机场',
            'FOC' => '长乐机场',
            'FUG' => '西关机场',
            'JIL' => '二台子机场',
            'KOW' => '黄金机场',
            'GOQ' => '格尔木机场',
            'GYS' => '盘龙机场',
            'GXH' => '夏河机场',
            'GHN' => '广汉机场',
            'CAN' => '白云机场',
            'KWL' => '两江机场',
            'KWE' => '龙洞堡机场',
            'KHH' => '高雄机场',
            'GYU' => '六盘山机场',
            'HAK' => '美兰机场',
            'HLD' => '东山机场',
            'HMI' => '哈密机场',
            'HDG' => '邯郸机场',
            'HSC' => '韶关机场',
            'HGH' => '萧山机场',
            'HZG' => '西关机场',
            'HRB' => '太平机场',
            'HFE' => '骆岗机场',
            'HEK' => '黑河机场',
            'HTN' => '和田机场',
            'HPG' => '红坪机场',
            'HET' => '白塔国际机场',
            'TXN' => '屯溪机场',
            'HYN' => '路桥机场',
            'HUZ' => '平潭机场',
            'HJJ' => '芷江机场',
            'HIA' => '涟水机场',
            'HNY' => '东江机场',
            'JMU' => '佳木斯机场',
            'JGN' => '嘉峪关机场',
            'TNA' => '遥墙机场',
            'JDZ' => '罗家机场',
            'JGS' => '井冈山机场',
            'JNG' => '济宁机场',
            'JUH' => '九华山机场',
            'JNZ' => '小岭子机场',
            'JIU' => '庐山机场',
            'JIC' => '金昌机场',
            'JZH' => '黄龙机场',
            'JXA' => '兴凯湖机场',
            'JJN' => '晋江机场',
            'KJI' => '喀纳斯机场',
            'KRY' => '克拉玛依机场',
            'KHG' => '喀什机场',
            'KRL' => '库尔勒机场',
            'KCA' => '库车机场',
            'KMG' => '巫家坝机场',
            'KNH' => '尚义机场',
            'KGT' => '康定机场',
            'KNC' => '吉安机场',
            'LHW' => '兰州机场',
            'LXA' => '贡嘎机场',
            'LCX' => '连城机场',
            'LYG' => '白塔埠机场',
            'LJG' => '三义机场',
            'LZJ' => '云南丽江机场',
            'LNJ' => '临沧机场',
            'LYI' => '沭埠岭机场',
            'LZY' => '米林机场',
            'HZH' => '黎平机场',
            'LZH' => '白莲机场',
            'LYA' => '洛阳机场',
            'LZO' => '蓝田机场',
            'LCX' => '福建龙岩冠豸山机场',
            'LLB' => '荔波机场',
            'LUM' => '芒市机场',
            'NZH' => '西郊机场',
            'MXZ' => '梅州机场',
            'MZG' => '马公机场',
            'MIG' => '南郊机场',
            'MDG' => '海浪机场',
            'OHE' => '古莲机场',
            'KHN' => '昌北机场',
            'NAO' => '高坪机场',
            'NGQ' => '昆莎机场',
            'NKG' => '禄口机场',
            'NNG' => '吴圩机场',
            'NTG' => '兴东机场',
            'NLH' => '泸沽湖机场',
            'NNY' => '姜营机场',
            'NGB' => '栎社机场',
            'NNN' => '嫩江机场',
            'NLT' => '那拉提机场',
            'PZI' => '保安营机场',
            'IQM' => '且末机场',
            'TAO' => '流亭机场',
            'IQN' => '庆阳机场',
            'NDG' => '三家子机场',
            'JJN' => '泉州晋江国际机场',
            'JUZ' => '衢州机场',
            'JIQ' => '武陵山机场',
            'RMQ' => '清泉岗机场',
            'RKZ' => '和平机场',
            'RLK' => '天吉泰机场',
            'SZV' => '苏州机场',
            'SYX' => '凤凰机场',
            'SHA' => '虹桥机场',
            'PVG' => '浦东机场',
            'SWA' => '揭阳潮汕机场',
            'SHE' => '桃仙机场',
            'SZX' => '宝安机场',
            'SJW' => '正定机场',
            'SYM' => '普洱机场',
            'SHS' => '沙市机场',
            'SHP' => '山海关机场',
            'SWA' => '外砂机场',
            'TNH' => '通化机场',
            'TCG' => '塔城机场',
            'TVS' => '三女河机场',
            'TTT' => '丰年机场',
            'TLQ' => '交河机场',
            'TYN' => '武宿机场',
            'TVS' => '三女河机场',
            'TSN' => '滨海机场',
            'TSA' => '台北松山机场',
            'TPE' => '桃园机场',
            'TGO' => '通辽机场',
            'TEN' => '凤凰机场',
            'TCZ' => '驼峰机场',
            'THQ' => '麦积山机场',
            'TNN' => '台南机场',
            'HYN' => '台州路桥机场',
            'HUN' => '花莲机场',
            'HLH' => '乌兰浩特机场',
            'URC' => '地窝堡机场',
            'WXN' => '五桥机场',
            'WEF' => '潍坊机场',
            'WEH' => '文登大水泊机场',
            'WOT' => '望安机场',
            'WNH' => '普者黑机场',
            'WNZ' => '永强机场',
            'WUA' => '乌海机场',
            'WUH' => '天河机场',
            'WJD' => '王家墩国际机场',
            'WUX' => '硕放机场',
            'WUS' => '武夷山机场',
            'WUZ' => '长洲岛机场',
            'XMN' => '高崎机场',
            'XFN' => '刘集机场',
            'XEN' => '兴城机场',
            'XIY' => '咸阳机场',
            'XIC' => '青山机场',
            'XIL' => '锡林浩特机场',
            'XNT' => '褡裢机场',
            'XNN' => '曹家堡机场',
            'NLT' => '那拉提机场',
            'JHG' => '嘎洒机场',
            'XUZ' => '观音机场',
            'ACX' => '兴义万峰林机场',
            'DIG' => '香格里拉机场',
            'ENY' => '二十里铺机场',
            'YNZ' => '南洋机场',
            'YNJ' => '朝阳川机场',
            'YIE' => '伊尔施机场',
            'YIC' => '明月山机场',
            'YNT' => '莱山机场',
            'YBP' => '宜宾机场',
            'YIH' => '三峡机场',
            'INC' => '河东机场',
            'YIN' => '伊宁机场',
            'YZY' => '张掖机场',
            'YIW' => '义乌机场',
            'LLF' => '零陵机场',
            'UYN' => '西沙机场',
            'YCU' => '关公机场',
            'LDS' => '林都机场',
            'YUS' => '巴塘机场',
            'YTY' => '泰州机场',
            'DYG' => '荷花机场',
            'ZHA' => '湛江机场',
            'ZAT' => '昭通机场',
            'CGO' => '新郑机场',
            'HSN' => '普陀山朱家尖机场',
            'ZUH' => '三灶机场',
            'ZHY' => '沙坡头机场',
            'ZYI' => '遵义机场',
            'ZQZ' => '宁远机场',
        );
        return $codearr[$code];
    }

}
