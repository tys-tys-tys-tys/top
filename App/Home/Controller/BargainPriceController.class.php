<?php

namespace Home\Controller;

use Common\Top;
use Common\Utils;
use Think\Controller;

class BargainPriceController extends CommonController
{


    public function index()
    {
        $operatorId = $this->shareOperators;

        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
        } else {
            $where['state'] = 10;
        }

        $list = M('tour')
            ->where($where)
            ->field('id,cover_pic')
            ->order('id desc')
            ->group('destination')
            ->limit(1)
            ->find();

        $pic = substr($list['cover_pic'], 0, 7);
        $picUrl = str_replace($pic, C('FILE1_BASE_URL'), $list['cover_pic']);
        $pic = Utils::GetImageUrl($picUrl, 300, 300, 1);

        $userList = $this->userInfo;
        $page = I('page');

        if ($page == '') {
            $page = 1;
        }

        $html = $this->bargainPriceListGetPageHtml($page);

        if (IS_POST && IS_AJAX) {
            $this->ajaxReturn($html);
        } else {
            $this->assign('html', $html);
        }

        $domain = $_SERVER['SERVER_NAME'];
        $domain = "http://" . $domain . "/index.php/Home/BargainPrice/index.html";
        $this->assign('domain', $domain);
        $this->assign('pic', $pic);
        $this->assign('userList', $userList);
        $this->display();
    }


    public function lists()
    {
        $id = I('get.id');
        //查找 toutr 表的line_type、destination两个字段
        $areaList = M('tour')
            ->field('id,line_type,destination')
            ->where("id='{$id}'")
            ->find();
        $line_type['line_type'] = $areaList['line_type'];

        $where['destination'] = $areaList['destination'];

        $operatorId = $this->shareOperators;

        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
        } else {
            $where['state'] = 10;
        }

        $list = M('tour')
            ->where($where)
            ->field('id,cover_pic')
            ->order('id desc')
            ->limit(1)
            ->find();

        $pic = substr($list['cover_pic'], 0, 7);
        $picUrl = str_replace($pic, C('FILE1_BASE_URL'), $list['cover_pic']);
        $pic = Utils::GetImageUrl($picUrl, 300, 300, 1);

        $userList = $this->userInfo;
        $page = I('page');

        if ($page == '') {
            $page = 1;
        }

        $html = $this->bargainPriceMoreGetPageHtml($page);

        if (IS_POST && IS_AJAX) {
            $this->ajaxReturn($html);
        } else {
            $this->assign('html', $html);
        }

        $id = I('id');
        $domain = $_SERVER['SERVER_NAME'];
        $domain = "http://" . $domain . "/index.php/Home/BargainPrice/lists/id/{$id}.html";
        $this->assign('domain', $domain);
        $this->assign('id', $id);
        $this->assign('pic', $pic);
        $this->assign('userList', $userList);
        $this->display();
    }

  //限时特价搜索列表及加载更多
    public function search()
    {
         $operatorId = $this->shareOperators;

        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
        } else {
            $where['state'] = 10;
        }

        $list = M('tour')
            ->where($where)
            ->field('id,cover_pic')
            ->order('id desc')
            ->limit(1)
            ->find();
       
   $pic = substr($list['cover_pic'], 0, 7);
        $picUrl = str_replace($pic, C('FILE1_BASE_URL'), $list['cover_pic']);
        $pic = Utils::GetImageUrl($picUrl, 300, 300, 1);

        $userList = $this->userInfo;
        $page = I('page');

        if ($page == '') {
            $page = 1;
        }

        $html = $this->bargainPriceSearchMoreGetPageHtml($page);

        if (IS_POST && IS_AJAX) {
            $this->ajaxReturn($html);
        } else {
            $this->assign('html', $html);
        }

        $domain = $_SERVER['SERVER_NAME'];
        $domain = "http://" . $domain . "/index.php/Home/Outbound/index.html";
        $this->assign('domain', $domain);
        $this->assign('pic', $pic);
        $this->assign('userList', $userList);
        $this->display();
    }

    protected function bargainPriceListGetPageHtml($page)
    {
        $userList = $this->userInfo;
        $pageSize = C('PRODUCT_PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $operatorId = $this->shareOperators;

        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
        } else {
            $where['state'] = 10;
        }

        $list = M('tour')
            ->where($where)
            ->field(array('count(destination)' => 'total', 'id,destination,cover_pic,min_price'))
            ->order('id desc')
            ->group('destination')
            ->limit($index, $pageSize)
            ->select();

        $agencyId = $userList['id'];
        //查看是否改价
        $now = date("Y-m-d 00:00:00");
        $agencyTourList = M('agency_tour_price')->where("agency_id = '{$agencyId}' && start_time >= '{$now}'")->field('agency_id,start_time,tour_id,min(price_adult_list) price_adult_list')->group('tour_id')->select();

        $arr3 = array();
        foreach ($list as $key => $vo) {
            foreach ($agencyTourList as $k => $v) {
                if ($vo['id'] == $v['tour_id']) {
                    $arr3[$key]['id'] = $vo['id'];
                    $list[$key]['min_price'] = min((int)$vo['min_price'], (int)$v['price_adult_list']);
                } else {
                    $arr3[$key]['id'] = $vo['id'];
                    $arr3[$key]['min_price'] = Utils::getYuan($vo['min_price']);
                }
            }
        }

        $Pic = array();
        foreach ($list as $key => $vo) {
            $vo['min_price'] = Utils::getYuan($vo['min_price']);
            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic[] = Utils::GetImageUrl($url, 300, 300, 1);

            if ($vo['cover_pic'] == '') {
                $coverPic = "<img class='lazy' data-original='http://file1.lydlr.com/c/300/300/tour/2016/0722/01414731830.jpg'/>";
            } else {
                $coverPic = "<img class='lazy' data-original='" . $Pic[$key] . "'/>";
            }

            $evaldata .= "<dl class='item clearfix'>";
            $evaldata .= "<dt><a href=" . U('BargainPrice/lists', array('id' => $vo['id'])) . ">" . $coverPic . "</a></dt>";
            $evaldata .= "<dd>";
            $evaldata .= "<a href=" . U('BargainPrice/lists', array('id' => $vo['id'])) . ">";
            $evaldata .= "<p class='name'>" . $vo['destination'] . "</p>";
            $evaldata .= "<p class='bt'>" . $vo['total'] . "条<span class='price'><dfn>&yen;" . $vo['min_price'] . "</dfn>起</span></p>";
            $evaldata .= "</a>";
            $evaldata .= "</dd>";
            $evaldata .= "</dl>";
        }

        return $evaldata;
    }



    protected function bargainPriceMoreGetPageHtml($page)
    {
        $userList = $this->userInfo;
        $id = I('id');

        //查找 toutr 表的line_type、destination两个字段
        $areaList = M('tour')
            ->field('id,line_type,destination')
            ->where("id='{$id}'")
            ->find();
        $line_type['line_type'] = $areaList['line_type'];

        $where['destination'] = $areaList['destination'];

        $operatorId = $this->shareOperators;

        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
        } else {
            $where['state'] = 10;
        }

        $pageSize = C('PRODUCT_PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $list = M('tour')
            ->where($where)
            ->field('id,name,min_price,start_time,cover_pic,line_type,update_time,departure')
            ->order('update_time desc')
            ->limit($index, $pageSize)
            ->select();

        $agencyId = $userList['id'];
        //查看是否改价
        $now = date("Y-m-d 00:00:00");
        $agencyTourList = M('agency_tour_price')->where("agency_id = '{$agencyId}' && start_time >= '{$now}'")->field('agency_id,start_time,tour_id,min(price_adult_list) price_adult_list')->group('tour_id')->select();

        $arr3 = array();
        foreach ($list as $key => $vo) {
            foreach ($agencyTourList as $k => $v) {
                if ($vo['id'] == $v['tour_id']) {
                    $arr3[$key]['id'] = $vo['id'];
                    $list[$key]['min_price'] = min((int)$vo['min_price'], (int)$v['price_adult_list']);
                } else {
                    $arr3[$key]['id'] = $vo['id'];
                    $arr3[$key]['min_price'] = Utils::getYuan($vo['min_price']);
                }
            }
        }

        $Pic = array();
        foreach ($list as $key => $vo) {
            $departure = explode(" ", $vo['departure']);
            if ($departure['0'] == $departure['1']) {
                unset($departure['1']);
                $departure = implode(" ", $departure);
            } else {
                $departure = implode(" ", $departure);
            }

            $dep = "<span style='background-color:#1e86d4;color:white;padding: 2px 5px;
    border-radius: 3px;'>{$departure} 出发</span>";

            if ($vo['line_type'] == 1) {
                $lineTypeName = '周边游';
            } else if ($vo['line_type'] == 2) {
                $lineTypeName = '国内游';
            } else if ($vo['line_type'] == 3) {
                $lineTypeName = '出境游';
            }

            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic[] = Utils::GetImageUrl($url, 300, 300, 1);

            $vo['min_price'] = Utils::getYuan($vo['min_price']);

            if ($vo['cover_pic'] == '') {
                $coverPic = "<img class='lazy' data-original='http://file1.lydlr.com/c/300/300/tour/2016/0722/01414731830.jpg'/>";
            } else {
                $coverPic = "<img class='lazy' data-original='" . $Pic[$key] . "'/>";
            }

            $evaldata .= "<dl class='item clearfix'>";
            $evaldata .= "<dt><a href=" . U('Agency/details', array('id' => $vo['id'])) . ">" . $coverPic . "</a></dt>";
            $evaldata .= "<dd>";
            $evaldata .= "<a href=" . U('Agency/details', array('id' => $vo['id'])) . ">";
            $evaldata .= "<p class='name'>" . $vo['name'] . "</p>";
            $evaldata .= "<div class='mid clearfix'>";
            $evaldata .= "<span class='tag pull-left'>" . $lineTypeName . "</span>";
            $evaldata .= "<span class='price pull-right'><dfn>&yen;" . $vo['min_price'] . "</dfn>起</span>";
            $evaldata .= "</div>";
            $evaldata .= "<div class='bottom clearfix'>";
            $evaldata .= "<span class='tag pull-left'>" . $dep . "</span>";
            $evaldata .= "<span class='ck pull-right'>[详情]</span>";
            $evaldata .= "</div>";
            $evaldata .= "</a>";
            $evaldata .= "</dd>";
            $evaldata .= "</dl>";
        }

        return $evaldata;
    }
          /*
     *限时特价搜索及加载更多处理-拼接html 
     **/
    protected function bargainPriceSearchMoreGetPageHtml($page)
    {
          $userList = $this->userInfo;
        $pageSize = C('PRODUCT_PAGE_SET');
        $index = ($page - 1) * $pageSize;

        $operatorId = $this->shareOperators;
         $name = I('names');
            $nameId = substr($name, 2);
            $nameCode = substr($name, 0, 2);
            $state = Top::ProductStateIsSale;

            if ($nameCode == '10') {
                if (is_numeric($name)) {
                    $tourInfo = M('tour')->where("id = '{$nameId}' && state = '{$state}' && operator_id in($operatorId)")->find();
                    $rediRectUrl = "Agency/details";
                }
            } else if ($nameCode == '20') {
                if (is_numeric($name)) {
                    $tourInfo = M('product')->where("id = '{$nameId}' && state = '{$state}'")->find();
                    $rediRectUrl = "Product/details";
                }
            }
            
            if (!empty($tourInfo)) {
                $this->redirect($rediRectUrl, array("id" => $nameId));
            }
        if (!empty($operatorId)) {
            $where['operator_id'] = array('in', $operatorId);
            $where['promotion'] = 1;
            $where['state'] = Top::ProductStateIsSale;
            $where['provider_type'] = Top::NotOutOrder;
             if($name){
                $where['name|destinations|provider_name'] = array('like', '%' . $name . '%');
            }
        } else {
            $where['state'] = 10;
        }
        $list = M('tour')
            ->where($where)
            ->field('id,name,min_price,start_time,cover_pic,update_time,departure,destination')
            ->limit($index, $pageSize)
            ->order('update_time desc')
            ->select();
        $this->assign('name', $name);
        $agencyId = $userList['id'];

        $now = date("Y-m-d 00:00:00");
        $agencyTourList = M('agency_tour_price')->where("agency_id = '{$agencyId}' && start_time >= '{$now}'")->field('agency_id,start_time,tour_id,min(price_adult_list) price_adult_list')->group('tour_id')->select();

        $arr3 = array();
        foreach ($list as $key => $vo) {
            foreach ($agencyTourList as $k => $v) {
                if ($vo['id'] == $v['tour_id']) {
                    $arr3[$key]['id'] = $vo['id'];
                    $list[$key]['min_price'] = min((int)$vo['min_price'], (int)$v['price_adult_list']);
                } else {
                    $arr3[$key]['id'] = $vo['id'];
                    $arr3[$key]['min_price'] = Utils::getYuan($vo['min_price']);
                }
            }
        }

        $Pic = array();
        foreach ($list as $key => $vo) {
            $departure = explode(" ", $vo['departure']);
            if ($departure['0'] == $departure['1']) {
                unset($departure['1']);
                $departure = implode(" ", $departure);
            } else {
                $departure = implode(" ", $departure);
            }

            $dep = "<span style='background-color:#1e86d4;color:white;padding: 2px 5px;
    border-radius: 3px;'>{$departure} 出发</span>";

            $coverPic = $vo['cover_pic'];
            $url = str_replace("#file1#", C('FILE1_BASE_URL'), $coverPic);
            $Pic[] = Utils::GetImageUrl($url, 300, 300, 1);

            $vo['min_price'] = Utils::getYuan($vo['min_price']);

            if ($vo['cover_pic'] == '') {
                $coverPic = "<img class='lazy' data-original='http://file1.lydlr.com/c/300/300/tour/2016/0722/01414731830.jpg'/>";
            } else {
                $coverPic = "<img class='lazy' data-original='" . $Pic[$key] . "'/>";
            }

            $evaldata .= "<dl class='item clearfix'>";
            $evaldata .= "<dt><a href=" . U('Agency/details', array('id' => $vo['id'])) . ">" . $coverPic . "</a></dt>";
            $evaldata .= "<dd>";
            $evaldata .= "<a href=" . U('Agency/details', array('id' => $vo['id'])) . ">";
            $evaldata .= "<p class='name'>" . $vo['name'] . "</p>";
            $evaldata .= "<div class='mid clearfix'>";
            $evaldata .= "<span class='tag pull-left'>限时特价---".$vo['destination']."</span>";
            $evaldata .= "<span class='price pull-right'><dfn>&yen;" . $vo['min_price'] . "</dfn>起</span>";
            $evaldata .= "</div>";
            $evaldata .= "<div class='bottom clearfix'>";
            $evaldata .= "<span class='tag pull-left'>" . $dep . "</span>";
            $evaldata .= "<span class='ck pull-right'>[详情]</span>";
            $evaldata .= "</div>";
            $evaldata .= "</a>";
            $evaldata .= "</dd>";
            $evaldata .= "</dl>";
        }

        return $evaldata;
    }


}
