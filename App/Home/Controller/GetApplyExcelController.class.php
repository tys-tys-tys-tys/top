<?php

namespace Home\Controller;

use Common\Top;
use Common\Utils;
use Think\Controller;

class GetApplyExcelController extends CommonController
{

    /**
     * Excel 代理商 - 交易记录 - 导出
     */
    public function getAgencyTransactionExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entityType = Top::EntityTypeAgency;
            $agencyId = $this->userInfo['id'];
            $operatorId = $this->userInfo['operator_id'];
            $name = $this->userInfo['name'];

            $where = "operator_id = '{$operatorId}' && entity_type = '{$entityType}' && entity_id = '{$agencyId}'";


            $data = M('account_transaction')->where($where)->select();
            $date = date('Y-m-d', time());
            $fileName = "_{$date}{$name}的交易记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '订单号');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '交易号');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '明细');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '用途');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '金额');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '余额');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '操作时间');

            $i = 2;
            foreach ($data as $r) {
                if ($r['object_type'] == 1) {
                    $r['objectName']['tour_name'] = '充值';
                } else if ($r['object_type'] == 2) {
                    $r['objectName']['tour_name'] = '提现';
                } else if ($r['object_type'] == 3) {
                    $r['objectName'] = M('agency_invoice')->where("id = " . $r['object_id'])->field('detail tour_name')->find();
                } else if ($r['object_type'] == 4) {
                    $r['objectName']['tour_name'] = '合同';
                } else if ($r['object_type'] == 5) {
                    $r['objectName'] = M('order_tour')->where("id = " . $r['object_id'])->field('tour_name')->find();
                } else if ($r['object_type'] == 6) {
                    $r['objectName'] = M('order_product')->where("id = " . $r['object_id'])->field('product_name tour_name')->find();
                } else if ($r['object_type'] == 7) {
                    $r['objectName']['tour_name'] = '机票';
                } else if ($r['object_type'] == 8) {
                    $r['objectName']['tour_name'] = '账户调整';
                } else if ($r['object_type'] == 9) {
                    $r['objectName']['tour_name'] = '门票';
                }

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['object_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, ' ' . $r['sn']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['objectName']['tour_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['action']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['memo']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['create_time']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 充值申请 - 导出
     */
    public function getAgencyTopUpExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = $this->userInfo['operator_id'];
            $agencyId = $this->userInfo['id'];
            $name = $this->userInfo['name'];

            $where = "operator_id = '{$operatorId}' && entity_id = '{$agencyId}'";

            $data = M('account_topup')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}{$name}的充值申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '充值金额');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 代理商 - 服务费- 导出
     */
    public function getServiceChargeExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

       
            $agencyId = $this->userInfo['id'];
            $name = $this->userInfo['name'];

            $where = " entity_id = '{$agencyId}'";

            $data = M('service_charge')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}{$name}的扣除服务费记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '服务名称');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '扣除金额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '服务时间');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 0) {
                    $r['state'] = '未确认';
                } else if ($r['state'] == 1) {
                    $r['state'] = '已确认';
                }


                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $name);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['service_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, Utils::getYuan($r['service_money']));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

    /**
     * Excel 代理商 - 提现申请 - 导出
     */
    public function getAgencyWithDrawExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $entity_type = Top::EntityTypeAgency;
            $operatorId = $this->userInfo['operator_id'];
            $agencyId = $this->userInfo['id'];
            $name = $this->userInfo['name'];

            $where = "operator_id = '{$operatorId}' && entity_type = '{$entity_type}' && entity_id = '{$agencyId}'";

            $data = M('account_withdraw')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}{$name}的提现申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '提现金额');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '账户总额');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '提现账户');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '状态');

            $i = 2;
            foreach ($data as $r) {
                $entityId = $r['entity_id'];
                $balanceInfo = M('agency')->where("id = '{$entityId}' && operator_id = '{$operatorId}'")->field('id,account_balance')->find();
                $r['balance'] = $balanceInfo['account_balance'];

                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已确认，待财务处理';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                } else if ($r['state'] == 4) {
                    $r['state'] = '处理完成';
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];
                $r['bank'] = $r['bank_name'] . $r['bank_account'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, Utils::getYuan($r['amount']));
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, Utils::getYuan($r['balance']));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['bank']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 发票申请 - 导出
     */
    public function getAgencyInvoiceExcel()
    {
        if (IS_POST) {

            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = $this->userInfo['operator_id'];
            $agencyId = $this->userInfo['id'];
            $name = $this->userInfo['name'];

            $where = "operator_id = '{$operatorId}' && entity_id = '{$agencyId}'";

            $data = M('agency_invoice')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}{$name}的发票申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '发票抬头');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '发票项目');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '发票金额');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '服务费');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '索取方式');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '备注');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                if ($r['request_method'] == 1) {
                    $r['request_method'] = '自取';
                } else if ($r['request_method'] == 2 && $r['express_fee'] == 0) {
                    $r['request_method'] = '快递 到付';
                } else {
                    $r['request_method'] = '快递 包邮';
                }

                $r['amount'] = Utils::getYuan($r['amount']);
                if ($r['detail'] == '会议服务费') {
                    $r['tax'] = Utils::setServiceFeeTax($r['amount']);
                } else {
                    $r['tax'] = Utils::setTax($r['amount']);
                }

                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['title']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['detail']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['amount']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, Utils::getYuan($r['tax']));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['request_method']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $r['remark']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }


    /**
     * Excel 代理商 - 合同申请 - 导出
     */
    public function getAgencyContractExcel()
    {
        if (IS_POST) {
            import("Common.Org.PHPExcel");
            import("Common.Org.PHPExcel.Writer.Excel5");
            import("Common.Org.PHPExcel.IOFactory.php");

            $operatorId = $this->userInfo['operator_id'];
            $agencyId = $this->userInfo['id'];
            $name = $this->userInfo['name'];

            $where = "operator_id = '{$operatorId}' && entity_id = '{$agencyId}'";

            $data = M('agency_contract')->where($where)->select();

            $date = date("Y-m-d", time());
            $fileName = "_{$date}{$name}的合同申请记录.xls";

            $objPHPExcel = new \PHPExcel();

            $objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->setCellValue('A1', '编号');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', '代理商');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', '国内合同');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', '境外合同');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', '单项委托');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', '数量');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', '单价');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', '邮费');
            $objPHPExcel->getActiveSheet()->setCellValue('I1', '合计');
            $objPHPExcel->getActiveSheet()->setCellValue('J1', '索取方式');
            $objPHPExcel->getActiveSheet()->setCellValue('K1', '申请时间');
            $objPHPExcel->getActiveSheet()->setCellValue('L1', '审核时间');
            $objPHPExcel->getActiveSheet()->setCellValue('M1', '状态');

            $i = 2;
            foreach ($data as $r) {
                if ($r['express_fee'] == 0) {
                    $r['express_fee'] = 0;
                } else {
                    $r['express_fee'] = Utils::getYuan($r['express_fee']);
                }

                $r['contract_fee'] = Utils::getYuan($r['contract_fee']);

                if ($r['state'] == 1) {
                    $r['state'] = '待审核';
                } else if ($r['state'] == 2) {
                    $r['state'] = '已审核';
                } else if ($r['state'] == 3) {
                    $r['state'] = '拒绝';
                }

                if ($r['request_method'] == 1) {
                    $r['request_method'] = '自取';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'];
                } else if ($r['request_method'] == 2 && $r['express_fee'] == 0) {
                    $r['request_method'] = '快递 到付';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'];
                } else {
                    $r['request_method'] = '快递 包邮';
                    $r['totalPrice'] = $r['peritem_count'] * $r['contract_fee'] + $r['outbound_count'] * $r['contract_fee'] + $r['inland_count'] * $r['contract_fee'] + $r['express_fee'];
                }

                $r['totalNum'] = $r['inland_count'] + $r['inland_count'] + $r['peritem_count'];
                $r['proof'] = $r['proof_type'] . '   ' . $r['proof_code'];

                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $r['id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $r['entity_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $r['inland_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $r['outbound_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $r['peritem_count']);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $r['totalNum']);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $r['contract_fee']);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $r['express_fee']);
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $r['totalPrice']);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $r['request_method']);
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $r['create_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $r['update_time']);
                $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $r['state']);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('名称');

            $fileName = iconv("utf-8", "gb2312", $fileName);

            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Disposition: attachment;filename=\"$fileName\"");
            header('Cache-Control: max-age=0');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }

}
