<?php

namespace Home\Controller;

use Think\Controller;
use Common\Top;
use Common\Utils;

class FinanceController extends CommonController
{

    //个人中心-财务管理  
    public function index()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $userList = $this->userInfo;
            //实体类型
            $entityType = Top::EntityTypeAgency;
            //登陆用户id
            $entityId = $userList['id'];
            //冻结资金
            $freezeMoney = Utils::getFrozenFen($entityType, $entityId);
//            dump($freezeMoney);exit;
            //可用余额
            $availableBalance = Utils::getAgencyAvailableFen($entityId);

            $this->assign('freezeMoney', $freezeMoney);
            $this->assign('availableBalance', $availableBalance);

            $this->assign('userList', $userList);

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }

    //前台用户充值申请控制处理
    public function applyTopup()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $userList = $this->userInfo;

            if (IS_POST && IS_AJAX) {
                $data['entity_type'] = Top::EntityTypeAgency; //实体类型4，顾问
                $data['operator_id'] = $userList['operator_id']; //所属运营商id
                $data['entity_id'] = $userList['id'];  //顾问id
                $data['entity_name'] = $userList['name'];//顾问姓名
                $data['state'] = Top::ApplyStateWait; //待审核状态
                $data['account'] = $userList['account']; //账户
                $data['create_time'] = date('Y-m-d H:i:s', time());//申请充值时间
                $data['amount'] = Utils::getFen(I('amount'));//充值钱转换*100
                $data['topup_type'] = I('topup_type');//充值类型id
                $data['payer'] = I('payer'); //如果是银行转账（topup_type=4）,要加上汇款方名称
                $data['pay_state'] = 0; //线下支付

                $postCode = I('code');//判断用的码，简单的防止post恶意请求
                $code = session('applyTopUp');

                if ($postCode == $code) {
                    $result = M('account_topup')->add($data);
                    session('applyTopUp', null);
                }

                if ($result) {
                    $data['status'] = 1;
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $this->ajaxReturn($data);
                }
            } else {
                $_SESSION['applyTopUp'] = 400;
                $operatorId = $userList['operator_id'];
                $bankInfo = M('operator')->where("id = '{$operatorId}'")->field('id,topup_info,alipay_info,alipay_state')->find();
                $this->topupInfo = $bankInfo['topup_info'];
                $this->alipayInfo = $bankInfo['alipay_info'];
                $this->alipayState = $bankInfo['alipay_state'];
                $this->assign('userList', $userList);
            }
            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }

//充值申请记录
    public function topupList()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->topupListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }
//顾问确定扣除服务费处理
    public function serviceHandle()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
             $userList = $this->userInfo;
            $id = I('id');
            $service_charge =M('service_charge'); 
           $res = $service_charge->where('id='.$id)->setField('state',1);
           $dl_serveice_data = $service_charge->where('id='.$id)->find();
           $agency_result = M('agency')->where("id = '{$userList['id']}'")->setDec('account_balance', $dl_serveice_data['service_money']);
            $transaction_data['operator_id'] = $userList['operator_id'];
            $transaction_data['entity_type'] = Top::EntityTypeAgency;
            $transaction_data['action'] = $dl_serveice_data['service_name'];
            $transaction_data['sn'] = Utils::getSn();
            $transaction_data['entity_id'] = $userList['id'];
            $transaction_data['account'] = $userList['account'];
            $transaction_data['amount'] = '-' . $dl_serveice_data['service_money'];
            $transaction_data['create_time'] = date('Y-m-d H:i:s', time());
            $transaction_data['balance'] = $userList['account_balance'];
            $transaction_data['remark'] = 'top_sevice_charge.id:' . $id;
            $transaction_data['object_id'] = $id;
            $transaction_data['object_type'] = Top::ObjectTypeService;
            $transaction_data['object_name'] = '扣除顾问服务费';

            $transaction_result = M('account_transaction')->add($transaction_data);
           if($res && $transaction_result){
               $data['status'] =1;
               $data['dl_url'] = U('Finance/serviceList');
               $this->ajaxReturn($data);
           }else{
               $data['status'] = -1;
                $data['dl_url'] = U('Finance/serviceList');
               $this->ajaxReturn($data);
           }
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    public function accountTransaction()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->transactionGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    public function applyWithdraw()
    {
        $isLogin = session('isLogin');

        if ($isLogin == 'yes') {
            $userList = $this->userInfo;
            $entityId = $userList['id'];
            $availableBalance = Utils::getAgencyAvailableFen($entityId);

            if (IS_AJAX && IS_POST) {
                if ($userList['state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被禁用';
                    $this->ajaxReturn($data);
                }

                if ($userList['account_state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被冻结';
                    $this->ajaxReturn($data);
                }

                $agency['account_balance'] = Utils::getFen(I('account_balance'));
                $where['id'] = $userList['id'];
                $list = M('agency')->where($where)->find();
                $data['entity_type'] = Top::EntityTypeAgency;
                $data['entity_id'] = $userList['id'];
                $data['entity_name'] = $userList['name'];
                $data['full_name'] = $userList['name'];
                $data['operator_id'] = $userList['operator_id'];
                $data['state'] = Top::ApplyStateWait;
                $data['amount'] = I('amount');
                $data['account'] = $userList['account'];
                $data['amount'] = Utils::getFen($data['amount']);
                $data['bank_name'] = $list['bank_name'];
                $data['bank_account'] = $list['bank_account'];
                $data['create_time'] = date('Y-m-d H:i:s', time());

                $amount = intval($data['amount']);

                if ($availableBalance - $amount >= 0) {
                    $postCode = I('code');
                    $code = session('applyWithDraw');

                    if ($postCode == $code) {
                        session('applyWithDraw', null);
                        $result = M('account_withdraw')->add($data);
                    }
                } else {
                    $this->show('提现金额不能大于账户余额');
                }

                if ($result) {
                    $data['status'] = 1;
                    $this->ajaxReturn($data);
                } else {
                    $data['status'] = 0;
                    $this->ajaxReturn($data);
                }
            } else {
                $_SESSION['applyWithDraw'] = 400;
                $userList = $this->userInfo;
                $where['id'] = $userList['id'];
                $list = M('agency')->where($where)->find();

                $list['account_balance'] = Utils::getYuan($list['account_balance']);
                $this->assign('availableBalance', $availableBalance);
                $this->assign('list', $list);
            }
            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    public function withdrawList()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->getPageHtml($page);
            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    public function applyInvoice()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $userList = $this->userInfo;
            $operatorId = $userList['operator_id'];
            $invoiceFeeInfo = M('operator')->where("id = '{$operatorId}'")->field('express_fee')->find();

            $entity_type = Top::EntityTypeAgency;
            $entity_id = $userList['id'];
            $operatorId = $userList['operator_id'];
            $topUpTotal = M('account_transaction')->where("entity_type = '{$entity_type}' && entity_id = '{$entity_id}' && operator_id = '{$operatorId}' && object_type = 1")->field('sum(amount) topUpTotal')->select();

            $invoiceTotal = M('agency_invoice')->where("entity_type = '{$entity_type}' && entity_id = '{$entity_id}' && operator_id = '{$operatorId}' && (state = 2 or state = 1)")->field('sum(amount) invoicetotal')->select();
            $lastTotal = (int)$topUpTotal[0]['topuptotal'] - $invoiceTotal[0]['invoicetotal'];

            if (IS_AJAX && IS_POST) {
                if ($userList['state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被禁用';
                    $this->ajaxReturn($data);
                }

                if ($userList['account_state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被冻结';
                    $this->ajaxReturn($data);
                }

                /*
                $amount = I('amount');

                if ($lastTotal <= 0) {
                    $data['status'] = 0;
                    $data['msg'] = '剩余发票额度不足';
                    $this->ajaxReturn($data);
                }

                if ($amount > $lastTotal) {
                    $data['status'] = 0;
                    $data['msg'] = '发票金额需少于剩余发票额度';
                    $this->ajaxReturn($data);
                }
                */

                $data['state'] = I('request_method');
                $postCode = I('code');
                $code = session('applyInvoice');
                //判断是否是快递 2是 1自取
                if ($data['state'] == '2') {
                    $data['entity_id'] = $userList['id'];
                    $data['operator_id'] = $userList['operator_id'];
                    $data['entity_type'] = Top::EntityTypeAgency;
                    $data['entity_name'] = $userList['name'];
                    $data['title'] = I('title');
                    $data['detail'] = I('detail');
                    $data['remark'] = I('remark');
                    $data['state'] = Top::ApplyStateWait;
                    $data['amount'] = Utils::getFen(I('amount'));

                    $detail = I('detail');
                    $invoiceFeeRateInfo = M('operator_invoice_fee')->where("operator_id = '{$operatorId}' && detail = '{$detail}'")->find();

                    $data['fee_rate'] = $invoiceFeeRateInfo['fee_rate'];
                    $data['fee'] = Utils::CalcCommission($data['fee_rate'], $data['amount']);

                    $data['request_method'] = I('request_method');
                    $data['create_time'] = date('Y-m-d H:i:s', time());
                    $contactInfo['name'] = I('name');
                    $contactInfo['phone'] = I('phone');
                    $contactInfo['addr'] = I('addr');
                    $data['addr'] = implode(",", $contactInfo);

                    $expressFeeValue = I('express_fee');

                    if ($expressFeeValue == 3) {
                        $data['express_fee'] = 0;
                    } else if ($expressFeeValue == 4) {
                        $data['express_fee'] = $invoiceFeeInfo['express_fee'];
                    }

                    if ($data['title'] != '' && $data['amount']) {
                        if ($postCode == $code) {
                            session('applyInvoice', null);
                            $result = M('agency_invoice')->add($data);
                        }
                    } else {
                        $this->show('抬头或金额不能为空');
                    }

                    if ($result) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                } else {
                    $data['entity_id'] = $userList['id'];
                    $data['operator_id'] = $userList['operator_id'];
                    $data['entity_type'] = Top::EntityTypeAgency;
                    $data['entity_name'] = $userList['name'];
                    $data['title'] = I('title');
                    $data['detail'] = I('detail');
                    $data['amount'] = Utils::getFen(I('amount'));

                    $detail = I('detail');
                    $invoiceFeeRateInfo = M('operator_invoice_fee')->where("operator_id = '{$operatorId}' && detail = '{$detail}'")->find();

                    $data['fee_rate'] = $invoiceFeeRateInfo['fee_rate'];
                    $data['fee'] = Utils::CalcCommission($data['fee_rate'], $data['amount']);

                    $data['request_method'] = I('request_method');
                    $data['state'] = Top::ApplyStateWait;
                    $data['create_time'] = date('Y-m-d H:i:s', time());

                    if ($postCode == $code) {
                        session('applyInvoice', null);
                        $result = M('agency_invoice')->add($data);
                    }

                    if ($result) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $_SESSION['applyInvoice'] = 400;
                $operatorId = $userList['operator_id'];
                $invoiceTypeList = M('operator_invoice_fee')->where("operator_id = '{$operatorId}'")->select();
                $this->assign('invoiceTypeList', $invoiceTypeList);
                $this->assign('lastTotal', $lastTotal);
                $this->assign('invoiceFeeInfo', $invoiceFeeInfo);
            }
            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }
    //顾问查看扣除服务费列表
    public function serviceList(){
          $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->ServiceListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }

    public function invoiceList()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->InvoiceListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }

//合同申请页面
    public function applyContract()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $userList = $this->userInfo;
            $contractFeeInfo = M('operator')->where("id = " . $userList['operator_id'])->field("contract_fee,express_fee")->find();

            if (IS_POST && IS_AJAX) {
                if ($userList['state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被禁用';
                    $this->ajaxReturn($data);
                }

                if ($userList['account_state'] != '1') {
                    $data['status'] = 0;
                    $data['msg'] = '账户被冻结';
                    $this->ajaxReturn($data);
                }

                $postCode = I('code');
                $code = session('applyContract');
                $data['state'] = I('request_method');
                //判断是快递还是自取 2快递 1自取
                if ($data['state'] == '2') {
                    $data['entity_id'] = $userList['id'];
                    $data['operator_id'] = $userList['operator_id'];
                    $data['entity_type'] = Top::EntityTypeAgency;
                    $data['inland_count'] = I('inland');
                    $data['outbound_count'] = I('outbound');
                    $data['peritem_count'] = I('peritem');
                    $data['entity_name'] = $userList['name'];
                    $data['request_method'] = I('request_method');
                    $data['state'] = Top::ApplyStateWait;
                    $data['contract_fee'] = $contractFeeInfo['contract_fee'];
                    $data['create_time'] = date('Y-m-d H:i:s', time());
                    $contactInfo['name'] = I('name');
                    $contactInfo['phone'] = I('phone');
                    $contactInfo['addr'] = I('addr');
                    $data['addr'] = implode(",", $contactInfo);

                    $expressFeeValue = I('express_fee');

                    if ($expressFeeValue == 3) {
                        $data['express_fee'] = 0;
                    } else if ($expressFeeValue == 4) {
                        $data['express_fee'] = $contractFeeInfo['express_fee'];
                    }

                    if ($contactInfo['name'] != '' && $contactInfo['phone'] != '' && $contactInfo['addr'] != '') {
                        if ($postCode == $code) {
                            session('applyContract', null);
                            $result = M('agency_contract')->add($data);
                        }
                    }

                    if ($result) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                } else {
                    $data['entity_id'] = $userList['id'];
                    $data['operator_id'] = $userList['operator_id'];
                    $data['entity_type'] = Top::EntityTypeAgency;
                    $data['entity_name'] = $userList['name'];
                    $data['inland_count'] = I('inland');
                    $data['outbound_count'] = I('outbound');
                    $data['contract_fee'] = $contractFeeInfo['contract_fee'];
                    $data['peritem_count'] = I('peritem');
                    $data['request_method'] = I('request_method');
                    $data['state'] = Top::ApplyStateWait;
                    $data['create_time'] = date('Y-m-d H:i:s', time());

                    if ($postCode == $code) {
                        session('applyContract', null);
                        $result = M('agency_contract')->add($data);
                    }

                    if ($result) {
                        $data['status'] = 1;
                        $this->ajaxReturn($data);
                    } else {
                        $data['status'] = 0;
                        $this->ajaxReturn($data);
                    }
                }
            } else {
                $_SESSION['applyContract'] = 400;
                $this->assign('userList', $userList);
                $this->assign('contractFeeInfo', $contractFeeInfo);
            }
            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    public function contractList()
    {
        $isLogin = session('isLogin');
        if ($isLogin == 'yes') {
            $page = I('page');

            if ($page == '') {
                $page = 1;
            }

            $html = $this->contractListGetPageHtml($page);

            if (IS_POST && IS_AJAX) {
                $this->ajaxReturn($html);
            } else {
                $this->assign('html', $html);
            }

            $this->display();
        } else {
            $domain = $_SERVER['SERVER_NAME'];
            $url = "http://" . $domain . "/index.php";
            header("Location:" . $url);
        }
    }


    protected function transactionGetPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;

        $type = Top::EntityTypeAgency;
        $id = $userList['id'];
        $pageSize = 10;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_account_transaction as t')
            ->where("t.entity_id = {$id} && t.entity_type = '{$type}'")
            ->field('t.id,t.create_time,t.action,t.amount,t.balance,t.sn')
            ->order('t.id desc')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $list[$key]['amount'] = Utils::getYuan($vo['amount']);
            $list[$key]['balance'] = Utils::getYuan($vo['balance']);
        }

        foreach ($list as $vo) {

            if ($vo['action'] == '充值') {
                $action = "<i class='icon i-2'></i>";
            } else if ($vo['action'] == '提现') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '支出线路产品订单金额') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '收入线路产品订单金额') {
                $action = "<i class='icon i-2'></i>";
            } else if ($vo['action'] == '支出线路产品订单佣金') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '收入线路产品订单佣金') {
                $action = "<i class='icon i-2'></i>";
            } else if ($vo['action'] == '财务服务费') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '购买合同') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '订单取消:退款') {
                $action = "<i class='icon i-2'></i>";
            } else if ($vo['action'] == '机票退款') {
                $action = "<i class='icon i-2'></i>";
            } else if ($vo['action'] == '支出机票订单金额') {
                $action = "<i class='icon i-1'></i>";
            } else if ($vo['action'] == '支出门票产品金额') {
                $action = "<i class='icon i-1'></i>";
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "" . $action . "";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>" . $vo['action'] . "</p>";
            $evaldata .= "<p class='price'>发生金额：<dfn>&yen;" . $vo['amount'] . "</dfn><span class='line'></p>";
            $evaldata .= "<p class='price'>账户余额：<dfn>&yen;" . $vo['balance'] . "</dfn><span class='line'></p>";
            $evaldata .= "<p class='code'>交易号：" . $vo['sn'] . "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }


    protected function topupListGetPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;

        $type = Top::EntityTypeAgency;
        $id = $userList['id'];
        $pageSize = 5;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_account_topup as t')
            ->where("t.entity_id = {$id} && t.entity_type = '{$type}'")
            ->field('t.id,t.create_time,t.amount,t.state,t.memo,t.payer')
            ->order('t.id desc')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $list[$key]['amount'] = Utils::getYuan($vo['amount']);
            $list[$key]['balance'] = Utils::getYuan($vo['balance']);
        }

        foreach ($list as $key => $vo) {
            $vo['account'] = Utils::getAccount($vo['id']);
            if ($vo['state'] == 1) {
                $state = '未审核';
            } elseif ($vo['state'] == 2) {
                $state = '审核通过';
            } elseif ($vo['state'] == 3) {
                $state = '审核失败 失败理由：';
                $state .= $vo['memo'];
            } elseif ($vo['state'] == 5) {
                $state = '待支付';
            } elseif ($vo['state'] == 6) {
                $state = '已支付';
            }

            if ($vo['payer']) {
                $vo['payer'] = "<p class='price'>汇款方：" . $vo['payer'] . "</p>";
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>充值<i class='icon i-2'></i></p>";
            $evaldata .= "<p class='price'>状态：<dfn>" . $state . "</dfn></p>";
            $evaldata .= "<p class='price'>充值金额：<dfn>&yen;" . $vo['amount'] . "</dfn></p>";
            $evaldata .= $vo['payer'];
            $evaldata .= "<p class='code'>交易号：" . $vo['account'] . "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }
    protected function serviceListGetPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;

        $type = Top::EntityTypeAgency;
        $id = $userList['id'];
        $pageSize = 5;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_service_charge')
            ->where("entity_id = {$id} && entity_type = '{$type}'")
            ->field('id,create_time,service_name,state,service_money')
            ->order('id desc')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $list[$key]['service_money'] = Utils::getYuan($vo['service_money']);
        }
        foreach ($list as $key => $vo) {
        

            if ($vo['state'] == 0) {
                $dl_show = "&nbsp;&nbsp;<p>
						<span  onclick='dl_service_ok(".$vo['id'].")' class='btn btn-success btn-sm'>
							确定
						</span></p>";
            }elseif($vo['state'] == 1){
                $dl_show ="";
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>扣除服务费<i class='icon i-2'></i></p>";
            $evaldata .= "<p class='price'>服务名称：<dfn>" . $vo['service_name'] . "</dfn></p>";
            $evaldata .= "<p class='price'>服务金额：<dfn>&yen;" . $vo['service_money'] . "</dfn></p>";
            $evaldata .= $dl_show;
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }

    protected function getPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;

        $type = Top::EntityTypeAgency;
        $id = $userList['id'];
        $pageSize = 4;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_account_withdraw as w')
            ->where("w.entity_type='{$type}' && w.entity_id='{$id}'")
            ->field(array(
                'w.id' => 'id',
                'w.amount' => 'amount',
                'w.create_time' => 'create_time',
                'w.state' => 'state',
                'w.memo' => 'memo',
            ))
            ->order('w.id desc')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $list[$key]['amount'] = Utils::getYuan($vo['amount']);
            $list[$key]['balance'] = Utils::getYuan($vo['balance']);
        }

        foreach ($list as $vo) {
            $vo['account'] = Utils::getAccount($vo['id']);
            if ($vo['state'] == 1) {
                $state = '未审核';
            } elseif ($vo['state'] == 2) {
                $state = '已确认，待财务处理';
            } elseif ($vo['state'] == 4) {
                $state = '处理完成';
            } elseif ($vo['state'] == 3) {
                $state = '审核失败 失败理由：';
                $state .= $vo['memo'];
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "<i class='icon i-1'></i>";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>提现</p>";
            $evaldata .= "<p class='price'>状态：<dfn>" . $state . "</dfn></p>";
            $evaldata .= "<p class='price'>金额：<dfn>&yen;" . $vo['amount'] . "</dfn><span class='line'></p>";
            $evaldata .= "<p class='code'>交易号：" . $vo['account'] . "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }


    protected function InvoiceListGetPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;
        $where['operator_id'] = $userList['operator_id'];
        $where['entity_id'] = $userList['id'];
        $pageSize = 4;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_agency_invoice')
            ->where($where)
            ->field('id,title,amount,detail,create_time,state,memo')
            ->order('id desc')
            ->limit($index, $pageSize)
            ->select();

        foreach ($list as $key => $vo) {
            $list[$key]['amount'] = Utils::getYuan($vo['amount']);
        }

        foreach ($list as $vo) {
            $vo['account'] = Utils::getAccount($vo['id']);
            if ($vo['state'] == 1) {
                $state = '未审核';
            } elseif ($vo['state'] == 2) {
                $state = '审核通过';
            } elseif ($vo['state'] == 3) {
                $state = '审核失败 失败理由：';
                $state .= $vo['memo'];
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "<i class='icon i-1'></i>";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>发票申请</p>";
            $evaldata .= "<p class='price'>状态：<dfn>" . $state . "</dfn></p>";
            $evaldata .= "<p class='price'>发票抬头：<dfn>" . $vo['title'] . "</dfn>";
            $evaldata .= "<span class='line'>|</span>发票项目：<em>" . $vo['detail'] . "</em></p>";
            $evaldata .= "<p class='price'>发票金额：<dfn>" . $vo['amount'] . "</dfn></p>";
            $evaldata .= "<p class='code'>交易号：" . $vo['account'] . "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }


    protected function contractListGetPageHtml($page)
    {
        $M = M();
        $userList = $this->userInfo;
        $where['operator_id'] = $userList['operator_id'];
        $where['entity_id'] = $userList['id'];
        $pageSize = 4;
        $index = ($page - 1) * $pageSize;

        $list = $M->table('top_agency_contract')
            ->where($where)
            ->field('id,memo,inland_count,outbound_count,peritem_count,create_time,state')
            ->order('id desc')
            ->limit($index, $pageSize)
            ->select();


        foreach ($list as $vo) {
            $vo['account'] = Utils::getAccount($vo['id']);
            if ($vo['state'] == 1) {
                $state = '未审核';
            } elseif ($vo['state'] == 2) {
                $state = '审核通过';
            } elseif ($vo['state'] == 3) {
                $state = '审核失败 失败理由：';
                $state .= $vo['memo'];
            }

            $evaldata .= "<ul class='tradeList'>";
            $evaldata .= "<li>";
            $evaldata .= "<i class='icon i-1'></i>";
            $evaldata .= "<p class='time'>时间：" . $vo['create_time'] . "</p>";
            $evaldata .= "<p class='name'>合同申请</p>";
            $evaldata .= "<p class='price'>状态：<dfn>" . $state . "</dfn></p>";
            $evaldata .= "<p class='price'>国内合同申请份数：<dfn>" . $vo['inland_count'] . "份</dfn>";
            $evaldata .= "<p class='price'>境外合同申请份数：<dfn>" . $vo['outbound_count'] . "份</dfn>";
            $evaldata .= "<p class='price'>单项委托份数：<dfn>" . $vo['peritem_count'] . "份</dfn></p>";
            $evaldata .= "<p class='code'>交易号：" . $vo['account'] . "</p>";
            $evaldata .= "</li>";
            $evaldata .= "</ul>";
        }

        return $evaldata;
    }

}
