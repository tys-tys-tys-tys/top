//区域改变
$(".regionCode").change(function(){
    var code = $(".regionCode").val();
    var url = $("#ticketUrl").val();
    var info = {};
    info.regionCode = code;
    $("#content").empty();
    $(".showCircle").show();
    $(".moreTicketContent").html('');
    
    $.post(url,info,function(data){
        if(data){
           $(".moreTicketContent").html('加载更多');
           $(".showCircle").hide();
           $("#content").empty();
           $("#content").append(data);
            $("img.lazy").lazyload({effect: "fadeIn"});
        }else{
            $(".showCircle").hide();
            $("#content").empty();
            $("#content").html("<h4>暂无可售门票</h4>");
        }
    })
});

//加载
$(".moreTicketContent").click(function () {
    $(".moreTicketContent").text('正在加载...');
    var page = $("#page_nav").val();
    var code = $(".regionCode").val();
    
    $.post("index", {page: page,regionCode:code}, function (data) {
        if (data) {
            $(".moreTicketContent").text('点击查看更多');
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
            $("img.lazy").lazyload({effect: "fadeIn"});
        } else {
            $(".moreTicketContent").text('暂无更多内容');
        }
    });
});
//提交订单 -- 门票
$(".doSubmitTicketOrder").click(function () {
    $(".showButton").hide();
    $(".showCircle").show();
   
    var url = $("#orderDetailUrl").val();
    var code = $("#code").val();
    var id = $("#ticketId").val();
    var keyid = $("#keyid").val();
    
    var info = {};
    info.code = code;
    info.id = id;
    info.keyid = keyid;
    
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $(".showCircle").hide();
            $(".proofAgain").attr("disabled", true);
            $("#myModalmob").modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html('付款成功!');

            $(".showJumpButton").show();
        } else if ($data['status'] == 2) {
            $(".showCircle").hide();
            $(".showTopupButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html('账户余额不足,请充值');
            return false;
        }else if($data['status'] == 6){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html('账户被冻结,请联系客服');
            return false;
        }else if($data['status'] == 0){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }else{
            $(".showCircle").hide();
            $(".showButton").show();
            return false;
        }

    })

});


setTicketTotal();
//成人
$("#ticketNumTotal").blur(function () {
    var ticketNum = $(".ticketNumTotal").val();

    if (ticketNum < 1) {
        $(".ticketNumTotal").val(1);
    }
    setTicketTotal();
})

//数量+ 
$(".ticketAdd").click(function () {
    var ticketNum = $(this).parent().find('input[class*=ticketNumTotal]');
    ticketNum.val(parseInt(ticketNum.val()) + 1);
    setTicketTotal();
});
//数量- 
$(".ticketReduce").click(function () {
    var ticketNum = $(this).parent().find('input[class*=ticketNumTotal]');
    ticketNum.val(parseInt(ticketNum.val()) - 1);
    if (parseInt(ticketNum.val()) < 1) {
        ticketNum.val(1);
    }
    setTicketTotal();
});

function setTicketTotal() {
    var s = 0;
    var adultTotal = $(".ticketNumTotal");
    s += adultTotal.val() * $(".SettlementPrice").val();
    $("#TicketTotal").html(s.toFixed(2));
}
//退票
$(".doRefundApplication").click(function () {
    $(".showRefundButton").hide();
    $(".showRefundCircle").show();

    var url = $("#refundApplication").val();
    var num = $(".num").val();
    var id = $("#id").val();

    var info = {};
    info.num = num;
    info.id = id;

    if (num <= 0) {
        $(".showRefundButton").show();
        $(".showRefundCircle").hide();

        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('数量不能小于等于0');
        return false;
    } else {
        $.post(url, info, function ($data1) {
            if ($data1['status'] == 1) {
                $(".showRefundCircle").hide();
                $(".showGoBack").show();
                $('#myModalmob').modal({backdrop: 'static', keyboard: false});
                $("#myModalmob").modal();
                $(".tipsBox").html($data1['msg']);
            }else{
                $(".showRefundButton").show();
                $(".showRefundCircle").hide();
                $('#myModalmob').modal({backdrop: 'static', keyboard: false});
                $("#myModalmob").modal();
                $(".tipsBox").html($data1['msg']);
            }
        })
    }
});


var CredentialsRequired = $(".CredentialsRequired").val();
if(CredentialsRequired == 'true'){
    $("#TicketTotal").html($(".SettlementPrice").val());
}

var filterNum = 2;
var num = 1;

$(".realNameAdd").click(function () {
    var CertificateType = $(".CertificateType").val();

    //$(this).html(filterNum);

    if (filterNum < 7) {
        var newDiv = document.createElement('div');

        var str = "<div class='div" + filterNum + "'>";
        str += "<p class='t'>游客" + filterNum + "</p>";
        str += "<div class='form-inline preorder'>";
        str += "<div class='form-group'>";
        str += "<div class='input-group'>";
        str += "<div class='input-group-addon pt'>游客姓名</div>";
        str += "<input type='text' id='contacts' class='form-control nb' name='real_name" + filterNum + "' placeholder='请输入游客姓名'/>";
        str += "</div>";
        str += "</div>";
        str += "</div>";

        str += "<div class='form-inline preorder'>";
        str += "<div class='form-group'>";
        str += "<div class='input-group'>";
        str += "<div class='input-group-addon pt'>证件类型</div>";
        str += "<input type='text' id='contacts' class='form-control nb identity_type disabled' name='identity_type" + filterNum + "'/>";
        str += "</div>";
        str += "</div>"
        str += "</div>";

        str += "<div class='form-inline preorder last'>";
        str += "<div class='form-group'>";
        str += "<div class='input-group'>";
        str += "<div class='input-group-addon pt'>证件号码</div>";
        str += "<input type='text' id='remark' class='form-control nb' name='identity_no" + filterNum + "' placeholder='请输入游客的证件号'>";
        str += "</div>";
        str += "</div>";
        str += "</div>";
        //delete
        str += "<div class='row'>";
        str += "<p> </p>";
        str += "<div class='col-xs-11 text-right'>";
        str += "<input type='button' class='btn btn-danger delItem" + filterNum + "' value='删除该乘机人'>";
        str += "</div>";
        str += "</div>";
        //delete
        str += "<p>&nbsp;</p>";
        str += "<input type='hidden' name='ticket_num' value='" + filterNum + "'>"
        str += "<div class='flight-list-border-dashed-b2'></div>";
        str += "<p>&nbsp;</p>";
        str += "</div>";
        //str += "</div>";

        newDiv.innerHTML = str;
        //newDiv.setAttribute("div" + filterNum);
        $(".filterNum").append(newDiv);

        if (CertificateType == '1') {
            $(".identity_type").val('身份证');
        } else if (CertificateType == '2') {
            $(".identity_type").val('导游证');
        } else if (CertificateType == '4') {
            $(".identity_type").val('学生证');
        } else if (CertificateType == '8') {
            $(".identity_type").val('军官证');
        } else if (CertificateType == '16') {
            $(".identity_type").val('老年证');
        } else if (CertificateType == '32') {
            $(".identity_type").val('出生证');
        } else if (CertificateType == '64') {
            $(".identity_type").val('驾驶证');
        } else if (CertificateType == '128') {
            $(".identity_type").val('其他');
        }

        var priPer = $(".SettlementPrice").val();
        var count = $('.ordertotal>span>span').text();
        $('.ordertotal>span>span').text(count * 1 + priPer * 1);

        //移除
        $(".delItem" + filterNum).click(function () {
            filterNum--;
            var count = $('.ordertotal>span>span').text();
            var priPer = $(".SettlementPrice").val();
            $('.ordertotal>span>span').text(count * 1 - priPer * 1);

            $(".div" + filterNum).remove();
        });

    } else {
        $("#myModalmob").modal();
        $filterNum--;
    }


    filterNum++;
    num++;
});

$("form").submit(function (event) {
    if (!$("#contacts").val()) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('取票人姓名不能为空!');
        return false;
    }else if (!$("#phone").val()) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('手机不能为空!');
        return false;
    }else{
        $(".submitTicketOrder").addClass("disabled");
        $(".submitTicketOrder").html('订单提交中...');

    }
});


//订单取消
$(".doSubmitOrderCancel").one("click", function(){
    var url = $("#orderCancel").val();
    var id = $("#ticketId").val();
    var keyid = $("#keyid").val();
    var info = {};
    info.id = id;
    info.keyid = keyid;

    $.post(url, info, function ($data) {
        if($data['status'] == 1){
            $(".proofAgain").attr("disabled", true);
            $("#myModalmob").modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);

            $(".showButton").hide();
            $(".showJumpButton").show();
        }else{
            $(".proofAgain").attr("disabled", true);
            $("#myModalmob").modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);

            $(".doSubmitWarning").click(function(){
                location.reload(true);
            });

            $(".doCloseWarning").click(function(){
                location.reload(true);
            });
        }
    })

});


//门票搜索
$(".scenicNameSearch").click(function(){
    var url = $("#searchUrl").val();
    var code = $(".regionCode").val();
    var name = $(".scenicName").val();
    var info = {};
    info.name = name;
    info.code = code;

    $.post(url, info, function($data){

        if($data){
            $("#content").empty();
            $("#content").append($data);
            $(".moreTicketContent").empty();
            $("img.lazy").lazyload({effect: "fadeIn"});
        }
    })
});
