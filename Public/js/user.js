//新增
function addUser() {
    var url = $('#chka').val();
    clearDivValue();
    showHidenDiv();
    saveUserInfo(url);
}

//新增角色
function addRole(){
    var url = $("#chka").val();
    $("#auth_name").val('');
    showAddHidenDiv();
    saveRoleInfoAdd(url);
}

//编辑
function editUser(tid) {
    clearDivValue();
    var url = $('#chke').val();
    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            showHidenDiv();
            showDivValue(tid, $data['info']);
            $(".user-pass").remove();
            $(".user-confirm-pass").remove();
            saveUserInfo(url,tid);
        } else {
            return false;
        }
    });
}

//公共编辑
function editRole(tid) {
    var url = $('#chke').val();
    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            $(".roleName").html($data['name']);
            showHidenDiv();
            saveRoleInfo(url,tid);
        } else {
            return false;
        }
    });
}

//清空数据
function clearDivValue() {
    $("#modal_name").val('');
    $("#modal_login").val('');
    $("#modal_pwd").val('');
    $("#modal_pwd2").val('');
    $("#mobile").val('');
    $("#modal_role").val(0);
}

//弹出层
function showHidenDiv() {
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

function showAddHidenDiv() {
    $(".editRole").hide();
    $(".addRole").show();
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//显示数据
function showDivValue(tid, $data) {
    $("#modal_name").val($data['name']);
    $("#modal_login").val($data['login_name']);
    $("#modal_role").val($data['type']);
    $("#mobile").val($data['mobile']);
}

//保存
function saveUserInfo(url, tid) {
    $('#buttonSaveUserInfo').click(function () {
        var name = $("#modal_name").val();
        var modal_login = $("#modal_login").val();
        var modal_role = $("#modal_role").val();
        var mobile = $("#mobile").val();

        var info = {};
        info.tid = tid;
        info.name = name;
        info.modal_login = modal_login;
        info.modal_role = modal_role;
        info.mobile = mobile;
        var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

        if(name == ''){
            jalert('请填写真实姓名', function () {
            });
            return false;
        }

        if(modal_login == ''){
            jalert('请填写登录名', function () {
            });
            return false;
        }

        if(!tid){
            var modal_pwd = $("#modal_pwd").val();
            var modal_pwd2 = $("#modal_pwd2").val();
            if(modal_pwd == ''){
                jalert('请填写密码', function () {
                });
                return false;
            }

            if(modal_pwd.length < 6){
                jalert('密码长度不得少于6位', function () {
                });
                return false;
            }

            if(modal_pwd != modal_pwd2){
                jalert('两次密码输入不一致', function () {
                });
                return false;
            }

            info.modal_pwd = modal_pwd;
            info.modal_pwd2 = modal_pwd2;
        }

        if(mobile == ''){
            jalert('请填写手机号', function () {
            });
            return false;
        }

        if(!reg.test(mobile)){
            jalert('请填写11位手机号', function () {
            });
            return false;
        }

        if (modal_role == 0) {
            jalert('请选择所属角色', function () {
            });
            return false;
        }

        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $('.theme-popover-mask').fadeOut(100);
                $('.theme-popover').slideUp(200);
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {});
                return false;
            }
        });
    });
}

//保存
function saveRoleInfoAdd(url, tid) {
    $('#buttonSaveRoleInfo').click(function () {
        var name = $(".auth_name_add").val();

        var info = {};
        info.tid = tid;
        info.name = name;

        if (name == '') {
            jalert('请将信息填写完整', function () {
            });
            return false;
        } else {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    $('.theme-popover-mask').fadeOut(100);
                    $('.theme-popover').slideUp(200);
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {}); 
                    return false;
                }
            });
        }
    });
}


//保存
function saveRoleInfo(url, tid) {
    $('#buttonSaveRoleInfo').click(function () {
        var name = $(".auth_name").val();

        var info = {};
        info.tid = tid;
        info.name = name;

        if (name == '') {
            jalert('请将信息填写完整', function () {
            });
            return false;
        } else {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    $('.theme-popover-mask').fadeOut(100);
                    $('.theme-popover').slideUp(200);
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {});
                    return false;
                }
            });
        }
    });
}



function providerAppointType(tid){
    var url = $("#getUserName").val();
    var info = {};
    info.id = tid;
    $.post(url, info, function($data){
        if($data['status'] == 1){
            $(".appointName").html($data['name']);

            if($data['productMessage'] == 1){
                $(".productMessage").attr("checked", true);
            }

            if($data['transactionMessage'] == 1){
                $(".transactionMessage").attr("checked", true);
            }

            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".saveNotifyType").one("click", function () {
                saveProductMessageType(tid);
            });
        }else{
            return false;
        }
    })
}

function saveProductMessageType(tid){
    var url = $("#saveNotifyType").val();
    var info = {};
    info.id = tid;
    if($(".productMessage").is(":checked")){
        info.productMessage = 1;
    }else{
        info.productMessage = 0;
    }

    if($(".transactionMessage").is(":checked")){
        info.transactionMessage = 1;
    }else{
        info.transactionMessage = 0;
    }

    $.post(url, info, function($data){
        if($data['status'] == 1){
            jalert($data['msg'], function () {
                location.reload();
            });
        }else{
            jalert($data['msg'], function () {
            });
            return false;
        }
    })
}