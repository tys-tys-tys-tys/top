//退出登录
function loginOut() {
    var url = $("#loginOut").val();
    jconfirm('确定要退出吗', function () {
        window.location.href = url;
    });
}

$(".showqrc").click(function () {
    var id = $(this).attr('value');
    var src = "http://zhangda.m.lydlr.com/index.php/Home/Agency/showQRCode?id=" + id;
    $("#myModalSuc").modal();
    $(".suc").html("<img src='" + src + "' style='height:250px'/>");

});

//线路 - 订单取消
function orderCancel(tid, state, info) {
    jconfirm(info, function () {
        var url = $("#orderCancel").val();
        var info = {};
        info.id = tid;
        info.state = state;
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                });
                return false;
            }
        })
    });
}

//线路 - 订单取消
function orderCancelFinance(tid, state, info) {
    jconfirm(info, function () {
        var url = $("#orderCancelFinance").val();
        var info = {};
        info.id = tid;
        info.state = state;
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                });
                return false;
            }
        })
    });
}

//提示
function friendRemind(tid) {
    jconfirm('只能修改本人创建的线路', function () {
    });
    return false;
}


//提现申请
function withdraw(tid, account_balance, operator_id) {
    //var reg = /^[0-9]+(.[0-9]{1,2})?$/;
    var balance = account_balance;
    var withdraw = $('.withdraw' + tid).val() * 100;
    var bankName = $(".bankNameInfo").html();
    var bankAccount = $(".bankAccountInfo").html();
    var code = $("#code").val();

    if (bankName == '' || bankAccount == '') {
        jalert('请将提现账号信息填写完整', function () {
        });
        return false;
    }

    if (withdraw != '' && withdraw != null) {
        if (!$(".withdraw" + tid).val().match(/^[0-9]+(.[0-9]{1,2})?$/)) {
            jalert('只能输入整数或者小数', function () {
            });
            return false;
        }
    }

    if (withdraw <= 0) {
        jalert('金额必须大于0', function () {
        });
        return false;
    }


    $.post('about', {amount: withdraw, operator_id: operator_id, code: code}, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                $('.withdraw' + tid).val('');
                location.reload();
            });
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
}


function orderChange(tid, state) {
    var adjust = $("#adjust" + tid).val();
    var amount = $("#amount" + tid).val();
    $("#editPrice").html();
    showDiv(tid, '确认订单');
    $('#button' + tid).click(function () {
        var editamount = trim($('#amount' + tid).val());//预付金额
        var editadjust = trim($('#adjust' + tid).val());//价格调整
        var edit_price = $('#edit_price' + tid).html();//调后价格
        var edittotal = $('#total' + tid).html();//订单金额
        var memo = $('#memo' + tid).children('div').children('textarea').val();//备注
        var reg = /^((-)?\d+(\.\d+)?)?$/;
        var reg1 = /^[0-9]+([.]{1}[0-9]+){0,1}$/;
        var info = {};
        info.id = tid;
        info.editstate = state;
        info.editadjust = editadjust;
        info.editamount = editamount;
        info.edit_price = edit_price;
        info.memo = memo;
        if (editadjust != '' || editadjust != 0 || editadjust != null) {
            if (!reg.test(editadjust)) {
                return false;
            }

            if (editamount < 0) {
                $('#prepay' + tid).children('span').html('只能输入整数或者小数');
                $('#prepay' + tid).children('span').css('color', 'red');
                return false;
            } else if (parseInt(editamount) > parseInt(edit_price)) {
                jalert('调后价格不能小于预付金额', function () {
                });
                return false;
            } else {
                $('#prepay' + tid).children('span').html('金额输入正确');
                $('#prepay' + tid).children('span').css('color', 'blue');
            }
        }
        $.post('about', info, function ($data) {
            if ($data['status'] == 1) {
                location.reload();
            } else {
                jalert($data['msg'], function () {
                });
                return false;
            }
        });
    });
}

//确认订单
function editprice(tid) {
    var editadjust = $('#adjust' + tid).val();//价格调整
    var editadjust = toDecimal2(editadjust);
    var edittotal = $('#total' + tid).html();//订单金额
    var reg = /^((-)?\d+(\.\d+)?)?$/;
    if (trim(editadjust) == 0 || trim(editadjust) == null || trim(editadjust) == '') {
        $('#edit_price' + tid).html(edittotal);
    } else {
        if (trim(editadjust) != '' || trim(editadjust) != 0 || trim(editadjust) != null) {
            if (!reg.test(editadjust)) {
                $('#editp' + tid).children('span').html('输入金额不正确');
                $('#editp' + tid).children('span').css('color', 'red');
                return false;
            } else {
                if (trim(editadjust) > 0 && parseInt(trim(editadjust)) > parseInt(trim(editadjust))) {
                    $('#editp' + tid).children('span').html('输入金额大于账户可用余额');
                    $('#editp' + tid).children('span').css('color', 'red');
                    return false;
                } else if (trim(editadjust) < 0 && (parseInt(-trim(editadjust)) > parseInt(trim(edittotal)))) {
                    $('#editp' + tid).children('span').html('输入金额大于账户可用余额');
                    $('#editp' + tid).children('span').css('color', 'red');
                    return false;
                } else {
                    $('#editp' + tid).children('span').html('金额输入正确');
                    $('#editp' + tid).children('span').css('color', 'blue');
                }
            }
        }
    }
    $.post('', {adjust: editadjust, total: edittotal}, function ($data) {
        if ($data['status'] == 1) {
            $('#edit_price' + tid).html($data['price']);
        } else {
            return false;
        }
    });

}

function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x * 100) / 100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

//以下是公共部分
/*公共改变状态*/
function changeState(tid, state) {
    if (state && tid) {
        var reason = '';
        var url = $("#chkState").val();

        jconfirm('您确定要同意吗', function () {
            $.post(url, {id: tid, state: state, memo: reason}, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    } else {
        return false;
    }
}

function changeAccountState(tid, state) {
    if (state && tid) {
        $('#myModalNotice').modal({backdrop: 'static', keyboard: false});
        $("#myModalNotice").modal();
        var url = $("#chkState").val();

        $(".doSubmit").click(function () {
            var reason = $(".reason").val();

            if (reason == '') {
                $("#myModalNotice").modal('hide');
                jalert('请填写理由', function () {
                    location.reload();
                });
                return false;
            }

            $(".doSubmit").addClass("disabled");
            $.post(url, {id: tid, state: state, memo: reason}, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    });
                } else {
                    $("#myModalNotice").modal('hide');
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });

        });
    } else {
        return false;
    }
}


function changeWithDrawState(tid, keyid, state, provider_state, account_state) {
    if (provider_state != '1') {
        jalert('供应商账户被禁用', function () {
        });
        return false;
    }

    if (account_state != '1') {
        jalert('供应商账户被冻结', function () {
        });
        return false;
    }

    if (state && tid) {
        var reason = '';
        var url = $("#chkState").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = state;
        info.memo = reason;

        jconfirm('您确定要同意吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    } else {
        return false;
    }
}


function changeProviderWithdrawState(tid, keyid, state, provider_state, account_state) {
    if (provider_state != '1') {
        jalert('供应商账户被禁用', function () {
        });
        return false;
    }

    if (account_state != '1') {
        jalert('供应商账户被冻结', function () {
        });
        return false;
    }

    jconfirm('您确定要同意吗', function () {
        $(".proof_type").val('');
        $(".proof_code").val('');
        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
        $("#myModalEdit").modal();

        $(".doSubmitTopup").one("click", function () {
            var url = $("#chkStatePass").val();
            var code = $("#code").val();
            var reason = '';
            var proof_type = $(".proof_type").val();
            var proof_code = $(".proof_code").val();

            if (proof_type == '' || proof_code == '') {
                $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                $("#myModalEdit").modal('hide');
                jalert('请填写凭证信息', function () {
                    location.reload();
                });
                return false;
            }

            var info = {};
            info.id = tid;
            info.keyid = keyid;
            info.state = state;
            info.memo = reason;
            info.code = code;
            info.proof_type = proof_type;
            info.proof_code = proof_code;


            if (state && tid) {
                $.post(url, info, function ($data) {
                    if ($data['status'] == 1) {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert('操作成功', function () {
                            location.reload();
                        });
                    } else {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert($data['msg'], function () {
                            location.reload();
                        });
                        return false;
                    }
                }, "json").error(function (xmlHttpRequest) {
                    jalert("服务器错误:" + xmlHttpRequest.status);
                });

            } else {
                return false;
            }
        });
    });
}

//审核产品
function changeAuditState(tid, state) {
    if (state && tid) {
        var reason = '';
        var url = $("#chkState").val();
        jconfirm('您确定要同意吗', function () {
            $.post(url, {id: tid, state: state, memo: reason}, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    });
                } else {
                    jalert('操作成功', function () {
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    } else {
        return false;
    }
}

//跑马灯设置
//function marquee(tid, keyid) {
//    if (tid) {
//        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
//        $("#myModalSuc").modal();
//        var url = $("#marqueeInfo").val();
//        var info = {};
//        info.id = tid;
//
//        $.post(url, info, function ($data) {
//            if ($data['status'] == 1) {
//                $(".marquee_start_time").val($data['marquee_start_time']);
//                $(".marquee_end_time").val($data['marquee_end_time']);
//                var marquee = $data['is_marquee'];
//
//                if (marquee == '1') {
//                    $(".is_marquee").attr("checked", true);
//                } else if (marquee == '0') {
//                    $(".is_marquee").attr("checked", false);
//                }
//
//                $(".doSubmit").one("click", function () {
//                    submitMarqueeInfo(tid, keyid);
//                });
//            } else {
//                return false;
//            }
//        })
//    }
//}


function submitMarqueeInfo(tid, keyid) {
    var url = $("#marquee").val();
    var marquee_start_time = $(".marquee_start_time").val();
    var marquee_end_time = $(".marquee_end_time").val();
    var is_marquee = $(".is_marquee").prop('checked');
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.marquee_start_time = marquee_start_time;
    info.marquee_end_time = marquee_end_time;
    info.is_marquee = is_marquee;

    if (marquee_start_time == '') {
        jalert('请选择开始时间', function () {
        });
        return false;
    }

    if (marquee_end_time == '') {
        jalert('请选择结束时间', function () {
        });
        return false;
    }

    if (info.is_marquee == true) {
        info.is_marquee = 1;
    } else if (info.is_marquee == false) {
        info.is_marquee = 0;
    }

    jconfirm('您确定要同意吗', function () {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $("#myModalSuc").modal('hide');
                jalert('操作成功', function () {
                    location.reload();
                });
            } else {
                $("#myModalSuc").modal('hide');
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        })
    })
}

//置顶
function changeTop(tid, keyid) {
    if (tid) {
        var url = $("#topRecommended").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;

        jconfirm('您确定要同意吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    })
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            })
        })
    }
}


//特价产品
function changePromotion(tid, keyid) {
    if (tid) {
        var url = $("#promotion").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;

        jconfirm('您确定要同意吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    })
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            })
        })
    }
}

//外部订单设置
function changeProviderType(tid, keyid) {
    if (tid) {
        var url = $("#outOrder").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;

        jconfirm('您确定要同意吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    })
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            })
        })
    }
}

//审核产品（ 改变背景 ）
$(function () {
    $("a.navi-audit-state").removeClass("active");
    var state = $("#state").val();
    if (state == "")
        state = 0;

    $("a.navi-audit-state" + state).addClass("active");
})

//提现申请（ 改变背景 ）
$(function () {
    $("a.nav-audit-state").removeClass("active");
    var state = $("#status").val();
    if (state == "")
        state = 0;

    $("a.nav-audit-state" + state).addClass("active");
});


$(function () {
    $("a.nav-operator-agency-state").removeClass("active");
    var state = $('.operatorId').val();

    if (state == "") {
        state = 0;
    }

    $("a.nav-operator-agency-state" + state).addClass("active");


    var agencyTotalNum = '(' + $(".agencyTotalNum").val() + ')';
    $("a.nav-operator-agency-state" + state).append(agencyTotalNum);
});

$(function () {
    $("a.nav-operator-state").removeClass("active");
    var state = $('.operatorId').val();

    if (state == "") {
        state = 0;
    }

    $("a.nav-operator-state" + state).addClass("active");
});

//报表
$(function () {
    $("a.nav-operator-report-state").removeClass("active");
    var state = $('.operatorId').val();

    if (state == "" || state == 0) {
        state = 1;
    }

    $("a.nav-operator-report-state" + state).addClass("active");
});


//公共拒绝
function reject(tid, state) {
    $('#modal_reject').children('textarea').val('');
    showHidenDiv();

    $('#button').click(function () {
        var reason = $('#modal_reject').children('textarea').val();

        if (reason == '') {
            jalert('请填写拒绝理由', function () {
            });
            return false;
        }

        $('#button').attr('disabled', true);
        $.post('about', {id: tid, state: state, memo: reason}, function ($data) {
            if ($data['status'] == 1) {
                jalert('操作成功', function () {
                    location.reload(true);
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload(true);
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });

}

function providerWithDrawReject(tid, keyid, state) {
    $('#modal_reject').children('textarea').val('');
    showHidenDiv();
    $('#button').one("click", function () {
        var url = $("#chkState").val();
        var reason = $('#modal_reject').children('textarea').val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = state;
        info.memo = reason;

        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert('操作成功', function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });

}


//全选、反选
function selectAll(box) {
    if (box.checked == true) {
        $(".checkitem").each(function () {
            this.checked = true;
        });
    } else {
        $(".checkitem").each(function () {
            this.checked = false;
        });
    }
}

//所有删除功能集合
function jqchk(tid) {
    var chk_value = [];
    var url = $("#chkr").val();
    if (tid || tid != undefined) {
        chk_value.push(tid);
    } else {
        $('input[name="subBox"]:checked').each(function () {
            chk_value.push($(this).val());
        });
    }
    if (chk_value.length == 0) {
        jalert('你还没有选择任何内容！', function () {
        });
        return false;
    } else {
        jconfirm('确定要删除这些数据吗？', function () {
            $.post(url, {action_code: chk_value}, function ($data) {
                if ($data['status'] == 1) {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    }

}

function optertorDelete() {
    jalert('运营商暂不支持删除！', function () {
    });
}

//分配权限
function saveAuth() {
    var chk_value = [];
    var url = $("#chkr").val();
    $('input[name="subBox"]:checked').each(function () {
        chk_value.push($(this).val());
    });
    if (chk_value.length == 0) {
        jalert('你还没有选择任何内容！', function () {
        });
        return false;
    } else {
        $.post(url, {action_code: chk_value}, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });

    }
}

function trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//交通方法单选框
$("input.traffic").change(function () {
    if ($(this).is(":checked")) {
        $("input.traffic").prop("checked", false);
        $(this).prop("checked", true);
    }
});

//确认订单时
function showDiv(tid, msg) {
    $('#myModalLabel').html(msg);
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('#showMSG' + tid).slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        $('#showMSG' + tid).slideUp(200);
    });
    $('.theme-poptit #reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        $('#showMSG' + tid).slideUp(200);
    });
}

//弹出层
function showHidenDiv(msg) {
    $('#myModalLabel').html(msg);
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//更新编码
function addProviderCode(tid) {
    var url = $("#providerCodeEditUrl").val();

    $.post(url, {id: tid}, function (data) {
        if (data['status'] == 1) {
            $('#myModalAdd').modal();
            $('#myModalAdd').modal({backdrop: 'static', keyboard: false});
            $("#providerCode").val(data['code']);
            $("#saveProviderCode").one("click", function () {
                saveProviderCode(tid);
            });
        } else {
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}


function saveProviderCode(tid) {
    var url = $("#providerCodeUrl").val();
    var code = $("#providerCode").val();

    if (tid) {
        $.post(url, {id: tid, code: code}, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalAdd').modal({backdrop: 'static', keyboard: false});
                $('#myModalAdd').modal('hide');
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                $('#myModalAdd').modal({backdrop: 'static', keyboard: false});
                $('#myModalAdd').modal('hide');
                jalert($data['msg'], function () {
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });

    } else {
        return false;
    }
}

//清空数据
function clearDivValue() {
}


