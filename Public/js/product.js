/**
 * 编辑&新增
 */
$("#saveProductEdit").click(function(){
	var waitForUploadPicCount = $("div.uploader-wait").length;
    if (waitForUploadPicCount > 0) {
        if (!confirm("您有尚未上传的产品图片，确认放弃上传么？")) {
            return;
        }
    }

    var url = $("#editProduct").val();
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var name = $("#name").val();
    var price_cost = $("#price_cost").val();
    var price = $("#price").val();
    //var para_type = $("#para_type").val();
    //var para_value = $("#para_value").val();
    var pics = getPics();
    var cover_pic = $("#cover-pic").val();
    var stock = $("#stock").val();
    var description = [];
    description.push(UE.getEditor('editor1').getContent());
    description.join("\n");
    var info = {};

    if(id != ''){
        info.id = id;
        info.keyid = keyid;
    }

    info.name = name;
    info.description = description;
    info.price_cost = price_cost;
    info.price = price;
    //info.para_type = para_type;
    //info.para_value = para_value;
    info.pics = pics;
    info.cover_pic = cover_pic;
    info.stock = stock;

    if(name == ''){
        jalert('请输入产品名称', function () {
        });
        return false;
    }

    if(description == ''){
        jalert('请输入产品描述', function () {
        });
        return false;
    }

    if(price == ''){
        jalert('请输入产品价格', function () {
        });
        return false;
    }


    if(parseInt(price) <= 0){
        jalert('价格输入错误', function () {
        });
        return false;
    }

    $("#saveProductEdit").attr('disabled', true);
    $.post(url, info, function($data){
          if ($data['status'] == 1){
                jalert($data['msg'], function () {
                    window.location.href = $data['url'];
                });
          } else{
                jalert($data['msg'], function () {
                    window.location.href = $data['url'];
                });
          return false;
          }
    });
});

function getPics() {
    var pics = "";

    var i = 0;
    $("div.upload-item").each(function () {
        i++;

        var path = $(this).attr("path");
        var isCover = $(this).attr("isCover");
        var description = $(this).find("textarea").val();

        if ($("#cover-pic").val() == '') {
            $("#cover-pic").val(path);
        }

        var s = "";
        if (path != "") {
            s += path + "|";
            s += isCover + "|";
            s += description;
            //+"|";
            s += "@@@";
        }
        pics = pics + s;
    });

    return pics;
}

$("body").on("click", "a.setCover", function () {
    var path = $(this).closest(".upload-item").attr("path");
    if (path == "") {
        jalert("该文件暂未上传", function () {
        });
        return;
    }

    $("#cover-pic").val(path);
    $("img.img-cover").removeClass("img-cover");
    $(this).closest(".upload-item").find("img.img-preview").addClass("img-cover");
});
    
    

    







