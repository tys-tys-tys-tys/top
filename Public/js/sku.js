//新增位控
function addSku(header, tid, keyid, header, show) {
    var url = $('#chka').val();
    clearDivValue();
    showHidenDiv(header);
    if (show == 'showdates') {
        var s = showdates();
    }
    addSkuInfo(tid, keyid, url, '');
}

//位控编辑
function editSku(tid, keyid, header, show) {
    clearDivValue();
    var url = $('#chke').val();
     var state = "edit";
    $.post(url, {id: tid}, function ($data) {

        if ($data['status'] == 1) {
            $("#tid").val(tid);
            showHidenDiv(header);
            if (show == 'showdateOne') {
                timesOne();
            }
            showDivValue(tid, $data['info']);
            addSkuInfo(tid, keyid, url, state);
        } else {
            return false;
        }
    });
}

//清空数据
function clearDivValue() {
    $("#timeend").val('');
    $('#aagency').val('');
    $('#alist').val('');
    $('#cagency').val('');
    $('#clist').val('');
    $('#hagency').val(0);
    $('#hlist').val(0);
    $('#num').val(0);
    $('#sold').val('');
    $('#days').val('');
    $('#desc').val('');
    $("#timestartOne").attr('');
    $("#timestartOne").val('');
}

//弹出层
function showHidenDiv(msg) {
    $('#myModalLabel').html(msg);
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//显示数据
function showDivValue(tid, $data) { 
    $("#timestartOne").attr('placeholder', $data['start']);
    $("#timestartOne").val($data['start']);
    $('#aagency').val($data['aagency']);
    $('#alist').val($data['alist']);
    $('#cagency').val($data['cagency']);
    $('#clist').val($data['clist']);
    $('#hagency').val($data['hagency']);
    $('#hlist').val($data['hlist']);
    $('#num').val($data['num']);
    $('#sold').val($data['sold']);
    $('#days').val($data['days']);
    $('#desc').val($data['desc']);
}

//时间多选
function showdates() {
    $('.dateCoenter').show();
    $('.dateCoenterOne').hide();
    var uzSFC = new HLTSelectFullCalendarContext({
        id: '#timeDateShow',
        callback: function ($data) {
            $("#dataValues").val($data);
        }
    });
    $("#timeDateShow").fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        }
        , height: 100
        , selectable: true
        , editable: false
        , select: function (start, end, allDay) {
            uzSFC.selectDates(start, end);
        }
        , events: function (start, end, callback) {
            uzSFC.initContainer(start, end);
        }
    });
}

//时间单选
function timesOne() {
    $('.dateCoenterOne').show();
    $('.dateCoenter').hide();
}

//验证及传值到后台处理
function addSkuInfo(tid, keyid, url, state) {
    $('#button').click(function () {
        //位控
        var times = $("#dataValues").val();//多个时间
        var timestartOne = $("#timestartOne").val();
        var aagency = $('#aagency').val();
        var alist = $('#alist').val();
        var cagency = $('#cagency').val();
        var clist = $('#clist').val();
        var hagency = $('#hagency').val();
        var hlist = $('#hlist').val();
        var num = $('#num').val();
        var sold = $('#sold').val();
        var days = $('#days').val();
        if (days) {
            var days = $('#days').val();
        } else {
            var days = '1';
        }
        var desc = $('#desc').val();
       
        
        var reg = /^[0-9]*[1-9][0-9]*$/;
        var reg2 = /^(0|\+?[1-9][0-9]*)$/;
        var info = {};

        info.tid = tid;
        info.keyid = keyid;
        info.times = times;
        info.timestartOne = timestartOne;
        info.skuState = state;
        info.aagency = aagency;
        info.alist = alist;
        info.cagency = cagency;
        info.clist = clist;
        info.hagency = hagency;
        info.hlist = hlist;
        info.num = num;
        info.sold = sold;
        info.days = days;
        info.desc = desc;
        info.day = tid;
        
        //位控验证
        if (aagency != undefined || alist != undefined
                || cagency != undefined || clist != undefined || hagency != undefined || hlist != undefined
                || num != undefined || sold != undefined) {
            if (aagency == '' || aagency == null) {
                $('#aagency').attr('placeholder', '请输入金额');
                $('#aagency').css('font-size', '10px');
                $('#aagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(aagency)) {
                $('#aagency').val('');
                $('#aagency').attr('placeholder', '请输入整数');
                $('#aagency').css('font-size', '10px');
                $('#aagency').css('border', '1px solid red');
                return false;
            } else {
                $('#aagency').css('border', '1px solid #5bc0de');
            }
            if (alist == '' || alist == null) {
                $('#alist').attr('placeholder', '请输入金额');
                $('#alist').css('font-size', '10px');
                $('#alist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(alist)) {
                $('#alist').val('');
                $('#alist').attr('placeholder', '请输入整数');
                $('#alist').css('font-size', '10px');
                $('#alist').css('border', '1px solid red');
                return false;
            } else {
                $('#alist').css('border', '1px solid #5bc0de');
            }

            if (cagency == '' || cagency == null) {
                $('#cagency').attr('placeholder', '请输入金额');
                $('#cagency').css('font-size', '10px');
                $('#cagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(cagency)) {
                $('#cagency').val('');
                $('#cagency').attr('placeholder', '请输入整数');
                $('#cagency').css('font-size', '10px');
                $('#cagency').css('border', '1px solid red');
                return false;
            } else {
                $('#cagency').css('border', '1px solid #5bc0de');
            }
            if (clist == '' || clist == null) {
                $('#clist').attr('placeholder', '请输入金额');
                $('#clist').css('font-size', '10px');
                $('#clist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(clist)) {
                $('#clist').val('');
                $('#clist').attr('placeholder', '请输入整数');
                $('#clist').css('font-size', '10px');
                $('#clist').css('border', '1px solid red');
                return false;
            } else {
                $('#clist').css('border', '1px solid #5bc0de');
            }

            if (hagency == '' || hagency == null) {
                $('#hagency').attr('placeholder', '请输入金额');
                $('#hagency').css('font-size', '10px');
                $('#hagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(hagency)) {
                $('#hagency').val('');
                $('#hagency').attr('placeholder', '请输入整数');
                $('#hagency').css('font-size', '10px');
                $('#hagency').css('border', '1px solid red');
                return false;
            } else {
                $('#hagency').css('border', '1px solid #5bc0de');
            }
            if (hlist == '' || hlist == null) {
                $('#hlist').attr('placeholder', '请输入金额');
                $('#hlist').css('font-size', '10px');
                $('#hlist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(hlist)) {
                $('#hlist').val('');
                $('#hlist').attr('placeholder', '请输入整数');
                $('#hlist').css('font-size', '10px');
                $('#hlist').css('border', '1px solid red');
                return false;
            } else {
                $('#hlist').css('border', '1px solid #5bc0de');
            }
            if (num == '' || num == null) {
                $('#num').attr('placeholder', '不能为空');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else if (num == 0) {
                $('#num').val('');
                $('#num').attr('placeholder', '必须大于1');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else if (!reg.test(num)) {
                $('#num').attr('placeholder', '输入有误');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else {
                $('#num').css('border', '1px solid #5bc0de');
            }
            if (sold != 0) {
                if (!reg.test(sold)) {
                    $('#sold').attr('placeholder', '输入有误');
                    $('#sold').css('font-size', '10px');
                    $('#sold').css('border', '1px solid red');
                    return false;
                } else {
                    $('#sold').css('border', '1px solid #5bc0de');
                }

            }
            if (!reg.test(days)) {
                $('#days').attr('placeholder', '输入有误');
                $('#days').css('font-size', '10px');
                $('#days').css('border', '1px solid red');
                return false;
            } else {
                $('#days').css('border', '1px solid #5bc0de');
            }
        }
        if (typeof (timestart) != "undefined" || typeof (timeend) != "undefined") {
            if (timestart == null || timeend == null) {
                $("#modal_times").children('span').html('*此项不能为空');
                $("#modal_times").children('span').css('color', 'red');
                return false;
            } else {
                $("#modal_times").children('span').html('OK');
                $("#modal_times").children('span').css('color', 'blue');
            }
        }
        if (typeof (times) != "undefined") {
            if (times == "" && !timestartOne) {
                $(".timeDateShowsgs").children('span').html('*此项不能为空');
                $(".timeDateShowsgs").children('span').css('color', 'red');
                return false;
            } else {
                $(".timeDateShowsgs").children('span').html('OK');
                $(".timeDateShowsgs").children('span').css('color', 'blue');
            }
        }

        $('#button').attr('disabled', true);
        $.post(url, info, function ($data) {

            if ($data['status'] == 1) {
                $('.theme-popover-mask').fadeOut(100);
                $('.theme-popover').slideUp(200);
                jalert($data['msg'], function () {
                    location.reload();
                });

            } else {
                $('.theme-popover-mask').fadeOut(100);
                $('.theme-popover').slideUp(200);
                jalert($data['msg'], function () {
                    location.reload();
                    return false;
                });
            }
        });

    });
}

