//出境
$("#line_type3").on("change",function(){
    if($(this).is(":checked")){
        $(".traffic_go").prop("checked",false);
        $(".traffic_back").prop("checked",false);
        $("input.plane").prop("checked",true);
    }    
});

//自由
$("#line_type1").on("change",function(){
    $(".traffic_go").prop("checked",false);
    $(".traffic_back").prop("checked",false);
});

//国内
$("#line_type2").on("change",function(){
    $(".traffic_go").prop("checked",false);
    $(".traffic_back").prop("checked",false);
});

$(".traffic_go").on("change",function(){
    if($(this).is(":checked")){
         $(".traffic_go").prop("checked",false);
         $(this).prop("checked",true);
    }
});

$(".traffic_back").on("change",function(){
    if($(this).is(":checked")){
         $(".traffic_back").prop("checked",false);
         $(this).prop("checked",true);
    }
});

function commonBlur(v, msg) {
    if (v === 'linename') {
        var vars = trim($("#" + v).val()).length / 1024;
        var length = Math.round(vars * Math.pow(10, 4));
    } else {
        var vars = trim($("#" + v).val());
    }
    if (vars == '' || vars == undefined || vars == null) {
        $("#" + msg).html('此项不能为空');
        $("#" + msg).css('color', '#ea6644');
        return false;
    } else {
        if (v === 'linename') {
            if (length > 391) {
                $("#" + msg).html('不能超过40个汉字或者字符');
                $("#" + msg).css('color', '#ea6644');
                return false;
            } else {
                $("#" + msg).html('OK');
                $("#" + msg).css('color', '#428bca');
            }
        } else {
            if (v === 'contact_phone') {
                var patrn = /^((\+?86)|(\(\+86\)))?\d{3,4}-\d{7,8}(-\d{3,4})?$|^((\+?86)|(\(\+86\)))?1\d{10}$/;
                if (!patrn.test(vars)) {
                    $("#" + msg).html('联系电话输入有误');
                    $("#" + msg).css('color', '#ea6644');
                    return false;
                } else {
                    $("#" + msg).html('OK');
                    $("#" + msg).css('color', '#428bca');
                }
            } else {
                $("#" + msg).html('OK');
                $("#" + msg).css('color', '#428bca');
            }

        }
    }
}

function getPics() {
    var pics = "";

    var i = 0;
    $("div.upload-item").each(function () {
        i++;

        var path = $(this).attr("path");
        var isCover = $(this).attr("isCover");
        var description = $(this).find("textarea").val();

        if ($("#cover-pic").val() == '') {
            $("#cover-pic").val(path);
        }

        var s = "";
        if (path != "") {
            s += path + "|";
            s += isCover + "|";
            s += description;
            +"|";
            s += "@@@";
        }
        pics = pics + s;
    });

    return pics;
}

$("body").on("click", "a.setCover", function () {
    var path = $(this).closest(".upload-item").attr("path");
    if (path == "") {
        jalert("该文件暂未上传", function () {
        });
        return;
    }

    $("#cover-pic").val(path);
    $("img.img-cover").removeClass("img-cover");
    $(this).closest(".upload-item").find("img.img-preview").addClass("img-cover");
});

function addExtend(tid, keyid) {
    var url = $('#addExtend').val();
    commextend(tid, keyid, url);
}
function editExtend(tid, keyid) {
    var url = $('#editExtend').val();
    commextend(tid, keyid, url);
}
function commextend(tid, keyid, url) {
    var arr1 = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];
    arr1.push(UE.getEditor('editor1').getContent());
    arr1.join("\n");
    arr2.push(UE.getEditor('editor2').getContent());
    arr2.join("\n");
    arr3.push(UE.getEditor('editor3').getContent());
    arr3.join("\n");
    arr4.push(UE.getEditor('editor4').getContent());
    arr4.join("\n");
    var info = {};
    info.tid = tid;
    info.keyid = keyid;
    info.description = arr1;
    info.notice = arr2;
    info.fee_include = arr3;
    info.fee_exclude = arr4;

    $(".editExtend").attr('disabled', true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                window.location.href = $data['url'];
            });
        } else {
            jalert($data['msg'], function () {
                window.location.href = $data['url'];
            });
            return false;
        }
    });
}

function departure() {
    var departure = $("#departure").val();
    $.post('about', {city: departure}, function (data)
    {
        $("#departure1").empty();
        $("#departure1").append("<option value=''>请选择</option>");
        for (var i = 0; i < data.length; i++)
        {
            $("#departure1").append("<option value='" + data[i]['name'] + "'>" + data[i]['name'] + "</option>");
        }
    });
    if (departure == 0) {
        $("#csnyTip").html('请选择出发的地');
        $("#csnyTip").css('color', '#ea6644');
        return false;
    }
}
function departure1() {
    var departure1 = $("#departure1").val();
    if (departure1 == 0) {
        $("#csnyTip").html('请选择出发的地');
        $("#csnyTip").css('color', '#ea6644');
        return false;
    }else{
          $("#csnyTip").html('OK');
        $("#csnyTip").css('color', '#428bca');
    }
}

function destination() {
    var destination = trim($("#destination").val());
    if (destination == '' || destination == 0) {
        $('#destinations').val('');
        $('#destinations').attr('placeholder','');
        $("#csnyTips").html('请选择目的地');
        $("#csnyTips").css('color', '#ea6644');
        return false;
    } else {
        $('#destinations').val(destination);
        $('#destinations').attr('placeholder',destination);
        $("#csnyTips").html('OK');
        $("#csnyTips").css('color', '#428bca');
    }
}
function destinations() {
    var destinations = trim($("#destinations").val());
    if (destinations == '' || destinations == 0) {
        $("#csnyTipss").html('请输入途径目的地');
        $("#csnyTipss").css('color', '#ea6644');
        return false;
    } else {
        $("#csnyTipss").html('OK');
        $("#csnyTipss").css('color', '#428bca');
    }
}

//添加产品
function addTourInfo(tid, keyid) {
    var url = $('#addtourinfo').val();
    common(tid, keyid, url);
}

//编辑产品
function edittourinfo(tid, keyid) {
    var url = $('#edittourinfo').val();
    common(tid, keyid, url);
}

function common(tid, keyid, url) {
	var waitForUploadPicCount = $("div.uploader-wait").length;
    if (waitForUploadPicCount > 0) {
        if (!confirm("您有尚未上传的产品图片，确认放弃上传么？")) {
            return;
        }
    }

    var promotion = 0;
    if ($("#promotion").is(":checked")) {
        promotion = 1;
    }
    
    var linename = trim($("#linename").val());
    var count = linename.length / 1024;
    var line_type = $("#line_type").children('label').children('input:checked').val();
    var traffic_go = $("#traffic_go").children('label').children('input:checked').val();
    var traffic_back = $("#traffic_back").children('label').children('input:checked').val();
    var travel_type = $("#travel_type").children('label').children('input:checked').val();
    var destination = $("#destination").children('option:checked').val();
    var destinations = $("#destinations").val();
    var departure = $("#departure").val();
    var departure1 = $("#departure1").val();
    var contact_person = $("#contact_person").val();
    var contact_phone = $("#contact_phone").val();
    var length = Math.round(count * Math.pow(10, 4));
    var pics = getPics();
    var coverPic = $("#cover-pic").val();
    var p_name = $(".p_name").html();

    if (destinations.indexOf('，') > 0) {
        var dest = destinations.replace('，', ',');
    } else {
        var dest = destinations;
    }
    var info = {};
    info.tid = tid;
    info.keyid = keyid;
    info.name = linename;
    info.p_name = p_name;
    info.line_type = line_type;
    info.departure = departure;
    info.departure1 = departure1;
    info.destination = destination;
    info.destinations = dest;
    info.traffic_go = traffic_go;
    info.traffic_back = traffic_back;
    info.contact_person = contact_person;
    info.contact_phone = contact_phone;
    info.travel_type = travel_type;
    info.pics = pics;
    info.promotion = promotion;
    info.cover_pic = coverPic;
    if (length == 0) {
        jalert('线路名称不能为空', function () {
        });
        return false;
    } else if (length > 391) {
        jalert('线路名称不能操过40个汉字或者字符', function () {
        });
        return false;
    }
    if (departure == 0 || departure1 == 0) {
        jalert('请选择出发地', function () {
        });
        return false;
    }
    if (destination == '' || destination == 0) {
        jalert('请选择目的地', function () {
        });
        return false;
    }
    if (destinations == '') {
        jalert('请输入途径目的地', function () {
        });
        return false;
    }

    if (contact_person == '') {
        jalert('联系人必须填写', function () {
        });
        return false;
    }
    if (contact_phone == '') {
        jalert('联系人电话必须填写', function () {
        });
        return false;
    } else {
        var patrn = /^((\+?86)|(\(\+86\)))?\d{3,4}-\d{7,8}(-\d{3,4})?$|^((\+?86)|(\(\+86\)))?1\d{10}$/;
        if (!patrn.test(contact_phone)) {
            jalert('联系电话输入有误', function () {
            });
            return false;
        }
    }
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                window.location.href = $data['url'];
            });
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    });
}

//行程
function addPlan(tid) {
    var url = $("#planadd").val();
//    var url=$("#skuadd").val();
    $.post(url, {tid: tid}, function ($data) {
        if ($data['status'] == 1) {
            window.location.href = $data['msg'];
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    });
}
//位控团期
function addSku(tid) {
    var url = $("#skuadd").val();
    $.post(url, {tid: tid}, function ($data) {
        if ($data['status'] == 1) {
            window.location.href = $data['msg'];
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    });
}
function trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//位控
$("body").on("click", "#saveEditSku", function () {
        var times = $("#dataValues").val();//多个时间
        var timestartOne = $("#timestartOne").val();
        var aoriginal = $('#aoriginal').val();
        var aagency = $('#aagency').val();
        var alist = $('#alist').val();
        var coriginal = $('#coriginal').val();
        var cagency = $('#cagency').val();
        var clist = $('#clist').val();
        var hagency = $('#hagency').val();
        var hlist = $('#hlist').val();
        var num = $('#num').val();
        var sold = $('#sold').val();
        var days = $('#days').val();
        if (days) {
            var days = $('#days').val();
        } else {
            var days = '1';
        }
        var state = 'edit';
        var tid = $("#tid").val();
        var desc = $('#desc').val();
        var url = $('#chka').val();
        var reg = /^[0-9]*[1-9][0-9]*$/;
        var reg1 = /^[0-9]+([.]{1}[0-9]+){0,1}$/;
        var reg2 = /^(0|\+?[1-9][0-9]*)$/;
        var info = {};
        //位控
        info.times = times;
        info.timestartOne = timestartOne;
        info.skuState = state;
        info.aoriginal = aoriginal;
        info.aagency = aagency;
        info.alist = alist;
        info.coriginal = coriginal;
        info.cagency = cagency;
        info.clist = clist;
        info.hagency = hagency;
        info.hlist = hlist;
        info.num = num;
        info.sold = sold;
        info.days = days;
        info.desc = desc;
        info.day = tid;
        
        //位控验证
        if (aoriginal != undefined || aagency != undefined || alist != undefined || coriginal != undefined
                || cagency != undefined || clist != undefined || hagency != undefined || hlist != undefined
                || num != undefined || sold != undefined) {
            if (aagency == '' || aagency == null) {
                $('#aagency').attr('placeholder', '请输入金额');
                $('#aagency').css('font-size', '10px');
                $('#aagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(aagency)) {
                $('#aagency').attr('placeholder', '请输入整数');
                $('#aagency').val('请输入整数');
                $('#aagency').css('font-size', '10px');
                $('#aagency').css('border', '1px solid red');
                return false;
            } else {
                $('#aagency').css('border', '1px solid #5bc0de');
            }
            if (alist == '' || alist == null) {
                $('#alist').attr('placeholder', '请输入金额');
                $('#alist').css('font-size', '10px');
                $('#alist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(alist)) {
                $('#alist').attr('placeholder', '请输入整数');
                $('#alist').val('请输入整数');
                $('#alist').css('font-size', '10px');
                $('#alist').css('border', '1px solid red');
                return false;
            } else {
                $('#alist').css('border', '1px solid #5bc0de');
            }
            if (aoriginal != '' || aoriginal != 0) {
                if (!reg2.test(aoriginal)) {
                    $('#aoriginal').attr('placeholder', '请输入整数');
                    $('#aoriginal').val('请输入整数');
                    $('#aoriginal').css('font-size', '10px');
                    $('#aoriginal').css('border', '1px solid red');
                    return false;
                } else {
                    $('#aoriginal').css('border', '1px solid #5bc0de');
                }
            }
            if (cagency == '' || cagency == null) {
                $('#cagency').attr('placeholder', '请输入金额');
                $('#cagency').css('font-size', '10px');
                $('#cagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(cagency)) {
                $('#cagency').attr('placeholder', '请输入整数');
                $('#cagency').val('请输入整数');
                $('#cagency').css('font-size', '10px');
                $('#cagency').css('border', '1px solid red');
                return false;
            } else {
                $('#cagency').css('border', '1px solid #5bc0de');
            }
            if (clist == '' || clist == null) {
                $('#clist').attr('placeholder', '请输入金额');
                $('#clist').css('font-size', '10px');
                $('#clist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(clist)) {
                $('#clist').attr('placeholder', '请输入整数');
                $('#clist').val('请输入整数');
                $('#clist').css('font-size', '10px');
                $('#clist').css('border', '1px solid red');
                return false;
            } else {
                $('#clist').css('border', '1px solid #5bc0de');
            }

            if (coriginal != 0) {
                if (!reg2.test(coriginal)) {
                    $('#coriginal').attr('placeholder', '请输入整数');
                    $('#coriginal').val('请输入整数');
                    $('#coriginal').css('font-size', '10px');
                    $('#coriginal').css('color', '#ccc');
                    $('#coriginal').css('border', '1px solid red');
                    return false;
                } else {
                    $('#coriginal').css('border', '1px solid #5bc0de');
                }
            }
            if (hagency == '' || hagency == null) {
                $('#hagency').attr('placeholder', '请输入金额');
                $('#hagency').css('font-size', '10px');
                $('#hagency').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(hagency)) {
                $('#hagency').attr('placeholder', '请输入整数');
                $('#hagency').val('请输入整数');
                $('#hagency').css('font-size', '10px');
                $('#hagency').css('border', '1px solid red');
                return false;
            } else {
                $('#hagency').css('border', '1px solid #5bc0de');
            }
            if (hlist == '' || hlist == null) {
                $('#hlist').attr('placeholder', '请输入金额');
                $('#hlist').css('font-size', '10px');
                $('#hlist').css('border', '1px solid red');
                return false;
            } else if (!reg2.test(hlist)) {
                $('#hlist').attr('placeholder', '请输入整数');
                $('#hlist').val('请输入整数');
                $('#hlist').css('font-size', '10px');
                $('#hlist').css('border', '1px solid red');
                return false;
            } else {
                $('#hlist').css('border', '1px solid #5bc0de');
            }
            if (num == '' || num == null) {
                $('#num').attr('placeholder', '不能为空');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else if (num == 0) {
                $('#num').attr('placeholder', '必须大于1');
                $('#num').val('必须大于1');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else if (!reg.test(num)) {
                $('#num').attr('placeholder', '输入有误');
                $('#num').css('font-size', '10px');
                $('#num').css('border', '1px solid red');
                return false;
            } else {
                $('#num').css('border', '1px solid #5bc0de');
            }
            if (sold != 0) {
                if (!reg.test(sold)) {
                    $('#sold').attr('placeholder', '输入有误');
                    $('#sold').css('font-size', '10px');
                    $('#sold').css('border', '1px solid red');
                    return false;
                } else {
                    $('#sold').css('border', '1px solid #5bc0de');
                }

            }
            if (!reg.test(days)) {
                $('#days').attr('placeholder', '输入有误');
                $('#days').css('font-size', '10px');
                $('#days').css('border', '1px solid red');
                return false;
            } else {
                $('#days').css('border', '1px solid #5bc0de');
            }
        }
        if (typeof (timestart) != "undefined" || typeof (timeend) != "undefined") {
            if (timestart == null || timeend == null) {
                $("#modal_times").children('span').html('*此项不能为空');
                $("#modal_times").children('span').css('color', 'red');
                return false;
            } else {
                $("#modal_times").children('span').html('OK');
                $("#modal_times").children('span').css('color', 'blue');
            }
        }
        if (typeof (times) != "undefined") {
            if (times == "" && !timestartOne) {
                $(".timeDateShowsgs").children('span').html('*此项不能为空');
                $(".timeDateShowsgs").children('span').css('color', 'red');
                return false;
            } else {
                $(".timeDateShowsgs").children('span').html('OK');
                $(".timeDateShowsgs").children('span').css('color', 'blue');
            }
        }
        
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $('.theme-popover-mask').fadeOut(100);
                $('.theme-popover').slideUp(200);
                jalert($data['msg'], function () {
                    location.reload();
                });

            } else {
                $('.theme-popover-mask').fadeOut(100);
                $('.theme-popover').slideUp(200);
                jalert($data['msg'], function () {
                    location.reload();
                    return false;
                });
            }
        });

    });
