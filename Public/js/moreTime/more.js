/**
 * fullcalendar插件说明：
 * Created by bolen on 12/16/2014.
 */
var HLTSelectFullCalendarContext = (function (options) {
    var defaults = {
        activeDayClass: "showlists"
        , activeDayColor: "#FFFFFF"
        , id: '#dateer' //selector
        , dataDate: 'data-date'
        , dateFormat: 'yyyy-MM-dd'
        , callback: function () {
        }
    };
    var opts = $.extend({}, defaults, options);
    var selectData = new Array();
    return {
        getOptions: function () {
            return opts;
        }
        , getActiveClass: function () {
            return opts.activeDayClass;
        }
        , getSelectDates: function () {
            return selectData;
        }
        , selectDates: function (start, end) {
            var _date = '';
            var mydate = new Date();
            var cd = mydate.toLocaleDateString();
            var choseDate = start.toLocaleDateString();

            var startDate = new Date(cd.replace("-", "/").replace("-", "/"));
            var endDate = new Date(choseDate.replace("-", "/").replace("-", "/"));


            if (endDate < startDate) {
                jalert('不能选择比当期日期早的时间', function () {
                });
                this.getCalendar().fullCalendar('unselect');
                return;
            }
            for (var current = start; current <= end; current.setDate(current.getDate() + 1)) {
                _date = this.formatData(current);
                this.selectDay(_date, true);
            }
        }
        , activeDay: function (_t) {
            var hasClass = false;
            if ($(_t).hasClass(opts.activeDayClass)) {
                $(_t).removeClass(opts.activeDayClass);
                $(_t).css('color', 'inherit');
                hasClass = true;
            } else {
                $(_t).addClass(opts.activeDayClass);
                $(_t).css('color', opts.activeDayColor);
            }
            return hasClass;
        }
        , selectDay: function (_date, hasToggle) {
            var _t = this.getDayElement(_date);
            var hasClass = this.activeDay(_t);
            if (hasToggle && hasClass) {
                this.removeDate(_date);
            } else {
                this.addDate(_date);
            }
        }
        , refershSelectDay: function () {
            selectData = new Array();
            $(opts.id + ' .fc-view .uz-bg-status-5').each(function () {
                this.addDate($(this).attr(opts.dataDate));
            });
        }
        , initContainer: function (start, end) {
            var _date = '';
            var isOnly = start.getTime() < end.getTime() ? false : true;
            for (var current = start; current <= end; current.setDate(current.getDate() + 1)) {
                _date = this.formatData(current);
                if (this.contains(_date)) {
                    var _t = this.getDayElement(_date);
                    this.activeDay(_t);
                }
            }
        }
        , addDate: function (_date) {
            var _index = $.inArray(_date, selectData);
            if (_index == -1) {
                selectData.push(_date);
                this.sortArray();
                opts.callback(selectData);
            }
        }
        , removeDate: function (_date) {
            var _index = $.inArray(_date, selectData);
            if (_index != -1) {
                selectData.splice(_index, 1);
                opts.callback(selectData);
            }
        }
        , initData: function (_array) {
            selectData = _array;
            this.sortArray();
        }
        , sortArray: function () {
            selectData = selectData.sort(function (a, b) {
                var s = $.fullCalendar.parseDate(a);
                var e = $.fullCalendar.parseDate(b);
                return s.getTime() > e.getTime();
            });
        }
        , getCalendar: function () {
            var _t = $(opts.id);
            return _t;
        }
        , getDayElement: function (_date) {
            var _t = $(opts.id + ' .fc-view .fc-day[data-date="' + _date + '"]');
            return _t;
        }
        , formatData: function (date) {
            return $.fullCalendar.formatDate(date, opts.dateFormat);
        }
        , contains: function (_date) {
            var _index = $.inArray(_date, selectData);
            return _index != -1;
        }
    }
});