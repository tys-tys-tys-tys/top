/**
 * 订单取消-机票
 */
$(".orderCancel").click(function(){
    var id = $("#id").val();
    var url = $("#orderCancel").val();
    var info = {};
    info.id = id;

    $.post(url, info, function($data){
        if($data['status'] == 1){
            $("#myModalSuc").modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".suc").html($data['msg']);

            $(".doSubmit").click(function(){
                location.reload();
            });
        }else{
            $("#myModalSuc").modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".suc").html($data['msg']);
            return false;
        }
    })
});


/**
 * 订单取消 - 线路
 */
$("body").on("click", ".tourOrderCancel", function () {
    var id = $("#id").val();
    var url = $("#orderCancel").val();
    var code = $("#code").val();
    var info = {};
    info.id = id;
    info.code = code;

    $.post(url, info, function ($data) {
        if($data['status'] == 1){
            $(".proofAgain").attr("disabled", true);
            $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".doSubmitAllPayed").click(function () {
                location.reload();
            });
        }else{
            $(".proofAgain").attr("disabled", true);
            $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }
    })
});

$(function () {
    $(".cruiseId").hide();
    $(".cruiseId").eq(0).show();
});
$(".start_time").change(function () {
    $(".cruiseId").hide();
    $(".cruiseId").eq($(".start_time option:selected").index()).show();

})


/**
 * 延迟加载
 */
$(function() {
    $("img.lazy").lazyload({effect: "fadeIn"});
});



/**
 * [改价页]
 */
$('.dropdown-toggle').dropdown();

/**
 * [详情页-(伸缩框)]
 */
$("[data-event-trigger]").click(function () {
    $(this).siblings(".con").slideToggle();
    $(this).parent(".blockItem").siblings().children(".con").hide();
});

/**
 * [申请合同(快递、自取切换)]
 */
$("[data-num]").click(function () {
    if ($(this).attr("data-num") == 2) {
        $("#zq").show();
        $("#kd").hide();
    } else if ($(this).attr("data-num") == 1) {
        $("#zq").hide();
        $("#kd").show();
    }
});

/**
 * [加载更多] (国内游、出境游、周边游、自由行)
 */
$(".moreContent").click(function () {
    var page = $("#page_nav").val();
    $.post("index", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
            $("img.lazy").lazyload({effect: "fadeIn"});
        } else {
            $(".moreContent").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多] (国内游、出境游、周边游、自由行)
 */
$(".moreContentList").click(function () {
    var page = $("#page_nav").val();
    var tid = $("#id").val();

    $.post("lists", {id: tid, page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
            $("img.lazy").lazyload({effect: "fadeIn"});
        } else {
            $(".moreContentList").text('暂无更多内容');
        }
    });
});
/**
 * [加载搜索更多] (国内游、出境游、周边游、自由行)
 */
$(".SearchmoreContentList").click(function () {
    var page = $("#page_nav").val();
    var names = $("#names").val();
    var tid = $("#id").val();

    $.post("search", {id: tid, page: page, names:names}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
            $("img.lazy").lazyload({effect: "fadeIn"});
        } else {
            $(".SearchmoreContentList").text('暂无更多内容');
        }
    });
});

/**
 * [一键推荐]
 */
$("#recommend").click(function () {
    var url = $("#doRecommend").val();
    //获取线路ID
    var id = $("#id").val();
    $.post(url, {tour_id: id}, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
        } else if ($data['status'] == 0) {
            $('#myModalAuc').modal({backdrop: 'static', keyboard: false});
            $(".suc").html($data['msg']);
            $("#myModalAuc").modal();
        }
    })
});

/**
 * [改价]
 */
$('.savePrice').click(function () {
    var url = $("#changePrice").val();
    var postData = "";
    var newPrice = $(".newPrice").val();


    $("tr.sku").each(function () {
        var sku_id = $(this).attr("skuid");
        var priceType = $(this).find("input").attr("priceType");
        var price = $(this).find("input").val();

        postData += sku_id;
        postData += ",";
        postData += priceType;
        postData += ",";
        postData += price;
        postData += ",";
    });

    $(".newPrice").each(function () {
        var val = $(this).val();
        if (val <= 0) {
            $('#myModalErr').modal({backdrop: 'static', keyboard: false});
            $("#myModalErr").modal();

            $(".submitError").click(function () {
                location.reload();
            });
            $(".resetError").click(function () {
                location.reload();
            });
            return false;
        } else {
            $.post(url, {postData: postData}, function ($data) {
                if ($data) {
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".submit").click(function () {
                        location.reload();
                    });
                    $(".reset").click(function () {
                        location.reload();
                    });
                } else {
                    return false;
                }
            })
        }
    });

})
/**
 * [改分享标题]
 */
$('.saveTitle').click(function () {
    var url = $("#changeTitle").val();
    var shareTitle = $(".shareTitle").val();
    var tour_id = $("#tour_Id").val();
     var info = {};
     info.shareTitle = shareTitle;
     info.tour_id = tour_id;
 
        if (shareTitle == "") {
            $('#myModalErr').modal({backdrop: 'static', keyboard: false});
            $("#myModalErr").modal();
            return false;
        } 
        $.post(url, info, function ($data) {
            if ($data) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".submit").click(function () {
                    location.reload();
                });
                $(".reset").click(function () {
                    location.reload();
                });
            } else {
                return false;
            }
        })
})



/**
 * [加载更多](交易记录)
 */
$(".moreTransaction").click(function () {
    var page = $("#page_nav").val();
    $.post("accountTransaction", {page: page}, function (data) {

        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreTransaction").text('暂无更多内容');
        }
    });
});

//改变价钱
$("#startTime").change(function () {
    var url = $("#changeOrderMoney").val();
    var startTime = $(this).val();
    var id = $("#id").val();
    $.post(url, {startTime: startTime, id: id}, function (data) {

        if (data) {
            $(".priceAdultList").html(data.price_adult_agency);
            $(".priceChildList").html(data.price_child_agency);
            $(".priceHotelList").html(data.price_hotel_agency);
            setTotal();
        }
    })

});

//提交订单
$("#doSubmit").click(function () {
    var url = $("#orderInfo").val();
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var adultCount = $("#adult").val();
    var childCount = $("#child").val();
    var startTime = $("#startTime").val();
    var hotelCount = $("#hotel").val();
    var contacts = $("#contacts").val();
    var phone = $("#phone").val();
    var remark = $("#remark").val();
    var code = $("#code").val();
    var coupon = $('input[name="coupon"]:checked').val();
    var account_state = $("#account_state").val();

    if(account_state != 1){
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('账户被冻结');
        return false;
    }

    var info = {};
    info.id = id;
    info.keyid = keyid;
    info.code = code;
    info.client_adult_count = adultCount;
    info.client_child_count = childCount;
    info.start_time = startTime;
    info.hotel_count = hotelCount;
    info.contacts = contacts;
    info.contact_phone = phone;
    info.memo = remark;
    info.coupon = coupon;
    
    if (info.coupon == undefined) {
        info.coupon = '0';
    }

    if (info.start_time == '' || info.start_time == null) {
        $("#myModalDel").modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('暂无团期！');
        return false;
    }

    if (contacts == '' || phone == '') {
        $("#myModalDel").modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        return false;
    } else {
        jconfirm('您确定要提交订单吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 3) {
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html('请勿重复提交！');
                    return false;
                } else if ($data['status'] == 1) {
                    $(".proofAgain").attr("disabled", true);
                    $("#myModalSuc").modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".doOrderInfoJump").click(function () {
                        var jump = $("#myorder").val();
                        location.href = jump;
                    })

                } else if($data['status'] == 0){
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html($data['msg']);
                    return false;
                }else{
                    return false;
                }
            });
        })
    }

});

setTotal();
//成人
$("#adult").blur(function () {
    var adultNum = $(".adult").val();
    if (adultNum < 1) {
        $(".adult").val(1);
    }
    setTotal();
})
//儿童
$("#child").blur(function () {
    var childNum = $("#child").val();
    if (childNum < 1) {
        $("#child").val(0);
    }
    setTotal();
})
//宾馆 
$("#hotel").blur(function () {
    var hotelNum = $("#hotel").val();
    if (hotelNum < 1) {
        $("#hotel").val(0);
    }
    setTotal();
})

//数量+ 成人
$(".adultAdd").click(function () {
    var adultNum = $(this).parent().find('input[class*=adultTotal]');
    adultNum.val(parseInt(adultNum.val()) + 1);
    setTotal();
});
//数量- 成人
$(".adultReduce").click(function () {
    var adultNum = $(this).parent().find('input[class*=adultTotal]');
    adultNum.val(parseInt(adultNum.val()) - 1);
    if (parseInt(adultNum.val()) < 1) {
        adultNum.val(1);
    }
    setTotal();
});


//数量+ 儿童
$(".childAdd").click(function () {
    var childNum = $(this).parent().find('input[class*=childTotal]');
    childNum.val(parseInt(childNum.val()) + 1);
    setTotal();
});
//数量- 儿童
$(".childReduce").click(function () {
    var childNum = $(this).parent().find('input[class*=childTotal]');
    childNum.val(parseInt(childNum.val()) - 1);
    if (parseInt(childNum.val()) < 0) {
        childNum.val(0);
    }
    setTotal();
});
//数量+ 单房差
$(".hotelAdd").click(function () {
    var hotelNum = $(this).parent().find('input[class*=hotelTotal]');
    hotelNum.val(parseInt(hotelNum.val()) + 1);
    setTotal();
});
//数量- 单房差
$(".hotelReduce").click(function () {
    var hotelNum = $(this).parent().find('input[class*=hotelTotal]');
    hotelNum.val(parseInt(hotelNum.val()) - 1);
    if (parseInt(hotelNum.val()) < 1) {
        hotelNum.val(0);
    }
    setTotal();
});

function setTotal() {

    var s = s1 = s2 = s3 = s4 = 0;

    var adultTotal = $(".adultTotal");
    var childTotal = $(".childTotal");
    var hotelTotal = $(".hotelTotal");
    s1 += parseInt(adultTotal.val() * $(".priceAdultList").html());
    s2 += parseInt(childTotal.val() * $(".priceChildList").html());
    s3 += parseInt(hotelTotal.val() * $(".priceHotelList").html());

    var totalPerson = parseInt(adultTotal.val()) + parseInt(childTotal.val());

    if (totalPerson >= 2) {
        $(".coupon").removeAttr("disabled");
    } else {
        $(".coupon").attr("disabled", "true");
        $(".coupon").prop("checked", "");
    }

    $("input.coupon").change(function () {
        if ($(this).is(":checked")) {
            $(this).prop("checked", true);
            var s4 = parseInt($("#coupon").val());

            s = s1 + s2 + s3 - s4;
            $("#total").html(s.toFixed(2));
        } else {
            $(this).prop("checked", false);
            var s4 = 0;
            s = s1 + s2 + s3 + s4;
            $("#total").html(s.toFixed(2));
        }
    });

    s = s1 + s2 + s3 + s4;
    $("#total").html(s.toFixed(2));
}

function accountBalance() {
    location.reload();
}
/**
 *[扣除服务费-确定] 
 *
 */
function dl_service_ok(id){
    var url = $('#serviceHandle').val();
    var info = {};
    info.id = id;
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".doSubmitAllPayed").click(function () {
                window.location.href = $data['dl_url'];
            });
        }else if ($data['status'] == 1){
            window.location.href = $data['dl_url'];
        }

    });
}
/**
 * [合同申请]
 */
//+ 份数
$(".add").click(function () {
    var addNum = $(this).parent().find('input[class*=numTotal]');
//    console.log(addNum);
    var contractNum = $("#contractNum").val();
    addNum.val(parseInt(addNum.val()) + 1);
//    if (parseInt(addNum.val()) > contractNum) {
//        addNum.val(contractNum);
//    }
});
//- 份数
$(".reduce").click(function () {
    var reduceNum = $(this).parent().find('input[class*=numTotal]');

    reduceNum.val(parseInt(reduceNum.val()) - 1);
    if (parseInt(reduceNum.val()) < 1) {
        reduceNum.val(0);
    }
});

$("#doSubmitApplyContract").click(function(){
    var inland = $(".inland").val();
    var outbound = $(".outbound").val();
    var peritem = $(".peritem").val();
    var url = $("#applyContract").val();
    var request_method = $('input[name="request_method"]:checked').val();
    var addr = $("#addr").val();
    var name = $("#name").val();
    var phone = $("#phone").val();
    var express_fee = $('input[name="express_fee"]:checked').val();
    var code = $("#code").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

    if (request_method == 2) {
        if (inland == 0 && outbound == 0 && peritem == 0) {
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('合同份数不能全部为0');
            refreshPage();
            return false;
        }

        if(addr == ''){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('请填写邮寄地址');
            return false;
        }

        if(name == ''){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('请填写联系人');
            return false;
        }

        if (phone == '') {
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('请填写手机号');
            return false;
        }

        if(!reg.test(phone)){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('请填写11位手机号');
            return false;
        }

        $("#doSubmitApplyContract").attr('disabled', true);
        $.post(url, {request_method: request_method, express_fee: express_fee, addr: addr, name: name, phone: phone, inland: inland, outbound: outbound, peritem: peritem, code: code}, function ($data) {
            if ($data['status'] == 1) {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmitApplyContract").click(function () {
                    var url = $("#contractList").val()
                    location.href = url;
                });

            } else {
                $('#myModalDel').modal({backdrop: 'static', keyboard: false});
                $("#myModalDel").modal();
                $(".errorInfo").html($data['msg']);
                return false;
            }
        })
    } else {
        if (inland == 0 && outbound == 0 && peritem == 0) {
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".errorInfo").html('合同份数不能全部为0');
            refreshPage();
            return false;
        } else {
            $("#doSubmitApplyContract").attr('disabled', true);
            $.post(url, {request_method: request_method, inland: inland, outbound: outbound, peritem: peritem, code: code}, function ($data) {
                if ($data['status'] == 1) {
                    $(".proofAgain").attr("disabled", true);
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".doSubmitApplyContract").click(function () {
                        var url = $("#contractList").val()
                        location.href = url;
                    });
                } else {
                    $('#myModalDel').modal({backdrop: 'static', keyboard: false});
                    $("#myModalDel").modal();
                    $(".errorInfo").html($data['msg']);
                    return false;
                }
            })
        }

    }

});
/**
 * [发票申请]
 */
$("#doSubmitApplyInvoice").click(function(){
    var url = $("#applyInvoice").val();
    var title = $("#title").val();
    var amount = $("#amount").val();
    var request_method = $('input[name="request_method"]:checked').val();
    var express_fee = $('input[name="express_fee"]:checked').val();
    var addr = $("#addr").val();
    var name = $("#name").val();
    var phone = $("#phone").val();
    var remark = $("#remark").val();
    var code = $("#code").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;
    var detail = $("#detail option:selected").val();

    if(title == ''){
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('请填写发票抬头');
        return false;
    }

    if(amount == ''){
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('请填写发票金额');
        return false;
    }

    if(amount < 100){
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('发票金额需100元以上');
        return false;
    }

    if (request_method == 2) {
        if(addr == ''){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".tipsBox").html('请填写邮件地址');
            return false;
        }

        if(name == ''){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".tipsBox").html('请填写联系人');
            return false;
        }
        if (phone == '') {
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".tipsBox").html('请填写手机号');
            return false;
        }

        if(!reg.test(phone)){
            $('#myModalDel').modal({backdrop: 'static', keyboard: false});
            $("#myModalDel").modal();
            $(".tipsBox").html('请填写11位手机号');
            return false;
        }

        $("#doSubmitApplyInvoice").attr('disabled', true);
        $.post(url, {title: title, amount: amount, detail: detail, request_method: request_method, express_fee: express_fee, addr: addr, name: name, phone: phone, remark: remark, code: code}, function ($data) {
            if ($data['status'] == 1) {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html('申请成功');
                $(".doSubmitApplyInvoice").click(function () {
                    var url = $("#invoiceList").val();
                    location.href = url;
                });
            } else {
                $('#myModalDel').modal({backdrop: 'static', keyboard: false});
                $("#myModalDel").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            }
        })
    } else {
        $("#doSubmitApplyInvoice").attr('disabled', true);
        $.post(url, {title: title, amount: amount, detail: detail, request_method: request_method, code: code}, function ($data) {
            if ($data['status'] == 1) {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmitApplyInvoice").click(function () {
                    var url = $("#invoiceList").val();
                    location.href = url;
                });
            } else {
                $('#myModalDel').modal({backdrop: 'static', keyboard: false});
                $("#myModalDel").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            }
        })

    }

});

/**
 * [充值申请-支票充值]
 */
$(".bankTransfer").click(function(){
    var topup_type = $('input[name="topup_type"]:checked').val();

    if(topup_type == 3 || topup_type == 4){
        $(".remitter").show();
    }else{
        $(".remitter").hide();
    }
});

/**
 * [充值申请]
 */
$("#doCommitApplyTopUp").one("click", function () {
    var topup_type = $('input[name="topup_type"]:checked').val();
      if(topup_type == 3 || topup_type == 4){
        var payer = $(".remitterName").val();

        if(payer == ''){
            $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
            $("#myModalSuc").modal();
            $(".tipsBox").html('请输入汇款方名称');
            refreshPage();
            return false;
        }
    }else{
        payer = '';
    }
    var url = $("#applyTopup").val();
    var amount = $(".amount").val();
    var account_state = $("#account_state").val();
    var code = $("#code").val();
    //判断金额是否为空
    if (amount == '') {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (amount < 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (amount == 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else {
        $.post(url, {topup_type: topup_type, amount: amount, payer: payer, code: code}, function ($data) {
            if ($data['status'] == 1) {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmit").click(function () {
                    var url = $("#doSubmitApplyTopup").val();
                    location.href = url;
                });
            } else {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            }
        })
    }
});
/**
 * [微信在线充值]
 */
$("#no_weixin_pay").one("click", function () {
    var wx_topup_type = $('input[name="on_topup_type"]:checked').val();//获取微信支付类型id  6
    var url = $("#applyTopup").val();
    var amount = $("#wx_pay_amount").val();
    var account_state = $("#account_state").val();//查看该顾问资金是否被冻结
    var code = $("#code").val();
    //判断金额是否为空
    if (amount == '') {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (amount < 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (amount == 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else {
       $('#wx_pay_submit').submit();
    }
});


/**
 * [提现申请]
 */
$("#doSubmitApplyWithdraw").one("click", function () {
    var url = $("#applyWithdraw").val();
    var amount = $("#amount").val();
    var account_state = $("#account_state").val();
    var account_balance = Number($("#account_balance").html());
    var bankMessage = $(".bankMessage").html();
    var code = $("#code").val();

    if (bankMessage == '') {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('您的提现账号为空,请联系我们的客服人员');
        refreshPage();
        return false;
    }

    if (amount > account_balance) {
        $('#myModalThan').modal({backdrop: 'static', keyboard: false});
        $("#myModalThan").modal();
        refreshPage();
        return false;
    } else if (account_balance < 0) {
        $('#myModalDanger').modal({backdrop: 'static', keyboard: false});
        $("#myModalDanger").modal();
        refreshPage();
        return false;
    } else if (amount < 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (amount == 0) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        refreshPage();
        return false;
    } else if (account_state == 2) {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('您的账户被冻结，请联系管理员');
        refreshPage();
        return false;
    } else {
        $.post(url, {amount: amount, account_balance: account_balance,code: code}, function ($data) {
            if ($data['status'] == 1) {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmit").click(function () {
                    var url = $("#doSubmitWithdraw").val();
                    location.href = url;
                });
            } else {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            }
        })
    }
});

/**
 * [加载更多](充值)
 */
$(".moreTopUpList").click(function () {
    var page = $("#page_nav").val();
    $.post("topupList", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreTopUpList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多](发票)
 */
$(".moreInvoiceList").click(function () {
    var page = $("#page_nav").val();
    $.post("invoiceList", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreInvoiceList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多](合同)
 */
$(".moreContractList").click(function () {
    var page = $("#page_nav").val();
    $.post("contractList", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreContractList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多]（提现）
 */
$(".moreWithdrawList").click(function () {
    var page = $("#page_nav").val();
    $.post("withdrawList", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreWithdrawList").text('暂无更多内容');
        }
    });
});


/**
 * [加载更多]（提现）
 */
$(".moreWithdrawList").click(function () {
    var page = $("#page_nav").val();
    $.post("withdrawList", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreWithdrawList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多]（机票）
 */
$(".morePlaneTicketList").click(function () {
    var page = $("#page_nav").val();
    $.post("myPlaneTicket", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".morePlaneTicketList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多]（直客机票）
 */
$(".moreVisitorPlaneTicketList").click(function () {
    var page = $("#page_nav").val();
    $.post("visitorPlaneTicket", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreVisitorPlaneTicketList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多]（机票）
 */
$(".morePlaneProfitList").click(function () {
    var page = $("#page_nav").val();
    $.post("flightProfit", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".morePlaneProfitList").text('暂无更多内容');
        }
    });
});

/**
 * [加载更多]（门票）
 */
$(".moreScenicTicketList").click(function () {
    var page = $("#page_nav").val();
    $.post("myScenicTicket", {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreScenicTicketList").text('暂无更多内容');
        }
    });
});

/**
 * [登录]
 */
$("#doLogin").click(function () {
    var url = $("#login").val();
    var login_name = $("#login_name").val();
    var login_pwd = $("#login_pwd").val();
    if (login_name == '') {
        $("#msgInfo").html('登录名不能为空');
        return false;
    } else if (login_pwd == '') {
        $("#msgInfo").html('密码不能为空');
        return false;
    } else {
        $.post(url, {login_name: login_name, login_pwd: login_pwd}, function ($data) {
            if ($data['status'] == 0) {
                $("#msgInfo").html($data['msg']);
                return false;
            } else {
                window.location.href = $data['url'];
            }
        });
    }

});

/**
 * [登录]
 */
$("#wxDoLogin").click(function () {
    var url = $("#login").val();
    var login_pwd = $("#login_pwd").val();

    if (login_pwd == '') {
        $("#msgInfo").html('密码不能为空');
        return false;
    } else {
        $.post(url, {login_pwd: login_pwd}, function ($data) {
            if ($data['status'] == 0) {
                $("#msgInfo").html($data['msg']);
                return false;
            } else {
                window.location.href = $data['url'];
            }
        });
    }

});
$("[data-event-upload]").on("click", function () {
    $(this).siblings("input").trigger("click");
});

/**
 * [description 修改个人信息-修改密码]
 */
$("#doSaveEdit").click(function () {
    var login_pwd = $("#password").val();
    var confirm_pwd = $("#confirm_password").val();
    if (login_pwd == '') {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('请输入密码');
        return false;
    } else if (login_pwd.length < 6) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('密码长度不得少于6位');
        return false;
    } else if (login_pwd != confirm_pwd) {
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('两次密码输入不一致');
        return false;
    } else {
        var jump = $("#personalJump").val();
        location.href = jump;
    }

});
/**
 * [description 修改个人栏目排序]
 */
$("#columnSortEdit").click(function () {
      var numArr = []; // 定义一个空数组
        var txt = $('#column_sort_val').find(':text'); // 获取所有文本框
        for (var i = 0; i < txt.length; i++) {
            numArr.push(txt.eq(i).val()); // 将文本框的值添加到数组中
        }
       if(mm(numArr) == true){
        $('#myModalDel').modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('请不要输入重复排序值');
        return false;
       }
});
//正则验证数组是否有重复值
function mm(a)
{
   return /(\x0f[^\x0f]+)\x0f[\s\S]*\1/.test("\x0f"+a.join("\x0f\x0f") +"\x0f");
}


var wait = 60 * 3;

$("#validate").attr("disabled", false);
function time() {
    var o = $("#validate");
    if (wait == 0) {

        o.attr("disabled", false);

        o.val("免费获取验证码");

        wait = 60 * 3;

    } else {
        o.attr("disabled", true);
        o.val("重新发送(" + wait + ")");

        wait--;

        setTimeout(function () {
            time(o)
        }, 1000)

    }

}

/**
 * [验证手机]
 */
$("#validate").click(function () {
    var url = $("#getCode").val();
    var mobile = $("#telphone").val();
    var openId = Request.QueryString("openId");
    if (mobile == '') {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('请输入手机号');
        $(".doSubmit").click(function () {
            location.reload();
        });
        $(".doReset").click(function () {
            location.reload();
        });
    } else {
        $.post(url, {mobile: mobile, openId: openId}, function ($data) {

            if ($data['status'] == '0') {
                $("#myModalSuc").modal();
                $(".tipsBox").html('您的手机与预留手机不同，请重试');
                $(".doSubmit").click(function () {
                    location.reload();
                });
                $(".doReset").click(function () {
                    location.reload();
                });
            } else if ($data['status'] == '2') {
                $("#myModalSuc").modal();

                $(this).disabled = true;
                time();
                $(".tipsBox").html('验证码已发，请注意查收');
                $("#validate").attr("disabled", true);
            } else if ($data['status'] == '10') {
                $("#myModalSuc").modal();
                $(".tipsBox").html('请于3分钟后重新获取验证码');
            } else if ($data['status'] == '5') {
                $("#myModalSuc").modal();
                $(".tipsBox").html('异常！未配置域名');
            } else if ($data['status'] == '11') {
                $("#myModalSuc").modal();
                $(".tipsBox").html('此手机号暂未通过系统审核');
            } else if ($data['status'] == '20') {
                $("#myModalWar").modal();
            }

        });
    }
});




/**
 * [申请绑定微信]
 */
$("#doBind").click(function(){
    var url = $("#bindWeixin").val();
    var openId = Request.QueryString("openId");
    var pwd = $("#pwd").val();
    var mobile = $("#telphone").val();

    if (mobile == '') {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('请输入登陆名');
        $(".doSubmit").click(function () {
            $("#telphone").val('');
            $("#pwd").val('');
            location.reload(true);
        });
        $(".doReset").click(function () {
            $("#telphone").val('');
            $("#pwd").val('');
            location.reload(true);
        });
    } else if (pwd == '') {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('请输入登陆密码');
        $(".doSubmit").click(function () {
            $("#telphone").val('');
            $("#pwd").val('');
            location.reload(true);
        });
        $(".doReset").click(function () {
            $("#telphone").val('');
            $("#pwd").val('');
            location.reload(true);
        });
    } else {
        $.post(url, {mobile: mobile, openId: openId, pwd: pwd}, function ($data) {
            if ($data['status'] == '1') {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html('恭喜您！绑定成功！');
                $(".doSubmit").click(function () {
                    window.location.href = $data['url'];
                });
                $(".doReset").click(function () {
                    window.location.href = $data['url'];
                });
            } else if ($data['status'] == '0') {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                $(".doSubmit").click(function () {
                    $("#telphone").val('');
                    $("#pwd").val('');
                    location.reload(true);
                });
                $(".doReset").click(function () {
                    $("#telphone").val('');
                    $("#pwd").val('');
                    location.reload(true);
                });
            }
        });
    }
});
/**
 * [付预付款]
 */
$("body").on("click", ".prePayed", function () {
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var url = $("#prePayed").val();
    var account_state = $("#account_state").val();
    var code = $("#code").val();

    if (account_state == 2) {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('您的账户被冻结，请联系管理员！');
        return false;
    } else {
        $.post(url, {id: id, keyid: keyid, code: code}, function ($data) {

            if ($data['status'] == 3) {
                return false;
            } else if ($data['status'] == 0) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html('您的可用余额不足，请先充值！');
                return false;
            }else if ($data['status'] == 10) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            } else {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmitPrePayed").click(function () {
                    location.reload();
                });
            }
        })
    }

});
/**
 * [付尾款]
 */
$("body").on("click", ".preForum", function () {
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var url = $("#preForum").val();
    var account_state = $("#account_state").val();
    var code = $("#code").val();

    if (account_state == 2) {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('您的账户被冻结，请联系管理员');
        return false;
    } else {
        $.post(url, {id: id, keyid: keyid, code: code}, function ($data) {

            if ($data['status'] == 3) {
                return false;
            } else if ($data['status'] == 0) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html('您的可用余额不足，请先充值！');
                return false;
            }else if ($data['status'] == 10) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            } else {
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmitPrePayed").click(function () {
                    location.reload();
                });
                $(".doReset").click(function () {
                    location.reload();
                });
            }
        })
    }
});
/**
 * [点击刷新页面]
 */
$(".doRefresh").click(function () {
    location.reload(true)
});
/**
 * [付全款]
 */
$("body").on("click", ".allPayed", function () {
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var url = $("#allPayed").val();
    var account_state = $("#account_state").val();
    var code = $("#code").val();

    if (account_state == 2) {
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('您的账户被冻结，请联系管理员');
        return false;
    } else {
        $.post(url, {id: id, keyid: keyid, code: code}, function ($data) {

            if ($data['status'] == 3) {
                return false;
            } else if ($data['status'] == 0) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html('您的可用余额不足，请先充值！');
                return false;
            }else if ($data['status'] == 10) {
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".tipsBox").html($data['msg']);
                return false;
            } else if($data['status'] == 1){
                $(".proofAgain").attr("disabled", true);
                $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                $("#myModalSuc").modal();
                $(".doSubmitAllPayed").click(function () {
                    location.reload();
                });
            }
        })
    }
});
/**
 * [description 加载更多订单]
 */
$(".moreOrder").click(function () {
    var url = $("#myorder").val();
    var page = $("#page_nav").val();
    $.post(url, {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreOrder").text('暂无更多内容');
        }
    });
});
/**
 * [description 加载更多 我的推荐]
 */
$(".moreRecommend").click(function () {
    var url = $("#recommend").val();
    var page = $("#page_nav").val();
    $.post(url, {page: page}, function (data) {
        if (data) {
            $("#page_nav").val(parseInt(page) + 1);
            $("#content").append(data);
        } else {
            $(".moreRecommend").text('暂无更多内容');
        }
    });
});

/**
 * 充值申请-银行转账
 */
$("[data-num]").click(function () {
    if ($(this).attr("data-num") == 4) {
        $(".bankInfo").show();
        $(".alipayInfo").hide();
    }else if($(this).attr("data-num") == 5){
        $(".alipayInfo").show();
        $(".bankInfo").hide();
    } else {
        $(".bankInfo").hide();
        $(".alipayInfo").hide();
    }
});
/**
 * 充值申请-线上充值
 */
$("#on_line_recharge").click(function () {
     var admin_id = $('#admin_id').val();
    if(admin_id == 295 || admin_id == 296){
        $("#line_recharge_area").hide();
        $("#on_line_recharge_area").show();
            }
     return false;  
});
/**
 * 充值申请-线下充值
 */
$("#line_recharge").click(function () {
   
         $("#line_recharge_area").show();
        $("#on_line_recharge_area").hide();

});


//提交订单
$("#doSubmitProduct").click(function () {
    var url = $("#orderInfo").val();
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var adultCount = $("#adult").val();
    var contacts = $("#contacts").val();
    var phone = $("#phone").val();
    var remark = $("#remark").val();
    var code = $("#code").val();
    var account_state = $("#account_state").val();

    if(account_state != 1){
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('账户被冻结');
        return false;
    }

    var info = {};
    info.id = id;
    info.keyid = keyid;
    info.code = code;
    info.client_adult_count = adultCount;
    info.contacts = contacts;
    info.contact_phone = phone;
    info.memo = remark;
    
    if (contacts == '' || phone == '') {
        $("#myModalDel").modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        return false;
    } else {
        jconfirm('您确定要提交订单吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 3) {
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html('请勿重复提交！');
                    return false;
                } else if ($data['status'] == 1) {
                    $(".proofAgain").attr("disabled", true);
                    $("#myModalSuc").modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".doOrderInfoJump").click(function () {
                        var jump = $("#myorder").val();
                        location.href = jump;
                    })

                }else if($data['status'] == 0){
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html($data['msg']);
                    return false;
                } else {
                    return false;
                }
            });
        });
    }

});

setProductTotal();
//成人
$("#adult").blur(function () {
    var productNum = $(".adult").val();
    if (productNum < 1) {
        $(".adult").val(1);
    }
    setProductTotal();
})

//数量+ 
$(".adultAdd").click(function () {
    var productNum = $(this).parent().find('input[class*=productTotal]');
    productNum.val(parseInt(productNum.val()) + 1);
    setProductTotal();
});
//数量- 
$(".adultReduce").click(function () {
    var productNum = $(this).parent().find('input[class*=productTotal]');
    productNum.val(parseInt(productNum.val()) - 1);
    if (parseInt(productNum.val()) < 1) {
        productNum.val(1);
    }
    setProductTotal();
});

function setProductTotal() {
    var s = 0;
    var adultTotal = $(".productTotal");
    s += adultTotal.val() * $(".priceAdultList").html();
    $("#productTotal").html(s.toFixed(2));
}


function refreshPage(){
    $(".doSubmit").click(function(){
        location.reload();
        return false;
    });

    $(".doCancel").click(function(){
        location.reload();
        return false;
    });
}


$(".clickToMoreForProviderInfo").click(function(){
    $(".hiddenProviderInfo").show();
    $(".clickToMoreForProviderInfo").html('暂无更多内容');
});

$(".jumpToProviderList").click(function(){
    var url = $(".jumpToProviderList").attr("id");
    var jumpUrl = '/index.php/Home/Personal/lists/id/'+url+'.html';
    window.location.href = jumpUrl;
});