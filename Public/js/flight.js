$(".doRefundFlightApplication").click(function () {
    var url = $("#refundApplication").val();
    var passengerName = $(".passengerName").val();
    var ticketNo = $(".ticketNo").val();
    var segment = $(".segment").val();
    var orderNo = $(".orderNo").val();
    var refundType = $(".refundType").val();

    var info = {};
    info.passengerName = passengerName;
    info.ticketNo = ticketNo;
    info.segment = segment;
    info.orderNo = orderNo;
    info.refundType = refundType;

    if (refundType == 0) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请按实际情况选择退票类型');
        return false;
    }

    if (passengerName == '' || ticketNo == '' || orderNo == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请将信息填写完整!');
        return false;
    } else {
        $(".doRefundFlightApplication").html('申请中');
        $(".doRefundFlightApplication").addClass('disabled');
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalmob').modal({backdrop: 'static', keyboard: false});
                $("#myModalmob").modal();
                $(".tipsBox").html($data['msg']);
            } else if ($data['status'] == 2) {
                $('#myModalmob').modal({backdrop: 'static', keyboard: false});
                $("#myModalmob").modal();
                $(".tipsBox").html($data['msg']);
                $(".doRefundFlightApplication").html('确定');
                $(".doRefundFlightApplication").removeClass('disabled');
                return false;
            }
        })
    }
});