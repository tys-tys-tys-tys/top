//运营商添加
function addOpoerator() {
    $("#modal_name").val('');
    $("#modal_login").val('');
    $("#modal_phone").val('');
    $("#modal_pwd").val('');
    $("#modal_pwd2").val('');

    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
    });
    $('.modal-footer #reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
    });

    $('#button').click(function () {
        var url = $("#chka").val();
        var name = $("#modal_name").val();
        var loginname = $("#modal_login").val();
        var pwd = $("#modal_pwd").val();
        var pwd2 = $("#modal_pwd2").val();
        var phone = $("#modal_phone").val();
        var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

        var info = {};
        info.name = name;
        info.login_name = loginname;
        info.login_pwd = pwd;
        info.login_pwd2 = pwd2;
        info.phone = phone;

        if (name == '') {
            jalert('请填写公司名称', function () {
            });
            return false;
        }

        if (loginname == '') {
            jalert('请填写登录名', function () {
            });
            return false;
        }

        if (pwd == '') {
            jalert('请填写密码', function () {
            });
            return false;
        }

        if (pwd.length < 6) {
            jalert('密码长度不得少于6位', function () {
            });
            return false;
        }

        if (pwd != pwd2) {
            jalert('两次密码输入不一致', function () {
            });
            return false;
        }

        if (!reg.test(phone)) {
            jalert('请填写11位手机号', function () {
            });
            return false;
        }

        $('#button').attr('disabled', true);
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload(true);
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload(true);
                });
                return false;
            }
        });
    });
}


$(".saveOperatorEditInfo").click(function () {
    var url = $("#editOperatorName").val();
    var id = $("#id").val();
    var invoiceFee1 = $(".团款").val();
    var invoiceFee2 = $(".旅游费").val();
    var invoiceFee3 = $(".代订机票").val();
    var invoiceFee4 = $(".代订房费").val();
    var invoiceFee5 = $(".代订车费").val();
    var invoiceFee6 = $(".签证费").val();
    var invoiceFee7 = $(".会议服务费").val();
    var contractFee = $(".contract_fee").val();
    var expressFee = $(".express_fee").val();
    var mobile = $(".mobile").val();
    var topup_info = $(".topup_info").val();
    var alipay_info = $(".alipay_info").val();
    var invoice_title = $(".invoice_title").val();
    var address = $(".address").val();
    var copyright = $(".copyright").val();
    var tel = $(".tel").val();
    var fax = $(".fax").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

    if (!reg.test(mobile)) {
        jalert('请填写11位手机号', function () {
        });
        return false;
    }

    var info = {};
    info.id = id;
    info.invoiceFee1 = invoiceFee1;
    info.invoiceFee2 = invoiceFee2;
    info.invoiceFee3 = invoiceFee3;
    info.invoiceFee4 = invoiceFee4;
    info.invoiceFee5 = invoiceFee5;
    info.invoiceFee6 = invoiceFee6;
    info.invoiceFee7 = invoiceFee7;
    info.contractFee = contractFee;
    info.expressFee = expressFee;
    info.mobile = mobile;
    info.topup_info = topup_info;
    info.alipay_info = alipay_info;
    info.invoice_title = invoice_title;
    info.address = address;
    info.copyright = copyright;
    info.tel = tel;
    info.fax = fax;
    //console.info(info);
    //return false;

    $(".saveOperatorEditInfo").attr('disabled', true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
});