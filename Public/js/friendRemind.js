
$(".friendRemind").click(function () {
    $("#myModalNotice").modal();
      $("#dl_star_login").val('');
    $("#comm_content_login").val('');
    $('#saveAdviserFeedback').click(function(){
        saveAdviserFeedback();
    });
});
$(".friendRemind_login").click(function () {
    $("#myModalSupplier").modal();
    $("#dl_star_login").val('');
    $("#comm_content_login").val('');
    $('#saveAdviserFeedback_login').click(function(){
        saveAdviserFeedback_login();
    });
});

//处理顾问对供应商的意见反馈
function saveAdviserFeedback_login() {
    
    var url = $("#feddback_url").val();
    var supplier_id = $("#dl_supplier_id").val();
    var star = $("#dl_star_login").val();
    var comm_content = $("#comm_content_login").val();
   
    var info = {};
    info.supplier_id = supplier_id;
    info.star = star;
    info.comm_content = comm_content;
    if(supplier_id == 0) {
        $('#myModalSupplier').modal('hide');
        jalert('请选择供应商', function () {
        });
        return false;
    }
    if(comm_content == ''){
        $('#myModalSupplier').modal('hide');
        jalert('意见内容不能为空', function () {
        });
        return false;
    }
    $('#saveAdviserFeedback_login').attr("disabled", true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalSupplier').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalSupplier').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}
//处理C客户对顾问的意见反馈
function saveAdviserFeedback() {
    
    var url = $("#feddback_url").val();
    var star = $("#dl_star").val();
    var comm_content = $("#comm_content").val();
   
    var info = {};
    info.star = star;
    info.comm_content = comm_content;
    if(comm_content == ''){
        $('#myModalNotice').modal('hide');
        jalert('意见内容不能为空', function () {
        });
        return false;
    }
    $('#saveAdviserFeedback').attr("disabled", true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalNotice').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalNotice').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}
$(".shopKeeper").click(function () {
    $("#myModalstore").modal();
});


$(".stores").click(function () {
    $("#myModalstores").modal();
});

//$(".contact").click(function () {
//    $("#myModalmob").modal();
//});



