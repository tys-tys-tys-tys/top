$(".actualAmount").focus(); 
$(".doSubmit").click(function(){
    var actualAmount = $(".actualAmount").val();
    var fee = actualAmount / (1 - 0.0075);
    var amountPayable = fee.toFixed(2);
    
    if(actualAmount <= 0){
        $("#myModalNotice").modal();
        $(".notice").html('请输入0元以上金额');
        
        $(".submit").click(function(){
            location.reload();
            $(".actualAmount").val('');
        })
        $(".reset").click(function(){
            location.reload();
            $(".actualAmount").val('');
        });
    }else if (!actualAmount.match(/^[0-9]*$/)) {
        $("#myModalNotice").modal();
        $(".notice").html('请您输入正确的金额');
        
        $(".submit").click(function(){
            location.reload();
            $(".actualAmount").val('');
        })
        $(".reset").click(function(){
            location.reload();
            $(".actualAmount").val('');
        });
    }else if (actualAmount == ''){
        $("#myModalNotice").modal();
        $(".notice").html('您还未输入金额');
        
        $(".submit").click(function(){
            location.reload();
            $(".actualAmount").val('');
        })
        $(".reset").click(function(){
            location.reload();
            $(".actualAmount").val('');
        });
    }else{
        $(".actualAmount").html("您输入的金额为：" + "<span style='color:#FFA500'>¥" + actualAmount + "</span>");
        $(".amount-payable").html("您应刷卡金额为：" + "<span style='color:#FFA500'>¥" + amountPayable + "</span>");
    }
});