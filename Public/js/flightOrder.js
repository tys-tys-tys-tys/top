$(".doSubmitFlightOrder").click(function () {
    $(".showButton").hide();
    $(".showCircle").show();

    var priceTotal = $(".priceTotal").html();
    var flightId = $("#flightId").val();
    var keyid = $("#keyid").val();
    var seatCode = $("#seatCode").val();
    var flightNo = $("#flightNo").val();
    var specialParPrice = $("#specialParPrice").val();
    var code = $("#code").val();
    var url = $("#orderPay").val();
    
    var info = {};
    priceTotal = priceTotal * 100;
    info.priceTotal = priceTotal;
    info.flightId = flightId;
    info.keyid = keyid;
    info.seatCode = seatCode;
    info.flightNo = flightNo;
    info.specialParPrice = specialParPrice;
    info.code = code;

    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $(".showCircle").hide();
            $(".proofAgain").attr("disabled", true);
            $("#myModalmob").modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html('付款成功!');

            $(".doSubmitWarning").click(function () {
                $(".payContent").html('付款成功!');
            });
            
            $(".showJumpButton").show();
        } else if ($data['status'] == 2) {
            $(".showCircle").hide();
            $(".showTopupButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html('账户余额不足,请充值');
            return false;
        }else if($data['status'] == 3){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html('请勿重复提交!');
            return false;
        }else if($data['status'] == 4){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }else if($data['status'] == 5){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }else if($data['status'] == 6){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html('账户被冻结,请联系客服');
            return false;
        }else if($data['status'] == 7){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }else if($data['status'] == 8){
            $(".showCircle").hide();
            $(".showButton").show();
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }

    })
});


//进行倒计时显示
var time;
var h=30;
function run(){
    --h;

    if(h < 1){
        h = 30;
    }
    $('.showDiv').html("亲,您的订单正在支付,请勿关闭订单  " + h);
}