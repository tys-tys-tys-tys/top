function setCommission(tid, keyid){
    $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
    $("#myModalEdit").modal();
    var url = $("#showCommission").val();
    var info = {};
    info.id = tid;

    $.post(url, info, function(data){
        if(data){
            $(".commission_rate").val(data['commission_rate']);

            $(".doSubmitTopup").one("click", function () {
                saveCommission(tid, keyid);
            });
        }else{
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })

}

function saveCommission(tid, keyid){
    var url = $("#saveCommission").val();
    var commission_rate = $(".commission_rate").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.commission_rate = commission_rate;

    $.post(url, info, function($data){
        if($data['status'] == 1){
            $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
            $("#myModalEdit").modal('hide');
            jalert($data['msg'], function () {
                location.reload();
            });
        }else{
            $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
            $("#myModalEdit").modal('hide');
            jalert($data['msg'], function () {
                location.reload();
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}

function setAccount(tid, keyid, state){
    var url = $("#setAccount").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.state = state;

    jconfirm('您确定要同意吗', function () {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });
}

function refundFlightTicket(tid, fid){
    $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
    $("#myModalEdit").modal();

    $(".doSubmitRefundFlightTicket").one("click", function () {
        var url = $("#refundFlightTicket").val();
        var refund_fee = $(".refund_fee").val();
        $.post(url, {id: tid, fid: fid, refund_fee: refund_fee}, function($data){
            if($data['status'] == 1){
                $("#myModalEdit").modal('hide');
                jalert($data['msg'], function () {
                    location.reload();
                });
            }else{
                $("#myModalEdit").modal('hide');
                jalert($data['msg'], function () {
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        })
    });
}

function showProviderAccountInfo(tid){
    var url = $("#showProviderAccount").val();
    $.post(url,{id:tid},function($data){
        if($data['status'] == 1){
            $('#myModalInfo').modal({backdrop: 'static', keyboard: false});
            $("#myModalInfo").modal();
            $(".provider_name").html($data['name']);
            $(".total").html($data['total']);
            $(".frozenMoney").html($data['frozenMoney']);
            $(".availableBalance").html($data['availableBalance']);
        }else{
            jalert($data['msg'], function () {
            });
            return false;
        }
    })
}
/**
 * 查看账户总额
 */
function showAccountBalance(tid, entityId, oid) {
    var url = $("#showAccountBalance").val();
    $.post(url, {id: tid, oid: oid}, function ($data) {
        if ($data['status'] == 1) {
            $(".showAccountBalance" + entityId).hide();
            $(".showBalance" + entityId).show();
            $(".showBalance" + entityId).html($data['account_balance']);
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    })
}

/**
 * 查看全名
 */
function showFullName(tid, entityId) {
    var url = $("#showFullName").val();
    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            $(".showFullName" + entityId).hide();
            $(".fullName" + entityId).show();
            $(".fullName" + entityId).html($data['full_name']);
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    })
}


/**
 * [已审核]
 */
function changePassState(tid, create_time, state, code) {

    if (state && tid) {
        var reason = '';
        var url = $("#chkState").val();
        jconfirm('您确定要同意吗', function () {
            $.post(url, {id: tid, state: state, memo: reason, code: code, create_time: create_time}, function ($data) {
                if ($data['status'] == 1) {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                    });
                    return false;
                }
            });
        });
    } else {
        return false;
    }
}

/**
 * 充值申请 - 已审核
 */
function changeTopupPassState(tid, keyid, create_time, state, agency_state, account_state) {
    /*
    if(agency_state != '1'){
        jalert('代理商账户被禁用', function () {
        });
        return false;
    }

    if(account_state != '1'){
        jalert('代理商账户被冻结', function () {
        });
        return false;
    }
    */

    jconfirm('您确定要同意吗', function () {
        $(".proof_type").val('');
        $(".proof_code").val('');
        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
        $("#myModalEdit").modal();

        $(".doSubmitTopup").one("click", function () {
            var url = $("#chkState").val();
            var reason = '';
            var proof_type = $(".proof_type").val();
            var proof_code = $(".proof_code").val();
            var code = $("#code").val();
            var info = {};
            info.id = tid;
            info.keyid = keyid;
            info.state = state;
            info.memo = reason;
            info.code = code;
            info.proof_type = proof_type;
            info.proof_code = proof_code;
            info.create_time = create_time;

            if (proof_type == '' || proof_code == '') {
                $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                $("#myModalEdit").modal('hide');
                jalert('请填写凭证信息', function () {
                    location.reload();
                });
                return false;
            }

            if (state && tid) {
                $.post(url, info, function ($data) {
                    if ($data['status'] == 1) {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert($data['msg'], function () {
                            location.reload();
                        });
                    } else {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert($data['msg'], function () {
                            location.reload();
                        });
                        return false;
                    }
                }, "json").error(function (xmlHttpRequest) {
                    jalert("服务器错误:" + xmlHttpRequest.status);
                });

            } else {
                return false;
            }
        });
    });
}

/**
 * 读取参数
 */

function showPara(tid){
    var url = $("#showPara").val();

    $(".para_type").val('');
    $(".para_value").val('');
    $('#myModalPara').modal({backdrop: 'static', keyboard: false});
    $("#myModalPara").modal();

    $.post(url,{id:tid},function(data){
        if(data){
            $(".para_type").val(data['para_type']);
            $(".para_value").val(data['para_value']);

            $(".doSubmitTopup").one("click", function () {
                updateParaInfo(tid);
            });
        }else{
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}

/**
 * 更新参数
 */
function updateParaInfo(tid) {
    var url = $("#addPara").val();
    var para_type = $(".para_type").val();
    var para_value = $(".para_value").val();

    if (para_type == '' || para_value == '') {
        $('#myModalPara').modal({backdrop: 'static', keyboard: false});
        $("#myModalPara").modal('hide');
        jalert('请填写参数信息', function () {
            location.reload();
        });
        return false;
    }

    if (tid) {
        $.post(url, {id: tid, para_value: para_value, para_type: para_type}, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalPara').modal({backdrop: 'static', keyboard: false});
                $("#myModalPara").modal('hide');
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                $('#myModalPara').modal({backdrop: 'static', keyboard: false});
                $("#myModalPara").modal('hide');
                jalert($data['msg'], function () {
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });

    } else {
        return false;
    }
}


/**
 * 更新凭证
 */

function updateProof(tid, keyid, state, type){
    var url = $("#showProof").val();
    var info = {};
    info.id = tid;
    info.type = type;

    $(".proof_type").val('');
    $(".proof_code").val('');
    $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
    $("#myModalEdit").modal();
    
    $.post(url, info, function(data){
        if(data){
            $(".proof_type").val(data['proof_type']);
            $(".proof_code").val(data['proof_code']);
            
            $(".doSubmitTopup").click(function(){
                submitUpdateProofInfo(tid, keyid, state);
            });
        }else{
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}



function submitUpdateProofInfo(tid, keyid, state) {
    var url = $("#invoiceList").val();
    var proof_type = $(".proof_type").val();
    var proof_code = $(".proof_code").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.state = state;
    info.proof_type = proof_type;
    info.proof_code = proof_code;

    if (proof_type == '' || proof_code == '') {
        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
        $("#myModalEdit").modal('hide');
        jalert('请填写凭证信息', function () {
            location.reload();
        });
        return false;
    }

    if (state && tid) {
        $(".doSubmitTopup").attr('disabled', true);
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                $("#myModalEdit").modal('hide');
                jalert($data['msg'], function () {
                    location.reload(true);
                });
            } else {
                $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                $("#myModalEdit").modal('hide');
                jalert($data['msg'], function () {
                    location.reload(true);
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });

    }
}

/**
 * [提现申请-已审核,待财务处理]
 */
function changeWithdrawPassState(tid, keyid, state, agency_state, account_state) {
    if(agency_state != '1'){
        jalert('代理商账户被禁用', function () {
        });
        return false;
    }

    if(account_state != '1'){
        jalert('代理商账户被冻结', function () {
        });
        return false;
    }

    if (state && tid) {
        var reason = '';
        var url = $("#chkState").val();
        jconfirm('您确定要同意吗', function () {
            var info = {};
            info.id = tid;
            info.keyid = keyid;
            info.state = state;
            info.memo = reason;

            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    } else {
        return false;
    }
}


/**
 * [提现申请-处理完成]
 */
function changeWithdrawPass(tid, keyid, state, agency_state, account_state) {
    if(agency_state != '1'){
        jalert('代理商账户被禁用', function () {
        });
        return false;
    }

    if(account_state != '1'){
        jalert('代理商账户被冻结', function () {
        });
        return false;
    }

    jconfirm('您确定要同意吗', function () {
        $(".proof_type").val('');
        $(".proof_code").val('');
        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
        $("#myModalEdit").modal();

        $(".doSubmitTopup").one("click", function () {
            var url = $("#chkStatePass").val();
            var reason = '';
            var proof_type = $(".proof_type").val();
            var proof_code = $(".proof_code").val();
            var code = $("#code").val();

            if (proof_type == '' || proof_code == '') {
                $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                $("#myModalEdit").modal('hide');
                jalert('请填写凭证信息', function () {
                    location.reload();
                });
                return false;
            }

            if (state && tid) {
                var info = {};
                info.id = tid;
                info.keyid = keyid;
                info.state = state;
                info.memo = reason;
                info.code = code;
                info.proof_type = proof_type;
                info.proof_code = proof_code;

                $.post(url, info, function ($data) {
                    if ($data['status'] == 1) {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert($data['msg'], function () {
                            location.reload();
                        });
                    } else {
                        $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
                        $("#myModalEdit").modal('hide');
                        jalert($data['msg'], function () {
                            location.reload();
                        });
                        return false;
                    }
                }, "json").error(function (xmlHttpRequest) {
                    jalert("服务器错误:" + xmlHttpRequest.status);
                });

            } else {
                return false;
            }
        });
    });
}


/**
 * 拒绝
 */
function rejectWithdrawState(tid, keyid, amount) {
    $('#modal_reject').children('textarea').val('');
    var amount = parseInt($(".amount").html());
    clearDivValue();
    showHidenDiv('拒绝理由');
    $('#button').one("click", function () {
        var url = $("#chkState").val();
        var reason = $('#modal_reject').children('textarea').val();
        if (reason == '') {
            jalert('请输入拒绝理由', function () {
            });
            return false;
        }

        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = 3;
        info.memo = reason;
        info.amount = amount;

        $.post(url, info, function ($data) {

            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });
}


/**
 * 拒绝
 */
function financeRejectWithdrawState(tid, keyid, amount) {
    $('#modal_reject').children('textarea').val('');
    var amount = parseInt($(".amount").html());
    clearDivValue();
    showHidenDiv('拒绝理由');
    $('#button').one("click", function () {
        var url = $("#financeAuditState").val();
        var reason = $('#modal_reject').children('textarea').val();
        if (reason == '') {
            jalert('请输入拒绝理由', function () {
            });
            return false;
        }

        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = 3;
        info.memo = reason;
        info.amount = amount;

        $.post(url, info, function ($data) {

            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });
}


/**
 * 拒绝
 */
function rejectState(tid, keyid, state) {
    $('#modal_reject').children('textarea').val('');
    clearDivValue();
    showHidenDiv('拒绝理由');
    $('#button').one("click", function () {
        var url = $("#chkState").val();
        var reason = $('#modal_reject').children('textarea').val();
        if (reason == '') {
            jalert('请输入拒绝理由', function () {
            });
            return false;
        } else {
            var info = {};
            info.id = tid;
            info.keyid = keyid;
            info.state = state;
            info.memo = reason;

            $.post(url, info, function ($data) {
                if ($data) {
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                } else {
                    jalert($data['msg'], function () {
                    });
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        }

    });
}

/**
 * [passChange 发票 - 快递-已审核]
 */
function passChangeApply(tid, keyid, agency_state, account_state) {
    if(agency_state != '1'){
        jalert('代理商账户被禁用', function () {
        });
        return false;
    }

    if(account_state != '1'){
        jalert('代理商账户被冻结', function () {
        });
        return false;
    }

    jconfirm('您确定要同意吗', function () {
        var code = $("#code").val();
        var state = 2;
        var url = $("#invoiceList").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = state;
        info.code = code;

        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });
}


/**
 * 填写快递信息
 */
function saveExpress(tid, type) {
    var url = $("#showExpress").val();
    $('#myModalEdit1').modal({backdrop: 'static', keyboard: false});
    $("#myModalEdit1").modal();
    $(".name").val('');
    $(".mobile").val('');
    $(".contact_person").val('');
    $(".contact_phone").val('');
    $(".contact_addr").val('');
    $(".express_name").val('');
    $(".express_number").val('');
    var info = {};
    info.id = tid;
    info.type = type;

    $.post(url, info, function (data) {
        if (data) {
            $(".name").val(data['agencyList']['name']);
            $(".mobile").val(data['agencyList']['mobile']);
            $(".contact_person").val(data['agencyInfo']['contact_person']);
            $(".contact_phone").val(data['agencyInfo']['contact_phone']);
            $(".contact_addr").val(data['agencyInfo']['contact_addr']);
            $('.doSubmit').one("click", function () {
                expressPassSubmitInvoice(tid, type);
            });
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}


/**
 * [expressPassSubmitInvoice 发票申请-索取方式(:快递)-同意]
 */
function expressPassSubmitInvoice(tid, type) {
    var url = $("#saveExpress").val();
    var expressName = $(".express_name").val();
    var expressNumber = $(".express_number").val();
    var state = 2;
    var info = {};
    info.id = tid;
    info.state = state;
    info.type = type;
    info.expressName = expressName;
    info.expressNumber = expressNumber;

    if(expressName == ''){
        $('#myModalEdit1').modal('hide');
        jalert('请填写快递名称', function () {
        });
        return false;
    }

    if(expressNumber == ''){
        $('#myModalEdit1').modal('hide');
        jalert('请填写快递单号', function () {
        });
        return false;
    }

    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalEdit1').modal('hide');
            jalert($data['msg'], function () {
                location.reload();
            });
        } else {
            $('#myModalEdit1').modal('hide');
            jalert($data['msg'], function () {
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
}

/**
 * [rejectChange 选拒绝时 - 弹出拒绝框]
 */
function rejectChange(tid) {
    var state = 3;
    if (state == 3) {
        $('.theme-popover-mask').fadeIn(100);
        $('.theme-popover').slideDown(200);
        $('.theme-poptit #reset').click(function () {
            $('.theme-popover-mask').fadeOut(100);
            $('.theme-popover').slideUp(200);
        });
        $('#button').click(function () {
            saveReject(tid);
        });
    }
}

/**
 * [saveReject 保存拒绝理由]
 */
function saveReject(tid) {
    var url = $("#invoiceList").val();
    var reason = $('#modal_reject').children('textarea').val();
    var state = 3;
    $.post(url, {id: tid, state: state, memo: reason}, function ($data) {
        if ($data == 1) {
            location.reload();
        } else {
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
}


/**
 * [showExpress 展示快递信息]
 */
function showExpress(tid, type) {
    var url = $("#showExpress").val();
    var info = {};
    info.id = tid;
    info.type = type;

    $.post(url, info, function (data) {
        $('#myModalEdit1').modal();
        $(".name").val(data['agencyList']['name']);
        $(".mobile").val(data['agencyList']['mobile']);
        $(".contact_person").val(data['agencyInfo']['contact_person']);
        $(".contact_phone").val(data['agencyInfo']['contact_phone']);
        $(".contact_addr").val(data['agencyInfo']['contact_addr']);
        $(".express_name").val(data['list']['0']);
        $(".express_number").val(data['list']['1']);
        $('.cancel').one("click", function () {
            $('#myModalEdit1').modal('hide');
        });
        $('.doSubmit').one("click", function () {
            $('#myModalEdit1').modal('hide');
        });
    })
}

/**
 * [新增代理商 弹框]
 */
$("#addAgency").click(function(){
    $('#myModalLabel').html('代理商添加');
    $("#userName").val('');
    $("#telphone").val('');
    $("#login_name").val('');
    $("#password").val('');
    $("#confirm_password").val('');
    $("#domain").val('');
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $("#userName").focus();
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        location.reload();
    });
    $('.modal-footer #reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        location.reload();
    });
});

/**
 * [变动 弹框]
 */
function addRefund(tid) {
    $("#myModalLabel").modal();
    $("#amount").val('');
    $("#proof").val('');
    $("#memo").val('');
    $('#saveRefund').one("click", function () {
        saveRefundAdd(tid);
    });
}

function saveRefundAdd(tid) {
    var url = $("#saveRefundAdd").val();
    var amount = $("#amount").val();
    var proof = $("#proof").val();
    var memo = $("#memo").val();
    var account_type = $("#account_type").val();
    var code = $("#code").val();

    if(amount == 0){
        $('#myModalLabel').modal('hide');
        jalert('金额不能为0', function () {
        });
        return false;
    }
    
    if (amount == '' || memo == '' || proof == '') {
        $('#myModalLabel').modal('hide');
        jalert('请将信息输入完整', function () {
        });
        return false;
    } else {
        $.post(url, {id: tid, amount: amount, memo: memo,account_type:account_type,proof:proof,code:code}, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalLabel').modal('hide');
                jalert($data['msg'], function () {
                    var jump = $("#refundList").val();
                    location.href = jump;
                });
            } else if($data['status'] == 0){
                $('#myModalLabel').modal('hide');
                jalert($data['msg'], function () {
                });
                return false;
            }else{
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        })
    }

}


//审核 变动
function changeRefundState(tid, keyid, state) {
    if (state && tid) {
        var code = $("#code").val();
        var url = $("#chkState").val();
        var info = {};
        info.id = tid;
        info.keyid = keyid;
        info.state = state;
        info.code = code;

        jconfirm('您确定要同意吗', function () {
            $.post(url, info, function ($data) {
                if ($data['status'] == 1) {
                    jalert('操作成功', function () {
                        location.reload();
                    });
                } else if($data['status'] == 0){
                    jalert($data['msg'], function () {
                        location.reload();
                    });
                    return false;
                }else{
                    return false;
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
        });
    } else {
        return false;
    }
}

/**
 * 高级编辑
 */
function seniorEditAgencyInfo(tid, keyid, tname, idcard) {
    var name = tname;
    $('#myModalAgencyInfo').modal({backdrop: 'static', keyboard: false});
    $("#myModalAgencyInfo").modal();
    $(".real_name").val(name);
    $(".identity_type").val(1);
    $(".identity_num").val(idcard);
    $('.submitEdit').one("click", function(){
        saveSeniorEdit(tid, keyid);
    });
}

function saveSeniorEdit(tid, keyid) {
    var url = $("#saveSeniorEdit").val();
    var real_name = $(".real_name").val();
    var identity_type = $(".identity_type").val();
    var identity_num = $(".identity_num").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.real_name = real_name;
    info.identity_type = identity_type;
    info.identity_num = identity_num;

    if (real_name == '') {
        $('#myModalAgencyInfo').modal('hide');
        jalert('请填写代理的真实姓名', function () {
        });
        return false;
    }

    if(identity_num.length < 15 || (identity_num.length > 15 && identity_num.length != 18)){
        $('#myModalAgencyInfo').modal('hide');
        jalert('请填写正确的身份证号', function () {
        });
        return false;
    }

    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalAgencyInfo').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalAgencyInfo').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}



/**
 * [修改代理商密码 弹框]
 */
function editAgencyPwd(tid, keyid, tname) {
    var name = tname;
    $('#myModalEdit').modal({backdrop: 'static', keyboard: false});
    $("#myModalEdit").modal();
    $("#agencyName").val(name);
    $("#newpassword").val('');
    $("#password2").val('');
    $('#saveAgencyPwd').click(function(){
        saveAgencyEdit(tid, keyid);
    });
}
/**
 * [扣除代理商服务费 弹框]
 */
function reduceServiceCharge(tid, keyid, tname) {
    var name = tname;
    $('#myModalServer').modal({backdrop: 'static', keyboard: false});
    $("#myModalServer").modal();
    $("#agencyName").val(name);
    $("#dl_server_name").val('');
    $("#dl_server_money").val('');
    $('#saveAgencyServer').click(function(){
        saveAgencyServiceCharge(tid, keyid);
    });
}
//修改代理商密码验证
function saveAgencyEdit(tid, keyid) {
    var url = $("#editAgencyPwd").val();
    var newPwd = $("#newpassword").val();
    var confirmPassword = $("#password2").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.newpwd = newPwd;
    info.password2 = confirmPassword;

    if (newPwd == '') {
        $('#myModalEdit').modal('hide');
        jalert('密码不能为空', function () {
        });
        return false;
    }

    if(newPwd.length < 6){
        $('#myModalEdit').modal('hide');
        jalert('密码长度不得少于6位', function () {
        });
        return false;
    }

    if(newPwd != confirmPassword){
        $('#myModalEdit').modal('hide');
        jalert('两次密码输入不一致', function () {
        });
        return false;
    }

    $('#saveAgencyPwd').attr("disabled", true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalEdit').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalEdit').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}
//修改扣除代理商服务费验证
function saveAgencyServiceCharge(tid, keyid) {
    var url = $("#reduceServiceCharge").val();
    var server_name = $("#dl_server_name").val();
    var server_money = $("#dl_server_money").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.server_name = server_name;
    info.server_money = server_money;

    if (server_name == '') {
        $('#myModalServer').modal('hide');
        jalert('服务名称不能为空', function () {
        });
        return false;
    }

    if(server_money == ''){
        $('#myModalServer').modal('hide');
        jalert('服务价格不能为空', function () {
        });
        return false;
    }

    $('#saveAgencyServer').attr("disabled", true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalServer').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalServer').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}


/**
 * [编辑代理商 - 切换]
 */
$("[data-click-tab]").on("click", function () {
    var id = $(this).attr("data-click-tab");
    $(this).addClass("current").siblings().removeClass("current");
    $("#tab-" + id).show().siblings(".panelCon").hide();
});
/**
 * 编辑代理商
 */
$(".saveEditAgencyInfo").click(function(){
    var url = $("#editName").val();
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var telphone = $("#telphone").val();
    var bank_name = $("#bank_name").val();
    var bank_account = $("#bank_account").val();
    var domain = $("#domain").val();
    var email = $("#email").val();
    var qq = $("#qq").val();
    var client_manager = $("#client_manager").val();
    var vocation = $("#vocation").val();
    var timestart = $("#timestart").val();
    var timeend = $("#timeend").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;
    var info = {};
    info.id = id;
    info.keyid = keyid;
    info.telphone = telphone;
    info.login_name = telphone;
    info.bank_name = bank_name;
    info.bank_account = bank_account;
    info.domain = domain;
    info.email = email;
    info.qq = qq;
    info.client_manager = client_manager;
    info.vocation = vocation;
    info.contract_start_time = timestart;
    info.contract_end_time = timeend;

    if(!reg.test(telphone)){
        jalert('请填写11位手机号', function () {
        });
        return false;
    }

    $("#saveEdit").attr('disabled', true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
});


/**
 * 编辑运营商
 */
$(".saveOperatorEdit").click(function(){
    var url = $("#editName").val();
    var id = $("#id").val();
    var name = $(".name").val();
    var msg1 = $(".msg1").val();
    var login_name = $(".login_name").val();
    var share_operators = $(".share_operators").val();
    var mobile = $(".mobile").val();
    var tel = $(".tel").val();
    var fax = $(".fax").val();
    var address = $(".address").val();
    var topup_info = $(".topup_info").val();
    var alipay_info = $(".alipay_info").val();
    var invoice_title = $(".invoice_title").val();
    var domain = $(".domain").val();
    var cdkey = $(".cdkey").val();
    var pwd = $(".pwd").val();
    var signature = $(".signature").val();
    var copyright = $(".copyright").val();
    var timestart = $("#timestart").val();
    var timeend = $("#timeend").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

    if(name == ''){
        jalert('请填写公司名称', function () {
        });
        return false;
    }

    if(login_name == ''){
        jalert('请填写登录名', function () {
        });
        return false;
    }

    if (!reg.test(mobile)) {
        jalert('请填写11位手机号', function () {
        });
        return false;
    }

    var info = {};
    info.id = id;
    info.name = name;
    info.login_name = login_name;
    info.share_operators = share_operators;
    info.mobile = mobile;
    info.tel = tel;
    info.fax = fax;
    info.address = address;
    info.topup_info = topup_info;
    info.alipay_info = alipay_info;
    info.invoice_title = invoice_title;
    info.domain = domain;
    info.cdkey = cdkey;
    info.pwd = pwd;
    info.signature = signature;
    info.copyright = copyright;
    info.msg1 = msg1;
    info.contract_start_time = timestart;
    info.contract_end_time = timeend;

    if (info.login_name.length <= 2) {
        jalert('登录名长度不能少于2位', function () {
        });
        return false;
    }

    $(".saveOperatorEdit").attr('disabled', true);
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
});


$("#savePersonalPwd").one("click", function () {
    var url = $("#editPwd").val();
    var pwd = $("#password").val();
    var newPwd = $("#newpassword").val();
    var confirmPassword = $("#password2").val();
    if (newPwd.length < 6) {
        jalert('密码长度不得少于6位', function () {
        });
        return false;
    }

    if (newPwd != confirmPassword) {
        jalert('两次密码输入不一致', function () {
        });
        return false;
    }

    if (newPwd == '') {
        jalert('新密码不能为空', function () {
        });
        return false;
    } else {
        $.post(url, {pwd: pwd, newpwd: newPwd, password2: confirmPassword}, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        })
    }
});
$("#password").val('');
//重置微信绑定
function resetAgencyOpenId(tid, keyid) {
    var url = $("#resetAgencyOpenId").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;

    jconfirm('您确定要重置吗', function () {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else {
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        })
    });
}


