$("input:radio[name='passenger_type']").change(function () {
    var passenger_type = $("input:radio:checked").val();

    if (passenger_type == 1) {
        $(".birth").removeClass("hide");
        $(".birth").addClass("show");
    } else {
        $(".birth").removeClass("show");
        $(".birth").addClass("hide");
    }
});

//新增乘机人
$(".addFrequentPassenger").click(function () {
    var passengerIds = $(".passengerIds").val();
    var flightNo = $(".flightNo").val();
    var depDate = $(".depDate").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var seatCode = $(".seatCode").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var arrDate = $(".arrDate").val();
    var parPrice = $(".parPrice").val();
    var settlePrice = $(".settlePrice").val();
    var airportTax = $(".airportTax").val();
    var fuelTax = $(".fuelTax").val();
    var type = $(".type").val();
    var policyId = $(".policyId").val();
    var delId = $(".delId").val();
    var info = {};
    info.passengerIds = passengerIds;
    info.flightNo = flightNo;
    info.depDate = depDate;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.seatCode = seatCode;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.parPrice = parPrice;
    info.settlePrice = settlePrice;
    info.airportTax = airportTax;
    info.fuelTax = fuelTax;
    info.type = type;
    info.policyId = policyId;
    info.delId = delId;

    var message = 'passengerIds=' + passengerIds + '&flightNo=' + flightNo + '&depCode=' + depCode + '&arrCode=' + arrCode + '&depDate=' + depDate + '&depTime=' + depTime + '&arrTime=' + arrTime + '&planeModel=' + planeModel + '&seatCode=' + seatCode
        + '&orgJetquay=' + orgJetquay + '&dstJetquay=' + dstJetquay + '&arrDate=' + arrDate + '&parPrice=' + parPrice + '&settlePrice=' + settlePrice + '&airportTax=' + airportTax + '&fuelTax=' + fuelTax + '&type=' + type + '&policyId=' + policyId + '&delId=' + delId;

    var url = $(".addUrl").val();
    window.location.href = url + '?' + message;
});

//添加选择乘机人
$(".addPassenger").click(function () {
    var passengerIds = $(".passengerIds").val();
    var flightNo = $(".flightNo").val();
    var depDate = $(".depDate").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var seatCode = $(".seatCode").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var arrDate = $(".arrDate").val();
    var parPrice = $(".parPrice").val();
    var settlePrice = $(".settlePrice").val();
    var airportTax = $(".airportTax").val();
    var fuelTax = $(".fuelTax").val();
    var type = $(".type").val();
    var policyId = $(".policyId").val();
    var delId = $(".delId").val();
    var info = {};
    info.passengerIds = passengerIds;
    info.flightNo = flightNo;
    info.depDate = depDate;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.seatCode = seatCode;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.parPrice = parPrice;
    info.settlePrice = settlePrice;
    info.airportTax = airportTax;
    info.fuelTax = fuelTax;
    info.type = type;
    info.policyId = policyId;
    info.delId = delId;

    var message = 'passengerIds=' + passengerIds + '&flightNo=' + flightNo + '&depCode=' + depCode + '&arrCode=' + arrCode + '&depDate=' + depDate + '&depTime=' + depTime + '&arrTime=' + arrTime + '&planeModel=' + planeModel + '&seatCode=' + seatCode
        + '&orgJetquay=' + orgJetquay + '&dstJetquay=' + dstJetquay + '&arrDate=' + arrDate + '&parPrice=' + parPrice + '&settlePrice=' + settlePrice + '&airportTax=' + airportTax + '&fuelTax=' + fuelTax + '&type=' + type + '&policyId=' + policyId + '&delId=' + delId;

    var url = $(".addPassengerUrl").val();
    window.location.href = url + '?' + message;
});


$(".childCheckBox").click(function () {
    var type = $(".type").val();
    if (type == 0) {
        $(".childCheckBox").addClass('disabled');
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('该舱位不支持儿童票的购买');
        return false;
    }
});


$(".adultCheckBox").click(function () {
    var type = $(".type").val();
    if (type == 1) {
        $(".adultCheckBox").addClass('disabled');
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('该舱位不支持成人票的购买');
        return false;
    }
});

//修改乘机人
$(".editPassenger").click(function () {
    var id = $(this).find(".id").val();
    var passengerIds = $(".passengerIds").val();
    var flightNo = $(".flightNo").val();
    var depDate = $(".depDate").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var seatCode = $(".seatCode").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var arrDate = $(".arrDate").val();
    var parPrice = $(".parPrice").val();
    var settlePrice = $(".settlePrice").val();
    var airportTax = $(".airportTax").val();
    var fuelTax = $(".fuelTax").val();
    var type = $(".type").val();
    var policyId = $(".policyId").val();
    var delId = $(".delId").val();
    var info = {};
    info.passengerIds = passengerIds;
    info.flightNo = flightNo;
    info.depDate = depDate;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.seatCode = seatCode;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.parPrice = parPrice;
    info.settlePrice = settlePrice;
    info.airportTax = airportTax;
    info.fuelTax = fuelTax;
    info.type = type;
    info.policyId = policyId;
    info.delId = delId;

    var message = 'id=' + id + '&passengerIds=' + passengerIds + '&flightNo=' + flightNo + '&depCode=' + depCode + '&arrCode=' + arrCode + '&depDate=' + depDate + '&depTime=' + depTime + '&arrTime=' + arrTime + '&planeModel=' + planeModel + '&seatCode=' + seatCode
        + '&orgJetquay=' + orgJetquay + '&dstJetquay=' + dstJetquay + '&arrDate=' + arrDate + '&parPrice=' + parPrice + '&settlePrice=' + settlePrice + '&airportTax=' + airportTax + '&fuelTax=' + fuelTax + '&type=' + type + '&policyId=' + policyId + '&delId=' + delId;

    var url = $(".editUrl").val();
    window.location.href = url + '?' + message;
});

//提交订单
$(".submitOrder").click(function () {
    var url = $(".submitUrl").val();
    var passengerIds = $(".passengerIds").val();
    var flightNo = $(".flightNo").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var depDate = $(".depDate").val();
    var arrDate = $(".arrDate").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var seatCode = $(".seatCode").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var linkMan = $(".linkMan").val();
    var linkPhone = $(".linkPhone").val();
    var flightOrderCreate = $(".flightOrderCreate").val();
    var parPrice = $(".parPrice").val();
    var settlePrice = $(".settlePrice").val();
    var airportTax = $(".airportTax").val();
    var fuelTax = $(".fuelTax").val();
    var policyId = $(".policyId").val();
    var type = $(".type").val();
    var delId = $(".delId").val();
    var totalPrice = $(".ordertotal").html();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;

    if (totalPrice * 100 <= 0) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请添加乘机人');
        return false;
    }

    if (linkMan == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请填写联系人');
        return false;
    }


    if (linkPhone == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请填写手机号');
        return false;
    }

    if (!reg.test(linkPhone)) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请填写11位手机号');
        return false;
    }

    var info = {};
    info.passengerIds = passengerIds;
    info.flightNo = flightNo;
    info.depDate = depDate;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.seatCode = seatCode;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.linkMan = linkMan;
    info.linkPhone = linkPhone;
    info.flightOrderCreate = flightOrderCreate;
    info.parPrice = parPrice;
    info.settlePrice = settlePrice;
    info.airportTax = airportTax;
    info.fuelTax = fuelTax;
    info.policyId = policyId;
    info.type = type;
    info.delId = delId;

    $(".showSub").hide();
    setInterval("run()", 1000);
    $(".delPassenger").addClass('hide');
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            clearInterval(run);
            $("#showDiv").remove();
            $(".dia-close").remove();
            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);

            $(".dia-confirm").click(function () {
                var jumpUrl = $(".detailUrl").val();
                window.location.href = jumpUrl + "?id=" + $data['id'];
            });
        } else if ($data['status'] == 0) {
            $(".delPassenger").removeClass('hide');
            $(".showLoad").hide();
            $(".showSub").show();
            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        } else {
            return false;
        }
    });


});

//进行倒计时显示
var time;
var h = 30;
function run() {
    --h;

    if (h < 1) {
        h = 30;
    }
    $('#showDiv').html("提交中,请勿关闭页面  " + h);
}

$("#passenger_name").blur(function () {
    $(".firstWord").val($(this).toPinyin().replace(/[ ]/g, "").toLowerCase());
});

$(".subFrequentPassenger").click(function () {
    var url = $("#addUrl").val();
    var pinyin = $(".firstWord").val();
    var passenger_name = $("#passenger_name").val();
    var passenger_type = $("input:radio:checked").val();
    var identity_type = $(".identity_type").val();
    var identity_no = $("#identity_no").val();
    var birthday = $(".birthday").val();
    var passengerIds = $(".passengerIds").val();
    var flightNo = $(".flightNo").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var depDate = $(".depDate").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var policyId = $(".policyId").val();
    var seatCode = $(".seatCode").val();
    var settlePrice = $(".settlePrice").val();
    var parPrice = $(".parPrice").val();
    var fuelTax = $(".fuelTax").val();
    var airportTax = $(".airportTax").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var arrDate = $(".arrDate").val();
    var type = $(".type").val();

    if (passenger_name == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请填写乘客姓名!');
        return false;
    }

    if (passenger_type == 1 && birthday == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请选择生日');
        return false;
    }

    if (identity_no == '') {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请填写证件号码!');
        return false;
    }

    var info = {};
    info.passenger_name = passenger_name;
    info.pinyin = pinyin.substr(0, 1).toUpperCase();
    info.passenger_type = passenger_type;
    info.identity_type = identity_type;
    info.identity_no = identity_no;
    info.birthday = birthday;
    info.passengerIds = passengerIds;
    info.flightNo = flightNo;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.depDate = depDate;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.policyId = policyId;
    info.seatCode = seatCode;
    info.settlePrice = settlePrice;
    info.parPrice = parPrice;
    info.fuelTax = fuelTax;
    info.airportTax = airportTax;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.type = type;

    var message = 'passengerIds=' + passengerIds + '&flightNo=' + flightNo + '&depCode=' + depCode + '&arrCode=' + arrCode + '&depDate=' + depDate + '&depTime=' + depTime + '&arrTime=' + arrTime + '&planeModel=' + planeModel + '&policyId=' + policyId + '&seatCode=' + seatCode
        + '&settlePrice=' + settlePrice + '&parPrice=' + parPrice + '&fuelTax=' + fuelTax + '&airportTax=' + airportTax + '&orgJetquay=' + orgJetquay + '&dstJetquay=' + dstJetquay + '&type=' + type + '&arrDate=' + arrDate;

    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);

            var jumpUrl = $(".jumpUrl").val();
            $(".doSubmitWarning").click(function () {
                window.location.href = jumpUrl + '?' + message;
            });

            $(".doCloseWarning").click(function () {
                window.location.href = jumpUrl + '?' + message;
            });
        } else {
            $('#myModalmob').modal({backdrop: 'static', keyboard: false});
            $("#myModalmob").modal();
            $(".tipsBox").html($data['msg']);
            return false;
        }
    });
});


$(".submitPassengerInfo").click(function () {
    var flightNo = $(".flightNo").val();
    var depCode = $(".depCode").val();
    var arrCode = $(".arrCode").val();
    var depDate = $(".depDate").val();
    var depTime = $(".depTime").val();
    var arrTime = $(".arrTime").val();
    var planeModel = $(".planeModel").val();
    var seatCode = $(".seatCode").val();
    var orgJetquay = $(".orgJetquay").val();
    var dstJetquay = $(".dstJetquay").val();
    var arrDate = $(".arrDate").val();
    var parPrice = $(".parPrice").val();
    var settlePrice = $(".settlePrice").val();
    var airportTax = $(".airportTax").val();
    var fuelTax = $(".fuelTax").val();
    var type = $(".type").val();
    var policyId = $(".policyId").val();
    var flightOrderCreate = $(".flightOrderCreate").val();

    var info = {};
    info.flightNo = flightNo;
    info.depCode = depCode;
    info.arrCode = arrCode;
    info.depDate = depDate;
    info.depTime = depTime;
    info.arrTime = arrTime;
    info.planeModel = planeModel;
    info.seatCode = seatCode;
    info.orgJetquay = orgJetquay;
    info.dstJetquay = dstJetquay;
    info.arrDate = arrDate;
    info.parPrice = parPrice;
    info.settlePrice = settlePrice;
    info.airportTax = airportTax;
    info.fuelTax = fuelTax;
    info.type = type;
    info.policyId = policyId;
    info.flightOrderCreate = flightOrderCreate;

    var passengerId = [];

    $('input[name="passengerId"]:checked').each(function () {
        passengerId.push($(this).val());
    });

    var message = 'passengerId=' + passengerId + '&flightNo=' + flightNo + '&depCode=' + depCode + '&arrCode=' + arrCode + '&depDate=' + depDate + '&depTime=' + depTime + '&arrTime=' + arrTime + '&planeModel=' + planeModel + '&seatCode=' + seatCode
        + '&orgJetquay=' + orgJetquay + '&dstJetquay=' + dstJetquay + '&arrDate=' + arrDate + '&parPrice=' + parPrice + '&settlePrice=' + settlePrice + '&airportTax=' + airportTax + '&fuelTax=' + fuelTax + '&type=' + type + '&policyId=' + policyId + '&flightOrderCreate=' + flightOrderCreate;

    if (passengerId.length == 0) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('请选择乘机人');
        return false;
    }

    if (passengerId.length > 6) {
        $('#myModalmob').modal({backdrop: 'static', keyboard: false});
        $("#myModalmob").modal();
        $(".tipsBox").html('最多添加6位乘机人');
        return false;
    }

    var jumpUrl = $(".createUrl").val();
    window.location.href = jumpUrl + "?" + message;
});


function deletePassenger(tid, type) {
    var passengerIds = $(".delId").val();

    if (passengerIds == '') {
        $(".delId").attr('value', tid);
    } else {
        $(".delId").attr('value', passengerIds + ',' + tid);
    }

    var totalPrice = $(".ordertotal").html();

    if (type == 0) {
        var adultTotal = $(".adultTotal").html();
        var totalPrice = (totalPrice * 100 - adultTotal * 100) / 100;
    }


    if (type == 1) {
        var childTotal = $(".adultTotal").html();
        var totalPrice = (totalPrice * 100 - childTotal * 100) / 100;
    }

    $(".ordertotal").html(totalPrice);
    $(".table" + tid).remove();
}





