//新增
function addAuth() {
    var url = $('#chka').val();
    clearDivValue();
    showHidenDiv();
    editAuthInfo(url);
}

//编辑
function editAuth(tid) {
    var url = $('#chke').val();
    var state = "edit";
    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            showHidenDiv();
            showDivValue(tid, $data['info']);
            editAuthInfo(url, tid, state);
        } else {
            return false;
        }
    });
}

//清空数据
function clearDivValue() {
    $("#auth_name").val('');
    $("#parent").val(0);
    $("#controller").val('');
    $("#method").val('');
    $("#modal_role").val(0);
}

//弹出层
function showHidenDiv() {
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//显示数据
function showDivValue(tid, $data) {
    $("#auth_name").val($data['name']);
    $("#parent").val($data['parent_id']);
    $("#controller").val($data['controller']);
    $("#method").val($data['method']);
    $("#entity_type").val($data['entity_type']);
    $("#remark").val($data['remark']);
}

//保存
function editAuthInfo(url,tid,state){
    $("#buttonSaveAuthInfo").one("click", function () {
        var name = $("#auth_name").val();
        var parent = $("#parent").val();
        var controller = $("#controller").val();
        var action = $("#method").val();
        var entity_type = $("#entity_type").val();
        var remark = $("#remark").val();
        
         var info = {};
         info.tid = tid;
         info.name = name;
         info.parent = parent;
         info.controller = controller;
         info.action = action;
         info.entity_type = entity_type;
         info.skuState = state;
         info.remark = remark;

         if(name == '' || controller == '' || action == '' || entity_type == 0){
             jalert('请将信息填写完整', function () {});
             return false;
         }else{
             $.post(url,info, function (resp) {
                if (resp.status == "1") {
                    jalert(resp.msg, function () {
                        location.reload();
                    });
                } else {
                    jalert(resp.Message);
                }
            }, "json").error(function (xmlHttpRequest) {
                jalert("服务器错误:" + xmlHttpRequest.status);
            });
         }
    });
    
}

