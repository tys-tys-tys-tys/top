
var BASE_URL = '__PUBLIC__/js/webuploader-0.1.5';

// 图片上传demo
jQuery(function () {
    var $ = jQuery,
            $list = $('#fileList'),
            // 优化retina, 在retina下这个值是2
            ratio = window.devicePixelRatio || 1,
            $btnUploadCtrl = $('#btnUploadCtrl'),
            // 缩略图大小
            thumbnailWidth = 100 * ratio,
            thumbnailHeight = 100 * ratio,
            state = 'pending',
            fileCount = 0,
            // 添加的文件总大小
            fileSize = 0,
            // 所有文件的进度信息，key为file id
            percentages = {},
            // Web Uploader实例
            uploader;

    // 初始化Web Uploader
    uploader = WebUploader.create({
        // 自动上传。
        auto: false,
        // swf文件路径
        swf: BASE_URL + '/js/Uploader.swf',
        // 文件接收服务端。
        server: 'http://upload1.lydlr.com/UploadSingle.do?folder=tour',
        // 选择文件的按钮。可选。
        // 内部根据当前运行是创建，可能是input元素，也可能是flash.
        pick: '#filePicker',
        // 只允许选择文件，可选。
        accept: {
            title: 'Images',
            extensions: 'gif,jpg,jpeg,bmp,png',
            mimeTypes: 'image/*'
        }
    });



    // 当有文件添加进来的时候
    uploader.on('fileQueued', function (file) {
        var $li = $(
                '<div id="' + file.id + '" class="row upload-item uploader-wait" style="margin-bottom: 10px;" path="" isCover="0">' +
                '<div class="col-sm-2"><img class="img-preview" alt="" src="http://cdn.lydlr.com/public/system/images/temp/5.jpg"></div>' +
                '<div class="col-sm-8">' +
                '<textarea rows="3" class="form-control" placeholder="请输入图片描述" ></textarea>' +
                '</div>' +
                '<div class="col-sm-2">' +
                '<div class="dropdown">' +
                '<button data-toggle="dropdown" id="dropdownMenu1" class="btn dropdown-toggle" type="button">' +
                '操作' +
                '<span class="caret"></span>' +
                '</button>' +
                '<ul aria-labelledby="dropdownMenu1" role="menu" class="dropdown-menu">' +
                '<li role="presentation">' +
                '<a class="move-up" href="#" tabindex="-1" role="menuitem">上移</a>' +
                '</li>' +
                '<li role="presentation">' +
                '<a class="move-down" href="#" tabindex="-1" role="menuitem">下移</a>' +
                '</li>' +
                '<li role="presentation">' +
                '<a class="setCover" href="javascript:;" tabindex="-1" role="menuitem">设为封面</a>' +
                '</li>' +
                '<li role="presentation">' +
                '<a class="removeFile" href="javascript:;" tabindex="-1" role="menuitem">' +
                '移除图片' +
                '</a>' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '<div class="upload-msg"></div>' +
                '</div> ' +
                '</div>'
                ),
                $img = $li.find('img');

        $list.append($li);

        // 创建缩略图
        uploader.makeThumb(file, function (error, src) {
            if (error) {
                $img.replaceWith('<span>不能预览</span>');
                return;
            }

            $img.attr('src', src);
        }, thumbnailWidth, thumbnailHeight);

        //增加队列 Jet
        fileCount++;

        $li.on('click', 'a.removeFile', function () {
            //console.log(file.name);
            uploader.removeFile(file);
        });
    });

    uploader.onFileDequeued = function (file) {
        fileCount--;
        fileSize -= file.size;

        if (!fileCount) {
            setState('pedding');
        }

        removeFile(file);
        //updateTotalProgress();

    };

    function setState(val) {

    }
    // 负责view的销毁
    function removeFile(file) {
        var $li = $('#' + file.id);

        delete percentages[file.id];
        //updateTotalProgress();
        $li.off().find('.removeFile').off().end().remove();
    }
    // 文件上传过程中创建进度条实时显示。
    uploader.on('uploadProgress', function (file, percentage) {
        var $li = $('#' + file.id),
                $percent = $li.find('.progress span');

        // 避免重复创建
        if (!$percent.length) {
            $percent = $('<p class="progress"><span></span></p>')
                    .appendTo($li)
                    .find('span');
        }

        $percent.css('width', percentage * 100 + '%');
    });

    // 文件上传成功，给item添加成功class, 用样式标记上传成功。
    uploader.on('uploadSuccess', function (file) {
        $('#' + file.id).addClass('upload-state-done');
    });

    // 文件上传失败，现实上传出错。
    uploader.on('uploadError', function (file) {
        var $li = $('#' + file.id),
                $error = $li.find('div.error');

        // 避免重复创建
        if (!$error.length) {
            $error = $('<div class="error"></div>').appendTo($li);
        }

        $error.text('上传失败');
    });

    // 完成上传完了，成功或者失败，先删除进度条。
    uploader.on('uploadComplete', function (file) {
        $('#' + file.id).find('.progress').remove();
    });

    uploader.on('uploadAccept', function (file, response) {
        if (response._raw.indexOf("#") != 0) {
            // 通过return false来告诉组件，此文件上传有错。
            return false;
        }
        $('#' + file.file.id).find("div.upload-msg").text('上传成功');
		$('#' + file.file.id).removeClass("uploader-wait");
        $('#uploadMessage').text(file.file.name + " 上传成功");
        $('#' + file.file.id).attr('path', response._raw);
        //$("#respPath").val(response._raw);
        return true;
    });

	uploader.on("uploadFinished",
        function() {
            $("#uploadMessage").text("所有文件上传结束");
        });

    uploader.on('uploadSuccess', function (file) {
        //$('#' + file.id).find("div.upload-msg").text('已上传');
    });

    uploader.on('uploadError', function (file) {
        var msg = $('#' + file.id).find('.upload-smg');
        $(msg).text('上传出错');
    });

    uploader.on('uploadProgress', function (file, percentage) {
        $('#' + file.id).find("div.upload-msg").text(parseInt(percentage * 100) + "%");
    });

    $btnUploadCtrl.on('click', function () {
        if (state === 'uploading') {
            uploader.stop();
        } else {
            if (fileCount > 0) {
                $('#uploadMessage').text("正在上传...");
                uploader.upload();
            } else {
                $('#uploadMessage').text("请选择文件");
            }
        }
    });
});

$(".uploaded").click(function () {
    $(this).closest("div.upload-item").remove();
});

$("body").on("click", ".move-up", function (e) {
    e.preventDefault();
    var current = $(this).closest("div.upload-item");
    var prev =$(current).prev("div.upload-item");
    current.insertBefore(prev)
});

$("body").on("click", ".move-down", function (e) {
    e.preventDefault();
    var current = $(this).closest("div.upload-item");
    var next =$(current).next("div.upload-item");
    next.insertBefore(current)
});