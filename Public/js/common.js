$(function(){
	
	//左侧菜单
	$("[data-event-trigger]").on("click",function(){
		$(this).toggleClass("tg").siblings("dd").slideToggle();
	});	
	
	$("[data-event-upload]").on("click",function(){
		$("#upload").trigger("click");	
	});
	
	$('#myTab a').click(function (e) {
		$(this).parents("span").addClass("active");
		$(this).parents("span").siblings("span").removeClass("active");
	    e.preventDefault()
	    $(this).tab('show')
	})
	
	$("[data-event-click]").on(function(){
		$(this).toggleClass("tg").siblings(".slide").slideToggle();
	});
});


function jalert(message,okHandler) {
    dialog({
        title: '提示',
        content: message,
        width: 200,
        okValue: '确定',
        ok: okHandler,
    }).showModal();
}

function jconfirm(message,okHandler) {
    dialog({
        title: '提示',
        content: message,
        width: 200,
        okValue: '确定',
        ok: okHandler,
        cancel: function () {
        },
        cancelValue: '取消'
    }).showModal();
}
//Request.QueryString("name")
Request = {
    QueryString: function (item) {
        var svalue = location.search.match(new RegExp("[\?\&]" + item + "=([^\&]*)(\&?)", "i"));
        return svalue ? svalue[1] : svalue;
    }
}

//一级
$("input.checkMain").change(function(){
    if($(this).is(":checked")){
        $(this).closest("dl").find(".sub").prop("checked", true);
    }else{
        $(this).closest("dl").find(".sub").prop("checked",false);
    }
});

//二级
$("input.parentRole").change(function(){
    if($(this).is(":checked")){
        $(this).closest("dl").find(".checkMain").prop("checked",true);
        $(this).closest(".div2").find(".sub").prop("checked", true);
    }else{
        $(this).closest(".div2").find(".sub").prop("checked", false);

        var div1UnChecked = true;
        var div1 = $(this).closest("dl");
        $(div1).find(".div2 .parentRole").each(function(){
            if($(this).is(":checked")){
                div1UnChecked = false;
            }
        });

        if(div1UnChecked){
            $(div1).find(".checkMain").prop("checked", false);
        }
    }
});

//三级
$("input.check3").change(function(){
    if($(this).is(":checked")){
        $(this).closest("dl").find(".checkMain").prop("checked",true);
        $(this).closest(".div3").find(".sub").prop("checked", true);
        $(this).closest(".div2").find(".parentRole").prop("checked", true);
    }else{
        $(this).closest(".div3").find(".checkRole").prop("checked", false);

        var div2UnChecked = true;
        var div2 = $(this).closest(".div2");
        $(div2).find(".div3 .check3").each(function(){
            if($(this).is(":checked")){
                div2UnChecked = false;
            }
        });

        if(div2UnChecked){
            $(div2).find(".parentRole").prop("checked", false);
        }


        var div1UnChecked = true;
        var div1 = $(this).closest("dl");
        $(div1).find(".div2 .parentRole").each(function(){
            if($(this).is(":checked")){
                div1UnChecked = false;
            }
        });

        if(div1UnChecked){
            $(div1).find(".checkMain").prop("checked", false);
        }
    }
});

//四级
$("input.checkRole").change(function(){
    if($(this).is(":checked")){
        $(this).closest("dl").find(".checkMain").prop("checked",true);
        $(this).closest(".div2").find(".parentRole").prop("checked", true);
        $(this).closest(".div3").find(".check3").prop("checked", true);
    }else{
        var div3 = $(this).closest(".div3");
        var allUnChecked = true;

        $(div3).find(".div4 .checkRole").each(function(){
            if($(this).is(":checked")){
                allUnChecked = false;
            }
        });

        if(allUnChecked){
            $(div3).find(".check3").prop("checked",false);
        }

        var div2UnChecked = true;
        var div2 = $(this).closest(".div2");
        $(div2).find(".div3 .check3").each(function(){
            if($(this).is(":checked")){
                div2UnChecked = false;
            }
        });

        if(div2UnChecked){
            $(div2).find(".parentRole").prop("checked", false);
        }


        var div1UnChecked = true;
        var div1 = $(this).closest("dl");
        $(div1).find(".div2 .parentRole").each(function(){
            if($(this).is(":checked")){
                div1UnChecked = false;
            }
        });

        if(div1UnChecked){
            $(div1).find(".checkMain").prop("checked", false);
        }

    }
});


$(".name").click(function(){
    var mainName = $(this).closest(".div1").find(".div2").css("display");
    if(mainName == 'block'){
        $(this).closest(".div1").find(".div2").hide();
    }else{
        $(this).closest(".div1").find(".div2").show();
    }
});


$(".name2").click(function(){
    var mainName = $(this).closest(".div2").find(".div3").css("display");
    if(mainName == 'block'){
        $(this).closest(".div2").find(".div3").hide();
    }else{
        $(this).closest(".div2").find(".div3").show();
    }
});

$('.name3').click(function(){
    var mainName = $(this).closest(".div3").find(".div4").css("display");
    if(mainName == 'block'){
        $(this).closest(".div3").find(".div4").hide();
        //$(this).closest(".div3").find(".div4-1").hide();
    }else{
        $(this).closest(".div3").find(".div4").show();
        //$(this).closest(".div3").find(".div4-1").show();
    }
});


$(".authHover").mouseover( function() {
    this.style.background = "#eee";
}).mouseout( function(){
    this.style.background = "none";
});