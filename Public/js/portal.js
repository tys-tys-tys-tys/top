$(".name").val('');
$(".phone").val('');
$(".remark").val('');
$(".company").val('');

$(".JoinProviderButton").click(function () {
    var url = $("#join").val();
    var name = $(".name").val(); 
    var phone = $(".phone").val();
    var company = $(".company").val();
    var remark = $(".remark").val();
    
    if (name == '') {
        jalert('请输入您的姓名', function () {
        });
        return false;
    }
    

    if (phone == '') {
        jalert('请输入您的手机号', function () {
        });
        return false;
    }
    
    if (company == '') {
        jalert('请输入您的公司名称', function () {
        });
        return false;
    }
    
    var info = {};
    info.name = name;
    info.phone = phone;
    info.company = company;
    info.remark = remark;
    
    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            jalert($data['msg'], function () {
                location.reload();
            });
        } else {
            jalert($data['msg'], function () {
            });
            return false;
        }
    })
});