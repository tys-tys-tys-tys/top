//改变价钱
$("#startTime").change(function () {
    var url = $("#changeOrderMoney").val();
    var startTime = $(this).val();
    var id = $("#id").val();
    $.post(url, {startTime: startTime, id: id}, function (data) {
        //console.log(data);
        if (data) {
            $(".priceAdultList").html(data.price_adult_list);
            setTotal();
        }
    })

});

//提交订单
$("#doSaveCruiseInfo").click(function () {
    var url = $("#orderInfo").val();
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var adultCount = $("#adult").val();
    var startTime = $("#startTime").val();
    var contacts = $("#contacts").val();
    var phone = $("#phone").val();
    var remark = $("#remark").val();
    var code = $("#code").val();
    var account_state = $("#account_state").val();

    if(account_state != 1){
        $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
        $("#myModalSuc").modal();
        $(".tipsBox").html('账户被冻结');
        return false;
    }
    
    if(startTime == '' || startTime == null){
        $("#myModalDel").modal({backdrop: 'static', keyboard: false});
        $("#myModalDel").modal();
        $(".tipsBox").html('暂无团期！');
        return false;
    }
    
    if (contacts == '' || phone == '') {
        $("#myModalDel").modal();
        return false;
    } else {
        jconfirm('您确定要提交订单吗', function () {
            $.post(url, {
                id: id,
                keyid:keyid,
                client_adult_count: adultCount,
                code:code,
                start_time: startTime,
                contacts: contacts,
                contact_phone: phone,
                memo: remark
            }, function ($data) {
                if($data['status'] == 3){
                   $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html('请勿重复提交！');
                    return false;
                }else if ($data['status'] == 1) {
                    $(".proofAgain").attr("disabled", true);
                    $("#myModalSuc").modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".doOrderInfoJump").click(function () {
                        var jump = $("#myorder").val();
                        location.href = jump;
                    })

                }else if($data['status'] == 0){
                    $('#myModalSuc').modal({backdrop: 'static', keyboard: false});
                    $("#myModalSuc").modal();
                    $(".tipsBox").html($data['msg']);
                    return false;
                } else {
                    return false;
                }
            });
        });
    }

});

setTotal();
//成人
$("#adult").blur(function () {
    var adultNum = $("#adult").val();
    if (adultNum < 1) {
        $(".adult").val(1);
    }
    setTotal();
})

//数量+ 成人
$(".adultCruiseAdd").click(function () {
    var adultNum = $(this).parent().find('input[class*=adultTotal]');
    adultNum.val(parseInt(adultNum.val()) + 1);
    setTotal();
});
//数量- 成人
$(".adultCruiseReduce").click(function () {
    var adultNum = $(this).parent().find('input[class*=adultTotal]');
    adultNum.val(parseInt(adultNum.val()) - 1);
    if (parseInt(adultNum.val()) < 1) {
        adultNum.val(1);
    }
    setTotal();
});

function setTotal() {

    var s = s1 = 0;
    var adultTotal = $(".adultTotal");
    s1 += parseInt(adultTotal.val() * $(".priceAdultList").html());
    s = s1;
    $("#total").html(s.toFixed(2));
}