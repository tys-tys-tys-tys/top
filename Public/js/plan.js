//新增线路
function addPlan(tid, keyid) {
    var url = $('#chka').val();
    clearDivValue();
    showHidenDiv();
    savePlan(tid, keyid, url);
}

//编辑
function editPlan(tid, keyid) {
    clearDivValue();
    var url = $('#chke').val();
    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            showHidenDiv();
            showDivValue(tid, $data['info']);
            savePlan(tid, keyid, url);
        } else {
            return false;
        }
    });
}

//清空数据
function clearDivValue() {
    $("#activity_addr").val('');
    $("#traffic1").prop('checked', false);
    $("#traffic2").prop('checked', false);
    $("#traffic3").prop('checked', false);
    $("#traffic4").prop('checked', false);
    $("#traffic5").prop('checked', false);
    $("#scenery").val('');
    $("#description").val('');
    $("#hotel").val('');
    $("#dining1").prop('checked', false);
    $("#dining2").prop('checked', false);
    $("#dining3").prop('checked', false);
}

//显示数据
function showDivValue(tid, $data) {
    $("#modal_name").children('div').children('input').val($data['name']);

    //行程
    var daynum = $('#daynum' + tid).children('strong').html();
    $('#top').html(daynum);
    $("#activity_addr").val($data['activity_addr']);

    $("input.traffic").prop("checked", false)
    if ($data['traffic'] == '1') {
        $("#traffic1").prop('checked', true);
    } else if ($data['traffic'] == '2') {
        $("#traffic2").prop('checked', true);
    } else if ($data['traffic'] == '3') {
        $("#traffic3").prop('checked', true);
    } else if ($data['traffic'] == '4') {
        $("#traffic4").prop('checked', true);
    } else if ($data['traffic'] == '5') {
        $("#traffic5").prop('checked', true);
    }

    //1,2,3
    //1,2
    //3
    //2,3
    var dinings = $data['dining'];
    if (dinings != undefined) {
        if (dinings.indexOf('1') >= 0) {
            $("#dining1").prop('checked', true);
        }
        if (dinings.indexOf('2') >= 0) {
            $("#dining2").prop('checked', true);
        }
        if (dinings.indexOf('3') >= 0) {
            $("#dining3").prop('checked', true);
        }
    }

    $("#scenery").val($data['scenery']);
    $("#description").val($data['description']);
    $("#hotel").val($data['hotel']);
}

//弹出层
function showHidenDiv(msg) {
    $('#myModalLabel').html(msg);
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//修改线路时，新增、修改 详细行程
function savePlan(tid, keyid, url) {
        $("#buttonSavePlan").click(function(){
        var activity_addr = $("#activity_addr").val();
        var traffic = ""
        if ($("#traffic1").is(":checked")) {
            traffic = "1";
        }
        if ($("#traffic2").is(":checked")) {
            traffic = "2";
        }
        if ($("#traffic3").is(":checked")) {
            traffic = "3";
        }
        if ($("#traffic4").is(":checked")) {
            traffic = "4";
        }
        if ($("#traffic5").is(":checked")) {
            traffic = "5";
        }

        var scenery = $("#scenery").val();
        var description = $("#description").val();
        var hotel = $("#hotel").val();
        var tourId = $("#tourId").val();

        var dining = "";
        if ($("#dining1").is(":checked")) {
            dining += "1";
            dining += ",";
        }
        if ($("#dining2").is(":checked")) {
            dining += "2";
            dining += ",";
        }
        if ($("#dining3").is(":checked")) {
            dining += "3";
            dining += ",";
        }
        
        var act = "edit";
        if (tid == "")
        {
            act = "insert";
        }

        var info = {};
        info.day = tid;
        info.keyid = keyid;
        info.tour_id = tourId;
        info.tid = tid;
        info.activity_addr = activity_addr;
        info.traffic = traffic;
        info.scenery = scenery;
        info.description = description;
        info.hotel = hotel;
        info.dining = dining;
        info.skuState = act;

        if(info.activity_addr == ''){
            jalert('您尚未填写活动地区', function () {
            });
            return false
        }

        if(info.traffic == ''){
            jalert('您尚未勾选交通信息', function () {
            });
            return false
        }

        if(info.scenery == ''){
            jalert('您尚未填写景点及活动信息', function () {
            });
            return false
        }

        if(info.description == ''){
            jalert('您尚未填写行程描述信息', function () {
            });
            return false
        }

        if (info.hotel == '') {
            jalert('您尚未填写住宿信息', function () {
            });
            return false;
        }

        var url = $("#savePlanUrl").val();
        $.post(url, info, function (resp) {
            if (resp.status == "1") {
                jalert(resp.msg, function () {
                    location.reload();
                });
            } else {
                jalert(resp.Message);
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
        });
    });
}