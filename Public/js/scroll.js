function scrollNews(selector,Entry,time,StartIndex)
{
	var _self=this;
	this.selector=selector;
	this.Entry=Entry;
	this.time = time;
	this.i=StartIndex||0;
	this.Count=$(this.selector+" ul li").length;
	$(this.selector+" ul li").hide();//全部隐藏
	$(this.selector+" ul li").eq(this.i).show();//第i个显示
	$(this.selector).bind("mouseenter",function(){
		if(_self.sI){clearInterval(_self.sI);}
	}).bind("mouseleave",function(){
		_self.showIndex(_self.i++);
	})
	/*生成激活OL项目*/
	for(var j=0;j<this.Count;j++)
		$(this.selector+" .activeOL").append('<li><a onclick="'+this.Entry+'.showIndex('+j+');" href="#"><img src="images/crystal.gif"></a></li>');
	$(this.selector+" ol li a").eq(this.i).addClass("active");
	this.sI=setInterval(this.Entry+".showIndex(null)",this.time);

	this.Getselector=function(){return this.selector;}
	this.showIndex=function(index)
	{
		this.i++;//显示下一个
		if(this.sI){clearInterval(this.sI);}
		this.sI=setInterval(this.Entry+".showIndex()",this.time);
		if (index!=null)
		{
			this.i=index;
		}
		if(this.i==this.Count)
			this.i=0;
		$(this.selector+" ul li").hide();
		$(this.selector+" ul li").eq(this.i).slideDown();
		$(this.selector+" ol li a").removeClass("active");
		$(this.selector+" ol li a").eq(this.i).addClass("active");
	}
}

var scroll=new scrollNews("#scroll","scroll"  , 3000 , 1);