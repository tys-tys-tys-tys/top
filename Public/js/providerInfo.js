//新增
function addProvider() {
    clearDivValue();
    showHidenDiv();
}

//弹出层
function showHidenDiv() {
    $('.theme-popover-mask').fadeIn(100);
    $('.theme-popover').slideDown(200);
    $("#name").focus();
    $('.theme-poptit .closes').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
    $('#reset').click(function () {
        $('.theme-popover-mask').fadeOut(100);
        $('.theme-popover').slideUp(200);
        clearDivValue();
        //防止修改添加url获取出错，要重新加载页面
        location.reload();
    });
}

//清空数据
function clearDivValue() {
    $("#name").val('');
    $("#login_name").val('');
    $("#login_pwd").val('');
    $("#login_pwd2").val('');
    $("#contact_person").val('');
    $("#mobile").val('');
    $("#destinations").val('');
}

//新增供应商 - 个人信息
$("#buttonSaveProviderInfo").click(function () {
    var opname = $("#operid").val();
    var name = $("#name").val();
    var full_name = $("#fullName").val();
    var login_name = $("#login_name").val();
    var login_pwd = $("#login_pwd").val();
    var login_pwd2 = $("#login_pwd2").val();
    var destinations = $("#destinations").val();
    var commission_rate = $("#commission_rate").val();
    var url = $("#chka").val();

    if(commission_rate == ''){
        commission_rate = 10;
    }

    var info = {};
    info.opname = opname;
    info.name = name;
    info.full_name = full_name;
    info.login_name = login_name;
    info.login_pwd = login_pwd;
    info.login_pwd2 = login_pwd2;
    info.destinations = destinations;
    info.commission_rate = commission_rate;

    if(name == ''){
        jalert('请填写公司简称', function () {
        });
        return false;
    }

    if(login_name == ''){
        jalert('请填写登录名', function () {
        });
        return false;
    }

    if(login_pwd == ''){
        jalert('请填写登陆密码', function () {
        });
        return false;
    }

    if(login_pwd2 == ''){
        jalert('请填写重复密码', function () {
        });
        return false;
    }

    if(destinations == ''){
        jalert('请填写经营目的地', function () {
        });
        return false;
    }

    if (login_pwd.length < 6) {
        jalert('密码长度不得少于6位', function () {
        });
        return false;
    }

    if (info.login_name.length <= 2) {
        jalert('登录名长度不能少于2位', function () {
        });
        return false;
    }

    $("#buttonSaveProviderInfo").attr("disabled", true);
    $.post(url, info, function (resp) {
        if (resp.status == "1") {
            jalert(resp.msg, function () {
                location.reload(true);
            });
        } else {
            jalert(resp.msg, function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
});

//修改供应商 - 个人信息
$(".saveEditProviderInfo").click(function () {
    var id = $("#id").val();
    var keyid = $("#keyid").val();
    var name = $("#name").val();
    var phone = $("#phone").val();
    var login_name = $("#login_name").val();
    var contact_person = $("#contact_person").val();
    var mobile = $("#mobile").val();
    var bank_name = $("#bank_name").val();
    var bank_account = $("#bank_account").val();
    var destinations = $("#destinations").val();
    var commission_rate = $("#commission_rate").val();
    var timestart = $("#timestart").val();
    var timeend = $("#timeend").val();
    var url = $("#editName").val();
    var settlement_interval = $("#settlement_interval").val();
    var reg = /^0?1[3|4|5|8|7][0-9]\d{8}$/;
    
    var info = {};
    info.id = id;
    info.keyid = keyid;
    info.name = name;
    info.phone = phone;
    info.login_name = login_name;
    info.contact_person = contact_person;
    info.mobile = mobile;
    info.destinations = destinations;
    info.bank_name = bank_name;
    info.bank_account = bank_account;
    info.commission_rate = commission_rate;
    info.settlement_interval = settlement_interval;
    info.timestart = timestart;
    info.timeend = timeend;

    if($(".productMessage").is(":checked")){
        info.productMessage = 1;
    }else{
        info.productMessage = 0;
    }

    if($(".transactionMessage").is(":checked")){
        info.transactionMessage = 1;
    }else{
        info.transactionMessage = 0;
    }

    if(name == ''){
        jalert('请填写公司简称', function () {
        });
        return false;
    }

    if(login_name == ''){
        jalert('请填写登录名', function () {
        });
        return false;
    }

    if(contact_person == ''){
        jalert('请填写业务联系人', function () {
        });
        return false;
    }

    if(mobile == ''){
        jalert('请填写业务联系人手机', function () {
        });
        return false;
    }

    if(!reg.test(mobile)){
        jalert('请填写11位手机号', function () {
        });
        return false;
    }

    if (destinations == '') {
        jalert('请填写经营目的地', function () {
        });
        return false;
    }

    if (!$("#mobile").val().match(/^1[3|4|5|7|6|8][0-9]\d{4,8}$/)) {
        jalert('请填写11位手机号', function () {
        });
        return false;
    }
    
    if (info.login_name.length <= 2) {
        jalert('登录名长度不能少于2位', function () {
        });
        return false;
    }

    $("#saveEditProviderInfo").attr("disabled", true);
    $.post(url, info, function (resp) {
        if (resp.status == "1") {
            jalert(resp.msg, function () {
                location.reload(true);
            });
        } else {
            jalert(resp.msg, function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    });
});


/**
 * 高级编辑
 */
function seniorEditProviderInfo(tid, keyid, tname, fname) {
    var name = tname;
    $('#myModalProviderInfo').modal({backdrop: 'static', keyboard: false});
    $("#myModalProviderInfo").modal();
    $(".name").val(tname);
    $(".full_name").val(fname);
    $('.submitEdit').one("click", function(){
        saveSeniorEdit(tid, keyid);
    });
}

function saveSeniorEdit(tid, keyid) {
    var url = $("#saveSeniorEdit").val();
    var full_name = $(".full_name").val();
    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.full_name = full_name;

    if (full_name == '') {
        $('#myModalAgencyInfo').modal('hide');
        jalert('请填写公司全称', function () {
        });
        return false;
    }

    $.post(url, info, function ($data) {
        if ($data['status'] == 1) {
            $('#myModalAgencyInfo').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
        } else {
            $('#myModalAgencyInfo').modal('hide');
            jalert($data['msg'], function () {
                location.reload(true);
            });
            return false;
        }
    }, "json").error(function (xmlHttpRequest) {
        jalert("服务器错误:" + xmlHttpRequest.status);
    })
}

