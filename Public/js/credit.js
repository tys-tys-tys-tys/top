/**
 * [授信申请 弹框]
 */
function applyCredit() {
    $('#myModalLabel').modal({backdrop: 'static', keyboard: false});
    $("#myModalLabel").modal();
    $("#amount").val('');
    $("#proof").val('');
    $("#memo").val('');
    $('#saveApply').click(function () {
        saveApplyCredit();
    });
}

$("#topUpType").change(function(){
    var topup_type = $(".topup_type").val();

    if(topup_type == 3){
        $(".remitterName").show();
    }else{
        $(".remitterName").hide();
    }

    if(topup_type == 4){
        $(".bankInfo").show();
    }else{
        $(".bankInfo").hide();
    }
})

function saveApplyCredit() {
    var url = $("#saveApplyCredit").val();
    var account = $("#account").val();
    var amount = $("#amount").val();
    var topup_type = $(".topup_type").val();
    var code = $("#code").val();
    var state = $("#state").val();

    var info = {};
    info.account = account;
    info.amount = amount;
    info.topup_type = topup_type;
    info.code = code;
    info.state = state;

    if(topup_type == 3){
        var payer = $(".payer").val();
        info.payer = payer;

        if(info.payer == ''){
            $('#myModalLabel').modal('hide');
            jalert('请填写汇款方', function () {
            });
            return false;
        }
    }


    if(topup_type == ''){
        $('#myModalLabel').modal('hide');
        jalert('请选择充值类型', function () {
        });
        return false;
    }

    if (amount == '') {
        $('#myModalLabel').modal('hide');
        jalert('请将信息输入完整', function () {
        });
        return false;
    } else {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                $('#myModalLabel').modal('hide');
                jalert($data['msg'], function () {
                    location.reload();
                });
            } else if($data['status'] == 0){
                $('#myModalLabel').modal('hide');
                jalert($data['msg'], function () {
                });
                return false;
            }else{
                $('#myModalLabel').modal('hide');
                return false;
            }
        })
    }

}



function changeApplyState(tid, keyid, oid, state) {
    var code = $("#code").val();
    var url = $("#chkState").val();

    var info = {};
    info.id = tid;
    info.keyid = keyid;
    info.operator_id = oid;
    info.code = code;
    info.state = state;

    jconfirm('您确定要同意吗', function () {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert('操作成功', function () {
                    location.reload();
                });
            } else if($data['status'] == 0){
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }else{
                return false;
            }
        });
    });
}


function changeRejectState(tid, keyid, oid, state) {
    var url = $("#chkState").val();
    var info = {};
    info.id = tid;
    info.state = state;
    info.operator_id = oid;
    info.keyid = keyid;

    jconfirm('您确定要同意吗', function () {
        $.post(url, info, function ($data) {
            if ($data['status'] == 1) {
                jalert('操作成功', function () {
                    location.reload();
                });
            } else if($data['status'] == 0){
                jalert($data['msg'], function () {
                    location.reload();
                });
                return false;
            }else{
                return false;
            }
        });
    });
}