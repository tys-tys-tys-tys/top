//登录验证
$(function () {
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            $("#loginForm").click();
        }
    });

    $('#loginForm').click(function () {
        var loginname = $(".userloginname").val();
        var loginpwd = $(".userloginpwd").val();
        if (loginname == '') {
            $("#msgInfo").html('登录名不能为空');
            return false;
        } else if (loginpwd == '') {
            $("#msgInfo").html('密码不能为空');
            return false;
        } else {
            var url = $('#loginsbumit').val();
            $.post(url, {loginname: loginname, loginpwd: loginpwd}, function ($data) {
                if ($data['status'] == 0) {
                    $("#msgInfo").html($data['msg']);
                    return false;
                } else {
                    window.location.href = $data['url'];
                }
            });
        }


    });
});
