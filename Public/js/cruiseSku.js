//新增位控
function addCruiseSku(header, tid, keyid, header, show) {
    var url = $('#cruise').val();
    clearCruiseDivValue();
    $('.theme-popover-mask').fadeIn(100);
    $('.cruiseDiv').slideDown(200);

    if (show == 'showCruiseDates') {
        var ss = showCruiseDates();
    }

    addCruiseInfo(tid, keyid, url, '');
}

$(".closeCruise").one("click", function(){
    $('.theme-popover-mask').fadeOut(100);
    $('.cruiseDiv').slideUp(200);
    clearCruiseDivValue();
    //防止修改添加url获取出错，要重新加载页面
    location.reload();
});

//位控编辑
function editCruiseSku(tid, keyid, header, show) {
    clearCruiseDivValue();
    var url = $('#editCruise').val();
    var state = "edit";

    $.post(url, {id: tid}, function ($data) {
        if ($data['status'] == 1) {
            $('.theme-popover-mask').fadeIn(100);
            $('.cruiseDiv').slideDown(200);

            if (show == 'showCruiseDateOne') {
                timesCruiseOne();
            }

            showDivCruiseValue(tid, $data['info']);
            addCruiseInfo(tid, keyid, url, state);
        } else {
            return false;
        }
    });
}

//关闭时清空DIV，重载页面
$("#closeCuriseDiv").click(function () {
    clearCruiseDivValue();
    location.reload();
});

//清空数据
function clearCruiseDivValue() {
    $("#timestartOne").attr('');
    $("#timestartOne").val('');
    $(".cruise").val('');
    $("#min_price").val('');
}

//时间多选
function showCruiseDates() {
    $('.dateCruiseCoenter').show();
    $('.dateCruiseCoenterOne').hide();
    var uzSFC = new HLTSelectFullCalendarContext({
        id: '#timeDateShowCruise',
        callback: function ($data) {
            $("#cruiseDataValues").val($data);
        }
    });

    $("#timeDateShowCruise").fullCalendar({
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        }
        , height: 100
        , selectable: true
        , editable: false
        , select: function (start, end, allDay) {
            uzSFC.selectDates(start, end);
        }
        , events: function (start, end, callback) {
            uzSFC.initContainer(start, end);
        }
    });
}

//时间单选
function timesCruiseOne() {
    $('.dateCruiseCoenterOne').show();
    $('.dateCruiseCoenter').hide();
}

//显示数据
function showDivCruiseValue(tid, $data) {
    //console.log($data);
    $("#timeCruiseStartOne").attr('placeholder', $data['start']);
    $("#timeCruiseStartOne").val($data['start']);
    $('.cruise').val($data['cruise']);
    $("#min_price").val($data['price_adult_list']);
}

//验证及传值到后台处理
function addCruiseInfo(tid, keyid, url, state) {
    $('#saveCruiseInfo').click(function () {
        var times = $("#cruiseDataValues").val();//多个时间
        var timestartOne = $("#timeCruiseStartOne").val();
        var min_price = $("#min_price").val();
        var cruise = $(".cruise").val();
        var info = {};

        info.tid = tid;
        info.keyid = keyid;
        info.times = times;
        info.timestartOne = timestartOne;
        info.cruise = cruise;
        info.skuState = state;
        info.min_price = min_price;

        if (min_price == '' || cruise == '') {
            jalert('请将信息填写完整', function () {
                location.reload();
            });
            $('#myModalEdit').modal('hide');
            return false;
        }

        $('#saveCruiseInfo').attr('disabled', true);
        $.post(url, info, function (resp) {
            if (resp.status == "1") {
                jalert(resp.msg, function () {

                    location.reload();
                });
                $('#myModalEdit').modal('hide');

            } else {
                jalert(resp.msg, function () {
                    location.reload();
                });
                $('#myModalEdit').modal('hide');
                return false;
            }
        }, "json").error(function (xmlHttpRequest) {
            jalert("服务器错误:" + xmlHttpRequest.status);
            return false;
        });
    });

}


